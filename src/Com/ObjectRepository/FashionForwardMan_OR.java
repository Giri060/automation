package Com.ObjectRepository;

import org.openqa.selenium.By;

public class FashionForwardMan_OR 
{
	
	public By FFM = By.xpath("//ul[@class='nav nav-pills']/li[6]/a/span");

	public By FFM_Image = By.xpath("//section[@class='catalog-grid']/div/div[1]/div/div[1]/a[2]/img");
	
	public By FFM_Image1 = By.xpath("//div/div/div/div/a/img");

	public By Readmore = By.linkText("Read More");

	public By Readless = By.linkText("Read Less");

	public By FFM_Description = By.xpath("//*[@id='show_description']");

	public By FFM_Description1 = By.xpath("//*[@id='hide_more_description']");

	public By FFM_MRP = By.xpath("//div[3]/span");

	public By FFM_Product1 = By.xpath("//a[@id='shirt-btn']/img");

	public By FFM_Product2 = By.xpath("//a[@id='mens-full-sleeve-btn']/img");
	
	public By FFM_Product3 = By.xpath("//a[@id='FFM-btn']/img");


}
