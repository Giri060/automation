package Com.ObjectRepository;

import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;

public class MyAccount_OR 
{

	WebDriver driver;

	public By DiscountDailogclose = By.className("privy-x");
	
	public By Dashboard = By.xpath("//a[contains(text(),'Dashboard')]");

	public By CreateSell = By.xpath("//a[contains(@href, '/design')]");

	public By Create_AddText = By.xpath("//li[@id='text-tab']/a/i");

	public By Create_UploadImage = By.xpath("//li[@id='upload-image-tab']/a/i");

	public By Create_Uploadfile = By.xpath("//*[@id='image-upload']");

	public By Create_Uploadfilemsg1 = By.cssSelector("div.bootbox-body");

	public By Create_UploadfileOKbtn = By.cssSelector("div.modal-footer > button.btn.btn-primary");

	public By Create_Imgecenter = By.xpath(".//*[@id='align_c']");

	public By Create_ImageFilter = By.xpath("//li[@id='filters-tab']/a/i");

	public By Create_MirrorEffect = By.xpath("//li[@id='mirror-tab']/a/i");

	public By Create_AddClipart = By.xpath("//li[@id='art-tab']/a/a/i");

	public By Create_EnterText = By.xpath("//textarea[@id='add-text']");

	public By Create_ChooseFont = By.xpath("//div[@id='font-area']/div/div/div/div");

	public By Create_selectFont = By.xpath("//li[@id='hover_font']/span");

	public By Create_FontSize = By.xpath("//select[@id='fontSizeDD']");

	public By Create_FontColor = By.xpath("//div[@class='tab-pane active']/div[4]/div/div[2]/div/div");

	public By Create_FontColorBlue = By.xpath("//div[@id='0000ff']");

	public By Create_Bold = By.xpath("//button[@id='text-bold']");

	public By Create_Italic = By.xpath("//button[@id='text-italic']");

	public By Create_AlignLeft = By.xpath("//button[@id='left']");

	public By Create_AlignCenter = By.xpath("//button[@id='center']");

	public By Create_AlignRight = By.xpath("//button[@id='right']");

	public By Create_Flip = By.xpath("//button[@id='flip']");

	public By Create_Undo = By.xpath("//button[@id='undo']");

	public By Create_Copy = By.xpath("//button[@id='copy']");

	public By Create_Paste = By.xpath("//button[@id='paste']");

	public By Create_Delete = By.xpath("//button[@id='deleteBtn']");

	public By Create_Clear = By.xpath("//button[@id='clear']");

	public By Create_Save = By.xpath("//button[@id='save']");

	public By Create_ProductDD = By.xpath("//select[@id='product-select']");

	public By Create_Cost = By.xpath("//div[@id='products-select']/i");

	public By Create_Submit = By.xpath("//button[@id='before_submit']");

	public By Create_ColorBlack = By.xpath("//li[contains(@title,'Black')]");

	public By Create_ColorWhite = By.xpath("//li[contains(@title,'White')]");

	public By Create_ColorGrey = By.xpath("//li[contains(@title,'Grey Melange')]");

	public By Create_ColorYellow = By.xpath("//li[contains(@title,' Naples Yellow')]");

	public By Create_ColorRoyalBlue = By.xpath("//li[contains(@title,'Royal Blue')]");

	public By Create_ColorNavyBlue = By.xpath("//li[contains(@title,'Navy Blue')]");

	public By Create_ColorRed	 = By.xpath("//li[contains(@title,'Red')]");

	public By Create_Color1	 = By.xpath("//ul[@id='colorselector']/li[1]");

	public By Create_Color2	 = By.xpath("//ul[@id='colorselector']/li[2]");

	public By Create_Color3	 = By.xpath("//ul[@id='colorselector']/li[3]");

	public By Create_Color4	 = By.xpath("//ul[@id='colorselector']/li[4]");

	public By Create_Color5	 = By.xpath("//ul[@id='colorselector']/li[5]");

	public By Create_Color6	 = By.xpath("//ul[@id='colorselector']/li[6]");

	public By Create_Color7	 = By.xpath("//ul[@id='colorselector']/li[7]");

	public By Create_Product_Color	 = By.xpath("//*[@id='colorselector']/div/div/div/div");

	public By Create_Product_ColorChange	 = By.xpath("//*[@id='33ff33']");

	public By Create_Product_ColorChange1	 = By.xpath("//*[@id='00ccff']");

	public By Create_ImageFilter_ConverttoBlack	 = By.xpath("//label[contains(text(),'Convert Image To Black')]");

	public By Create_ImageFilter_ReduceColor	 = By.xpath("//label[contains(text(),'Reduce Color')]");

	public By Create_ImageFilter_OriginalImage	 = By.xpath("//label[contains(text(),'Back to Original Image')]");

	public By Create_ImageFilter_ImageColor	 = By.xpath("//*[@id='image-filters']/div[4]/div/div[2]/div/div[1]");

	public By Create_ImageFilter_ImageColorChange	 = By.xpath("//div[@id='ff0099']");

	public By Create_ImageFilter_Apply = By.xpath("//*[@id='tint_color_val']");

	public By Create_Mirrorr_Horizontal	 = By.xpath("//*[@id='rotate-h']");

	public By Create_Mirrorr_Vertical	 = By.xpath(".//*[@id='rotate-v']");

	public By Create_Clipart_Color	 = By.xpath("//div[@id='art-color']/div/div/div[2]/div");

	public By Create_Clipart_ColorChange	 = By.cssSelector("#art-color > div.trow > div.tcell > div.simpleColorContainer > div.simpleColorChooser > #ffff33");

	public By Create_Clipart_CategoryDD	 = By.xpath(".//*[@id='category-selector']");

	public By Create_Clipart_Image1	 = By.xpath(".//*[@id='computer']/div/div[1]/img");

	public By Create_Clipart_Apply	 = By.xpath("//div[@id='art-color']/div/div/button");





	public By Create_Campaign_MarkupPriceField	 = By.cssSelector("small");

	public By Create_Campaign_MarkupPrice	 = By.xpath("//div[@id='appmain']/div/div/div/div/div/div[4]/div/div/div[2]/input");

	public By Create_Campaign_SellingpriceField	 = By.xpath("//div[@id='appmain']/div/div/div/div/div/div[4]/div/div/div[3]/small");

	public By Create_Campaign_Sellingprice	 = By.xpath("//div[@id='appmain']/div/div/div/div/div/div[4]/div/div/div[3]/span/h3");

	public By Create_Campaign_AddColors	 = By.xpath("//*[@id='appmain']/div[1]/div/div[1]/div/div/div[4]/div/button");

	public By Create_Campaign_Color1	 = By.xpath("//*[@id='appmain']/div[1]/div/div[1]/div/div/div[4]/div/div[2]/div[2]/button[1]");

	public By Create_Campaign_Done	 = By.xpath("//div[@id='appmain']/div/div/div/div/div/div[4]/div/div[2]/div[3]/button");

	public By Create_Campaign_ColorRemove	 = By.xpath("//*[@id='appmain']/div[1]/div/div[1]/div/div/div[4]/div/span[2]/a/i");

	public By Create_Campaign_AddProdcuts	 = By.xpath("//div[@id='appmain']/div/div/div/div/div/div[6]/button");

	public By Create_Campaign_ProductDD	 = By.xpath(".//*[@id='product-select']");

	public By Create_Campaign_Add	 = By.xpath(".//*[@id='appmain']/div[1]/div/div[1]/div/div/div[5]/div/div/div[2]/button");

	public By Create_Campaign_ProductDelete	 = By.xpath(".//*[@id='appmain']/div[1]/div/div[1]/div/div/div[5]/button");

	public By Create_Campaign_Next	 = By.xpath(".//*[@id='appmain']/div[1]/div/div[1]/div/div/div[7]/button");

	public By Create_Campaign_Title	 = By.xpath(".//*[@id='campaign_title']");

	public By Create_Campaign_TitleExamples = By.xpath("//a[contains(text(),'(see examples)')]");

	public By Create_Campaign_TitleExample1 = By.xpath("//a[contains(text(),'Example 1')]");

	public By Create_Campaign_TitleExample1Text = By.xpath("//div[@id='example-1']");

	public By Create_Campaign_TitleExample2 = By.xpath("//a[contains(text(),'Example 2')]");

	public By Create_Campaign_TitleExample2Text = By.xpath("//div[@id='example-2']");

	public By Create_Campaign_TitleExample3 = By.xpath("//a[contains(text(),'Example 3')]");

	public By Create_Campaign_TitleExample3Text = By.xpath("//div[@id='example-3']");

	public By Create_Campaign_TitleExampleClose = By.xpath("//div[@id='title-example']/div/div/div/button");

	public By Create_Campaign_Description	 = By.xpath(".//*[@id='wysiwyg-editor']");

	public By Create_Campaign_DescriptionExamples = By.xpath("//form[@id='frm_reset_discription']/div[2]/label/span/a");

	public By Create_Campaign_DescriptionExample1 = By.xpath("//a[contains(@href, '#example1')]");

	public By Create_Campaign_DescriptionExample1Text = By.xpath("//div[@id='example1']");

	public By Create_Campaign_DescriptionExample2 = By.xpath("//a[contains(@href, '#example2')]");

	public By Create_Campaign_DescriptionExample2Text = By.xpath("//div[@id='example2']");

	public By Create_Campaign_DescriptionExample3 = By.xpath("//a[contains(@href, '#example3')]");

	public By Create_Campaign_DescriptionExample3Text = By.xpath("//div[@id='example3']");

	public By Create_Campaign_DescriptionExampleClose = By.xpath("//div[@id='description-example']/div/div/div/button");
	
	public By Create_Campaign_CampaignLengthDD = By.xpath(".//*[@id='campaign_length']");
	
	public By Create_Campaign_SelectCategoryDD = By.xpath("//input[@type='search']");
	
	public By Create_Campaign_SelectCategoryMovies = By.xpath(".//*[@id='select2-tags-result-hyqi-Movies']");
	
	public By Create_Campaign_TermsRadio = By.xpath(".//*[@id='terms']");
	
	public By Create_Campaign_Finish = By.xpath(".//*[@id='next_step']");
	
	public By Create_Campaign_CongratsMsg = By.xpath("//div[@class='panel panel-default']/div/h3");
	
	public By Create_Campaign_FinishClickHere = By.xpath("//a[contains(text(),'Click here')]");
	
	public By Create_Campaign_Howitworks = By.xpath("//a[contains(text(),'1. How it works?')]");
	
	public By Create_Campaign_HowitworksText = By.xpath("//div[@class='col-lg-9']/h1");
	
	public By Create_Campaign_IntroAds = By.xpath("//a[contains(text(),'2. Introduction to Facebook Ads')]");
	
	public By Create_Campaign_CreateFB = By.xpath("//a[contains(text(),'3. Create a Facebook page')]");
	
	public By Create_Campaign_TypesFB = By.xpath("//a[contains(text(),'4. Types of Facebook Ads')]");
	
	public By Create_Campaign_LaunchFBAd = By.xpath("//a[contains(text(),'5. Launch your first Facebook Ad')]");
	
	public By Create_Campaign_MailSupport = By.xpath("//a[contains(text(),'sellersuccess@mydreamstore.in')]");
	
	public By Create_Campaign_MailSupportText = By.xpath("//a/div");
	
	public By Create_Campaign_JoinFBGroup = By.xpath("//img[@id='btn_grp']");
	
	public By Create_Campaign_JoinFBGroupText = By.xpath(".//*[@id='header_block']/span");
	
	public By Create_Campaign_InstallFBPixel = By.xpath("//a[contains(text(),'6. Installing Facebook Pixel in your My Dream Store account')]");
	
	
	
	
	
	public By Account_ProfileSettings = By.xpath("//a[contains(text(),'Profile Settings')]");
	
	public By Account_ProfileSettings_PrivateRadio = By.xpath("//*[@id='enable_private_campaign']");

	public By Account_ProfileSettings_AffiliateRadio = By.xpath("//*[@id='enable_affiliate_for_all_campaigns']");
	
	public By Account_ProfileSettings_ChangesOK = By.className("confirm");

	public By Account_ProfileSettings_GoogleAnalytics = By.xpath(".//*[@id='tracking']");
	
	public By Account_ProfileSettings_GoogleAnalyticsSave = By.xpath(".//*[@id='analyticsSubmit']");

	public By Account_ProfileSettings_FBPixel = By.xpath(".//*[@id='fb_c_pixel']");

	public By Account_ProfileSettings_FBPixelSave = By.xpath(".//*[@id='fb_c_submit']");

	public By Account_ProfileSettings_GSTNumber = By.xpath(".//*[@id='gst_in_number']");

	public By Account_ProfileSettings_GSTNumberSave = By.xpath(".//*[@id='gst_submit']");

	public By Account_ProfileSettings_PayoutSettings = By.xpath("//a[contains(@href, '#payout')]");
	
	public By Account_ProfileSettings_PayoutSettingsAccNo = By.xpath("//input[@id='accountnumber']");

	public By Account_ProfileSettings_PayoutSettingsAccName = By.xpath("//input[@id='name']");

	public By Account_ProfileSettings_PayoutSettingsBankName = By.xpath("//input[@id='bank']");

	public By Account_ProfileSettings_PayoutSettingsBranchName = By.xpath("//input[@id='branch']");

	public By Account_ProfileSettings_PayoutSettingsIFSC = By.xpath("//input[@id='ifsc']");
	
	public By Account_ProfileSettings_PayoutSettingsPAN = By.xpath(".//*[@id='pan']");

	public By Account_ProfileSettings_PayoutSettingsSave = By.xpath(".//*[@id='neftSubmit']");

	public By Account_ProfileSettings_PayoutSettingsDashboard = By.xpath("//div[4]/ul/li[3]/a");
	
	public By Account_UpdateProfile = By.xpath("//a[contains(text(),'Update Profile')]");
	
	public By Account_UpdateProfile_Name = By.xpath(".//*[@id='firstName']");

	public By Account_UpdateProfile_Address = By.xpath(".//*[@id='address']");

	public By Account_UpdateProfile_Colony = By.xpath(".//*[@id='street_2']");

	public By Account_UpdateProfile_Landmark = By.xpath(".//*[@id='landmark']");

	public By Account_UpdateProfile_Contact = By.xpath(".//*[@id='phone_number']");

	public By Account_UpdateProfile_State = By.xpath(".//*[@id='state']");

	public By Account_UpdateProfile_City = By.xpath(".//*[@id='city_div']");
	
	public By Account_UpdateProfile_Zip = By.xpath(".//*[@id='zip_code']");

	public By Account_UpdateProfile_AddressType = By.xpath("//select[@name='address_type']");

	public By Account_UpdateProfile_Change = By.xpath("//button[@type='submit']");

	public By Account_UpdateProfile_Msg = By.xpath("html/body/div[7]/p");
	
	public By Account_UpdateProfile_OK = By.xpath("html/body/div[7]/div[7]/div/button");
	
	
	
	/* Store fronts */
	public By Account_Storefronts = By.xpath("//a[@href='https://mydreamstore.in/storefront']");
	
	public By Account_Storefront_Createstorefront = By.xpath("//a[@href='/storefront/create']");
		
	public By Account_Storefront_name = By.xpath("//input[@id='name']");
	
	public By Account_Storefront_description = By.xpath("//textarea[@id='description']");

	public By Account_Storefront_url = By.xpath("//input[@id='url']");

	public By Account_Storefront_create = By.xpath("//button[@type='submit'][contains(text(),'Create')]");

	public By Account_Storefront_ok = By.xpath("//button[@class='confirm']");
	
	public By Account_Storefront_textdisplyed = By.xpath("//div[@class='sweet-alert showSweetAlert visible']");

	public By Account_Storefront_campaign1 = By.xpath("//table[@class='table table-hover']/tbody/tr[1]//td[1]/input[1]");

	public By Account_Storefront_campaign2 = By.xpath("//table[@class='table table-hover']/tbody/tr[2]//td[1]/input[1]");

	







	
	
	
	
	

















}
