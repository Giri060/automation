package Com.ObjectRepository;

import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;

public class ComputerSkin_OR 
{
	WebDriver driver;
	
	public By ComputerSkinlink = By.xpath("//center/ul/li[6]/a/span");
	
	public By ComputerSkin_Image1 = By.xpath("//*[@id='appmain']/div/div/div[2]/div[3]/div[1]/section/div/div/div/div[1]/a[1]/img");
	
	public By Readmore = By.linkText("Read More");

	public By Readless = By.linkText("Read Less");

	public By ComputerSkin_Description = By.xpath("//*[@id='hide_more_description']/p");
	
	public By ComputerSkin_MRP = By.xpath("//div[3]/span");
	
	public By ComputerSkin_Product = By.xpath("//*[@id='product-select']");


}
