package Com.ObjectRepository;

import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;

public class Login_OR {
	
	WebDriver driver;
	
	public By log = By.xpath("html/body/header/div/div/div[3]/ul/li[3]/a");
	
	public By Logintext = By.xpath("//div[@id='appmain']/div/form/div/center/h3");
	
	//public By Login = By.linkText("Login");
	
	public By Login = By.className("login header-font font_top");
	
	//public By MyAccount = By.linkText("My Account");
	
	public By Login_Email = By.xpath("//input[@id='data']");
	
	public By Email_error = By.xpath("//span[@class='errorMessage ng-binding ng-scope']");
	
	public By Password_error = By.xpath(".//*[@id='appmain']/div/form/div[3]/span[3]");
	
	public By Login_Continue = By.xpath("//button[@type='submit']");
	
	public By Login_password = By.xpath("//input[@type='password']");
	
	public By Login_loginbutton = By.xpath("//button[@type='submit']");
	
	public By Login_login = By.className("button buttonBlue ng-scope");
	
	public By MyAccount = By.xpath("//a[contains(text(),'My Account')]");
	
	public By Logout = By.xpath(".//*[@id='logout_button']");
	
	public By Mobile = By.name("mobile");
	
	public By Mobilecode = By.xpath("//span[@class='mobilecode']");
	
	public By MobileErrormsg = By.xpath("//span[@class='errorMessage ng-scope']");
	
	public By MobileErrormsg1 = By.xpath("//span[@class='errorMessage ng-scope'][contains(text(),'Mobile Number Already Registered')]");

	
	public By Passworderror = By.xpath("//span[@class='errorMessage ng-scope'][contains(text(),'Password should be minimum 6 characters')]");
	

}
