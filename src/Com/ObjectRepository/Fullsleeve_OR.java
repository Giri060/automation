package Com.ObjectRepository;

import org.openqa.selenium.By;

public class Fullsleeve_OR 
{
	public By Fullsleeve = By.xpath("//span[contains(text(),'FULL SLEEVE')]");

	public By Fullsleeve_Image1 = By.xpath("//section[@class='catalog-grid']/div/div[1]/div/div[1]/a[2]/img");

	public By Readmore = By.linkText("Read More");

	public By Readless = By.linkText("Read Less");

	public By Fullsleeve_Description = By.xpath("//*[@id='show_description']");

	public By Fullsleeve_Description1 = By.xpath("//*[@id='hide_more_description']");

	public By Fullsleeve_MRP = By.xpath("//div[3]/span");

	public By Fullsleeve_Product1 = By.xpath("//a[@id='shirt-btn']/img");

	public By Fullsleeve_Product2 = By.xpath("//a[@id='mens-full-sleeve-btn']/img");

	public By Fullsleeve_Product3 = By.xpath("//a[@id='hoodie-btn']/img");


}
