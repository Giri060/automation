package Com.ObjectRepository;

import org.openqa.selenium.By;

public class Hoodie_OR 
{
	public By Hoodie = By.xpath("//ul[@class='nav nav-pills']/li[5]/a/span");

	public By Hoodie_Image1 = By.xpath("//section[@class='catalog-grid']/div/div[1]/div/div[1]/a[2]/img");

	public By Readmore = By.linkText("Read More");

	public By Readless = By.linkText("Read Less");

	public By Hoodie_Description = By.xpath("//*[@id='show_description']");

	public By Hoodie_Description1 = By.xpath("//*[@id='hide_more_description']");

	public By Hoodie_MRP = By.xpath("//div[3]/span");

	public By Hoodie_Product1 = By.xpath("//a[@id='shirt-btn']/img");

	public By Hoodie_Product2 = By.xpath("//a[@id='mens-full-sleeve-btn']/img");
	
	public By Hoodie_Product3 = By.xpath("//a[@id='hoodie-btn']/img");

}
