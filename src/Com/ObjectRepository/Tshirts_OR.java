package Com.ObjectRepository;

import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;

public class Tshirts_OR 
{
	
	WebDriver driver;
	
	public By Tshirts = By.linkText("T-SHIRTS");
	
	public By Tshirts_Image1 = By.xpath("//section[@class='catalog-grid']/div/div/div/div/a[2]");
	
	public By Readmore = By.linkText("Read More");
	
	public By Readless = By.linkText("Read Less");
	
	public By Tshirts_Description = By.xpath("//*[@id='show_description']");
	
	public By Tshirts_Description1 = By.xpath("//*[@id='hide_more_description']");
	
	public By Tshirts_Product1 = By.xpath("//a[@id='shirt-btn']/img");
	
	public By Tshirts_Product2 = By.xpath("//a[@id='w-round-neck-tee-btn']/img");

	public By Tshirts_Product3 = By.xpath("//a[@id='mens-full-sleeve-btn']/img");

	public By Tshirts_MRP = By.xpath("//div[3]/span");
	
	
	
	

}
