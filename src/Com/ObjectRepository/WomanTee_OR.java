package Com.ObjectRepository;

import org.openqa.selenium.By;

public class WomanTee_OR 
{
	public By WomanTee = By.xpath("//ul[@class='nav nav-pills']/li[4]/a/span");

	public By WomanTee_Image1 = By.xpath("//section[@class='catalog-grid']/div/div[1]/div/div[1]/a[2]/img");

	public By Readmore = By.linkText("Read More");

	public By Readless = By.linkText("Read Less");

	public By WomanTee_Description = By.xpath("//*[@id='show_description']");

	public By WomanTee_Description1 = By.xpath("//*[@id='hide_more_description']");

	public By WomanTee_MRP = By.xpath("//div[3]/span");

	public By WomanTee_Product1 = By.xpath("//a[@id='shirt-btn']/img");

	public By WomanTee_Product2 = By.xpath("///a[@id='w-round-neck-tee-btn']/img");

	

}
