package Com.ObjectRepository;

import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;

public class Mugs_OR {
	
WebDriver driver;
	
	public By Mugslink = By.xpath("//center/ul/li[7]/a/span");
	
	public By Mugs_Image1 = By.xpath(".//*[@id='appmain']/div/div/div[2]/div[3]/div[1]/section/div/div[1]/div/div[1]/a[2]/img");
	
	public By Readmore = By.linkText("Read More");

	public By Readless = By.linkText("Read Less");

	public By Mugs_Description1 = By.xpath(".//*[@id='show_description']/p[1]");
	
	public By Mugs_Description2 = By.xpath(".//*[@id='show_description']/p[6]");
	
	//public By Mugs_Description2 = By.xpath(".//*[@id='show_description']/p[6]");
	
	public By Mugs_Description3 = By.xpath("//div[@id='show_description']");



	
	public By Mugs_MRP = By.xpath("//div[3]/span");
	
	public By Mugs_Product = By.xpath("//*[@id='product-select']");


}
