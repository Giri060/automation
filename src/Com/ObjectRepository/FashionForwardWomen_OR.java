package Com.ObjectRepository;

import org.openqa.selenium.By;

public class FashionForwardWomen_OR 
{
	public By FFW = By.xpath("//ul[@class='nav nav-pills']/li[7]/a/span");

	public By FFW_Image = By.xpath("//section[@class='catalog-grid']/div/div[1]/div/div[1]/a[2]/img");
	
	public By FFW_Image1 = By.xpath("//div[2]/div/div/a/img");

	public By Readmore = By.linkText("Read More");

	public By Readless = By.linkText("Read Less");

	public By FFW_Description = By.xpath("//*[@id='show_description']");

	public By FFW_Description1 = By.xpath("//*[@id='hide_more_description']");

	public By FFW_MRP = By.xpath("//div[3]/span");

	public By FFW_Product1 = By.xpath("//a[@id='shirt-btn']/img");

	public By FFW_Product2 = By.xpath("//a[@id='mens-full-sleeve-btn']/img");
	
	public By FFW_Product3 = By.xpath("//a[@id='FFW-btn']/img");
	

}
