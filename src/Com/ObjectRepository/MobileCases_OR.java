package Com.ObjectRepository;

import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;

public class MobileCases_OR {

	WebDriver driver;

	public By Mobilecaseslink = By.xpath("//ul[@class='nav nav-pills']/li[4]/a/span");

	public By Mobilecases_Image1 = By.xpath("//section[@class='catalog-grid']/div/div[1]/div/div[1]/a/img");

	public By Readmore = By.linkText("Read More");

	public By Readless = By.linkText("Read Less");

	public By Mobilecases_Description1 = By.xpath("//*[@id='show_description']/p");

	public By Mobilecases_Description2 = By.xpath("//*[@id='show_description']/p[3]");

	public By Mobilecases_Description3 = By.xpath("//*[@id='show_description']/p[6]");

	public By Mobilecases_Description4 = By.xpath("//*[@id='hide_more_description']/p");

	public By Mobilecases_Description5 = By.xpath("//*[@id='show_description']/ul");

	public By Mobilecases_ProductDD = By.xpath("	//*[@id='product-select']");

	public By Mobilecases_MRP = By.xpath("//div[3]/span");




}
