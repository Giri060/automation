package Com.Homepage;
import java.io.File;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.IOException;
import java.util.LinkedHashMap;
import java.util.List;
import java.util.Map;
import java.util.Set;
import java.util.Date;
import java.util.concurrent.TimeUnit;
import org.apache.poi.hssf.usermodel.HSSFCell;
import org.apache.poi.hssf.usermodel.HSSFCellStyle;
import org.apache.poi.hssf.usermodel.HSSFRow;
import org.apache.poi.hssf.usermodel.HSSFSheet;
import org.apache.poi.hssf.usermodel.HSSFWorkbook;
import org.apache.poi.ss.usermodel.Row;
import org.apache.poi.ss.usermodel.Cell;
import org.apache.poi.ss.usermodel.CellStyle;
import org.apache.poi.ss.usermodel.IndexedColors;
import org.openqa.selenium.By;
import org.openqa.selenium.Keys;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.interactions.Actions;
import org.openqa.selenium.support.Color;
import org.openqa.selenium.support.ui.ExpectedConditions;
import org.openqa.selenium.support.ui.WebDriverWait;
import org.testng.Assert;
import org.testng.annotations.AfterClass;
import org.testng.annotations.BeforeClass;
import org.testng.annotations.Test;
import com.aventstack.extentreports.Status;
import com.aventstack.extentreports.markuputils.ExtentColor;
import com.aventstack.extentreports.markuputils.MarkupHelper;
import Com.ObjectRepository.Homepage2_OR;
import Com.ObjectRepository.Homepage_OR;
import Com.ObjectRepository.Login_OR;
import Com.Utilities.Browsers;
import Com.Utilities.CommonMethods;
import Com.Utilities.GetScreenShot;
import Com.Utilities.Screenshot;
import Com.Utilities.Steps;
import Com.Utilities.TestBase;
import jxl.format.Border;
import jxl.format.UnderlineStyle;
import jxl.write.Colour;
import jxl.write.WritableCellFormat;
import jxl.write.WritableFont;
import jxl.write.WriteException;
public class FooterLinks extends TestBase
{
	WebDriver driver;
	Login_OR login = new Login_OR();
	Steps steps = new Steps();
	GetScreenShot getscreen = new GetScreenShot();
	Screenshot screen = new Screenshot();
	public String strAbsolutepath = new File("").getAbsolutePath();
	HSSFWorkbook workbook;
	HSSFSheet sheet;
	String strDataFileName = this.getClass().getSimpleName();
	Map<String, Object[]> testresultdata; 
	CommonMethods commonmethods = new CommonMethods();
	Homepage_OR home = new Homepage_OR();
	Homepage2_OR home2 = new Homepage2_OR();
	Screenshot screenshot = new Screenshot();
	String passstatus = screenshot.status("PASS");
	String failstatus = screenshot.status("FAIL");
	int cnt = 2;
	public static String FacebookPage = "";
	public static String TwitterPage = "";
	public static String InstagramPage = "";
	public static String MDSBlogPage = "";
	public static String FollowUSstatus = "";
	public static String FollowUSstatusfail = "";
	public static String AboutPage = "";
	public static String FAQPage = "";
	public static String ReturnRefundpolicyPage = "";
	public static String UniversityPage = "";
	public static String BlogPage = "";
	public static String SupportPage = "";
	public static String ContactPage = "";
	public static String CareersPage = "";
	public static String PrivacyPolicyPage = "";
	public static String TermsofUsePage = "";
	public static String FooterLinksstatus = "";
	public static String FooterLinksstatusfail = "";
	public FooterLinks()
	{
		Browsers b = new Browsers();
		this.driver = b.getBrowsers("chrome1");		 
	}
	@Test(priority = 0)
	public void FollowUS() throws IOException, InterruptedException
	{
		String name = new Object(){}.getClass().getEnclosingMethod().getName();
		cnt ++;
		try 
		{
			test = extent.createTest("Follow US");
			System.out.println("Follow US Test Case Executing...");
			commonmethods.Login(driver);
			test.log(Status.INFO, MarkupHelper.createLabel("User able to Login", ExtentColor.BLUE));
			Thread.sleep(3000);
			commonmethods.URLNaviagtion(driver);
			Thread.sleep(1000);
			steps.clickJSButton(home.Mydreamstore, driver, name);
			String homepage = getscreen.capture(driver, name);
			test.addScreenCaptureFromPath(homepage);
			steps.MoveElement(home2.FollowUS_Facebook, driver, name);
			steps.clickJSButton(home2.FollowUS_Facebook, driver, name);
			test.log(Status.INFO, MarkupHelper.createLabel("User clicked on Facebook Image under FOLLOW US", ExtentColor.BLUE));
			Thread.sleep(2000);
			FacebookPage = getscreen.capture(driver, "FacebookPage");
			test.addScreenCaptureFromPath(FacebookPage);
			Thread.sleep(2000);
			driver.navigate().back();
			Thread.sleep(2000);
			steps.MoveElement(home2.FollowUS_Twitter, driver, name);
			steps.clickJSButton(home2.FollowUS_Twitter, driver, name);
			test.log(Status.INFO, MarkupHelper.createLabel("User clicked on Twitter Image under FOLLOW US", ExtentColor.BLUE));
			Thread.sleep(2000);
			TwitterPage = getscreen.capture(driver, "TwitterPage");
			test.addScreenCaptureFromPath(TwitterPage);
			Thread.sleep(2000);
			driver.navigate().back();
			Thread.sleep(2000);
			steps.MoveElement(home2.FollowUS_Instagram, driver, name);
			steps.clickJSButton(home2.FollowUS_Instagram, driver, name);
			test.log(Status.INFO, MarkupHelper.createLabel("User clicked on Instagram Image under FOLLOW US", ExtentColor.BLUE));
			Thread.sleep(2000);
			InstagramPage = getscreen.capture(driver, "InstagramPage");
			test.addScreenCaptureFromPath(InstagramPage);
			Thread.sleep(2000);
			driver.navigate().back();
			Thread.sleep(2000);
			steps.MoveElement(home2.FollowUS_MDSBlog, driver, name);
			steps.clickJSButton(home2.FollowUS_MDSBlog, driver, name);
			test.log(Status.INFO, MarkupHelper.createLabel("User clicked on MDS Blog Image under FOLLOW US", ExtentColor.BLUE));
			Thread.sleep(2000);
			MDSBlogPage = getscreen.capture(driver, "MDSBlogPage");
			test.addScreenCaptureFromPath(MDSBlogPage);
			Thread.sleep(2000);
			driver.navigate().back();
			Thread.sleep(2000);
			FollowUSstatus = passstatus;
			Assert.assertTrue(true);
			System.out.println("Follow US Successful");
			testresultdata.put(Integer.toString(cnt), new Object[] {name, "Follow US", "Follow US Successful",passstatus});	
		}
		catch(Exception e)
		{
			System.out.println("Follow US Unsuccessful");
			testresultdata.put(Integer.toString(cnt), new Object[] {name, "Follow US", "Follow US Unsuccessful",failstatus});
			FollowUSstatus = failstatus;
			FollowUSstatusfail = getscreen.capture(driver, "FollowUSstatusfail");
			test.addScreenCaptureFromPath(FollowUSstatusfail);
			Assert.assertTrue(false);
		}
	}
	@Test(priority = 1)
	public void Footer() throws IOException, InterruptedException
	{
		String name = new Object(){}.getClass().getEnclosingMethod().getName();
		cnt ++;
		try 
		{
			test = extent.createTest("Footer Links");
			System.out.println("Footer Links Test Case Executing...");
			Thread.sleep(1000);
			commonmethods.URLNaviagtion(driver);
			Thread.sleep(1000);
			/*commonmethods.Login(driver);
			test.log(Status.INFO, MarkupHelper.createLabel("User able to Login", ExtentColor.BLUE));
			Thread.sleep(3000);
			steps.clickJSButton(home.Mydreamstore, driver, name);
			String homepage = getscreen.capture(driver, name);
			test.addScreenCaptureFromPath(homepage);*/
			String Parent = driver.getWindowHandle();
			steps.MoveElement(home2.Footer_About, driver, name);
			steps.clickJSButton(home2.Footer_About, driver, name);
			test.log(Status.INFO, MarkupHelper.createLabel("User clicked on About link under Footer Links", ExtentColor.BLUE));
			Thread.sleep(2000);
			AboutPage = getscreen.capture(driver, "AboutPage");
			test.addScreenCaptureFromPath(AboutPage);
			Thread.sleep(2000);
			driver.navigate().back();
			Thread.sleep(2000);
			steps.MoveElement(home2.Footer_FAQ, driver, name);
			steps.clickJSButton(home2.Footer_FAQ, driver, name);
			test.log(Status.INFO, MarkupHelper.createLabel("User clicked on FAQ link under Footer Links", ExtentColor.BLUE));	
			Thread.sleep(2000);
			FAQPage = getscreen.capture(driver, "FAQPage");
			test.addScreenCaptureFromPath(FAQPage);
			Thread.sleep(2000);
			driver.navigate().back();
			Thread.sleep(2000);
			steps.MoveElement(home2.Footer_Return_Refundpolicy, driver, name);
			steps.clickJSButton(home2.Footer_Return_Refundpolicy, driver, name);
			test.log(Status.INFO, MarkupHelper.createLabel("User clicked on Return/Refund Policy link under Footer Links", ExtentColor.BLUE));
			Thread.sleep(2000);
			ReturnRefundpolicyPage = getscreen.capture(driver, "ReturnRefundpolicyPage");
			test.addScreenCaptureFromPath(ReturnRefundpolicyPage);
			Thread.sleep(2000);
			driver.navigate().back();
			Thread.sleep(2000);
			steps.MoveElement(home2.Footer_University, driver, name);
			steps.clickJSButton(home2.Footer_University, driver, name);
			test.log(Status.INFO, MarkupHelper.createLabel("User clicked on University link under Footer Links", ExtentColor.BLUE));
			Thread.sleep(2000);
			steps.ChildWindow(driver);
			driver.manage().window().maximize();
			Thread.sleep(2000);
			UniversityPage = getscreen.capture(driver, "UniversityPage");
			test.addScreenCaptureFromPath(UniversityPage);
			driver.close();
			Thread.sleep(2000);
			driver.switchTo().window(Parent);
			Thread.sleep(2000);
			steps.MoveElement(home2.Footer_Blog, driver, name);
			steps.clickJSButton(home2.Footer_Blog, driver, name);
			test.log(Status.INFO, MarkupHelper.createLabel("User clicked on Blog link under Footer Links", ExtentColor.BLUE));
			Thread.sleep(2000);
			BlogPage = getscreen.capture(driver, "BlogPage");
			test.addScreenCaptureFromPath(BlogPage);
			Thread.sleep(2000);
			driver.navigate().back();
			Thread.sleep(2000);
			steps.MoveElement(home2.Footer_Support, driver, name);
			steps.clickJSButton(home2.Footer_Support, driver, name);
			test.log(Status.INFO, MarkupHelper.createLabel("User clicked on Support link under Footer Links", ExtentColor.BLUE));
			Thread.sleep(2000);
			SupportPage = getscreen.capture(driver, "SupportPage");
			test.addScreenCaptureFromPath(SupportPage);
			Thread.sleep(2000);
			driver.navigate().back();
			Thread.sleep(2000);
			steps.MoveElement(home2.Footer_Contact, driver, name);
			steps.clickJSButton(home2.Footer_Contact, driver, name);
			test.log(Status.INFO, MarkupHelper.createLabel("User clicked on Contact link under Footer Links", ExtentColor.BLUE));
			Thread.sleep(2000);
			ContactPage = getscreen.capture(driver, "ContactPage");
			test.addScreenCaptureFromPath(ContactPage);
			Thread.sleep(2000);
			driver.navigate().back();
			Thread.sleep(2000);
			steps.MoveElement(home2.Footer_Careers, driver, name);
			steps.clickJSButton(home2.Footer_Careers, driver, name);
			test.log(Status.INFO, MarkupHelper.createLabel("User clicked on Careers link under Footer Links", ExtentColor.BLUE));
			Thread.sleep(2000);
			CareersPage = getscreen.capture(driver, "CareersPage");
			test.addScreenCaptureFromPath(CareersPage);
			Thread.sleep(2000);
			driver.navigate().back();
			Thread.sleep(2000);
			steps.MoveElement(home2.Footer_PrivacyPolicy, driver, name);
			steps.clickJSButton(home2.Footer_PrivacyPolicy, driver, name);
			test.log(Status.INFO, MarkupHelper.createLabel("User clicked on Privacy Policy link under Footer Links", ExtentColor.BLUE));
			Thread.sleep(2000);
			PrivacyPolicyPage = getscreen.capture(driver, "PrivacyPolicyPage");
			test.addScreenCaptureFromPath(PrivacyPolicyPage);
			Thread.sleep(2000);
			driver.navigate().back();
			Thread.sleep(2000);
			steps.MoveElement(home2.Footer_TermsofUse, driver, name);
			steps.clickJSButton(home2.Footer_TermsofUse, driver, name);
			test.log(Status.INFO, MarkupHelper.createLabel("User clicked on Terms of Use link under Footer Links", ExtentColor.BLUE));
			Thread.sleep(2000);
			TermsofUsePage = getscreen.capture(driver, "TermsofUsePage");
			test.addScreenCaptureFromPath(TermsofUsePage);
			Thread.sleep(2000);
			driver.navigate().back();
			Thread.sleep(2000);
			FooterLinksstatus = passstatus;
			Assert.assertTrue(true);
			System.out.println("Footer Links Successful");
			testresultdata.put(Integer.toString(cnt), new Object[] {name, "Footer Links", "Footer Links Successful",passstatus});	
		}
		catch(Exception e)
		{
			System.out.println("Footer Links Unsuccessful");
			testresultdata.put(Integer.toString(cnt), new Object[] {name, "Footer Links", "Footer Links Unsuccessful",failstatus});
			FooterLinksstatus = failstatus;
			FooterLinksstatusfail = getscreen.capture(driver, "FooterLinksstatusfail");
			test.addScreenCaptureFromPath(FooterLinksstatusfail);
			Assert.assertTrue(false);
		}
	}
	@BeforeClass
	public void before()
	{
		workbook = new HSSFWorkbook();
		sheet = workbook.createSheet(strDataFileName);
		testresultdata = new LinkedHashMap<String, Object[]>();
		testresultdata.put("1", new Object[] { "Test Step Name", "Expected Result","Actual Result" ,"Status"});
	}
	@AfterClass
	public void after() throws Exception
	{
		Set<String> keyset = testresultdata.keySet();
		int rownum = 0;
		for (String key : keyset) {
			Row row = sheet.createRow(rownum++);
			Object [] objArr = testresultdata.get(key);
			int cellnum = 0;
			for (Object obj : objArr) {
				Cell cell = row.createCell(cellnum++);
				if(obj instanceof Date) 
					cell.setCellValue((Date)obj);
				else if(obj instanceof Boolean)
					cell.setCellValue((Boolean)obj);
				else if(obj instanceof String)
					cell.setCellValue((String)obj);
				else if(obj instanceof Double)
					cell.setCellValue((Double)obj);
			}
		}
		try {
			FileOutputStream out = new FileOutputStream(new File("TestResults/"+strDataFileName +".xls"));
			workbook.write(out);
			out.close();
			System.out.println("Excel written successfully..");
			TestResult();
			driver.quit();
		}
		catch (FileNotFoundException e) 
		{
			e.printStackTrace();
		} 
		catch (IOException e) 
		{
			e.printStackTrace();
		}
	}
	public void TestResult() throws Exception
	{
		HSSFRow row;
		HSSFCell cell;
		WritableFont cellFont;
		WritableCellFormat cellFormat;
		Border border = null;
		int count;
		FileInputStream inp = new FileInputStream( "TestResults/"+strDataFileName +".xls");
		HSSFWorkbook wb = new HSSFWorkbook(inp);
		HSSFSheet sh = wb.getSheetAt(0);
		count=sh.getLastRowNum();
		for (int i = 1; i <= count; i++) 
		{
			row=sh.getRow(i);
			cell=row.getCell(3);
			String status=row.getCell(3).getStringCellValue();
			if(status.equalsIgnoreCase("Pass"))
			{
				HSSFCellStyle pass= wb.createCellStyle();
				pass.setFillForegroundColor(IndexedColors.GREEN.getIndex());
				pass.setFillPattern(CellStyle.SOLID_FOREGROUND);
				cell.setCellStyle(pass);
			}
			else
			{
				HSSFCellStyle fail= wb.createCellStyle();
				fail.setFillForegroundColor(IndexedColors.RED.getIndex());
				fail.setFillPattern(CellStyle.SOLID_FOREGROUND);
				cell.setCellStyle(fail);
			}
		}
		FileOutputStream out = new FileOutputStream("TestResults/"+strDataFileName+".xls");
		wb.write(out);
		wb.close();
	}
}
