package Com.Homepage;
import java.io.File;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.IOException;
import java.util.LinkedHashMap;
import java.util.Map;
import java.util.Set;
import java.util.Date;
import java.util.concurrent.TimeUnit;
import org.apache.poi.hssf.usermodel.HSSFCell;
import org.apache.poi.hssf.usermodel.HSSFCellStyle;
import org.apache.poi.hssf.usermodel.HSSFRow;
import org.apache.poi.hssf.usermodel.HSSFSheet;
import org.apache.poi.hssf.usermodel.HSSFWorkbook;
import org.apache.poi.ss.usermodel.Row;
import org.apache.poi.ss.usermodel.Cell;
import org.apache.poi.ss.usermodel.CellStyle;
import org.apache.poi.ss.usermodel.IndexedColors;
import org.openqa.selenium.By;
import org.openqa.selenium.Keys;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.interactions.Actions;
import org.openqa.selenium.support.ui.ExpectedConditions;
import org.openqa.selenium.support.ui.WebDriverWait;
import org.testng.Assert;
import org.testng.annotations.AfterClass;
import org.testng.annotations.BeforeClass;
import org.testng.annotations.Test;
import com.aventstack.extentreports.Status;
import com.aventstack.extentreports.markuputils.ExtentColor;
import com.aventstack.extentreports.markuputils.MarkupHelper;
import Com.ObjectRepository.Login_OR;
import Com.Utilities.Browsers;
import Com.Utilities.CommonMethods;
import Com.Utilities.GetScreenShot;
import Com.Utilities.Screenshot;
import Com.Utilities.Steps;
import Com.Utilities.TestBase;
import jxl.format.Border;
import jxl.format.UnderlineStyle;
import jxl.write.Colour;
import jxl.write.WritableCellFormat;
import jxl.write.WritableFont;
import jxl.write.WriteException;
public class Login extends TestBase
{
	WebDriver driver;
	Login_OR login = new Login_OR();
	Steps steps = new Steps();
	GetScreenShot getscreen = new GetScreenShot();
	Screenshot screen = new Screenshot();
	public String strAbsolutepath = new File("").getAbsolutePath();
	HSSFWorkbook workbook;
	HSSFSheet sheet;
	String strDataFileName = this.getClass().getSimpleName();
	Map<String, Object[]> testresultdata; 
	CommonMethods commonmethods = new CommonMethods();
	Screenshot screenshot = new Screenshot();
	String passstatus = screenshot.status("PASS");
	String failstatus = screenshot.status("FAIL");
	int cnt = 2;
	public static String EmailError = "";
	public static String PasswordError = "";
	public static String URLNavigationstatus = "";
	public static String Loginstatus = "";
	public Login()
	{
		Browsers b = new Browsers();
		this.driver = b.getBrowsers("chrome1");		 
	}
	@Test(priority=0)
	public void URLNavigation() throws IOException, Exception
	{
		String name = new Object(){}.getClass().getEnclosingMethod().getName();
		cnt ++;
		try
		{
			test = extent.createTest("URL Navigation");
			steps.OpenUrl(driver, strDataFileName, "URL", name);
			System.out.println("URL navigated successfully");
			test.log(Status.INFO, MarkupHelper.createLabel("URL Navigated Successfully", ExtentColor.BLUE));
			testresultdata.put(Integer.toString(cnt), new Object[] {name, "URL Navigation", "URL Navigation Successful",passstatus});
			URLNavigationstatus = passstatus;
			String URLScreenPass = getscreen.capture(driver, "URLScreenPass");
			test.addScreenCaptureFromPath(URLScreenPass);
			Assert.assertTrue(true);
			System.out.println("URL Navigation Successful");
			test.log(Status.INFO, MarkupHelper.createLabel("URL Navigation Successful", ExtentColor.BLUE));
		}
		catch(Exception e)
		{
			System.out.println("URL Navigation Unsuccessful");
			testresultdata.put(Integer.toString(cnt), new Object[] {name, "URL Navigation", "URL Navigation Unsuccessful",failstatus});
			URLNavigationstatus = failstatus;
			String URLScreenFail = getscreen.capture(driver, "URLScreenFail");
			test.addScreenCaptureFromPath(URLScreenFail);
			Assert.assertTrue(false);
		}
	}
	@Test(priority=1)
	public void LoginTest() throws IOException, Exception
	{
		String name = new Object(){}.getClass().getEnclosingMethod().getName();
		cnt ++;
		try
		{
			test = extent.createTest("User Login");
			System.out.println("User Login Test Case Executing...");
			Thread.sleep(1000);
			commonmethods.URLNaviagtion(driver);
			Thread.sleep(1000);
			steps.clickJSButton(login.log, driver, name);
			System.out.println("User clicked on Login/MyAccount");
			/*Actions act = new Actions(driver);
			act.sendKeys(Keys.ENTER).build().perform();*/
			test.log(Status.INFO, MarkupHelper.createLabel("Clicked on Login", ExtentColor.BLUE));
			Thread.sleep(1000);
			steps.EnterText(login.Login_Email, driver, strDataFileName, "InvalidUsername", name);
			System.out.println("User entered invalid data in Email");
			String continuetext = driver.findElement(login.Login_Continue).getText();
			System.out.println(continuetext);
			steps.clickJSButton(login.Login_Continue, driver, name);
			System.out.println("User clicked on Continue");
			EmailError = driver.findElement(login.Email_error).getText();
			test.log(Status.INFO, MarkupHelper.createLabel("Invalid Email Entered   "+ EmailError, ExtentColor.RED));
			steps.Clear(login.Login_Email, driver, name);
			steps.EnterText(login.Login_Email, driver, strDataFileName, "Username", name);
			System.out.println("User entered valid data in Email");
			test.log(Status.INFO, MarkupHelper.createLabel("Email Entered successfully", ExtentColor.BLUE));
			String continuetext1 = driver.findElement(login.Login_Continue).getText();
			System.out.println(continuetext1);
			steps.clickJSButton(login.Login_Continue, driver, name);
			System.out.println("User clicked on Continue");
			test.log(Status.INFO, MarkupHelper.createLabel("Clicked on Continue", ExtentColor.BLUE));
			Thread.sleep(1000);
			steps.EnterText(login.Login_password, driver, strDataFileName, "InvalidPassword", name);
			System.out.println("User entered invalid data in Password");
			String logintext = driver.findElement(login.Login_loginbutton).getText();
			System.out.println(logintext);
			steps.clickJSButton(login.Login_loginbutton, driver, name);
			System.out.println("User clicked on Login");
			Thread.sleep(1000);
			PasswordError = driver.findElement(login.Password_error).getText();
			System.out.println(PasswordError);
			test.log(Status.INFO, MarkupHelper.createLabel("Inavalid Password Entered   "+ PasswordError, ExtentColor.RED));
			steps.Clear(login.Login_password, driver, name);
			steps.EnterText(login.Login_password, driver, strDataFileName, "Password", name);
			System.out.println("User entered valid data in Password");
			test.log(Status.INFO, MarkupHelper.createLabel("Password Entered successfully", ExtentColor.BLUE));
			String logintext1 = driver.findElement(login.Login_loginbutton).getText();
			System.out.println(logintext1);
			steps.clickJSButton(login.Login_loginbutton, driver, name);
			System.out.println("User clicked on Login button");
			test.log(Status.INFO, MarkupHelper.createLabel("Clicked on Login button", ExtentColor.BLUE));
			Thread.sleep(1000);
			commonmethods.Logout(driver);
			Loginstatus = passstatus;
			String LoginScreenPass = getscreen.capture(driver, "LoginScreenPass");
			test.addScreenCaptureFromPath(LoginScreenPass);
			Assert.assertTrue(true);
			System.out.println("User Login Successful");
			test.log(Status.INFO, MarkupHelper.createLabel("User Logged In Successfully", ExtentColor.BLUE));
			testresultdata.put(Integer.toString(cnt), new Object[] {name, "User Login", "User Login Successful",passstatus});
		}
		catch(Exception e)
		{
			System.out.println("User Login Unsuccessful");
			testresultdata.put(Integer.toString(cnt), new Object[] {name, "User Login", "User Login Unsuccessful",failstatus});
			Loginstatus = failstatus;
			String LoginScreenFail = getscreen.capture(driver, "LoginScreenFail");
			test.addScreenCaptureFromPath(LoginScreenFail);
			Assert.assertTrue(false);
		}
	}
	@BeforeClass
	public void before()
	{
		workbook = new HSSFWorkbook();
		sheet = workbook.createSheet(strDataFileName);
		testresultdata = new LinkedHashMap<String, Object[]>();
		testresultdata.put("1", new Object[] { "Test Step Name", "Expected Result","Actual Result" ,"Status"});
	}
	@AfterClass
	public void after() throws Exception
	{
		Set<String> keyset = testresultdata.keySet();
		int rownum = 0;
		for (String key : keyset) {
			Row row = sheet.createRow(rownum++);
			Object [] objArr = testresultdata.get(key);
			int cellnum = 0;
			for (Object obj : objArr) {
				Cell cell = row.createCell(cellnum++);
				if(obj instanceof Date) 
					cell.setCellValue((Date)obj);
				else if(obj instanceof Boolean)
					cell.setCellValue((Boolean)obj);
				else if(obj instanceof String)
					cell.setCellValue((String)obj);
				else if(obj instanceof Double)
					cell.setCellValue((Double)obj);
			}
		}
		try {
			FileOutputStream out = new FileOutputStream(new File("TestResults/"+strDataFileName +".xls"));
			workbook.write(out);
			out.close();
			System.out.println("Excel written successfully..");
			TestResult();
			driver.quit();
		}
		catch (FileNotFoundException e) 
		{
			e.printStackTrace();
		} 
		catch (IOException e) 
		{
			e.printStackTrace();
		}
	}
	public void TestResult() throws Exception
	{
		HSSFRow row;
		HSSFCell cell;
		WritableFont cellFont;
		WritableCellFormat cellFormat;
		Border border = null;
		int count;
		FileInputStream inp = new FileInputStream( "TestResults/"+strDataFileName +".xls");
		HSSFWorkbook wb = new HSSFWorkbook(inp);
		HSSFSheet sh = wb.getSheetAt(0);
		count=sh.getLastRowNum();
		for (int i = 1; i <= count; i++) 
		{
			row=sh.getRow(i);
			cell=row.getCell(3);
			String status=row.getCell(3).getStringCellValue();
			if(status.equalsIgnoreCase("Pass"))
			{
				HSSFCellStyle pass= wb.createCellStyle();
				pass.setFillForegroundColor(IndexedColors.GREEN.getIndex());
				pass.setFillPattern(CellStyle.SOLID_FOREGROUND);
				cell.setCellStyle(pass);
			}
			else
			{
				HSSFCellStyle fail= wb.createCellStyle();
				fail.setFillForegroundColor(IndexedColors.RED.getIndex());
				fail.setFillPattern(CellStyle.SOLID_FOREGROUND);
				cell.setCellStyle(fail);
			}
		}
		FileOutputStream out = new FileOutputStream("TestResults/"+strDataFileName+".xls");
		wb.write(out);
		wb.close();
	}
}
