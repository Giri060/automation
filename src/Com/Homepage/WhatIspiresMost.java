package Com.Homepage;
import java.io.File;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.IOException;
import java.util.Date;
import java.util.LinkedHashMap;
import java.util.Map;
import java.util.Set;
import org.apache.poi.hssf.usermodel.HSSFCell;
import org.apache.poi.hssf.usermodel.HSSFCellStyle;
import org.apache.poi.hssf.usermodel.HSSFRow;
import org.apache.poi.hssf.usermodel.HSSFSheet;
import org.apache.poi.hssf.usermodel.HSSFWorkbook;
import org.apache.poi.ss.usermodel.Cell;
import org.apache.poi.ss.usermodel.CellStyle;
import org.apache.poi.ss.usermodel.IndexedColors;
import org.apache.poi.ss.usermodel.Row;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.support.Color;
import org.testng.Assert;
import org.testng.annotations.AfterClass;
import org.testng.annotations.BeforeClass;
import org.testng.annotations.Test;
import com.aventstack.extentreports.Status;
import com.aventstack.extentreports.markuputils.ExtentColor;
import com.aventstack.extentreports.markuputils.MarkupHelper;
import Com.ObjectRepository.Homepage2_OR;
import Com.ObjectRepository.Homepage_OR;
import Com.ObjectRepository.Login_OR;
import Com.Utilities.Browsers;
import Com.Utilities.CommonMethods;
import Com.Utilities.GetScreenShot;
import Com.Utilities.Screenshot;
import Com.Utilities.Steps;
import Com.Utilities.TestBase;
import jxl.format.Border;
import jxl.write.WritableCellFormat;
import jxl.write.WritableFont;
public class WhatIspiresMost extends TestBase
{
	WebDriver driver;
	Login_OR login = new Login_OR();
	Steps steps = new Steps();
	GetScreenShot getscreen = new GetScreenShot();
	Screenshot screen = new Screenshot();
	public String strAbsolutepath = new File("").getAbsolutePath();
	HSSFWorkbook workbook;
	HSSFSheet sheet;
	String strDataFileName = this.getClass().getSimpleName();
	Map<String, Object[]> testresultdata; 
	CommonMethods commonmethods = new CommonMethods();
	Homepage_OR home = new Homepage_OR();
	Homepage2_OR home2 = new Homepage2_OR();
	Screenshot screenshot = new Screenshot();
	String passstatus = screenshot.status("PASS");
	String failstatus = screenshot.status("FAIL");
	int cnt = 2;
	public static String Photographystatus = "";
	public static String NoCampaignMsg1 = "";
	public static String NoCampaignMsg2 = "";
	public static String NoCampaignMsg3 = "";
	public static String Description1 = "";
	public static String MRP1 = "";
	public static String PhotoColor1 = "";
	public static String Photohex1 = "";
	public static String PhotoFirstcolor = "";
	public static String PhotoColor2 = "";
	public static String Photohex2 = "";
	public static String PhotoSecondcolor = "";
	public static String PhotoColor3 = "";
	public static String Photohex3 = "";
	public static String PhotoThirdcolor = "";
	public static String PhotoColor4 = "";
	public static String Photohex4 = "";
	public static String PhotoFourthcolor = "";
	public static String Photocqty = "";
	public static String Photoiqty = "";
	public static String Photodqty = "";
	public static String PhotoSizemsg = "";
	public static String PhotosizeS1 = "";
	public static String PhotosizeM1 = "";
	public static String PhotosizeL1 = "";
	public static String PhotosizeXL1 = "";
	public static String PhotosizeXXL1 = "";
	public static String PhotosizeXXXL1 = "";
	public static String Photofindursize1 = "";
	public static String Photoupdateproduct1 = "";
	public static String Photoremovecart1 = "";
	public static String Photoaddcart1 = "";
	public static String PhotographyProducts = "";
	public static String MothersDayProducts = "";
	public static String BollywoodProducts = "";
	public static String SportsProducts = "";
	public static String Photographyimage1 = "";
	public static String PhotographyProduct2 = "";
	public static String PhotographyProduct3 = "";
	public static String PhotographyProduct1 = "";
	public static String below510 = "";
	public static String above510 = "";
	public static String tightfit = "";
	public static String comfit = "";
	public static String Travellingstatus = "";
	public static String TravelNoCampaignMsg1 = "";
	public static String TravelNoCampaignMsg2 = "";
	public static String TravelNoCampaignMsg3 = "";
	public static String TravellingDescription = "";
	public static String TravellingMRP = "";
	public static String TravellingColor1 = "";
	public static String Travellinghex1 = "";
	public static String TravellingFirstcolor = "";
	public static String TravellingColor2 = "";
	public static String Travellinghex2 = "";
	public static String TravellingSecondcolor = "";
	public static String TravellingColor3 = "";
	public static String Travellinghex3 = "";
	public static String TravellingThirdcolor = "";
	public static String TravellingColor4 = "";
	public static String Travellinghex4 = "";
	public static String TravellingFourthcolor = "";
	public static String Travellingcqty = "";
	public static String Travellingiqty = "";
	public static String Travellingdqty = "";
	public static String TravellingSizemsg = "";
	public static String TravellingsizeS1 = "";
	public static String TravellingsizeM1 = "";
	public static String TravellingsizeL1 = "";
	public static String TravellingsizeXL1 = "";
	public static String TravellingsizeXXL1 = "";
	public static String TravellingsizeXXXL1 = "";
	public static String Travellingfindursize1 = "";
	public static String Travellingupdateproduct1 = "";
	public static String Travellingremovecart1 = "";
	public static String Travellingaddcart1 = "";
	public static String TravellinggraphyProducts = "";
	public static String TravellingMothersDayProducts = "";
	public static String TravellingBollywoodProducts = "";
	public static String TravellingSportsProducts = "";
	public static String Travellinggraphyimage1 = "";
	public static String TravellinggraphyProduct2 = "";
	public static String TravellinggraphyProduct3 = "";
	public static String TravellinggraphyProduct1 = "";
	public static String Travellingbelow510 = "";
	public static String Travellingabove510 = "";
	public static String Travellingtightfit = "";
	public static String Travellingcomfit = "";
	public static String TravellingProducts = "";
	public static String MothersDayProducts1 = "";
	public static String BollywoodProducts1 = "";
	public static String TravellingSportsProducts1 = "";
	public static String Travellingimage1 = "";
	public static String TravellingProduct2 = "";
	public static String TravellingProduct3 = "";
	public static String TravellingProduct1 = "";
	public static String TravellingPhotoColor1 = "";
	public static String TravellingPhotohex1 = "";
	public static String MusicLoverstatus = "";
	public static String MusicLoverNoCampaignMsg1 = "";
	public static String MusicLoverNoCampaignMsg2 = "";
	public static String MusicLoverNoCampaignMsg3 = "";
	public static String MusicLoverDescription = "";
	public static String MusicLoverMRP = "";
	public static String MusicLoverColor1 = "";
	public static String MusicLoverhex1 = "";
	public static String MusicLoverFirstcolor = "";
	public static String MusicLoverColor2 = "";
	public static String MusicLoverhex2 = "";
	public static String MusicLoverSecondcolor = "";
	public static String MusicLoverColor3 = "";
	public static String MusicLoverhex3 = "";
	public static String MusicLoverThirdcolor = "";
	public static String MusicLoverColor4 = "";
	public static String MusicLoverhex4 = "";
	public static String MusicLoverFourthcolor = "";
	public static String MusicLovercqty = "";
	public static String MusicLoveriqty = "";
	public static String MusicLoverdqty = "";
	public static String MusicLoverSizemsg = "";
	public static String MusicLoversizeS1 = "";
	public static String MusicLoversizeM1 = "";
	public static String MusicLoversizeL1 = "";
	public static String MusicLoversizeXL1 = "";
	public static String MusicLoversizeXXL1 = "";
	public static String MusicLoversizeXXXL1 = "";
	public static String MusicLoversizeS2 = "";
	public static String MusicLoversizeM2 = "";
	public static String MusicLoversizeL2 = "";
	public static String MusicLoversizeXL2 = "";
	public static String MusicLoversizeXXL2 = "";
	public static String MusicLoversizeXXXL2 = "";
	public static String MusicLoverfindursize1 = "";
	public static String MusicLoverupdateproduct1 = "";
	public static String MusicLoverremovecart1 = "";
	public static String MusicLoveraddcart1 = "";
	public static String MusicLovergraphyProducts = "";
	public static String MusicLoverMothersDayProducts = "";
	public static String MusicLoverBollywoodProducts = "";
	public static String MusicLoverSportsProducts = "";
	public static String MusicLovergraphyimage1 = "";
	public static String MusicLovergraphyProduct2 = "";
	public static String MusicLovergraphyProduct3 = "";
	public static String MusicLovergraphyProduct1 = "";
	public static String MusicLoverbelow510 = "";
	public static String MusicLoverabove510 = "";
	public static String MusicLovertightfit = "";
	public static String MusicLovercomfit = "";
	public static String MusicLoverProducts = "";
	public static String MusicLoverMothersDayProducts1 = "";
	public static String MusicLoverBollywoodProducts1 = "";
	public static String MusicLoverSportsProducts1 = "";
	public static String MusicLoverimage1 = "";
	public static String MusicLoverProduct2 = "";
	public static String MusicLoverProduct3 = "";
	public static String MusicLoverProduct1 = "";
	public static String MusicLoverPhotoColor1 = "";
	public static String MusicLoverPhotohex1 = "";
	public static String Fitnessproductdetails = "";
	public static String FitnessDescription = "";
	public static String FitnessMRP = "";
	public static String FitnessColor1 = "";
	public static String Fitnesshex1 = "";
	public static String FitnessFirstcolor = "";
	public static String FitnessColor2 = "";
	public static String Fitnesshex2 = "";
	public static String FitnessSecondcolor = "";
	public static String FitnessColor3 = "";
	public static String Fitnesshex3 = "";
	public static String FitnessThirdcolor = "";
	public static String Fitnesscolor5 = "";
	public static String Fitnesshex5 = "";
	public static String FitnessFifthcolor = "";
	public static String Fitnesscqty = "";
	public static String Fitnessiqty = "";
	public static String Fitnessdqty = "";
	public static String FitnessSizemsg = "";
	public static String FitnesssizeS = "";
	public static String FitnesssizeM = "";
	public static String FitnesssizeL = "";
	public static String FitnesssizeXL = "";
	public static String FitnesssizeXXL = "";
	public static String FitnesssizeXXXL = "";
	public static String Fitnessfindursize = "";
	public static String Fitnessaddcart = "";
	public static String Fitnessupdateproduct = "";
	public static String Fitnessremovecart = "";
	public static String Fitnessstatus = "";
	public static String Fitnessstatusfail = "";
	public static String FitnessFreakPage = "";
	public static String Fitnessbelow510 = "";
	public static String Fitnessabove510 = "";
	public static String Fitnesstightfit = "";
	public static String Fitnesscomfit = "";
	public static String Foodieproductdetails = "";
	public static String FoodieDescription = "";
	public static String FoodieDescription1 = "";
	public static String FoodieMRP = "";
	public static String FoodieColor1 = "";
	public static String Foodiehex1 = "";
	public static String FoodieFirstcolor = "";
	public static String FoodieColor2 = "";
	public static String Foodiehex2 = "";
	public static String FoodieSecondcolor = "";
	public static String FoodieColor3 = "";
	public static String Foodiehex3 = "";
	public static String FoodieThirdcolor = "";
	public static String Foodiecolor5 = "";
	public static String Foodiehex5 = "";
	public static String FoodieFifthcolor = "";
	public static String Foodiecqty = "";
	public static String Foodieiqty = "";
	public static String Foodiedqty = "";
	public static String FoodieSizemsg = "";
	public static String FoodiesizeS = "";
	public static String FoodiesizeM = "";
	public static String FoodiesizeL = "";
	public static String FoodiesizeXL = "";
	public static String FoodiesizeXXL = "";
	public static String FoodiesizeXXXL = "";
	public static String Foodiefindursize = "";
	public static String Foodieaddcart = "";
	public static String Foodieupdateproduct = "";
	public static String Foodieremovecart = "";
	public static String Foodiestatus = "";
	public static String Foodiestatusfail = "";
	public static String FoodieFreakPage = "";
	public static String Foodiebelow510 = "";
	public static String Foodieabove510 = "";
	public static String Foodietightfit = "";
	public static String Foodiecomfit = "";
	public static String FoodieNoCampaignMsg1 = "";
	public static String FoodieMothersDayProducts1 = "";
	public static String FoodieNoCampaignMsg2 = "";
	public static String FoodieBollywoodProducts1 = "";
	public static String FoodieNoCampaignMsg3 = "";
	public static String FoodieSportsProducts1 = "";
	public static String Bikerproductdetails = "";
	public static String BikerDescription = "";
	public static String BikerMRP = "";
	public static String Bikercqty = "";
	public static String Bikeriqty = "";
	public static String Bikerdqty = "";
	public static String BikerSizemsg = "";
	public static String BikersizeS = "";
	public static String BikersizeM = "";
	public static String BikersizeL = "";
	public static String BikersizeXL = "";
	public static String BikersizeXXL = "";
	public static String BikersizeXXXL = "";
	public static String Bikerfindursize = "";
	public static String Bikeraddcart = "";
	public static String Bikerupdateproduct = "";
	public static String Bikerremovecart = "";
	public static String Bikerstatus = "";
	public static String Bikerstatusfail = "";
	public static String BikerFreakPage = "";
	public static String Bikerbelow510 = "";
	public static String Bikerabove510 = "";
	public static String Bikertightfit = "";
	public static String Bikercomfit = "";
	public static String BikerNoCampaignMsg1 = "";
	public static String BikerMothersDayProducts1 = "";
	public static String BikerNoCampaignMsg2 = "";
	public static String BikerBollywoodProducts1 = "";
	public static String BikerNoCampaignMsg3 = "";
	public static String BikerSportsProducts1 = "";
	public static String BikerPage = "";
	public static String Bikerproduct2 = "";
	public static String Bikerproduct1 = "";
	public static String StartUpproductdetails = "";
	public static String StartUpDescription = "";
	public static String StartUpMRP = "";
	public static String StartUpcqty = "";
	public static String StartUpiqty = "";
	public static String StartUpdqty = "";
	public static String StartUpSizemsg = "";
	public static String StartUpsizeS = "";
	public static String StartUpsizeM = "";
	public static String StartUpsizeL = "";
	public static String StartUpsizeXL = "";
	public static String StartUpsizeXXL = "";
	public static String StartUpsizeXXXL = "";
	public static String StartUpfindursize = "";
	public static String StartUpaddcart = "";
	public static String StartUpupdateproduct = "";
	public static String StartUpremovecart = "";
	public static String StartUpstatus = "";
	public static String StartUpstatusfail = "";
	public static String StartUpFreakPage = "";
	public static String StartUpbelow510 = "";
	public static String StartUpabove510 = "";
	public static String StartUptightfit = "";
	public static String StartUpcomfit = "";
	public static String StartUpNoCampaignMsg1 = "";
	public static String StartUpProducts1 = "";
	public static String StartUpNoCampaignMsg2 = "";
	public static String StartUpBollywoodProducts1 = "";
	public static String StartUpNoCampaignMsg3 = "";
	public static String StartUpSportsProducts1 = "";
	public static String StartUpPage = "";
	public static String StartUpProduct2 = ""; 
	public static String StartUpProduct1 = "";
	public static String StartUpColor1 = "";
	public static String StartUphex1 = "";
	public static String StartUpFirstcolor = "";
	public static String StartUpColor2 = "";
	public static String StartUphex2 = "";
	public static String StartUpSecondcolor = "";
	public static String StartUpColor3 = "";
	public static String StartUphex3 = "";
	public static String StartUpThirdcolor = "";
	public static String StartUpColor4 = "";
	public static String StartUphex4 = "";
	public static String StartUpFourthcolor = "";
	public static String Professionproductdetails = "";
	public static String ProfessionDescription = "";
	public static String ProfessionMRP = "";
	public static String Professioncqty = "";
	public static String Professioniqty = "";
	public static String Professiondqty = "";
	public static String ProfessionSizemsg = "";
	public static String ProfessionsizeS = "";
	public static String ProfessionsizeM = "";
	public static String ProfessionsizeL = "";
	public static String ProfessionsizeXL = "";
	public static String ProfessionsizeXXL = "";
	public static String ProfessionsizeXXXL = "";
	public static String Professionfindursize = "";
	public static String Professionaddcart = "";
	public static String Professionupdateproduct = "";
	public static String Professionremovecart = "";
	public static String Professionstatus = "";
	public static String Professionstatusfail = "";
	public static String ProfessionFreakPage = "";
	public static String Professionbelow510 = "";
	public static String Professionabove510 = "";
	public static String Professiontightfit = "";
	public static String Professioncomfit = "";
	public static String ProfessionNoCampaignMsg1 = "";
	public static String ProfessionProducts1 = "";
	public static String ProfessionNoCampaignMsg2 = "";
	public static String ProfessionBollywoodProducts1 = "";
	public static String ProfessionNoCampaignMsg3 = "";
	public static String ProfessionSportsProducts1 = "";
	public static String ProfessionPage = "";
	public static String ProfessionProduct2 = ""; 
	public static String ProfessionProduct1 = "";
	public static String ProfessionColor1 = "";
	public static String Professionhex1 = "";
	public static String ProfessionFirstcolor = "";
	public static String ProfessionColor2 = "";
	public static String Professionhex2 = "";
	public static String ProfessionSecondcolor = "";
	public static String ProfessionColor3 = "";
	public static String Professionhex3 = "";
	public static String ProfessionThirdcolor = "";
	public static String ProfessionColor4 = "";
	public static String Professionhex4 = "";
	public static String ProfessionFourthcolor = "";
	public static String Professionupdateproduct1 = "";
	public static String Giftingproductdetails = "";
	public static String GiftingDescription = "";
	public static String GiftingMRP = "";
	public static String Giftingcqty = "";
	public static String Giftingiqty = "";
	public static String Giftingdqty = "";
	public static String GiftingSizemsg = "";
	public static String GiftingsizeS = "";
	public static String GiftingsizeM = "";
	public static String GiftingsizeL = "";
	public static String GiftingsizeXL = "";
	public static String GiftingsizeXXL = "";
	public static String GiftingsizeXXXL = "";
	public static String Giftingfindursize = "";
	public static String Giftingaddcart = "";
	public static String Giftingupdateproduct = "";
	public static String Giftingremovecart = "";
	public static String Giftingstatus = "";
	public static String Giftingstatusfail = "";
	public static String GiftingFreakPage = "";
	public static String Giftingbelow510 = "";
	public static String Giftingabove510 = "";
	public static String Giftingtightfit = "";
	public static String Giftingcomfit = "";
	public static String GiftingNoCampaignMsg1 = "";
	public static String GiftingProducts1 = "";
	public static String GiftingNoCampaignMsg2 = "";
	public static String GiftingBollywoodProducts1 = "";
	public static String GiftingNoCampaignMsg3 = "";
	public static String GiftingSportsProducts1 = "";
	public static String GiftingPage = "";
	public static String GiftingPage2 = "";
	public static String GiftingProduct2 = ""; 
	public static String GiftingProduct1 = "";
	public static String GiftingColor1 = "";
	public static String Giftinghex1 = "";
	public static String GiftingFirstcolor = "";
	public static String GiftingColor2 = "";
	public static String Giftinghex2 = "";
	public static String GiftingSecondcolor = "";
	public static String GiftingColor3 = "";
	public static String Giftinghex3 = "";
	public static String GiftingThirdcolor = "";
	public static String GiftingColor4 = "";
	public static String Giftinghex4 = "";
	public static String GiftingFourthcolor = "";
	public static String Giftingupdateproduct1 = "";
	public static String Sportsproductdetails = "";
	public static String SportsDescription = "";
	public static String SportsMRP = "";
	public static String Sportscqty = "";
	public static String Sportsiqty = "";
	public static String Sportsdqty = "";
	public static String SportsSizemsg = "";
	public static String SportssizeS = "";
	public static String SportssizeM = "";
	public static String SportssizeL = "";
	public static String SportssizeXL = "";
	public static String SportssizeXXL = "";
	public static String SportssizeXXXL = "";
	public static String Sportsfindursize = "";
	public static String Sportsaddcart = "";
	public static String Sportsupdateproduct = "";
	public static String Sportsremovecart = "";
	public static String Sportsstatus = "";
	public static String Sportsstatusfail = "";
	public static String SportsFreakPage = "";
	public static String Sportsbelow510 = "";
	public static String Sportsabove510 = "";
	public static String Sportstightfit = "";
	public static String Sportscomfit = "";
	public static String SportsNoCampaignMsg1 = "";
	public static String SportsProducts1 = "";
	public static String SportsNoCampaignMsg2 = "";
	public static String SportsBollywoodProducts1 = "";
	public static String SportsNoCampaignMsg3 = "";
	public static String SportsSportsProducts1 = "";
	public static String SportsPage = "";
	public static String SportsPage2 = "";
	public static String SportsProduct2 = ""; 
	public static String SportsProduct1 = "";
	public static String Sportsupdateproduct1 = "";
	public static String SportsWColor1 = "";
	public static String SportsWhex1 = "";
	public static String SportsWFirstcolor = "";
	public static String SportsWColor2 = "";
	public static String SportsWhex2 = "";
	public static String SportsWSecondcolor = "";
	public static String SportsWColor3 = "";
	public static String SportsWhex3 = "";
	public static String SportsWThirdcolor = "";
	public static String SportsWColor4 = "";
	public static String SportsWhex4 = "";
	public static String SportsWFourthcolor = "";
	public static String SportsMColor1 = "";
	public static String SportsMhex1 = "";
	public static String SportsMFirstcolor = "";
	public static String SportsMColor2 = "";
	public static String SportsMhex2 = "";
	public static String SportsMSecondcolor = "";
	public static String SportsMColor3 = "";
	public static String SportsMhex3 = "";
	public static String SportsMThirdcolor = "";
	public static String SportsMColor4 = "";
	public static String SportsMhex4 = "";
	public static String SportsMFourthcolor = "";
	public static String Humourproductdetails = "";
	public static String HumourDescription = "";
	public static String HumourMRP = "";
	public static String Humourcqty = "";
	public static String Humouriqty = "";
	public static String Humourdqty = "";
	public static String HumourSizemsg = "";
	public static String HumoursizeS = "";
	public static String HumoursizeM = "";
	public static String HumoursizeL = "";
	public static String HumoursizeXL = "";
	public static String HumoursizeXXL = "";
	public static String HumoursizeXXXL = "";
	public static String Humourfindursize = "";
	public static String Humouraddcart = "";
	public static String Humourupdateproduct = "";
	public static String Humourremovecart = "";
	public static String Humourstatus = "";
	public static String Humourstatusfail = "";
	public static String HumourFreakPage = "";
	public static String Humourbelow510 = "";
	public static String Humourabove510 = "";
	public static String Humourtightfit = "";
	public static String Humourcomfit = "";
	public static String HumourNoCampaignMsg1 = "";
	public static String HumourProducts1 = "";
	public static String HumourNoCampaignMsg2 = "";
	public static String HumourBollywoodProducts1 = "";
	public static String HumourNoCampaignMsg3 = "";
	public static String HumourHumourProducts1 = "";
	public static String HumourPage = "";
	public static String HumourPage2 = "";
	public static String HumourPage1 = "";
	public static String HumourProduct2 = ""; 
	public static String HumourProduct1 = "";
	public static String Humourupdateproduct1 = "";
	public static String HumourWColor1 = "";
	public static String HumourWhex1 = "";
	public static String HumourWFirstcolor = "";
	public static String HumourWColor2 = "";
	public static String HumourWhex2 = "";
	public static String HumourWSecondcolor = "";
	public static String HumourWColor3 = "";
	public static String HumourWhex3 = "";
	public static String HumourWThirdcolor = "";
	public static String HumourWColor4 = "";
	public static String HumourWhex4 = "";
	public static String HumourWFourthcolor = "";
	public static String HumourMColor1 = "";
	public static String HumourMhex1 = "";
	public static String HumourMFirstcolor = "";
	public static String HumourMColor2 = "";
	public static String HumourMhex2 = "";
	public static String HumourMSecondcolor = "";
	public static String HumourMColor3 = "";
	public static String HumourMhex3 = "";
	public static String HumourMThirdcolor = "";
	public static String HumourMColor4 = "";
	public static String HumourMhex4 = "";
	public static String HumourMFourthcolor = "";
	public static String Developerproductdetails = "";
	public static String DeveloperDescription1 = "";
	public static String DeveloperDescription2 = "";
	public static String DeveloperDescription3 = "";
	public static String DeveloperDescription4 = "";
	public static String DeveloperMRP = "";
	public static String Developercqty = "";
	public static String Developeriqty = "";
	public static String Developerdqty = "";
	public static String DeveloperSizemsg = "";
	public static String DevelopersizeS = "";
	public static String DevelopersizeM = "";
	public static String DevelopersizeL = "";
	public static String DevelopersizeXL = "";
	public static String DevelopersizeXXL = "";
	public static String DevelopersizeXXXL = "";
	public static String Developerfindursize = "";
	public static String Developeraddcart = "";
	public static String Developerupdateproduct = "";
	public static String Developerremovecart = "";
	public static String Developerstatus = "";
	public static String Developerstatusfail = "";
	public static String DeveloperFreakPage = "";
	public static String Developerbelow510 = "";
	public static String Developerabove510 = "";
	public static String Developertightfit = "";
	public static String Developercomfit = "";
	public static String DeveloperNoCampaignMsg1 = "";
	public static String DeveloperProducts1 = "";
	public static String DeveloperNoCampaignMsg2 = "";
	public static String DeveloperBollywoodProducts1 = "";
	public static String DeveloperNoCampaignMsg3 = "";
	public static String DeveloperDeveloperProducts1 = "";
	public static String DeveloperPage = "";
	public static String DeveloperPage2 = "";
	public static String DeveloperPage1 = "";
	public static String DeveloperProduct2 = ""; 
	public static String DeveloperProduct1 = "";
	public static String Developerupdateproduct1 = "";
	public static String DeveloperWColor1 = "";
	public static String DeveloperWhex1 = "";
	public static String DeveloperWFirstcolor = "";
	public static String DeveloperWColor2 = "";
	public static String DeveloperWhex2 = "";
	public static String DeveloperWSecondcolor = "";
	public static String DeveloperWColor3 = "";
	public static String DeveloperWhex3 = "";
	public static String DeveloperWThirdcolor = "";
	public static String DeveloperWColor4 = "";
	public static String DeveloperWhex4 = "";
	public static String DeveloperWFourthcolor = "";
	public static String DeveloperMColor1 = "";
	public static String DeveloperMhex1 = "";
	public static String DeveloperMFirstcolor = "";
	public static String DeveloperMColor2 = "";
	public static String DeveloperMhex2 = "";
	public static String DeveloperMSecondcolor = "";
	public static String DeveloperMColor3 = "";
	public static String DeveloperMhex3 = "";
	public static String DeveloperMThirdcolor = "";
	public static String DeveloperMColor4 = "";
	public static String DeveloperMhex4 = "";
	public static String DeveloperMFourthcolor = "";
	public static String DevEdit = "";
	public WhatIspiresMost()
	{
		Browsers b = new Browsers();
		this.driver = b.getBrowsers("chrome1");		 
	}
	@Test(priority=0)
	public void Photography() throws Exception
	{
		String name = new Object(){}.getClass().getEnclosingMethod().getName();
		cnt ++;
		try
		{
			test = extent.createTest("Photography under What Inspires You Most");
			System.out.println("Photography under What Inspires You Most Test Case Executing...");
			commonmethods.Login(driver);
			test.log(Status.INFO, MarkupHelper.createLabel("User able to Login", ExtentColor.BLUE));
			Thread.sleep(1000);
			commonmethods.URLNaviagtion(driver);
			Thread.sleep(1000);
			steps.clickButton(home.Mydreamstore, driver, name);
			String Parent = driver.getWindowHandle();
			test.log(Status.INFO, MarkupHelper.createLabel("User naviagated to Mydreamstore Homepage Successfully", ExtentColor.BLUE));
			Thread.sleep(2000);
			steps.MoveElement(home2.WIM_Image1, driver, name);
			steps.clickButton(home2.WIM_Image1, driver, name);
			test.log(Status.INFO, MarkupHelper.createLabel("User clicked on Photography Category in Who Inspires You Most", ExtentColor.BLUE));
			Thread.sleep(2000);
			steps.ChildWindow(driver);
			driver.manage().window().maximize();
			test.log(Status.INFO, MarkupHelper.createLabel("User navigated to Next Window", ExtentColor.BLUE));
			PhotographyProducts = getscreen.capture(driver, "PhotographyProducts");
			test.addScreenCaptureFromPath(PhotographyProducts);
			Thread.sleep(1000);
			try
			{
				String Filterpage1 = driver.findElement(home.Filterpagetext).getText();
				System.out.println("User navigated to :   "+Filterpage1);
				steps.clickButton(home2.Photography_Motherday, driver, name);
				test.log(Status.INFO, MarkupHelper.createLabel("User selected on Mothers Day Category", ExtentColor.BLUE));
				try
				{
					NoCampaignMsg1 = driver.findElement(home2.Photography_NoCampaignFound).getText();
					System.out.println("Campaigns Not Found under Mother's day Category");
					test.log(Status.INFO, MarkupHelper.createLabel("User hasn't found Active Campaigns under Mothers Day Category", ExtentColor.BLUE));
					MothersDayProducts = getscreen.capture(driver, "MothersDayProducts");
					test.addScreenCaptureFromPath(MothersDayProducts);
					steps.clickButton(home2.Photography_Motherday, driver, name);
				}
				catch(Exception e)
				{
					System.out.println("Campaigns Found");
					test.log(Status.INFO, MarkupHelper.createLabel("User found Active Campaigns under Mothers Day Category", ExtentColor.BLUE));
					steps.clickButton(home2.Photography_Motherday, driver, name);
				}
				steps.clickButton(home2.Photography_Bolly, driver, name);
				try
				{
					NoCampaignMsg2 = driver.findElement(home2.Photography_NoCampaignFound).getText();
					System.out.println("Campaigns Not Found under Bollywood/Movie Personalities Category");
					test.log(Status.INFO, MarkupHelper.createLabel("User hasn't found Active Campaigns under Bollywood/Movie Personalities Category", ExtentColor.BLUE));
					BollywoodProducts = getscreen.capture(driver, "BollywoodProducts");
					test.addScreenCaptureFromPath(BollywoodProducts);
					steps.clickButton(home2.Photography_Bolly, driver, name);
				}
				catch(Exception e)
				{
					System.out.println("Campaigns Found");
					test.log(Status.INFO, MarkupHelper.createLabel("User found Active Campaigns under Bollywood/Movie Personalities Category", ExtentColor.BLUE));
					steps.clickButton(home2.Photography_Bolly, driver, name);
				}
				steps.clickButton(home2.Photography_Sports, driver, name);
				try
				{
					NoCampaignMsg3 = driver.findElement(home2.Photography_NoCampaignFound).getText();
					System.out.println("Campaigns Not Found under Sports Category");
					test.log(Status.INFO, MarkupHelper.createLabel("User hasn't found Active Campaigns under Sports Category", ExtentColor.BLUE));
					SportsProducts = getscreen.capture(driver, "SportsProducts");
					test.addScreenCaptureFromPath(SportsProducts);
					steps.clickButton(home2.Photography_Sports, driver, name);
				}
				catch(Exception e)
				{
					System.out.println("Campaigns Found");
					test.log(Status.INFO, MarkupHelper.createLabel("User found Active Campaigns under Sports Category", ExtentColor.BLUE));
					steps.clickButton(home2.Photography_Sports, driver, name);
				}
				Thread.sleep(1000);
				steps.selectDropdown(home2.Photography_SortDD, driver, strDataFileName, "PhotographySortBy1", name);
				test.log(Status.INFO, MarkupHelper.createLabel("User sort out New Arrival products in Photography", ExtentColor.BLUE));
				Thread.sleep(2000);
				steps.selectDropdown(home2.Photography_SortDD, driver, strDataFileName, "PhotographySortBy2", name);
				test.log(Status.INFO, MarkupHelper.createLabel("User sort out Best Selling products in Photography", ExtentColor.BLUE));
				steps.ScrollUp(driver);
				Thread.sleep(2000);
				steps.MoveElement(home2.Photography_Image1, driver, name);
				steps.clickJSButton(home2.Photography_Image1, driver, name);
				Photographyimage1 = getscreen.capture(driver, "Photographyimage1");
				test.addScreenCaptureFromPath(Photographyimage1);
				test.log(Status.INFO, MarkupHelper.createLabel("User selected First Image", ExtentColor.BLUE));
			}
			catch(Exception e )
			{
				System.out.println("User navigated to wrong page");
			}
			Thread.sleep(1000);
			try
			{
				steps.clickButton(home2.Photography_Image1_Readmore, driver, name);
				Description1 = driver.findElement(home2.Photography_Image1_Description).getText();
				System.out.println("Product Description:  "+Description1);
				test.log(Status.INFO, MarkupHelper.createLabel("Product Description:  "+Description1, ExtentColor.BROWN));
				steps.clickButton(home2.Photography_Image1_Readless, driver, name);
			}
			catch(Exception e)
			{
				steps.clickButton(home2.Photography_Image1_Readmore, driver, name);
				String Description2 = driver.findElement(home2.Photography_Image1_Description1).getText();
				System.out.println("Product Description:  "+Description2);
				test.log(Status.INFO, MarkupHelper.createLabel("Product Description:  "+Description2, ExtentColor.BROWN));
				steps.clickButton(home2.Photography_Image1_Readless, driver, name);
			}
			MRP1 = driver.findElement(home2.Photography_Image1_MRP).getText();
			System.out.println("Product MRP:  "+MRP1);
			test.log(Status.INFO, MarkupHelper.createLabel("Product MRP:  "+MRP1, ExtentColor.ORANGE));
			try
			{
				String product2 = driver.findElement(home2.Photography_Image1_Prpduct2).getCssValue("color");
				System.out.println("Product 2 exists:  "+product2);
				steps.clickButton(home2.Photography_Image1_Prpduct2, driver, name);
				test.log(Status.INFO, MarkupHelper.createLabel("User selected Second Product", ExtentColor.BLUE));
				PhotographyProduct2 = getscreen.capture(driver, "PhotographyProduct2");
				test.addScreenCaptureFromPath(PhotographyProduct2);
			}
			catch(Exception e)
			{
				System.out.println("Product 2 is not available");
			}
			try
			{
				String product3 = driver.findElement(home2.Photography_Image1_Prpduct3).getCssValue("color");
				System.out.println("Product 2 exists:  "+product3);
				steps.clickButton(home2.Photography_Image1_Prpduct3, driver, name);
				test.log(Status.INFO, MarkupHelper.createLabel("User selected Third Product", ExtentColor.BLUE));
				PhotographyProduct3 = getscreen.capture(driver, "PhotographyProduct3");
				test.addScreenCaptureFromPath(PhotographyProduct3);
			}
			catch(Exception e)
			{
				System.out.println("Product 3 is not available");
			}
			try
			{
				String product1 = driver.findElement(home2.Photography_Image1_Prpduct1).getCssValue("color");
				System.out.println("Product 1 exists:  "+product1);
				steps.clickButton(home2.Photography_Image1_Prpduct1, driver, name);
				test.log(Status.INFO, MarkupHelper.createLabel("User selected First Product", ExtentColor.BLUE));
				PhotographyProduct1 = getscreen.capture(driver, "PhotographyProduct1");
				test.addScreenCaptureFromPath(PhotographyProduct1);
			}
			catch(Exception e)
			{
				System.out.println("Product 1 is not available");
			}
			try
			{
				String col1 = driver.findElement(home2.color1).getCssValue("color");
				System.out.println("Color 2 exists:  "+col1);
				steps.clickButton(home2.color1, driver, name);
				Thread.sleep(2000);
				PhotoColor1 = driver.findElement(home2.color1).getCssValue("background-color");
				Photohex1 = Color.fromString(PhotoColor1).asHex();
				System.out.println("Image color: "+Photohex1);
				test.log(Status.INFO, MarkupHelper.createLabel("User changed item color to:  "+Photohex1, ExtentColor.BLUE));
				PhotoFirstcolor = getscreen.capture(driver, "PhotoFirstcolor");
				test.addScreenCaptureFromPath(PhotoFirstcolor);
			}
			catch(Exception e)
			{
				System.out.println("Color 2 is not available");
			}
			try
			{
				String col2 = driver.findElement(home2.color2).getCssValue("color");
				System.out.println("Color 3 exists:  "+col2);
				steps.clickButton(home2.color2, driver, name);
				Thread.sleep(1000);
				PhotoColor2 = driver.findElement(home2.color2).getCssValue("background-color");
				Photohex2 = Color.fromString(PhotoColor2).asHex();
				System.out.println("Image color: "+Photohex2);
				test.log(Status.INFO, MarkupHelper.createLabel("User changed item color to:  "+Photohex2, ExtentColor.BLUE));
				PhotoSecondcolor = getscreen.capture(driver, "PhotoSecondcolor");
				test.addScreenCaptureFromPath(PhotoSecondcolor);
			}
			catch(Exception e)
			{
				System.out.println("Color 3 is not available");
			}
			try
			{
				String col3 = driver.findElement(home2.color3).getCssValue("color");
				System.out.println("Color 4 exists:  "+col3);
				steps.clickButton(home2.color3, driver, name);
				Thread.sleep(1000);
				PhotoColor3 = driver.findElement(home2.color3).getCssValue("background-color");
				Photohex3 = Color.fromString(PhotoColor3).asHex();
				System.out.println("Image color: "+Photohex3);
				test.log(Status.INFO, MarkupHelper.createLabel("User changed item color to:  "+Photohex3, ExtentColor.BLUE));
				PhotoThirdcolor = getscreen.capture(driver, "PhotoThirdcolor");
				test.addScreenCaptureFromPath(PhotoThirdcolor);
			}
			catch(Exception e)
			{
				System.out.println("Color 4 is not available");
			}
			try
			{
				String col4 = driver.findElement(home2.color4).getCssValue("color");
				System.out.println("Color 1 exists:  "+col4);
				steps.clickButton(home2.color4, driver, name);
				Thread.sleep(1000);
				PhotoColor4 = driver.findElement(home2.color4).getCssValue("background-color");
				Photohex4 = Color.fromString(PhotoColor4).asHex();
				System.out.println("Image color: "+Photohex4);
				test.log(Status.INFO, MarkupHelper.createLabel("User changed item color to:  "+Photohex4, ExtentColor.BLUE));
				PhotoFourthcolor = getscreen.capture(driver, "PhotoFourthcolor");
				test.addScreenCaptureFromPath(PhotoFourthcolor);
			}
			catch(Exception e)
			{
				System.out.println("Color 1 is not available");
			}
			Photocqty = driver.findElement(home2.Qty).getAttribute("value");
			System.out.println("Current Quantity:  "+ Photocqty);
			test.log(Status.INFO, MarkupHelper.createLabel("Current Quantity of product:  "+Photocqty, ExtentColor.BLUE));
			Thread.sleep(1000);
			steps.clickButton(home2.QtyIncrease, driver, name);
			Photoiqty = driver.findElement(home2.Qty).getAttribute("value");
			System.out.println("Quantity after increase:  "+ Photoiqty);
			test.log(Status.INFO, MarkupHelper.createLabel("Current Quantity of product after Increase:  "+Photoiqty, ExtentColor.BLUE));
			steps.clickButton(home.Trend_QtyDecrease, driver, name);
			Photodqty = driver.findElement(home2.Qty).getAttribute("value");
			System.out.println("Quantity after decrease:  "+ Photodqty);
			test.log(Status.INFO, MarkupHelper.createLabel("Current Quantity of product after increase:  "+Photodqty, ExtentColor.BLUE));
			Thread.sleep(2000);
			steps.clickButton(home2.Addcart, driver, name);
			PhotoSizemsg = driver.findElement(home2.Sizeerrormsg).getText();
			System.out.println("User clicked on Addcart before Size selection:  "+PhotoSizemsg);
			test.log(Status.INFO, MarkupHelper.createLabel("User clicked on Addcart before Size selection:  "+PhotoSizemsg, ExtentColor.RED));
			steps.clickJSButton(home2.Size_S, driver, name);
			PhotosizeS1 = driver.findElement(home2.Size_S).getText();
			System.out.println("Selected Size:  "+PhotosizeS1);
			test.log(Status.INFO, MarkupHelper.createLabel("User selected size:  "+PhotosizeS1, ExtentColor.BLUE));
			steps.clickButton(home2.Size_M, driver, name);
			PhotosizeM1 = driver.findElement(home2.Size_M).getText();
			System.out.println("Selected Size:  "+PhotosizeM1);
			test.log(Status.INFO, MarkupHelper.createLabel("User selected size:  "+PhotosizeM1, ExtentColor.BLUE));
			steps.clickButton(home2.Size_L, driver, name);
			PhotosizeL1 = driver.findElement(home2.Size_L).getText();
			System.out.println("Selected Size:  "+PhotosizeL1);
			test.log(Status.INFO, MarkupHelper.createLabel("User selected size:  "+PhotosizeL1, ExtentColor.BLUE));
			steps.clickButton(home2.Size_XL, driver, name);
			PhotosizeXL1 = driver.findElement(home2.Size_XL).getText();
			System.out.println("Selected Size:  "+PhotosizeXL1);
			test.log(Status.INFO, MarkupHelper.createLabel("User selected size:  "+PhotosizeXL1, ExtentColor.BLUE));
			steps.clickButton(home2.Size_XXL, driver, name);
			PhotosizeXXL1 = driver.findElement(home2.Size_XXL).getText();
			System.out.println("Selected Size:  "+PhotosizeXXL1);
			test.log(Status.INFO, MarkupHelper.createLabel("User selected size:  "+PhotosizeXXL1, ExtentColor.BLUE));
			steps.clickButton(home2.Size_XXXL, driver, name);
			PhotosizeXXXL1 = driver.findElement(home2.Size_XXXL).getText();
			System.out.println("Selected Size:  "+PhotosizeXXXL1);
			test.log(Status.INFO, MarkupHelper.createLabel("User selected size:  "+PhotosizeXXXL1, ExtentColor.BLUE));
			steps.clickButton(home2.FindUrSize, driver, name);
			test.log(Status.INFO, MarkupHelper.createLabel("User clicked on Find your size", ExtentColor.BLUE));
			Thread.sleep(1000);
			try
			{
				String R510 = driver.findElement(home.Trend_FindUrSize_510radio).getText();
				System.out.println(R510);
				steps.clickButton(home.Trend_FindUrSize_510radio, driver, name);
				below510 = driver.findElement(home.Trend_FindUrSize_510radio).getText();
				System.out.println("User Selected:  "+below510);
				test.log(Status.INFO, MarkupHelper.createLabel("User selected Height:  "+below510, ExtentColor.BLUE));
				steps.clickButton(home.Trend_FindUrSize_above510radio, driver, name);
				above510 = driver.findElement(home.Trend_FindUrSize_above510radio).getText();
				System.out.println("User Selected:  "+above510);
				test.log(Status.INFO, MarkupHelper.createLabel("User selected Height:  "+above510, ExtentColor.BLUE));
				steps.clickButton(home.Trend_FindUrSize_Tightradio, driver, name);
				tightfit = driver.findElement(home.Trend_FindUrSize_Tightradio).getText();
				System.out.println("User Selected:  "+tightfit);
				test.log(Status.INFO, MarkupHelper.createLabel("User selected Fit:  "+tightfit, ExtentColor.BLUE));
				steps.clickButton(home.Trend_FindUrSize_Comfortradio, driver, name);
				comfit = driver.findElement(home.Trend_FindUrSize_Comfortradio).getText();
				System.out.println("User Selected:  "+comfit);
				test.log(Status.INFO, MarkupHelper.createLabel("User selected Fit:  "+comfit, ExtentColor.BLUE));
				steps.selectDropdown(home2.FindUrSize_sizeDD, driver, strDataFileName, "size", name);
				test.log(Status.INFO, MarkupHelper.createLabel("User selected shirt size", ExtentColor.BLUE));
				steps.clickButton(home2.FindUrSize_submit, driver, name);
				test.log(Status.INFO, MarkupHelper.createLabel("User clicked on Submit", ExtentColor.BLUE));
				Photofindursize1 = getscreen.capture(driver, "Photofindursize1");
				test.addScreenCaptureFromPath(Photofindursize1);
				Thread.sleep(1000);
				steps.clickButton(home2.FindUrSize_Okbutton, driver, name);
				test.log(Status.INFO, MarkupHelper.createLabel("system displayed size for given metrics", ExtentColor.BLUE));
				steps.clickButton(home2.FindUrSize_closebutton, driver, name);
				test.log(Status.INFO, MarkupHelper.createLabel("User navigate to Product details page", ExtentColor.BLUE));
			}
			catch(Exception e)
			{
				steps.clickButton(home2.FindUrSize_closebutton, driver, name);
				test.log(Status.INFO, MarkupHelper.createLabel("User navigate to Product details page", ExtentColor.BLUE));
			}
			steps.clickButton(home2.Size_L, driver, name);
			test.log(Status.INFO, MarkupHelper.createLabel("User selected size", ExtentColor.BLUE));
			steps.clickButton(home2.Addcart, driver, name);
			test.log(Status.INFO, MarkupHelper.createLabel("User added product to Cart", ExtentColor.BLUE));
			Photoaddcart1 = getscreen.capture(driver, "Photoaddcart1");
			test.addScreenCaptureFromPath(Photoaddcart1);
			try
			{
				String edit = driver.findElement(home2.Addcart_edit).getText();
				System.out.println(edit);
				steps.clickButton(home2.Addcart_edit, driver, name);
				test.log(Status.INFO, MarkupHelper.createLabel("User clicked on Edit", ExtentColor.BLUE));
				steps.selectDropdown(home2.Addcart_selectsize, driver, strDataFileName, "SelectSize", name);
				steps.selectDropdown(home2.Addcart_selectqty, driver, strDataFileName, "Quantity", name);
				test.log(Status.INFO, MarkupHelper.createLabel("User selected Size and Quantity", ExtentColor.BLUE));
				steps.clickButton(home2.Addcart_cancel, driver, name);
				steps.clickButton(home2.Addcart_edit, driver, name);
				steps.selectDropdown(home2.Addcart_selectsize, driver, strDataFileName, "SelectSize", name);
				steps.selectDropdown(home2.Addcart_selectqty, driver, strDataFileName, "Quantity", name);
				steps.clickButton(home2.Addcart_save, driver, name);
				Photoupdateproduct1 = getscreen.capture(driver, "Photoupdateproduct1");
				test.addScreenCaptureFromPath(Photoupdateproduct1);
				test.log(Status.INFO, MarkupHelper.createLabel("User updated product size and quantity", ExtentColor.BLUE));
			}
			catch(Exception e)
			{
				System.out.println("Edit feature is not available for this product");
			}
			steps.clickButton(home.Trend_Addcart_remove, driver, name);
			Photoremovecart1 = getscreen.capture(driver, "Photoremovecart1");
			test.addScreenCaptureFromPath(Photoremovecart1);
			test.log(Status.INFO, MarkupHelper.createLabel("User removed product from Cart", ExtentColor.BLUE));
			steps.clickButton(home.Size_L, driver, name);
			test.log(Status.INFO, MarkupHelper.createLabel("User selected size", ExtentColor.BLUE));
			steps.clickButton(home.BuyNow, driver, name);
			test.log(Status.INFO, MarkupHelper.createLabel("User clicked on BuyNow, navigated to checkout page", ExtentColor.BLUE));
			String Prodbuynow1 = getscreen.capture(driver, "Prodbuynow1");
			test.addScreenCaptureFromPath(Prodbuynow1);
			try
			{
				String cash = driver.findElement(home.CashonDeliveryText).getText();
				System.out.println("Payment method:  "+cash);
				steps.Verifytext(home.CashonDeliveryText, driver, cash, name);
				steps.clickButton(home.CashonDeliveryradio, driver, name);
				String Prodbuynow2 = driver.findElement(home.PopUpSaveMsg).getText();
				System.out.println("Displayed message:   "+Prodbuynow2);
				test.log(Status.INFO, MarkupHelper.createLabel("Displayed message:   "+Prodbuynow2, ExtentColor.BLUE));
				Thread.sleep(1000);
				/*	buynow3 = driver.findElement(home.PopUpMsg).getAttribute("value");
			System.out.println("Displayed message:   "+buynow3);
			test.log(Status.INFO, MarkupHelper.createLabel("Displayed message:   "+buynow3, ExtentColor.BLUE));
				 */
				String	 Prodbuynow4 = getscreen.capture(driver, "Prodbuynow4");
				test.addScreenCaptureFromPath(Prodbuynow4);
				steps.clickButton(home.Cashondelivery, driver, name);
				test.log(Status.INFO, MarkupHelper.createLabel("User clicked on Cash on Delivery link", ExtentColor.BLUE));
			}
			catch(Exception e)
			{
				System.out.println("Cash on Delivery is not available");
			}
			Thread.sleep(1000);
			steps.clickButton(home.CartIcon, driver, name);
			steps.clickButton(home.Trend_Addcart_remove, driver, name);
			test.log(Status.INFO, MarkupHelper.createLabel("User Completed transactions of 1st Image in Photography", ExtentColor.PURPLE));
			driver.close();
			driver.switchTo().window(Parent);
			Thread.sleep(1000);
			Photographystatus = passstatus;
			Assert.assertTrue(true);
			System.out.println("Photography Who Inpires You Most Successful");
			testresultdata.put(Integer.toString(cnt), new Object[] {name, "Photography under Who Inpires You Most", "Photography under Who Inpires You Most Successful",passstatus});
		}
		catch(Exception e)
		{
			System.out.println("Photography under Who Inpires You Most Unsuccessful");
			testresultdata.put(Integer.toString(cnt), new Object[] {name, "Photography under Who Inpires You Most", "Photography under Who Inpires You Most Unsuccessful",failstatus});
			Photographystatus = failstatus;
			String Photographyfail = getscreen.capture(driver, "Photographyfail");
			test.addScreenCaptureFromPath(Photographyfail);
			driver.close();
			Assert.assertTrue(false);
		}
	}
	@Test(priority=1)
	public void Travelling() throws Exception
	{
		String name = new Object(){}.getClass().getEnclosingMethod().getName();
		cnt ++;
		try
		{
			test = extent.createTest("Travelling under What Inspires You Most");
			System.out.println("Travelling under What Inspires You Most Test Case Executing...");
			Thread.sleep(1000);
			commonmethods.URLNaviagtion(driver);
			Thread.sleep(1000);
			/*			commonmethods.Login(driver);
			test.log(Status.INFO, MarkupHelper.createLabel("User able to Login", ExtentColor.BLUE));
			Thread.sleep(1000);
			 */
			steps.clickButton(home.Mydreamstore, driver, name);
			String Parent = driver.getWindowHandle();
			Thread.sleep(2000);
			steps.MoveElement(home2.WIM_Image2, driver, name);
			steps.clickButton(home2.WIM_Image2, driver, name);
			test.log(Status.INFO, MarkupHelper.createLabel("User clicked on Travelling Category in Who Inspires You Most", ExtentColor.BLUE));
			Thread.sleep(2000);
			steps.ChildWindow(driver);
			driver.manage().window().maximize();
			test.log(Status.INFO, MarkupHelper.createLabel("User navigated to Next Window", ExtentColor.BLUE));
			TravellingProducts = getscreen.capture(driver, "TravellingProducts");
			test.addScreenCaptureFromPath(TravellingProducts);
			Thread.sleep(1000);
			try
			{
				String Filterpage2 = driver.findElement(home.Filterpagetext).getText();
				System.out.println("User navigated to :   "+Filterpage2);
			}
			catch(Exception e )
			{
				System.out.println("User navigated to Different landing page");
				steps.clickButton(home2.Travelling_Image1, driver, name);
				driver.close();
				Thread.sleep(2000);
				steps.ChildWindow(driver);
				driver.manage().window().maximize();
			}
			Thread.sleep(2000);
			/*			steps.clickButton(home2.Photography_Motherday, driver, name);
			test.log(Status.INFO, MarkupHelper.createLabel("User selected on Mothers Day Category", ExtentColor.BLUE));
			try
			{
				TravelNoCampaignMsg1 = driver.findElement(home2.Photography_NoCampaignFound).getText();
				System.out.println("Campaigns Not Found under Mother's day Category");
				test.log(Status.INFO, MarkupHelper.createLabel("User hasn't found Active Campaigns under Mothers Day Category", ExtentColor.BLUE));
				MothersDayProducts1 = getscreen.capture(driver, "MothersDayProducts1");
				test.addScreenCaptureFromPath(MothersDayProducts1);
				steps.clickButton(home2.Photography_Motherday, driver, name);
			}
			catch(Exception e)
			{
				System.out.println("Campaigns Found");
				test.log(Status.INFO, MarkupHelper.createLabel("User found Active Campaigns under Mothers Day Category", ExtentColor.BLUE));
				steps.clickButton(home2.Photography_Motherday, driver, name);
			}
			steps.clickButton(home2.Photography_Bolly, driver, name);
			try
			{
				TravelNoCampaignMsg2 = driver.findElement(home2.Photography_NoCampaignFound).getText();
				System.out.println("Campaigns Not Found under Bollywood/Movie Personalities Category");
				test.log(Status.INFO, MarkupHelper.createLabel("User hasn't found Active Campaigns under Bollywood/Movie Personalities Category", ExtentColor.BLUE));
				BollywoodProducts1 = getscreen.capture(driver, "BollywoodProducts1");
				test.addScreenCaptureFromPath(BollywoodProducts1);
				steps.clickButton(home2.Photography_Bolly, driver, name);
			}
			catch(Exception e)
			{
				System.out.println("Campaigns Found");
				test.log(Status.INFO, MarkupHelper.createLabel("User found Active Campaigns under Bollywood/Movie Personalities Category", ExtentColor.BLUE));
				steps.clickButton(home2.Photography_Bolly, driver, name);
			}
			steps.clickButton(home2.Photography_Sports, driver, name);
			try
			{
				TravelNoCampaignMsg3 = driver.findElement(home2.Photography_NoCampaignFound).getText();
				System.out.println("Campaigns Not Found under Sports Category");
				test.log(Status.INFO, MarkupHelper.createLabel("User hasn't found Active Campaigns under Sports Category", ExtentColor.BLUE));
				TravellingSportsProducts1 = getscreen.capture(driver, "TravellingSportsProducts1");
				test.addScreenCaptureFromPath(TravellingSportsProducts1);
				steps.clickButton(home2.Photography_Sports, driver, name);
			}
			catch(Exception e)
			{
				System.out.println("Campaigns Found");
				test.log(Status.INFO, MarkupHelper.createLabel("User found Active Campaigns under Sports Category", ExtentColor.BLUE));
				steps.clickButton(home2.Photography_Sports, driver, name);
			}
			Thread.sleep(2000);
			steps.selectDropdown(home2.Photography_SortDD, driver, strDataFileName, "PhotographySortBy1", name);
			test.log(Status.INFO, MarkupHelper.createLabel("User sort out New Arrival products in Photography", ExtentColor.BLUE));
			Thread.sleep(3000);
			steps.selectDropdown(home2.Photography_SortDD, driver, strDataFileName, "PhotographySortBy2", name);
			test.log(Status.INFO, MarkupHelper.createLabel("User sort out Best Selling products in Photography", ExtentColor.BLUE));
			steps.ScrollUp(driver);
			Thread.sleep(2000);
			steps.MoveElement(home2.Travelling_Image1, driver, name);
			steps.clickJSButton(home2.Travelling_Image1, driver, name);
			Travellingimage1 = getscreen.capture(driver, "Travellingimage1");
			test.addScreenCaptureFromPath(Travellingimage1);
			test.log(Status.INFO, MarkupHelper.createLabel("User selected First Image", ExtentColor.BLUE));
			 */
			try
			{
				steps.clickButton(home2.Photography_Image1_Readmore, driver, name);
				TravellingDescription = driver.findElement(home2.Travelling__Description).getText();
				System.out.println("Product Description:  "+TravellingDescription);
				test.log(Status.INFO, MarkupHelper.createLabel("Product Description:  "+TravellingDescription, ExtentColor.BROWN));
				Thread.sleep(2000);
				//steps.ScrollUp(driver);
				steps.MoveElement(home2.Photography_Image1_Readless, driver, name);
				steps.clickButton(home2.Photography_Image1_Readless, driver, name);
			}
			catch(Exception e)
			{
				steps.clickButton(home2.Photography_Image1_Readmore, driver, name);
				TravellingDescription = driver.findElement(home2.Travelling__Description1).getText();
				System.out.println("Product Description:  "+TravellingDescription);
				test.log(Status.INFO, MarkupHelper.createLabel("Product Description:  "+TravellingDescription, ExtentColor.BROWN));
				Thread.sleep(2000);
				steps.clickButton(home2.Photography_Image1_Readless, driver, name);
			}
			TravellingMRP = driver.findElement(home2.Photography_Image1_MRP).getText();
			System.out.println("Product MRP:  "+TravellingMRP);
			test.log(Status.INFO, MarkupHelper.createLabel("Product MRP:  "+TravellingMRP, ExtentColor.ORANGE));
			try
			{
				String product2 = driver.findElement(home2.Travelling_Prpduct2).getCssValue("color");
				System.out.println("Product 2 exists:  "+product2);
				steps.clickButton(home2.Travelling_Prpduct2, driver, name);
				test.log(Status.INFO, MarkupHelper.createLabel("User selected Second Product", ExtentColor.BLUE));
				TravellingProduct2 = getscreen.capture(driver, "TravellingProduct2");
				test.addScreenCaptureFromPath(TravellingProduct2);
			}
			catch(Exception e)
			{
				System.out.println("Product 2 is not available");
			}
			try
			{
				String product1 = driver.findElement(home2.Travelling_Prpduct1).getCssValue("color");
				System.out.println("Product 1 exists:  "+product1);
				steps.clickButton(home2.Travelling_Prpduct1, driver, name);
				test.log(Status.INFO, MarkupHelper.createLabel("User selected Third Product", ExtentColor.BLUE));
				TravellingProduct3 = getscreen.capture(driver, "TravellingProduct3");
				test.addScreenCaptureFromPath(TravellingProduct3);
			}
			catch(Exception e)
			{
				System.out.println("Product 1 is not available");
			}
			try
			{
				String col1 = driver.findElement(home2.color1).getCssValue("color");
				System.out.println("Color 2 exists:  "+col1);
				steps.clickButton(home2.color1, driver, name);
				Thread.sleep(2000);
				TravellingPhotoColor1 = driver.findElement(home2.color1).getCssValue("background-color");
				TravellingPhotohex1 = Color.fromString(TravellingPhotoColor1).asHex();
				System.out.println("Image color: "+TravellingPhotohex1);
				test.log(Status.INFO, MarkupHelper.createLabel("User changed item color to:  "+TravellingPhotohex1, ExtentColor.BLUE));
				TravellingFirstcolor = getscreen.capture(driver, "TravellingFirstcolor");
				test.addScreenCaptureFromPath(TravellingFirstcolor);
			}
			catch(Exception e)
			{
				System.out.println("Color 2 is not available");
			}
			try
			{
				String col2 = driver.findElement(home2.color2).getCssValue("color");
				System.out.println("Color 3 exists:  "+col2);
				steps.clickButton(home2.color2, driver, name);
				Thread.sleep(1000);
				TravellingColor2 = driver.findElement(home2.color2).getCssValue("background-color");
				Travellinghex2 = Color.fromString(TravellingColor2).asHex();
				System.out.println("Image color: "+Travellinghex2);
				test.log(Status.INFO, MarkupHelper.createLabel("User changed item color to:  "+Travellinghex2, ExtentColor.BLUE));
				TravellingSecondcolor = getscreen.capture(driver, "TravellingSecondcolor");
				test.addScreenCaptureFromPath(TravellingSecondcolor);
			}
			catch(Exception e)
			{
				System.out.println("Color 3 is not available");
			}
			try
			{
				String col3 = driver.findElement(home2.color3).getCssValue("color");
				System.out.println("Color 4 exists:  "+col3);
				steps.clickButton(home2.color3, driver, name);
				Thread.sleep(1000);
				TravellingColor3 = driver.findElement(home2.color3).getCssValue("background-color");
				Travellinghex3 = Color.fromString(TravellingColor3).asHex();
				System.out.println("Image color: "+Travellinghex3);
				test.log(Status.INFO, MarkupHelper.createLabel("User changed item color to:  "+Travellinghex3, ExtentColor.BLUE));
				TravellingThirdcolor = getscreen.capture(driver, "TravellingThirdcolor");
				test.addScreenCaptureFromPath(TravellingThirdcolor);
			}
			catch(Exception e)
			{
				System.out.println("Color 4 is not available");
			}
			try
			{
				String col4 = driver.findElement(home2.color4).getCssValue("color");
				System.out.println("Color 1 exists:  "+col4);
				steps.clickButton(home2.color4, driver, name);
				Thread.sleep(1000);
				TravellingColor4 = driver.findElement(home2.color4).getCssValue("background-color");
				Travellinghex4 = Color.fromString(TravellingColor4).asHex();
				System.out.println("Image color: "+Travellinghex4);
				test.log(Status.INFO, MarkupHelper.createLabel("User changed item color to:  "+Travellinghex4, ExtentColor.BLUE));
				TravellingFourthcolor = getscreen.capture(driver, "TravellingFourthcolor");
				test.addScreenCaptureFromPath(TravellingFourthcolor);
			}
			catch(Exception e)
			{
				System.out.println("Color 1 is not available");
			}
			Travellingcqty = driver.findElement(home2.Qty).getAttribute("value");
			System.out.println("Current Quantity:  "+ Travellingcqty);
			test.log(Status.INFO, MarkupHelper.createLabel("Current Quantity of product:  "+Travellingcqty, ExtentColor.BLUE));
			Thread.sleep(1000);
			steps.clickButton(home2.QtyIncrease, driver, name);
			Travellingiqty = driver.findElement(home2.Qty).getAttribute("value");
			System.out.println("Quantity after increase:  "+ Travellingiqty);
			test.log(Status.INFO, MarkupHelper.createLabel("Current Quantity of product after Increase:  "+Travellingiqty, ExtentColor.BLUE));
			steps.clickButton(home.Trend_QtyDecrease, driver, name);
			Travellingdqty = driver.findElement(home2.Qty).getAttribute("value");
			System.out.println("Quantity after decrease:  "+ Travellingdqty);
			test.log(Status.INFO, MarkupHelper.createLabel("Current Quantity of product after increase:  "+Travellingdqty, ExtentColor.BLUE));
			Thread.sleep(2000);
			steps.clickButton(home2.Addcart, driver, name);
			TravellingSizemsg = driver.findElement(home2.Sizeerrormsg).getText();
			System.out.println("User clicked on Addcart before Size selection:  "+TravellingSizemsg);
			test.log(Status.INFO, MarkupHelper.createLabel("User clicked on Addcart before Size selection:  "+TravellingSizemsg, ExtentColor.RED));
			steps.clickJSButton(home2.Size_S, driver, name);
			TravellingsizeS1 = driver.findElement(home2.Size_S).getText();
			System.out.println("Selected Size:  "+TravellingsizeS1);
			test.log(Status.INFO, MarkupHelper.createLabel("User selected size:  "+TravellingsizeS1, ExtentColor.BLUE));
			steps.clickButton(home2.Size_M, driver, name);
			TravellingsizeM1 = driver.findElement(home2.Size_M).getText();
			System.out.println("Selected Size:  "+TravellingsizeM1);
			test.log(Status.INFO, MarkupHelper.createLabel("User selected size:  "+TravellingsizeM1, ExtentColor.BLUE));
			steps.clickButton(home2.Size_L, driver, name);
			TravellingsizeL1 = driver.findElement(home2.Size_L).getText();
			System.out.println("Selected Size:  "+TravellingsizeL1);
			test.log(Status.INFO, MarkupHelper.createLabel("User selected size:  "+TravellingsizeL1, ExtentColor.BLUE));
			steps.clickButton(home2.Size_XL, driver, name);
			TravellingsizeXL1 = driver.findElement(home2.Size_XL).getText();
			System.out.println("Selected Size:  "+TravellingsizeXL1);
			test.log(Status.INFO, MarkupHelper.createLabel("User selected size:  "+TravellingsizeXL1, ExtentColor.BLUE));
			steps.clickButton(home2.Size_XXL, driver, name);
			TravellingsizeXXL1 = driver.findElement(home2.Size_XXL).getText();
			System.out.println("Selected Size:  "+TravellingsizeXXL1);
			test.log(Status.INFO, MarkupHelper.createLabel("User selected size:  "+TravellingsizeXXL1, ExtentColor.BLUE));
			steps.clickButton(home2.Size_XXXL, driver, name);
			TravellingsizeXXXL1 = driver.findElement(home2.Size_XXXL).getText();
			System.out.println("Selected Size:  "+TravellingsizeXXXL1);
			test.log(Status.INFO, MarkupHelper.createLabel("User selected size:  "+TravellingsizeXXXL1, ExtentColor.BLUE));
			steps.clickButton(home2.FindUrSize, driver, name);
			test.log(Status.INFO, MarkupHelper.createLabel("User clicked on Find your size", ExtentColor.BLUE));
			Thread.sleep(1000);
			try
			{
				String R510 = driver.findElement(home.Trend_FindUrSize_510radio).getText();
				System.out.println(R510);
				steps.clickButton(home.Trend_FindUrSize_510radio, driver, name);
				Travellingbelow510 = driver.findElement(home.Trend_FindUrSize_510radio).getText();
				System.out.println("User Selected:  "+Travellingbelow510);
				test.log(Status.INFO, MarkupHelper.createLabel("User selected Height:  "+Travellingbelow510, ExtentColor.BLUE));
				steps.clickButton(home.Trend_FindUrSize_above510radio, driver, name);
				Travellingabove510 = driver.findElement(home.Trend_FindUrSize_above510radio).getText();
				System.out.println("User Selected:  "+Travellingabove510);
				test.log(Status.INFO, MarkupHelper.createLabel("User selected Height:  "+Travellingabove510, ExtentColor.BLUE));
				steps.clickButton(home.Trend_FindUrSize_Tightradio, driver, name);
				Travellingtightfit = driver.findElement(home.Trend_FindUrSize_Tightradio).getText();
				System.out.println("User Selected:  "+Travellingtightfit);
				test.log(Status.INFO, MarkupHelper.createLabel("User selected Fit:  "+Travellingtightfit, ExtentColor.BLUE));
				steps.clickButton(home.Trend_FindUrSize_Comfortradio, driver, name);
				Travellingcomfit = driver.findElement(home.Trend_FindUrSize_Comfortradio).getText();
				System.out.println("User Selected:  "+Travellingcomfit);
				test.log(Status.INFO, MarkupHelper.createLabel("User selected Fit:  "+Travellingcomfit, ExtentColor.BLUE));
				steps.selectDropdown(home2.FindUrSize_sizeDD, driver, strDataFileName, "size", name);
				test.log(Status.INFO, MarkupHelper.createLabel("User selected shirt size", ExtentColor.BLUE));
				steps.clickButton(home2.FindUrSize_submit, driver, name);
				test.log(Status.INFO, MarkupHelper.createLabel("User clicked on Submit", ExtentColor.BLUE));
				Travellingfindursize1 = getscreen.capture(driver, "Travellingfindursize1");
				test.addScreenCaptureFromPath(Travellingfindursize1);
				Thread.sleep(1000);
				steps.clickButton(home2.FindUrSize_Okbutton, driver, name);
				test.log(Status.INFO, MarkupHelper.createLabel("system displayed size for given metrics", ExtentColor.BLUE));
				steps.clickButton(home2.FindUrSize_closebutton, driver, name);
				test.log(Status.INFO, MarkupHelper.createLabel("User navigate to Product details page", ExtentColor.BLUE));
			}
			catch(Exception e)
			{
				steps.clickButton(home2.FindUrSize_closebutton, driver, name);
				test.log(Status.INFO, MarkupHelper.createLabel("User navigate to Product details page", ExtentColor.BLUE));
			}
			steps.clickButton(home2.Size_XL, driver, name);
			test.log(Status.INFO, MarkupHelper.createLabel("User selected size", ExtentColor.BLUE));
			steps.clickButton(home2.Addcart, driver, name);
			test.log(Status.INFO, MarkupHelper.createLabel("User added product to Cart", ExtentColor.BLUE));
			Travellingaddcart1 = getscreen.capture(driver, "Travellingaddcart1");
			test.addScreenCaptureFromPath(Travellingaddcart1);
			try
			{
				String edit = driver.findElement(home2.Addcart_edit).getText();
				System.out.println(edit);
				steps.clickButton(home2.Addcart_edit, driver, name);
				test.log(Status.INFO, MarkupHelper.createLabel("User clicked on Edit", ExtentColor.BLUE));
				steps.selectDropdown(home2.Addcart_selectsize, driver, strDataFileName, "SelectSize", name);
				steps.selectDropdown(home2.Addcart_selectqty, driver, strDataFileName, "Quantity", name);
				test.log(Status.INFO, MarkupHelper.createLabel("User selected Size and Quantity", ExtentColor.BLUE));
				steps.clickButton(home2.Addcart_cancel, driver, name);
				steps.clickButton(home2.Addcart_edit, driver, name);
				steps.selectDropdown(home2.Addcart_selectsize, driver, strDataFileName, "SelectSize", name);
				steps.selectDropdown(home2.Addcart_selectqty, driver, strDataFileName, "Quantity", name);
				steps.clickButton(home2.Addcart_save, driver, name);
				Travellingupdateproduct1 = getscreen.capture(driver, "Travellingupdateproduct1");
				test.addScreenCaptureFromPath(Travellingupdateproduct1);
				test.log(Status.INFO, MarkupHelper.createLabel("User updated product size and quantity", ExtentColor.BLUE));
			}
			catch(Exception e)
			{
				System.out.println("Edit feature is not available for this product");
			}
			steps.clickButton(home.Trend_Addcart_remove, driver, name);
			Travellingremovecart1 = getscreen.capture(driver, "Travellingremovecart1");
			test.addScreenCaptureFromPath(Travellingremovecart1);
			test.log(Status.INFO, MarkupHelper.createLabel("User removed product from Cart", ExtentColor.BLUE));
			steps.clickButton(home.Size_L, driver, name);
			test.log(Status.INFO, MarkupHelper.createLabel("User selected size", ExtentColor.BLUE));
			steps.clickButton(home.BuyNow, driver, name);
			test.log(Status.INFO, MarkupHelper.createLabel("User clicked on BuyNow, navigated to checkout page", ExtentColor.BLUE));
			String Travbuynow1 = getscreen.capture(driver, "Travbuynow1");
			test.addScreenCaptureFromPath(Travbuynow1);
			try
			{
				String cash = driver.findElement(home.CashonDeliveryText).getText();
				System.out.println("Payment method:  "+cash);
				steps.Verifytext(home.CashonDeliveryText, driver, cash, name);
				steps.clickButton(home.CashonDeliveryradio, driver, name);
				String Travbuynow2 = driver.findElement(home.PopUpSaveMsg).getText();
				System.out.println("Displayed message:   "+Travbuynow2);
				test.log(Status.INFO, MarkupHelper.createLabel("Displayed message:   "+Travbuynow2, ExtentColor.BLUE));
				Thread.sleep(1000);
				/*	buynow3 = driver.findElement(home.PopUpMsg).getAttribute("value");
			System.out.println("Displayed message:   "+buynow3);
			test.log(Status.INFO, MarkupHelper.createLabel("Displayed message:   "+buynow3, ExtentColor.BLUE));
				 */
				String	 Travbuynow4 = getscreen.capture(driver, "Travbuynow4");
				test.addScreenCaptureFromPath(Travbuynow4);
				steps.clickButton(home.Cashondelivery, driver, name);
				test.log(Status.INFO, MarkupHelper.createLabel("User clicked on Cash on Delivery link", ExtentColor.BLUE));
			}
			catch(Exception e)
			{
				System.out.println("Cash on Delivery is not available");
			}
			Thread.sleep(1000);
			steps.clickButton(home.CartIcon, driver, name);
			steps.clickButton(home.Trend_Addcart_remove, driver, name);
			test.log(Status.INFO, MarkupHelper.createLabel("User Completed transactions of 1st Image in Photography", ExtentColor.PURPLE));
			driver.close();
			driver.switchTo().window(Parent);
			Thread.sleep(1000);
			Travellingstatus = passstatus;
			Assert.assertTrue(true);
			System.out.println("Travelling Who Inpires You Most Successful");
			testresultdata.put(Integer.toString(cnt), new Object[] {name, "Travelling under Who Inpires You Most", "Travelling under Who Inpires You Most Successful",passstatus});
		}
		catch(Exception e)
		{
			System.out.println("Travelling under Who Inpires You Most Unsuccessful");
			testresultdata.put(Integer.toString(cnt), new Object[] {name, "Travelling under Who Inpires You Most", "Travelling under Who Inpires You Most Unsuccessful",failstatus});
			Travellingstatus = failstatus;
			String Travellingfail = getscreen.capture(driver, "Travellingfail");
			test.addScreenCaptureFromPath(Travellingfail);
			driver.close();
			Assert.assertTrue(false);
		}
	}
	@Test(priority=2)
	public void MusicLover() throws Exception
	{
		String name = new Object(){}.getClass().getEnclosingMethod().getName();
		cnt ++;
		try
		{
			test = extent.createTest("Music Lover under What Inspires You Most");
			System.out.println("Music Lover under What Inspires You Most Test Case Executing...");
			Thread.sleep(1000);
			commonmethods.URLNaviagtion(driver);
			Thread.sleep(1000);
			/*			commonmethods.Login(driver);
			test.log(Status.INFO, MarkupHelper.createLabel("User able to Login", ExtentColor.BLUE));
			Thread.sleep(2000);
			 */
			steps.clickButton(home.Mydreamstore, driver, name);
			String Parent = driver.getWindowHandle();
			Thread.sleep(2000);
			steps.MoveElement(home2.WIM_Image3, driver, name);
			steps.clickButton(home2.WIM_Image3, driver, name);
			test.log(Status.INFO, MarkupHelper.createLabel("User clicked on Music Lover Category in Who Inspires You Most", ExtentColor.BLUE));
			Thread.sleep(2000);
			steps.ChildWindow(driver);
			driver.manage().window().maximize();
			test.log(Status.INFO, MarkupHelper.createLabel("User navigated to Next Window", ExtentColor.BLUE));
			MusicLoverProducts = getscreen.capture(driver, "MusicLoverProducts");
			test.addScreenCaptureFromPath(MusicLoverProducts);
			try
			{
				String Filterpage3 = driver.findElement(home.Filterpagetext).getText();
				System.out.println("User navigated to :   "+Filterpage3);
				steps.clickButton(home2.Photography_Motherday, driver, name);
				test.log(Status.INFO, MarkupHelper.createLabel("User selected on Mothers Day Category", ExtentColor.BLUE));
				try
				{
					MusicLoverNoCampaignMsg1 = driver.findElement(home2.Photography_NoCampaignFound).getText();
					System.out.println("Campaigns Not Found under Mother's day Category");
					test.log(Status.INFO, MarkupHelper.createLabel("User hasn't found Active Campaigns under Mothers Day Category", ExtentColor.BLUE));
					MusicLoverMothersDayProducts1 = getscreen.capture(driver, "MusicLoverMothersDayProducts1");
					test.addScreenCaptureFromPath(MusicLoverMothersDayProducts1);
					steps.clickButton(home2.Photography_Motherday, driver, name);
				}
				catch(Exception e)
				{
					System.out.println("Campaigns Found");
					test.log(Status.INFO, MarkupHelper.createLabel("User found Active Campaigns under Mothers Day Category", ExtentColor.BLUE));
					steps.clickButton(home2.Photography_Motherday, driver, name);
				}
				steps.clickButton(home2.Photography_Bolly, driver, name);
				try
				{
					MusicLoverNoCampaignMsg2 = driver.findElement(home2.Photography_NoCampaignFound).getText();
					System.out.println("Campaigns Not Found under Bollywood/Movie Personalities Category");
					test.log(Status.INFO, MarkupHelper.createLabel("User hasn't found Active Campaigns under Bollywood/Movie Personalities Category", ExtentColor.BLUE));
					MusicLoverBollywoodProducts1 = getscreen.capture(driver, "MusicLoverBollywoodProducts1");
					test.addScreenCaptureFromPath(MusicLoverBollywoodProducts1);
					steps.clickButton(home2.Photography_Bolly, driver, name);
				}
				catch(Exception e)
				{
					System.out.println("Campaigns Found");
					test.log(Status.INFO, MarkupHelper.createLabel("User found Active Campaigns under Bollywood/Movie Personalities Category", ExtentColor.BLUE));
					steps.clickButton(home2.Photography_Bolly, driver, name);
				}
				steps.clickButton(home2.Photography_Sports, driver, name);
				try
				{
					MusicLoverNoCampaignMsg3 = driver.findElement(home2.Photography_NoCampaignFound).getText();
					System.out.println("Campaigns Not Found under Sports Category");
					test.log(Status.INFO, MarkupHelper.createLabel("User hasn't found Active Campaigns under Sports Category", ExtentColor.BLUE));
					MusicLoverSportsProducts1 = getscreen.capture(driver, "MusicLoverSportsProducts1");
					test.addScreenCaptureFromPath(MusicLoverSportsProducts1);
					steps.clickButton(home2.Photography_Sports, driver, name);
				}
				catch(Exception e)
				{
					System.out.println("Campaigns Found");
					test.log(Status.INFO, MarkupHelper.createLabel("User found Active Campaigns under Sports Category", ExtentColor.BLUE));
					steps.clickButton(home2.Photography_Sports, driver, name);
				}
				Thread.sleep(2000);
				steps.selectDropdown(home2.Photography_SortDD, driver, strDataFileName, "MusicLoverSortBy1", name);
				test.log(Status.INFO, MarkupHelper.createLabel("User sort out New Arrival products in Photography", ExtentColor.BLUE));
				Thread.sleep(3000);
				steps.selectDropdown(home2.Photography_SortDD, driver, strDataFileName, "MusicLoverSortBy2", name);
				test.log(Status.INFO, MarkupHelper.createLabel("User sort out Best Selling products in Music Lover", ExtentColor.BLUE));
				steps.ScrollUp(driver);
				driver.navigate().refresh();
				Thread.sleep(2000);
				steps.MoveElement(home2.MusicLover_Image1, driver, name);
				steps.clickJSButton(home2.MusicLover_Image1, driver, name);
				MusicLoverimage1 = getscreen.capture(driver, "MusicLoverimage1");
				test.addScreenCaptureFromPath(MusicLoverimage1);
				test.log(Status.INFO, MarkupHelper.createLabel("User selected First Image", ExtentColor.BLUE));
			}
			catch(Exception e )
			{
				System.out.println("User navigated to wrong page");
			}
			try
			{
				steps.clickButton(home2.Photography_Image1_Readmore, driver, name);
				MusicLoverDescription = driver.findElement(home2.Travelling__Description).getText();
				System.out.println("Product Description:  "+MusicLoverDescription);
				test.log(Status.INFO, MarkupHelper.createLabel("Product Description:  "+MusicLoverDescription, ExtentColor.BROWN));
				steps.ScrollDown(driver);
				Thread.sleep(3000);
				steps.MoveElement(home2.Photography_Image1_Readless, driver, name);
				steps.clickJSButton(home2.Photography_Image1_Readless, driver, name);
				steps.ScrollUp(driver);
			}
			catch(Exception e)
			{
				steps.clickButton(home2.Photography_Image1_Readmore, driver, name);
				MusicLoverDescription = driver.findElement(home2.Travelling__Description1).getText();
				System.out.println("Product Description:  "+MusicLoverDescription);
				test.log(Status.INFO, MarkupHelper.createLabel("Product Description:  "+MusicLoverDescription, ExtentColor.BROWN));
				steps.clickJSButton(home2.Photography_Image1_Readless, driver, name);
			}
			MusicLoverMRP = driver.findElement(home2.Photography_Image1_MRP).getText();
			System.out.println("Product MRP:  "+MusicLoverMRP);
			test.log(Status.INFO, MarkupHelper.createLabel("Product MRP:  "+MusicLoverMRP, ExtentColor.ORANGE));
			try
			{
				String Musiccol1 = driver.findElement(home2.color1).getCssValue("color");
				System.out.println("Color 2 exists:  "+Musiccol1);
				steps.clickButton(home2.color1, driver, name);
				Thread.sleep(2000);
				MusicLoverPhotoColor1 = driver.findElement(home2.color1).getCssValue("background-color");
				MusicLoverPhotohex1 = Color.fromString(MusicLoverPhotoColor1).asHex();
				System.out.println("Image color: "+MusicLoverPhotohex1);
				test.log(Status.INFO, MarkupHelper.createLabel("User changed item color to:  "+MusicLoverPhotohex1, ExtentColor.BLUE));
				MusicLoverFirstcolor = getscreen.capture(driver, "MusicLoverFirstcolor");
				test.addScreenCaptureFromPath(MusicLoverFirstcolor);
			}
			catch(Exception e)
			{
				System.out.println("Color 1 is not available");
			}
			try
			{
				String Musiccol2 = driver.findElement(home2.color2).getCssValue("color");
				System.out.println("Color 2 exists:  "+Musiccol2);
				steps.clickButton(home2.color2, driver, name);
				Thread.sleep(1000);
				MusicLoverColor2 = driver.findElement(home2.color2).getCssValue("background-color");
				Travellinghex2 = Color.fromString(MusicLoverColor2).asHex();
				System.out.println("Image color: "+MusicLoverhex2);
				test.log(Status.INFO, MarkupHelper.createLabel("User changed item color to:  "+MusicLoverhex2, ExtentColor.BLUE));
				MusicLoverSecondcolor = getscreen.capture(driver, "MusicLoverSecondcolor");
				test.addScreenCaptureFromPath(MusicLoverSecondcolor);
			}
			catch(Exception e)
			{
				System.out.println("Color 2 is not available");
			}
			try
			{
				String Musiccol3 = driver.findElement(home2.color3).getCssValue("color");
				System.out.println("Color 2 exists:  "+Musiccol3);
				steps.clickButton(home2.color3, driver, name);
				Thread.sleep(1000);
				MusicLoverColor3 = driver.findElement(home2.color3).getCssValue("background-color");
				MusicLoverhex3 = Color.fromString(MusicLoverColor3).asHex();
				System.out.println("Image color: "+MusicLoverhex3);
				test.log(Status.INFO, MarkupHelper.createLabel("User changed item color to:  "+MusicLoverhex3, ExtentColor.BLUE));
				MusicLoverThirdcolor = getscreen.capture(driver, "MusicLoverThirdcolor");
				test.addScreenCaptureFromPath(MusicLoverThirdcolor);
			}
			catch(Exception e)
			{
				System.out.println("Color 3 is not available");
			}
			try
			{
				String Musiccol4 = driver.findElement(home2.color4).getCssValue("color");
				System.out.println("Color 2 exists:  "+Musiccol4);
				steps.clickButton(home2.color4, driver, name);
				Thread.sleep(1000);
				MusicLoverColor4 = driver.findElement(home2.color4).getCssValue("background-color");
				MusicLoverhex4 = Color.fromString(MusicLoverColor4).asHex();
				System.out.println("Image color: "+MusicLoverhex4);
				test.log(Status.INFO, MarkupHelper.createLabel("User changed item color to:  "+MusicLoverhex4, ExtentColor.BLUE));
				MusicLoverFourthcolor = getscreen.capture(driver, "MusicLoverFourthcolor");
				test.addScreenCaptureFromPath(MusicLoverFourthcolor);
			}
			catch(Exception e)
			{
				System.out.println("Color 4 is not available");
			}
			MusicLovercqty = driver.findElement(home2.Qty).getAttribute("value");
			System.out.println("Current Quantity:  "+ MusicLovercqty);
			test.log(Status.INFO, MarkupHelper.createLabel("Current Quantity of product:  "+MusicLovercqty, ExtentColor.BLUE));
			Thread.sleep(1000);
			steps.clickButton(home2.QtyIncrease, driver, name);
			MusicLoveriqty = driver.findElement(home2.Qty).getAttribute("value");
			System.out.println("Quantity after increase:  "+ MusicLoveriqty);
			test.log(Status.INFO, MarkupHelper.createLabel("Current Quantity of product after Increase:  "+MusicLoveriqty, ExtentColor.BLUE));
			steps.clickButton(home.Trend_QtyDecrease, driver, name);
			MusicLoverdqty = driver.findElement(home2.Qty).getAttribute("value");
			System.out.println("Quantity after decrease:  "+ MusicLoverdqty);
			test.log(Status.INFO, MarkupHelper.createLabel("Current Quantity of product after increase:  "+MusicLoverdqty, ExtentColor.BLUE));
			Thread.sleep(2000);
			steps.clickButton(home2.Addcart, driver, name);
			MusicLoverSizemsg = driver.findElement(home2.Sizeerrormsg).getText();
			System.out.println("User clicked on Addcart before Size selection:  "+MusicLoverSizemsg);
			test.log(Status.INFO, MarkupHelper.createLabel("User clicked on Addcart before Size selection:  "+MusicLoverSizemsg, ExtentColor.RED));
			Thread.sleep(1000);
			steps.clickJSButton(home.Size_S, driver, name);
			MusicLoversizeS2 = driver.findElement(home.Size_S).getText();
			System.out.println("Selected Size:  "+MusicLoversizeS2);
			test.log(Status.INFO, MarkupHelper.createLabel("User selected size:  "+MusicLoversizeS2, ExtentColor.BLUE));
			steps.clickButton(home.Size_M, driver, name);
			MusicLoversizeM2 = driver.findElement(home.Size_M).getText();
			System.out.println("Selected Size:  "+MusicLoversizeM2);
			test.log(Status.INFO, MarkupHelper.createLabel("User selected size:  "+MusicLoversizeM2, ExtentColor.BLUE));
			steps.clickButton(home.Size_L, driver, name);
			MusicLoversizeL2 = driver.findElement(home.Size_L).getText();
			System.out.println("Selected Size:  "+MusicLoversizeL2);
			test.log(Status.INFO, MarkupHelper.createLabel("User selected size:  "+MusicLoversizeL2, ExtentColor.BLUE));
			steps.clickButton(home.Size_XL, driver, name);
			MusicLoversizeXL2 = driver.findElement(home.Size_XL).getText();
			System.out.println("Selected Size:  "+MusicLoversizeXL2);
			test.log(Status.INFO, MarkupHelper.createLabel("User selected size:  "+MusicLoversizeXL2, ExtentColor.BLUE));
			steps.clickButton(home.Size_XXL, driver, name);
			MusicLoversizeXXL2 = driver.findElement(home.Size_XXL).getText();
			System.out.println("Selected Size:  "+MusicLoversizeXXL2);
			test.log(Status.INFO, MarkupHelper.createLabel("User selected size:  "+MusicLoversizeXXL2, ExtentColor.BLUE));
			steps.clickButton(home.Size_XXXL, driver, name);
			MusicLoversizeXXXL2 = driver.findElement(home.Size_XXXL).getText();
			System.out.println("Selected Size:  "+MusicLoversizeXXXL2);
			test.log(Status.INFO, MarkupHelper.createLabel("User selected size:  "+MusicLoversizeXXXL2, ExtentColor.BLUE));
			Thread.sleep(1000);
			steps.clickButton(home2.FindUrSize, driver, name);
			test.log(Status.INFO, MarkupHelper.createLabel("User clicked on Find your size", ExtentColor.BLUE));
			Thread.sleep(1000);
			try
			{
				String R510 = driver.findElement(home.Trend_FindUrSize_510radio).getText();
				System.out.println(R510);
				steps.clickButton(home.Trend_FindUrSize_510radio, driver, name);
				MusicLoverbelow510 = driver.findElement(home.Trend_FindUrSize_510radio).getText();
				System.out.println("User Selected:  "+MusicLoverbelow510);
				test.log(Status.INFO, MarkupHelper.createLabel("User selected Height:  "+MusicLoverbelow510, ExtentColor.BLUE));
				steps.clickButton(home.Trend_FindUrSize_above510radio, driver, name);
				MusicLoverabove510 = driver.findElement(home.Trend_FindUrSize_above510radio).getText();
				System.out.println("User Selected:  "+MusicLoverabove510);
				test.log(Status.INFO, MarkupHelper.createLabel("User selected Height:  "+MusicLoverabove510, ExtentColor.BLUE));
				steps.clickButton(home.Trend_FindUrSize_Tightradio, driver, name);
				MusicLovertightfit = driver.findElement(home.Trend_FindUrSize_Tightradio).getText();
				System.out.println("User Selected:  "+MusicLovertightfit);
				test.log(Status.INFO, MarkupHelper.createLabel("User selected Fit:  "+MusicLovertightfit, ExtentColor.BLUE));
				steps.clickButton(home.Trend_FindUrSize_Comfortradio, driver, name);
				MusicLovercomfit = driver.findElement(home.Trend_FindUrSize_Comfortradio).getText();
				System.out.println("User Selected:  "+MusicLovercomfit);
				test.log(Status.INFO, MarkupHelper.createLabel("User selected Fit:  "+MusicLovercomfit, ExtentColor.BLUE));
				steps.selectDropdown(home2.FindUrSize_sizeDD, driver, strDataFileName, "size", name);
				test.log(Status.INFO, MarkupHelper.createLabel("User selected shirt size", ExtentColor.BLUE));
				steps.clickButton(home2.FindUrSize_submit, driver, name);
				test.log(Status.INFO, MarkupHelper.createLabel("User clicked on Submit", ExtentColor.BLUE));
				MusicLoverfindursize1 = getscreen.capture(driver, "MusicLoverfindursize1");
				test.addScreenCaptureFromPath(MusicLoverfindursize1);
				Thread.sleep(1000);
				steps.clickButton(home2.FindUrSize_Okbutton, driver, name);
				test.log(Status.INFO, MarkupHelper.createLabel("system displayed size for given metrics", ExtentColor.BLUE));
				steps.clickButton(home2.FindUrSize_closebutton, driver, name);
				test.log(Status.INFO, MarkupHelper.createLabel("User navigate to Product details page", ExtentColor.BLUE));
			}
			catch(Exception e)
			{
				steps.clickButton(home2.FindUrSize_closebutton, driver, name);
				test.log(Status.INFO, MarkupHelper.createLabel("User navigate to Product details page", ExtentColor.BLUE));
			}
			steps.clickButton(home2.Size_L, driver, name);
			test.log(Status.INFO, MarkupHelper.createLabel("User selected size", ExtentColor.BLUE));
			steps.clickButton(home2.Addcart, driver, name);
			test.log(Status.INFO, MarkupHelper.createLabel("User added product to Cart", ExtentColor.BLUE));
			MusicLoveraddcart1 = getscreen.capture(driver, "MusicLoveraddcart1");
			test.addScreenCaptureFromPath(MusicLoveraddcart1);
			Thread.sleep(2000);
			steps.clickButton(home2.MusicLoverAddcart_remove, driver, name);
			MusicLoverremovecart1 = getscreen.capture(driver, "MusicLoverremovecart1");
			test.addScreenCaptureFromPath(MusicLoverremovecart1);
			test.log(Status.INFO, MarkupHelper.createLabel("User removed product from Cart", ExtentColor.BLUE));
			steps.clickButton(home.Size_L, driver, name);
			test.log(Status.INFO, MarkupHelper.createLabel("User selected size", ExtentColor.BLUE));
			steps.clickButton(home.BuyNow, driver, name);
			test.log(Status.INFO, MarkupHelper.createLabel("User clicked on BuyNow, navigated to checkout page", ExtentColor.BLUE));
			String Musicbuynow1 = getscreen.capture(driver, "Musicbuynow1");
			test.addScreenCaptureFromPath(Musicbuynow1);
			try
			{
				String cash = driver.findElement(home.CashonDeliveryText).getText();
				System.out.println("Payment method:  "+cash);
				steps.Verifytext(home.CashonDeliveryText, driver, cash, name);
				steps.clickButton(home.CashonDeliveryradio, driver, name);
				String Musicbuynow2 = driver.findElement(home.PopUpSaveMsg).getText();
				System.out.println("Displayed message:   "+Musicbuynow2);
				test.log(Status.INFO, MarkupHelper.createLabel("Displayed message:   "+Musicbuynow2, ExtentColor.BLUE));
				Thread.sleep(1000);
				/*	buynow3 = driver.findElement(home.PopUpMsg).getAttribute("value");
			System.out.println("Displayed message:   "+buynow3);
			test.log(Status.INFO, MarkupHelper.createLabel("Displayed message:   "+buynow3, ExtentColor.BLUE));
				 */
				String	Musicbuynow4 = getscreen.capture(driver, "Musicbuynow4");
				test.addScreenCaptureFromPath(Musicbuynow4);
				steps.clickButton(home.Cashondelivery, driver, name);
				test.log(Status.INFO, MarkupHelper.createLabel("User clicked on Cash on Delivery link", ExtentColor.BLUE));
			}
			catch(Exception e)
			{
				System.out.println("Cash on Delivery is not available");
			}
			Thread.sleep(1000);
			steps.clickButton(home.CartIcon, driver, name);
			steps.clickButton(home.Trend_Addcart_remove, driver, name);
			test.log(Status.INFO, MarkupHelper.createLabel("User Completed transactions of 1st Image in Photography", ExtentColor.PURPLE));
			driver.close();
			driver.switchTo().window(Parent);
			Thread.sleep(1000);
			MusicLoverstatus = passstatus;
			Assert.assertTrue(true);
			System.out.println("MusicLover Who Inpires You Most Successful");
			testresultdata.put(Integer.toString(cnt), new Object[] {name, "MusicLover under Who Inpires You Most", "MusicLover under Who Inpires You Most Successful",passstatus});
		}
		catch(Exception e)
		{
			System.out.println("MusicLover under Who Inpires You Most Unsuccessful");
			testresultdata.put(Integer.toString(cnt), new Object[] {name, "MusicLover under Who Inpires You Most", "MusicLover under Who Inpires You Most Unsuccessful",failstatus});
			MusicLoverstatus = failstatus;
			String MusicLoverfail = getscreen.capture(driver, "MusicLoverfail");
			test.addScreenCaptureFromPath(MusicLoverfail);
			driver.close();
			Assert.assertTrue(false);
		}
	}
	@Test(priority=3)
	public void FitnessFreak() throws IOException, InterruptedException
	{
		String name = new Object(){}.getClass().getEnclosingMethod().getName();
		cnt ++;
		try 
		{
			test = extent.createTest("Fitness Freak under What Inspires You Most");
			System.out.println("Fitness Freak under What Inspires You Most Test Case Executing...");
			Thread.sleep(2000);
			commonmethods.URLNaviagtion(driver);
			Thread.sleep(1000);
			/*			commonmethods.Login(driver);
			test.log(Status.INFO, MarkupHelper.createLabel("User able to Login", ExtentColor.BLUE));
			Thread.sleep(1000);
			 */
			steps.clickButton(home.Mydreamstore, driver, name);
			String homepage = getscreen.capture(driver, name);
			test.addScreenCaptureFromPath(homepage);
			String Parent = driver.getWindowHandle();
			steps.MoveElement(home2.WIM_Image4, driver, name);
			steps.clickJSButton(home2.WIM_Image4, driver, name);
			test.log(Status.INFO, MarkupHelper.createLabel("User clicked on Fitness Freak Image under Who Inspires You Most", ExtentColor.BLUE));
			steps.ChildWindow(driver);
			driver.manage().window().maximize();
			FitnessFreakPage = getscreen.capture(driver, "FitnessFreakPage");
			test.addScreenCaptureFromPath(FitnessFreakPage);
			try
			{
				String Filterpage1 = driver.findElement(home.Filterpagetext).getText();
				System.out.println("User navigated to :   "+Filterpage1);
			}
			catch(Exception e)
			{
				System.out.println("User navigated to different landing page");
				steps.clickButton(home2.Fitness_Image1, driver, name);
				test.log(Status.INFO, MarkupHelper.createLabel("User clicked on 1st Image of Fitness Freak", ExtentColor.BLUE));
				driver.close();
				steps.ChildWindow(driver);
				driver.manage().window().maximize();
			}
			Thread.sleep(1000);
			try
			{
				steps.clickButton(home.Trend_Image_Readmore, driver, name);
				test.log(Status.INFO, MarkupHelper.createLabel("User clicked on Readmore", ExtentColor.BLUE));
				Fitnessproductdetails = getscreen.capture(driver, "Fitnessproductdetails");
				test.addScreenCaptureFromPath(Fitnessproductdetails);
				FitnessDescription = driver.findElement(home.Trend_Image_Description).getText();
				System.out.println("Production Description   "+FitnessDescription);
				test.log(Status.INFO, MarkupHelper.createLabel("Description about Item:  "+FitnessDescription, ExtentColor.BLUE));
				steps.clickButton(home.Trend_Image_Readless, driver, name);
			}
			catch(Exception e)
			{
				steps.clickButton(home.Trend_Image_Readmore, driver, name);
				FitnessDescription = driver.findElement(home.Trend_Image_Description1).getText();
				System.out.println("Production Description   "+FitnessDescription);
				test.log(Status.INFO, MarkupHelper.createLabel("Description about Item:  "+FitnessDescription, ExtentColor.BLUE));
				steps.clickButton(home.Trend_Image_Readless, driver, name);
			}
			Thread.sleep(1000);
			FitnessMRP = driver.findElement(home.Trend_Image_MRP).getText();
			System.out.println("Item Price   "+FitnessMRP);
			test.log(Status.INFO, MarkupHelper.createLabel("MRP of an Product:  "+FitnessMRP, ExtentColor.ORANGE));
			try
			{
				String col1 = driver.findElement(home2.color1).getCssValue("color");
				System.out.println("Color 2 exists:  "+col1);
				steps.clickButton(home.Trend_color1, driver, name);
				Thread.sleep(2000);
				FitnessColor1 = driver.findElement(home.Trend_color1).getCssValue("background-color");
				Fitnesshex1 = Color.fromString(FitnessColor1).asHex();
				System.out.println("Image color: "+Fitnesshex1);
				test.log(Status.INFO, MarkupHelper.createLabel("User changed item color to:  "+Fitnesshex1, ExtentColor.BLUE));
				FitnessFirstcolor = getscreen.capture(driver, "FitnessFirstcolor");
				test.addScreenCaptureFromPath(FitnessFirstcolor);
			}
			catch(Exception e)
			{
				System.out.println("Color 2 is not available");
			}
			try
			{
				String col2 = driver.findElement(home2.color2).getCssValue("color");
				System.out.println("Color 3 exists:  "+col2);
				steps.clickButton(home.Trend_color2, driver, name);
				Thread.sleep(2000);
				FitnessColor2 = driver.findElement(home.Trend_color2).getCssValue("background-color");
				Fitnesshex2 = Color.fromString(FitnessColor2).asHex();
				System.out.println("Image color: "+Fitnesshex2);
				test.log(Status.INFO, MarkupHelper.createLabel("User changed item color to:  "+Fitnesshex2, ExtentColor.BLUE));
				FitnessSecondcolor = getscreen.capture(driver, "FitnessSecondcolor");
				test.addScreenCaptureFromPath(FitnessSecondcolor);
			}
			catch(Exception e)
			{
				System.out.println("Color 3 is not available");
			}
			try
			{
				String col3 = driver.findElement(home2.color3).getCssValue("color");
				System.out.println("Color 4 exists:  "+col3);
				steps.clickButton(home.Trend_color3, driver, name);
				Thread.sleep(2000);
				FitnessColor3 = driver.findElement(home.Trend_color3).getCssValue("background-color");
				Fitnesshex3 = Color.fromString(FitnessColor3).asHex();
				System.out.println("Image color: "+Fitnesshex3);
				test.log(Status.INFO, MarkupHelper.createLabel("User changed item color to:  "+Fitnesshex3, ExtentColor.BLUE));
				FitnessThirdcolor = getscreen.capture(driver, "FitnessThirdcolor");
				test.addScreenCaptureFromPath(FitnessThirdcolor);
			}
			catch(Exception e)
			{
				System.out.println("Color 4 is not available");
			}
			try
			{
				String col4 = driver.findElement(home.Trend_color5).getCssValue("color");
				System.out.println("Color 1 exists:  "+col4);
				steps.clickButton(home.Trend_color5, driver, name);
				Thread.sleep(2000);
				Fitnesscolor5 = driver.findElement(home.Trend_color5).getCssValue("background-color");
				Fitnesshex5 = Color.fromString(Fitnesscolor5).asHex();
				System.out.println("Image color: "+Fitnesshex5);
				test.log(Status.INFO, MarkupHelper.createLabel("User changed item color to:  "+Fitnesshex5, ExtentColor.BLUE));
				FitnessFifthcolor = getscreen.capture(driver, "FitnessFifthcolor");
				test.addScreenCaptureFromPath(FitnessFifthcolor);
			}
			catch(Exception e)
			{
				System.out.println("Color 1 is not available");
			}
			Fitnesscqty = driver.findElement(home.Qty).getAttribute("value");
			System.out.println("Current Quantity:  "+ Fitnesscqty);
			test.log(Status.INFO, MarkupHelper.createLabel("Current Quantity of product:  "+Fitnesscqty, ExtentColor.BLUE));
			Thread.sleep(1000);
			steps.clickButton(home.Trend_QtyIncrease, driver, name);
			Fitnessiqty = driver.findElement(home.Qty).getAttribute("value");
			System.out.println("Quantity after increase:  "+ Fitnessiqty);
			test.log(Status.INFO, MarkupHelper.createLabel("Current Quantity of product after Increase:  "+Fitnessiqty, ExtentColor.BLUE));
			steps.clickButton(home.Trend_QtyDecrease, driver, name);
			Fitnessdqty = driver.findElement(home.Qty).getAttribute("value");
			System.out.println("Quantity after decrease:  "+ Fitnessdqty);
			test.log(Status.INFO, MarkupHelper.createLabel("Current Quantity of product after increase:  "+Fitnessdqty, ExtentColor.BLUE));
			Thread.sleep(2000);
			steps.clickButton(home.Trend_Addcart, driver, name);
			FitnessSizemsg = driver.findElement(home.Trend_Sizeerrormsg).getText();
			System.out.println("User clicked on Addcart before Size selection:  "+FitnessSizemsg);
			test.log(Status.INFO, MarkupHelper.createLabel("User clicked on Addcart before Size selection:  "+FitnessSizemsg, ExtentColor.RED));
			steps.clickJSButton(home.Size_S, driver, name);
			FitnesssizeS = driver.findElement(home.Size_S).getText();
			System.out.println("Selected Size:  "+FitnesssizeS);
			test.log(Status.INFO, MarkupHelper.createLabel("User selected size:  "+FitnesssizeS, ExtentColor.BLUE));
			steps.clickButton(home.Size_M, driver, name);
			FitnesssizeM = driver.findElement(home.Size_M).getText();
			System.out.println("Selected Size:  "+FitnesssizeM);
			test.log(Status.INFO, MarkupHelper.createLabel("User selected size:  "+FitnesssizeM, ExtentColor.BLUE));
			steps.clickButton(home.Size_L, driver, name);
			FitnesssizeL = driver.findElement(home.Size_L).getText();
			System.out.println("Selected Size:  "+FitnesssizeL);
			test.log(Status.INFO, MarkupHelper.createLabel("User selected size:  "+FitnesssizeL, ExtentColor.BLUE));
			steps.clickButton(home.Size_XL, driver, name);
			FitnesssizeXL = driver.findElement(home.Size_XL).getText();
			System.out.println("Selected Size:  "+FitnesssizeXL);
			test.log(Status.INFO, MarkupHelper.createLabel("User selected size:  "+FitnesssizeXL, ExtentColor.BLUE));
			steps.clickButton(home.Size_XXL, driver, name);
			FitnesssizeXXL = driver.findElement(home.Size_XXL).getText();
			System.out.println("Selected Size:  "+FitnesssizeXXL);
			test.log(Status.INFO, MarkupHelper.createLabel("User selected size:  "+FitnesssizeXXL, ExtentColor.BLUE));
			steps.clickButton(home.Size_XXXL, driver, name);
			FitnesssizeXXXL = driver.findElement(home.Size_XXXL).getText();
			System.out.println("Selected Size:  "+FitnesssizeXXXL);
			test.log(Status.INFO, MarkupHelper.createLabel("User selected size:  "+FitnesssizeXXXL, ExtentColor.BLUE));
			steps.clickButton(home.Trend_FindUrSize, driver, name);
			test.log(Status.INFO, MarkupHelper.createLabel("User clicked on Find your size", ExtentColor.BLUE));
			Thread.sleep(1000);
			try
			{
				String R510 = driver.findElement(home.Trend_FindUrSize_510radio).getText();
				System.out.println(R510);
				steps.clickButton(home.Trend_FindUrSize_510radio, driver, name);
				Fitnessbelow510 = driver.findElement(home.Trend_FindUrSize_510radio).getText();
				System.out.println("User Selected:  "+Fitnessbelow510);
				test.log(Status.INFO, MarkupHelper.createLabel("User selected Height:  "+Fitnessbelow510, ExtentColor.BLUE));
				steps.clickButton(home.Trend_FindUrSize_above510radio, driver, name);
				Fitnessabove510 = driver.findElement(home.Trend_FindUrSize_above510radio).getText();
				System.out.println("User Selected:  "+Fitnessabove510);
				test.log(Status.INFO, MarkupHelper.createLabel("User selected Height:  "+Fitnessabove510, ExtentColor.BLUE));
				steps.clickButton(home.Trend_FindUrSize_Tightradio, driver, name);
				Fitnesstightfit = driver.findElement(home.Trend_FindUrSize_Tightradio).getText();
				System.out.println("User Selected:  "+Fitnesstightfit);
				test.log(Status.INFO, MarkupHelper.createLabel("User selected Fit:  "+Fitnesstightfit, ExtentColor.BLUE));
				steps.clickButton(home.Trend_FindUrSize_Comfortradio, driver, name);
				Fitnesscomfit = driver.findElement(home.Trend_FindUrSize_Comfortradio).getText();
				System.out.println("User Selected:  "+Fitnesscomfit);
				test.log(Status.INFO, MarkupHelper.createLabel("User selected Fit:  "+Fitnesscomfit, ExtentColor.BLUE));
				steps.selectDropdown(home.Trend_FindUrSize_sizeDD, driver, strDataFileName, "size", name);
				test.log(Status.INFO, MarkupHelper.createLabel("User selected shirt size", ExtentColor.BLUE));
				steps.clickButton(home.Trend_FindUrSize_submit, driver, name);
				test.log(Status.INFO, MarkupHelper.createLabel("User clicked on Submit", ExtentColor.BLUE));
				Fitnessfindursize = getscreen.capture(driver, "Fitnessfindursize");
				test.addScreenCaptureFromPath(Fitnessfindursize);
				Thread.sleep(1000);
				steps.clickButton(home.Trend_FindUrSize_Okbutton, driver, name);
				test.log(Status.INFO, MarkupHelper.createLabel("system displayed size for given metrics", ExtentColor.BLUE));
				steps.clickButton(home.Trend_FindUrSize_closebutton, driver, name);
				test.log(Status.INFO, MarkupHelper.createLabel("User navigate to Product details page", ExtentColor.BLUE));
			}
			catch(Exception e)
			{
				steps.clickButton(home2.FindUrSize_closebutton, driver, name);
				test.log(Status.INFO, MarkupHelper.createLabel("User navigate to Product details page", ExtentColor.BLUE));
			}
			steps.clickButton(home.Size_L, driver, name);
			test.log(Status.INFO, MarkupHelper.createLabel("User selected size", ExtentColor.BLUE));
			steps.clickButton(home.Trend_Addcart, driver, name);
			test.log(Status.INFO, MarkupHelper.createLabel("User added product to Cart", ExtentColor.BLUE));
			Fitnessaddcart = getscreen.capture(driver, "Fitnessaddcart");
			test.addScreenCaptureFromPath(Fitnessaddcart);
			try
			{
				String edit = driver.findElement(home2.Addcart_edit).getText();
				System.out.println(edit);
				steps.clickButton(home.Trend_Addcart_edit, driver, name);
				test.log(Status.INFO, MarkupHelper.createLabel("User clicked on Edit", ExtentColor.BLUE));
				steps.selectDropdown(home.Trend_Addcart_selectsize, driver, strDataFileName, "SelectSize", name);
				steps.selectDropdown(home.Trend_Addcart_selectqty, driver, strDataFileName, "Quantity", name);
				test.log(Status.INFO, MarkupHelper.createLabel("User selected Size and Quantity", ExtentColor.BLUE));
				steps.clickButton(home.Trend_Addcart_cancel, driver, name);
				steps.clickButton(home.Trend_Addcart_edit, driver, name);
				steps.selectDropdown(home.Trend_Addcart_selectsize, driver, strDataFileName, "SelectSize", name);
				steps.selectDropdown(home.Trend_Addcart_selectqty, driver, strDataFileName, "Quantity", name);
				steps.clickButton(home.Trend_Addcart_save, driver, name);
				Fitnessupdateproduct = getscreen.capture(driver, "Fitnessupdateproduct");
				test.addScreenCaptureFromPath(Fitnessupdateproduct);
				test.log(Status.INFO, MarkupHelper.createLabel("User updated product size and quantity", ExtentColor.BLUE));
			}
			catch(Exception e)
			{
				System.out.println("Edit feature is not available for this product");
			}
			steps.clickButton(home.Trend_Addcart_remove, driver, name);
			Fitnessremovecart = getscreen.capture(driver, "Fitnessfindursize");
			test.addScreenCaptureFromPath(Fitnessremovecart);
			test.log(Status.INFO, MarkupHelper.createLabel("User removed product from Cart", ExtentColor.BLUE));
			steps.clickButton(home.Size_L, driver, name);
			test.log(Status.INFO, MarkupHelper.createLabel("User selected size", ExtentColor.BLUE));
			steps.clickButton(home.BuyNow, driver, name);
			test.log(Status.INFO, MarkupHelper.createLabel("User clicked on BuyNow, navigated to checkout page", ExtentColor.BLUE));
			String Fitbuynow1 = getscreen.capture(driver, "Fitbuynow1");
			test.addScreenCaptureFromPath(Fitbuynow1);
			try
			{
				String cash = driver.findElement(home.CashonDeliveryText).getText();
				System.out.println("Payment method:  "+cash);
				steps.Verifytext(home.CashonDeliveryText, driver, cash, name);
				steps.clickButton(home.CashonDeliveryradio, driver, name);
				String Fitbuynow2 = driver.findElement(home.PopUpSaveMsg).getText();
				System.out.println("Displayed message:   "+Fitbuynow2);
				test.log(Status.INFO, MarkupHelper.createLabel("Displayed message:   "+Fitbuynow2, ExtentColor.BLUE));
				Thread.sleep(1000);
				/*	buynow3 = driver.findElement(home.PopUpMsg).getAttribute("value");
			System.out.println("Displayed message:   "+buynow3);
			test.log(Status.INFO, MarkupHelper.createLabel("Displayed message:   "+buynow3, ExtentColor.BLUE));
				 */
				String	 Fitbuynow4 = getscreen.capture(driver, "Fitbuynow4");
				test.addScreenCaptureFromPath(Fitbuynow4);
				steps.clickButton(home.Cashondelivery, driver, name);
				test.log(Status.INFO, MarkupHelper.createLabel("User clicked on Cash on Delivery link", ExtentColor.BLUE));
			}
			catch(Exception e)
			{
				System.out.println("Cash on Delivery is not available");
			}
			Thread.sleep(1000);
			steps.clickButton(home.CartIcon, driver, name);
			steps.clickButton(home.Trend_Addcart_remove, driver, name);
			test.log(Status.INFO, MarkupHelper.createLabel("User Completed transactions of 1st Image in New Arrivals", ExtentColor.PURPLE));
			driver.close();
			driver.switchTo().window(Parent);
			Fitnessstatus = passstatus;
			Assert.assertTrue(true);
			System.out.println("Fitness Freak Successful");
			testresultdata.put(Integer.toString(cnt), new Object[] {name, "Fitness Freak", "Fitness Freak Successful",passstatus});	
		}
		catch(Exception e)
		{
			System.out.println("Fitness Freak Unsuccessful");
			testresultdata.put(Integer.toString(cnt), new Object[] {name, "Fitness Freak", "Fitness Freak Unsuccessful",failstatus});
			Fitnessstatus = failstatus;
			Fitnessstatusfail = getscreen.capture(driver, "Fitnessstatusfail");
			test.addScreenCaptureFromPath(Fitnessstatusfail);
			driver.close();
			Assert.assertTrue(false);
		}
	}
	@Test(priority = 4)
	public void Foodie() throws IOException, InterruptedException
	{
		String name = new Object(){}.getClass().getEnclosingMethod().getName();
		cnt ++;
		try 
		{
			test = extent.createTest("Foodie under What Inspires You Most");
			System.out.println("Foodie under What Inspires You Most Test Case Executing...");
			Thread.sleep(1000);
			commonmethods.URLNaviagtion(driver);
			Thread.sleep(1000);
			/*			commonmethods.Login(driver);
			test.log(Status.INFO, MarkupHelper.createLabel("User able to Login", ExtentColor.BLUE));
			Thread.sleep(1000);
			 */
			steps.clickButton(home.Mydreamstore, driver, name);
			String homepage = getscreen.capture(driver, name);
			test.addScreenCaptureFromPath(homepage);
			String Parent = driver.getWindowHandle();
			steps.MoveElement(home2.WIM_Image5, driver, name);
			steps.clickJSButton(home2.WIM_Image5, driver, name);
			test.log(Status.INFO, MarkupHelper.createLabel("User clicked on Foodie Image under Who Inspires You Most", ExtentColor.BLUE));
			steps.ChildWindow(driver);
			driver.manage().window().maximize();
			FoodieFreakPage = getscreen.capture(driver, "FoodieFreakPage");
			test.addScreenCaptureFromPath(FoodieFreakPage);
			Thread.sleep(2000);
			try
			{
				String Filterpage1 = driver.findElement(home.Filterpagetext).getText();
				System.out.println("User navigated to :   "+Filterpage1);
				steps.clickButton(home2.Photography_Motherday, driver, name);
				test.log(Status.INFO, MarkupHelper.createLabel("User selected on Mothers Day Category", ExtentColor.BLUE));
				try
				{
					FoodieNoCampaignMsg1 = driver.findElement(home2.Photography_NoCampaignFound).getText();
					System.out.println("Campaigns Not Found under Mother's day Category");
					test.log(Status.INFO, MarkupHelper.createLabel("User hasn't found Active Campaigns under Mothers Day Category", ExtentColor.BLUE));
					FoodieMothersDayProducts1 = getscreen.capture(driver, "FoodieMothersDayProducts1");
					test.addScreenCaptureFromPath(FoodieMothersDayProducts1);
					steps.clickButton(home2.Photography_Motherday, driver, name);
				}
				catch(Exception e)
				{
					System.out.println("Campaigns Found");
					test.log(Status.INFO, MarkupHelper.createLabel("User found Active Campaigns under Mothers Day Category", ExtentColor.BLUE));
					steps.clickButton(home2.Photography_Motherday, driver, name);
				}
				steps.clickButton(home2.Photography_Bolly, driver, name);
				try
				{
					FoodieNoCampaignMsg2 = driver.findElement(home2.Photography_NoCampaignFound).getText();
					System.out.println("Campaigns Not Found under Bollywood/Movie Personalities Category");
					test.log(Status.INFO, MarkupHelper.createLabel("User hasn't found Active Campaigns under Bollywood/Movie Personalities Category", ExtentColor.BLUE));
					FoodieBollywoodProducts1 = getscreen.capture(driver, "FoodieBollywoodProducts1");
					test.addScreenCaptureFromPath(FoodieBollywoodProducts1);
					steps.clickButton(home2.Photography_Bolly, driver, name);
				}
				catch(Exception e)
				{
					System.out.println("Campaigns Found");
					test.log(Status.INFO, MarkupHelper.createLabel("User found Active Campaigns under Bollywood/Movie Personalities Category", ExtentColor.BLUE));
					steps.clickButton(home2.Photography_Bolly, driver, name);
				}
				steps.clickButton(home2.Photography_Sports, driver, name);
				try
				{
					FoodieNoCampaignMsg3 = driver.findElement(home2.Photography_NoCampaignFound).getText();
					System.out.println("Campaigns Not Found under Sports Category");
					test.log(Status.INFO, MarkupHelper.createLabel("User hasn't found Active Campaigns under Sports Category", ExtentColor.BLUE));
					FoodieSportsProducts1 = getscreen.capture(driver, "FoodieSportsProducts1");
					test.addScreenCaptureFromPath(FoodieSportsProducts1);
					steps.clickButton(home2.Photography_Sports, driver, name);
				}
				catch(Exception e)
				{
					System.out.println("Campaigns Found");
					test.log(Status.INFO, MarkupHelper.createLabel("User found Active Campaigns under Sports Category", ExtentColor.BLUE));
					steps.clickButton(home2.Photography_Sports, driver, name);
				}
				Thread.sleep(2000);
				steps.selectDropdown(home2.Photography_SortDD, driver, strDataFileName, "FoodieSortBy1", name);
				test.log(Status.INFO, MarkupHelper.createLabel("User sort out New Arrival products in Photography", ExtentColor.BLUE));
				Thread.sleep(3000);
				steps.selectDropdown(home2.Photography_SortDD, driver, strDataFileName, "FoodieSortBy2", name);
				test.log(Status.INFO, MarkupHelper.createLabel("User sort out Best Selling products in Music Lover", ExtentColor.BLUE));
				steps.ScrollUp(driver);
				Thread.sleep(2000);
				steps.MoveElement(home2.Foodie_Image1, driver, name);
				steps.clickJSButton(home2.Foodie_Image1, driver, name);
				test.log(Status.INFO, MarkupHelper.createLabel("User clicked on 1st Image of Foodie", ExtentColor.BLUE));
			}
			catch(Exception e)
			{
				System.out.println("User navigated to different landing page");
			}
			Thread.sleep(1000);
			try
			{
				steps.clickJSButton(home2.Photography_Image1_Readmore, driver, name);
				FoodieDescription = driver.findElement(home2.Foodie_Description).getText();
				System.out.println("Production Description   "+FoodieDescription);
				test.log(Status.INFO, MarkupHelper.createLabel("Description about Item:  "+FoodieDescription, ExtentColor.BLUE));
				steps.clickJSButton(home2.Photography_Image1_Readless, driver, name);
			}
			catch(Exception e)
			{
				steps.clickJSButton(home2.Photography_Image1_Readmore, driver, name);
				FoodieDescription1 = driver.findElement(home2.Foodie_Description1).getText();
				System.out.println("Production Description   "+FoodieDescription1);
				test.log(Status.INFO, MarkupHelper.createLabel("Description about Item:  "+FoodieDescription1, ExtentColor.BLUE));
			}
			FoodieMRP = driver.findElement(home.Trend_Image_MRP).getText();
			System.out.println("Item Price   "+FoodieMRP);
			test.log(Status.INFO, MarkupHelper.createLabel("MRP of an Product:  "+FoodieMRP, ExtentColor.ORANGE));
			Thread.sleep(1000);
			try
			{
				String col1 = driver.findElement(home.Trend_color1).getCssValue("color");
				System.out.println("Color 2 exists:  "+col1);
				steps.clickButton(home.Trend_color1, driver, name);
				Thread.sleep(2000);
				FoodieColor1 = driver.findElement(home.Trend_color1).getCssValue("background-color");
				Foodiehex1 = Color.fromString(FoodieColor1).asHex();
				System.out.println("Image color: "+Foodiehex1);
				test.log(Status.INFO, MarkupHelper.createLabel("User changed item color to:  "+Foodiehex1, ExtentColor.BLUE));
				FoodieFirstcolor = getscreen.capture(driver, "FoodieFirstcolor");
				test.addScreenCaptureFromPath(FoodieFirstcolor);
			}
			catch(Exception e)
			{
				System.out.println("Color 1 is not available");
			}
			Thread.sleep(1000);
			try
			{
				String col2 = driver.findElement(home.Trend_color2).getCssValue("color");
				System.out.println("Color 3 exists:  "+col2);
				steps.clickButton(home.Trend_color2, driver, name);
				Thread.sleep(2000);
				FoodieColor2 = driver.findElement(home.Trend_color2).getCssValue("background-color");
				Foodiehex2 = Color.fromString(FoodieColor2).asHex();
				System.out.println("Image color: "+Foodiehex2);
				test.log(Status.INFO, MarkupHelper.createLabel("User changed item color to:  "+Foodiehex2, ExtentColor.BLUE));
				FoodieSecondcolor = getscreen.capture(driver, "FoodieSecondcolor");
				test.addScreenCaptureFromPath(FoodieSecondcolor);
			}
			catch(Exception e)
			{
				System.out.println("Color 2 is not available");
			}
			Thread.sleep(1000);
			//if(driver.findElement(home2.color3).isDisplayed())
			try
			{
				String col3 = driver.findElement(home.Trend_color3).getCssValue("color");
				System.out.println("Color 4 exists:  "+col3);
				steps.clickButton(home.Trend_color3, driver, name);
				Thread.sleep(2000);
				FoodieColor3 = driver.findElement(home.Trend_color3).getCssValue("background-color");
				Foodiehex3 = Color.fromString(FoodieColor3).asHex();
				System.out.println("Image color: "+Foodiehex3);
				test.log(Status.INFO, MarkupHelper.createLabel("User changed item color to:  "+Foodiehex3, ExtentColor.BLUE));
				FoodieThirdcolor = getscreen.capture(driver, "FoodieThirdcolor");
				test.addScreenCaptureFromPath(FoodieThirdcolor);
			}
			catch(Exception e)
			{
				System.out.println("Color 3 is not available");
			}
			Thread.sleep(1000);
			try
			{
				String col4 = driver.findElement(home.Trend_color5).getCssValue("color");
				System.out.println("Color 1 exists:  "+col4);
				steps.clickButton(home.Trend_color5, driver, name);
				Thread.sleep(2000);
				Foodiecolor5 = driver.findElement(home.Trend_color5).getCssValue("background-color");
				Foodiehex5 = Color.fromString(Foodiecolor5).asHex();
				System.out.println("Image color: "+Foodiehex5);
				test.log(Status.INFO, MarkupHelper.createLabel("User changed item color to:  "+Foodiehex5, ExtentColor.BLUE));
				FoodieFifthcolor = getscreen.capture(driver, "FoodieFifthcolor");
				test.addScreenCaptureFromPath(FoodieFifthcolor);
			}
			catch(Exception e)
			{
				System.out.println("Color 5 is not available");
			}
			Foodiecqty = driver.findElement(home.Qty).getAttribute("value");
			System.out.println("Current Quantity:  "+ Foodiecqty);
			test.log(Status.INFO, MarkupHelper.createLabel("Current Quantity of product:  "+Foodiecqty, ExtentColor.BLUE));
			Thread.sleep(1000);
			steps.clickButton(home.Trend_QtyIncrease, driver, name);
			Foodieiqty = driver.findElement(home.Qty).getAttribute("value");
			System.out.println("Quantity after increase:  "+ Foodieiqty);
			test.log(Status.INFO, MarkupHelper.createLabel("Current Quantity of product after Increase:  "+Foodieiqty, ExtentColor.BLUE));
			steps.clickButton(home.Trend_QtyDecrease, driver, name);
			Foodiedqty = driver.findElement(home.Qty).getAttribute("value");
			System.out.println("Quantity after decrease:  "+ Foodiedqty);
			test.log(Status.INFO, MarkupHelper.createLabel("Current Quantity of product after increase:  "+Foodiedqty, ExtentColor.BLUE));
			Thread.sleep(2000);
			steps.clickButton(home.Trend_Addcart, driver, name);
			FoodieSizemsg = driver.findElement(home.Trend_Sizeerrormsg).getText();
			System.out.println("User clicked on Addcart before Size selection:  "+FoodieSizemsg);
			test.log(Status.INFO, MarkupHelper.createLabel("User clicked on Addcart before Size selection:  "+FoodieSizemsg, ExtentColor.RED));
			steps.clickJSButton(home2.Size_S, driver, name);
			FoodiesizeS = driver.findElement(home2.Size_S).getText();
			System.out.println("Selected Size:  "+FoodiesizeS);
			test.log(Status.INFO, MarkupHelper.createLabel("User selected size:  "+FoodiesizeS, ExtentColor.BLUE));
			steps.clickButton(home2.Size_M, driver, name);
			FoodiesizeM = driver.findElement(home2.Size_M).getText();
			System.out.println("Selected Size:  "+FoodiesizeM);
			test.log(Status.INFO, MarkupHelper.createLabel("User selected size:  "+FoodiesizeM, ExtentColor.BLUE));
			steps.clickButton(home.Size_L, driver, name);
			FoodiesizeL = driver.findElement(home.Size_L).getText();
			System.out.println("Selected Size:  "+FoodiesizeL);
			test.log(Status.INFO, MarkupHelper.createLabel("User selected size:  "+FoodiesizeL, ExtentColor.BLUE));
			steps.clickButton(home.Size_XL, driver, name);
			FoodiesizeXL = driver.findElement(home.Size_XL).getText();
			System.out.println("Selected Size:  "+FoodiesizeXL);
			test.log(Status.INFO, MarkupHelper.createLabel("User selected size:  "+FoodiesizeXL, ExtentColor.BLUE));
			steps.clickButton(home.Size_XXL, driver, name);
			FoodiesizeXXL = driver.findElement(home.Size_XXL).getText();
			System.out.println("Selected Size:  "+FoodiesizeXXL);
			test.log(Status.INFO, MarkupHelper.createLabel("User selected size:  "+FoodiesizeXXL, ExtentColor.BLUE));
			steps.clickButton(home.Size_XXXL, driver, name);
			FoodiesizeXXXL = driver.findElement(home.Size_XXXL).getText();
			System.out.println("Selected Size:  "+FoodiesizeXXXL);
			test.log(Status.INFO, MarkupHelper.createLabel("User selected size:  "+FoodiesizeXXXL, ExtentColor.BLUE));
			steps.clickButton(home.Trend_FindUrSize, driver, name);
			test.log(Status.INFO, MarkupHelper.createLabel("User clicked on Find your size", ExtentColor.BLUE));
			Thread.sleep(1000);
			try
			{
				String R510 = driver.findElement(home.Trend_FindUrSize_510radio).getText();
				System.out.println(R510);
				steps.clickButton(home.Trend_FindUrSize_510radio, driver, name);
				Foodiebelow510 = driver.findElement(home.Trend_FindUrSize_510radio).getText();
				System.out.println("User Selected:  "+Foodiebelow510);
				test.log(Status.INFO, MarkupHelper.createLabel("User selected Height:  "+Foodiebelow510, ExtentColor.BLUE));
				steps.clickButton(home.Trend_FindUrSize_above510radio, driver, name);
				Foodieabove510 = driver.findElement(home.Trend_FindUrSize_above510radio).getText();
				System.out.println("User Selected:  "+Foodieabove510);
				test.log(Status.INFO, MarkupHelper.createLabel("User selected Height:  "+Foodieabove510, ExtentColor.BLUE));
				steps.clickButton(home.Trend_FindUrSize_Tightradio, driver, name);
				Foodietightfit = driver.findElement(home.Trend_FindUrSize_Tightradio).getText();
				System.out.println("User Selected:  "+Foodietightfit);
				test.log(Status.INFO, MarkupHelper.createLabel("User selected Fit:  "+Foodietightfit, ExtentColor.BLUE));
				steps.clickButton(home.Trend_FindUrSize_Comfortradio, driver, name);
				Foodiecomfit = driver.findElement(home.Trend_FindUrSize_Comfortradio).getText();
				System.out.println("User Selected:  "+Foodiecomfit);
				test.log(Status.INFO, MarkupHelper.createLabel("User selected Fit:  "+Foodiecomfit, ExtentColor.BLUE));
				steps.selectDropdown(home.Trend_FindUrSize_sizeDD, driver, strDataFileName, "size", name);
				test.log(Status.INFO, MarkupHelper.createLabel("User selected shirt size", ExtentColor.BLUE));
				steps.clickButton(home.Trend_FindUrSize_submit, driver, name);
				test.log(Status.INFO, MarkupHelper.createLabel("User clicked on Submit", ExtentColor.BLUE));
				Foodiefindursize = getscreen.capture(driver, "Foodiefindursize");
				test.addScreenCaptureFromPath(Foodiefindursize);
				Thread.sleep(1000);
				steps.clickButton(home.Trend_FindUrSize_Okbutton, driver, name);
				test.log(Status.INFO, MarkupHelper.createLabel("system displayed size for given metrics", ExtentColor.BLUE));
				steps.clickButton(home.Trend_FindUrSize_closebutton, driver, name);
				test.log(Status.INFO, MarkupHelper.createLabel("User navigate to Product details page", ExtentColor.BLUE));
			}
			catch(Exception e)
			{
				steps.clickButton(home2.FindUrSize_closebutton, driver, name);
				test.log(Status.INFO, MarkupHelper.createLabel("User navigate to Product details page", ExtentColor.BLUE));
			}
			steps.clickButton(home.Size_L, driver, name);
			test.log(Status.INFO, MarkupHelper.createLabel("User selected size", ExtentColor.BLUE));
			steps.clickButton(home.Trend_Addcart, driver, name);
			test.log(Status.INFO, MarkupHelper.createLabel("User added product to Cart", ExtentColor.BLUE));
			Foodieaddcart = getscreen.capture(driver, "Foodieaddcart");
			test.addScreenCaptureFromPath(Foodieaddcart);
			Thread.sleep(1000);
			steps.clickButton(home2.Foodie_remove, driver, name);
			Foodieremovecart = getscreen.capture(driver, "Foodiefindursize");
			test.addScreenCaptureFromPath(Foodieremovecart);
			test.log(Status.INFO, MarkupHelper.createLabel("User removed product from Cart", ExtentColor.BLUE));
			steps.clickButton(home.Size_L, driver, name);
			test.log(Status.INFO, MarkupHelper.createLabel("User selected size", ExtentColor.BLUE));
			steps.clickButton(home.BuyNow, driver, name);
			test.log(Status.INFO, MarkupHelper.createLabel("User clicked on BuyNow, navigated to checkout page", ExtentColor.BLUE));
			String Foodiebuynow1 = getscreen.capture(driver, "Foodiebuynow1");
			test.addScreenCaptureFromPath(Foodiebuynow1);
			try
			{
				String cash = driver.findElement(home.CashonDeliveryText).getText();
				System.out.println("Payment method:  "+cash);
				steps.clickButton(home.CashonDeliveryradio, driver, name);
				String Foodiebuynow2 = driver.findElement(home.PopUpSaveMsg).getText();
				System.out.println("Displayed message:   "+Foodiebuynow2);
				test.log(Status.INFO, MarkupHelper.createLabel("Displayed message:   "+Foodiebuynow2, ExtentColor.BLUE));
				Thread.sleep(1000);
				/*	buynow3 = driver.findElement(home.PopUpMsg).getAttribute("value");
			System.out.println("Displayed message:   "+buynow3);
			test.log(Status.INFO, MarkupHelper.createLabel("Displayed message:   "+buynow3, ExtentColor.BLUE));
				 */
				String	 Foodiebuynow4 = getscreen.capture(driver, "Foodiebuynow4");
				test.addScreenCaptureFromPath(Foodiebuynow4);
				steps.clickButton(home.Cashondelivery, driver, name);
				test.log(Status.INFO, MarkupHelper.createLabel("User clicked on Cash on Delivery link", ExtentColor.BLUE));
			}
			catch(Exception e)
			{
				System.out.println("Cash on Delivery is not available");
			}
			Thread.sleep(1000);
			steps.clickButton(home.CartIcon, driver, name);
			steps.clickButton(home.Trend_Addcart_remove, driver, name);
			test.log(Status.INFO, MarkupHelper.createLabel("User Completed transactions of 1st Image in New Arrivals", ExtentColor.PURPLE));
			driver.close();
			driver.switchTo().window(Parent);
			Foodiestatus = passstatus;
			Assert.assertTrue(true);
			System.out.println("Foodie Successful");
			testresultdata.put(Integer.toString(cnt), new Object[] {name, "Foodie", "Foodie Successful",passstatus});	
		}
		catch(Exception e)
		{
			System.out.println("Foodie Unsuccessful");
			testresultdata.put(Integer.toString(cnt), new Object[] {name, "Foodie", "Foodi Unsuccessful",failstatus});
			Foodiestatus = failstatus;
			Foodiestatusfail = getscreen.capture(driver, "Foodiestatusfail");
			test.addScreenCaptureFromPath(Foodiestatusfail);
			driver.close();
			Assert.assertTrue(false);
		}
	}
	@Test(priority = 5)
	public void Biker() throws IOException, InterruptedException
	{
		String name = new Object(){}.getClass().getEnclosingMethod().getName();
		cnt ++;
		try 
		{
			test = extent.createTest("Biker under What Inspires You Most");
			System.out.println("Foodie under What Inspires You Most Test Case Executing...");
			Thread.sleep(1000);
			commonmethods.URLNaviagtion(driver);
			Thread.sleep(1000);
			/*	commonmethods.Login(driver);
			test.log(Status.INFO, MarkupHelper.createLabel("User able to Login", ExtentColor.BLUE));
			Thread.sleep(3000);
			 */
			steps.clickButton(home.Mydreamstore, driver, name);
			String homepage = getscreen.capture(driver, name);
			test.addScreenCaptureFromPath(homepage);
			String Parent = driver.getWindowHandle();
			steps.MoveElement(home2.WIM_Image6, driver, name);
			steps.clickJSButton(home2.WIM_Image6, driver, name);
			test.log(Status.INFO, MarkupHelper.createLabel("User clicked on Biker Image under Who Inspires You Most", ExtentColor.BLUE));
			steps.ChildWindow(driver);
			driver.manage().window().maximize();
			BikerPage = getscreen.capture(driver, "BikerPage");
			test.addScreenCaptureFromPath(BikerPage);
			try
			{
				String Filterpage1 = driver.findElement(home.Filterpagetext).getText();
				System.out.println("User navigated to :   "+Filterpage1);
				steps.clickButton(home2.Photography_Motherday, driver, name);
				test.log(Status.INFO, MarkupHelper.createLabel("User selected on Mothers Day Category", ExtentColor.BLUE));
				try
				{
					FoodieNoCampaignMsg1 = driver.findElement(home2.Photography_NoCampaignFound).getText();
					System.out.println("Campaigns Not Found under Mother's day Category");
					test.log(Status.INFO, MarkupHelper.createLabel("User hasn't found Active Campaigns under Mothers Day Category", ExtentColor.BLUE));
					BikerMothersDayProducts1 = getscreen.capture(driver, "BikerMothersDayProducts1");
					test.addScreenCaptureFromPath(BikerMothersDayProducts1);
					steps.clickButton(home2.Photography_Motherday, driver, name);
				}
				catch(Exception e)
				{
					System.out.println("Campaigns Found");
					test.log(Status.INFO, MarkupHelper.createLabel("User found Active Campaigns under Mothers Day Category", ExtentColor.BLUE));
					steps.clickButton(home2.Photography_Motherday, driver, name);
				}
				steps.clickButton(home2.Photography_Bolly, driver, name);
				try
				{
					BikerNoCampaignMsg2 = driver.findElement(home2.Photography_NoCampaignFound).getText();
					System.out.println("Campaigns Not Found under Bollywood/Movie Personalities Category");
					test.log(Status.INFO, MarkupHelper.createLabel("User hasn't found Active Campaigns under Bollywood/Movie Personalities Category", ExtentColor.BLUE));
					BikerBollywoodProducts1 = getscreen.capture(driver, "BikerBollywoodProducts1");
					test.addScreenCaptureFromPath(BikerBollywoodProducts1);
					steps.clickButton(home2.Photography_Bolly, driver, name);
				}
				catch(Exception e)
				{
					System.out.println("Campaigns Found");
					test.log(Status.INFO, MarkupHelper.createLabel("User found Active Campaigns under Bollywood/Movie Personalities Category", ExtentColor.BLUE));
					steps.clickButton(home2.Photography_Bolly, driver, name);
				}
				steps.clickButton(home2.Photography_Sports, driver, name);
				try
				{
					BikerNoCampaignMsg3 = driver.findElement(home2.Photography_NoCampaignFound).getText();
					System.out.println("Campaigns Not Found under Sports Category");
					test.log(Status.INFO, MarkupHelper.createLabel("User hasn't found Active Campaigns under Sports Category", ExtentColor.BLUE));
					BikerSportsProducts1 = getscreen.capture(driver, "BikerSportsProducts1");
					test.addScreenCaptureFromPath(BikerSportsProducts1);
					steps.clickButton(home2.Photography_Sports, driver, name);
				}
				catch(Exception e)
				{
					System.out.println("Campaigns Found");
					test.log(Status.INFO, MarkupHelper.createLabel("User found Active Campaigns under Sports Category", ExtentColor.BLUE));
					steps.clickButton(home2.Photography_Sports, driver, name);
				}
				Thread.sleep(5000);
				steps.selectDropdown(home2.Photography_SortDD, driver, strDataFileName, "BikerSortBy1", name);
				test.log(Status.INFO, MarkupHelper.createLabel("User sort out New Arrival products in Photography", ExtentColor.BLUE));
				Thread.sleep(3000);
				steps.selectDropdown(home2.Photography_SortDD, driver, strDataFileName, "BikerSortBy2", name);
				test.log(Status.INFO, MarkupHelper.createLabel("User sort out Best Selling products in Music Lover", ExtentColor.BLUE));
				steps.ScrollUp(driver);
				Thread.sleep(2000);
				steps.clickButton(home2.Biker_Filter_Image1, driver, name);
				test.log(Status.INFO, MarkupHelper.createLabel("User clicked on 1st Image of Biker", ExtentColor.BLUE));
			}
			catch(Exception e )
			{
				System.out.println("User navigated to different  page");
				steps.clickButton(home2.Biker_Image1, driver, name);
				driver.close();
				steps.ChildWindow(driver);
				driver.manage().window().maximize();
			}
			Thread.sleep(2000);
			try
			{
				steps.clickButton(home2.Photography_Image1_Readmore, driver, name);
				BikerDescription = driver.findElement(home2.Biker_description).getText();
				System.out.println("Production Description   "+BikerDescription);
				test.log(Status.INFO, MarkupHelper.createLabel("Description about Item:  "+BikerDescription, ExtentColor.BLUE));
				steps.clickButton(home2.Photography_Image1_Readless, driver, name);
			}
			catch(Exception e)
			{
				steps.clickButton(home2.Photography_Image1_Readmore, driver, name);
				BikerDescription = driver.findElement(home2.Biker_description1).getText();
				System.out.println("Production Description   "+BikerDescription);
				test.log(Status.INFO, MarkupHelper.createLabel("Description about Item:  "+BikerDescription, ExtentColor.BLUE));
				steps.clickButton(home2.Photography_Image1_Readless, driver, name);
			}
			BikerMRP = driver.findElement(home.Trend_Image_MRP).getText();
			System.out.println("Product Price   "+BikerMRP);
			test.log(Status.INFO, MarkupHelper.createLabel("MRP of an Product:  "+BikerMRP, ExtentColor.ORANGE));
			try
			{
				String product2 = driver.findElement(home2.Biker_product2).getCssValue("color");
				System.out.println("Product 2 exists:  "+product2);
				steps.clickButton(home2.Biker_product2, driver, name);
				Thread.sleep(1000);
				test.log(Status.INFO, MarkupHelper.createLabel("User selected second Product ", ExtentColor.INDIGO));
				Bikerproduct2 = getscreen.capture(driver, "Bikerproduct2");
				test.addScreenCaptureFromPath(Bikerproduct2);
			}
			catch(Exception e)
			{
				System.out.println("Product 2 is not available");
			}
			try
			{
				String product3 = driver.findElement(home2.Biker_product1).getCssValue("color");
				System.out.println("Product 1 exists:  "+product3);
				steps.clickButton(home2.Biker_product1, driver, name);
				Thread.sleep(1000);
				test.log(Status.INFO, MarkupHelper.createLabel("User selected First Product ", ExtentColor.INDIGO));
				Bikerproduct1 = getscreen.capture(driver, "Bikerproduct1");
				test.addScreenCaptureFromPath(Bikerproduct1);
			}
			catch(Exception e)
			{
				System.out.println("Product 1 is not available");
			}
			Thread.sleep(1000);
			Bikercqty = driver.findElement(home.Qty).getAttribute("value");
			System.out.println("Current Quantity:  "+ Bikercqty);
			test.log(Status.INFO, MarkupHelper.createLabel("Current Quantity of product:  "+Bikercqty, ExtentColor.BLUE));
			Thread.sleep(1000);
			steps.clickButton(home.Trend_QtyIncrease, driver, name);
			Bikeriqty = driver.findElement(home.Qty).getAttribute("value");
			System.out.println("Quantity after increase:  "+ Bikeriqty);
			test.log(Status.INFO, MarkupHelper.createLabel("Current Quantity of product after Increase:  "+Bikeriqty, ExtentColor.BLUE));
			steps.clickButton(home.Trend_QtyDecrease, driver, name);
			Bikerdqty = driver.findElement(home.Qty).getAttribute("value");
			System.out.println("Quantity after decrease:  "+ Bikerdqty);
			test.log(Status.INFO, MarkupHelper.createLabel("Current Quantity of product after increase:  "+Bikerdqty, ExtentColor.BLUE));
			Thread.sleep(2000);
			steps.clickButton(home.Trend_Addcart, driver, name);
			BikerSizemsg = driver.findElement(home.Trend_Sizeerrormsg).getText();
			System.out.println("User clicked on Addcart before Size selection:  "+BikerSizemsg);
			test.log(Status.INFO, MarkupHelper.createLabel("User clicked on Addcart before Size selection:  "+BikerSizemsg, ExtentColor.RED));
			steps.clickJSButton(home2.Size_S, driver, name);
			BikersizeS = driver.findElement(home2.Size_S).getText();
			System.out.println("Selected Size:  "+BikersizeS);
			test.log(Status.INFO, MarkupHelper.createLabel("User selected size:  "+BikersizeS, ExtentColor.BLUE));
			steps.clickButton(home2.Size_M, driver, name);
			BikersizeM = driver.findElement(home2.Size_M).getText();
			System.out.println("Selected Size:  "+BikersizeM);
			test.log(Status.INFO, MarkupHelper.createLabel("User selected size:  "+BikersizeM, ExtentColor.BLUE));
			steps.clickButton(home2.Size_L, driver, name);
			BikersizeL = driver.findElement(home2.Size_L).getText();
			System.out.println("Selected Size:  "+BikersizeL);
			test.log(Status.INFO, MarkupHelper.createLabel("User selected size:  "+BikersizeL, ExtentColor.BLUE));
			steps.clickButton(home2.Size_XL, driver, name);
			BikersizeXL = driver.findElement(home2.Size_XL).getText();
			System.out.println("Selected Size:  "+BikersizeXL);
			test.log(Status.INFO, MarkupHelper.createLabel("User selected size:  "+BikersizeXL, ExtentColor.BLUE));
			steps.clickButton(home2.Size_XXL, driver, name);
			BikersizeXXL = driver.findElement(home2.Size_XXL).getText();
			System.out.println("Selected Size:  "+BikersizeXXL);
			test.log(Status.INFO, MarkupHelper.createLabel("User selected size:  "+BikersizeXXL, ExtentColor.BLUE));
			steps.clickButton(home2.Size_XXXL, driver, name);
			BikersizeXXXL = driver.findElement(home2.Size_XXXL).getText();
			System.out.println("Selected Size:  "+BikersizeXXXL);
			test.log(Status.INFO, MarkupHelper.createLabel("User selected size:  "+BikersizeXXXL, ExtentColor.BLUE));
			steps.clickButton(home.Trend_FindUrSize, driver, name);
			test.log(Status.INFO, MarkupHelper.createLabel("User clicked on Find your size", ExtentColor.BLUE));
			Thread.sleep(1000);
			try
			{
				String R510 = driver.findElement(home.Trend_FindUrSize_510radio).getText();
				System.out.println(R510);
				steps.clickButton(home.Trend_FindUrSize_510radio, driver, name);
				Bikerbelow510 = driver.findElement(home.Trend_FindUrSize_510radio).getText();
				System.out.println("User Selected:  "+Bikerbelow510);
				test.log(Status.INFO, MarkupHelper.createLabel("User selected Height:  "+Bikerbelow510, ExtentColor.BLUE));
				steps.clickButton(home.Trend_FindUrSize_above510radio, driver, name);
				Bikerabove510 = driver.findElement(home.Trend_FindUrSize_above510radio).getText();
				System.out.println("User Selected:  "+Bikerabove510);
				test.log(Status.INFO, MarkupHelper.createLabel("User selected Height:  "+Bikerabove510, ExtentColor.BLUE));
				steps.clickButton(home.Trend_FindUrSize_Tightradio, driver, name);
				Bikertightfit = driver.findElement(home.Trend_FindUrSize_Tightradio).getText();
				System.out.println("User Selected:  "+Bikertightfit);
				test.log(Status.INFO, MarkupHelper.createLabel("User selected Fit:  "+Bikertightfit, ExtentColor.BLUE));
				steps.clickButton(home.Trend_FindUrSize_Comfortradio, driver, name);
				Bikercomfit = driver.findElement(home.Trend_FindUrSize_Comfortradio).getText();
				System.out.println("User Selected:  "+Bikercomfit);
				test.log(Status.INFO, MarkupHelper.createLabel("User selected Fit:  "+Bikercomfit, ExtentColor.BLUE));
				steps.selectDropdown(home.Trend_FindUrSize_sizeDD, driver, strDataFileName, "size", name);
				test.log(Status.INFO, MarkupHelper.createLabel("User selected shirt size", ExtentColor.BLUE));
				steps.clickButton(home.Trend_FindUrSize_submit, driver, name);
				test.log(Status.INFO, MarkupHelper.createLabel("User clicked on Submit", ExtentColor.BLUE));
				Bikerfindursize = getscreen.capture(driver, "Bikerfindursize");
				test.addScreenCaptureFromPath(Bikerfindursize);
				Thread.sleep(1000);
				steps.clickButton(home.Trend_FindUrSize_Okbutton, driver, name);
				test.log(Status.INFO, MarkupHelper.createLabel("system displayed size for given metrics", ExtentColor.BLUE));
				steps.clickButton(home.Trend_FindUrSize_closebutton, driver, name);
				test.log(Status.INFO, MarkupHelper.createLabel("User navigate to Product details page", ExtentColor.BLUE));
			}
			catch(Exception e)
			{
				steps.clickButton(home2.FindUrSize_closebutton, driver, name);
				test.log(Status.INFO, MarkupHelper.createLabel("User navigate to Product details page", ExtentColor.BLUE));
			}
			steps.clickButton(home2.Size_L, driver, name);
			test.log(Status.INFO, MarkupHelper.createLabel("User selected size", ExtentColor.BLUE));
			steps.clickButton(home.Trend_Addcart, driver, name);
			test.log(Status.INFO, MarkupHelper.createLabel("User added product to Cart", ExtentColor.BLUE));
			Bikeraddcart = getscreen.capture(driver, "Bikeraddcart");
			test.addScreenCaptureFromPath(Bikeraddcart);
			Thread.sleep(1000);
			try
			{
				String edit = driver.findElement(home2.Addcart_edit).getText();
				System.out.println(edit);
				steps.clickButton(home.Trend_Addcart_edit, driver, name);
				test.log(Status.INFO, MarkupHelper.createLabel("User clicked on Edit", ExtentColor.BLUE));
				steps.selectDropdown(home.Trend_Addcart_selectsize, driver, strDataFileName, "SelectSize", name);
				steps.selectDropdown(home.Trend_Addcart_selectqty, driver, strDataFileName, "Quantity", name);
				test.log(Status.INFO, MarkupHelper.createLabel("User selected Size and Quantity", ExtentColor.BLUE));
				steps.clickButton(home.Trend_Addcart_cancel, driver, name);
				steps.clickButton(home.Trend_Addcart_edit, driver, name);
				steps.selectDropdown(home.Trend_Addcart_selectsize, driver, strDataFileName, "SelectSize", name);
				steps.selectDropdown(home.Trend_Addcart_selectqty, driver, strDataFileName, "Quantity", name);
				steps.clickButton(home.Trend_Addcart_save, driver, name);
				Bikerupdateproduct = getscreen.capture(driver, "Bikerupdateproduct");
				test.addScreenCaptureFromPath(Bikerupdateproduct);
				test.log(Status.INFO, MarkupHelper.createLabel("User updated product size and quantity", ExtentColor.BLUE));
			}
			catch(Exception e)
			{
				System.out.println("Edit feature is not available for this product");
			}
			steps.clickButton(home2.Biker_remove, driver, name);
			Bikerremovecart = getscreen.capture(driver, "Bikerremovecart");
			test.addScreenCaptureFromPath(Bikerremovecart);
			test.log(Status.INFO, MarkupHelper.createLabel("User removed product from Cart", ExtentColor.BLUE));
			steps.clickButton(home.Size_L, driver, name);
			test.log(Status.INFO, MarkupHelper.createLabel("User selected size", ExtentColor.BLUE));
			steps.clickButton(home.BuyNow, driver, name);
			test.log(Status.INFO, MarkupHelper.createLabel("User clicked on BuyNow, navigated to checkout page", ExtentColor.BLUE));
			String Bikebuynow1 = getscreen.capture(driver, "Bikebuynow1");
			test.addScreenCaptureFromPath(Bikebuynow1);
			try
			{
				String cash = driver.findElement(home.CashonDeliveryText).getText();
				System.out.println("Payment method:  "+cash);
				steps.Verifytext(home.CashonDeliveryText, driver, cash, name);
				steps.clickButton(home.CashonDeliveryradio, driver, name);
				String Bikebuynow2 = driver.findElement(home.PopUpSaveMsg).getText();
				System.out.println("Displayed message:   "+Bikebuynow2);
				test.log(Status.INFO, MarkupHelper.createLabel("Displayed message:   "+Bikebuynow2, ExtentColor.BLUE));
				Thread.sleep(1000);
				/*	buynow3 = driver.findElement(home.PopUpMsg).getAttribute("value");
			System.out.println("Displayed message:   "+buynow3);
			test.log(Status.INFO, MarkupHelper.createLabel("Displayed message:   "+buynow3, ExtentColor.BLUE));
				 */
				String	 Bikebuynow4 = getscreen.capture(driver, "Bikebuynow4");
				test.addScreenCaptureFromPath(Bikebuynow4);
				steps.clickButton(home.Cashondelivery, driver, name);
				test.log(Status.INFO, MarkupHelper.createLabel("User clicked on Cash on Delivery link", ExtentColor.BLUE));
			}
			catch(Exception e)
			{
				System.out.println("Cash on Delivery is not available");
			}
			Thread.sleep(1000);
			steps.clickButton(home.CartIcon, driver, name);
			steps.clickButton(home.Trend_Addcart_remove, driver, name);
			test.log(Status.INFO, MarkupHelper.createLabel("User Completed transactions of 1st Image in New Arrivals", ExtentColor.PURPLE));
			driver.close();
			driver.switchTo().window(Parent);
			Bikerstatus = passstatus;
			Assert.assertTrue(true);
			System.out.println("Biker Successful");
			testresultdata.put(Integer.toString(cnt), new Object[] {name, "Biker", "Biker Successful",passstatus});	
		}
		catch(Exception e)
		{
			System.out.println("Biker Unsuccessful");
			testresultdata.put(Integer.toString(cnt), new Object[] {name, "Biker", "Biker Unsuccessful",failstatus});
			Bikerstatus = failstatus;
			Bikerstatusfail = getscreen.capture(driver, "Bikerstatusfail");
			test.addScreenCaptureFromPath(Bikerstatusfail);
			driver.close();
			Assert.assertTrue(false);
		}
	}
	@Test(priority = 6)
	public void StartUp() throws IOException, InterruptedException
	{
		String name = new Object(){}.getClass().getEnclosingMethod().getName();
		cnt ++;
		try 
		{
			test = extent.createTest("StartUp under What Inspires You Most");
			System.out.println("StartUp under What Inspires You Most Test Case Executing...");
			Thread.sleep(1000);
			commonmethods.URLNaviagtion(driver);
			Thread.sleep(1000);
			/*commonmethods.Login(driver);
			test.log(Status.INFO, MarkupHelper.createLabel("User able to Login", ExtentColor.BLUE));
			Thread.sleep(3000);*/
			steps.clickButton(home.Mydreamstore, driver, name);
			String homepage = getscreen.capture(driver, name);
			test.addScreenCaptureFromPath(homepage);
			String Parent = driver.getWindowHandle();
			steps.MoveElement(home2.WIM_Image7, driver, name);
			steps.clickJSButton(home2.WIM_Image7, driver, name);
			test.log(Status.INFO, MarkupHelper.createLabel("User clicked on StartUp Image under Who Inspires You Most", ExtentColor.BLUE));
			steps.ChildWindow(driver);
			driver.manage().window().maximize();
			StartUpPage = getscreen.capture(driver, "StartUpPage");
			test.addScreenCaptureFromPath(StartUpPage);
			Thread.sleep(2000);
			try
			{
				String Filterpage1 = driver.findElement(home.Filterpagetext).getText();
				System.out.println("User navigated to :   "+Filterpage1);
				steps.selectDropdown(home2.Photography_SortDD, driver, strDataFileName, "StartUpSortBy1", name);
				test.log(Status.INFO, MarkupHelper.createLabel("User sort out New Arrival products in StartUp", ExtentColor.BLUE));
				Thread.sleep(10000);
				steps.selectDropdown(home2.Photography_SortDD, driver, strDataFileName, "StartUpSortBy2", name);
				test.log(Status.INFO, MarkupHelper.createLabel("User sort out Best Selling products in StartUp", ExtentColor.BLUE));
				steps.ScrollUp(driver);
				Thread.sleep(6000);
				steps.clickButton(home2.Startup_image1, driver, name);
				test.log(Status.INFO, MarkupHelper.createLabel("User clicked on 1st Image of StartUp", ExtentColor.BLUE));
			}
			catch(Exception e )
			{
				System.out.println("User navigated to wrong page");
			}
			try
			{
				steps.clickButton(home2.Photography_Image1_Readmore, driver, name);
				StartUpDescription = driver.findElement(home2.Startup_description).getText();
				System.out.println("Production Description   "+StartUpDescription);
				test.log(Status.INFO, MarkupHelper.createLabel("Description about Item:  "+StartUpDescription, ExtentColor.BLUE));
				steps.clickButton(home2.Photography_Image1_Readless, driver, name);
			}
			catch(Exception e)
			{
				steps.clickButton(home2.Photography_Image1_Readmore, driver, name);
				StartUpDescription = driver.findElement(home2.Startup_description1).getText();
				System.out.println("Production Description   "+StartUpDescription);
				test.log(Status.INFO, MarkupHelper.createLabel("Description about Item:  "+StartUpDescription, ExtentColor.BLUE));
				steps.clickButton(home2.Photography_Image1_Readless, driver, name);
			}
			StartUpMRP = driver.findElement(home.Trend_Image_MRP).getText();
			System.out.println("Product Price   "+StartUpMRP);
			test.log(Status.INFO, MarkupHelper.createLabel("MRP of an Product:  "+StartUpMRP, ExtentColor.ORANGE));
			Thread.sleep(1000);
			try
			{
				String product2 = driver.findElement(home2.Startup_product2).getCssValue("color");
				System.out.println("Product 2 exists:  "+product2);
				steps.clickButton(home2.Startup_product2, driver, name);
				StartUpProduct2 = getscreen.capture(driver, "StartUpProduct2");
				test.addScreenCaptureFromPath(StartUpProduct2);
				Thread.sleep(2000);
			}
			catch(Exception e)
			{
				System.out.println("Product 2 is not available");
			}
			try
			{
				String product1 = driver.findElement(home2.Startup_product1).getCssValue("color");
				System.out.println("Product 1 exists:  "+product1);
				steps.clickButton(home2.Startup_product1, driver, name);
				StartUpProduct1 = getscreen.capture(driver, "StartUpProduct1");
				test.addScreenCaptureFromPath(StartUpProduct1);
			}
			catch(Exception e)
			{
				System.out.println("Product 1 is not available");
			}
			Thread.sleep(2000);
			try
			{
				String Startcol1 = driver.findElement(home2.color1).getCssValue("color");
				System.out.println("Color 2 exists:  "+Startcol1);
				steps.clickButton(home2.color1, driver, name);
				Thread.sleep(2000);
				StartUpColor1 = driver.findElement(home2.color1).getCssValue("background-color");
				StartUphex1 = Color.fromString(StartUpColor1).asHex();
				System.out.println("Image color: "+StartUphex1);
				test.log(Status.INFO, MarkupHelper.createLabel("User changed item color to:  "+StartUphex1, ExtentColor.BLUE));
				StartUpFirstcolor = getscreen.capture(driver, "StartUpFirstcolor");
				test.addScreenCaptureFromPath(StartUpFirstcolor);
			}
			catch(Exception e)
			{
				System.out.println("Color 1 is not exists");
			}
			try
			{
				String Startcol2 = driver.findElement(home2.color2).getCssValue("color");
				System.out.println("Color 3 exists:  "+Startcol2);
				steps.clickButton(home2.color2, driver, name);
				Thread.sleep(1000);
				StartUpColor2 = driver.findElement(home2.color2).getCssValue("background-color");
				StartUphex2 = Color.fromString(StartUpColor2).asHex();
				System.out.println("Image color: "+StartUphex2);
				test.log(Status.INFO, MarkupHelper.createLabel("User changed item color to:  "+StartUphex2, ExtentColor.BLUE));
				StartUpSecondcolor = getscreen.capture(driver, "StartUpSecondcolor");
				test.addScreenCaptureFromPath(StartUpSecondcolor);
			}
			catch(Exception e)
			{
				System.out.println("Color 2 is not exists");
			}
			try
			{
				String Startcol3 = driver.findElement(home2.color3).getCssValue("color");
				System.out.println("Color 4 exists:  "+Startcol3);
				steps.clickButton(home2.color3, driver, name);
				Thread.sleep(1000);
				StartUpColor3 = driver.findElement(home2.color3).getCssValue("background-color");
				StartUphex3 = Color.fromString(StartUpColor3).asHex();
				System.out.println("Image color: "+StartUphex3);
				test.log(Status.INFO, MarkupHelper.createLabel("User changed item color to:  "+StartUphex3, ExtentColor.BLUE));
				StartUpThirdcolor = getscreen.capture(driver, "StartUpThirdcolor");
				test.addScreenCaptureFromPath(StartUpThirdcolor);
			}
			catch(Exception e)
			{
				System.out.println("Color 3 is not exists");
			}
			try 
			{
				String Startcol4 = driver.findElement(home2.color4).getCssValue("color");
				System.out.println("Color 1 exists:  "+Startcol4);
				steps.clickButton(home2.color4, driver, name);
				Thread.sleep(1000);
				StartUpColor4 = driver.findElement(home2.color4).getCssValue("background-color");
				StartUphex4 = Color.fromString(StartUpColor4).asHex();
				System.out.println("Image color: "+StartUphex4);
				test.log(Status.INFO, MarkupHelper.createLabel("User changed item color to:  "+StartUphex4, ExtentColor.BLUE));
				StartUpFourthcolor = getscreen.capture(driver, "StartUpFourthcolor");
				test.addScreenCaptureFromPath(StartUpFourthcolor);
			}
			catch(Exception e)
			{
				System.out.println("Color 4 is not exists");
			}
			Thread.sleep(2000);
			StartUpcqty = driver.findElement(home.Qty).getAttribute("value");
			System.out.println("Current Quantity:  "+ StartUpcqty);
			test.log(Status.INFO, MarkupHelper.createLabel("Current Quantity of product:  "+StartUpcqty, ExtentColor.BLUE));
			Thread.sleep(1000);
			steps.clickButton(home.Trend_QtyIncrease, driver, name);
			StartUpiqty = driver.findElement(home.Qty).getAttribute("value");
			System.out.println("Quantity after increase:  "+ StartUpiqty);
			test.log(Status.INFO, MarkupHelper.createLabel("Current Quantity of product after Increase:  "+StartUpiqty, ExtentColor.BLUE));
			steps.clickButton(home.Trend_QtyDecrease, driver, name);
			StartUpdqty = driver.findElement(home.Qty).getAttribute("value");
			System.out.println("Quantity after decrease:  "+ StartUpdqty);
			test.log(Status.INFO, MarkupHelper.createLabel("Current Quantity of product after increase:  "+StartUpdqty, ExtentColor.BLUE));
			steps.clickButton(home.Trend_Addcart, driver, name);
			StartUpSizemsg = driver.findElement(home.Trend_Sizeerrormsg).getText();
			System.out.println("User clicked on Addcart before Size selection:  "+StartUpSizemsg);
			test.log(Status.INFO, MarkupHelper.createLabel("User clicked on Addcart before Size selection:  "+StartUpSizemsg, ExtentColor.RED));
			//steps.clickJSButton(home2.Startup_sizeS, driver, name);
			steps.clickJSButton(home2.Size_S, driver, name);
			StartUpsizeS = driver.findElement(home2.Size_S).getText();
			System.out.println("Selected Size:  "+StartUpsizeS);
			test.log(Status.INFO, MarkupHelper.createLabel("User selected size:  "+StartUpsizeS, ExtentColor.BLUE));
			steps.clickButton(home2.Size_M, driver, name);
			StartUpsizeM = driver.findElement(home2.Size_M).getText();
			System.out.println("Selected Size:  "+StartUpsizeM);
			test.log(Status.INFO, MarkupHelper.createLabel("User selected size:  "+StartUpsizeM, ExtentColor.BLUE));
			steps.clickButton(home2.Size_L, driver, name);
			StartUpsizeL = driver.findElement(home2.Size_L).getText();
			System.out.println("Selected Size:  "+StartUpsizeL);
			test.log(Status.INFO, MarkupHelper.createLabel("User selected size:  "+StartUpsizeL, ExtentColor.BLUE));
			steps.clickButton(home2.Size_XL, driver, name);
			StartUpsizeXL = driver.findElement(home2.Size_XL).getText();
			System.out.println("Selected Size:  "+StartUpsizeXL);
			test.log(Status.INFO, MarkupHelper.createLabel("User selected size:  "+StartUpsizeXL, ExtentColor.BLUE));
			steps.clickButton(home2.Size_XXL, driver, name);
			StartUpsizeXXL = driver.findElement(home2.Size_XXL).getText();
			System.out.println("Selected Size:  "+StartUpsizeXXL);
			test.log(Status.INFO, MarkupHelper.createLabel("User selected size:  "+StartUpsizeXXL, ExtentColor.BLUE));
			steps.clickButton(home2.Size_XXXL, driver, name);
			StartUpsizeXXXL = driver.findElement(home2.Size_XXXL).getText();
			System.out.println("Selected Size:  "+StartUpsizeXXXL);
			test.log(Status.INFO, MarkupHelper.createLabel("User selected size:  "+StartUpsizeXXXL, ExtentColor.BLUE));
			steps.clickButton(home.Trend_FindUrSize, driver, name);
			test.log(Status.INFO, MarkupHelper.createLabel("User clicked on Find your size", ExtentColor.BLUE));
			Thread.sleep(1000);
			try
			{
				String S510 = driver.findElement(home.Trend_FindUrSize_510radio).getText();
				System.out.println(S510);
				steps.clickButton(home.Trend_FindUrSize_510radio, driver, name);
				StartUpbelow510 = driver.findElement(home.Trend_FindUrSize_510radio).getText();
				System.out.println("User Selected:  "+StartUpbelow510);
				test.log(Status.INFO, MarkupHelper.createLabel("User selected Height:  "+StartUpbelow510, ExtentColor.BLUE));
				steps.clickButton(home.Trend_FindUrSize_above510radio, driver, name);
				StartUpabove510 = driver.findElement(home.Trend_FindUrSize_above510radio).getText();
				System.out.println("User Selected:  "+StartUpabove510);
				test.log(Status.INFO, MarkupHelper.createLabel("User selected Height:  "+StartUpabove510, ExtentColor.BLUE));
				steps.clickButton(home.Trend_FindUrSize_Tightradio, driver, name);
				StartUptightfit = driver.findElement(home.Trend_FindUrSize_Tightradio).getText();
				System.out.println("User Selected:  "+StartUptightfit);
				test.log(Status.INFO, MarkupHelper.createLabel("User selected Fit:  "+StartUptightfit, ExtentColor.BLUE));
				steps.clickButton(home.Trend_FindUrSize_Comfortradio, driver, name);
				StartUpcomfit = driver.findElement(home.Trend_FindUrSize_Comfortradio).getText();
				System.out.println("User Selected:  "+StartUpcomfit);
				test.log(Status.INFO, MarkupHelper.createLabel("User selected Fit:  "+StartUpcomfit, ExtentColor.BLUE));
				steps.selectDropdown(home.Trend_FindUrSize_sizeDD, driver, strDataFileName, "size", name);
				test.log(Status.INFO, MarkupHelper.createLabel("User selected shirt size", ExtentColor.BLUE));
				steps.clickButton(home.Trend_FindUrSize_submit, driver, name);
				test.log(Status.INFO, MarkupHelper.createLabel("User clicked on Submit", ExtentColor.BLUE));
				StartUpfindursize = getscreen.capture(driver, "StartUpfindursize");
				test.addScreenCaptureFromPath(StartUpfindursize);
				Thread.sleep(1000);
				steps.clickButton(home.Trend_FindUrSize_Okbutton, driver, name);
				test.log(Status.INFO, MarkupHelper.createLabel("system displayed size for given metrics", ExtentColor.BLUE));
				steps.clickButton(home.Trend_FindUrSize_closebutton, driver, name);
				test.log(Status.INFO, MarkupHelper.createLabel("User navigate to Product details page", ExtentColor.BLUE));
			}
			catch(Exception e)
			{
				steps.clickButton(home2.FindUrSize_closebutton, driver, name);
				test.log(Status.INFO, MarkupHelper.createLabel("User navigate to Product details page", ExtentColor.BLUE));
			}
			steps.clickButton(home2.Size_L, driver, name);
			test.log(Status.INFO, MarkupHelper.createLabel("User selected size", ExtentColor.BLUE));
			steps.clickButton(home.Trend_Addcart, driver, name);
			Thread.sleep(1000);
			test.log(Status.INFO, MarkupHelper.createLabel("User added product to Cart", ExtentColor.BLUE));
			StartUpaddcart = getscreen.capture(driver, "StartUpaddcart");
			test.addScreenCaptureFromPath(StartUpaddcart);
			Thread.sleep(1000);
			steps.clickButton(home2.Biker_remove, driver, name);
			StartUpremovecart = getscreen.capture(driver, "StartUpremovecart");
			test.addScreenCaptureFromPath(StartUpremovecart);
			test.log(Status.INFO, MarkupHelper.createLabel("User removed product from Cart", ExtentColor.BLUE));
			steps.clickButton(home.Size_L, driver, name);
			test.log(Status.INFO, MarkupHelper.createLabel("User selected size", ExtentColor.BLUE));
			steps.clickButton(home.BuyNow, driver, name);
			test.log(Status.INFO, MarkupHelper.createLabel("User clicked on BuyNow, navigated to checkout page", ExtentColor.BLUE));
			String Startbuynow1 = getscreen.capture(driver, "Startbuynow1");
			test.addScreenCaptureFromPath(Startbuynow1);
			try
			{
				String cash = driver.findElement(home.CashonDeliveryText).getText();
				System.out.println("Payment method:  "+cash);
				steps.clickButton(home.CashonDeliveryradio, driver, name);
				String Startbuynow2 = driver.findElement(home.PopUpSaveMsg).getText();
				System.out.println("Displayed message:   "+Startbuynow2);
				test.log(Status.INFO, MarkupHelper.createLabel("Displayed message:   "+Startbuynow2, ExtentColor.BLUE));
				Thread.sleep(1000);
				/*	buynow3 = driver.findElement(home.PopUpMsg).getAttribute("value");
			System.out.println("Displayed message:   "+buynow3);
			test.log(Status.INFO, MarkupHelper.createLabel("Displayed message:   "+buynow3, ExtentColor.BLUE));
				 */
				String	 Startbuynow4 = getscreen.capture(driver, "Startbuynow4");
				test.addScreenCaptureFromPath(Startbuynow4);
				steps.clickButton(home.Cashondelivery, driver, name);
				test.log(Status.INFO, MarkupHelper.createLabel("User clicked on Cash on Delivery link", ExtentColor.BLUE));
			}
			catch(Exception e)
			{
				System.out.println("Cash on Delivery is not available");
			}
			Thread.sleep(1000);
			steps.clickButton(home.CartIcon, driver, name);
			steps.clickButton(home.Trend_Addcart_remove, driver, name);
			test.log(Status.INFO, MarkupHelper.createLabel("User Completed transactions of 1st Image in StartUp", ExtentColor.PURPLE));
			driver.close();
			driver.switchTo().window(Parent);
			StartUpstatus = passstatus;
			Assert.assertTrue(true);
			System.out.println("StartUp Successful");
			testresultdata.put(Integer.toString(cnt), new Object[] {name, "StartUp", "StartUp Successful",passstatus});	
		}
		catch(Exception e)
		{
			System.out.println("StartUp Unsuccessful");
			testresultdata.put(Integer.toString(cnt), new Object[] {name, "StartUp", "StartUp Unsuccessful",failstatus});
			StartUpstatus = failstatus;
			StartUpstatusfail = getscreen.capture(driver, "StartUpstatusfail");
			test.addScreenCaptureFromPath(StartUpstatusfail);
			driver.close();
			Assert.assertTrue(false);
		}
	}
	@Test(priority = 7)
	public void Profession() throws IOException, InterruptedException
	{
		String name = new Object(){}.getClass().getEnclosingMethod().getName();
		cnt ++;
		try 
		{
			test = extent.createTest("Profession under What Inspires You Most");
			System.out.println("Profession under What Inspires You Most Test Case Executing...");
			Thread.sleep(1000);
			commonmethods.URLNaviagtion(driver);
			Thread.sleep(1000);
			/*commonmethods.Login(driver);
			test.log(Status.INFO, MarkupHelper.createLabel("User able to Login", ExtentColor.BLUE));
			Thread.sleep(3000);*/
			steps.clickButton(home.Mydreamstore, driver, name);
			String homepage = getscreen.capture(driver, name);
			test.addScreenCaptureFromPath(homepage);
			String Parent = driver.getWindowHandle();
			steps.MoveElement(home2.WIM_Image8, driver, name);
			steps.clickJSButton(home2.WIM_Image8, driver, name);
			test.log(Status.INFO, MarkupHelper.createLabel("User clicked on Profession Image under Who Inspires You Most", ExtentColor.BLUE));
			steps.ChildWindow(driver);
			driver.manage().window().maximize();
			ProfessionPage = getscreen.capture(driver, "ProfessionPage");
			test.addScreenCaptureFromPath(ProfessionPage);
			Thread.sleep(2000);
			try
			{
				String Filterpage1 = driver.findElement(home.Filterpagetext).getText();
				System.out.println("User navigated to :   "+Filterpage1);
				steps.selectDropdown(home2.Photography_SortDD, driver, strDataFileName, "ProfessionSortBy1", name);
				test.log(Status.INFO, MarkupHelper.createLabel("User sort out New Arrival products in Profession", ExtentColor.BLUE));
				Thread.sleep(5000);
				steps.selectDropdown(home2.Photography_SortDD, driver, strDataFileName, "ProfessionSortBy2", name);
				test.log(Status.INFO, MarkupHelper.createLabel("User sort out Best Selling products in Profession", ExtentColor.BLUE));
				steps.ScrollUp(driver);
				Thread.sleep(2000);
				steps.clickButton(home2.Profession_image1, driver, name);
				test.log(Status.INFO, MarkupHelper.createLabel("User clicked on 1st Image of Profession", ExtentColor.BLUE));
			}
			catch(Exception e )
			{
				System.out.println("User navigated to wrong page");
			}
			Thread.sleep(1000);
			try
			{
				steps.clickButton(home2.Photography_Image1_Readmore, driver, name);
				ProfessionDescription = driver.findElement(home2.Profession_description).getText();
				System.out.println("Production Description   "+ProfessionDescription);
				test.log(Status.INFO, MarkupHelper.createLabel("Description about Item:  "+ProfessionDescription, ExtentColor.BLUE));
				steps.clickButton(home2.Profession_Readless, driver, name);
			}
			catch(Exception e)
			{
				steps.clickButton(home2.Photography_Image1_Readmore, driver, name);
				ProfessionDescription = driver.findElement(home2.Profession_description1).getText();
				System.out.println("Production Description   "+ProfessionDescription);
				test.log(Status.INFO, MarkupHelper.createLabel("Description about Item:  "+ProfessionDescription, ExtentColor.BLUE));
				steps.clickButton(home2.Profession_Readless, driver, name);
			}
			ProfessionMRP = driver.findElement(home.Trend_Image_MRP).getText();
			System.out.println("Product Price   "+ProfessionMRP);
			test.log(Status.INFO, MarkupHelper.createLabel("MRP of an Product:  "+ProfessionMRP, ExtentColor.ORANGE));
			try
			{
				String col1 = driver.findElement(home2.color1).getCssValue("color");
				System.out.println("Color 2 exists:  "+col1);
				steps.clickButton(home2.color1, driver, name);
				Thread.sleep(2000);
				ProfessionColor1 = driver.findElement(home2.color1).getCssValue("background-color");
				Professionhex1 = Color.fromString(ProfessionColor1).asHex();
				System.out.println("Image color: "+Professionhex1);
				test.log(Status.INFO, MarkupHelper.createLabel("User changed item color to:  "+Professionhex1, ExtentColor.BLUE));
				ProfessionFirstcolor = getscreen.capture(driver, "ProfessionFirstcolor");
				test.addScreenCaptureFromPath(ProfessionFirstcolor);
			}
			catch(Exception e)
			{
				System.out.println("Color 2 is not available");
			}
			try
			{
				String col2 = driver.findElement(home2.color2).getCssValue("color");
				System.out.println("Color 3 exists:  "+col2);
				steps.clickButton(home2.color2, driver, name);
				Thread.sleep(1000);
				ProfessionColor2 = driver.findElement(home2.color2).getCssValue("background-color");
				Professionhex2 = Color.fromString(ProfessionColor2).asHex();
				System.out.println("Image color: "+Professionhex2);
				test.log(Status.INFO, MarkupHelper.createLabel("User changed item color to:  "+Professionhex2, ExtentColor.BLUE));
				ProfessionSecondcolor = getscreen.capture(driver, "ProfessionSecondcolor");
				test.addScreenCaptureFromPath(ProfessionSecondcolor);
			}
			catch(Exception e)
			{
				System.out.println("Color 3 is not available");
			}
			try
			{
				String col3 = driver.findElement(home2.color3).getCssValue("color");
				System.out.println("Color 4 exists:  "+col3);
				steps.clickButton(home2.color3, driver, name);
				Thread.sleep(1000);
				ProfessionColor3 = driver.findElement(home2.color3).getCssValue("background-color");
				Professionhex3 = Color.fromString(ProfessionColor3).asHex();
				System.out.println("Image color: "+Professionhex3);
				test.log(Status.INFO, MarkupHelper.createLabel("User changed item color to:  "+Professionhex3, ExtentColor.BLUE));
				ProfessionThirdcolor = getscreen.capture(driver, "ProfessionThirdcolor");
				test.addScreenCaptureFromPath(ProfessionThirdcolor);
			}
			catch(Exception e)
			{
				System.out.println("Color 4 is not available");
			}
			try
			{
				String col4 = driver.findElement(home2.color4).getCssValue("color");
				System.out.println("Color 1 exists:  "+col4);
				steps.clickButton(home2.color4, driver, name);
				Thread.sleep(1000);
				ProfessionColor4 = driver.findElement(home2.color4).getCssValue("background-color");
				Professionhex4 = Color.fromString(ProfessionColor4).asHex();
				System.out.println("Image color: "+Professionhex4);
				test.log(Status.INFO, MarkupHelper.createLabel("User changed item color to:  "+Professionhex4, ExtentColor.BLUE));
				ProfessionFourthcolor = getscreen.capture(driver, "ProfessionFourthcolor");
				test.addScreenCaptureFromPath(ProfessionFourthcolor);
			}
			catch(Exception e)
			{
				System.out.println("Color 1 is not available");
			}
			Thread.sleep(2000);
			Professioncqty = driver.findElement(home.Qty).getAttribute("value");
			System.out.println("Current Quantity:  "+ Professioncqty);
			test.log(Status.INFO, MarkupHelper.createLabel("Current Quantity of product:  "+Professioncqty, ExtentColor.BLUE));
			Thread.sleep(1000);
			steps.clickButton(home.Trend_QtyIncrease, driver, name);
			Professioniqty = driver.findElement(home.Qty).getAttribute("value");
			System.out.println("Quantity after increase:  "+ Professioniqty);
			test.log(Status.INFO, MarkupHelper.createLabel("Current Quantity of product after Increase:  "+Professioniqty, ExtentColor.BLUE));
			steps.clickButton(home.Trend_QtyDecrease, driver, name);
			Professiondqty = driver.findElement(home.Qty).getAttribute("value");
			System.out.println("Quantity after decrease:  "+ Professiondqty);
			test.log(Status.INFO, MarkupHelper.createLabel("Current Quantity of product after increase:  "+Professiondqty, ExtentColor.BLUE));
			steps.clickButton(home.Trend_Addcart, driver, name);
			ProfessionSizemsg = driver.findElement(home.Trend_Sizeerrormsg).getText();
			System.out.println("User clicked on Addcart before Size selection:  "+ProfessionSizemsg);
			test.log(Status.INFO, MarkupHelper.createLabel("User clicked on Addcart before Size selection:  "+ProfessionSizemsg, ExtentColor.RED));
			//steps.clickJSButton(home2.Profession_sizeS, driver, name);
			steps.clickJSButton(home2.Size_S, driver, name);
			ProfessionsizeS = driver.findElement(home2.Size_S).getText();
			System.out.println("Selected Size:  "+ProfessionsizeS);
			test.log(Status.INFO, MarkupHelper.createLabel("User selected size:  "+ProfessionsizeS, ExtentColor.BLUE));
			steps.clickButton(home2.Size_M, driver, name);
			ProfessionsizeM = driver.findElement(home2.Size_M).getText();
			System.out.println("Selected Size:  "+ProfessionsizeM);
			test.log(Status.INFO, MarkupHelper.createLabel("User selected size:  "+ProfessionsizeM, ExtentColor.BLUE));
			steps.clickButton(home2.Size_L, driver, name);
			ProfessionsizeL = driver.findElement(home2.Size_L).getText();
			System.out.println("Selected Size:  "+ProfessionsizeL);
			test.log(Status.INFO, MarkupHelper.createLabel("User selected size:  "+ProfessionsizeL, ExtentColor.BLUE));
			steps.clickButton(home2.Size_XL, driver, name);
			ProfessionsizeXL = driver.findElement(home2.Size_XL).getText();
			System.out.println("Selected Size:  "+ProfessionsizeXL);
			test.log(Status.INFO, MarkupHelper.createLabel("User selected size:  "+ProfessionsizeXL, ExtentColor.BLUE));
			steps.clickButton(home2.Size_XXL, driver, name);
			ProfessionsizeXXL = driver.findElement(home2.Size_XXL).getText();
			System.out.println("Selected Size:  "+ProfessionsizeXXL);
			test.log(Status.INFO, MarkupHelper.createLabel("User selected size:  "+ProfessionsizeXXL, ExtentColor.BLUE));
			steps.clickButton(home2.Size_XXXL, driver, name);
			ProfessionsizeXXXL = driver.findElement(home2.Size_XXXL).getText();
			System.out.println("Selected Size:  "+ProfessionsizeXXXL);
			test.log(Status.INFO, MarkupHelper.createLabel("User selected size:  "+ProfessionsizeXXXL, ExtentColor.BLUE));
			steps.clickButton(home.Trend_FindUrSize, driver, name);
			test.log(Status.INFO, MarkupHelper.createLabel("User clicked on Find your size", ExtentColor.BLUE));
			Thread.sleep(1000);
			try
			{
				String P510 = driver.findElement(home.Trend_FindUrSize_510radio).getText();
				System.out.println(P510);
				steps.clickButton(home.Trend_FindUrSize_510radio, driver, name);
				Professionbelow510 = driver.findElement(home.Trend_FindUrSize_510radio).getText();
				System.out.println("User Selected:  "+Professionbelow510);
				test.log(Status.INFO, MarkupHelper.createLabel("User selected Height:  "+Professionbelow510, ExtentColor.BLUE));
				steps.clickButton(home.Trend_FindUrSize_above510radio, driver, name);
				Professionabove510 = driver.findElement(home.Trend_FindUrSize_above510radio).getText();
				System.out.println("User Selected:  "+Professionabove510);
				test.log(Status.INFO, MarkupHelper.createLabel("User selected Height:  "+Professionabove510, ExtentColor.BLUE));
				steps.clickButton(home.Trend_FindUrSize_Tightradio, driver, name);
				Professiontightfit = driver.findElement(home.Trend_FindUrSize_Tightradio).getText();
				System.out.println("User Selected:  "+Professiontightfit);
				test.log(Status.INFO, MarkupHelper.createLabel("User selected Fit:  "+Professiontightfit, ExtentColor.BLUE));
				steps.clickButton(home.Trend_FindUrSize_Comfortradio, driver, name);
				Professioncomfit = driver.findElement(home.Trend_FindUrSize_Comfortradio).getText();
				System.out.println("User Selected:  "+Professioncomfit);
				test.log(Status.INFO, MarkupHelper.createLabel("User selected Fit:  "+Professioncomfit, ExtentColor.BLUE));
				steps.selectDropdown(home.Trend_FindUrSize_sizeDD, driver, strDataFileName, "size", name);
				test.log(Status.INFO, MarkupHelper.createLabel("User selected shirt size", ExtentColor.BLUE));
				steps.clickButton(home.Trend_FindUrSize_submit, driver, name);
				test.log(Status.INFO, MarkupHelper.createLabel("User clicked on Submit", ExtentColor.BLUE));
				Professionfindursize = getscreen.capture(driver, "Professionfindursize");
				test.addScreenCaptureFromPath(Professionfindursize);
				Thread.sleep(1000);
				steps.clickButton(home.Trend_FindUrSize_Okbutton, driver, name);
				test.log(Status.INFO, MarkupHelper.createLabel("system displayed size for given metrics", ExtentColor.BLUE));
				steps.clickButton(home.Trend_FindUrSize_closebutton, driver, name);
				test.log(Status.INFO, MarkupHelper.createLabel("User navigate to Product details page", ExtentColor.BLUE));
			}
			catch(Exception e)
			{
				steps.clickButton(home2.FindUrSize_closebutton, driver, name);
				test.log(Status.INFO, MarkupHelper.createLabel("User navigate to Product details page", ExtentColor.BLUE));
			}
			steps.clickButton(home2.Size_L, driver, name);
			test.log(Status.INFO, MarkupHelper.createLabel("User selected size", ExtentColor.BLUE));
			steps.clickButton(home.Trend_Addcart, driver, name);
			Thread.sleep(1000);
			test.log(Status.INFO, MarkupHelper.createLabel("User added product to Cart", ExtentColor.BLUE));
			Professionaddcart = getscreen.capture(driver, "Professionaddcart");
			test.addScreenCaptureFromPath(Professionaddcart);
			Thread.sleep(1000);
			try
			{
				String edit = driver.findElement(home2.Addcart_edit).getText();
				System.out.println(edit);
				steps.clickButton(home2.Addcart_edit, driver, name);
				test.log(Status.INFO, MarkupHelper.createLabel("User clicked on Edit", ExtentColor.BLUE));
				steps.selectDropdown(home2.Addcart_selectsize, driver, strDataFileName, "SelectSize", name);
				steps.selectDropdown(home2.Addcart_selectqty, driver, strDataFileName, "Quantity", name);
				test.log(Status.INFO, MarkupHelper.createLabel("User selected Size and Quantity", ExtentColor.BLUE));
				steps.clickButton(home2.Addcart_cancel, driver, name);
				steps.clickButton(home2.Addcart_edit, driver, name);
				steps.selectDropdown(home2.Addcart_selectsize, driver, strDataFileName, "SelectSize", name);
				steps.selectDropdown(home2.Addcart_selectqty, driver, strDataFileName, "Quantity", name);
				steps.clickButton(home2.Addcart_save, driver, name);
				Professionupdateproduct1 = getscreen.capture(driver, "Professionupdateproduct1");
				test.addScreenCaptureFromPath(Professionupdateproduct1);
				test.log(Status.INFO, MarkupHelper.createLabel("User updated product size and quantity", ExtentColor.BLUE));
			}
			catch(Exception e)
			{
				System.out.println("Edit feature is not available for this product");
			}
			Thread.sleep(2000);
			steps.clickButton(home2.Biker_remove, driver, name);
			Professionremovecart = getscreen.capture(driver, "Professionremovecart");
			test.addScreenCaptureFromPath(Professionremovecart);
			test.log(Status.INFO, MarkupHelper.createLabel("User removed product from Cart", ExtentColor.BLUE));
			steps.clickButton(home.Size_L, driver, name);
			test.log(Status.INFO, MarkupHelper.createLabel("User selected size", ExtentColor.BLUE));
			steps.clickButton(home.BuyNow, driver, name);
			test.log(Status.INFO, MarkupHelper.createLabel("User clicked on BuyNow, navigated to checkout page", ExtentColor.BLUE));
			String Profbuynow1 = getscreen.capture(driver, "Profbuynow1");
			test.addScreenCaptureFromPath(Profbuynow1);
			Thread.sleep(1000);
			try
			{
				String cash = driver.findElement(home.CashonDeliveryText).getText();
				System.out.println("Payment method:  "+cash);
				steps.Verifytext(home.CashonDeliveryText, driver, cash, name);
				steps.clickButton(home.CashonDeliveryradio, driver, name);
				String Profbuynow2 = driver.findElement(home.PopUpSaveMsg).getText();
				System.out.println("Displayed message:   "+Profbuynow2);
				test.log(Status.INFO, MarkupHelper.createLabel("Displayed message:   "+Profbuynow2, ExtentColor.BLUE));
				String	 Profbuynow4 = getscreen.capture(driver, "Profbuynow4");
				test.addScreenCaptureFromPath(Profbuynow4);
				steps.clickButton(home.Cashondelivery, driver, name);
				test.log(Status.INFO, MarkupHelper.createLabel("User clicked on Cash on Delivery link", ExtentColor.BLUE));	
			}
			catch(Exception e)
			{
				System.out.println("Cash on Delivery is not available");
			}
			Thread.sleep(1000);
			steps.clickButton(home.CartIcon, driver, name);
			steps.clickButton(home.Trend_Addcart_remove, driver, name);
			test.log(Status.INFO, MarkupHelper.createLabel("User Completed transactions of 1st Image in Profession", ExtentColor.PURPLE));
			driver.close();
			driver.switchTo().window(Parent);
			Professionstatus = passstatus;
			Assert.assertTrue(true);
			System.out.println("Profession Successful");
			testresultdata.put(Integer.toString(cnt), new Object[] {name, "Profession", "Profession Successful",passstatus});	
		}
		catch(Exception e)
		{
			System.out.println("Profession Unsuccessful");
			testresultdata.put(Integer.toString(cnt), new Object[] {name, "Profession", "Profession Unsuccessful",failstatus});
			Professionstatus = failstatus;
			Professionstatusfail = getscreen.capture(driver, "Professionstatusfail");
			test.addScreenCaptureFromPath(Professionstatusfail);
			driver.close();
			Assert.assertTrue(false);
		}
	}
	@Test(priority = 8)
	public void Gifting() throws IOException, InterruptedException
	{
		String name = new Object(){}.getClass().getEnclosingMethod().getName();
		cnt ++;
		try 
		{
			test = extent.createTest("Gifting under What Inspires You Most");
			System.out.println("Gifting under What Inspires You Most Test Case Executing...");
			Thread.sleep(1000);
			commonmethods.URLNaviagtion(driver);
			Thread.sleep(1000);
			/*commonmethods.Login(driver);
			test.log(Status.INFO, MarkupHelper.createLabel("User able to Login", ExtentColor.BLUE));
			Thread.sleep(3000);*/
			steps.clickButton(home.Mydreamstore, driver, name);
			String homepage = getscreen.capture(driver, name);
			test.addScreenCaptureFromPath(homepage);
			String Parent = driver.getWindowHandle();
			steps.MoveElement(home2.WIM_Image9, driver, name);
			steps.clickJSButton(home2.WIM_Image9, driver, name);
			test.log(Status.INFO, MarkupHelper.createLabel("User clicked on Gifting Image under Who Inspires You Most", ExtentColor.BLUE));
			steps.ChildWindow(driver);
			driver.manage().window().maximize();
			GiftingPage = getscreen.capture(driver, "GiftingPage");
			test.addScreenCaptureFromPath(GiftingPage);
			Thread.sleep(2000);
			try
			{
				String Filterpage1 = driver.findElement(home.Filterpagetext).getText();
				System.out.println("User navigated to :   "+Filterpage1);
			}
			catch(Exception e )
			{
				System.out.println("User navigated to different landing page");
				steps.clickButton(home2.Gifting_image1, driver, name);
				test.log(Status.INFO, MarkupHelper.createLabel("User clicked on 1st Image of Gifting", ExtentColor.BLUE));
				driver.close();
				steps.ChildWindow(driver);
				driver.manage().window().maximize();
				GiftingPage2 = getscreen.capture(driver, "GiftingPage2");
				test.addScreenCaptureFromPath(GiftingPage2);
			}
			Thread.sleep(1000);
			try
			{
				steps.clickButton(home2.Photography_Image1_Readmore, driver, name);
				GiftingDescription = driver.findElement(home2.Gifting_description).getText();
				System.out.println("Production Description   "+GiftingDescription);
				test.log(Status.INFO, MarkupHelper.createLabel("Description about Item:  "+GiftingDescription, ExtentColor.BLUE));
				steps.clickButton(home2.Profession_Readless, driver, name);
			}
			catch(Exception e)
			{
				steps.clickButton(home2.Photography_Image1_Readmore, driver, name);
				GiftingDescription = driver.findElement(home2.Gifting_description1).getText();
				System.out.println("Production Description   "+GiftingDescription);
				test.log(Status.INFO, MarkupHelper.createLabel("Description about Item:  "+GiftingDescription, ExtentColor.BLUE));
				steps.clickButton(home2.Profession_Readless, driver, name);
			}
			GiftingMRP = driver.findElement(home.Trend_Image_MRP).getText();
			System.out.println("Product Price   "+GiftingMRP);
			test.log(Status.INFO, MarkupHelper.createLabel("MRP of an Product:  "+GiftingMRP, ExtentColor.ORANGE));
			try
			{
				String col1 = driver.findElement(home2.color1).getCssValue("color");
				System.out.println("Color 2 exists:  "+col1);
				steps.clickButton(home2.color1, driver, name);
				Thread.sleep(2000);
				GiftingColor1 = driver.findElement(home2.color1).getCssValue("background-color");
				Giftinghex1 = Color.fromString(GiftingColor1).asHex();
				System.out.println("Image color: "+Giftinghex1);
				test.log(Status.INFO, MarkupHelper.createLabel("User changed item color to:  "+Giftinghex1, ExtentColor.BLUE));
				GiftingFirstcolor = getscreen.capture(driver, "GiftingFirstcolor");
				test.addScreenCaptureFromPath(GiftingFirstcolor);
			}
			catch(Exception e)
			{
				System.out.println("Color 2 is not available");
			}
			try
			{
				String col2 = driver.findElement(home2.color2).getCssValue("color");
				System.out.println("Color 3 exists:  "+col2);
				steps.clickButton(home2.color2, driver, name);
				Thread.sleep(1000);
				GiftingColor2 = driver.findElement(home2.color2).getCssValue("background-color");
				Giftinghex2 = Color.fromString(GiftingColor2).asHex();
				System.out.println("Image color: "+Giftinghex2);
				test.log(Status.INFO, MarkupHelper.createLabel("User changed item color to:  "+Giftinghex2, ExtentColor.BLUE));
				GiftingSecondcolor = getscreen.capture(driver, "GiftingSecondcolor");
				test.addScreenCaptureFromPath(GiftingSecondcolor);
			}
			catch(Exception e)
			{
				System.out.println("Color 3 is not available");
			}
			try
			{
				String col3 = driver.findElement(home2.color3).getCssValue("color");
				System.out.println("Color 4 exists:  "+col3);
				steps.clickButton(home2.color3, driver, name);
				Thread.sleep(1000);
				GiftingColor3 = driver.findElement(home2.color3).getCssValue("background-color");
				Giftinghex3 = Color.fromString(GiftingColor3).asHex();
				System.out.println("Image color: "+Giftinghex3);
				test.log(Status.INFO, MarkupHelper.createLabel("User changed item color to:  "+Giftinghex3, ExtentColor.BLUE));
				GiftingThirdcolor = getscreen.capture(driver, "GiftingThirdcolor");
				test.addScreenCaptureFromPath(GiftingThirdcolor);
			}
			catch(Exception e)
			{
				System.out.println("Color 4 is not available");
			}
			try
			{
				String col4 = driver.findElement(home2.color4).getCssValue("color");
				System.out.println("Color 1 exists:  "+col4);
				steps.clickButton(home2.color4, driver, name);
				Thread.sleep(1000);
				GiftingColor4 = driver.findElement(home2.color4).getCssValue("background-color");
				Giftinghex4 = Color.fromString(GiftingColor4).asHex();
				System.out.println("Image color: "+Giftinghex4);
				test.log(Status.INFO, MarkupHelper.createLabel("User changed item color to:  "+Giftinghex4, ExtentColor.BLUE));
				GiftingFourthcolor = getscreen.capture(driver, "GiftingFourthcolor");
				test.addScreenCaptureFromPath(GiftingFourthcolor);
			}
			catch(Exception e)
			{
				System.out.println("Color 1 is not available");
			}
			Thread.sleep(2000);
			Giftingcqty = driver.findElement(home.Qty).getAttribute("value");
			System.out.println("Current Quantity:  "+ Giftingcqty);
			test.log(Status.INFO, MarkupHelper.createLabel("Current Quantity of product:  "+Giftingcqty, ExtentColor.BLUE));
			Thread.sleep(1000);
			steps.clickButton(home.Trend_QtyIncrease, driver, name);
			Giftingiqty = driver.findElement(home.Qty).getAttribute("value");
			System.out.println("Quantity after increase:  "+ Giftingiqty);
			test.log(Status.INFO, MarkupHelper.createLabel("Current Quantity of product after Increase:  "+Giftingiqty, ExtentColor.BLUE));
			steps.clickButton(home.Trend_QtyDecrease, driver, name);
			Giftingdqty = driver.findElement(home.Qty).getAttribute("value");
			System.out.println("Quantity after decrease:  "+ Giftingdqty);
			test.log(Status.INFO, MarkupHelper.createLabel("Current Quantity of product after increase:  "+Giftingdqty, ExtentColor.BLUE));
			steps.clickButton(home.Trend_Addcart, driver, name);
			GiftingSizemsg = driver.findElement(home.Trend_Sizeerrormsg).getText();
			System.out.println("User clicked on Addcart before Size selection:  "+GiftingSizemsg);
			test.log(Status.INFO, MarkupHelper.createLabel("User clicked on Addcart before Size selection:  "+GiftingSizemsg, ExtentColor.RED));
			//steps.clickJSButton(home2.Gifting_sizeS, driver, name);
			steps.clickJSButton(home2.Size_S, driver, name);
			GiftingsizeS = driver.findElement(home2.Size_S).getText();
			System.out.println("Selected Size:  "+GiftingsizeS);
			test.log(Status.INFO, MarkupHelper.createLabel("User selected size:  "+GiftingsizeS, ExtentColor.BLUE));
			steps.clickButton(home2.Size_M, driver, name);
			GiftingsizeM = driver.findElement(home2.Size_M).getText();
			System.out.println("Selected Size:  "+GiftingsizeM);
			test.log(Status.INFO, MarkupHelper.createLabel("User selected size:  "+GiftingsizeM, ExtentColor.BLUE));
			steps.clickButton(home2.Size_L, driver, name);
			GiftingsizeL = driver.findElement(home2.Size_L).getText();
			System.out.println("Selected Size:  "+GiftingsizeL);
			test.log(Status.INFO, MarkupHelper.createLabel("User selected size:  "+GiftingsizeL, ExtentColor.BLUE));
			steps.clickButton(home2.Size_XL, driver, name);
			GiftingsizeXL = driver.findElement(home2.Size_XL).getText();
			System.out.println("Selected Size:  "+GiftingsizeXL);
			test.log(Status.INFO, MarkupHelper.createLabel("User selected size:  "+GiftingsizeXL, ExtentColor.BLUE));
			steps.clickButton(home2.Size_XXL, driver, name);
			GiftingsizeXXL = driver.findElement(home2.Size_XXL).getText();
			System.out.println("Selected Size:  "+GiftingsizeXXL);
			test.log(Status.INFO, MarkupHelper.createLabel("User selected size:  "+GiftingsizeXXL, ExtentColor.BLUE));
			steps.clickButton(home2.Size_XXXL, driver, name);
			GiftingsizeXXXL = driver.findElement(home2.Size_XXXL).getText();
			System.out.println("Selected Size:  "+GiftingsizeXXXL);
			test.log(Status.INFO, MarkupHelper.createLabel("User selected size:  "+GiftingsizeXXXL, ExtentColor.BLUE));
			steps.clickButton(home.Trend_FindUrSize, driver, name);
			test.log(Status.INFO, MarkupHelper.createLabel("User clicked on Find your size", ExtentColor.BLUE));
			Thread.sleep(1000);
			try
			{
				String G510 = driver.findElement(home.Trend_FindUrSize_510radio).getText();
				System.out.println(G510);
				steps.clickButton(home.Trend_FindUrSize_510radio, driver, name);
				Giftingbelow510 = driver.findElement(home.Trend_FindUrSize_510radio).getText();
				System.out.println("User Selected:  "+Giftingbelow510);
				test.log(Status.INFO, MarkupHelper.createLabel("User selected Height:  "+Giftingbelow510, ExtentColor.BLUE));
				steps.clickButton(home.Trend_FindUrSize_above510radio, driver, name);
				Giftingabove510 = driver.findElement(home.Trend_FindUrSize_above510radio).getText();
				System.out.println("User Selected:  "+Giftingabove510);
				test.log(Status.INFO, MarkupHelper.createLabel("User selected Height:  "+Giftingabove510, ExtentColor.BLUE));
				steps.clickButton(home.Trend_FindUrSize_Tightradio, driver, name);
				Giftingtightfit = driver.findElement(home.Trend_FindUrSize_Tightradio).getText();
				System.out.println("User Selected:  "+Giftingtightfit);
				test.log(Status.INFO, MarkupHelper.createLabel("User selected Fit:  "+Giftingtightfit, ExtentColor.BLUE));
				steps.clickButton(home.Trend_FindUrSize_Comfortradio, driver, name);
				Giftingcomfit = driver.findElement(home.Trend_FindUrSize_Comfortradio).getText();
				System.out.println("User Selected:  "+Giftingcomfit);
				test.log(Status.INFO, MarkupHelper.createLabel("User selected Fit:  "+Giftingcomfit, ExtentColor.BLUE));
				steps.selectDropdown(home.Trend_FindUrSize_sizeDD, driver, strDataFileName, "size", name);
				test.log(Status.INFO, MarkupHelper.createLabel("User selected shirt size", ExtentColor.BLUE));
				steps.clickButton(home.Trend_FindUrSize_submit, driver, name);
				test.log(Status.INFO, MarkupHelper.createLabel("User clicked on Submit", ExtentColor.BLUE));
				Giftingfindursize = getscreen.capture(driver, "Giftingfindursize");
				test.addScreenCaptureFromPath(Giftingfindursize);
				Thread.sleep(1000);
				steps.clickButton(home.Trend_FindUrSize_Okbutton, driver, name);
				test.log(Status.INFO, MarkupHelper.createLabel("system displayed size for given metrics", ExtentColor.BLUE));
				steps.clickButton(home.Trend_FindUrSize_closebutton, driver, name);
				test.log(Status.INFO, MarkupHelper.createLabel("User navigate to Product details page", ExtentColor.BLUE));
			}
			catch(Exception e)
			{
				steps.clickButton(home2.FindUrSize_closebutton, driver, name);
				test.log(Status.INFO, MarkupHelper.createLabel("User navigate to Product details page", ExtentColor.BLUE));
			}
			steps.clickButton(home2.Size_L, driver, name);
			test.log(Status.INFO, MarkupHelper.createLabel("User selected size", ExtentColor.BLUE));
			steps.clickButton(home.Trend_Addcart, driver, name);
			Thread.sleep(1000);
			test.log(Status.INFO, MarkupHelper.createLabel("User added product to Cart", ExtentColor.BLUE));
			Giftingaddcart = getscreen.capture(driver, "Giftingaddcart");
			test.addScreenCaptureFromPath(Giftingaddcart);
			Thread.sleep(1000);
			try
			{
				String edit = driver.findElement(home2.Addcart_edit).getText();
				System.out.println(edit);
				steps.clickButton(home2.Addcart_edit, driver, name);
				test.log(Status.INFO, MarkupHelper.createLabel("User clicked on Edit", ExtentColor.BLUE));
				steps.selectDropdown(home2.Addcart_selectsize, driver, strDataFileName, "SelectSize", name);
				steps.selectDropdown(home2.Addcart_selectqty, driver, strDataFileName, "Quantity", name);
				test.log(Status.INFO, MarkupHelper.createLabel("User selected Size and Quantity", ExtentColor.BLUE));
				steps.clickButton(home2.Addcart_cancel, driver, name);
				steps.clickButton(home2.Addcart_edit, driver, name);
				steps.selectDropdown(home2.Addcart_selectsize, driver, strDataFileName, "SelectSize", name);
				steps.selectDropdown(home2.Addcart_selectqty, driver, strDataFileName, "Quantity", name);
				steps.clickButton(home2.Addcart_save, driver, name);
				Giftingupdateproduct1 = getscreen.capture(driver, "Giftingupdateproduct1");
				test.addScreenCaptureFromPath(Giftingupdateproduct1);
				test.log(Status.INFO, MarkupHelper.createLabel("User updated product size and quantity", ExtentColor.BLUE));
			}
			catch(Exception e)
			{
				System.out.println("Edit feature is not available for this product");
			}
			Thread.sleep(2000);
			steps.clickButton(home2.Biker_remove, driver, name);
			Giftingremovecart = getscreen.capture(driver, "Giftingremovecart");
			test.addScreenCaptureFromPath(Giftingremovecart);
			test.log(Status.INFO, MarkupHelper.createLabel("User removed product from Cart", ExtentColor.BLUE));
			steps.clickButton(home.Size_L, driver, name);
			test.log(Status.INFO, MarkupHelper.createLabel("User selected size", ExtentColor.BLUE));
			steps.clickButton(home.BuyNow, driver, name);
			test.log(Status.INFO, MarkupHelper.createLabel("User clicked on BuyNow, navigated to checkout page", ExtentColor.BLUE));
			String Giftbuynow1 = getscreen.capture(driver, "Giftbuynow1");
			test.addScreenCaptureFromPath(Giftbuynow1);
			try
			{
				String cash = driver.findElement(home.CashonDeliveryText).getText();
				System.out.println("Payment method:  "+cash);
				steps.Verifytext(home.CashonDeliveryText, driver, cash, name);
				steps.clickButton(home.CashonDeliveryradio, driver, name);
				String Giftbuynow2 = driver.findElement(home.PopUpSaveMsg).getText();
				System.out.println("Displayed message:   "+Giftbuynow2);
				test.log(Status.INFO, MarkupHelper.createLabel("Displayed message:   "+Giftbuynow2, ExtentColor.BLUE));
				String	 Giftbuynow4 = getscreen.capture(driver, "Giftbuynow4");
				test.addScreenCaptureFromPath(Giftbuynow4);
				steps.clickButton(home.Cashondelivery, driver, name);
				test.log(Status.INFO, MarkupHelper.createLabel("User clicked on Cash on Delivery link", ExtentColor.BLUE));	
			}
			catch(Exception e)
			{
				System.out.println("Cash on Delivery is not available");
			}
			Thread.sleep(1000);
			steps.clickButton(home.CartIcon, driver, name);
			steps.clickButton(home.Trend_Addcart_remove, driver, name);
			test.log(Status.INFO, MarkupHelper.createLabel("User Completed transactions of 1st Image in Gifting", ExtentColor.PURPLE));
			driver.close();
			driver.switchTo().window(Parent);
			Giftingstatus = passstatus;
			Assert.assertTrue(true);
			System.out.println("Gifting Successful");
			testresultdata.put(Integer.toString(cnt), new Object[] {name, "Gifting", "Gifting Successful",passstatus});	
		}
		catch(Exception e)
		{
			System.out.println("Gifting Unsuccessful");
			testresultdata.put(Integer.toString(cnt), new Object[] {name, "Gifting", "Gifting Unsuccessful",failstatus});
			Giftingstatus = failstatus;
			Giftingstatusfail = getscreen.capture(driver, "Giftingstatusfail");
			test.addScreenCaptureFromPath(Giftingstatusfail);
			driver.close();
			Assert.assertTrue(false);
		}
	}
	@Test(priority = 9)
	public void Sports() throws IOException, InterruptedException
	{
		String name = new Object(){}.getClass().getEnclosingMethod().getName();
		cnt ++;
		try 
		{
			test = extent.createTest("Sports under What Inspires You Most");
			System.out.println("Sports under What Inspires You Most Test Case Executing...");
			Thread.sleep(1000);
			commonmethods.URLNaviagtion(driver);
			Thread.sleep(1000);
			/*commonmethods.Login(driver);
			test.log(Status.INFO, MarkupHelper.createLabel("User able to Login", ExtentColor.BLUE));
			Thread.sleep(3000);*/
			steps.clickButton(home.Mydreamstore, driver, name);
			String homepage = getscreen.capture(driver, name);
			test.addScreenCaptureFromPath(homepage);
			String Parent = driver.getWindowHandle();
			steps.MoveElement(home2.WIM_Image10, driver, name);
			steps.clickJSButton(home2.WIM_Image10, driver, name);
			test.log(Status.INFO, MarkupHelper.createLabel("User clicked on Sports Image under Who Inspires You Most", ExtentColor.BLUE));
			steps.ChildWindow(driver);
			driver.manage().window().maximize();
			SportsPage = getscreen.capture(driver, "SportsPage");
			test.addScreenCaptureFromPath(SportsPage);
			Thread.sleep(2000);
			try
			{
				String Filterpage1 = driver.findElement(home.Filterpagetext).getText();
				System.out.println("User navigated to :   "+Filterpage1);
			}
			catch(Exception e )
			{
				System.out.println("User navigated to different landing page");
				steps.clickButton(home2.Sports_imageBuyNow, driver, name);
				test.log(Status.INFO, MarkupHelper.createLabel("User clicked on 1st Image of Sports", ExtentColor.BLUE));
			}
			try
			{
				steps.clickButton(home2.Photography_Image1_Readmore, driver, name);
				SportsDescription = driver.findElement(home2.Sports_description).getText();
				System.out.println("Production Description   "+SportsDescription);
				test.log(Status.INFO, MarkupHelper.createLabel("Description about Item:  "+SportsDescription, ExtentColor.BLUE));
				steps.clickButton(home2.Profession_Readless, driver, name);
			}
			catch(Exception e)
			{
				steps.clickButton(home2.Photography_Image1_Readmore, driver, name);
				SportsDescription = driver.findElement(home2.Sports_description1).getText();
				System.out.println("Production Description   "+SportsDescription);
				test.log(Status.INFO, MarkupHelper.createLabel("Description about Item:  "+SportsDescription, ExtentColor.BLUE));
				steps.clickButton(home2.Profession_Readless, driver, name);
			}
			SportsMRP = driver.findElement(home.Trend_Image_MRP).getText();
			System.out.println("Product Price   "+SportsMRP);
			test.log(Status.INFO, MarkupHelper.createLabel("MRP of an Product:  "+SportsMRP, ExtentColor.ORANGE));
			try
			{
				String product2 = driver.findElement(home2.Sports_Product2).getCssValue("color");
				System.out.println("Product 2 exists:  "+product2);
				steps.clickButton(home2.Sports_Product2, driver, name);
				test.log(Status.INFO, MarkupHelper.createLabel("User selected Second Product", ExtentColor.BLUE));
				SportsProduct2 = getscreen.capture(driver, "SportsProduct2");
				test.addScreenCaptureFromPath(SportsProduct2);
			}
			catch(Exception e)
			{
				System.out.println("Product 2 is not available");
			}
			try
			{
				String col1 = driver.findElement(home2.color1).getCssValue("color");
				System.out.println("Color 2 exists:  "+col1);
				steps.clickButton(home2.color1, driver, name);
				Thread.sleep(2000);
				SportsWColor1 = driver.findElement(home2.color1).getCssValue("background-color");
				SportsWhex1 = Color.fromString(SportsWColor1).asHex();
				System.out.println("Image color: "+SportsWhex1);
				test.log(Status.INFO, MarkupHelper.createLabel("User changed item color to:  "+SportsWhex1, ExtentColor.BLUE));
				SportsWFirstcolor = getscreen.capture(driver, "SportsWFirstcolor");
				test.addScreenCaptureFromPath(SportsWFirstcolor);
			}
			catch(Exception e)
			{
				System.out.println("Color 1 is not exists");
			}
			try
			{
				String col2 = driver.findElement(home2.color2).getCssValue("color");
				System.out.println("Color 3 exists:  "+col2);
				steps.clickButton(home2.color2, driver, name);
				Thread.sleep(1000);
				SportsWColor2 = driver.findElement(home2.color2).getCssValue("background-color");
				SportsWhex2 = Color.fromString(SportsWColor2).asHex();
				System.out.println("Image color: "+SportsWhex2);
				test.log(Status.INFO, MarkupHelper.createLabel("User changed item color to:  "+SportsWhex2, ExtentColor.BLUE));
				SportsWSecondcolor = getscreen.capture(driver, "SportsWSecondcolor");
				test.addScreenCaptureFromPath(SportsWSecondcolor);
			}
			catch(Exception e)
			{
				System.out.println("Color 2 is not exists");
			}
			try
			{
				String col3 = driver.findElement(home2.color3).getCssValue("color");
				System.out.println("Color 4 exists:  "+col3);
				steps.clickButton(home2.color3, driver, name);
				Thread.sleep(1000);
				SportsWColor3 = driver.findElement(home2.color3).getCssValue("background-color");
				SportsWhex3 = Color.fromString(SportsWColor3).asHex();
				System.out.println("Image color: "+SportsWhex3);
				test.log(Status.INFO, MarkupHelper.createLabel("User changed item color to:  "+SportsWhex3, ExtentColor.BLUE));
				SportsWThirdcolor = getscreen.capture(driver, "SportsWThirdcolor");
				test.addScreenCaptureFromPath(SportsWThirdcolor);
			}
			catch(Exception e)
			{
				System.out.println("Color 3 is not exists");
			}
			try 
			{
				String col4 = driver.findElement(home2.color4).getCssValue("color");
				System.out.println("Color 1 exists:  "+col4);
				steps.clickButton(home2.color4, driver, name);
				Thread.sleep(1000);
				SportsWColor4 = driver.findElement(home2.color4).getCssValue("background-color");
				SportsWhex4 = Color.fromString(SportsWColor4).asHex();
				System.out.println("Image color: "+SportsWhex4);
				test.log(Status.INFO, MarkupHelper.createLabel("User changed item color to:  "+SportsWhex4, ExtentColor.BLUE));
				SportsWFourthcolor = getscreen.capture(driver, "SportsWFourthcolor");
				test.addScreenCaptureFromPath(SportsWFourthcolor);
			}
			catch(Exception e)
			{
				System.out.println("Color 4 is not exists");
			}
			Thread.sleep(2000);
			try
			{
				String product1 = driver.findElement(home2.Sports_Product1).getCssValue("color");
				System.out.println("Product 1 exists:  "+product1);
				steps.clickButton(home2.Sports_Product1, driver, name);
				test.log(Status.INFO, MarkupHelper.createLabel("User selected First Product", ExtentColor.BLUE));
				SportsProduct1 = getscreen.capture(driver, "SportsProduct1");
				test.addScreenCaptureFromPath(SportsProduct1);
			}
			catch(Exception e)
			{
				System.out.println("Product 1 is not available");
			}	
			Thread.sleep(2000);
			Sportscqty = driver.findElement(home.Qty).getAttribute("value");
			System.out.println("Current Quantity:  "+ Sportscqty);
			test.log(Status.INFO, MarkupHelper.createLabel("Current Quantity of product:  "+Sportscqty, ExtentColor.BLUE));
			Thread.sleep(1000);
			steps.clickButton(home.Trend_QtyIncrease, driver, name);
			Sportsiqty = driver.findElement(home.Qty).getAttribute("value");
			System.out.println("Quantity after increase:  "+ Sportsiqty);
			test.log(Status.INFO, MarkupHelper.createLabel("Current Quantity of product after Increase:  "+Sportsiqty, ExtentColor.BLUE));
			steps.clickButton(home.Trend_QtyDecrease, driver, name);
			Sportsdqty = driver.findElement(home.Qty).getAttribute("value");
			System.out.println("Quantity after decrease:  "+ Sportsdqty);
			test.log(Status.INFO, MarkupHelper.createLabel("Current Quantity of product after increase:  "+Sportsdqty, ExtentColor.BLUE));
			steps.clickButton(home.Trend_Addcart, driver, name);
			SportsSizemsg = driver.findElement(home.Trend_Sizeerrormsg).getText();
			System.out.println("User clicked on Addcart before Size selection:  "+SportsSizemsg);
			test.log(Status.INFO, MarkupHelper.createLabel("User clicked on Addcart before Size selection:  "+SportsSizemsg, ExtentColor.RED));
			//steps.clickJSButton(home2.Sports_sizeS, driver, name);
			steps.clickJSButton(home2.Size_S, driver, name);
			SportssizeS = driver.findElement(home2.Size_S).getText();
			System.out.println("Selected Size:  "+SportssizeS);
			test.log(Status.INFO, MarkupHelper.createLabel("User selected size:  "+SportssizeS, ExtentColor.BLUE));
			steps.clickButton(home2.Size_M, driver, name);
			SportssizeM = driver.findElement(home2.Size_M).getText();
			System.out.println("Selected Size:  "+SportssizeM);
			test.log(Status.INFO, MarkupHelper.createLabel("User selected size:  "+SportssizeM, ExtentColor.BLUE));
			steps.clickButton(home2.Size_L, driver, name);
			SportssizeL = driver.findElement(home2.Size_L).getText();
			System.out.println("Selected Size:  "+SportssizeL);
			test.log(Status.INFO, MarkupHelper.createLabel("User selected size:  "+SportssizeL, ExtentColor.BLUE));
			steps.clickButton(home2.Size_XL, driver, name);
			SportssizeXL = driver.findElement(home2.Size_XL).getText();
			System.out.println("Selected Size:  "+SportssizeXL);
			test.log(Status.INFO, MarkupHelper.createLabel("User selected size:  "+SportssizeXL, ExtentColor.BLUE));
			steps.clickButton(home2.Size_XXL, driver, name);
			SportssizeXXL = driver.findElement(home2.Size_XXL).getText();
			System.out.println("Selected Size:  "+SportssizeXXL);
			test.log(Status.INFO, MarkupHelper.createLabel("User selected size:  "+SportssizeXXL, ExtentColor.BLUE));
			steps.clickButton(home2.Size_XXXL, driver, name);
			SportssizeXXXL = driver.findElement(home2.Size_XXXL).getText();
			System.out.println("Selected Size:  "+SportssizeXXXL);
			test.log(Status.INFO, MarkupHelper.createLabel("User selected size:  "+SportssizeXXXL, ExtentColor.BLUE));
			steps.clickButton(home.Trend_FindUrSize, driver, name);
			test.log(Status.INFO, MarkupHelper.createLabel("User clicked on Find your size", ExtentColor.BLUE));
			Thread.sleep(1000);
			try
			{
				String S510 = driver.findElement(home.Trend_FindUrSize_510radio).getText();
				System.out.println(S510);
				steps.clickButton(home.Trend_FindUrSize_510radio, driver, name);
				Sportsbelow510 = driver.findElement(home.Trend_FindUrSize_510radio).getText();
				System.out.println("User Selected:  "+Sportsbelow510);
				test.log(Status.INFO, MarkupHelper.createLabel("User selected Height:  "+Sportsbelow510, ExtentColor.BLUE));
				steps.clickButton(home.Trend_FindUrSize_above510radio, driver, name);
				Sportsabove510 = driver.findElement(home.Trend_FindUrSize_above510radio).getText();
				System.out.println("User Selected:  "+Sportsabove510);
				test.log(Status.INFO, MarkupHelper.createLabel("User selected Height:  "+Sportsabove510, ExtentColor.BLUE));
				steps.clickButton(home.Trend_FindUrSize_Tightradio, driver, name);
				Sportstightfit = driver.findElement(home.Trend_FindUrSize_Tightradio).getText();
				System.out.println("User Selected:  "+Sportstightfit);
				test.log(Status.INFO, MarkupHelper.createLabel("User selected Fit:  "+Sportstightfit, ExtentColor.BLUE));
				steps.clickButton(home.Trend_FindUrSize_Comfortradio, driver, name);
				Sportscomfit = driver.findElement(home.Trend_FindUrSize_Comfortradio).getText();
				System.out.println("User Selected:  "+Sportscomfit);
				test.log(Status.INFO, MarkupHelper.createLabel("User selected Fit:  "+Sportscomfit, ExtentColor.BLUE));
				Thread.sleep(1000);
				steps.selectDropdown(home.Trend_FindUrSize_sizeDD, driver, strDataFileName, "size", name);
				test.log(Status.INFO, MarkupHelper.createLabel("User selected shirt size", ExtentColor.BLUE));
				steps.clickButton(home.Trend_FindUrSize_submit, driver, name);
				test.log(Status.INFO, MarkupHelper.createLabel("User clicked on Submit", ExtentColor.BLUE));
				Sportsfindursize = getscreen.capture(driver, "Sportsfindursize");
				test.addScreenCaptureFromPath(Sportsfindursize);
				Thread.sleep(1000);
				steps.clickButton(home.Trend_FindUrSize_Okbutton, driver, name);
				test.log(Status.INFO, MarkupHelper.createLabel("system displayed size for given metrics", ExtentColor.BLUE));
				steps.clickButton(home.Trend_FindUrSize_closebutton, driver, name);
				test.log(Status.INFO, MarkupHelper.createLabel("User navigate to Product details page", ExtentColor.BLUE));
			}
			catch(Exception e)
			{
				steps.clickButton(home2.FindUrSize_closebutton, driver, name);
				test.log(Status.INFO, MarkupHelper.createLabel("User navigate to Product details page", ExtentColor.BLUE));
			}
			steps.clickButton(home2.Size_L, driver, name);
			test.log(Status.INFO, MarkupHelper.createLabel("User selected size", ExtentColor.BLUE));
			steps.clickButton(home.Trend_Addcart, driver, name);
			Thread.sleep(1000);
			test.log(Status.INFO, MarkupHelper.createLabel("User added product to Cart", ExtentColor.BLUE));
			Sportsaddcart = getscreen.capture(driver, "Sportsaddcart");
			test.addScreenCaptureFromPath(Sportsaddcart);
			Thread.sleep(1000);
			try
			{
				String edit = driver.findElement(home2.Addcart_edit).getText();
				System.out.println(edit);
				steps.clickButton(home2.Addcart_edit, driver, name);
				test.log(Status.INFO, MarkupHelper.createLabel("User clicked on Edit", ExtentColor.BLUE));
				steps.selectDropdown(home2.Addcart_selectsize, driver, strDataFileName, "SelectSize", name);
				steps.selectDropdown(home2.Addcart_selectqty, driver, strDataFileName, "Quantity", name);
				test.log(Status.INFO, MarkupHelper.createLabel("User selected Size and Quantity", ExtentColor.BLUE));
				steps.clickButton(home2.Addcart_cancel, driver, name);
				steps.clickButton(home2.Addcart_edit, driver, name);
				steps.selectDropdown(home2.Addcart_selectsize, driver, strDataFileName, "SelectSize", name);
				steps.selectDropdown(home2.Addcart_selectqty, driver, strDataFileName, "Quantity", name);
				steps.clickButton(home2.Addcart_save, driver, name);
				Sportsupdateproduct1 = getscreen.capture(driver, "Sportsupdateproduct1");
				test.addScreenCaptureFromPath(Sportsupdateproduct1);
				test.log(Status.INFO, MarkupHelper.createLabel("User updated product size and quantity", ExtentColor.BLUE));
			}
			catch(Exception e)
			{
				System.out.println("Edit feature is not available for this product");
			}
			Thread.sleep(2000);
			steps.clickButton(home2.Biker_remove, driver, name);
			Sportsremovecart = getscreen.capture(driver, "Sportsremovecart");
			test.addScreenCaptureFromPath(Sportsremovecart);
			test.log(Status.INFO, MarkupHelper.createLabel("User removed product from Cart", ExtentColor.BLUE));
			steps.clickButton(home.Size_L, driver, name);
			test.log(Status.INFO, MarkupHelper.createLabel("User selected size", ExtentColor.BLUE));
			steps.clickButton(home.BuyNow, driver, name);
			test.log(Status.INFO, MarkupHelper.createLabel("User clicked on BuyNow, navigated to checkout page", ExtentColor.BLUE));
			String Sportbuynow1 = getscreen.capture(driver, "Sportbuynow1");
			test.addScreenCaptureFromPath(Sportbuynow1);
			try
			{
				String cash = driver.findElement(home.CashonDeliveryText).getText();
				System.out.println("Payment method:  "+cash);
				steps.Verifytext(home.CashonDeliveryText, driver, cash, name);
				steps.clickButton(home.CashonDeliveryradio, driver, name);
				String Sportbuynow2 = driver.findElement(home.PopUpSaveMsg).getText();
				System.out.println("Displayed message:   "+Sportbuynow2);
				test.log(Status.INFO, MarkupHelper.createLabel("Displayed message:   "+Sportbuynow2, ExtentColor.BLUE));
				String	 Sportbuynow4 = getscreen.capture(driver, "Sportbuynow4");
				test.addScreenCaptureFromPath(Sportbuynow4);
				steps.clickButton(home.Cashondelivery, driver, name);
				test.log(Status.INFO, MarkupHelper.createLabel("User clicked on Cash on Delivery link", ExtentColor.BLUE));	
			}
			catch(Exception e)
			{
				System.out.println("Cash on Delivery is not available");
			}
			Thread.sleep(1000);
			steps.clickButton(home.CartIcon, driver, name);
			steps.clickButton(home.Trend_Addcart_remove, driver, name);
			test.log(Status.INFO, MarkupHelper.createLabel("User Completed transactions of 1st Image in Sports", ExtentColor.PURPLE));
			driver.close();
			driver.switchTo().window(Parent);
			Sportsstatus = passstatus;
			Assert.assertTrue(true);
			System.out.println("Sports Successful");
			testresultdata.put(Integer.toString(cnt), new Object[] {name, "Sports", "Sports Successful",passstatus});	
		}
		catch(Exception e)
		{
			System.out.println("Sports Unsuccessful");
			testresultdata.put(Integer.toString(cnt), new Object[] {name, "Sports", "Sports Unsuccessful",failstatus});
			Sportsstatus = failstatus;
			Sportsstatusfail = getscreen.capture(driver, "Sportsstatusfail");
			test.addScreenCaptureFromPath(Sportsstatusfail);
			driver.close();
			Assert.assertTrue(false);
		}
	}
	@Test(priority = 10)
	public void Humour() throws IOException, InterruptedException
	{
		String name = new Object(){}.getClass().getEnclosingMethod().getName();
		cnt ++;
		try 
		{
			test = extent.createTest("Humour under What Inspires You Most");
			System.out.println("Humour under What Inspires You Most Test Case Executing...");
			Thread.sleep(1000);
			commonmethods.URLNaviagtion(driver);
			Thread.sleep(1000);
			/*commonmethods.Login(driver);
			test.log(Status.INFO, MarkupHelper.createLabel("User able to Login", ExtentColor.BLUE));
			Thread.sleep(3000);*/
			steps.clickButton(home.Mydreamstore, driver, name);
			String homepage = getscreen.capture(driver, name);
			test.addScreenCaptureFromPath(homepage);
			String Parent = driver.getWindowHandle();
			steps.MoveElement(home2.WIM_Image11, driver, name);
			steps.clickJSButton(home2.WIM_Image11, driver, name);
			test.log(Status.INFO, MarkupHelper.createLabel("User clicked on Humour Image under Who Inspires You Most", ExtentColor.BLUE));
			steps.ChildWindow(driver);
			driver.manage().window().maximize();
			HumourPage = getscreen.capture(driver, "HumourPage");
			test.addScreenCaptureFromPath(HumourPage);
			Thread.sleep(2000);
			try
			{
				String Filterpage1 = driver.findElement(home.Filterpagetext).getText();
				System.out.println("User navigated to :   "+Filterpage1);
			}
			catch(Exception e )
			{
				System.out.println("User navigated to different landing page");
				steps.clickButton(home2.Humour_image1, driver, name);
				test.log(Status.INFO, MarkupHelper.createLabel("User clicked on 1st Image of Humour", ExtentColor.BLUE));
				HumourPage1 = getscreen.capture(driver, "HumourPage1");
				test.addScreenCaptureFromPath(HumourPage1);
				driver.close();
				steps.ChildWindow(driver);
				driver.manage().window().maximize();
				HumourPage2 = getscreen.capture(driver, "HumourPage2");
				test.addScreenCaptureFromPath(HumourPage2);
			}
			Thread.sleep(2000);
			try
			{
				steps.clickButton(home2.Photography_Image1_Readmore, driver, name);
				HumourDescription = driver.findElement(home2.Humour_description).getText();
				System.out.println("Production Description   "+HumourDescription);
				test.log(Status.INFO, MarkupHelper.createLabel("Description about Item:  "+HumourDescription, ExtentColor.BLUE));
				steps.clickButton(home2.Profession_Readless, driver, name);
			}
			catch(Exception e)
			{
				steps.clickButton(home2.Photography_Image1_Readmore, driver, name);
				HumourDescription = driver.findElement(home2.Humour_description1).getText();
				System.out.println("Production Description   "+HumourDescription);
				test.log(Status.INFO, MarkupHelper.createLabel("Description about Item:  "+HumourDescription, ExtentColor.BLUE));
				steps.clickButton(home2.Profession_Readless, driver, name);
			}
			HumourMRP = driver.findElement(home.Trend_Image_MRP).getText();
			System.out.println("Product Price   "+HumourMRP);
			test.log(Status.INFO, MarkupHelper.createLabel("MRP of an Product:  "+HumourMRP, ExtentColor.ORANGE));
			try
			{
				String col1 = driver.findElement(home2.color1).getCssValue("color");
				System.out.println("Color 2 exists:  "+col1);
				steps.clickButton(home2.color1, driver, name);
				Thread.sleep(2000);
				HumourMColor1 = driver.findElement(home2.color1).getCssValue("background-color");
				HumourMhex1 = Color.fromString(HumourMColor1).asHex();
				System.out.println("Image color: "+HumourMhex1);
				test.log(Status.INFO, MarkupHelper.createLabel("User changed item color to:  "+HumourMhex1, ExtentColor.BLUE));
				HumourMFirstcolor = getscreen.capture(driver, "HumourMFirstcolor");
				test.addScreenCaptureFromPath(HumourMFirstcolor);
			}
			catch(Exception e)
			{
				System.out.println("Color 1 is not exists");
			}
			try
			{
				String col2 = driver.findElement(home2.color2).getCssValue("color");
				System.out.println("Color 3 exists:  "+col2);
				steps.clickButton(home2.color2, driver, name);
				Thread.sleep(1000);
				HumourMColor2 = driver.findElement(home2.color2).getCssValue("background-color");
				HumourMhex2 = Color.fromString(HumourMColor2).asHex();
				System.out.println("Image color: "+HumourMhex2);
				test.log(Status.INFO, MarkupHelper.createLabel("User changed item color to:  "+HumourMhex2, ExtentColor.BLUE));
				HumourMSecondcolor = getscreen.capture(driver, "HumourMSecondcolor");
				test.addScreenCaptureFromPath(HumourMSecondcolor);
			}
			catch(Exception e)
			{
				System.out.println("Color 2 is not exists");
			}
			try
			{
				String col3 = driver.findElement(home2.color3).getCssValue("color");
				System.out.println("Color 4 exists:  "+col3);
				steps.clickButton(home2.color3, driver, name);
				Thread.sleep(1000);
				HumourMColor3 = driver.findElement(home2.color3).getCssValue("background-color");
				HumourMhex3 = Color.fromString(HumourMColor3).asHex();
				System.out.println("Image color: "+HumourMhex3);
				test.log(Status.INFO, MarkupHelper.createLabel("User changed item color to:  "+HumourMhex3, ExtentColor.BLUE));
				HumourMThirdcolor = getscreen.capture(driver, "HumourMThirdcolor");
				test.addScreenCaptureFromPath(HumourMThirdcolor);
			}
			catch(Exception e)
			{
				System.out.println("Color 3 is not exists");
			}
			try
			{
				String col4 = driver.findElement(home2.color4).getCssValue("color");
				System.out.println("Color 1 exists:  "+col4);
				steps.clickButton(home2.color4, driver, name);
				Thread.sleep(1000);
				HumourMColor4 = driver.findElement(home2.color4).getCssValue("background-color");
				HumourMhex4 = Color.fromString(HumourMColor4).asHex();
				System.out.println("Image color: "+HumourMhex4);
				test.log(Status.INFO, MarkupHelper.createLabel("User changed item color to:  "+HumourMhex4, ExtentColor.BLUE));
				HumourMFourthcolor = getscreen.capture(driver, "HumourMFourthcolor");
				test.addScreenCaptureFromPath(HumourMFourthcolor);
			}
			catch(Exception e)
			{
				System.out.println("Color 4 is not exists");
			}
			Thread.sleep(2000);
			Humourcqty = driver.findElement(home.Qty).getAttribute("value");
			System.out.println("Current Quantity:  "+ Humourcqty);
			test.log(Status.INFO, MarkupHelper.createLabel("Current Quantity of product:  "+Humourcqty, ExtentColor.BLUE));
			Thread.sleep(1000);
			steps.clickButton(home.Trend_QtyIncrease, driver, name);
			Humouriqty = driver.findElement(home.Qty).getAttribute("value");
			System.out.println("Quantity after increase:  "+ Humouriqty);
			test.log(Status.INFO, MarkupHelper.createLabel("Current Quantity of product after Increase:  "+Humouriqty, ExtentColor.BLUE));
			steps.clickButton(home.Trend_QtyDecrease, driver, name);
			Humourdqty = driver.findElement(home.Qty).getAttribute("value");
			System.out.println("Quantity after decrease:  "+ Humourdqty);
			test.log(Status.INFO, MarkupHelper.createLabel("Current Quantity of product after increase:  "+Humourdqty, ExtentColor.BLUE));
			steps.clickButton(home.Trend_Addcart, driver, name);
			HumourSizemsg = driver.findElement(home.Trend_Sizeerrormsg).getText();
			System.out.println("User clicked on Addcart before Size selection:  "+HumourSizemsg);
			test.log(Status.INFO, MarkupHelper.createLabel("User clicked on Addcart before Size selection:  "+HumourSizemsg, ExtentColor.RED));
			//steps.clickJSButton(home2.Humour_sizeS, driver, name);
			steps.clickJSButton(home2.Size_S, driver, name);
			HumoursizeS = driver.findElement(home2.Size_S).getText();
			System.out.println("Selected Size:  "+HumoursizeS);
			test.log(Status.INFO, MarkupHelper.createLabel("User selected size:  "+HumoursizeS, ExtentColor.BLUE));
			steps.clickButton(home2.Size_M, driver, name);
			HumoursizeM = driver.findElement(home2.Size_M).getText();
			System.out.println("Selected Size:  "+HumoursizeM);
			test.log(Status.INFO, MarkupHelper.createLabel("User selected size:  "+HumoursizeM, ExtentColor.BLUE));
			steps.clickButton(home2.Size_L, driver, name);
			HumoursizeL = driver.findElement(home2.Size_L).getText();
			System.out.println("Selected Size:  "+HumoursizeL);
			test.log(Status.INFO, MarkupHelper.createLabel("User selected size:  "+HumoursizeL, ExtentColor.BLUE));
			steps.clickButton(home2.Size_XL, driver, name);
			HumoursizeXL = driver.findElement(home2.Size_XL).getText();
			System.out.println("Selected Size:  "+HumoursizeXL);
			test.log(Status.INFO, MarkupHelper.createLabel("User selected size:  "+HumoursizeXL, ExtentColor.BLUE));
			steps.clickButton(home2.Size_XXL, driver, name);
			HumoursizeXXL = driver.findElement(home2.Size_XXL).getText();
			System.out.println("Selected Size:  "+HumoursizeXXL);
			test.log(Status.INFO, MarkupHelper.createLabel("User selected size:  "+HumoursizeXXL, ExtentColor.BLUE));
			steps.clickButton(home2.Size_XXXL, driver, name);
			HumoursizeXXXL = driver.findElement(home2.Size_XXXL).getText();
			System.out.println("Selected Size:  "+HumoursizeXXXL);
			test.log(Status.INFO, MarkupHelper.createLabel("User selected size:  "+HumoursizeXXXL, ExtentColor.BLUE));
			steps.clickButton(home.Trend_FindUrSize, driver, name);
			test.log(Status.INFO, MarkupHelper.createLabel("User clicked on Find your size", ExtentColor.BLUE));
			Thread.sleep(1000);
			try
			{
				String H510 = driver.findElement(home.Trend_FindUrSize_510radio).getText();
				System.out.println(H510);
				steps.clickButton(home.Trend_FindUrSize_510radio, driver, name);
				Humourbelow510 = driver.findElement(home.Trend_FindUrSize_510radio).getText();
				System.out.println("User Selected:  "+Humourbelow510);
				test.log(Status.INFO, MarkupHelper.createLabel("User selected Height:  "+Humourbelow510, ExtentColor.BLUE));
				steps.clickButton(home.Trend_FindUrSize_above510radio, driver, name);
				Humourabove510 = driver.findElement(home.Trend_FindUrSize_above510radio).getText();
				System.out.println("User Selected:  "+Humourabove510);
				test.log(Status.INFO, MarkupHelper.createLabel("User selected Height:  "+Humourabove510, ExtentColor.BLUE));
				steps.clickButton(home.Trend_FindUrSize_Tightradio, driver, name);
				Humourtightfit = driver.findElement(home.Trend_FindUrSize_Tightradio).getText();
				System.out.println("User Selected:  "+Humourtightfit);
				test.log(Status.INFO, MarkupHelper.createLabel("User selected Fit:  "+Humourtightfit, ExtentColor.BLUE));
				steps.clickButton(home.Trend_FindUrSize_Comfortradio, driver, name);
				Humourcomfit = driver.findElement(home.Trend_FindUrSize_Comfortradio).getText();
				System.out.println("User Selected:  "+Humourcomfit);
				test.log(Status.INFO, MarkupHelper.createLabel("User selected Fit:  "+Humourcomfit, ExtentColor.BLUE));
				Thread.sleep(1000);
				steps.selectDropdown(home.Trend_FindUrSize_sizeDD, driver, strDataFileName, "size", name);
				test.log(Status.INFO, MarkupHelper.createLabel("User selected shirt size", ExtentColor.BLUE));
				steps.clickButton(home.Trend_FindUrSize_submit, driver, name);
				test.log(Status.INFO, MarkupHelper.createLabel("User clicked on Submit", ExtentColor.BLUE));
				Humourfindursize = getscreen.capture(driver, "Humourfindursize");
				test.addScreenCaptureFromPath(Humourfindursize);
				Thread.sleep(1000);
				steps.clickButton(home.Trend_FindUrSize_Okbutton, driver, name);
				test.log(Status.INFO, MarkupHelper.createLabel("system displayed size for given metrics", ExtentColor.BLUE));
				steps.clickButton(home.Trend_FindUrSize_closebutton, driver, name);
				test.log(Status.INFO, MarkupHelper.createLabel("User navigate to Product details page", ExtentColor.BLUE));
			}
			catch(Exception e)
			{
				steps.clickButton(home2.FindUrSize_closebutton, driver, name);
				test.log(Status.INFO, MarkupHelper.createLabel("User navigate to Product details page", ExtentColor.BLUE));
			}
			steps.clickButton(home2.Size_L, driver, name);
			test.log(Status.INFO, MarkupHelper.createLabel("User selected size", ExtentColor.BLUE));
			steps.clickButton(home.Trend_Addcart, driver, name);
			Thread.sleep(1000);
			test.log(Status.INFO, MarkupHelper.createLabel("User added product to Cart", ExtentColor.BLUE));
			Humouraddcart = getscreen.capture(driver, "Humouraddcart");
			test.addScreenCaptureFromPath(Humouraddcart);
			Thread.sleep(1000);
			try
			{
				String edit = driver.findElement(home2.Addcart_edit).getText();
				System.out.println(edit);
				steps.clickButton(home2.Addcart_edit, driver, name);
				test.log(Status.INFO, MarkupHelper.createLabel("User clicked on Edit", ExtentColor.BLUE));
				steps.selectDropdown(home2.Addcart_selectsize, driver, strDataFileName, "SelectSize", name);
				steps.selectDropdown(home2.Addcart_selectqty, driver, strDataFileName, "Quantity", name);
				test.log(Status.INFO, MarkupHelper.createLabel("User selected Size and Quantity", ExtentColor.BLUE));
				steps.clickButton(home2.Addcart_cancel, driver, name);
				steps.clickButton(home2.Addcart_edit, driver, name);
				steps.selectDropdown(home2.Addcart_selectsize, driver, strDataFileName, "SelectSize", name);
				steps.selectDropdown(home2.Addcart_selectqty, driver, strDataFileName, "Quantity", name);
				steps.clickButton(home2.Addcart_save, driver, name);
				Humourupdateproduct1 = getscreen.capture(driver, "Humourupdateproduct1");
				test.addScreenCaptureFromPath(Humourupdateproduct1);
				test.log(Status.INFO, MarkupHelper.createLabel("User updated product size and quantity", ExtentColor.BLUE));
			}
			catch(Exception e)
			{
				System.out.println("Edit feature is not available for this product");
			}
			Thread.sleep(2000);
			steps.clickButton(home2.Biker_remove, driver, name);
			Humourremovecart = getscreen.capture(driver, "Humourremovecart");
			test.addScreenCaptureFromPath(Humourremovecart);
			test.log(Status.INFO, MarkupHelper.createLabel("User removed product from Cart", ExtentColor.BLUE));
			steps.clickButton(home.Size_L, driver, name);
			test.log(Status.INFO, MarkupHelper.createLabel("User selected size", ExtentColor.BLUE));
			steps.clickButton(home.BuyNow, driver, name);
			test.log(Status.INFO, MarkupHelper.createLabel("User clicked on BuyNow, navigated to checkout page", ExtentColor.BLUE));
			String Humbuynow1 = getscreen.capture(driver, "Humbuynow1");
			test.addScreenCaptureFromPath(Humbuynow1);
			try
			{
				String cash = driver.findElement(home.CashonDeliveryText).getText();
				System.out.println("Payment method:  "+cash);
				steps.Verifytext(home.CashonDeliveryText, driver, cash, name);
				steps.clickButton(home.CashonDeliveryradio, driver, name);
				String Humbuynow2 = driver.findElement(home.PopUpSaveMsg).getText();
				System.out.println("Displayed message:   "+Humbuynow2);
				test.log(Status.INFO, MarkupHelper.createLabel("Displayed message:   "+Humbuynow2, ExtentColor.BLUE));
				String	 Humbuynow4 = getscreen.capture(driver, "Humbuynow4");
				test.addScreenCaptureFromPath(Humbuynow4);
				steps.clickButton(home.Cashondelivery, driver, name);
				test.log(Status.INFO, MarkupHelper.createLabel("User clicked on Cash on Delivery link", ExtentColor.BLUE));	
			}
			catch(Exception e)
			{
				System.out.println("Cash on Delivery is not available");
			}
			Thread.sleep(1000);
			steps.clickButton(home.CartIcon, driver, name);
			steps.clickButton(home.Trend_Addcart_remove, driver, name);
			test.log(Status.INFO, MarkupHelper.createLabel("User Completed transactions of 1st Image in Humour", ExtentColor.PURPLE));
			driver.close();
			driver.switchTo().window(Parent);
			Humourstatus = passstatus;
			Assert.assertTrue(true);
			System.out.println("Humour Successful");
			testresultdata.put(Integer.toString(cnt), new Object[] {name, "Humour", "Humour Successful",passstatus});	
		}
		catch(Exception e)
		{
			System.out.println("Humour Unsuccessful");
			testresultdata.put(Integer.toString(cnt), new Object[] {name, "Humour", "Humour Unsuccessful",failstatus});
			Humourstatus = failstatus;
			Humourstatusfail = getscreen.capture(driver, "Humourstatusfail");
			test.addScreenCaptureFromPath(Humourstatusfail);
			driver.close();
			Assert.assertTrue(false);
		}
	}
	@Test(priority = 11)
	public void Developer() throws IOException, InterruptedException
	{
		String name = new Object(){}.getClass().getEnclosingMethod().getName();
		cnt ++;
		try 
		{
			test = extent.createTest("Developer under What Inspires You Most");
			System.out.println("Developer under What Inspires You Most Test Case Executing...");
			Thread.sleep(1000);
			commonmethods.URLNaviagtion(driver);
			Thread.sleep(1000);
			/*commonmethods.Login(driver);
			test.log(Status.INFO, MarkupHelper.createLabel("User able to Login", ExtentColor.BLUE));
			Thread.sleep(3000);*/
			steps.clickButton(home.Mydreamstore, driver, name);
			String homepage = getscreen.capture(driver, name);
			test.addScreenCaptureFromPath(homepage);
			String Parent = driver.getWindowHandle();
			steps.MoveElement(home2.WIM_Image12, driver, name);
			steps.clickJSButton(home2.WIM_Image12, driver, name);
			test.log(Status.INFO, MarkupHelper.createLabel("User clicked on Developer Image under Who Inspires You Most", ExtentColor.BLUE));
			steps.ChildWindow(driver);
			driver.manage().window().maximize();
			DeveloperPage = getscreen.capture(driver, "DeveloperPage");
			test.addScreenCaptureFromPath(DeveloperPage);
			Thread.sleep(2000);
			try
			{
				String Filterpage1 = driver.findElement(home.Filterpagetext).getText();
				System.out.println("User navigated to :   "+Filterpage1);
				steps.clickButton(home2.Developer_image1, driver, name);
				test.log(Status.INFO, MarkupHelper.createLabel("User clicked on 1st Image of Developer", ExtentColor.BLUE));
				DeveloperPage1 = getscreen.capture(driver, "DeveloperPage1");
				test.addScreenCaptureFromPath(DeveloperPage1);
			}
			catch(Exception e )
			{
				System.out.println("User navigated to wrong page");
			}
			Thread.sleep(2000);
			try
			{
				steps.clickButton(home.Trend_Image_Readmore, driver, name);
				DeveloperDescription1 = driver.findElement(home2.Developer_description).getText();
				System.out.println("Production Description   "+DeveloperDescription1);
				steps.clickButton(home.Trend_Image_Readless, driver, name);
				test.log(Status.INFO, MarkupHelper.createLabel("Description about Item:  "+DeveloperDescription1 , ExtentColor.BLUE));
			}
			catch(Exception e)
			{
				steps.clickButton(home.Trend_Image_Readmore, driver, name);
				DeveloperDescription4 = driver.findElement(home2.Developer_description1).getText();
				System.out.println("Production Description   "+DeveloperDescription4);
				test.log(Status.INFO, MarkupHelper.createLabel("Description about Item:  "+DeveloperDescription4 , ExtentColor.BLUE));
			}
			DeveloperMRP = driver.findElement(home.Trend_Image_MRP).getText();
			System.out.println("Product Price   "+DeveloperMRP);
			test.log(Status.INFO, MarkupHelper.createLabel("MRP of an Product:  "+DeveloperMRP, ExtentColor.ORANGE));
			Thread.sleep(1000);
			try
			{	
				String col1 = driver.findElement(home2.color1).getCssValue("color");
				System.out.println("Color 2 exists:  "+col1);
				steps.clickButton(home2.color1, driver, name);
				Thread.sleep(2000);
				DeveloperMColor1 = driver.findElement(home2.color1).getCssValue("background-color");
				DeveloperMhex1 = Color.fromString(DeveloperMColor1).asHex();
				System.out.println("Image color: "+DeveloperMhex1);
				test.log(Status.INFO, MarkupHelper.createLabel("User changed item color to:  "+DeveloperMhex1, ExtentColor.BLUE));
				DeveloperMFirstcolor = getscreen.capture(driver, "DeveloperMFirstcolor");
				test.addScreenCaptureFromPath(DeveloperMFirstcolor);
			}
			catch(Exception e)
			{
				System.out.println("Color 1 is not available");
			}
			try
			{
				String col2 = driver.findElement(home2.color2).getCssValue("color");
				System.out.println("Color 3 exists:  "+col2);
				steps.clickButton(home2.color2, driver, name);
				Thread.sleep(1000);
				DeveloperMColor2 = driver.findElement(home2.color2).getCssValue("background-color");
				DeveloperMhex2 = Color.fromString(DeveloperMColor2).asHex();
				System.out.println("Image color: "+DeveloperMhex2);
				test.log(Status.INFO, MarkupHelper.createLabel("User changed item color to:  "+DeveloperMhex2, ExtentColor.BLUE));
				DeveloperMSecondcolor = getscreen.capture(driver, "DeveloperMSecondcolor");
				test.addScreenCaptureFromPath(DeveloperMSecondcolor);
			}
			catch(Exception e)
			{
				System.out.println("Color 2 is not available");
			}
			try
			{
				String col3 = driver.findElement(home2.color3).getCssValue("color");
				System.out.println("Color 4 exists:  "+col3);
				steps.clickButton(home2.color3, driver, name);
				Thread.sleep(1000);
				DeveloperMColor3 = driver.findElement(home2.color3).getCssValue("background-color");
				DeveloperMhex3 = Color.fromString(DeveloperMColor3).asHex();
				System.out.println("Image color: "+DeveloperMhex3);
				test.log(Status.INFO, MarkupHelper.createLabel("User changed item color to:  "+DeveloperMhex3, ExtentColor.BLUE));
				DeveloperMThirdcolor = getscreen.capture(driver, "DeveloperMThirdcolor");
				test.addScreenCaptureFromPath(DeveloperMThirdcolor);
			}
			catch(Exception e)
			{
				System.out.println("Color 3 is not available");
			}
			try
			{
				String col4 = driver.findElement(home2.color4).getCssValue("color");
				System.out.println("Color 1 exists:  "+col4);
				steps.clickButton(home2.color4, driver, name);
				Thread.sleep(1000);
				DeveloperMColor4 = driver.findElement(home2.color4).getCssValue("background-color");
				DeveloperMhex4 = Color.fromString(DeveloperMColor4).asHex();
				System.out.println("Image color: "+DeveloperMhex4);
				test.log(Status.INFO, MarkupHelper.createLabel("User changed item color to:  "+DeveloperMhex4, ExtentColor.BLUE));
				DeveloperMFourthcolor = getscreen.capture(driver, "DeveloperMFourthcolor");
				test.addScreenCaptureFromPath(DeveloperMFourthcolor);
			}
			catch(Exception e)
			{
				System.out.println("Color 4 is not available");
			}
			Thread.sleep(2000);
			Developercqty = driver.findElement(home.Qty).getAttribute("value");
			System.out.println("Current Quantity:  "+ Developercqty);
			test.log(Status.INFO, MarkupHelper.createLabel("Current Quantity of product:  "+Developercqty, ExtentColor.BLUE));
			Thread.sleep(1000);
			steps.clickButton(home.Trend_QtyIncrease, driver, name);
			Developeriqty = driver.findElement(home.Qty).getAttribute("value");
			System.out.println("Quantity after increase:  "+ Developeriqty);
			test.log(Status.INFO, MarkupHelper.createLabel("Current Quantity of product after Increase:  "+Developeriqty, ExtentColor.BLUE));
			steps.clickButton(home.Trend_QtyDecrease, driver, name);
			Developerdqty = driver.findElement(home.Qty).getAttribute("value");
			System.out.println("Quantity after decrease:  "+ Developerdqty);
			test.log(Status.INFO, MarkupHelper.createLabel("Current Quantity of product after increase:  "+Developerdqty, ExtentColor.BLUE));
			steps.clickButton(home.Trend_Addcart, driver, name);
			DeveloperSizemsg = driver.findElement(home.Trend_Sizeerrormsg).getText();
			System.out.println("User clicked on Addcart before Size selection:  "+DeveloperSizemsg);
			test.log(Status.INFO, MarkupHelper.createLabel("User clicked on Addcart before Size selection:  "+DeveloperSizemsg, ExtentColor.RED));
			//steps.clickJSButton(home2.Developer_sizeS, driver, name);
			steps.clickJSButton(home2.Size_S, driver, name);
			DevelopersizeS = driver.findElement(home2.Size_S).getText();
			System.out.println("Selected Size:  "+DevelopersizeS);
			test.log(Status.INFO, MarkupHelper.createLabel("User selected size:  "+DevelopersizeS, ExtentColor.BLUE));
			steps.clickButton(home2.Size_M, driver, name);
			DevelopersizeM = driver.findElement(home2.Size_M).getText();
			System.out.println("Selected Size:  "+DevelopersizeM);
			test.log(Status.INFO, MarkupHelper.createLabel("User selected size:  "+DevelopersizeM, ExtentColor.BLUE));
			steps.clickButton(home2.Size_L, driver, name);
			DevelopersizeL = driver.findElement(home2.Size_L).getText();
			System.out.println("Selected Size:  "+DevelopersizeL);
			test.log(Status.INFO, MarkupHelper.createLabel("User selected size:  "+DevelopersizeL, ExtentColor.BLUE));
			steps.clickButton(home2.Size_XL, driver, name);
			DevelopersizeXL = driver.findElement(home2.Size_XL).getText();
			System.out.println("Selected Size:  "+DevelopersizeXL);
			test.log(Status.INFO, MarkupHelper.createLabel("User selected size:  "+DevelopersizeXL, ExtentColor.BLUE));
			steps.clickButton(home2.Size_XXL, driver, name);
			DevelopersizeXXL = driver.findElement(home2.Size_XXL).getText();
			System.out.println("Selected Size:  "+DevelopersizeXXL);
			test.log(Status.INFO, MarkupHelper.createLabel("User selected size:  "+DevelopersizeXXL, ExtentColor.BLUE));
			steps.clickButton(home2.Size_XXXL, driver, name);
			DevelopersizeXXXL = driver.findElement(home2.Size_XXXL).getText();
			System.out.println("Selected Size:  "+DevelopersizeXXXL);
			test.log(Status.INFO, MarkupHelper.createLabel("User selected size:  "+DevelopersizeXXXL, ExtentColor.BLUE));
			steps.clickButton(home.Trend_FindUrSize, driver, name);
			test.log(Status.INFO, MarkupHelper.createLabel("User clicked on Find your size", ExtentColor.BLUE));
			Thread.sleep(1000);
			try
			{
				String D510 = driver.findElement(home.Trend_FindUrSize_510radio).getText();
				System.out.println(D510);
				steps.clickButton(home.Trend_FindUrSize_510radio, driver, name);
				Developerbelow510 = driver.findElement(home.Trend_FindUrSize_510radio).getText();
				System.out.println("User Selected:  "+Developerbelow510);
				test.log(Status.INFO, MarkupHelper.createLabel("User selected Height:  "+Developerbelow510, ExtentColor.BLUE));
				steps.clickButton(home.Trend_FindUrSize_above510radio, driver, name);
				Developerabove510 = driver.findElement(home.Trend_FindUrSize_above510radio).getText();
				System.out.println("User Selected:  "+Developerabove510);
				test.log(Status.INFO, MarkupHelper.createLabel("User selected Height:  "+Developerabove510, ExtentColor.BLUE));
				steps.clickButton(home.Trend_FindUrSize_Tightradio, driver, name);
				Developertightfit = driver.findElement(home.Trend_FindUrSize_Tightradio).getText();
				System.out.println("User Selected:  "+Developertightfit);
				test.log(Status.INFO, MarkupHelper.createLabel("User selected Fit:  "+Developertightfit, ExtentColor.BLUE));
				steps.clickButton(home.Trend_FindUrSize_Comfortradio, driver, name);
				Developercomfit = driver.findElement(home.Trend_FindUrSize_Comfortradio).getText();
				System.out.println("User Selected:  "+Developercomfit);
				test.log(Status.INFO, MarkupHelper.createLabel("User selected Fit:  "+Developercomfit, ExtentColor.BLUE));
				Thread.sleep(1000);
				steps.selectDropdown(home.Trend_FindUrSize_sizeDD, driver, strDataFileName, "size", name);
				test.log(Status.INFO, MarkupHelper.createLabel("User selected shirt size", ExtentColor.BLUE));
				steps.clickButton(home.Trend_FindUrSize_submit, driver, name);
				test.log(Status.INFO, MarkupHelper.createLabel("User clicked on Submit", ExtentColor.BLUE));
				Developerfindursize = getscreen.capture(driver, "Developerfindursize");
				test.addScreenCaptureFromPath(Developerfindursize);
				Thread.sleep(1000);
				steps.clickButton(home.Trend_FindUrSize_Okbutton, driver, name);
				test.log(Status.INFO, MarkupHelper.createLabel("system displayed size for given metrics", ExtentColor.BLUE));
				steps.clickButton(home.Trend_FindUrSize_closebutton, driver, name);
				test.log(Status.INFO, MarkupHelper.createLabel("User navigate to Product details page", ExtentColor.BLUE));
			}
			catch(Exception e)
			{
				steps.clickButton(home2.FindUrSize_closebutton, driver, name);
				test.log(Status.INFO, MarkupHelper.createLabel("User navigate to Product details page", ExtentColor.BLUE));
			}
			steps.clickButton(home2.Size_L, driver, name);
			test.log(Status.INFO, MarkupHelper.createLabel("User selected size", ExtentColor.BLUE));
			steps.clickButton(home.Trend_Addcart, driver, name);
			Thread.sleep(1000);
			test.log(Status.INFO, MarkupHelper.createLabel("User added product to Cart", ExtentColor.BLUE));
			Developeraddcart = getscreen.capture(driver, "Developeraddcart");
			test.addScreenCaptureFromPath(Developeraddcart);
			Thread.sleep(1000);
			try
			{			
				DevEdit = driver.findElement(home2.Addcart_edit).getText();
				System.out.println("User goint to edit:  "+DevEdit);
				test.log(Status.INFO, MarkupHelper.createLabel("User clicked on Edit", ExtentColor.BLUE));
				steps.clickButton(home2.Addcart_edit, driver, name);
				steps.selectDropdown(home2.Addcart_selectsize, driver, strDataFileName, "SelectSize", name);
				steps.selectDropdown(home2.Addcart_selectqty, driver, strDataFileName, "Quantity", name);
				test.log(Status.INFO, MarkupHelper.createLabel("User selected Size and Quantity", ExtentColor.BLUE));
				steps.clickButton(home2.Addcart_cancel, driver, name);
				steps.clickButton(home2.Addcart_edit, driver, name);
				steps.selectDropdown(home2.Addcart_selectsize, driver, strDataFileName, "SelectSize", name);
				steps.selectDropdown(home2.Addcart_selectqty, driver, strDataFileName, "Quantity", name);
				steps.clickButton(home2.Addcart_save, driver, name);
				Developerupdateproduct1 = getscreen.capture(driver, "Developerupdateproduct1");
				test.addScreenCaptureFromPath(Developerupdateproduct1);
				test.log(Status.INFO, MarkupHelper.createLabel("User updated product size and quantity", ExtentColor.BLUE));
				Thread.sleep(2000);
			}
			catch(Exception e)
			{
				System.out.println("Edit feature is not available for this product");
			}
			steps.clickButton(home2.Biker_remove, driver, name);
			Developerremovecart = getscreen.capture(driver, "Developerremovecart");
			test.addScreenCaptureFromPath(Developerremovecart);
			test.log(Status.INFO, MarkupHelper.createLabel("User removed product from Cart", ExtentColor.BLUE));
			steps.clickButton(home.Size_L, driver, name);
			test.log(Status.INFO, MarkupHelper.createLabel("User selected size", ExtentColor.BLUE));
			steps.clickButton(home.BuyNow, driver, name);
			test.log(Status.INFO, MarkupHelper.createLabel("User clicked on BuyNow, navigated to checkout page", ExtentColor.BLUE));
			String Devbuynow1 = getscreen.capture(driver, "Devbuynow1");
			test.addScreenCaptureFromPath(Devbuynow1);
			try
			{
				String cash = driver.findElement(home.CashonDeliveryText).getText();
				System.out.println("Payment method:  "+cash);
				steps.Verifytext(home.CashonDeliveryText, driver, cash, name);
				steps.clickButton(home.CashonDeliveryradio, driver, name);
				String Devbuynow2 = driver.findElement(home.PopUpSaveMsg).getText();
				System.out.println("Displayed message:   "+Devbuynow2);
				test.log(Status.INFO, MarkupHelper.createLabel("Displayed message:   "+Devbuynow2, ExtentColor.BLUE));
				String	 Devbuynow4 = getscreen.capture(driver, "Devbuynow4");
				test.addScreenCaptureFromPath(Devbuynow4);
				steps.clickButton(home.Cashondelivery, driver, name);
				test.log(Status.INFO, MarkupHelper.createLabel("User clicked on Cash on Delivery link", ExtentColor.BLUE));	
			}
			catch(Exception e)
			{
				System.out.println("Cash on Delivery is not available");
			}
			Thread.sleep(1000);
			steps.clickButton(home.CartIcon, driver, name);
			steps.clickButton(home.Trend_Addcart_remove, driver, name);
			test.log(Status.INFO, MarkupHelper.createLabel("User Completed transactions of 1st Image in Developer", ExtentColor.PURPLE));
			driver.close();
			driver.switchTo().window(Parent);
			Developerstatus = passstatus;
			Assert.assertTrue(true);
			System.out.println("Developer Successful");
			testresultdata.put(Integer.toString(cnt), new Object[] {name, "Developer", "Developer Successful",passstatus});	
		}
		catch(Exception e)
		{
			System.out.println("Developer Unsuccessful");
			testresultdata.put(Integer.toString(cnt), new Object[] {name, "Developer", "Developer Unsuccessful",failstatus});
			Developerstatus = failstatus;
			Developerstatusfail = getscreen.capture(driver, "Developerstatusfail");
			test.addScreenCaptureFromPath(Developerstatusfail);
			driver.close();
			Assert.assertTrue(false);
		}
	}
	@BeforeClass
	public void before()
	{
		workbook = new HSSFWorkbook();
		sheet = workbook.createSheet(strDataFileName);
		testresultdata = new LinkedHashMap<String, Object[]>();
		testresultdata.put("1", new Object[] { "Test Step Name", "Expected Result","Actual Result" ,"Status"});
	}
	@AfterClass
	public void after() throws Exception
	{
		Set<String> keyset = testresultdata.keySet();
		int rownum = 0;
		for (String key : keyset) {
			Row row = sheet.createRow(rownum++);
			Object [] objArr = testresultdata.get(key);
			int cellnum = 0;
			for (Object obj : objArr) {
				Cell cell = row.createCell(cellnum++);
				if(obj instanceof Date) 
					cell.setCellValue((Date)obj);
				else if(obj instanceof Boolean)
					cell.setCellValue((Boolean)obj);
				else if(obj instanceof String)
					cell.setCellValue((String)obj);
				else if(obj instanceof Double)
					cell.setCellValue((Double)obj);
			}
		}
		try {
			FileOutputStream out = new FileOutputStream(new File("TestResults/"+strDataFileName +".xls"));
			workbook.write(out);
			out.close();
			System.out.println("Excel written successfully..");
			TestResult();
			//driver.quit();
		}
		catch (FileNotFoundException e) 
		{
			e.printStackTrace();
		} 
		catch (IOException e) 
		{
			e.printStackTrace();
		}
	}
	public void TestResult() throws Exception
	{
		HSSFRow row;
		HSSFCell cell;
		WritableFont cellFont;
		WritableCellFormat cellFormat;
		Border border = null;
		int count;
		FileInputStream inp = new FileInputStream( "TestResults/"+strDataFileName +".xls");
		HSSFWorkbook wb = new HSSFWorkbook(inp);
		HSSFSheet sh = wb.getSheetAt(0);
		count=sh.getLastRowNum();
		for (int i = 1; i <= count; i++) 
		{
			row=sh.getRow(i);
			cell=row.getCell(3);
			String status=row.getCell(3).getStringCellValue();
			if(status.equalsIgnoreCase("Pass"))
			{
				HSSFCellStyle pass= wb.createCellStyle();
				pass.setFillForegroundColor(IndexedColors.GREEN.getIndex());
				pass.setFillPattern(CellStyle.SOLID_FOREGROUND);
				cell.setCellStyle(pass);
			}
			else
			{
				HSSFCellStyle fail= wb.createCellStyle();
				fail.setFillForegroundColor(IndexedColors.RED.getIndex());
				fail.setFillPattern(CellStyle.SOLID_FOREGROUND);
				cell.setCellStyle(fail);
			}
		}
		FileOutputStream out = new FileOutputStream("TestResults/"+strDataFileName+".xls");
		wb.write(out);
		wb.close();
	}
}
