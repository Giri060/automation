package Com.Homepage;
import java.io.File;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.IOException;
import java.util.Date;
import java.util.LinkedHashMap;
import java.util.Map;
import java.util.Set;
import org.apache.poi.hssf.usermodel.HSSFCell;
import org.apache.poi.hssf.usermodel.HSSFCellStyle;
import org.apache.poi.hssf.usermodel.HSSFRow;
import org.apache.poi.hssf.usermodel.HSSFSheet;
import org.apache.poi.hssf.usermodel.HSSFWorkbook;
import org.apache.poi.ss.usermodel.Cell;
import org.apache.poi.ss.usermodel.CellStyle;
import org.apache.poi.ss.usermodel.IndexedColors;
import org.apache.poi.ss.usermodel.Row;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.support.Color;
import org.testng.Assert;
import org.testng.annotations.AfterClass;
import org.testng.annotations.BeforeClass;
import org.testng.annotations.Test;
import com.aventstack.extentreports.Status;
import com.aventstack.extentreports.markuputils.ExtentColor;
import com.aventstack.extentreports.markuputils.MarkupHelper;
import Com.ObjectRepository.Homepage2_OR;
import Com.ObjectRepository.Homepage_OR;
import Com.ObjectRepository.Login_OR;
import Com.Utilities.Browsers;
import Com.Utilities.CommonMethods;
import Com.Utilities.GetScreenShot;
import Com.Utilities.Screenshot;
import Com.Utilities.Steps;
import Com.Utilities.TestBase;
import jxl.format.Border;
import jxl.write.WritableCellFormat;
import jxl.write.WritableFont;
public class BestSeller extends TestBase{
	WebDriver driver;
	Login_OR login = new Login_OR();
	Steps steps = new Steps();
	GetScreenShot getscreen = new GetScreenShot();
	Screenshot screen = new Screenshot();
	public String strAbsolutepath = new File("").getAbsolutePath();
	HSSFWorkbook workbook;
	HSSFSheet sheet;
	String strDataFileName = this.getClass().getSimpleName();
	Map<String, Object[]> testresultdata; 
	CommonMethods commonmethods = new CommonMethods();
	Homepage_OR home = new Homepage_OR();
	Homepage2_OR home2 = new Homepage2_OR();
	Screenshot screenshot = new Screenshot();
	String passstatus = screenshot.status("PASS");
	String failstatus = screenshot.status("FAIL");
	int cnt = 2;
	public static String BestSellerstatus = "";
	public static String Bestproductdetails = "";
	public static String BestMRP1 = "";
	public static String BestDescription1 = "";
	public static String Bestproductdetails1 = "";
	public static String Bestremovecart1 = "";
	public static String BestImage2 = "";
	public static String BestImage3 = "";
	public static String BestImage4 = "";
	public static String BestColor1 = "";
	public static String Besthex1 = "";
	public static String BestFirstcolor1 = "";
	public static String Bestcolor5 = "";
	public static String Besthex5 = "";
	public static String BestFifthcolor1 = "";
	public static String Bestcqty1 = "";
	public static String Bestiqty1 = "";
	public static String Bestdqty1 = "";
	public static String BestsizeS1 = "";
	public static String BestsizeM1 = "";
	public static String BestsizeL1 = "";
	public static String BestsizeXL1 = "";
	public static String BestsizeXXL1 = "";
	public static String BestsizeXXXL1 = "";
	public static String Bestbelow5101 = "";
	public static String Bestabove5101 = "";
	public static String Besttightfit1 = "";
	public static String Bestcomfit1 = "";
	public static String Bestfindursize1 = "";
	public static String Bestaddcart1 = "";
	public static String Bestupdateproduct1 = "";
	public static String BestSizemsg1 = "";
	public static String BestMRP2 = "";
	public static String BestDescription2 = "";
	public static String Bestproductdetails2 = "";
	public static String Bestremovecart2 = "";
	public static String BestFirstcolor2 = "";
	public static String Bestcqty2 = "";
	public static String Bestiqty2 = "";
	public static String Bestdqty2 = "";
	public static String BestsizeS2 = "";
	public static String BestsizeM2 = "";
	public static String BestsizeL2 = "";
	public static String BestsizeXL2 = "";
	public static String BestsizeXXL2 = "";
	public static String BestsizeXXXL2 = "";
	public static String Bestbelow5102 = "";
	public static String Bestabove5102 = "";
	public static String Besttightfit2 = "";
	public static String Bestcomfit2 = "";
	public static String Bestfindursize2 = "";
	public static String Bestaddcart2 = "";
	public static String Bestupdateproduct2 = "";
	public static String BestSizemsg2 = "";
	public static String buynow1 = "";
	public static String buynow2 = "";
	public static String buynow3 = "";
	public static String buynow4 = "";
	public static String RecentlyViewedstatus = "";
	
	public BestSeller()
	{
		Browsers b = new Browsers();
		this.driver = b.getBrowsers("chrome2");		 
	}
		//@Test(priority=0)
	public void RecentlyViewed() throws IOException, Exception
	{
		String name = new Object(){}.getClass().getEnclosingMethod().getName();
		cnt ++;
		try
		{
			test = extent.createTest("Recently Viewed Products");
			commonmethods.Login(driver);
			test.log(Status.INFO, MarkupHelper.createLabel("User able to Login", ExtentColor.BLUE));
			Thread.sleep(1000);
			steps.clickJSButton(home.Mydreamstore, driver, name);
			String Parent = driver.getWindowHandle();
			test.log(Status.INFO, MarkupHelper.createLabel("User naviagated to Mydreamstore Homepage Successfully", ExtentColor.BLUE));
			steps.ScrollDown(driver);
			steps.clickJSButton(home2.Recent_Image1, driver, name);
			test.log(Status.INFO, MarkupHelper.createLabel("User clicked on First Image in Recently Viewed", ExtentColor.BLUE));
			steps.ChildWindow(driver);
		//	driver.manage().window().maximize();
			test.log(Status.INFO, MarkupHelper.createLabel("User navigated to next window", ExtentColor.BLUE));
			Thread.sleep(500);
			String RecentlyFirstImage = getscreen.capture(driver, "RecentlyFirstImage");
			test.addScreenCaptureFromPath(RecentlyFirstImage);
			driver.close();
			driver.switchTo().window(Parent);
			Thread.sleep(1000);
			steps.clickJSButton(home2.Recent_Image2, driver, name);
			test.log(Status.INFO, MarkupHelper.createLabel("User clicked on Second Image in Recently Viewed", ExtentColor.BLUE));
			steps.ChildWindow(driver);
		//	driver.manage().window().maximize();
			test.log(Status.INFO, MarkupHelper.createLabel("User navigated to next window", ExtentColor.BLUE));
			Thread.sleep(500);
			String RecentlySecondImage = getscreen.capture(driver, "RecentlySecondImage");
			test.addScreenCaptureFromPath(RecentlySecondImage);
			driver.close();
			driver.switchTo().window(Parent);
			Thread.sleep(1000);
			steps.clickJSButton(home2.Recent_Image3, driver, name);
			test.log(Status.INFO, MarkupHelper.createLabel("User clicked on Third Image in Recently Viewed", ExtentColor.BLUE));
			steps.ChildWindow(driver);
	//		driver.manage().window().maximize();
			test.log(Status.INFO, MarkupHelper.createLabel("User navigated to next window", ExtentColor.BLUE));
			Thread.sleep(500);
			String RecentlyThirdImage = getscreen.capture(driver, "RecentlyThirdImage");
			test.addScreenCaptureFromPath(RecentlyThirdImage);
			driver.close();
			driver.switchTo().window(Parent);
			Thread.sleep(1000);
			RecentlyViewedstatus = passstatus;
			Assert.assertTrue(true);
			System.out.println("Recently Viewed Products Successful");
			test.log(Status.INFO, MarkupHelper.createLabel("Recently Viewed Products Successful", ExtentColor.BLUE));
		}
		catch(Exception e)
		{
			System.out.println("Recently Viewed Products Unsuccessful");
			testresultdata.put(Integer.toString(cnt), new Object[] {name, "Recently Viewed Products", "Recently Viewed Products Unsuccessful",failstatus});
			RecentlyViewedstatus = failstatus;
			String RecentlyViewedFail = getscreen.capture(driver, "RecentlyViewedFail");
			test.addScreenCaptureFromPath(RecentlyViewedFail);
			Assert.assertTrue(false);
		}
	}
	 	
	@Test(priority=0)
	public void BestSellerProducts() throws IOException, Exception
	{
		String name = new Object(){}.getClass().getEnclosingMethod().getName();
		cnt ++;
		try
		{
			test = extent.createTest("Best Seller");
			System.out.println("Best Seller Test Case Executing...");
			commonmethods.URLNaviagtion(driver);
			Thread.sleep(1000);
			commonmethods.Login(driver);
			test.log(Status.INFO, MarkupHelper.createLabel("User able to Login", ExtentColor.BLUE));
			Thread.sleep(1000);
			commonmethods.URLNaviagtion(driver);
			Thread.sleep(1000);
			steps.clickJSButton(home.Mydreamstore, driver, name);
			String Parent = driver.getWindowHandle();
			test.log(Status.INFO, MarkupHelper.createLabel("User naviagated to Mydreamstore Homepage Successfully", ExtentColor.BLUE));
			//steps.ScrollDown(driver);
			Thread.sleep(1000);
			steps.clickJSButton(home2.Best_Image1, driver, name);
			test.log(Status.INFO, MarkupHelper.createLabel("User clicked on First Image in Best Seller", ExtentColor.BLUE));
			steps.ChildWindow(driver);
		//	driver.manage().window().maximize();
			test.log(Status.INFO, MarkupHelper.createLabel("User navigated to next window", ExtentColor.BLUE));
			Thread.sleep(500);
		/*	try
			{
				String Filterpage1 = driver.findElement(home.Filterpagetext).getText();
				System.out.println("User navigated to :   "+Filterpage1);
			}
			catch(Exception e )
			{
				System.out.println("User navigated to different landing page");
				steps.clickJSButton(home2.Best_ImageUnderimage, driver, name);
				driver.close();
				steps.ChildWindow(driver);
				driver.manage().window().maximize();
			}
*/
			String BestFirstImage = getscreen.capture(driver, "BestFirstImage");
			test.addScreenCaptureFromPath(BestFirstImage);
			Thread.sleep(1000);
			try
			{
				steps.clickJSButton(home.Trend_Image_Readmore, driver, name);
				test.log(Status.INFO, MarkupHelper.createLabel("User clicked on Readmore", ExtentColor.BLUE));
				Bestproductdetails1 = getscreen.capture(driver, "Bestproductdetails1");
				test.addScreenCaptureFromPath(Bestproductdetails1);
				BestDescription1 = driver.findElement(home.Trend_Image_Description).getText();
				System.out.println("Item Description   "+BestDescription1);
				test.log(Status.INFO, MarkupHelper.createLabel("Description about Item:  "+BestDescription1, ExtentColor.BLUE));
				steps.clickJSButton(home.Trend_Image_Readless, driver, name);
			}
			catch(Exception e)
			{
				steps.clickJSButton(home.Trend_Image_Readmore, driver, name);
				test.log(Status.INFO, MarkupHelper.createLabel("User clicked on Readmore", ExtentColor.BLUE));
				Bestproductdetails1 = getscreen.capture(driver, "Bestproductdetails1");
				test.addScreenCaptureFromPath(Bestproductdetails1);
				BestDescription1 = driver.findElement(home.Trend_Image_Description1).getText();
				System.out.println("Item Description   "+BestDescription1);
				test.log(Status.INFO, MarkupHelper.createLabel("Description about Item:  "+BestDescription1, ExtentColor.BLUE));
				steps.clickJSButton(home.Trend_Image_Readless, driver, name);
			}
			Thread.sleep(1000);
			BestMRP1 = driver.findElement(home.Trend_Image_MRP).getText();
			System.out.println("Item Price   "+BestMRP1);
			test.log(Status.INFO, MarkupHelper.createLabel("MRP of an Item:  "+BestMRP1, ExtentColor.ORANGE));
			try
			{
				String col1 = driver.findElement(home.Trend_color1).getCssValue("color");
				System.out.println(col1);
				steps.clickJSButton(home.Trend_color1, driver, name);
				Thread.sleep(2000);
				BestColor1 = driver.findElement(home.Trend_color1).getCssValue("background-color");
				Besthex1 = Color.fromString(BestColor1).asHex();
				System.out.println("Image color: "+Besthex1);
				test.log(Status.INFO, MarkupHelper.createLabel("User changed item color to:  "+Besthex1, ExtentColor.BLUE));
				BestFirstcolor1 = getscreen.capture(driver, "BestFirstcolor1");
				test.addScreenCaptureFromPath(BestFirstcolor1);
			}
			catch(Exception e)
			{
				System.out.println("Color 1 is not exists");
			}
			try
			{
				String col2 = driver.findElement(home.Trend_color5).getCssValue("color");
				System.out.println(col2);
				steps.clickJSButton(home.Trend_color5, driver, name);
				Thread.sleep(2000);
				Bestcolor5 = driver.findElement(home.Trend_color5).getCssValue("background-color");
				Besthex5 = Color.fromString(Bestcolor5).asHex();
				System.out.println("Image color: "+Besthex5);
				test.log(Status.INFO, MarkupHelper.createLabel("User changed item color to:  "+Besthex5, ExtentColor.BLUE));
				BestFifthcolor1 = getscreen.capture(driver, "BestFifthcolor1");
				test.addScreenCaptureFromPath(BestFifthcolor1);
			}
			catch(Exception e)
			{
				System.out.println("Color 2 is not exists");
			}
			Bestcqty1 = driver.findElement(home.Qty).getAttribute("value");
			System.out.println("Current Quantity:  "+ Bestcqty1);
			test.log(Status.INFO, MarkupHelper.createLabel("Current Quantity of product:  "+Bestcqty1, ExtentColor.BLUE));
			Thread.sleep(1000);
			steps.clickJSButton(home.Trend_QtyIncrease, driver, name);
			Bestiqty1 = driver.findElement(home.Qty).getAttribute("value");
			System.out.println("Quantity after increase:  "+ Bestiqty1);
			test.log(Status.INFO, MarkupHelper.createLabel("Current Quantity of product after Increase:  "+Bestiqty1, ExtentColor.BLUE));
			steps.clickJSButton(home.Trend_QtyDecrease, driver, name);
			Bestdqty1 = driver.findElement(home.Qty).getAttribute("value");
			System.out.println("Quantity after decrease:  "+ Bestdqty1);
			test.log(Status.INFO, MarkupHelper.createLabel("Current Quantity of product after increase:  "+Bestdqty1, ExtentColor.BLUE));
			Thread.sleep(2000);
			steps.clickJSButton(home.Trend_Addcart, driver, name);
			BestSizemsg1 = driver.findElement(home.Trend_Sizeerrormsg).getText();
			System.out.println("User clicked on Addcart before Size selection:  "+BestSizemsg1);
			test.log(Status.INFO, MarkupHelper.createLabel("User clicked on Addcart before Size selection:  "+BestSizemsg1, ExtentColor.RED));
			steps.clickJSButton(home2.Size_S, driver, name);
			BestsizeS1 = driver.findElement(home2.Size_S).getText();
			System.out.println("Selected Size:  "+BestsizeS1);
			test.log(Status.INFO, MarkupHelper.createLabel("User selected size:  "+BestsizeS1, ExtentColor.BLUE));
			steps.clickJSButton(home2.Size_M, driver, name);
			BestsizeM1 = driver.findElement(home2.Size_M).getText();
			System.out.println("Selected Size:  "+BestsizeM1);
			test.log(Status.INFO, MarkupHelper.createLabel("User selected size:  "+BestsizeM1, ExtentColor.BLUE));
			steps.clickJSButton(home2.Size_L, driver, name);
			BestsizeL1 = driver.findElement(home2.Size_L).getText();
			System.out.println("Selected Size:  "+BestsizeL1);
			test.log(Status.INFO, MarkupHelper.createLabel("User selected size:  "+BestsizeL1, ExtentColor.BLUE));
			steps.clickJSButton(home2.Size_XL, driver, name);
			BestsizeXL1 = driver.findElement(home2.Size_XL).getText();
			System.out.println("Selected Size:  "+BestsizeXL1);
			test.log(Status.INFO, MarkupHelper.createLabel("User selected size:  "+BestsizeXL1, ExtentColor.BLUE));
			steps.clickJSButton(home2.Size_XXL, driver, name);
			BestsizeXXL1 = driver.findElement(home2.Size_XXL).getText();
			System.out.println("Selected Size:  "+BestsizeXXL1);
			test.log(Status.INFO, MarkupHelper.createLabel("User selected size:  "+BestsizeXXL1, ExtentColor.BLUE));
			steps.clickJSButton(home2.Size_XXXL, driver, name);
			BestsizeXXXL1 = driver.findElement(home2.Size_XXXL).getText();
			System.out.println("Selected Size:  "+BestsizeXXXL1);
			test.log(Status.INFO, MarkupHelper.createLabel("User selected size:  "+BestsizeXXXL1, ExtentColor.BLUE));
			steps.clickJSButton(home.Trend_FindUrSize, driver, name);
			test.log(Status.INFO, MarkupHelper.createLabel("User clicked on Find your size", ExtentColor.BLUE));
			Thread.sleep(1000);
			try
			{
				String radio1 = driver.findElement(home.Trend_FindUrSize_510radio).getText();
				System.out.println(radio1);
				steps.clickJSButton(home.Trend_FindUrSize_510radio, driver, name);
				Bestbelow5101 = driver.findElement(home.Trend_FindUrSize_510radio).getText();
				System.out.println("User Selected:  "+Bestbelow5101);
				test.log(Status.INFO, MarkupHelper.createLabel("User selected Height:  "+Bestbelow5101, ExtentColor.BLUE));
				steps.clickJSButton(home.Trend_FindUrSize_above510radio, driver, name);
				Bestabove5101 = driver.findElement(home.Trend_FindUrSize_above510radio).getText();
				System.out.println("User Selected:  "+Bestabove5101);
				test.log(Status.INFO, MarkupHelper.createLabel("User selected Height:  "+Bestabove5101, ExtentColor.BLUE));
				steps.clickJSButton(home.Trend_FindUrSize_Tightradio, driver, name);
				Besttightfit1 = driver.findElement(home.Trend_FindUrSize_Tightradio).getText();
				System.out.println("User Selected:  "+Besttightfit1);
				test.log(Status.INFO, MarkupHelper.createLabel("User selected Fit:  "+Besttightfit1, ExtentColor.BLUE));
				steps.clickJSButton(home.Trend_FindUrSize_Comfortradio, driver, name);
				Bestcomfit1 = driver.findElement(home.Trend_FindUrSize_Comfortradio).getText();
				System.out.println("User Selected:  "+Bestcomfit1);
				test.log(Status.INFO, MarkupHelper.createLabel("User selected Fit:  "+Bestcomfit1, ExtentColor.BLUE));
				steps.selectDropdown(home.Trend_FindUrSize_sizeDD, driver, strDataFileName, "size", name);
				test.log(Status.INFO, MarkupHelper.createLabel("User selected shirt size", ExtentColor.BLUE));
				steps.clickJSButton(home.Trend_FindUrSize_submit, driver, name);
				test.log(Status.INFO, MarkupHelper.createLabel("User clicked on Submit", ExtentColor.BLUE));
				Bestfindursize1 = getscreen.capture(driver, "Bestfindursize1");
				test.addScreenCaptureFromPath(Bestfindursize1);
				Thread.sleep(1000);
				steps.clickJSButton(home.Trend_FindUrSize_Okbutton, driver, name);
				test.log(Status.INFO, MarkupHelper.createLabel("system displayed size for given metrics", ExtentColor.BLUE));
				steps.clickJSButton(home.Trend_FindUrSize_closebutton, driver, name);
				test.log(Status.INFO, MarkupHelper.createLabel("User navigate to Product details page", ExtentColor.BLUE));
			}
			catch(Exception e)
			{
				steps.clickJSButton(home.Trend_FindUrSize_closebutton, driver, name);
				test.log(Status.INFO, MarkupHelper.createLabel("User navigate to Product details page", ExtentColor.BLUE));
			}
			steps.clickJSButton(home2.Size_L, driver, name);
			test.log(Status.INFO, MarkupHelper.createLabel("User selected size", ExtentColor.BLUE));
			steps.clickJSButton(home.Trend_Addcart, driver, name);
			test.log(Status.INFO, MarkupHelper.createLabel("User added product to Cart", ExtentColor.BLUE));
			Bestaddcart1 = getscreen.capture(driver, "Bestaddcart1");
			test.addScreenCaptureFromPath(Bestaddcart1);
			try
			{
				String edit1 = driver.findElement(home.Trend_Addcart_edit).getText();
				System.out.println(edit1);
				steps.clickJSButton(home.Trend_Addcart_edit, driver, name);
				test.log(Status.INFO, MarkupHelper.createLabel("User clicked on Edit", ExtentColor.BLUE));
				steps.selectDropdown(home.Trend_Addcart_selectsize, driver, strDataFileName, "SelectSize", name);
				steps.selectDropdown(home.Trend_Addcart_selectqty, driver, strDataFileName, "Quantity", name);
				test.log(Status.INFO, MarkupHelper.createLabel("User selected Size and Quantity", ExtentColor.BLUE));
				steps.clickJSButton(home.Trend_Addcart_cancel, driver, name);
				steps.clickJSButton(home.Trend_Addcart_edit, driver, name);
				steps.selectDropdown(home.Trend_Addcart_selectsize, driver, strDataFileName, "SelectSize", name);
				steps.selectDropdown(home.Trend_Addcart_selectqty, driver, strDataFileName, "Quantity", name);
				steps.clickJSButton(home.Trend_Addcart_save, driver, name);
				Bestupdateproduct1 = getscreen.capture(driver, "Bestupdateproduct1");
				test.addScreenCaptureFromPath(Bestupdateproduct1);
				test.log(Status.INFO, MarkupHelper.createLabel("User updated product size and quantity", ExtentColor.BLUE));
			}
			catch(Exception e)
			{
				System.out.println("Edit feature is not available for this product");
			}
			steps.clickJSButton(home.Trend_Addcart_remove, driver, name);
			Bestremovecart1 = getscreen.capture(driver, "Bestremovecart1");
			test.addScreenCaptureFromPath(Bestremovecart1);
			test.log(Status.INFO, MarkupHelper.createLabel("User removed product from Cart", ExtentColor.BLUE));
			//checkout process
			steps.clickJSButton(home.Size_L, driver, name);
			test.log(Status.INFO, MarkupHelper.createLabel("User selected size", ExtentColor.BLUE));
			steps.clickJSButton(home.BuyNow, driver, name);
			test.log(Status.INFO, MarkupHelper.createLabel("User clicked on BuyNow, navigated to checkout page", ExtentColor.BLUE));
			buynow1 = getscreen.capture(driver, "buynow1");
			test.addScreenCaptureFromPath(buynow1);
			try
			{
				String cash = driver.findElement(home.CashonDeliveryText).getText();
				System.out.println("Payment method:  "+cash);
				steps.Verifytext(home.CashonDeliveryText, driver, cash, name);
				steps.clickJSButton(home.CashonDeliveryradio, driver, name);
				buynow2 = driver.findElement(home.PopUpSaveMsg).getText();
				System.out.println("Displayed message:   "+buynow2);
				test.log(Status.INFO, MarkupHelper.createLabel("Displayed message:   "+buynow2, ExtentColor.BLUE));
				Thread.sleep(1000);
				/*	buynow3 = driver.findElement(home.PopUpMsg).getAttribute("value");
				System.out.println("Displayed message:   "+buynow3);
				test.log(Status.INFO, MarkupHelper.createLabel("Displayed message:   "+buynow3, ExtentColor.BLUE));
				 */
				buynow4 = getscreen.capture(driver, "buynow4");
				test.addScreenCaptureFromPath(buynow4);
				steps.clickJSButton(home.Cashondelivery, driver, name);
				test.log(Status.INFO, MarkupHelper.createLabel("User clicked on Cash on Delivery link", ExtentColor.BLUE));
			}
			catch(Exception e)
			{
				System.out.println("Cash on Delivery is not available");
			}
			Thread.sleep(1000);
			steps.clickJSButton(home.CartIcon, driver, name);
			steps.clickJSButton(home.Trend_Addcart_remove, driver, name);
			test.log(Status.INFO, MarkupHelper.createLabel("User Completed transactions of 1st Image in Best Seller", ExtentColor.PURPLE));
			driver.close();
			driver.switchTo().window(Parent);
			Thread.sleep(1000);
			steps.clickJSButton(home2.Best_Image2, driver, name);
			System.out.println("Clicked on 2nd Image");
			steps.ChildWindow(driver);
		//	driver.manage().window().maximize();
			steps.clickJSButton(home.Trend_Image_Readmore, driver, name);
			test.log(Status.INFO, MarkupHelper.createLabel("User clicked on Readmore", ExtentColor.BLUE));
			Bestproductdetails2 = getscreen.capture(driver, "Bestproductdetails2");
			test.addScreenCaptureFromPath(Bestproductdetails2);
			BestDescription2 = driver.findElement(home.Trend_Image_Description).getText();
			System.out.println("Item Description   "+BestDescription2);
			test.log(Status.INFO, MarkupHelper.createLabel("Description about Item:  "+BestDescription2, ExtentColor.BLUE));
			steps.clickJSButton(home.Trend_Image_Readless, driver, name);
			Thread.sleep(1000);
			BestMRP2 = driver.findElement(home.Trend_Image_MRP).getText();
			System.out.println("Item Price   "+BestMRP2);
			test.log(Status.INFO, MarkupHelper.createLabel("MRP of an Item:  "+BestMRP2, ExtentColor.ORANGE));
			Thread.sleep(2000);
			BestFirstcolor2 = getscreen.capture(driver, "BestFirstcolor2");
			test.addScreenCaptureFromPath(BestFirstcolor2);
			Bestcqty2 = driver.findElement(home.Qty).getAttribute("value");
			System.out.println("Current Quantity:  "+ Bestcqty2);
			test.log(Status.INFO, MarkupHelper.createLabel("Current Quantity of product:  "+Bestcqty2, ExtentColor.BLUE));
			Thread.sleep(1000);
			steps.clickJSButton(home.Trend_QtyIncrease, driver, name);
			Bestiqty2 = driver.findElement(home.Qty).getAttribute("value");
			System.out.println("Quantity after increase:  "+ Bestiqty2);
			test.log(Status.INFO, MarkupHelper.createLabel("Current Quantity of product after Increase:  "+Bestiqty2, ExtentColor.BLUE));
			steps.clickJSButton(home.Trend_QtyDecrease, driver, name);
			Bestdqty2 = driver.findElement(home.Qty).getAttribute("value");
			System.out.println("Quantity after decrease:  "+ Bestdqty2);
			test.log(Status.INFO, MarkupHelper.createLabel("Current Quantity of product after increase:  "+Bestdqty2, ExtentColor.BLUE));
			Thread.sleep(2000);
			steps.clickJSButton(home.Trend_Addcart, driver, name);
			BestSizemsg2 = driver.findElement(home.Trend_Sizeerrormsg).getText();
			System.out.println("User clicked on Addcart before Size selection:  "+BestSizemsg2);
			test.log(Status.INFO, MarkupHelper.createLabel("User clicked on Addcart before Size selection:  "+BestSizemsg2, ExtentColor.RED));
			steps.clickJSButton(home.Size_S, driver, name);
			BestsizeS2 = driver.findElement(home.Size_S).getText();
			System.out.println("Selected Size:  "+BestsizeS2);
			test.log(Status.INFO, MarkupHelper.createLabel("User selected size:  "+BestsizeS2, ExtentColor.BLUE));
			steps.clickJSButton(home.Size_M, driver, name);
			BestsizeM2 = driver.findElement(home.Size_M).getText();
			System.out.println("Selected Size:  "+BestsizeM2);
			test.log(Status.INFO, MarkupHelper.createLabel("User selected size:  "+BestsizeM2, ExtentColor.BLUE));
			steps.clickJSButton(home.Size_L, driver, name);
			BestsizeL2 = driver.findElement(home.Size_L).getText();
			System.out.println("Selected Size:  "+BestsizeL2);
			test.log(Status.INFO, MarkupHelper.createLabel("User selected size:  "+BestsizeL2, ExtentColor.BLUE));
			steps.clickJSButton(home.Size_XL, driver, name);
			BestsizeXL2 = driver.findElement(home.Size_XL).getText();
			System.out.println("Selected Size:  "+BestsizeXL2);
			test.log(Status.INFO, MarkupHelper.createLabel("User selected size:  "+BestsizeXL2, ExtentColor.BLUE));
			steps.clickJSButton(home.Size_XXL, driver, name);
			BestsizeXXL2 = driver.findElement(home.Size_XXL).getText();
			System.out.println("Selected Size:  "+BestsizeXXL2);
			test.log(Status.INFO, MarkupHelper.createLabel("User selected size:  "+BestsizeXXL2, ExtentColor.BLUE));
			steps.clickJSButton(home.Size_XXXL, driver, name);
			BestsizeXXXL2 = driver.findElement(home.Size_XXXL).getText();
			System.out.println("Selected Size:  "+BestsizeXXXL2);
			test.log(Status.INFO, MarkupHelper.createLabel("User selected size:  "+BestsizeXXXL2, ExtentColor.BLUE));
			steps.clickJSButton(home.Trend_FindUrSize, driver, name);
			test.log(Status.INFO, MarkupHelper.createLabel("User clicked on Find your size", ExtentColor.BLUE));
			Thread.sleep(1000);
			try
			{ 
				String radio2 = driver.findElement(home.Trend_FindUrSize_510radio).getText();
				System.out.println(radio2);
				steps.clickJSButton(home.Trend_FindUrSize_510radio, driver, name);
				Bestbelow5102 = driver.findElement(home.Trend_FindUrSize_510radio).getText();
				System.out.println("User Selected:  "+Bestbelow5102);
				test.log(Status.INFO, MarkupHelper.createLabel("User selected Height:  "+Bestbelow5102, ExtentColor.BLUE));
				steps.clickJSButton(home.Trend_FindUrSize_above510radio, driver, name);
				Bestabove5102 = driver.findElement(home.Trend_FindUrSize_above510radio).getText();
				System.out.println("User Selected:  "+Bestabove5102);
				test.log(Status.INFO, MarkupHelper.createLabel("User selected Height:  "+Bestabove5102, ExtentColor.BLUE));
				steps.clickJSButton(home.Trend_FindUrSize_Tightradio, driver, name);
				Besttightfit2 = driver.findElement(home.Trend_FindUrSize_Tightradio).getText();
				System.out.println("User Selected:  "+Besttightfit2);
				test.log(Status.INFO, MarkupHelper.createLabel("User selected Fit:  "+Besttightfit2, ExtentColor.BLUE));
				steps.clickJSButton(home.Trend_FindUrSize_Comfortradio, driver, name);
				Bestcomfit2 = driver.findElement(home.Trend_FindUrSize_Comfortradio).getText();
				System.out.println("User Selected:  "+Bestcomfit2);
				test.log(Status.INFO, MarkupHelper.createLabel("User selected Fit:  "+Bestcomfit2, ExtentColor.BLUE));
				steps.selectDropdown(home.Trend_FindUrSize_sizeDD, driver, strDataFileName, "size", name);
				test.log(Status.INFO, MarkupHelper.createLabel("User selected shirt size", ExtentColor.BLUE));
				steps.clickJSButton(home.Trend_FindUrSize_submit, driver, name);
				test.log(Status.INFO, MarkupHelper.createLabel("User clicked on Submit", ExtentColor.BLUE));
				Bestfindursize2 = getscreen.capture(driver, "Bestfindursize2");
				test.addScreenCaptureFromPath(Bestfindursize2);
				Thread.sleep(1000);
				steps.clickJSButton(home.Trend_FindUrSize_Okbutton, driver, name);
				test.log(Status.INFO, MarkupHelper.createLabel("system displayed size for given metrics", ExtentColor.BLUE));
				steps.clickJSButton(home.Trend_FindUrSize_closebutton, driver, name);
				test.log(Status.INFO, MarkupHelper.createLabel("User navigate to Product details page", ExtentColor.BLUE));
			}
			catch(Exception e )
			{
				steps.clickJSButton(home.Trend_FindUrSize_closebutton, driver, name);
				test.log(Status.INFO, MarkupHelper.createLabel("User navigate to Product details page", ExtentColor.BLUE));
			}
			steps.clickJSButton(home.Size_L, driver, name);
			test.log(Status.INFO, MarkupHelper.createLabel("User selected size", ExtentColor.BLUE));
			steps.clickJSButton(home.Trend_Addcart, driver, name);
			test.log(Status.INFO, MarkupHelper.createLabel("User added product to Cart", ExtentColor.BLUE));
			Bestaddcart2 = getscreen.capture(driver, "Bestaddcart1");
			test.addScreenCaptureFromPath(Bestaddcart2);
			try
			{
				String edit2 = driver.findElement(home.Trend_Addcart_edit).getText();
				System.out.println(edit2);
				steps.clickJSButton(home.Trend_Addcart_edit, driver, name);
				test.log(Status.INFO, MarkupHelper.createLabel("User clicked on Edit", ExtentColor.BLUE));
				steps.selectDropdown(home.Trend_Addcart_selectsize, driver, strDataFileName, "SelectSize", name);
				steps.selectDropdown(home.Trend_Addcart_selectqty, driver, strDataFileName, "Quantity", name);
				test.log(Status.INFO, MarkupHelper.createLabel("User selected Size and Quantity", ExtentColor.BLUE));
				steps.clickJSButton(home.Trend_Addcart_cancel, driver, name);
				steps.clickJSButton(home.Trend_Addcart_edit, driver, name);
				steps.selectDropdown(home.Trend_Addcart_selectsize, driver, strDataFileName, "SelectSize", name);
				steps.selectDropdown(home.Trend_Addcart_selectqty, driver, strDataFileName, "Quantity", name);
				steps.clickJSButton(home.Trend_Addcart_save, driver, name);
				Bestupdateproduct1 = getscreen.capture(driver, "Bestupdateproduct1");
				test.addScreenCaptureFromPath(Bestupdateproduct1);
				test.log(Status.INFO, MarkupHelper.createLabel("User updated product size and quantity", ExtentColor.BLUE));
			}
			catch(Exception f)
			{
				System.out.println("Edit feature is not available for this product");
			}
			steps.clickJSButton(home.Trend_Addcart_remove, driver, name);
			Bestremovecart2 = getscreen.capture(driver, "Bestremovecart2");
			test.addScreenCaptureFromPath(Bestremovecart2);
			test.log(Status.INFO, MarkupHelper.createLabel("User removed product from Cart", ExtentColor.BLUE));
			//checkout process
			steps.clickJSButton(home.Size_L, driver, name);
			test.log(Status.INFO, MarkupHelper.createLabel("User selected size", ExtentColor.BLUE));
			steps.clickJSButton(home.BuyNow, driver, name);
			test.log(Status.INFO, MarkupHelper.createLabel("User clicked on BuyNow, navigated to checkout page", ExtentColor.BLUE));
			buynow1 = getscreen.capture(driver, "buynow1");
			test.addScreenCaptureFromPath(buynow1);
			steps.clickJSButton(home.CashonDeliveryradio, driver, name);
			buynow2 = driver.findElement(home.PopUpSaveMsg).getText();
			System.out.println("Displayed message:   "+buynow2);
			test.log(Status.INFO, MarkupHelper.createLabel("Displayed message:   "+buynow2, ExtentColor.BLUE));
			Thread.sleep(1000);
			/*buynow3 = driver.findElement(home.PopUpMsg).getAttribute("value");
			System.out.println("Displayed message:   "+buynow3);
			test.log(Status.INFO, MarkupHelper.createLabel("Displayed message:   "+buynow3, ExtentColor.BLUE));
			 */
			buynow4 = getscreen.capture(driver, "buynow4");
			test.addScreenCaptureFromPath(buynow4);
			steps.clickJSButton(home.Cashondelivery, driver, name);
			test.log(Status.INFO, MarkupHelper.createLabel("User clicked on Cash on Delivery link", ExtentColor.BLUE));
			Thread.sleep(1000);
			steps.clickJSButton(home.CartIcon, driver, name);
			steps.clickJSButton(home.Trend_Addcart_remove, driver, name);
			test.log(Status.INFO, MarkupHelper.createLabel("User Completed transactions of 2nd Image in Best Seller", ExtentColor.PURPLE));
			driver.close();
			driver.switchTo().window(Parent);
			Thread.sleep(2000);
			steps.clickJSButton(home2.Best_Image3, driver, name);
			test.log(Status.INFO, MarkupHelper.createLabel("User clicked on Third Image in Best Seller", ExtentColor.BLUE));
			steps.ChildWindow(driver);
			//driver.manage().window().maximize();
			test.log(Status.INFO, MarkupHelper.createLabel("User navigated to next window", ExtentColor.BLUE));
			test.log(Status.INFO, MarkupHelper.createLabel("User opened 3rd Image in Best Seller", ExtentColor.PURPLE));
			Thread.sleep(500);
			String BestThirdImage = getscreen.capture(driver, "BestThirdImage");
			test.addScreenCaptureFromPath(BestThirdImage);
			driver.close();
			driver.switchTo().window(Parent);
			Thread.sleep(2000);
			steps.clickJSButton(home2.Best_Image4, driver, name);
			test.log(Status.INFO, MarkupHelper.createLabel("User clicked on Fourth Image in Best Seller", ExtentColor.BLUE));
			steps.ChildWindow(driver);
		//	driver.manage().window().maximize();
			test.log(Status.INFO, MarkupHelper.createLabel("User navigated to next window", ExtentColor.BLUE));
			test.log(Status.INFO, MarkupHelper.createLabel("User opened 4th Image in Best Seller", ExtentColor.PURPLE));
			Thread.sleep(500);
			String BestFourthImage = getscreen.capture(driver, "BestFourthImage");
			test.addScreenCaptureFromPath(BestFourthImage);
			driver.close();
			driver.switchTo().window(Parent);
			BestSellerstatus = passstatus;
			Assert.assertTrue(true);
			System.out.println("Best Seller Successful");
			test.log(Status.INFO, MarkupHelper.createLabel("Best Seller Successful", ExtentColor.BLUE));
			testresultdata.put(Integer.toString(cnt), new Object[] {name, "Best Seller", "Best Seller Successful",passstatus});	
		}
		catch(Exception e)
		{
			System.out.println("Best Seller Unsuccessful");
			testresultdata.put(Integer.toString(cnt), new Object[] {name, "Best Seller", "Best Seller Unsuccessful",failstatus});
			BestSellerstatus = failstatus;
			String BestSellerFail = getscreen.capture(driver, "BestSellerFail");
			test.addScreenCaptureFromPath(BestSellerFail);
			Assert.assertTrue(false);
		}
	}
	@BeforeClass
	public void before()
	{
		workbook = new HSSFWorkbook();
		sheet = workbook.createSheet(strDataFileName);
		testresultdata = new LinkedHashMap<String, Object[]>();
		testresultdata.put("1", new Object[] { "Test Step Name", "Expected Result","Actual Result" ,"Status"});
	}
	@AfterClass
	public void after() throws Exception
	{
		Set<String> keyset = testresultdata.keySet();
		int rownum = 0;
		for (String key : keyset) {
			Row row = sheet.createRow(rownum++);
			Object [] objArr = testresultdata.get(key);
			int cellnum = 0;
			for (Object obj : objArr) {
				Cell cell = row.createCell(cellnum++);
				if(obj instanceof Date) 
					cell.setCellValue((Date)obj);
				else if(obj instanceof Boolean)
					cell.setCellValue((Boolean)obj);
				else if(obj instanceof String)
					cell.setCellValue((String)obj);
				else if(obj instanceof Double)
					cell.setCellValue((Double)obj);
			}
		}
		try {
			FileOutputStream out = new FileOutputStream(new File("TestResults/"+strDataFileName +".xls"));
			workbook.write(out);
			out.close();
			System.out.println("Excel written successfully..");
			TestResult();
			driver.quit();
		}
		catch (FileNotFoundException e) 
		{
			e.printStackTrace();
		} 
		catch (IOException e) 
		{
			e.printStackTrace();
		}
	}
	public void TestResult() throws Exception
	{
		HSSFRow row;
		HSSFCell cell;
		WritableFont cellFont;
		WritableCellFormat cellFormat;
		Border border = null;
		int count;
		FileInputStream inp = new FileInputStream( "TestResults/"+strDataFileName +".xls");
		HSSFWorkbook wb = new HSSFWorkbook(inp);
		HSSFSheet sh = wb.getSheetAt(0);
		count=sh.getLastRowNum();
		for (int i = 1; i <= count; i++) 
		{
			row=sh.getRow(i);
			cell=row.getCell(3);
			String status=row.getCell(3).getStringCellValue();
			if(status.equalsIgnoreCase("Pass"))
			{
				HSSFCellStyle pass= wb.createCellStyle();
				pass.setFillForegroundColor(IndexedColors.GREEN.getIndex());
				pass.setFillPattern(CellStyle.SOLID_FOREGROUND);
				cell.setCellStyle(pass);
			}
			else
			{
				HSSFCellStyle fail= wb.createCellStyle();
				fail.setFillForegroundColor(IndexedColors.RED.getIndex());
				fail.setFillPattern(CellStyle.SOLID_FOREGROUND);
				cell.setCellStyle(fail);
			}
		}
		FileOutputStream out = new FileOutputStream("TestResults/"+strDataFileName+".xls");
		wb.write(out);
		wb.close();
	}
}
