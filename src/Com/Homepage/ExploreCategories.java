package Com.Homepage;
import java.io.File;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.IOException;
import java.util.Date;
import java.util.LinkedHashMap;
import java.util.Map;
import java.util.Set;
import org.apache.poi.hssf.usermodel.HSSFCell;
import org.apache.poi.hssf.usermodel.HSSFCellStyle;
import org.apache.poi.hssf.usermodel.HSSFRow;
import org.apache.poi.hssf.usermodel.HSSFSheet;
import org.apache.poi.hssf.usermodel.HSSFWorkbook;
import org.apache.poi.ss.usermodel.Cell;
import org.apache.poi.ss.usermodel.CellStyle;
import org.apache.poi.ss.usermodel.IndexedColors;
import org.apache.poi.ss.usermodel.Row;
import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.support.Color;
import org.openqa.selenium.support.ui.Select;
import org.testng.Assert;
import org.testng.annotations.AfterClass;
import org.testng.annotations.BeforeClass;
import org.testng.annotations.Test;
import com.aventstack.extentreports.Status;
import com.aventstack.extentreports.markuputils.ExtentColor;
import com.aventstack.extentreports.markuputils.MarkupHelper;
import Com.ObjectRepository.Homepage2_OR;
import Com.ObjectRepository.Homepage_OR;
import Com.ObjectRepository.Login_OR;
import Com.Utilities.Browsers;
import Com.Utilities.CommonMethods;
import Com.Utilities.GetScreenShot;
import Com.Utilities.Screenshot;
import Com.Utilities.Steps;
import Com.Utilities.TestBase;
import jxl.format.Border;
import jxl.write.WritableCellFormat;
import jxl.write.WritableFont;
public class ExploreCategories extends TestBase{
	WebDriver driver;
	Login_OR login = new Login_OR();
	Steps steps = new Steps();
	GetScreenShot getscreen = new GetScreenShot();
	Screenshot screen = new Screenshot();
	public String strAbsolutepath = new File("").getAbsolutePath();
	HSSFWorkbook workbook;
	HSSFSheet sheet;
	String strDataFileName = this.getClass().getSimpleName();
	Map<String, Object[]> testresultdata; 
	CommonMethods commonmethods = new CommonMethods();
	Homepage_OR home = new Homepage_OR();
	Homepage2_OR home2 = new Homepage2_OR();
	Screenshot screenshot = new Screenshot();
	String passstatus = screenshot.status("PASS");
	String failstatus = screenshot.status("FAIL");
	int cnt = 2;
	public static String ExpTshirtsPage = "";
	public static String ExpTshirtsPage2 = "";
	public static String ExpTshirtsDescription = "";
	public static String ExpTshirtsDescription1 = "";
	public static String ExpTshirtsColor1 = "";
	public static String ExpTshirtsMRP = "";
	public static String ExpTshirtshex1 = "";
	public static String ExpTshirtsFirstcolor = "";
	public static String ExpTshirtsColor2 = "";
	public static String ExpTshirtshex2 = "";
	public static String ExpTshirtsSecondcolor = "";
	public static String ExpTshirtsColor3 = "";
	public static String ExpTshirtshex3 = "";
	public static String ExpTshirtsThirdcolor = "";
	public static String ExpTshirtscqty = "";
	public static String ExpTshirtsiqty = "";
	public static String ExpTshirtsdqty = "";
	public static String ExpTshirtsSizemsg = "";
	public static String ExpTshirtssizeS = "";
	public static String ExpTshirtssizeM = "";
	public static String ExpTshirtssizeL = "";
	public static String ExpTshirtssizeXL = "";
	public static String ExpTshirtssizeXXL = "";
	public static String ExpTshirtssizeXXXL = "";
	public static String ExpTshirtsbelow510 = "";
	public static String ExpTshirtsabove510 = "";
	public static String ExpTshirtstightfit = "";
	public static String ExpTshirtscomfit = "";
	public static String ExpTshirtsfindursize = "";
	public static String ExpTshirtsaddcart = "";
	public static String ExpTshirtsupdateproduct1 = "";
	public static String ExpTshirtsremovecart = "";
	public static String ExpTshirtsstatus = "";
	public static String ExpTshirtsstatusfail = "";
	public static String ExpTshirtsColor11 = "";
	public static String ExpTshirtshex11 = "";
	public static String ExpTshirtsFirstcolor1 = "";
	public static String ExpTshirtsColor22 = "";
	public static String ExpTshirtshex22 = "";
	public static String ExpTshirtsSecondcolor1 = "";
	public static String ExpTshirtsColor33 = "";
	public static String ExpTshirtshex33 = "";
	public static String ExpTshirtsThirdcolor3 = "";
	public static String ExpPolosPage = "";
	public static String ExpPolosPage2 = "";
	public static String ExpPolosDescription = "";
	public static String ExpPolosDescription1 = "";
	public static String ExpPolosColor1 = "";
	public static String ExpPolosMRP = "";
	public static String ExpPoloshex1 = "";
	public static String ExpPolosFirstcolor = "";
	public static String ExpPolosColor2 = "";
	public static String ExpPoloshex2 = "";
	public static String ExpPolosSecondcolor = "";
	public static String ExpPolosColor3 = "";
	public static String ExpPoloshex3 = "";
	public static String ExpPolosThirdcolor = "";
	public static String ExpPoloscqty = "";
	public static String ExpPolosiqty = "";
	public static String ExpPolosdqty = "";
	public static String ExpPolosSizemsg = "";
	public static String ExpPolossizeS = "";
	public static String ExpPolossizeM = "";
	public static String ExpPolossizeL = "";
	public static String ExpPolossizeXL = "";
	public static String ExpPolossizeXXL = "";
	public static String ExpPolossizeXXXL = "";
	public static String ExpPolosbelow510 = "";
	public static String ExpPolosabove510 = "";
	public static String ExpPolostightfit = "";
	public static String ExpPoloscomfit = "";
	public static String ExpPolosfindursize = "";
	public static String ExpPolosaddcart = "";
	public static String ExpPolosupdateproduct1 = "";
	public static String ExpPolosremovecart = "";
	public static String ExpPolosstatus = "";
	public static String ExpPolosstatusfail = "";
	public static String ExpFullSleevePage = "";
	public static String ExpFullSleevePage2 = "";
	public static String ExpFullSleeveDescription = "";
	public static String ExpFullSleeveDescription1 = "";
	public static String ExpFullSleeveMRP = "";
	public static String Product1ExpFullSleeveColor1 = "";
	public static String Product1ExpFullSleevehex1 = "";
	public static String Product1ExpFullSleeveFirstcolor = "";
	public static String Product1ExpFullSleeveColor2 = "";
	public static String Product1ExpFullSleevehex2 = "";
	public static String Product1ExpFullSleeveSecondcolor = "";
	public static String Product1ExpFullSleeveColor3 = "";
	public static String Product1ExpFullSleevehex3 = "";
	public static String Product1ExpFullSleeveThirdcolor = "";
	public static String Product2ExpFullSleeveColor1 = "";
	public static String Product2ExpFullSleevehex1 = "";
	public static String Product2ExpFullSleeveFirstcolor = "";
	public static String Product2ExpFullSleeveColor2 = "";
	public static String Product2ExpFullSleevehex2 = "";
	public static String Product2ExpFullSleeveSecondcolor = "";
	public static String Product2ExpFullSleeveColor3 = "";
	public static String Product2ExpFullSleevehex3 = "";
	public static String Product2ExpFullSleeveThirdcolor = "";
	public static String Product3ExpFullSleeveColor1 = "";
	public static String Product3ExpFullSleevehex1 = "";
	public static String Product3ExpFullSleeveFirstcolor = "";
	public static String Product3ExpFullSleeveColor2 = "";
	public static String Product3ExpFullSleevehex2 = "";
	public static String Product3ExpFullSleeveSecondcolor = "";
	public static String Product3ExpFullSleeveColor3 = "";
	public static String Product3ExpFullSleevehex3 = "";
	public static String Product3ExpFullSleeveThirdcolor = "";
	public static String ExpFullSleevecqty = "";
	public static String ExpFullSleeveiqty = "";
	public static String ExpFullSleevedqty = "";
	public static String ExpFullSleeveSizemsg = "";
	public static String ExpFullSleevesizeS = "";
	public static String ExpFullSleevesizeM = "";
	public static String ExpFullSleevesizeL = "";
	public static String ExpFullSleevesizeXL = "";
	public static String ExpFullSleevesizeXXL = "";
	public static String ExpFullSleevesizeXXXL = "";
	public static String ExpFullSleevebelow510 = "";
	public static String ExpFullSleeveabove510 = "";
	public static String ExpFullSleevetightfit = "";
	public static String ExpFullSleevecomfit = "";
	public static String ExpFullSleevefindursize = "";
	public static String ExpFullSleeveaddcart = "";
	public static String ExpFullSleeveupdateproduct1 = "";
	public static String ExpFullSleeveremovecart = "";
	public static String ExpFullSleevestatus = "";
	public static String ExpFullSleevestatusfail = "";
	public static String ExpMobileCasesPage = "";
	public static String ExpMobileCasesPage2 = "";
	public static String ExpMobileCasesDescription = "";
	public static String ExpMobileCasesDescription1 = "";
	public static String ExpMobileCasesMRP = "";
	public static String Dropdown1 = "";
	public static String ExpMobileCasesDropdown1 = "";
	public static String Dropdown2 = "";
	public static String ExpMobileCasesDropdown2 = "";
	public static String Dropdown3 = "";
	public static String ExpMobileCasesDropdown3 = "";
	public static String Dropdown4 = "";
	public static String ExpMobileCasesDropdown4 = "";
	public static String ExpMobileCasescqty = "";
	public static String ExpMobileCasesiqty = "";
	public static String ExpMobileCasesdqty = "";
	public static String ExpMobileCasesaddcart = "";
	public static String ExpMobileCasesupdateproduct1 = "";
	public static String ExpMobileCasesremovecart = "";
	public static String ExpMobileCasesstatus = "";
	public static String ExpMobileCasesstatusfail = "";
	public static String ExpCoffeemugsPage = "";
	public static String ExpCoffeemugsPage1 = "";
	public static String ExpCoffeemugsPage2 = "";
	public static String ExpCoffeemugsstatus = "";
	public static String ExpCoffeemugsstatusfail = "";
	public static String Tshirtsbuynow1 = "";
	public static String Tshirtsbuynow2 = "";
	public static String Tshirtsbuynow4 = "";
	public static String Polosbuynow1 = "";
	public static String Polosbuynow2 = "";
	public static String Polosbuynow4 = "";
	public static String FSbuynow1 = "";
	public static String FSbuynow2 = "";
	public static String FSbuynow4 = "";
	public static String ExpCoffeeDescription = "";
	public static String ExpCoffeeDescription1 = "";
	public static String ExpCoffeecqty = "";
	public static String ExpCoffeeiqty = "";
	public static String ExpCoffeedqty = "";
	public static String ExpCoffeeMRP = "";
	public ExploreCategories()
	{
		Browsers b = new Browsers();
		this.driver = b.getBrowsers("chrome1");		 
	}
	@Test(priority = 0)
	public void ExploreTshirts() throws IOException, InterruptedException
	{
		String name = new Object(){}.getClass().getEnclosingMethod().getName();
		cnt ++;
		try 
		{
			test = extent.createTest("Tshirts under Explore Categories");
			System.out.println("Tshirts under Explore Categories Test Case Executing...");
			commonmethods.Login(driver);
			test.log(Status.INFO, MarkupHelper.createLabel("User able to Login", ExtentColor.BLUE));
			Thread.sleep(1000);
			commonmethods.URLNaviagtion(driver);
			Thread.sleep(3000);
			steps.clickJSButton(home.Mydreamstore, driver, name);
			String homepage = getscreen.capture(driver, name);
			test.addScreenCaptureFromPath(homepage);
			steps.MoveElement(home2.ExploreCat_TShirts, driver, name);
			steps.clickJSButton(home2.ExploreCat_TShirts, driver, name);
			test.log(Status.INFO, MarkupHelper.createLabel("User clicked on T Shirts Image under Explore Categories", ExtentColor.BLUE));
			ExpTshirtsPage = getscreen.capture(driver, "ExpTshirtsPage");
			test.addScreenCaptureFromPath(ExpTshirtsPage);
			//Thread.sleep(20000);
			try
			{
				steps.WaitUntilElementPresent(home2.ExploreCat_TShirts_Image1, driver);
				System.out.println("User navigated to Filter Page");
				//steps.clickJSButton(home2.ExploreCat_TShirts_Image1, driver, name);
				test.log(Status.INFO, MarkupHelper.createLabel("User clicked on 1st Image in T Shirts under Explore Categories", ExtentColor.BLUE));
				ExpTshirtsPage2 = getscreen.capture(driver, "ExpTshirtsPage2");
				test.addScreenCaptureFromPath(ExpTshirtsPage2);
			}
			catch(Exception e)
			{
				System.out.println("User navigated to wrong page");
			}
			Thread.sleep(1000);
			try
			{
				ExpTshirtsDescription = driver.findElement(home2.ExploreCat_TShirts_Description2).getText();
				System.out.println("Production Description   "+ExpTshirtsDescription);
				test.log(Status.INFO, MarkupHelper.createLabel("Description about Item:  "+ExpTshirtsDescription, ExtentColor.BLUE));
			}
			catch(Exception e)
			{
				steps.clickJSButton(home2.Photography_Image1_Readmore, driver, name);
				ExpTshirtsDescription1 = driver.findElement(home2.ExploreCat_TShirts_Description1).getText();
				System.out.println("Production Description   "+ExpTshirtsDescription1);
				test.log(Status.INFO, MarkupHelper.createLabel("Description about Item:  "+ExpTshirtsDescription1, ExtentColor.BLUE));
				steps.clickJSButton(home2.Profession_Readless, driver, name);
			}
			ExpTshirtsMRP = driver.findElement(home.Trend_Image_MRP).getText();
			System.out.println("Product Price   "+ExpTshirtsMRP);
			test.log(Status.INFO, MarkupHelper.createLabel("MRP of an Product:  "+ExpTshirtsMRP, ExtentColor.ORANGE));
			Thread.sleep(1000);
			try
			{
				steps.clickJSButton(home2.ExploreCat_TShirts_Product2, driver, name);
				try
				{
					String col1 = driver.findElement(home2.color1).getCssValue("color");
					System.out.println(col1);
					steps.clickJSButton(home2.color1, driver, name);
					Thread.sleep(2000);
					ExpTshirtsColor1 = driver.findElement(home2.color1).getCssValue("background-color");
					ExpTshirtshex1 = Color.fromString(ExpTshirtsColor1).asHex();
					System.out.println("Image color: "+ExpTshirtshex1);
					test.log(Status.INFO, MarkupHelper.createLabel("User changed item color to:  "+ExpTshirtshex1, ExtentColor.BLUE));
					ExpTshirtsFirstcolor = getscreen.capture(driver, "ExpTshirtsFirstcolor");
					test.addScreenCaptureFromPath(ExpTshirtsFirstcolor);
				}
				catch(Exception e)
				{
					System.out.println("Color 1 is not exists");
				}
				try 
				{
					String col2 = driver.findElement(home2.color2).getCssValue("color");
					System.out.println(col2);
					steps.clickJSButton(home2.color2, driver, name);
					Thread.sleep(1000);
					ExpTshirtsColor2 = driver.findElement(home2.color2).getCssValue("background-color");
					ExpTshirtshex2 = Color.fromString(ExpTshirtsColor2).asHex();
					System.out.println("Image color: "+ExpTshirtshex2);
					test.log(Status.INFO, MarkupHelper.createLabel("User changed item color to:  "+ExpTshirtshex2, ExtentColor.BLUE));
					ExpTshirtsSecondcolor = getscreen.capture(driver, "ExpTshirtsSecondcolor");
					test.addScreenCaptureFromPath(ExpTshirtsSecondcolor);
				}
				catch(Exception e)
				{
					System.out.println("Color 2 is not exists");
				}
				try 
				{
					String col3 = driver.findElement(home2.color4).getCssValue("color");
					System.out.println(col3);
					steps.clickJSButton(home2.color4, driver, name);
					Thread.sleep(1000);
					ExpTshirtsColor3 = driver.findElement(home2.color4).getCssValue("background-color");
					ExpTshirtshex3 = Color.fromString(ExpTshirtsColor3).asHex();
					System.out.println("Image color: "+ExpTshirtshex3);
					test.log(Status.INFO, MarkupHelper.createLabel("User changed item color to:  "+ExpTshirtshex3, ExtentColor.BLUE));
					ExpTshirtsThirdcolor = getscreen.capture(driver, "ExpTshirtsThirdcolor");
					test.addScreenCaptureFromPath(ExpTshirtsThirdcolor);
				}
				catch(Exception e)
				{
					System.out.println("Color 3 is not exists");
				}
			}
			catch(Exception e)
			{
				System.out.println("Second Product in not exists");
			}
			Thread.sleep(1000);
			try
			{
				steps.clickJSButton(home2.ExploreCat_TShirts_Product1, driver, name);
				try
				{
					String col4 = driver.findElement(home2.color1).getCssValue("color");
					System.out.println(col4);
					steps.clickJSButton(home2.color1, driver, name);
					Thread.sleep(2000);
					ExpTshirtsColor11 = driver.findElement(home2.color1).getCssValue("background-color");
					ExpTshirtshex11 = Color.fromString(ExpTshirtsColor11).asHex();
					System.out.println("Image color: "+ExpTshirtshex11);
					test.log(Status.INFO, MarkupHelper.createLabel("User changed item color to:  "+ExpTshirtshex11, ExtentColor.BLUE));
					ExpTshirtsFirstcolor1 = getscreen.capture(driver, "ExpTshirtsFirstcolor1");
					test.addScreenCaptureFromPath(ExpTshirtsFirstcolor1);
				}
				catch(Exception e)
				{
					System.out.println("Color 1 is not exists");
				}
				try 
				{
					String col5 = driver.findElement(home2.color2).getCssValue("color");
					System.out.println(col5);
					steps.clickJSButton(home2.color2, driver, name);
					Thread.sleep(1000);
					ExpTshirtsColor22 = driver.findElement(home2.color2).getCssValue("background-color");
					ExpTshirtshex22 = Color.fromString(ExpTshirtsColor22).asHex();
					System.out.println("Image color: "+ExpTshirtshex22);
					test.log(Status.INFO, MarkupHelper.createLabel("User changed item color to:  "+ExpTshirtshex22, ExtentColor.BLUE));
					ExpTshirtsSecondcolor1 = getscreen.capture(driver, "ExpTshirtsSecondcolor1");
					test.addScreenCaptureFromPath(ExpTshirtsSecondcolor1);
				}
				catch(Exception e)
				{
					System.out.println("Color 2 is not exists");
				}
				try 
				{
					String col6 = driver.findElement(home2.color4).getCssValue("color");
					System.out.println(col6);
					steps.clickJSButton(home2.color4, driver, name);
					Thread.sleep(1000);
					ExpTshirtsColor33 = driver.findElement(home2.color4).getCssValue("background-color");
					ExpTshirtshex33 = Color.fromString(ExpTshirtsColor33).asHex();
					System.out.println("Image color: "+ExpTshirtshex33);
					test.log(Status.INFO, MarkupHelper.createLabel("User changed item color to:  "+ExpTshirtshex33, ExtentColor.BLUE));
					ExpTshirtsThirdcolor3 = getscreen.capture(driver, "ExpTshirtsThirdcolor3");
					test.addScreenCaptureFromPath(ExpTshirtsThirdcolor3);
				}
				catch(Exception e)
				{
					System.out.println("Color 3 is not exists");
				}
			}
			catch(Exception e)
			{
				System.out.println("First Product in not exists");
			}
			Thread.sleep(2000);
			ExpTshirtscqty = driver.findElement(home.Qty).getAttribute("value");
			System.out.println("Current Quantity:  "+ ExpTshirtscqty);
			test.log(Status.INFO, MarkupHelper.createLabel("Current Quantity of product:  "+ExpTshirtscqty, ExtentColor.BLUE));
			Thread.sleep(1000);
			steps.clickJSButton(home.Trend_QtyIncrease, driver, name);
			ExpTshirtsiqty = driver.findElement(home.Qty).getAttribute("value");
			System.out.println("Quantity after increase:  "+ ExpTshirtsiqty);
			test.log(Status.INFO, MarkupHelper.createLabel("Current Quantity of product after Increase:  "+ExpTshirtsiqty, ExtentColor.BLUE));
			steps.clickJSButton(home.Trend_QtyDecrease, driver, name);
			ExpTshirtsdqty = driver.findElement(home.Qty).getAttribute("value");
			System.out.println("Quantity after decrease:  "+ ExpTshirtsdqty);
			test.log(Status.INFO, MarkupHelper.createLabel("Current Quantity of product after increase:  "+ExpTshirtsdqty, ExtentColor.BLUE));
			steps.clickJSButton(home.Trend_Addcart, driver, name);
			ExpTshirtsSizemsg = driver.findElement(home.Trend_Sizeerrormsg).getText();
			System.out.println("User clicked on Addcart before Size selection:  "+ExpTshirtsSizemsg);
			test.log(Status.INFO, MarkupHelper.createLabel("User clicked on Addcart before Size selection:  "+ExpTshirtsSizemsg, ExtentColor.RED));
			//steps.clickJSButton(home2.ExpTshirts_sizeS, driver, name);
			steps.clickJSButton(home2.Size_S, driver, name);
			ExpTshirtssizeS = driver.findElement(home2.Size_S).getText();
			System.out.println("Selected Size:  "+ExpTshirtssizeS);
			test.log(Status.INFO, MarkupHelper.createLabel("User selected size:  "+ExpTshirtssizeS, ExtentColor.BLUE));
			steps.clickJSButton(home2.Size_M, driver, name);
			ExpTshirtssizeM = driver.findElement(home2.Size_M).getText();
			System.out.println("Selected Size:  "+ExpTshirtssizeM);
			test.log(Status.INFO, MarkupHelper.createLabel("User selected size:  "+ExpTshirtssizeM, ExtentColor.BLUE));
			steps.clickJSButton(home2.Size_L, driver, name);
			ExpTshirtssizeL = driver.findElement(home2.Size_L).getText();
			System.out.println("Selected Size:  "+ExpTshirtssizeL);
			test.log(Status.INFO, MarkupHelper.createLabel("User selected size:  "+ExpTshirtssizeL, ExtentColor.BLUE));
			steps.clickJSButton(home2.Size_XL, driver, name);
			ExpTshirtssizeXL = driver.findElement(home2.Size_XL).getText();
			System.out.println("Selected Size:  "+ExpTshirtssizeXL);
			test.log(Status.INFO, MarkupHelper.createLabel("User selected size:  "+ExpTshirtssizeXL, ExtentColor.BLUE));
			steps.clickJSButton(home2.Size_XXL, driver, name);
			ExpTshirtssizeXXL = driver.findElement(home2.Size_XXL).getText();
			System.out.println("Selected Size:  "+ExpTshirtssizeXXL);
			test.log(Status.INFO, MarkupHelper.createLabel("User selected size:  "+ExpTshirtssizeXXL, ExtentColor.BLUE));
			try
			{
				ExpTshirtssizeXXXL = driver.findElement(home2.Size_XXXL).getText();
				System.out.println("Selected Size:  "+ExpTshirtssizeXXXL);
				steps.clickJSButton(home2.Size_XXXL, driver, name);
				test.log(Status.INFO, MarkupHelper.createLabel("User selected size:  "+ExpTshirtssizeXXXL, ExtentColor.BLUE));
			}
			catch(Exception e)
			{
				System.out.println("XXXL size is not available");
			}
			steps.clickJSButton(home.Trend_FindUrSize, driver, name);
			test.log(Status.INFO, MarkupHelper.createLabel("User clicked on Find your size", ExtentColor.BLUE));
			Thread.sleep(1000);
			try 
			{
				String Sizeradio = driver.findElement(home.Trend_FindUrSize_510radio).getText();
				System.out.println(Sizeradio);
				steps.clickJSButton(home.Trend_FindUrSize_510radio, driver, name);
				ExpTshirtsbelow510 = driver.findElement(home.Trend_FindUrSize_510radio).getText();
				System.out.println("User Selected:  "+ExpTshirtsbelow510);
				test.log(Status.INFO, MarkupHelper.createLabel("User selected Height:  "+ExpTshirtsbelow510, ExtentColor.BLUE));
				steps.clickJSButton(home.Trend_FindUrSize_above510radio, driver, name);
				ExpTshirtsabove510 = driver.findElement(home.Trend_FindUrSize_above510radio).getText();
				System.out.println("User Selected:  "+ExpTshirtsabove510);
				test.log(Status.INFO, MarkupHelper.createLabel("User selected Height:  "+ExpTshirtsabove510, ExtentColor.BLUE));
				steps.clickJSButton(home.Trend_FindUrSize_Tightradio, driver, name);
				ExpTshirtstightfit = driver.findElement(home.Trend_FindUrSize_Tightradio).getText();
				System.out.println("User Selected:  "+ExpTshirtstightfit);
				test.log(Status.INFO, MarkupHelper.createLabel("User selected Fit:  "+ExpTshirtstightfit, ExtentColor.BLUE));
				steps.clickJSButton(home.Trend_FindUrSize_Comfortradio, driver, name);
				ExpTshirtscomfit = driver.findElement(home.Trend_FindUrSize_Comfortradio).getText();
				System.out.println("User Selected:  "+ExpTshirtscomfit);
				test.log(Status.INFO, MarkupHelper.createLabel("User selected Fit:  "+ExpTshirtscomfit, ExtentColor.BLUE));
				steps.selectDropdown(home.Trend_FindUrSize_sizeDD, driver, strDataFileName, "size", name);
				test.log(Status.INFO, MarkupHelper.createLabel("User selected shirt size", ExtentColor.BLUE));
				steps.clickJSButton(home.Trend_FindUrSize_submit, driver, name);
				test.log(Status.INFO, MarkupHelper.createLabel("User clicked on Submit", ExtentColor.BLUE));
				ExpTshirtsfindursize = getscreen.capture(driver, "ExpTshirtsfindursize");
				test.addScreenCaptureFromPath(ExpTshirtsfindursize);
				Thread.sleep(1000);
				steps.clickJSButton(home.Trend_FindUrSize_Okbutton, driver, name);
				test.log(Status.INFO, MarkupHelper.createLabel("system displayed size for given metrics", ExtentColor.BLUE));
				steps.clickJSButton(home.Trend_FindUrSize_closebutton, driver, name);
				test.log(Status.INFO, MarkupHelper.createLabel("User navigate to Product details page", ExtentColor.BLUE));
			}
			catch(Exception f)
			{
				Thread.sleep(1000);
				steps.clickJSButton(home.Trend_FindUrSize_closebutton, driver, name);
				test.log(Status.INFO, MarkupHelper.createLabel("User navigate to Product details page", ExtentColor.BLUE));
			}
			steps.clickJSButton(home2.Size_L, driver, name);
			test.log(Status.INFO, MarkupHelper.createLabel("User selected size", ExtentColor.BLUE));
			steps.clickJSButton(home.Trend_Addcart, driver, name);
			Thread.sleep(1000);
			test.log(Status.INFO, MarkupHelper.createLabel("User added product to Cart", ExtentColor.BLUE));
			ExpTshirtsaddcart = getscreen.capture(driver, "ExpTshirtsaddcart");
			test.addScreenCaptureFromPath(ExpTshirtsaddcart);
			Thread.sleep(1000);
			try
			{
				String edit1 = driver.findElement(home2.Addcart_edit).getText();
				System.out.println(edit1);
				steps.clickJSButton(home2.Addcart_edit, driver, name);
				test.log(Status.INFO, MarkupHelper.createLabel("User clicked on Edit", ExtentColor.BLUE));
				steps.selectDropdown(home2.Addcart_selectsize, driver, strDataFileName, "SelectSize", name);
				steps.selectDropdown(home2.Addcart_selectqty, driver, strDataFileName, "Quantity", name);
				test.log(Status.INFO, MarkupHelper.createLabel("User selected Size and Quantity", ExtentColor.BLUE));
				steps.clickJSButton(home2.Addcart_cancel, driver, name);
				steps.clickJSButton(home2.Addcart_edit, driver, name);
				steps.selectDropdown(home2.Addcart_selectsize, driver, strDataFileName, "SelectSize", name);
				steps.selectDropdown(home2.Addcart_selectqty, driver, strDataFileName, "Quantity", name);
				steps.clickJSButton(home2.Addcart_save, driver, name);
				ExpTshirtsupdateproduct1 = getscreen.capture(driver, "ExpTshirtsupdateproduct1");
				test.addScreenCaptureFromPath(ExpTshirtsupdateproduct1);
				test.log(Status.INFO, MarkupHelper.createLabel("User updated product size and quantity", ExtentColor.BLUE));
				Thread.sleep(2000);
			}
			catch(Exception e)
			{
				System.out.println("Edit Feature is not available for this product");
			}
			steps.clickJSButton(home2.Biker_remove, driver, name);
			ExpTshirtsremovecart = getscreen.capture(driver, "ExpTshirtsremovecart");
			test.addScreenCaptureFromPath(ExpTshirtsremovecart);
			test.log(Status.INFO, MarkupHelper.createLabel("User removed product from Cart", ExtentColor.BLUE));
			Thread.sleep(1000);
			//checkout process
			steps.clickJSButton(home.Size_L, driver, name);
			test.log(Status.INFO, MarkupHelper.createLabel("User selected size", ExtentColor.BLUE));
			steps.clickJSButton(home.BuyNow, driver, name);
			test.log(Status.INFO, MarkupHelper.createLabel("User clicked on BuyNow, navigated to checkout page", ExtentColor.BLUE));
			Tshirtsbuynow1 = getscreen.capture(driver, "Tshirtsbuynow1");
			test.addScreenCaptureFromPath(Tshirtsbuynow1);
			steps.clickJSButton(home.CashonDeliveryradio, driver, name);
			Tshirtsbuynow2 = driver.findElement(home.PopUpSaveMsg).getText();
			System.out.println("Displayed message:   "+Tshirtsbuynow2);
			test.log(Status.INFO, MarkupHelper.createLabel("Displayed message:   "+Tshirtsbuynow2, ExtentColor.BLUE));
			Thread.sleep(1000);
			/*	buynow3 = driver.findElement(home.PopUpMsg).getAttribute("value");
			System.out.println("Displayed message:   "+buynow3);
			test.log(Status.INFO, MarkupHelper.createLabel("Displayed message:   "+buynow3, ExtentColor.BLUE));
			 */
			Tshirtsbuynow4 = getscreen.capture(driver, "Tshirtsbuynow4");
			test.addScreenCaptureFromPath(Tshirtsbuynow4);
			steps.clickJSButton(home.Cashondelivery, driver, name);
			test.log(Status.INFO, MarkupHelper.createLabel("User clicked on Cash on Delivery link", ExtentColor.BLUE));
			Thread.sleep(4000);
			steps.clickJSButton(home.CartIcon, driver, name);
			steps.clickJSButton(home.Trend_Addcart_remove, driver, name);
			steps.clickJSButton(home.Mydreamstore, driver, name);
			test.log(Status.INFO, MarkupHelper.createLabel("User Completed transactions of 1st Image in Tshirts under Explore Categories", ExtentColor.PURPLE));
			ExpTshirtsstatus = passstatus;
			Assert.assertTrue(true);
			System.out.println("Tshirts under Explore Categories Successful");
			testresultdata.put(Integer.toString(cnt), new Object[] {name, "Tshirts under Explore Categories", "Tshirts under Explore Categories Successful",passstatus});	
		}
		catch(Exception e)
		{
			System.out.println("Tshirts under Explore Categories Unsuccessful");
			testresultdata.put(Integer.toString(cnt), new Object[] {name, "Tshirts under Explore Categories", "Tshirts under Explore Categories Unsuccessful",failstatus});
			ExpTshirtsstatus = failstatus;
			ExpTshirtsstatusfail = getscreen.capture(driver, "ExpTshirtsstatusfail");
			test.addScreenCaptureFromPath(ExpTshirtsstatusfail);
			Assert.assertTrue(false);
		}
	}
	@Test(priority = 1)
	public void ExplorePolos() throws IOException, InterruptedException
	{
		String name = new Object(){}.getClass().getEnclosingMethod().getName();
		cnt ++;
		try 
		{
			test = extent.createTest("Polos under Explore Categories");
			Thread.sleep(2000);
			System.out.println("Polos under Explore Categories Test Case Executing...");
			commonmethods.URLNaviagtion(driver);
			Thread.sleep(1000);
			/*commonmethods.Login(driver);
			test.log(Status.INFO, MarkupHelper.createLabel("User able to Login", ExtentColor.BLUE));
			Thread.sleep(3000);
			steps.clickJSButton(home.Mydreamstore, driver, name);
			String homepage = getscreen.capture(driver, name);
			test.addScreenCaptureFromPath(homepage);*/
			steps.MoveElement(home2.ExploreCat_Polos, driver, name);
			steps.clickJSButton(home2.ExploreCat_Polos, driver, name);
			test.log(Status.INFO, MarkupHelper.createLabel("User clicked on Polos Image under Explore Categories", ExtentColor.BLUE));
			ExpPolosPage = getscreen.capture(driver, "ExpPolosPage");
			test.addScreenCaptureFromPath(ExpPolosPage);
			Thread.sleep(20000);
			try
			{
				steps.WaitUntilElementPresent(home2.ExploreCat_Polos_Image1, driver);
				//steps.clickJSButton(home2.ExploreCat_Polos_Image1, driver, name);
				System.out.println("User navigated to Filter page");
				test.log(Status.INFO, MarkupHelper.createLabel("User clicked on 1st Image in T Shirts under Explore Categories", ExtentColor.BLUE));
				ExpPolosPage2 = getscreen.capture(driver, "ExpPolosPage2");
				test.addScreenCaptureFromPath(ExpPolosPage2);
			}
			catch(Exception e)
			{
				System.out.println("User navigated to wrong page");
			}
			try
			{
				steps.clickJSButton(home2.Photography_Image1_Readmore, driver, name);
				ExpPolosDescription = driver.findElement(home2.ExploreCat_Polos_Description).getText();
				System.out.println("Production Description   "+ExpPolosDescription);
				test.log(Status.INFO, MarkupHelper.createLabel("Description about Item:  "+ExpPolosDescription, ExtentColor.BLUE));
				steps.clickJSButton(home2.Profession_Readless, driver, name);
			}
			catch(Exception e)
			{
				steps.clickJSButton(home2.Photography_Image1_Readmore, driver, name);
				ExpPolosDescription1 = driver.findElement(home2.ExploreCat_Polos_Description1).getText();
				System.out.println("Production Description   "+ExpPolosDescription1);
				test.log(Status.INFO, MarkupHelper.createLabel("Description about Item:  "+ExpPolosDescription1, ExtentColor.BLUE));
				steps.clickJSButton(home2.Profession_Readless, driver, name);
			}
			ExpPolosMRP = driver.findElement(home.Trend_Image_MRP).getText();
			System.out.println("Product Price   "+ExpPolosMRP);
			test.log(Status.INFO, MarkupHelper.createLabel("MRP of an Product:  "+ExpPolosMRP, ExtentColor.ORANGE));
			try
			{
				String polocol1 = driver.findElement(home2.color1).getCssValue("color");
				System.out.println(polocol1);
				steps.clickJSButton(home2.color1, driver, name);
				Thread.sleep(2000);
				ExpPolosColor1 = driver.findElement(home2.color1).getCssValue("background-color");
				ExpPoloshex1 = Color.fromString(ExpPolosColor1).asHex();
				System.out.println("Image color: "+ExpPoloshex1);
				test.log(Status.INFO, MarkupHelper.createLabel("User changed item color to:  "+ExpPoloshex1, ExtentColor.BLUE));
				ExpPolosFirstcolor = getscreen.capture(driver, "ExpPolosFirstcolor");
				test.addScreenCaptureFromPath(ExpPolosFirstcolor);
			}
			catch(Exception e)
			{
				System.out.println("Color 1 is not exists");
			}
			try 
			{
				String polocol2 = driver.findElement(home2.color2).getCssValue("color");
				System.out.println(polocol2);
				steps.clickJSButton(home2.color2, driver, name);
				Thread.sleep(1000);
				ExpPolosColor2 = driver.findElement(home2.color2).getCssValue("background-color");
				ExpPoloshex2 = Color.fromString(ExpPolosColor2).asHex();
				System.out.println("Image color: "+ExpPoloshex2);
				test.log(Status.INFO, MarkupHelper.createLabel("User changed item color to:  "+ExpPoloshex2, ExtentColor.BLUE));
				ExpPolosSecondcolor = getscreen.capture(driver, "ExpPolosSecondcolor");
				test.addScreenCaptureFromPath(ExpPolosSecondcolor);
			}
			catch(Exception e)
			{
				System.out.println("Color 2 is not exists");
			}
			try 
			{
				String polocol3 = driver.findElement(home2.color4).getCssValue("color");
				System.out.println(polocol3);
				steps.clickJSButton(home2.color4, driver, name);
				Thread.sleep(1000);
				ExpPolosColor3 = driver.findElement(home2.color4).getCssValue("background-color");
				ExpPoloshex3 = Color.fromString(ExpPolosColor3).asHex();
				System.out.println("Image color: "+ExpPoloshex3);
				test.log(Status.INFO, MarkupHelper.createLabel("User changed item color to:  "+ExpPoloshex3, ExtentColor.BLUE));
				ExpPolosThirdcolor = getscreen.capture(driver, "ExpPolosThirdcolor");
				test.addScreenCaptureFromPath(ExpPolosThirdcolor);
			}
			catch(Exception e)
			{
				System.out.println("Color 3 is not exists");
			}
			Thread.sleep(2000);
			ExpPoloscqty = driver.findElement(home.Qty).getAttribute("value");
			System.out.println("Current Quantity:  "+ ExpPoloscqty);
			test.log(Status.INFO, MarkupHelper.createLabel("Current Quantity of product:  "+ExpPoloscqty, ExtentColor.BLUE));
			Thread.sleep(1000);
			steps.clickJSButton(home.Trend_QtyIncrease, driver, name);
			ExpPolosiqty = driver.findElement(home.Qty).getAttribute("value");
			System.out.println("Quantity after increase:  "+ ExpPolosiqty);
			test.log(Status.INFO, MarkupHelper.createLabel("Current Quantity of product after Increase:  "+ExpPolosiqty, ExtentColor.BLUE));
			steps.clickJSButton(home.Trend_QtyDecrease, driver, name);
			ExpPolosdqty = driver.findElement(home.Qty).getAttribute("value");
			System.out.println("Quantity after decrease:  "+ ExpPolosdqty);
			test.log(Status.INFO, MarkupHelper.createLabel("Current Quantity of product after increase:  "+ExpPolosdqty, ExtentColor.BLUE));
			steps.clickJSButton(home.Trend_Addcart, driver, name);
			ExpPolosSizemsg = driver.findElement(home.Trend_Sizeerrormsg).getText();
			System.out.println("User clicked on Addcart before Size selection:  "+ExpPolosSizemsg);
			test.log(Status.INFO, MarkupHelper.createLabel("User clicked on Addcart before Size selection:  "+ExpPolosSizemsg, ExtentColor.RED));
			//steps.clickJSButton(home2.ExpPolos_sizeS, driver, name);
			steps.clickJSButton(home2.Size_S, driver, name);
			ExpPolossizeS = driver.findElement(home2.Size_S).getText();
			System.out.println("Selected Size:  "+ExpPolossizeS);
			test.log(Status.INFO, MarkupHelper.createLabel("User selected size:  "+ExpPolossizeS, ExtentColor.BLUE));
			steps.clickJSButton(home2.Size_M, driver, name);
			ExpPolossizeM = driver.findElement(home2.Size_M).getText();
			System.out.println("Selected Size:  "+ExpPolossizeM);
			test.log(Status.INFO, MarkupHelper.createLabel("User selected size:  "+ExpPolossizeM, ExtentColor.BLUE));
			steps.clickJSButton(home2.Size_L, driver, name);
			ExpPolossizeL = driver.findElement(home2.Size_L).getText();
			System.out.println("Selected Size:  "+ExpPolossizeL);
			test.log(Status.INFO, MarkupHelper.createLabel("User selected size:  "+ExpPolossizeL, ExtentColor.BLUE));
			steps.clickJSButton(home2.Size_XL, driver, name);
			ExpPolossizeXL = driver.findElement(home2.Size_XL).getText();
			System.out.println("Selected Size:  "+ExpPolossizeXL);
			test.log(Status.INFO, MarkupHelper.createLabel("User selected size:  "+ExpPolossizeXL, ExtentColor.BLUE));
			steps.clickJSButton(home2.Size_XXL, driver, name);
			ExpPolossizeXXL = driver.findElement(home2.Size_XXL).getText();
			System.out.println("Selected Size:  "+ExpPolossizeXXL);
			test.log(Status.INFO, MarkupHelper.createLabel("User selected size:  "+ExpPolossizeXXL, ExtentColor.BLUE));
			steps.clickJSButton(home2.Size_XXXL, driver, name);
			ExpPolossizeXXXL = driver.findElement(home2.Size_XXXL).getText();
			System.out.println("Selected Size:  "+ExpPolossizeXXXL);
			test.log(Status.INFO, MarkupHelper.createLabel("User selected size:  "+ExpPolossizeXXXL, ExtentColor.BLUE));
			steps.clickJSButton(home.Trend_FindUrSize, driver, name);
			test.log(Status.INFO, MarkupHelper.createLabel("User clicked on Find your size", ExtentColor.BLUE));
			Thread.sleep(1000);
			try 
			{
				String poloradio = driver.findElement(home.Trend_FindUrSize_510radio).getText();
				System.out.println(poloradio);
				steps.clickJSButton(home.Trend_FindUrSize_510radio, driver, name);
				ExpPolosbelow510 = driver.findElement(home.Trend_FindUrSize_510radio).getText();
				System.out.println("User Selected:  "+ExpPolosbelow510);
				test.log(Status.INFO, MarkupHelper.createLabel("User selected Height:  "+ExpPolosbelow510, ExtentColor.BLUE));
				steps.clickJSButton(home.Trend_FindUrSize_above510radio, driver, name);
				ExpPolosabove510 = driver.findElement(home.Trend_FindUrSize_above510radio).getText();
				System.out.println("User Selected:  "+ExpPolosabove510);
				test.log(Status.INFO, MarkupHelper.createLabel("User selected Height:  "+ExpPolosabove510, ExtentColor.BLUE));
				steps.clickJSButton(home.Trend_FindUrSize_Tightradio, driver, name);
				ExpPolostightfit = driver.findElement(home.Trend_FindUrSize_Tightradio).getText();
				System.out.println("User Selected:  "+ExpPolostightfit);
				test.log(Status.INFO, MarkupHelper.createLabel("User selected Fit:  "+ExpPolostightfit, ExtentColor.BLUE));
				steps.clickJSButton(home.Trend_FindUrSize_Comfortradio, driver, name);
				ExpPoloscomfit = driver.findElement(home.Trend_FindUrSize_Comfortradio).getText();
				System.out.println("User Selected:  "+ExpPoloscomfit);
				test.log(Status.INFO, MarkupHelper.createLabel("User selected Fit:  "+ExpPoloscomfit, ExtentColor.BLUE));
				steps.selectDropdown(home.Trend_FindUrSize_sizeDD, driver, strDataFileName, "size", name);
				test.log(Status.INFO, MarkupHelper.createLabel("User selected shirt size", ExtentColor.BLUE));
				steps.clickJSButton(home.Trend_FindUrSize_submit, driver, name);
				test.log(Status.INFO, MarkupHelper.createLabel("User clicked on Submit", ExtentColor.BLUE));
				ExpPolosfindursize = getscreen.capture(driver, "ExpPolosfindursize");
				test.addScreenCaptureFromPath(ExpPolosfindursize);
				Thread.sleep(1000);
				steps.clickJSButton(home.Trend_FindUrSize_Okbutton, driver, name);
				test.log(Status.INFO, MarkupHelper.createLabel("system displayed size for given metrics", ExtentColor.BLUE));
				steps.clickJSButton(home.Trend_FindUrSize_closebutton, driver, name);
				test.log(Status.INFO, MarkupHelper.createLabel("User navigate to Product details page", ExtentColor.BLUE));
			}
			catch(Exception e)
			{
				steps.clickJSButton(home.Trend_FindUrSize_closebutton, driver, name);
				test.log(Status.INFO, MarkupHelper.createLabel("User navigate to Product details page", ExtentColor.BLUE));
			}
			steps.clickJSButton(home2.Size_L, driver, name);
			test.log(Status.INFO, MarkupHelper.createLabel("User selected size", ExtentColor.BLUE));
			steps.clickJSButton(home.Trend_Addcart, driver, name);
			Thread.sleep(1000);
			test.log(Status.INFO, MarkupHelper.createLabel("User added product to Cart", ExtentColor.BLUE));
			ExpPolosaddcart = getscreen.capture(driver, "ExpPolosaddcart");
			test.addScreenCaptureFromPath(ExpPolosaddcart);
			Thread.sleep(1000);
			try
			{
				String poloedit = driver.findElement(home2.Addcart_edit).getText();
				System.out.println(poloedit);
				steps.clickJSButton(home2.Addcart_edit, driver, name);
				test.log(Status.INFO, MarkupHelper.createLabel("User clicked on Edit", ExtentColor.BLUE));
				steps.selectDropdown(home2.Addcart_selectsize, driver, strDataFileName, "SelectSize", name);
				steps.selectDropdown(home2.Addcart_selectqty, driver, strDataFileName, "Quantity", name);
				test.log(Status.INFO, MarkupHelper.createLabel("User selected Size and Quantity", ExtentColor.BLUE));
				steps.clickJSButton(home2.Addcart_cancel, driver, name);
				steps.clickJSButton(home2.Addcart_edit, driver, name);
				steps.selectDropdown(home2.Addcart_selectsize, driver, strDataFileName, "SelectSize", name);
				steps.selectDropdown(home2.Addcart_selectqty, driver, strDataFileName, "Quantity", name);
				steps.clickJSButton(home2.Addcart_save, driver, name);
				ExpPolosupdateproduct1 = getscreen.capture(driver, "ExpPolosupdateproduct1");
				test.addScreenCaptureFromPath(ExpPolosupdateproduct1);
				test.log(Status.INFO, MarkupHelper.createLabel("User updated product size and quantity", ExtentColor.BLUE));
				Thread.sleep(2000);
			}
			catch(Exception e)
			{
				System.out.println("Edit feature is not available for this product");
			}
			steps.clickJSButton(home2.Biker_remove, driver, name);
			ExpPolosremovecart = getscreen.capture(driver, "ExpPolosremovecart");
			test.addScreenCaptureFromPath(ExpPolosremovecart);
			test.log(Status.INFO, MarkupHelper.createLabel("User removed product from Cart", ExtentColor.BLUE));
			Thread.sleep(1000);
			//checkout process
			steps.clickJSButton(home.Size_L, driver, name);
			test.log(Status.INFO, MarkupHelper.createLabel("User selected size", ExtentColor.BLUE));
			steps.clickJSButton(home.BuyNow, driver, name);
			test.log(Status.INFO, MarkupHelper.createLabel("User clicked on BuyNow, navigated to checkout page", ExtentColor.BLUE));
			Polosbuynow1 = getscreen.capture(driver, "Polosbuynow1");
			test.addScreenCaptureFromPath(Tshirtsbuynow1);
			steps.clickJSButton(home.CashonDeliveryradio, driver, name);
			Polosbuynow2 = driver.findElement(home.PopUpSaveMsg).getText();
			System.out.println("Displayed message:   "+Polosbuynow2);
			test.log(Status.INFO, MarkupHelper.createLabel("Displayed message:   "+Polosbuynow2, ExtentColor.BLUE));
			Thread.sleep(1000);
			/*	buynow3 = driver.findElement(home.PopUpMsg).getAttribute("value");
			System.out.println("Displayed message:   "+buynow3);
			test.log(Status.INFO, MarkupHelper.createLabel("Displayed message:   "+buynow3, ExtentColor.BLUE));
			 */
			Polosbuynow4 = getscreen.capture(driver, "Polosbuynow4");
			test.addScreenCaptureFromPath(Polosbuynow4);
			steps.clickJSButton(home.Cashondelivery, driver, name);
			test.log(Status.INFO, MarkupHelper.createLabel("User clicked on Cash on Delivery link", ExtentColor.BLUE));
			Thread.sleep(4000);
			steps.clickJSButton(home.CartIcon, driver, name);
			steps.clickJSButton(home.Trend_Addcart_remove, driver, name);
			steps.clickJSButton(home.Mydreamstore, driver, name);
			test.log(Status.INFO, MarkupHelper.createLabel("User Completed transactions of 1st Image in Polos under Explore Categories", ExtentColor.PURPLE));
			ExpPolosstatus = passstatus;
			Assert.assertTrue(true);
			System.out.println("Polos under Explore Categories Successful");
			testresultdata.put(Integer.toString(cnt), new Object[] {name, "Polos under Explore Categories", "Polos under Explore Categories Successful",passstatus});	
		}
		catch(Exception e)
		{
			System.out.println("Polos under Explore Categories Unsuccessful");
			testresultdata.put(Integer.toString(cnt), new Object[] {name, "Polos under Explore Categories", "Polos under Explore Categories Unsuccessful",failstatus});
			ExpPolosstatus = failstatus;
			ExpPolosstatusfail = getscreen.capture(driver, "ExpPolosstatusfail");
			test.addScreenCaptureFromPath(ExpPolosstatusfail);
			Assert.assertTrue(false);
		}
	}
	@Test(priority=2)
	public void ExploreFullSleeve() throws IOException, InterruptedException
	{
		String name = new Object(){}.getClass().getEnclosingMethod().getName();
		cnt ++;
		try 
		{
			test = extent.createTest("FullSleeve under Explore Categories");
			System.out.println("FullSleeve under Explore Categories Test Case Executing...");
			Thread.sleep(2000);
			commonmethods.URLNaviagtion(driver);
			Thread.sleep(1000);
			/*commonmethods.Login(driver);
			test.log(Status.INFO, MarkupHelper.createLabel("User able to Login", ExtentColor.BLUE));
			Thread.sleep(3000);
			steps.clickJSButton(home.Mydreamstore, driver, name);
			String homepage = getscreen.capture(driver, name);
			test.addScreenCaptureFromPath(homepage);*/
			steps.MoveElement(home2.ExploreCat_FullSleeve, driver, name);
			steps.clickJSButton(home2.ExploreCat_FullSleeve, driver, name);
			test.log(Status.INFO, MarkupHelper.createLabel("User clicked on FullSleeve Image under Explore Categories", ExtentColor.BLUE));
			ExpFullSleevePage = getscreen.capture(driver, "ExpFullSleevePage");
			test.addScreenCaptureFromPath(ExpFullSleevePage);
			//Thread.sleep(20000);
			try
			{
				steps.WaitUntilElementPresent(home2.ExploreCat_FullSleeve_Image1, driver);
				//steps.clickJSButton(home2.ExploreCat_FullSleeve_Image1, driver, name);
				System.out.println("User navigated to Filter page");
				test.log(Status.INFO, MarkupHelper.createLabel("User clicked on 1st Image in T Shirts under Explore Categories", ExtentColor.BLUE));
				ExpFullSleevePage2 = getscreen.capture(driver, "ExpFullSleevePage2");
				test.addScreenCaptureFromPath(ExpFullSleevePage2);
			}
			catch(Exception e )
			{
				System.out.println("User navigated to wrong page");
			}
			try
			{
				steps.clickJSButton(home2.Photography_Image1_Readmore, driver, name);
				ExpFullSleeveDescription = driver.findElement(home2.ExploreCat_FullSleeve_Description).getText();
				System.out.println("Production Description   "+ExpFullSleeveDescription);
				test.log(Status.INFO, MarkupHelper.createLabel("Description about Item:  "+ExpFullSleeveDescription, ExtentColor.BLUE));
				steps.clickJSButton(home2.Profession_Readless, driver, name);
			}
			catch(Exception e)
			{
				steps.clickJSButton(home2.Photography_Image1_Readmore, driver, name);
				ExpFullSleeveDescription1 = driver.findElement(home2.ExploreCat_FullSleeve_Description2).getText();
				System.out.println("Production Description   "+ExpFullSleeveDescription1);
				test.log(Status.INFO, MarkupHelper.createLabel("Description about Item:  "+ExpFullSleeveDescription1, ExtentColor.BLUE));
				steps.clickJSButton(home2.Profession_Readless, driver, name);
			}
			ExpFullSleeveMRP = driver.findElement(home.Trend_Image_MRP).getText();
			System.out.println("Product Price   "+ExpFullSleeveMRP);
			test.log(Status.INFO, MarkupHelper.createLabel("MRP of an Product:  "+ExpFullSleeveMRP, ExtentColor.ORANGE));
			Thread.sleep(1000);
			try
			{
				steps.clickJSButton(home2.ExploreCat_FullSleeve_Product2, driver, name);
				String prod2 = driver.findElement(home2.ExploreCat_FullSleeve_Product2).getCssValue("color");
				System.out.println(prod2);
				try
				{
					String fullcol1 = driver.findElement(home2.color1).getCssValue("color");
					System.out.println(fullcol1);
					steps.clickJSButton(home2.color1, driver, name);
					Thread.sleep(2000);
					Product2ExpFullSleeveColor1 = driver.findElement(home2.color1).getCssValue("background-color");
					Product2ExpFullSleevehex1 = Color.fromString(Product2ExpFullSleeveColor1).asHex();
					System.out.println("Image color: "+Product2ExpFullSleevehex1);
					test.log(Status.INFO, MarkupHelper.createLabel("User changed item color to:  "+Product2ExpFullSleevehex1, ExtentColor.BLUE));
					Product2ExpFullSleeveFirstcolor = getscreen.capture(driver, "Product2ExpFullSleeveFirstcolor");
					test.addScreenCaptureFromPath(Product2ExpFullSleeveFirstcolor);
				}
				catch(Exception e)
				{
					System.out.println("Color 1 is not exists");
				}
				try 
				{
					String fullcol2 = driver.findElement(home2.color2).getCssValue("color");
					System.out.println(fullcol2);
					steps.clickJSButton(home2.color2, driver, name);
					Thread.sleep(1000);
					Product2ExpFullSleeveColor2 = driver.findElement(home2.color2).getCssValue("background-color");
					Product2ExpFullSleevehex2 = Color.fromString(Product2ExpFullSleeveColor2).asHex();
					System.out.println("Image color: "+Product2ExpFullSleevehex2);
					test.log(Status.INFO, MarkupHelper.createLabel("User changed item color to:  "+Product2ExpFullSleevehex2, ExtentColor.BLUE));
					Product2ExpFullSleeveSecondcolor = getscreen.capture(driver, "Product2ExpFullSleeveSecondcolor");
					test.addScreenCaptureFromPath(Product2ExpFullSleeveSecondcolor);
				}
				catch(Exception e)
				{
					System.out.println("Color 2 is not exists");
				}
				try 
				{
					String fullcol3 = driver.findElement(home2.color4).getCssValue("color");
					System.out.println(fullcol3);
					steps.clickJSButton(home2.color4, driver, name);
					Thread.sleep(1000);
					Product2ExpFullSleeveColor3 = driver.findElement(home2.color4).getCssValue("background-color");
					Product2ExpFullSleevehex3 = Color.fromString(Product2ExpFullSleeveColor3).asHex();
					System.out.println("Image color: "+Product2ExpFullSleevehex3);
					test.log(Status.INFO, MarkupHelper.createLabel("User changed item color to:  "+Product2ExpFullSleevehex3, ExtentColor.BLUE));
					Product2ExpFullSleeveThirdcolor = getscreen.capture(driver, "Product2ExpFullSleeveThirdcolor");
					test.addScreenCaptureFromPath(Product2ExpFullSleeveThirdcolor);
				}
				catch(Exception e)
				{
					System.out.println("Color 3 is not exists");
				}
				System.out.println("Product 2 Exists");
			}
			catch(Exception e)
			{
				System.out.println("Product 2 doesn't exists");
			}
			Thread.sleep(2000);
			try
			{
				steps.clickJSButton(home2.ExploreCat_FullSleeve_Product3, driver, name);
				String prod3 = driver.findElement(home2.ExploreCat_FullSleeve_Product3).getCssValue("color");
				System.out.println(prod3);
				try
				{
					String fullcol4 = driver.findElement(home2.color1).getCssValue("color");
					System.out.println(fullcol4);
					steps.clickJSButton(home2.color1, driver, name);
					Thread.sleep(2000);
					Product3ExpFullSleeveColor1 = driver.findElement(home2.color1).getCssValue("background-color");
					Product3ExpFullSleevehex1 = Color.fromString(Product3ExpFullSleeveColor1).asHex();
					System.out.println("Image color: "+Product3ExpFullSleevehex1);
					test.log(Status.INFO, MarkupHelper.createLabel("User changed item color to:  "+Product3ExpFullSleevehex1, ExtentColor.BLUE));
					Product3ExpFullSleeveFirstcolor = getscreen.capture(driver, "Product3ExpFullSleeveFirstcolor");
					test.addScreenCaptureFromPath(Product3ExpFullSleeveFirstcolor);
				}
				catch(Exception e)
				{
					System.out.println("Color 1 is not exists");
				}
				try 
				{
					String fullcol5 = driver.findElement(home2.color2).getCssValue("color");
					System.out.println(fullcol5);
					steps.clickJSButton(home2.color2, driver, name);
					Thread.sleep(1000);
					Product3ExpFullSleeveColor2 = driver.findElement(home2.color2).getCssValue("background-color");
					Product3ExpFullSleevehex2 = Color.fromString(Product3ExpFullSleeveColor2).asHex();
					System.out.println("Image color: "+Product3ExpFullSleevehex2);
					test.log(Status.INFO, MarkupHelper.createLabel("User changed item color to:  "+Product3ExpFullSleevehex2, ExtentColor.BLUE));
					Product3ExpFullSleeveSecondcolor = getscreen.capture(driver, "Product3ExpFullSleeveSecondcolor");
					test.addScreenCaptureFromPath(Product3ExpFullSleeveSecondcolor);
				}
				catch(Exception e)
				{
					System.out.println("Color 2 is not exists");
				}
				try 
				{
					String fullcol6 = driver.findElement(home2.color4).getCssValue("color");
					System.out.println(fullcol6);
					steps.clickJSButton(home2.color4, driver, name);
					Thread.sleep(1000);
					Product3ExpFullSleeveColor3 = driver.findElement(home2.color4).getCssValue("background-color");
					Product3ExpFullSleevehex3 = Color.fromString(Product3ExpFullSleeveColor3).asHex();
					System.out.println("Image color: "+Product3ExpFullSleevehex3);
					test.log(Status.INFO, MarkupHelper.createLabel("User changed item color to:  "+Product3ExpFullSleevehex3, ExtentColor.BLUE));
					Product3ExpFullSleeveThirdcolor = getscreen.capture(driver, "Product3ExpFullSleeveThirdcolor");
					test.addScreenCaptureFromPath(Product3ExpFullSleeveThirdcolor);
				}
				catch(Exception e)
				{
					System.out.println("Color 3 is not exists");
				}
				System.out.println("Product 3 Exists");
			}
			catch(Exception e)
			{
				System.out.println("Product 3 doesn't exists");
			}
			Thread.sleep(2000);
			try
			{
				steps.clickJSButton(home2.ExploreCat_FullSleeve_Product1, driver, name);
				String prod1 = driver.findElement(home2.ExploreCat_FullSleeve_Product1).getCssValue("color");
				System.out.println(prod1);
				try
				{
					String fullcol7 = driver.findElement(home2.color1).getCssValue("color");
					System.out.println(fullcol7);
					steps.clickJSButton(home2.color1, driver, name);
					Thread.sleep(2000);
					Product1ExpFullSleeveColor1 = driver.findElement(home2.color1).getCssValue("background-color");
					Product1ExpFullSleevehex1 = Color.fromString(Product1ExpFullSleeveColor1).asHex();
					System.out.println("Image color: "+Product1ExpFullSleevehex1);
					test.log(Status.INFO, MarkupHelper.createLabel("User changed item color to:  "+Product1ExpFullSleevehex1, ExtentColor.BLUE));
					Product1ExpFullSleeveFirstcolor = getscreen.capture(driver, "Product1ExpFullSleeveFirstcolor");
					test.addScreenCaptureFromPath(Product1ExpFullSleeveFirstcolor);
				}
				catch(Exception e)
				{
					System.out.println("Color 1 is not exists");
				}
				try 
				{
					String fullcol8 = driver.findElement(home2.color2).getCssValue("color");
					System.out.println(fullcol8);
					steps.clickJSButton(home2.color2, driver, name);
					Thread.sleep(1000);
					Product1ExpFullSleeveColor2 = driver.findElement(home2.color2).getCssValue("background-color");
					Product1ExpFullSleevehex2 = Color.fromString(Product1ExpFullSleeveColor2).asHex();
					System.out.println("Image color: "+Product1ExpFullSleevehex2);
					test.log(Status.INFO, MarkupHelper.createLabel("User changed item color to:  "+Product1ExpFullSleevehex2, ExtentColor.BLUE));
					Product1ExpFullSleeveSecondcolor = getscreen.capture(driver, "Product1ExpFullSleeveSecondcolor");
					test.addScreenCaptureFromPath(Product1ExpFullSleeveSecondcolor);
				}
				catch(Exception e)
				{
					System.out.println("Color 2 is not exists");
				}
				try 
				{
					String fullcol9 = driver.findElement(home2.color4).getCssValue("color");
					System.out.println(fullcol9);
					steps.clickJSButton(home2.color4, driver, name);
					Thread.sleep(1000);
					Product1ExpFullSleeveColor3 = driver.findElement(home2.color4).getCssValue("background-color");
					Product1ExpFullSleevehex3 = Color.fromString(Product1ExpFullSleeveColor3).asHex();
					System.out.println("Image color: "+Product1ExpFullSleevehex3);
					test.log(Status.INFO, MarkupHelper.createLabel("User changed item color to:  "+Product1ExpFullSleevehex3, ExtentColor.BLUE));
					Product1ExpFullSleeveThirdcolor = getscreen.capture(driver, "Product1ExpFullSleeveThirdcolor");
					test.addScreenCaptureFromPath(Product1ExpFullSleeveThirdcolor);
				}
				catch(Exception e)
				{
					System.out.println("Color 3 is not exists");
				}
				System.out.println("Product 1 Exists");
			}
			catch(Exception e)
			{
				System.out.println("Product 1 doesn't exists");
			}
			Thread.sleep(2000);
			ExpFullSleevecqty = driver.findElement(home.Qty).getAttribute("value");
			System.out.println("Current Quantity:  "+ ExpFullSleevecqty);
			test.log(Status.INFO, MarkupHelper.createLabel("Current Quantity of product:  "+ExpFullSleevecqty, ExtentColor.BLUE));
			Thread.sleep(1000);
			steps.clickJSButton(home.Trend_QtyIncrease, driver, name);
			ExpFullSleeveiqty = driver.findElement(home.Qty).getAttribute("value");
			System.out.println("Quantity after increase:  "+ ExpFullSleeveiqty);
			test.log(Status.INFO, MarkupHelper.createLabel("Current Quantity of product after Increase:  "+ExpFullSleeveiqty, ExtentColor.BLUE));
			steps.clickJSButton(home.Trend_QtyDecrease, driver, name);
			ExpFullSleevedqty = driver.findElement(home.Qty).getAttribute("value");
			System.out.println("Quantity after decrease:  "+ ExpFullSleevedqty);
			test.log(Status.INFO, MarkupHelper.createLabel("Current Quantity of product after Decrease:  "+ExpFullSleevedqty, ExtentColor.BLUE));
			steps.clickJSButton(home.Trend_Addcart, driver, name);
			ExpFullSleeveSizemsg = driver.findElement(home.Trend_Sizeerrormsg).getText();
			System.out.println("User clicked on Addcart before Size selection:  "+ExpFullSleeveSizemsg);
			test.log(Status.INFO, MarkupHelper.createLabel("User clicked on Addcart before Size selection:  "+ExpFullSleeveSizemsg, ExtentColor.RED));
			//steps.clickJSButton(home2.ExpFullSleeve_sizeS, driver, name);
			steps.clickJSButton(home2.Size_S, driver, name);
			ExpFullSleevesizeS = driver.findElement(home2.Size_S).getText();
			System.out.println("Selected Size:  "+ExpFullSleevesizeS);
			test.log(Status.INFO, MarkupHelper.createLabel("User selected size:  "+ExpFullSleevesizeS, ExtentColor.BLUE));
			steps.clickJSButton(home2.Size_M, driver, name);
			ExpFullSleevesizeM = driver.findElement(home2.Size_M).getText();
			System.out.println("Selected Size:  "+ExpFullSleevesizeM);
			test.log(Status.INFO, MarkupHelper.createLabel("User selected size:  "+ExpFullSleevesizeM, ExtentColor.BLUE));
			steps.clickJSButton(home2.Size_L, driver, name);
			ExpFullSleevesizeL = driver.findElement(home2.Size_L).getText();
			System.out.println("Selected Size:  "+ExpFullSleevesizeL);
			test.log(Status.INFO, MarkupHelper.createLabel("User selected size:  "+ExpFullSleevesizeL, ExtentColor.BLUE));
			steps.clickJSButton(home2.Size_XL, driver, name);
			ExpFullSleevesizeXL = driver.findElement(home2.Size_XL).getText();
			System.out.println("Selected Size:  "+ExpFullSleevesizeXL);
			test.log(Status.INFO, MarkupHelper.createLabel("User selected size:  "+ExpFullSleevesizeXL, ExtentColor.BLUE));
			steps.clickJSButton(home2.Size_XXL, driver, name);
			ExpFullSleevesizeXXL = driver.findElement(home2.Size_XXL).getText();
			System.out.println("Selected Size:  "+ExpFullSleevesizeXXL);
			test.log(Status.INFO, MarkupHelper.createLabel("User selected size:  "+ExpFullSleevesizeXXL, ExtentColor.BLUE));
			steps.clickJSButton(home2.Size_XXXL, driver, name);
			ExpFullSleevesizeXXXL = driver.findElement(home2.Size_XXXL).getText();
			System.out.println("Selected Size:  "+ExpFullSleevesizeXXXL);
			test.log(Status.INFO, MarkupHelper.createLabel("User selected size:  "+ExpFullSleevesizeXXXL, ExtentColor.BLUE));
			steps.clickJSButton(home.Trend_FindUrSize, driver, name);
			test.log(Status.INFO, MarkupHelper.createLabel("User clicked on Find your size", ExtentColor.BLUE));
			Thread.sleep(1000);
			try
			{
				String fullradio = driver.findElement(home.Trend_FindUrSize_510radio).getText();
				System.out.println(fullradio);
				steps.clickJSButton(home.Trend_FindUrSize_510radio, driver, name);
				ExpFullSleevebelow510 = driver.findElement(home.Trend_FindUrSize_510radio).getText();
				System.out.println("User Selected:  "+ExpFullSleevebelow510);
				test.log(Status.INFO, MarkupHelper.createLabel("User selected Height:  "+ExpFullSleevebelow510, ExtentColor.BLUE));
				steps.clickJSButton(home.Trend_FindUrSize_above510radio, driver, name);
				ExpFullSleeveabove510 = driver.findElement(home.Trend_FindUrSize_above510radio).getText();
				System.out.println("User Selected:  "+ExpFullSleeveabove510);
				test.log(Status.INFO, MarkupHelper.createLabel("User selected Height:  "+ExpFullSleeveabove510, ExtentColor.BLUE));
				steps.clickJSButton(home.Trend_FindUrSize_Tightradio, driver, name);
				ExpFullSleevetightfit = driver.findElement(home.Trend_FindUrSize_Tightradio).getText();
				System.out.println("User Selected:  "+ExpFullSleevetightfit);
				test.log(Status.INFO, MarkupHelper.createLabel("User selected Fit:  "+ExpFullSleevetightfit, ExtentColor.BLUE));
				steps.clickJSButton(home.Trend_FindUrSize_Comfortradio, driver, name);
				ExpFullSleevecomfit = driver.findElement(home.Trend_FindUrSize_Comfortradio).getText();
				System.out.println("User Selected:  "+ExpFullSleevecomfit);
				test.log(Status.INFO, MarkupHelper.createLabel("User selected Fit:  "+ExpFullSleevecomfit, ExtentColor.BLUE));
				steps.selectDropdown(home.Trend_FindUrSize_sizeDD, driver, strDataFileName, "size", name);
				test.log(Status.INFO, MarkupHelper.createLabel("User selected shirt size", ExtentColor.BLUE));
				steps.clickJSButton(home.Trend_FindUrSize_submit, driver, name);
				test.log(Status.INFO, MarkupHelper.createLabel("User clicked on Submit", ExtentColor.BLUE));
				ExpFullSleevefindursize = getscreen.capture(driver, "ExpFullSleevefindursize");
				test.addScreenCaptureFromPath(ExpFullSleevefindursize);
				Thread.sleep(1000);
				steps.clickJSButton(home.Trend_FindUrSize_Okbutton, driver, name);
				test.log(Status.INFO, MarkupHelper.createLabel("system displayed size for given metrics", ExtentColor.BLUE));
				steps.clickJSButton(home.Trend_FindUrSize_closebutton, driver, name);
				test.log(Status.INFO, MarkupHelper.createLabel("User navigate to Product details page", ExtentColor.BLUE));
			}
			catch(Exception e)
			{
				steps.clickJSButton(home.Trend_FindUrSize_closebutton, driver, name);
				test.log(Status.INFO, MarkupHelper.createLabel("User navigate to Product details page", ExtentColor.BLUE));
			}
			steps.clickJSButton(home2.Size_L, driver, name);
			test.log(Status.INFO, MarkupHelper.createLabel("User selected size", ExtentColor.BLUE));
			steps.clickJSButton(home.Trend_Addcart, driver, name);
			Thread.sleep(1000);
			test.log(Status.INFO, MarkupHelper.createLabel("User added product to Cart", ExtentColor.BLUE));
			ExpFullSleeveaddcart = getscreen.capture(driver, "ExpFullSleeveaddcart");
			test.addScreenCaptureFromPath(ExpFullSleeveaddcart);
			Thread.sleep(1000);
			try
			{
				String fulledit = driver.findElement(home2.Addcart_edit).getText();
				System.out.println(fulledit);
				steps.clickJSButton(home2.Addcart_edit, driver, name);
				test.log(Status.INFO, MarkupHelper.createLabel("User clicked on Edit", ExtentColor.BLUE));
				steps.selectDropdown(home2.Addcart_selectsize, driver, strDataFileName, "SelectSize", name);
				steps.selectDropdown(home2.Addcart_selectqty, driver, strDataFileName, "Quantity", name);
				test.log(Status.INFO, MarkupHelper.createLabel("User selected Size and Quantity", ExtentColor.BLUE));
				steps.clickJSButton(home2.Addcart_cancel, driver, name);
				steps.clickJSButton(home2.Addcart_edit, driver, name);
				steps.selectDropdown(home2.Addcart_selectsize, driver, strDataFileName, "SelectSize", name);
				steps.selectDropdown(home2.Addcart_selectqty, driver, strDataFileName, "Quantity", name);
				steps.clickJSButton(home2.Addcart_save, driver, name);
				ExpFullSleeveupdateproduct1 = getscreen.capture(driver, "ExpFullSleeveupdateproduct1");
				test.addScreenCaptureFromPath(ExpFullSleeveupdateproduct1);
				test.log(Status.INFO, MarkupHelper.createLabel("User updated product size and quantity", ExtentColor.BLUE));
				Thread.sleep(2000);
			}
			catch(Exception e)
			{
				System.out.println("Edit feature is not availabe for this product");
			}
			steps.clickJSButton(home2.Biker_remove, driver, name);
			ExpFullSleeveremovecart = getscreen.capture(driver, "ExpFullSleeveremovecart");
			test.addScreenCaptureFromPath(ExpFullSleeveremovecart);
			test.log(Status.INFO, MarkupHelper.createLabel("User removed product from Cart", ExtentColor.BLUE));
			Thread.sleep(1000);
			//checkout process
			steps.clickJSButton(home.Size_L, driver, name);
			test.log(Status.INFO, MarkupHelper.createLabel("User selected size", ExtentColor.BLUE));
			steps.clickJSButton(home.BuyNow, driver, name);
			test.log(Status.INFO, MarkupHelper.createLabel("User clicked on BuyNow, navigated to checkout page", ExtentColor.BLUE));
			FSbuynow1 = getscreen.capture(driver, "FSbuynow1");
			test.addScreenCaptureFromPath(FSbuynow1);
			steps.clickJSButton(home.CashonDeliveryradio, driver, name);
			FSbuynow2 = driver.findElement(home.PopUpSaveMsg).getText();
			System.out.println("Displayed message:   "+FSbuynow2);
			test.log(Status.INFO, MarkupHelper.createLabel("Displayed message:   "+FSbuynow2, ExtentColor.BLUE));
			Thread.sleep(3000);
			/*	buynow3 = driver.findElement(home.PopUpMsg).getAttribute("value");
			System.out.println("Displayed message:   "+buynow3);
			test.log(Status.INFO, MarkupHelper.createLabel("Displayed message:   "+buynow3, ExtentColor.BLUE));
			 */
			FSbuynow4 = getscreen.capture(driver, "FSbuynow4");
			test.addScreenCaptureFromPath(FSbuynow4);
			steps.clickJSButton(home.Cashondelivery, driver, name);
			test.log(Status.INFO, MarkupHelper.createLabel("User clicked on Cash on Delivery link", ExtentColor.BLUE));
			Thread.sleep(4000);
			steps.clickJSButton(home.CartIcon, driver, name);
			steps.clickJSButton(home.Trend_Addcart_remove, driver, name);
			steps.clickJSButton(home.Mydreamstore, driver, name);
			test.log(Status.INFO, MarkupHelper.createLabel("User Completed transactions of 1st Image in FullSleeve under Explore Categories", ExtentColor.PURPLE));
			ExpFullSleevestatus = passstatus;
			Assert.assertTrue(true);
			System.out.println("FullSleeve under Explore Categories Successful");
			testresultdata.put(Integer.toString(cnt), new Object[] {name, "FullSleeve under Explore Categories", "FullSleeve under Explore Categories Successful",passstatus});	
		}
		catch(Exception e)
		{
			System.out.println("FullSleeve under Explore Categories Unsuccessful");
			testresultdata.put(Integer.toString(cnt), new Object[] {name, "FullSleeve under Explore Categories", "FullSleeve under Explore Categories Unsuccessful",failstatus});
			ExpFullSleevestatus = failstatus;
			ExpFullSleevestatusfail = getscreen.capture(driver, "ExpFullSleevestatusfail");
			test.addScreenCaptureFromPath(ExpFullSleevestatusfail);
			Assert.assertTrue(false);
		}
	}
	@Test(priority = 3)
	public void ExploreMobileCases() throws IOException, InterruptedException
	{
		String name = new Object(){}.getClass().getEnclosingMethod().getName();
		cnt ++;
		try 
		{
			test = extent.createTest("Mobile Cases under Explore Categories");
			System.out.println("Mobile Cases under Explore Categories Test Case Executing...");
			Thread.sleep(2000);
			commonmethods.URLNaviagtion(driver);
			Thread.sleep(1000);
			/*commonmethods.Login(driver);
			test.log(Status.INFO, MarkupHelper.createLabel("User able to Login", ExtentColor.BLUE));
			Thread.sleep(3000);
			steps.clickJSButton(home.Mydreamstore, driver, name);
			String homepage = getscreen.capture(driver, name);
			test.addScreenCaptureFromPath(homepage);*/
			steps.MoveElement(home2.ExploreCat_MobileCases, driver, name);
			steps.clickJSButton(home2.ExploreCat_MobileCases, driver, name);
			test.log(Status.INFO, MarkupHelper.createLabel("User clicked on MobileCases Image under Explore Categories", ExtentColor.BLUE));
			ExpMobileCasesPage = getscreen.capture(driver, "ExpMobileCasesPage");
			test.addScreenCaptureFromPath(ExpMobileCasesPage);
			//Thread.sleep(20000);
			try
			{
			steps.WaitUntilElementPresent(home2.ExploreCat_MobileCases_Image1, driver);
			System.out.println("User navigated to Filter page");
			//steps.clickJSButton(home2.ExploreCat_MobileCases_Image1, driver, name);
			test.log(Status.INFO, MarkupHelper.createLabel("User clicked on 1st Image in T Shirts under Explore Categories", ExtentColor.BLUE));
			ExpMobileCasesPage2 = getscreen.capture(driver, "ExpMobileCasesPage2");
			test.addScreenCaptureFromPath(ExpMobileCasesPage2);
			}
			catch(Exception e)
			{
				System.out.println("User navigated to wrong page");
			}
			Thread.sleep(1000);
			try
			{
				steps.clickJSButton(home2.Photography_Image1_Readmore, driver, name);
				ExpMobileCasesDescription = driver.findElement(home2.ExploreCat_MobileCases_Description1).getText();
				System.out.println("Production Description   "+ExpMobileCasesDescription);
				test.log(Status.INFO, MarkupHelper.createLabel("Description about Item:  "+ExpMobileCasesDescription, ExtentColor.BLUE));
				steps.clickJSButton(home2.Profession_Readless, driver, name);
			}
			catch(Exception e)
			{
				steps.clickJSButton(home2.Photography_Image1_Readmore, driver, name);
				ExpMobileCasesDescription1 = driver.findElement(home2.ExploreCat_MobileCases_Description).getText();
				System.out.println("Production Description   "+ExpMobileCasesDescription1);
				test.log(Status.INFO, MarkupHelper.createLabel("Description about Item:  "+ExpMobileCasesDescription1, ExtentColor.BLUE));
				steps.clickJSButton(home2.Profession_Readless, driver, name);
			}
			Thread.sleep(1000);
			/*ExpMobileCasesDescription = driver.findElement(home2.ExploreCat_MobileCases_Description).getText();
			System.out.println("Production Description   "+ExpMobileCasesDescription);
			test.log(Status.INFO, MarkupHelper.createLabel("Description about Item:  "+ExpMobileCasesDescription, ExtentColor.BLUE));
			 */
			ExpMobileCasesMRP = driver.findElement(home.Trend_Image_MRP).getText();
			System.out.println("Product Price   "+ExpMobileCasesMRP);
			test.log(Status.INFO, MarkupHelper.createLabel("MRP of an Product:  "+ExpMobileCasesMRP, ExtentColor.ORANGE));
			Thread.sleep(1000);
			//steps.selectDropdown(home2.ExploreCat_MobileCases_Dropdown, driver, strDataFileName, "Iphone7", name);
			steps.selectDropdownIndex(home2.ExploreCat_MobileCases_Dropdown, driver, 2, name);
			Thread.sleep(1000);
			//Dropdown1 = driver.findElement(home2.ExploreCat_MobileCases_Dropdown).getFirstSelectedOption().getText();
			String Dropdown1 = new Select(driver.findElement(home2.ExploreCat_MobileCases_Dropdown)).getFirstSelectedOption().getText();
			System.out.println("Product Selected:   "+Dropdown1);
			test.log(Status.INFO, MarkupHelper.createLabel("User selected:  "+Dropdown1, ExtentColor.BLUE));
			ExpMobileCasesDropdown1 = getscreen.capture(driver, "ExpMobileCasesDropdown1");
			test.addScreenCaptureFromPath(ExpMobileCasesDropdown1);
			Thread.sleep(1000);
			steps.selectDropdownIndex(home2.ExploreCat_MobileCases_Dropdown, driver, 6, name);
			String Dropdown2 = new Select(driver.findElement(home2.ExploreCat_MobileCases_Dropdown)).getFirstSelectedOption().getText();
			Thread.sleep(1000);
			System.out.println("Product Selected:   "+Dropdown2);
			test.log(Status.INFO, MarkupHelper.createLabel("User selected:  "+Dropdown2, ExtentColor.BLUE));
			ExpMobileCasesDropdown2 = getscreen.capture(driver, "ExpMobileCasesDropdown2");
			test.addScreenCaptureFromPath(ExpMobileCasesDropdown2);
			Thread.sleep(1000);
			steps.selectDropdownIndex(home2.ExploreCat_MobileCases_Dropdown, driver, 10, name);
			Thread.sleep(1000);
			String Dropdown3 = new Select(driver.findElement(home2.ExploreCat_MobileCases_Dropdown)).getFirstSelectedOption().getText();
			System.out.println("Product Selected:   "+Dropdown3);
			test.log(Status.INFO, MarkupHelper.createLabel("User selected:  "+Dropdown3, ExtentColor.BLUE));
			ExpMobileCasesDropdown3 = getscreen.capture(driver, "ExpMobileCasesDropdown3");
			test.addScreenCaptureFromPath(ExpMobileCasesDropdown3);
			Thread.sleep(1000);
			steps.selectDropdownIndex(home2.ExploreCat_MobileCases_Dropdown, driver, 13, name);
			Thread.sleep(1000);
			String Dropdown4 = new Select(driver.findElement(home2.ExploreCat_MobileCases_Dropdown)).getFirstSelectedOption().getText();
			System.out.println("Product Selected:   "+Dropdown4);
			test.log(Status.INFO, MarkupHelper.createLabel("User selected:  "+Dropdown4, ExtentColor.BLUE));
			ExpMobileCasesDropdown4 = getscreen.capture(driver, "ExpMobileCasesDropdown4");
			test.addScreenCaptureFromPath(ExpMobileCasesDropdown4);
			Thread.sleep(2000);
			ExpMobileCasescqty = driver.findElement(home.Qty).getAttribute("value");
			System.out.println("Current Quantity:  "+ ExpMobileCasescqty);
			test.log(Status.INFO, MarkupHelper.createLabel("Current Quantity of product:  "+ExpMobileCasescqty, ExtentColor.BLUE));
			Thread.sleep(1000);
			steps.clickJSButton(home.Trend_QtyIncrease, driver, name);
			ExpMobileCasesiqty = driver.findElement(home.Qty).getAttribute("value");
			System.out.println("Quantity after increase:  "+ ExpMobileCasesiqty);
			test.log(Status.INFO, MarkupHelper.createLabel("Current Quantity of product after Increase:  "+ExpMobileCasesiqty, ExtentColor.BLUE));
			steps.clickJSButton(home.Trend_QtyDecrease, driver, name);
			ExpMobileCasesdqty = driver.findElement(home.Qty).getAttribute("value");
			System.out.println("Quantity after decrease:  "+ ExpMobileCasesdqty);
			test.log(Status.INFO, MarkupHelper.createLabel("Current Quantity of product after increase:  "+ExpMobileCasesdqty, ExtentColor.BLUE));
			steps.clickJSButton(home.Trend_Addcart, driver, name);
			Thread.sleep(1000);
			test.log(Status.INFO, MarkupHelper.createLabel("User added product to Cart", ExtentColor.BLUE));
			ExpMobileCasesaddcart = getscreen.capture(driver, "ExpMobileCasesaddcart");
			test.addScreenCaptureFromPath(ExpMobileCasesaddcart);
			Thread.sleep(1000);
			steps.clickJSButton(home2.Addcart_edit, driver, name);
			test.log(Status.INFO, MarkupHelper.createLabel("User clicked on Edit", ExtentColor.BLUE));
			steps.selectDropdown(home2.Addcart_selectqty, driver, strDataFileName, "Quantity", name);
			test.log(Status.INFO, MarkupHelper.createLabel("User selected Size and Quantity", ExtentColor.BLUE));
			steps.clickJSButton(home2.Addcart_cancel, driver, name);
			steps.clickJSButton(home2.Addcart_edit, driver, name);
			steps.selectDropdown(home2.Addcart_selectqty, driver, strDataFileName, "Quantity", name);
			steps.clickJSButton(home2.Addcart_save, driver, name);
			ExpMobileCasesupdateproduct1 = getscreen.capture(driver, "ExpMobileCasesupdateproduct1");
			test.addScreenCaptureFromPath(ExpMobileCasesupdateproduct1);
			test.log(Status.INFO, MarkupHelper.createLabel("User updated product quantity", ExtentColor.BLUE));
			Thread.sleep(2000);
			steps.clickJSButton(home2.Biker_remove, driver, name);
			ExpMobileCasesremovecart = getscreen.capture(driver, "ExpMobileCasesremovecart");
			test.addScreenCaptureFromPath(ExpMobileCasesremovecart);
			test.log(Status.INFO, MarkupHelper.createLabel("User removed product from Cart", ExtentColor.BLUE));
			Thread.sleep(1000);
			steps.clickJSButton(home.Mydreamstore, driver, name);
			test.log(Status.INFO, MarkupHelper.createLabel("User Completed transactions of 1st Image in MobileCases under Explore Categories", ExtentColor.PURPLE));
			ExpMobileCasesstatus = passstatus;
			Assert.assertTrue(true);
			System.out.println("MobileCases under Explore Categories Successful");
			testresultdata.put(Integer.toString(cnt), new Object[] {name, "MobileCases under Explore Categories", "MobileCases under Explore Categories Successful",passstatus});	
		}
		catch(Exception e)
		{
			System.out.println("MobileCases under Explore Categories Unsuccessful");
			testresultdata.put(Integer.toString(cnt), new Object[] {name, "MobileCases under Explore Categories", "MobileCases under Explore Categories Unsuccessful",failstatus});
			ExpMobileCasesstatus = failstatus;
			ExpMobileCasesstatusfail = getscreen.capture(driver, "ExpMobileCasesstatusfail");
			test.addScreenCaptureFromPath(ExpMobileCasesstatusfail);
			Assert.assertTrue(false);
		}
	}
	@Test(priority = 4)
	public void ExploreCoffeeMugs() throws IOException, InterruptedException
	{
		String name = new Object(){}.getClass().getEnclosingMethod().getName();
		cnt ++;
		try 
		{
			test = extent.createTest("Coffee Mugs under Explore Categories");
			System.out.println("Coffee Mugs under Explore Categories Test Case Executing...");
			Thread.sleep(1000);
			commonmethods.URLNaviagtion(driver);
			Thread.sleep(1000);
		/*	commonmethods.Login(driver);
			test.log(Status.INFO, MarkupHelper.createLabel("User able to Login", ExtentColor.BLUE));
			Thread.sleep(3000);
*/
			steps.clickJSButton(home.Mydreamstore, driver, name);
			String Parent = driver.getWindowHandle();
			String homepage = getscreen.capture(driver, name);
			test.addScreenCaptureFromPath(homepage);
			steps.MoveElement(home2.ExploreCat_CoffeeMugs, driver, name);
			steps.clickJSButton(home2.ExploreCat_CoffeeMugs, driver, name);
			test.log(Status.INFO, MarkupHelper.createLabel("User clicked on Coffee Mugs Image under Explore Categories", ExtentColor.BLUE));
			ExpCoffeemugsPage = getscreen.capture(driver, "ExpCoffeemugsPage");
			test.addScreenCaptureFromPath(ExpCoffeemugsPage);
			Thread.sleep(20000);
/*			ExpCoffeemugsPage1 = driver.findElement(home2.ExploreCat_Coffemugs_Page).getText();
			System.out.println("Coffe Mugs Page contains:   "+ExpCoffeemugsPage1);
*/
			ExpCoffeemugsPage2 = getscreen.capture(driver, "ExpCoffeemugsPage2");
			test.addScreenCaptureFromPath(ExpCoffeemugsPage2);
			Thread.sleep(1000);
			steps.clickJSButton(home2.ExploreCat_Coffemugs_Image1, driver, name);
			steps.ChildWindow(driver);
			driver.manage().window().maximize();
			try
			{
				steps.clickJSButton(home2.Photography_Image1_Readmore, driver, name);
				ExpCoffeeDescription = driver.findElement(home2.ExploreCat_MobileCases_Description1).getText();
				System.out.println("Production Description   "+ExpCoffeeDescription);
				test.log(Status.INFO, MarkupHelper.createLabel("Description about Item:  "+ExpCoffeeDescription, ExtentColor.BLUE));
				steps.clickJSButton(home2.Profession_Readless, driver, name);
			}
			catch(Exception e)
			{
				steps.clickJSButton(home2.Photography_Image1_Readmore, driver, name);
				ExpCoffeeDescription1 = driver.findElement(home2.ExploreCat_MobileCases_Description).getText();
				System.out.println("Production Description   "+ExpCoffeeDescription1);
				test.log(Status.INFO, MarkupHelper.createLabel("Description about Item:  "+ExpCoffeeDescription1, ExtentColor.BLUE));
				steps.clickJSButton(home2.Profession_Readless, driver, name);
			}
			Thread.sleep(1000);
			ExpCoffeecqty = driver.findElement(home.Qty).getAttribute("value");
			System.out.println("Current Quantity:  "+ ExpCoffeecqty);
			test.log(Status.INFO, MarkupHelper.createLabel("Current Quantity of product:  "+ExpCoffeecqty, ExtentColor.BLUE));
			Thread.sleep(1000);
			steps.clickJSButton(home.Trend_QtyIncrease, driver, name);
			ExpCoffeeiqty = driver.findElement(home.Qty).getAttribute("value");
			System.out.println("Quantity after increase:  "+ ExpCoffeeiqty);
			test.log(Status.INFO, MarkupHelper.createLabel("Current Quantity of product after Increase:  "+ExpCoffeeiqty, ExtentColor.BLUE));
			steps.clickJSButton(home.Trend_QtyDecrease, driver, name);
			ExpCoffeedqty = driver.findElement(home.Qty).getAttribute("value");
			System.out.println("Quantity after decrease:  "+ ExpCoffeedqty);
			test.log(Status.INFO, MarkupHelper.createLabel("Current Quantity of product after Decrease:  "+ExpCoffeedqty, ExtentColor.BLUE));
			ExpCoffeeMRP = driver.findElement(home.Trend_Image_MRP).getText();
			System.out.println("Product Price   "+ExpCoffeeMRP);
			test.log(Status.INFO, MarkupHelper.createLabel("MRP of an Product:  "+ExpCoffeeMRP, ExtentColor.ORANGE));
			steps.clickJSButton(home.Trend_Addcart, driver, name);
			Thread.sleep(1000);
			steps.clickJSButton(home2.Biker_remove, driver, name);
			Thread.sleep(1000);
			test.log(Status.INFO, MarkupHelper.createLabel("User added product to Cart", ExtentColor.BLUE));
			driver.close();
			driver.switchTo().window(Parent);
			test.log(Status.INFO, MarkupHelper.createLabel("User Completed transactions of 1st Image in Coffee Mugs under Explore Categories", ExtentColor.PURPLE));
			ExpCoffeemugsstatus = passstatus;
			Assert.assertTrue(true);
			System.out.println("Coffee Mugs under Explore Categories Successful");
			testresultdata.put(Integer.toString(cnt), new Object[] {name, "Coffee Mugs under Explore Categories", "Coffee Mugs under Explore Categories Successful",passstatus});	
		}
		catch(Exception e)
		{
			System.out.println("Coffee Mugs under Explore Categories Unsuccessful");
			testresultdata.put(Integer.toString(cnt), new Object[] {name, "Coffee Mugs under Explore Categories", "Coffee Mugs under Explore Categories Unsuccessful",failstatus});
			ExpCoffeemugsstatus = failstatus;
			ExpCoffeemugsstatusfail = getscreen.capture(driver, "ExpCoffeemugsstatusfail");
			test.addScreenCaptureFromPath(ExpCoffeemugsstatusfail);
			Assert.assertTrue(false);
		}
	}
	@BeforeClass
	public void before()
	{
		workbook = new HSSFWorkbook();
		sheet = workbook.createSheet(strDataFileName);
		testresultdata = new LinkedHashMap<String, Object[]>();
		testresultdata.put("1", new Object[] { "Test Step Name", "Expected Result","Actual Result" ,"Status"});
	}
	@AfterClass
	public void after() throws Exception
	{
		Set<String> keyset = testresultdata.keySet();
		int rownum = 0;
		for (String key : keyset) {
			Row row = sheet.createRow(rownum++);
			Object [] objArr = testresultdata.get(key);
			int cellnum = 0;
			for (Object obj : objArr) {
				Cell cell = row.createCell(cellnum++);
				if(obj instanceof Date) 
					cell.setCellValue((Date)obj);
				else if(obj instanceof Boolean)
					cell.setCellValue((Boolean)obj);
				else if(obj instanceof String)
					cell.setCellValue((String)obj);
				else if(obj instanceof Double)
					cell.setCellValue((Double)obj);
			}
		}
		try {
			FileOutputStream out = new FileOutputStream(new File("TestResults/"+strDataFileName +".xls"));
			workbook.write(out);
			out.close();
			System.out.println("Excel written successfully..");
			TestResult();
			driver.quit();
		}
		catch (FileNotFoundException e) 
		{
			e.printStackTrace();
		} 
		catch (IOException e) 
		{
			e.printStackTrace();
		}
	}
	public void TestResult() throws Exception
	{
		HSSFRow row;
		HSSFCell cell;
		WritableFont cellFont;
		WritableCellFormat cellFormat;
		Border border = null;
		int count;
		FileInputStream inp = new FileInputStream( "TestResults/"+strDataFileName +".xls");
		HSSFWorkbook wb = new HSSFWorkbook(inp);
		HSSFSheet sh = wb.getSheetAt(0);
		count=sh.getLastRowNum();
		for (int i = 1; i <= count; i++) 
		{
			row=sh.getRow(i);
			cell=row.getCell(3);
			String status=row.getCell(3).getStringCellValue();
			if(status.equalsIgnoreCase("Pass"))
			{
				HSSFCellStyle pass= wb.createCellStyle();
				pass.setFillForegroundColor(IndexedColors.GREEN.getIndex());
				pass.setFillPattern(CellStyle.SOLID_FOREGROUND);
				cell.setCellStyle(pass);
			}
			else
			{
				HSSFCellStyle fail= wb.createCellStyle();
				fail.setFillForegroundColor(IndexedColors.RED.getIndex());
				fail.setFillPattern(CellStyle.SOLID_FOREGROUND);
				cell.setCellStyle(fail);
			}
		}
		FileOutputStream out = new FileOutputStream("TestResults/"+strDataFileName+".xls");
		wb.write(out);
		wb.close();
	}
}
