package Com.Homepage;
import java.io.File;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.IOException;
import java.util.LinkedHashMap;
import java.util.Map;
import java.util.Set;
import java.util.StringTokenizer;
import java.util.Date;
import java.util.concurrent.TimeUnit;
import org.apache.poi.hssf.usermodel.HSSFCell;
import org.apache.poi.hssf.usermodel.HSSFCellStyle;
import org.apache.poi.hssf.usermodel.HSSFRow;
import org.apache.poi.hssf.usermodel.HSSFSheet;
import org.apache.poi.hssf.usermodel.HSSFWorkbook;
import org.apache.poi.ss.usermodel.Row;
import org.apache.poi.ss.usermodel.Cell;
import org.apache.poi.ss.usermodel.CellStyle;
import org.apache.poi.ss.usermodel.IndexedColors;
import org.openqa.selenium.By;
import org.openqa.selenium.Keys;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.interactions.Actions;
import org.openqa.selenium.support.Color;
import org.openqa.selenium.support.ui.ExpectedConditions;
import org.openqa.selenium.support.ui.WebDriverWait;
import org.testng.Assert;
import org.testng.annotations.AfterClass;
import org.testng.annotations.BeforeClass;
import org.testng.annotations.Test;
import com.aventstack.extentreports.Status;
import com.aventstack.extentreports.markuputils.ExtentColor;
import com.aventstack.extentreports.markuputils.MarkupHelper;
import Com.ObjectRepository.Homepage_OR;
import Com.ObjectRepository.Login_OR;
import Com.Utilities.Browsers;
import Com.Utilities.CommonMethods;
import Com.Utilities.GetScreenShot;
import Com.Utilities.Screenshot;
import Com.Utilities.Steps;
import Com.Utilities.TestBase;
import jxl.format.Border;
import jxl.format.UnderlineStyle;
import jxl.write.Colour;
import jxl.write.WritableCellFormat;
import jxl.write.WritableFont;
import jxl.write.WriteException;
public class Homepage extends TestBase
{
	WebDriver driver;
	Login_OR login = new Login_OR();
	Steps steps = new Steps();
	GetScreenShot getscreen = new GetScreenShot();
	Screenshot screen = new Screenshot();
	public String strAbsolutepath = new File("").getAbsolutePath();
	HSSFWorkbook workbook;
	HSSFSheet sheet;
	String strDataFileName = this.getClass().getSimpleName();
	Map<String, Object[]> testresultdata; 
	CommonMethods commonmethods = new CommonMethods();
	Homepage_OR home = new Homepage_OR();
	Screenshot screenshot = new Screenshot();
	String passstatus = screenshot.status("PASS");
	String failstatus = screenshot.status("FAIL");
	int cnt = 2;
	public static String MRP = "";
	public static String Description = "";
	public static String DynamicImagesstatus = "";
	public static String TrendingatMDSstatus = "";
	public static String NewArrivalsstatus = "";
	public static String NewMRP = "";
	public static String NewDescription = "";
	public static String Newproductdetails = "";
	public static String Newremovecart = "";
	public static String NewImage2 = "";
	public static String NewImage3 = "";
	public static String NewImage4 = "";
	public static String Color1 = "";
	public static String hex1 = "";
	public static String Firstcolor = "";
	public static String color2 = "";
	public static String hex2 = "";
	public static String Secondcolor = "";
	public static String color3 = "";
	public static String hex3 = "";
	public static String Thirdcolor = "";
	public static String color4 = "";
	public static String hex4 = "";
	public static String Fourthcolor = "";
	public static String color5 = "";
	public static String hex5 = "";
	public static String Fifthcolor = "";
	public static String cqty = "";
	public static String iqty = "";
	public static String dqty = "";
	public static String sizeS = "";
	public static String sizeM = "";
	public static String sizeL = "";
	public static String sizeXL = "";
	public static String sizeXXL = "";
	public static String sizeXXXL = "";
	public static String below510 = "";
	public static String above510 = "";
	public static String tightfit = "";
	public static String comfit = "";
	public static String findursize = "";
	public static String addcart = "";
	public static String updateproduct = "";
	public static String Sizemsg = "";
	/*New Arrivals */
	public static String NewColor1 = "";
	public static String Newhex1 = "";
	public static String NewFirstcolor = "";
	public static String Newcolor2 = "";
	public static String Newhex2 = "";
	public static String Newcolor5 = "";
	public static String Newhex5 = "";
	public static String NewFifthcolor = "";
	public static String Newcqty = "";
	public static String Newiqty = "";
	public static String Newdqty = "";
	public static String NewsizeS = "";
	public static String NewsizeM = "";
	public static String NewsizeL = "";
	public static String NewsizeXL = "";
	public static String NewsizeXXL = "";
	public static String NewsizeXXXL = "";
	public static String Newbelow510 = "";
	public static String Newabove510 = "";
	public static String Newtightfit = "";
	public static String Newcomfit = "";
	public static String Newfindursize = "";
	public static String Newaddcart = "";
	public static String Newupdateproduct = "";
	public static String NewSizemsg = "";
	public static String FirstImage = "";
	public static String SecondImage = "";
	public static String ThirdImage = "";
	public static String FourthImage = "";
	public static String FifthImage = "";
	public static String DynamicImagesfail = "";
	public static String buynow1 = "";
	public static String buynow2 = "";
	public static String buynow3 = "";
	public static String buynow4 = "";
	public static String buynow5 = "";
	public static String buynow6 = "";
	public static String buynow7 = "";
	public static String buynow8 = "";
	public static String buynow9 = "";
	public static String buynow10 = "";
	public Homepage()
	{
		Browsers b = new Browsers();
		this.driver = b.getBrowsers("chrome1");		 
	}
	//@Test(priority=0)
	public void DynamicImages() throws Exception
	{
		String name = new Object(){}.getClass().getEnclosingMethod().getName();
		cnt ++;
		try
		{
			test = extent.createTest("Dynamic Images");
			System.out.println("Dynamic Images Test Case Executing...");
			commonmethods.URLNaviagtion(driver);
			Thread.sleep(1000);
			commonmethods.Login(driver);
			test.log(Status.INFO, MarkupHelper.createLabel("User able to Login", ExtentColor.BLUE));
			Thread.sleep(1000);
			commonmethods.URLNaviagtion(driver);
			Thread.sleep(1000);
			steps.clickJSButton(home.Mydreamstore, driver, name);
			String Parent = driver.getWindowHandle();
			test.log(Status.INFO, MarkupHelper.createLabel("User naviagated to Mydreamstore Homepage Successfully", ExtentColor.BLUE));
			steps.clickJSButton(home.radio1, driver, name);
			test.log(Status.INFO, MarkupHelper.createLabel("User clicked on First radio button", ExtentColor.BLUE));
			steps.clickJSButton(home.image1, driver, name);
			test.log(Status.INFO, MarkupHelper.createLabel("User clicked on First Image", ExtentColor.BLUE));
			steps.ChildWindow(driver);
			//	driver.manage().window().maximize();
			test.log(Status.INFO, MarkupHelper.createLabel("User navigated to next tab", ExtentColor.BLUE));
			FirstImage = getscreen.capture(driver, "FirstImage");
			test.addScreenCaptureFromPath(FirstImage);
			driver.close();
			driver.switchTo().window(Parent);
			Thread.sleep(1000);
			steps.clickJSButton(home.radio2, driver, name);
			test.log(Status.INFO, MarkupHelper.createLabel("User clicked on Second radio button", ExtentColor.BLUE));
			steps.clickJSButton(home.image2, driver, name);
			test.log(Status.INFO, MarkupHelper.createLabel("User clicked on Second Image", ExtentColor.BLUE));
			steps.ChildWindow(driver);
			//		driver.manage().window().maximize();
			test.log(Status.INFO, MarkupHelper.createLabel("User navigated to next tab", ExtentColor.BLUE));
			SecondImage = getscreen.capture(driver, "SecondImage");
			test.addScreenCaptureFromPath(SecondImage);
			driver.close();
			driver.switchTo().window(Parent);
			Thread.sleep(1000);
			steps.clickJSButton(home.radio3, driver, name);
			test.log(Status.INFO, MarkupHelper.createLabel("User clicked on Third radio button", ExtentColor.BLUE));
			Thread.sleep(500);
			steps.clickJSButton(home.image3, driver, name);
			test.log(Status.INFO, MarkupHelper.createLabel("User clicked on Third Image", ExtentColor.BLUE));
			steps.ChildWindow(driver);
			//		driver.manage().window().maximize();
			test.log(Status.INFO, MarkupHelper.createLabel("User navigated to next tab", ExtentColor.BLUE));
			ThirdImage = getscreen.capture(driver, "ThirdImage");
			test.addScreenCaptureFromPath(ThirdImage);
			driver.close();
			driver.switchTo().window(Parent);
			Thread.sleep(1000);
			steps.clickJSButton(home.radio4, driver, name);
			test.log(Status.INFO, MarkupHelper.createLabel("User clicked on Fourth radio button", ExtentColor.BLUE));
			steps.clickJSButton(home.image4, driver, name);
			test.log(Status.INFO, MarkupHelper.createLabel("User clicked on Fourth Image", ExtentColor.BLUE));
			steps.ChildWindow(driver);
			//		driver.manage().window().maximize();
			test.log(Status.INFO, MarkupHelper.createLabel("User navigated to next tab", ExtentColor.BLUE));
			FourthImage = getscreen.capture(driver, "FourthImage");
			test.addScreenCaptureFromPath(FourthImage);
			driver.close();
			driver.switchTo().window(Parent);
			Thread.sleep(1000);
			steps.clickJSButton(home.radio5, driver, name);
			test.log(Status.INFO, MarkupHelper.createLabel("User clicked on Fifth radio button", ExtentColor.BLUE));
			steps.clickJSButton(home.image5, driver, name);
			test.log(Status.INFO, MarkupHelper.createLabel("User clicked on Fifth Image", ExtentColor.BLUE));
			steps.ChildWindow(driver);
			//	driver.manage().window().maximize();
			test.log(Status.INFO, MarkupHelper.createLabel("User navigated to next tab", ExtentColor.BLUE));
			FifthImage = getscreen.capture(driver, "FourthImage");
			test.addScreenCaptureFromPath(FifthImage);
			driver.close();
			driver.switchTo().window(Parent);
			Thread.sleep(1000);
			DynamicImagesstatus = passstatus;
			Assert.assertTrue(true);
			System.out.println("Dynamic Images Successful");
			testresultdata.put(Integer.toString(cnt), new Object[] {name, "Dynamic Images", "Dynamic Images Successful",passstatus});
		}
		catch(Exception e)
		{
			System.out.println("Dynamic Images Unsuccessful");
			testresultdata.put(Integer.toString(cnt), new Object[] {name, "Dynamic Images", "Dynamic Images Unsuccessful",failstatus});
			DynamicImagesstatus = failstatus;
			DynamicImagesfail = getscreen.capture(driver, "DynamicImagesfail");
			test.addScreenCaptureFromPath(DynamicImagesfail);
			Assert.assertTrue(false);
		}
	}
	@Test(priority=1)
	public void TrendingatMDS() throws Exception
	{
		String name = new Object(){}.getClass().getEnclosingMethod().getName();
		cnt ++;
		try 
		{
			test = extent.createTest("Trending at Mydreamstore");
			System.out.println("Trending at Mydreamstore Test Case Executing...");
			Thread.sleep(1000);
			commonmethods.Login(driver);
			test.log(Status.INFO, MarkupHelper.createLabel("User able to Login", ExtentColor.BLUE));
			Thread.sleep(1000);
			commonmethods.URLNaviagtion(driver);
			steps.clickJSButton(home.Mydreamstore, driver, name);
			String homepage = getscreen.capture(driver, "homepage");
			test.addScreenCaptureFromPath(homepage);
			Thread.sleep(2000);
			String Parent = driver.getWindowHandle();
			steps.clickJSButton(home.Trend_Image1, driver, name);
			test.log(Status.INFO, MarkupHelper.createLabel("User clicked on Image in Trending at mydreamstore", ExtentColor.BLUE));
			steps.ChildWindow(driver);
			Thread.sleep(4000);
			try
			{
				String filter = driver.findElement(home.Filterpagetext).getText();
				System.out.println("User Navigated to :  "+filter);
			}
			catch(Exception e)
			{
				System.out.println("User navigated to different  page");
				String SecondPageImage = getscreen.capture(driver, "SecondPageImage");
				test.addScreenCaptureFromPath(SecondPageImage);
				steps.clickJSButton(home.Trend_2ndpage_image1, driver, name);
				test.log(Status.INFO, MarkupHelper.createLabel("User clicked on 1st Image", ExtentColor.BLUE));
				driver.close();
				steps.ChildWindow(driver);
			}
			//	driver.manage().window().maximize();
			Thread.sleep(3000);
			try
			{	
				String	 Readmore2 = driver.findElement(home.Trend_Image_Readmore).getText();
				System.out.println(Readmore2);
				steps.clickJSButton(home.Trend_Image_Readmore, driver, name);
				test.log(Status.INFO, MarkupHelper.createLabel("User clicked on Readmore", ExtentColor.BLUE));
				String productdetails = getscreen.capture(driver, "productdetails");
				test.addScreenCaptureFromPath(productdetails);
				Thread.sleep(2000);
				Description = driver.findElement(home.Trend_Image_Description).getText();
				System.out.println("Item Description   "+Description);
				test.log(Status.INFO, MarkupHelper.createLabel("Description about Item:  "+Description, ExtentColor.BLUE));
				steps.clickJSButton(home.Trend_Image_Readless, driver, name);
			}
			catch(Exception e)
			{
				System.out.println("Description is not displayed");
			}
			Thread.sleep(1000);
			MRP = driver.findElement(home.Trend_Image_MRP).getText();
			System.out.println("Item Price   "+MRP);
			test.log(Status.INFO, MarkupHelper.createLabel("MRP of an Item:  "+MRP, ExtentColor.ORANGE));
			try
			{
				String	col1 = driver.findElement(home.Trend_color1).getCssValue("color");
				System.out.println(col1);
				steps.clickJSButton(home.Trend_color1, driver, name);
				Thread.sleep(2000);
				Color1 = driver.findElement(home.Trend_color1).getCssValue("background-color");
				hex1 = Color.fromString(Color1).asHex();
				System.out.println("Image color: "+hex1);
				test.log(Status.INFO, MarkupHelper.createLabel("User changed item color to:  "+hex1, ExtentColor.BLUE));
				Firstcolor = getscreen.capture(driver, "Firstcolor");
				test.addScreenCaptureFromPath(Firstcolor);
			}
			catch(Exception a)
			{
				System.out.println("Color 1 is not exists");
			}
			try
			{
				String	col2 = driver.findElement(home.Trend_color2).getCssValue("color");
				System.out.println(col2);
				steps.clickJSButton(home.Trend_color2, driver, name);
				Thread.sleep(2000);
				color2 = driver.findElement(home.Trend_color2).getCssValue("background-color");
				hex2 = Color.fromString(color2).asHex();
				System.out.println("Image color: "+hex2);
				test.log(Status.INFO, MarkupHelper.createLabel("User changed item color to:  "+hex2, ExtentColor.BLUE));
				Secondcolor = getscreen.capture(driver, "Secondcolor");
				test.addScreenCaptureFromPath(Secondcolor);
			}
			catch(Exception a)
			{
				System.out.println("Color 2 is not exists");
			}
			try
			{
				String	col3 = driver.findElement(home.Trend_color3).getCssValue("color");
				System.out.println(col3);
				steps.clickJSButton(home.Trend_color3, driver, name);
				Thread.sleep(2000);
				color3 = driver.findElement(home.Trend_color3).getCssValue("background-color");
				hex3 = Color.fromString(color3).asHex();
				System.out.println("Image color: "+hex3);
				test.log(Status.INFO, MarkupHelper.createLabel("User changed item color to:  "+hex3, ExtentColor.BLUE));
				Thirdcolor = getscreen.capture(driver, "Thirdcolor");
				test.addScreenCaptureFromPath(Thirdcolor);
			}
			catch(Exception a)
			{
				System.out.println("Color 3 is not exists");
			}
			try
			{
				String	col4 = driver.findElement(home.Trend_color4).getCssValue("color");
				System.out.println(col4); 
				steps.clickJSButton(home.Trend_color4, driver, name);
				Thread.sleep(2000);
				color4 = driver.findElement(home.Trend_color4).getCssValue("background-color");
				hex4 = Color.fromString(color4).asHex();
				System.out.println("Image color: "+hex4);
				test.log(Status.INFO, MarkupHelper.createLabel("User changed item color to:  "+hex4, ExtentColor.BLUE));
				Fourthcolor = getscreen.capture(driver, "Fourthcolor");
				test.addScreenCaptureFromPath(Fourthcolor);
			}
			catch(Exception a)
			{
				System.out.println("Color 4 is not exists");
			}
			try
			{
				String	col5 = driver.findElement(home.Trend_color5).getCssValue("color");
				System.out.println(col5); 
				steps.clickJSButton(home.Trend_color5, driver, name);
				Thread.sleep(2000);
				color5 = driver.findElement(home.Trend_color5).getCssValue("background-color");
				hex5 = Color.fromString(color5).asHex();
				System.out.println("Image color: "+hex5);
				test.log(Status.INFO, MarkupHelper.createLabel("User changed item color to:  "+hex5, ExtentColor.BLUE));
				Fifthcolor = getscreen.capture(driver, "Fifthcolor");
				test.addScreenCaptureFromPath(Fifthcolor);
			}
			catch(Exception a)
			{
				System.out.println("Color 5 is not exists");
			}
			cqty = driver.findElement(home.Qty).getAttribute("value");
			System.out.println("Current Quantity:  "+ cqty);
			test.log(Status.INFO, MarkupHelper.createLabel("Current Quantity of product:  "+cqty, ExtentColor.BLUE));
			Thread.sleep(1000);
			steps.clickJSButton(home.Trend_QtyIncrease, driver, name);
			iqty = driver.findElement(home.Qty).getAttribute("value");
			System.out.println("Quantity after increase:  "+ iqty);
			test.log(Status.INFO, MarkupHelper.createLabel("Current Quantity of product after Increase:  "+iqty, ExtentColor.BLUE));
			steps.clickJSButton(home.Trend_QtyDecrease, driver, name);
			dqty = driver.findElement(home.Qty).getAttribute("value");
			System.out.println("Quantity after decrease:  "+ dqty);
			test.log(Status.INFO, MarkupHelper.createLabel("Current Quantity of product after increase:  "+dqty, ExtentColor.BLUE));
			Thread.sleep(2000);
			//steps.HighLighterMethod(driver, home.Trend_sizeS);
			steps.clickJSButton(home.Trend_Addcart, driver, name);
			Sizemsg = driver.findElement(home.Trend_Sizeerrormsg).getText();
			System.out.println("User clicked on Addcart before Size selection:  "+Sizemsg);
			test.log(Status.INFO, MarkupHelper.createLabel("User clicked on Addcart before Size selection:  "+Sizemsg, ExtentColor.RED));
			steps.clickJSButton(home.Size_S, driver, name);
			sizeS = driver.findElement(home.Size_S).getText();
			System.out.println("Selected Size:  "+sizeS);
			test.log(Status.INFO, MarkupHelper.createLabel("User selected size:  "+sizeS, ExtentColor.BLUE));
			steps.clickJSButton(home.Size_M, driver, name);
			sizeM = driver.findElement(home.Size_M).getText();
			System.out.println("Selected Size:  "+sizeM);
			test.log(Status.INFO, MarkupHelper.createLabel("User selected size:  "+sizeM, ExtentColor.BLUE));
			steps.clickJSButton(home.Size_L, driver, name);
			sizeL = driver.findElement(home.Size_L).getText();
			System.out.println("Selected Size:  "+sizeL);
			test.log(Status.INFO, MarkupHelper.createLabel("User selected size:  "+sizeL, ExtentColor.BLUE));
			steps.clickJSButton(home.Size_XL, driver, name);
			sizeXL = driver.findElement(home.Size_XL).getText();
			System.out.println("Selected Size:  "+sizeXL);
			test.log(Status.INFO, MarkupHelper.createLabel("User selected size:  "+sizeXL, ExtentColor.BLUE));
			steps.clickJSButton(home.Size_XXL, driver, name);
			sizeXXL = driver.findElement(home.Size_XXL).getText();
			System.out.println("Selected Size:  "+sizeXXL);
			test.log(Status.INFO, MarkupHelper.createLabel("User selected size:  "+sizeXXL, ExtentColor.BLUE));
			steps.clickJSButton(home.Size_XXXL, driver, name);
			sizeXXXL = driver.findElement(home.Size_XXXL).getText();
			System.out.println("Selected Size:  "+sizeXXXL);
			test.log(Status.INFO, MarkupHelper.createLabel("User selected size:  "+sizeXXXL, ExtentColor.BLUE));
			steps.clickJSButton(home.Trend_FindUrSize, driver, name);
			test.log(Status.INFO, MarkupHelper.createLabel("User clicked on Find your size", ExtentColor.BLUE));
			Thread.sleep(3000);
			try 
			{
				String Sizeradio = driver.findElement(home.Trend_FindUrSize_510radio).getText();
				System.out.println(Sizeradio);
				steps.clickJSButton(home.Trend_FindUrSize_510radio, driver, name);
				below510 = driver.findElement(home.Trend_FindUrSize_510radio).getText();
				System.out.println("User Selected:  "+below510);
				test.log(Status.INFO, MarkupHelper.createLabel("User selected Height:  "+below510, ExtentColor.BLUE));
				steps.clickJSButton(home.Trend_FindUrSize_above510radio, driver, name);
				above510 = driver.findElement(home.Trend_FindUrSize_above510radio).getText();
				System.out.println("User Selected:  "+above510);
				test.log(Status.INFO, MarkupHelper.createLabel("User selected Height:  "+above510, ExtentColor.BLUE));
				steps.clickJSButton(home.Trend_FindUrSize_Tightradio, driver, name);
				tightfit = driver.findElement(home.Trend_FindUrSize_Tightradio).getText();
				System.out.println("User Selected:  "+tightfit);
				test.log(Status.INFO, MarkupHelper.createLabel("User selected Fit:  "+tightfit, ExtentColor.BLUE));
				steps.clickJSButton(home.Trend_FindUrSize_Comfortradio, driver, name);
				comfit = driver.findElement(home.Trend_FindUrSize_Comfortradio).getText();
				System.out.println("User Selected:  "+comfit);
				test.log(Status.INFO, MarkupHelper.createLabel("User selected Fit:  "+comfit, ExtentColor.BLUE));
				steps.selectDropdown(home.Trend_FindUrSize_sizeDD, driver, strDataFileName, "size", name);
				test.log(Status.INFO, MarkupHelper.createLabel("User selected shirt size", ExtentColor.BLUE));
				steps.clickJSButton(home.Trend_FindUrSize_submit, driver, name);
				test.log(Status.INFO, MarkupHelper.createLabel("User clicked on Submit", ExtentColor.BLUE));
				findursize = getscreen.capture(driver, "findursize");
				test.addScreenCaptureFromPath(findursize);
				Thread.sleep(1000);
				steps.clickJSButton(home.Trend_FindUrSize_Okbutton, driver, name);
				test.log(Status.INFO, MarkupHelper.createLabel("system displayed size for given metrics", ExtentColor.BLUE));
				steps.clickJSButton(home.Trend_FindUrSize_closebutton, driver, name);
				test.log(Status.INFO, MarkupHelper.createLabel("User navigate to Product details page", ExtentColor.BLUE));
			}
			catch(Exception e)
			{
				Thread.sleep(1000);
				steps.clickJSButton(home.Trend_FindUrSize_closebutton, driver, name);
				test.log(Status.INFO, MarkupHelper.createLabel("User navigate to Product details page", ExtentColor.BLUE));
			}
			Thread.sleep(2000);
			steps.clickJSButton(home.Size_L, driver, name);
			test.log(Status.INFO, MarkupHelper.createLabel("User selected size", ExtentColor.BLUE));
			steps.clickJSButton(home.Trend_Addcart, driver, name);
			test.log(Status.INFO, MarkupHelper.createLabel("User added product to Cart", ExtentColor.BLUE));
			addcart = getscreen.capture(driver, "addcart");
			test.addScreenCaptureFromPath(addcart);
			try
			{
				String edit = driver.findElement(home.Trend_Addcart_edit).getText();
				System.out.println(edit);
				steps.clickJSButton(home.Trend_Addcart_edit, driver, name);
				test.log(Status.INFO, MarkupHelper.createLabel("User clicked on Edit", ExtentColor.BLUE));
				steps.selectDropdown(home.Trend_Addcart_selectsize, driver, strDataFileName, "SelectSize", name);
				steps.selectDropdown(home.Trend_Addcart_selectqty, driver, strDataFileName, "Quantity", name);
				test.log(Status.INFO, MarkupHelper.createLabel("User selected Size and Quantity", ExtentColor.BLUE));
				steps.clickJSButton(home.Trend_Addcart_cancel, driver, name);
				steps.clickJSButton(home.Trend_Addcart_edit, driver, name);
				steps.selectDropdown(home.Trend_Addcart_selectsize, driver, strDataFileName, "SelectSize", name);
				steps.selectDropdown(home.Trend_Addcart_selectqty, driver, strDataFileName, "Quantity", name);
				steps.clickJSButton(home.Trend_Addcart_save, driver, name);
				updateproduct = getscreen.capture(driver, "updateproduct");
				test.addScreenCaptureFromPath(updateproduct);
				test.log(Status.INFO, MarkupHelper.createLabel("User updated product size and quantity", ExtentColor.BLUE));
			}
			catch(Exception e)
			{
				System.out.println("Edit feature is not available");
			}
			Thread.sleep(2000);
			steps.clickJSButton(home.Trend_Addcart_remove, driver, name);
			String removecart = getscreen.capture(driver, "removecart");
			test.addScreenCaptureFromPath(removecart);
			test.log(Status.INFO, MarkupHelper.createLabel("User removed product from Cart", ExtentColor.BLUE));
			Thread.sleep(2000);
			steps.clickJSButton(home.Size_L, driver, name);
			test.log(Status.INFO, MarkupHelper.createLabel("User selected size", ExtentColor.BLUE));
			steps.clickJSButton(home.BuyNow, driver, name);
			test.log(Status.INFO, MarkupHelper.createLabel("User clicked on BuyNow, navigated to checkout page", ExtentColor.BLUE));
			buynow1 = getscreen.capture(driver, "buynow1");
			test.addScreenCaptureFromPath(buynow1);
			try
			{
				String cash = driver.findElement(home.CashonDeliveryTex).getText();
				System.out.println(cash);
				steps.waitForElement(home.CashonDeliveryradio, driver);
				steps.clickJSButton(home.CashonDeliveryradio, driver, name);
				buynow2 = driver.findElement(home.PopUpSaveMsg).getText();
				System.out.println("Displayed message:   "+buynow2);
				test.log(Status.INFO, MarkupHelper.createLabel("Displayed message:   "+buynow2, ExtentColor.BLUE));
				buynow4 = getscreen.capture(driver, "buynow4");
				test.addScreenCaptureFromPath(buynow4);
				steps.clickJSButton(home.Cashondelivery, driver, name);
				test.log(Status.INFO, MarkupHelper.createLabel("User clicked on Cash on Delivery link", ExtentColor.BLUE));
			}
			catch(Exception e)
			{
				System.out.println("Cash on Delivery option not available");
			}
			Thread.sleep(3000);
			steps.MoveElement(home.CartIcon, driver, name);
			steps.clickJSButton(home.CartIcon, driver, name);
			Thread.sleep(2000);
			steps.clickJSButton(home.Trend_Addcart_remove, driver, name);
			driver.close();
			driver.switchTo().window(Parent);
			TrendingatMDSstatus = passstatus;
			Assert.assertTrue(true);
			System.out.println("Trending at Mydreamstore Successful");
			testresultdata.put(Integer.toString(cnt), new Object[] {name, "Trending at Mydreamstore", "Trending at Mydreamstore Successful",passstatus});	
		}
		catch(Exception e)
		{
			System.out.println("Trending at Mydreamstore Unsuccessful");
			testresultdata.put(Integer.toString(cnt), new Object[] {name, "Trending at Mydreamstore", "Trending at Mydreamstore Unsuccessful",failstatus});
			TrendingatMDSstatus = failstatus;
			String TrendingatMDSstatusfail = getscreen.capture(driver, "TrendingatMDSstatusfail");
			test.addScreenCaptureFromPath(TrendingatMDSstatusfail);
			Assert.assertTrue(false);
		}
	}
	@Test(priority=2)
	public void NewArrivals() throws Exception
	{
		String name = new Object(){}.getClass().getEnclosingMethod().getName();
		cnt ++;
		try
		{
			test = extent.createTest("New Arrivals");
			System.out.println("New Arrivals Test Case Executing...");
			Thread.sleep(1000);
			commonmethods.URLNaviagtion(driver);
			Thread.sleep(1000);
/*
			commonmethods.Login(driver);
			test.log(Status.INFO, MarkupHelper.createLabel("User able to Login", ExtentColor.BLUE));
			Thread.sleep(1000);
*/
			steps.clickJSButton(home.Mydreamstore, driver, name);
			String Parent = driver.getWindowHandle();
			test.log(Status.INFO, MarkupHelper.createLabel("User naviagated to Mydreamstore Homepage Successfully", ExtentColor.BLUE));
			steps.clickJSButton(home.NewArr_Image1, driver, name);
			test.log(Status.INFO, MarkupHelper.createLabel("User clicked on 1st Image in New Arrivals", ExtentColor.BLUE));
			steps.ChildWindow(driver);
			//		driver.manage().window().maximize();
			try
			{
				String Filter2 = driver.findElement(home.Filterpagetext).getText();
				System.out.println("User navigated to Filter Page:  "+Filter2);
				//steps.clickJSButton(home2.ExploreCat_TShirts_Image1, driver, name);
			}
			catch(Exception e)
			{
				System.out.println("User navigated to different page");
				String NewArr2ndpage = getscreen.capture(driver, "NewArr2ndpage");
				test.addScreenCaptureFromPath(NewArr2ndpage);
				steps.clickJSButton(home.NewArr_2ndpage_image1, driver, name);
				test.log(Status.INFO, MarkupHelper.createLabel("User clicked on 1st Image", ExtentColor.BLUE));
				driver.close();
				steps.ChildWindow(driver);
			}
			Thread.sleep(1000);
			try
			{
				String Readmore = driver.findElement(home.Trend_Image_Readmore).getText();
				System.out.println(Readmore);
				steps.clickJSButton(home.Trend_Image_Readmore, driver, name);
				test.log(Status.INFO, MarkupHelper.createLabel("User clicked on Readmore", ExtentColor.BLUE));
				Newproductdetails = getscreen.capture(driver, "Newproductdetails");
				test.addScreenCaptureFromPath(Newproductdetails);
				NewDescription = driver.findElement(home.Trend_Image_Description).getText();
				System.out.println("Item Description   "+NewDescription);
				test.log(Status.INFO, MarkupHelper.createLabel("Description about Item:  "+NewDescription, ExtentColor.BLUE));
				steps.clickJSButton(home.Trend_Image_Readless, driver, name);
			}
			catch(Exception e)
			{
				System.out.println("Description is not displayed");
			}
			Thread.sleep(1000);
			NewMRP = driver.findElement(home.Trend_Image_MRP).getText();
			System.out.println("Item Price   "+NewMRP);
			test.log(Status.INFO, MarkupHelper.createLabel("MRP of an Item:  "+NewMRP, ExtentColor.ORANGE));
			try
			{
				String	newcol1 = driver.findElement(home.Trend_color1).getCssValue("color");
				System.out.println(newcol1);
				steps.clickJSButton(home.Trend_color1, driver, name);
				Thread.sleep(2000);
				NewColor1 = driver.findElement(home.Trend_color1).getCssValue("background-color");
				Newhex1 = Color.fromString(NewColor1).asHex();
				System.out.println("Image color: "+Newhex1);
				test.log(Status.INFO, MarkupHelper.createLabel("User changed item color to:  "+Newhex1, ExtentColor.BLUE));
				NewFirstcolor = getscreen.capture(driver, "NewFirstcolor");
				test.addScreenCaptureFromPath(NewFirstcolor);
			}
			catch(Exception f)
			{
				System.out.println("Color 1 is not exists");
			}
			try
			{
				String	newcol2 = driver.findElement(home.Trend_color5).getCssValue("color");
				System.out.println(newcol2);
				steps.clickJSButton(home.Trend_color5, driver, name);
				Thread.sleep(2000);
				Newcolor5 = driver.findElement(home.Trend_color5).getCssValue("background-color");
				Newhex5 = Color.fromString(Newcolor5).asHex();
				System.out.println("Image color: "+Newhex5);
				test.log(Status.INFO, MarkupHelper.createLabel("User changed item color to:  "+Newhex5, ExtentColor.BLUE));
				NewFifthcolor = getscreen.capture(driver, "NewFifthcolor");
				test.addScreenCaptureFromPath(NewFifthcolor);
			}
			catch(Exception f)
			{
				System.out.println("color 2 is not exists");
			}
			Newcqty = driver.findElement(home.Qty).getAttribute("value");
			System.out.println("Current Quantity:  "+ Newcqty);
			test.log(Status.INFO, MarkupHelper.createLabel("Current Quantity of product:  "+Newcqty, ExtentColor.BLUE));
			Thread.sleep(1000);
			steps.clickJSButton(home.Trend_QtyIncrease, driver, name);
			Newiqty = driver.findElement(home.Qty).getAttribute("value");
			System.out.println("Quantity after increase:  "+ Newiqty);
			test.log(Status.INFO, MarkupHelper.createLabel("Current Quantity of product after Increase:  "+Newiqty, ExtentColor.BLUE));
			steps.clickJSButton(home.Trend_QtyDecrease, driver, name);
			Newdqty = driver.findElement(home.Qty).getAttribute("value");
			System.out.println("Quantity after decrease:  "+ Newdqty);
			test.log(Status.INFO, MarkupHelper.createLabel("Current Quantity of product after increase:  "+Newdqty, ExtentColor.BLUE));
			Thread.sleep(2000);
			steps.clickJSButton(home.Trend_Addcart, driver, name);
			NewSizemsg = driver.findElement(home.Trend_Sizeerrormsg).getText();
			System.out.println("User clicked on Addcart before Size selection:  "+NewSizemsg);
			test.log(Status.INFO, MarkupHelper.createLabel("User clicked on Addcart before Size selection:  "+NewSizemsg, ExtentColor.RED));
			steps.clickJSButton(home.Size_S, driver, name);
			NewsizeS = driver.findElement(home.Size_S).getText();
			System.out.println("Selected Size:  "+NewsizeS);
			test.log(Status.INFO, MarkupHelper.createLabel("User selected size:  "+NewsizeS, ExtentColor.BLUE));
			steps.clickJSButton(home.Size_M, driver, name);
			NewsizeM = driver.findElement(home.Size_M).getText();
			System.out.println("Selected Size:  "+NewsizeM);
			test.log(Status.INFO, MarkupHelper.createLabel("User selected size:  "+NewsizeM, ExtentColor.BLUE));
			steps.clickJSButton(home.Size_L, driver, name);
			NewsizeL = driver.findElement(home.Size_L).getText();
			System.out.println("Selected Size:  "+NewsizeL);
			test.log(Status.INFO, MarkupHelper.createLabel("User selected size:  "+NewsizeL, ExtentColor.BLUE));
			steps.clickJSButton(home.Size_XL, driver, name);
			NewsizeXL = driver.findElement(home.Size_XL).getText();
			System.out.println("Selected Size:  "+NewsizeXL);
			test.log(Status.INFO, MarkupHelper.createLabel("User selected size:  "+NewsizeXL, ExtentColor.BLUE));
			steps.clickJSButton(home.Size_XXL, driver, name);
			NewsizeXXL = driver.findElement(home.Size_XXL).getText();
			System.out.println("Selected Size:  "+NewsizeXXL);
			test.log(Status.INFO, MarkupHelper.createLabel("User selected size:  "+NewsizeXXL, ExtentColor.BLUE));
			steps.clickJSButton(home.Size_XXXL, driver, name);
			NewsizeXXXL = driver.findElement(home.Size_XXXL).getText();
			System.out.println("Selected Size:  "+NewsizeXXXL);
			test.log(Status.INFO, MarkupHelper.createLabel("User selected size:  "+NewsizeXXXL, ExtentColor.BLUE));
			steps.clickJSButton(home.Trend_FindUrSize, driver, name);
			test.log(Status.INFO, MarkupHelper.createLabel("User clicked on Find your size", ExtentColor.BLUE));
			Thread.sleep(1000);
			try {
				String SizeDD = driver.findElement(home.Trend_FindUrSize_sizeDD).getText();
				System.out.println(SizeDD);
				steps.selectDropdown(home.Trend_FindUrSize_sizeDD, driver, strDataFileName, "size", name);
				test.log(Status.INFO, MarkupHelper.createLabel("User selected shirt size", ExtentColor.BLUE));
				steps.clickJSButton(home.Trend_FindUrSize_submit, driver, name);
				test.log(Status.INFO, MarkupHelper.createLabel("User clicked on Submit", ExtentColor.BLUE));
				Newfindursize = getscreen.capture(driver, "Newfindursize");
				test.addScreenCaptureFromPath(Newfindursize);
				Thread.sleep(1000);
				steps.clickJSButton(home.Trend_FindUrSize_Okbutton, driver, name);
				test.log(Status.INFO, MarkupHelper.createLabel("system displayed size for given metrics", ExtentColor.BLUE));
				steps.clickJSButton(home.Trend_FindUrSize_closebutton, driver, name);
				test.log(Status.INFO, MarkupHelper.createLabel("User navigate to Product details page", ExtentColor.BLUE));
			}
			catch(Exception e)
			{
				steps.clickJSButton(home.Trend_FindUrSize_closebutton, driver, name);
				test.log(Status.INFO, MarkupHelper.createLabel("User navigate to Product details page", ExtentColor.BLUE));
			}
			steps.clickJSButton(home.Size_L, driver, name);
			test.log(Status.INFO, MarkupHelper.createLabel("User selected size", ExtentColor.BLUE));
			steps.clickJSButton(home.Trend_Addcart, driver, name);
			test.log(Status.INFO, MarkupHelper.createLabel("User added product to Cart", ExtentColor.BLUE));
			Newaddcart = getscreen.capture(driver, "Newaddcart");
			test.addScreenCaptureFromPath(Newaddcart);
			Thread.sleep(1000);
			try
			{
				String Newedit = driver.findElement(home.Trend_Addcart_edit).getText();
				System.out.println(Newedit);
				steps.clickJSButton(home.Trend_Addcart_edit, driver, name);
				test.log(Status.INFO, MarkupHelper.createLabel("User clicked on Edit", ExtentColor.BLUE));
				steps.selectDropdown(home.Trend_Addcart_selectsize, driver, strDataFileName, "SelectSize", name);
				steps.selectDropdown(home.Trend_Addcart_selectqty, driver, strDataFileName, "Quantity", name);
				test.log(Status.INFO, MarkupHelper.createLabel("User selected Size and Quantity", ExtentColor.BLUE));
				steps.clickJSButton(home.Trend_Addcart_cancel, driver, name);
				steps.clickJSButton(home.Trend_Addcart_edit, driver, name);
				steps.selectDropdown(home.Trend_Addcart_selectsize, driver, strDataFileName, "SelectSize", name);
				steps.selectDropdown(home.Trend_Addcart_selectqty, driver, strDataFileName, "Quantity", name);
				steps.clickJSButton(home.Trend_Addcart_save, driver, name);
				Newupdateproduct = getscreen.capture(driver, "Newupdateproduct");
				test.addScreenCaptureFromPath(Newupdateproduct);
				test.log(Status.INFO, MarkupHelper.createLabel("User updated product size and quantity", ExtentColor.BLUE));
			}
			catch(Exception e)
			{
				System.out.println("Product doesn't have Edit feature");
			}
			steps.waitForElement(home.Trend_Addcart_remove, driver);
			steps.clickJSButton(home.Trend_Addcart_remove, driver, name);
			Newremovecart = getscreen.capture(driver, "Newremovecart");
			test.addScreenCaptureFromPath(Newremovecart);
			test.log(Status.INFO, MarkupHelper.createLabel("User removed product from Cart", ExtentColor.BLUE));
			steps.clickJSButton(home.Size_L, driver, name);
			test.log(Status.INFO, MarkupHelper.createLabel("User selected size", ExtentColor.BLUE));
			steps.clickJSButton(home.BuyNow, driver, name);
			test.log(Status.INFO, MarkupHelper.createLabel("User clicked on BuyNow, navigated to checkout page", ExtentColor.BLUE));
			buynow8 = getscreen.capture(driver, "buynow8");
			test.addScreenCaptureFromPath(buynow8);
			try
			{
				String cash = driver.findElement(home.CashonDeliveryTex).getText();
				System.out.println(cash);
				steps.waitForElement(home.CashonDeliveryradio, driver);
				steps.clickJSButton(home.CashonDeliveryradio, driver, name);
				String nbuynow2 = driver.findElement(home.PopUpSaveMsg).getText();
				System.out.println("Displayed message:   "+nbuynow2);
				test.log(Status.INFO, MarkupHelper.createLabel("Displayed message:   "+nbuynow2, ExtentColor.BLUE));
				String nbuynow4 = getscreen.capture(driver, "nbuynow4");
				test.addScreenCaptureFromPath(nbuynow4);
				steps.clickJSButton(home.Cashondelivery, driver, name);
				test.log(Status.INFO, MarkupHelper.createLabel("User clicked on Cash on Delivery link", ExtentColor.BLUE));
			}
			catch(Exception e)
			{
				System.out.println("Cash on Delivery option not available");
			}
			Thread.sleep(3000);
			steps.MoveElement(home.CartIcon, driver, name);
			steps.clickJSButton(home.CartIcon, driver, name);
			steps.clickJSButton(home.Trend_Addcart_remove, driver, name);
			test.log(Status.INFO, MarkupHelper.createLabel("User Completed transactions of 1st Image in New Arrivals", ExtentColor.PURPLE));
			driver.close();
			driver.switchTo().window(Parent);
			Thread.sleep(1000);
			steps.clickJSButton(home.NewArr_Image2, driver, name);
			test.log(Status.INFO, MarkupHelper.createLabel("User clicked on 2nd Image in New Arrivals", ExtentColor.BLUE));
			steps.ChildWindow(driver);
			//	driver.manage().window().maximize();
			Thread.sleep(1000);
			NewImage2 = getscreen.capture(driver, "NewImage2");
			test.addScreenCaptureFromPath(NewImage2);
			test.log(Status.INFO, MarkupHelper.createLabel("User Completed Opened of 2nd Image in New Arrivals", ExtentColor.PURPLE));
			driver.close();
			driver.switchTo().window(Parent);
			Thread.sleep(1000);
			steps.clickJSButton(home.NewArr_Image3, driver, name);
			test.log(Status.INFO, MarkupHelper.createLabel("User clicked on 3rd Image in New Arrivals", ExtentColor.BLUE));
			steps.ChildWindow(driver);
			//		driver.manage().window().maximize();
			Thread.sleep(1000);
			NewImage3 = getscreen.capture(driver, "NewImage3");
			test.addScreenCaptureFromPath(NewImage3);
			test.log(Status.INFO, MarkupHelper.createLabel("User Completed Opened of 3rd Image in New Arrivals", ExtentColor.PURPLE));
			driver.close();
			driver.switchTo().window(Parent);
			Thread.sleep(1000);
			steps.clickJSButton(home.NewArr_Image4, driver, name);
			test.log(Status.INFO, MarkupHelper.createLabel("User clicked on 4th Image in New Arrivals", ExtentColor.BLUE));
			steps.ChildWindow(driver);
			//	driver.manage().window().maximize();
			Thread.sleep(1000);
			NewImage4 = getscreen.capture(driver, "NewImage4");
			test.addScreenCaptureFromPath(NewImage4);
			test.log(Status.INFO, MarkupHelper.createLabel("User Completed Opened of 4th Image in New Arrivals", ExtentColor.PURPLE));
			driver.close();
			driver.switchTo().window(Parent);
		//	commonmethods.Logout(driver);
			NewArrivalsstatus = passstatus;
			Assert.assertTrue(true);
			System.out.println("New Arrivals Successful");
			testresultdata.put(Integer.toString(cnt), new Object[] {name, "New Arrivals", "New Arrivals Successful",passstatus});	
		}
		catch(Exception e)
		{
			System.out.println("New Arrivals Unsuccessful");
			testresultdata.put(Integer.toString(cnt), new Object[] {name, "New Arrivals", "New Arrivals Unsuccessful",failstatus});
			NewArrivalsstatus = failstatus;
			String TrendingatMDSstatusfail = getscreen.capture(driver, "TrendingatMDSstatusfail");
			test.addScreenCaptureFromPath(TrendingatMDSstatusfail);
			Assert.assertTrue(false);
		}
	}
	@BeforeClass
	public void before()
	{
		workbook = new HSSFWorkbook();
		sheet = workbook.createSheet(strDataFileName);
		testresultdata = new LinkedHashMap<String, Object[]>();
		testresultdata.put("1", new Object[] { "Test Step Name", "Expected Result","Actual Result" ,"Status"});
	}
	@AfterClass
	public void after() throws Exception
	{
		Set<String> keyset = testresultdata.keySet();
		int rownum = 0;
		for (String key : keyset) {
			Row row = sheet.createRow(rownum++);
			Object [] objArr = testresultdata.get(key);
			int cellnum = 0;
			for (Object obj : objArr) {
				Cell cell = row.createCell(cellnum++);
				if(obj instanceof Date) 
					cell.setCellValue((Date)obj);
				else if(obj instanceof Boolean)
					cell.setCellValue((Boolean)obj);
				else if(obj instanceof String)
					cell.setCellValue((String)obj);
				else if(obj instanceof Double)
					cell.setCellValue((Double)obj);
			}
		}
		try {
			FileOutputStream out = new FileOutputStream(new File("TestResults/"+strDataFileName +".xls"));
			workbook.write(out);
			out.close();
			System.out.println("Excel written successfully..");
			TestResult();
			driver.quit();
		}
		catch (FileNotFoundException e) 
		{
			e.printStackTrace();
		} 
		catch (IOException e) 
		{
			e.printStackTrace();
		}
	}
	public void TestResult() throws Exception
	{
		HSSFRow row;
		HSSFCell cell;
		WritableFont cellFont;
		WritableCellFormat cellFormat;
		Border border = null;
		int count;
		FileInputStream inp = new FileInputStream( "TestResults/"+strDataFileName +".xls");
		HSSFWorkbook wb = new HSSFWorkbook(inp);
		HSSFSheet sh = wb.getSheetAt(0);
		count=sh.getLastRowNum();
		for (int i = 1; i <= count; i++) 
		{
			row=sh.getRow(i);
			cell=row.getCell(3);
			String status=row.getCell(3).getStringCellValue();
			if(status.equalsIgnoreCase("Pass"))
			{
				HSSFCellStyle pass= wb.createCellStyle();
				pass.setFillForegroundColor(IndexedColors.GREEN.getIndex());
				pass.setFillPattern(CellStyle.SOLID_FOREGROUND);
				cell.setCellStyle(pass);
			}
			else
			{
				HSSFCellStyle fail= wb.createCellStyle();
				fail.setFillForegroundColor(IndexedColors.RED.getIndex());
				fail.setFillPattern(CellStyle.SOLID_FOREGROUND);
				cell.setCellStyle(fail);
			}
		}
		FileOutputStream out = new FileOutputStream("TestResults/"+strDataFileName+".xls");
		wb.write(out);
		wb.close();
	}
}
