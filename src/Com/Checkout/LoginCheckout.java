package Com.Checkout;
import java.io.File;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.IOException;
import java.util.Date;
import java.util.LinkedHashMap;
import java.util.Map;
import java.util.Set;
import org.apache.poi.hssf.usermodel.HSSFCell;
import org.apache.poi.hssf.usermodel.HSSFCellStyle;
import org.apache.poi.hssf.usermodel.HSSFRow;
import org.apache.poi.hssf.usermodel.HSSFSheet;
import org.apache.poi.hssf.usermodel.HSSFWorkbook;
import org.apache.poi.ss.usermodel.Cell;
import org.apache.poi.ss.usermodel.CellStyle;
import org.apache.poi.ss.usermodel.IndexedColors;
import org.apache.poi.ss.usermodel.Row;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.support.Color;
import org.testng.Assert;
import org.testng.annotations.AfterClass;
import org.testng.annotations.BeforeClass;
import org.testng.annotations.Test;
import com.aventstack.extentreports.Status;
import com.aventstack.extentreports.markuputils.ExtentColor;
import com.aventstack.extentreports.markuputils.MarkupHelper;
import Com.ObjectRepository.Checkout_OR;
import Com.ObjectRepository.Homepage2_OR;
import Com.ObjectRepository.Homepage_OR;
import Com.ObjectRepository.Login_OR;
import Com.Utilities.Browsers;
import Com.Utilities.CommonMethods;
import Com.Utilities.GetScreenShot;
import Com.Utilities.Screenshot;
import Com.Utilities.Steps;
import Com.Utilities.TestBase;
import jxl.format.Border;
import jxl.write.WritableCellFormat;
import jxl.write.WritableFont;
public class LoginCheckout extends TestBase
{
	WebDriver driver;
	Login_OR login = new Login_OR();
	Steps steps = new Steps();
	GetScreenShot getscreen = new GetScreenShot();
	Screenshot screen = new Screenshot();
	public String strAbsolutepath = new File("").getAbsolutePath();
	HSSFWorkbook workbook;
	HSSFSheet sheet;
	String strDataFileName = this.getClass().getSimpleName();
	Map<String, Object[]> testresultdata; 
	CommonMethods commonmethods = new CommonMethods();
	Homepage_OR home = new Homepage_OR();
	Homepage2_OR home2 = new Homepage2_OR();
	Checkout_OR checkout = new Checkout_OR();
	Screenshot screenshot = new Screenshot();
	String passstatus = screenshot.status("PASS");
	String failstatus = screenshot.status("FAIL");
	int cnt = 2;
	public static String LPCcheck1 = "";
	public static String LPCcheck2 = "";
	public static String LPCcheck3 = "";
	public static String LPCcheck4 = "";
	public static String LPCcheck5 = "";
	public static String LPCcheck6 = "";
	public static String LPCcheck7 = "";
	public static String LPCcheck8 = "";
	public static String LPCcheck9 = "";
	public static String LPCcheckColor1 = "";
	public static String LPCcheckhex1 = "";
	public static String LPCcheckFirstcolor1 = "";
	public static String LPCcheckcolor5 = "";
	public static String LPCcheckhex5 = "";
	public static String LPCcheckFifthcolor1 = "";
	public static String LPCcheckcqty1 = "";
	public static String LPCcheckiqty1 = "";
	public static String LPCcheckdqty1 = "";
	public static String LPCcheckSizemsg1 = "";
	public static String LPCchecksizeS1 = "";
	public static String LPCchecksizeM1 = "";
	public static String LPCchecksizeL1 = "";
	public static String LPCchecksizeXL1 = "";
	public static String LPCchecksizeXXL1 = "";
	public static String LPCchecksizeXXXL1 = "";
	public static String LPCcheckbelow5101 = "";
	public static String LPCcheckabove5101 = "";
	public static String LPCchecktightfit1 = "";
	public static String LPCcheckcomfit1 = "";
	public static String LPCcheckfindursize1 = "";
	public static String LPCcheckaddcart1 = "";
	public static String LPCcheckupdateproduct1 = "";
	public static String LPCcheckremovecart1 = "";
	public static String LPCbuynow1 = "";
	public static String LPCbuynow2 = "";
	public static String LPCbuynow3 = "";
	public static String LPCLoginUserstatus = "";
	public static String LPCLoginUserstatusfail = "";
	public static String LCODcheck1 = "";
	public static String LCODcheck2 = "";
	public static String LCODcheck3 = "";
	public static String LCODcheck4 = "";
	public static String LCODcheck5 = "";
	public static String LCODcheck6 = "";
	public static String LCODcheck7 = "";
	public static String LCODcheck8 = "";
	public static String LCODcheck9 = "";
	public static String LCODcheckColor1 = "";
	public static String LCODcheckhex1 = "";
	public static String LCODcheckFirstcolor1 = "";
	public static String LCODcheckColor5 = "";
	public static String LCODcheckhex5 = "";
	public static String LCODcheckFifthcolor1 = "";
	public static String LCODcheckcqty1 = "";
	public static String LCODcheckiqty1 = "";
	public static String LCODcheckdqty1 = "";
	public static String LCODcheckSizemsg1 = "";
	public static String LCODchecksizeS1 = "";
	public static String LCODchecksizeM1 = "";
	public static String LCODchecksizeL1 = "";
	public static String LCODchecksizeXL1 = "";
	public static String LCODchecksizeXXL1 = "";
	public static String LCODchecksizeXXXL1 = "";
	public static String LCODcheckbelow5101 = "";
	public static String LCODcheckabove5101 = "";
	public static String LCODchecktightfit1 = "";
	public static String LCODcheckcomfit1 = "";
	public static String LCODcheckfindursize1 = "";
	public static String LCODcheckaddcart1 = "";
	public static String LCODcheckupdateproduct1 = "";
	public static String LCODcheckremovecart1 = "";
	public static String LCODbuynow1 = "";
	public static String LCODbuynow2 = "";
	public static String LCODbuynow3 = "";
	public static String LCODLoginUserstatus = "";
	public static String LCODLoginUserstatusfail = "";
	public LoginCheckout()
	{
		Browsers b = new Browsers();
		this.driver = b.getBrowsers("chrome1");		 
	}
	@Test(priority=0)
	public void LoginPrepaidCheckoutFlow() throws Exception
	{
		String name = new Object(){}.getClass().getEnclosingMethod().getName();
		cnt ++;
		try
		{
			test = extent.createTest("Login User Prepaid CheckOut");
			System.out.println("Login User Prepaid CheckOut Test Case Executing...");
			commonmethods.Login(driver);
			test.log(Status.INFO, MarkupHelper.createLabel("User able to Login", ExtentColor.BLUE));
			Thread.sleep(1000);
			commonmethods.URLNaviagtion(driver);
			Thread.sleep(1000);
			String Parent = driver.getWindowHandle();
			steps.clickButton(home.Mydreamstore, driver, name);
			Thread.sleep(2000);
			steps.clickButton(home2.Checkout_Image, driver, name);
			steps.ChildWindow(driver);
			driver.manage().window().maximize();
			LPCcheck1 = getscreen.capture(driver, "LPCcheck1");
			test.addScreenCaptureFromPath(LPCcheck1);
			Thread.sleep(1000);
			try
			{
				steps.clickButton(home.Trend_Image_Readmore, driver, name);
				test.log(Status.INFO, MarkupHelper.createLabel("User clicked on Readmore", ExtentColor.BLUE));
				LPCcheck2 = getscreen.capture(driver, "LPCcheck2");
				test.addScreenCaptureFromPath(LPCcheck2);
				LPCcheck3 = driver.findElement(home.Trend_Image_Description).getText();
				System.out.println("Item Description   "+LPCcheck3);
				test.log(Status.INFO, MarkupHelper.createLabel("Description about Item:  "+LPCcheck3, ExtentColor.BLUE));
				steps.clickButton(home.Trend_Image_Readless, driver, name);
			}
			catch(Exception e)
			{
				steps.clickButton(home.Trend_Image_Readmore, driver, name);
				test.log(Status.INFO, MarkupHelper.createLabel("User clicked on Readmore", ExtentColor.BLUE));
				LPCcheck4 = getscreen.capture(driver, "LPCcheck4");
				test.addScreenCaptureFromPath(LPCcheck4);
				LPCcheck5 = driver.findElement(home.Trend_Image_Description1).getText();
				System.out.println("Item Description   "+LPCcheck5);
				test.log(Status.INFO, MarkupHelper.createLabel("Description about Item:  "+LPCcheck5, ExtentColor.BLUE));
				steps.clickButton(home.Trend_Image_Readless, driver, name);
			}
			Thread.sleep(1000);
			LPCcheck6 = driver.findElement(home.Trend_Image_MRP).getText();
			System.out.println("Item Price   "+LPCcheck6);
			test.log(Status.INFO, MarkupHelper.createLabel("MRP of an Item:  "+LPCcheck6, ExtentColor.ORANGE));
			try
			{
				String col1 = driver.findElement(home.Trend_color1).getCssValue("color");
				System.out.println(col1);
				steps.clickButton(home.Trend_color1, driver, name);
				Thread.sleep(2000);
				LPCcheckColor1 = driver.findElement(home.Trend_color1).getCssValue("background-color");
				LPCcheckhex1 = Color.fromString(LPCcheckColor1).asHex();
				System.out.println("Image color: "+LPCcheckhex1);
				test.log(Status.INFO, MarkupHelper.createLabel("User changed item color to:  "+LPCcheckhex1, ExtentColor.BLUE));
				LPCcheckFirstcolor1 = getscreen.capture(driver, "LPCcheckFirstcolor1");
				test.addScreenCaptureFromPath(LPCcheckFirstcolor1);
			}
			catch(Exception e)
			{
				System.out.println("Color 2 is not exists");
			}
			try
			{
				String col2 = driver.findElement(home.Trend_color5).getCssValue("color");
				System.out.println(col2);
				steps.clickButton(home.Trend_color5, driver, name);
				Thread.sleep(2000);
				LPCcheckcolor5 = driver.findElement(home.Trend_color5).getCssValue("background-color");
				LPCcheckhex5 = Color.fromString(LPCcheckcolor5).asHex();
				System.out.println("Image color: "+LPCcheckhex5);
				test.log(Status.INFO, MarkupHelper.createLabel("User changed item color to:  "+LPCcheckhex5, ExtentColor.BLUE));
				LPCcheckFifthcolor1 = getscreen.capture(driver, "LPCcheckFifthcolor1");
				test.addScreenCaptureFromPath(LPCcheckFifthcolor1);
			}
			catch(Exception e)
			{
				System.out.println("Color 1 is not exists");
			}
			LPCcheckcqty1 = driver.findElement(home.Qty).getAttribute("value");
			System.out.println("Current Quantity:  "+ LPCcheckcqty1);
			test.log(Status.INFO, MarkupHelper.createLabel("Current Quantity of product:  "+LPCcheckcqty1, ExtentColor.BLUE));
			Thread.sleep(1000);
			steps.clickButton(home.Trend_QtyIncrease, driver, name);
			LPCcheckiqty1 = driver.findElement(home.Qty).getAttribute("value");
			System.out.println("Quantity after increase:  "+ LPCcheckiqty1);
			test.log(Status.INFO, MarkupHelper.createLabel("Current Quantity of product after Increase:  "+LPCcheckiqty1, ExtentColor.BLUE));
			steps.clickButton(home.Trend_QtyDecrease, driver, name);
			LPCcheckdqty1 = driver.findElement(home.Qty).getAttribute("value");
			System.out.println("Quantity after decrease:  "+ LPCcheckdqty1);
			test.log(Status.INFO, MarkupHelper.createLabel("Current Quantity of product after increase:  "+LPCcheckdqty1, ExtentColor.BLUE));
			Thread.sleep(2000);
			steps.clickButton(home.Trend_Addcart, driver, name);
			LPCcheckSizemsg1 = driver.findElement(home.Trend_Sizeerrormsg).getText();
			System.out.println("User clicked on Addcart before Size selection:  "+LPCcheckSizemsg1);
			test.log(Status.INFO, MarkupHelper.createLabel("User clicked on Addcart before Size selection:  "+LPCcheckSizemsg1, ExtentColor.RED));
			steps.clickJSButton(home2.Size_S, driver, name);
			LPCchecksizeS1 = driver.findElement(home2.Size_S).getText();
			System.out.println("Selected Size:  "+LPCchecksizeS1);
			test.log(Status.INFO, MarkupHelper.createLabel("User selected size:  "+LPCchecksizeS1, ExtentColor.BLUE));
			steps.clickButton(home2.Size_M, driver, name);
			LPCchecksizeM1 = driver.findElement(home2.Size_M).getText();
			System.out.println("Selected Size:  "+LPCchecksizeM1);
			test.log(Status.INFO, MarkupHelper.createLabel("User selected size:  "+LPCchecksizeM1, ExtentColor.BLUE));
			steps.clickButton(home2.Size_L, driver, name);
			LPCchecksizeL1 = driver.findElement(home2.Size_L).getText();
			System.out.println("Selected Size:  "+LPCchecksizeL1);
			test.log(Status.INFO, MarkupHelper.createLabel("User selected size:  "+LPCchecksizeL1, ExtentColor.BLUE));
			steps.clickButton(home2.Size_XL, driver, name);
			LPCchecksizeXL1 = driver.findElement(home2.Size_XL).getText();
			System.out.println("Selected Size:  "+LPCchecksizeXL1);
			test.log(Status.INFO, MarkupHelper.createLabel("User selected size:  "+LPCchecksizeXL1, ExtentColor.BLUE));
			steps.clickButton(home2.Size_XXL, driver, name);
			LPCchecksizeXXL1 = driver.findElement(home2.Size_XXL).getText();
			System.out.println("Selected Size:  "+LPCchecksizeXXL1);
			test.log(Status.INFO, MarkupHelper.createLabel("User selected size:  "+LPCchecksizeXXL1, ExtentColor.BLUE));
			steps.clickButton(home2.Size_XXXL, driver, name);
			LPCchecksizeXXXL1 = driver.findElement(home2.Size_XXXL).getText();
			System.out.println("Selected Size:  "+LPCchecksizeXXXL1);
			test.log(Status.INFO, MarkupHelper.createLabel("User selected size:  "+LPCchecksizeXXXL1, ExtentColor.BLUE));
			steps.clickButton(home.Trend_FindUrSize, driver, name);
			test.log(Status.INFO, MarkupHelper.createLabel("User clicked on Find your size", ExtentColor.BLUE));
			Thread.sleep(1000);
			steps.clickButton(home.Trend_FindUrSize_closebutton, driver, name);
			test.log(Status.INFO, MarkupHelper.createLabel("User navigate to Product details page", ExtentColor.BLUE));
			Thread.sleep(2000);
			steps.clickButton(home2.Size_L, driver, name);
			test.log(Status.INFO, MarkupHelper.createLabel("User selected size", ExtentColor.BLUE));
			steps.clickButton(home.Trend_Addcart, driver, name);
			test.log(Status.INFO, MarkupHelper.createLabel("User added product to Cart", ExtentColor.BLUE));
			LPCcheckaddcart1 = getscreen.capture(driver, "LPCcheckaddcart1");
			test.addScreenCaptureFromPath(LPCcheckaddcart1);
			try
			{
				String edit1 = driver.findElement(home.Trend_Addcart_edit).getText();
				System.out.println(edit1);
				steps.clickButton(home.Trend_Addcart_edit, driver, name);
				test.log(Status.INFO, MarkupHelper.createLabel("User clicked on Edit", ExtentColor.BLUE));
				steps.selectDropdown(home.Trend_Addcart_selectsize, driver, strDataFileName, "SelectSize", name);
				steps.selectDropdown(home.Trend_Addcart_selectqty, driver, strDataFileName, "Quantity", name);
				test.log(Status.INFO, MarkupHelper.createLabel("User selected Size and Quantity", ExtentColor.BLUE));
				steps.clickButton(home.Trend_Addcart_cancel, driver, name);
				steps.clickButton(home.Trend_Addcart_edit, driver, name);
				steps.selectDropdown(home.Trend_Addcart_selectsize, driver, strDataFileName, "SelectSize", name);
				steps.selectDropdown(home.Trend_Addcart_selectqty, driver, strDataFileName, "Quantity", name);
				steps.clickButton(home.Trend_Addcart_save, driver, name);
				LPCcheckupdateproduct1 = getscreen.capture(driver, "LPCcheckupdateproduct1");
				test.addScreenCaptureFromPath(LPCcheckupdateproduct1);
				test.log(Status.INFO, MarkupHelper.createLabel("User updated product size and quantity", ExtentColor.BLUE));
			}
			catch(Exception e)
			{
				System.out.println("Edit feature is not available for this product");
			}
			steps.clickButton(home.Trend_Addcart_remove, driver, name);
			LPCcheckremovecart1 = getscreen.capture(driver, "LPCcheckremovecart1");
			test.addScreenCaptureFromPath(LPCcheckremovecart1);
			test.log(Status.INFO, MarkupHelper.createLabel("User removed product from Cart", ExtentColor.BLUE));
			//checkout process
			steps.clickButton(home.Size_L, driver, name);
			test.log(Status.INFO, MarkupHelper.createLabel("User selected size", ExtentColor.BLUE));
			steps.clickButton(checkout.BuyNow, driver, name);
			test.log(Status.INFO, MarkupHelper.createLabel("User clicked on BuyNow, navigated to checkout page", ExtentColor.BLUE));
			LPCbuynow1 = getscreen.capture(driver, "LPCbuynow1");
			test.addScreenCaptureFromPath(LPCbuynow1);
			Thread.sleep(2000);
			// Debit card option
			try
			{
				String Debit = driver.findElement(checkout.Debitcard).getText();
				System.out.println("Payment method:  "+Debit);
				steps.Verifytext(checkout.Debitcard, driver, Debit, name);
				steps.clickButton(checkout.Debitcard, driver, name);
				System.out.println("Debit card option is available");
				test.log(Status.INFO, MarkupHelper.createLabel("Payment method:  "+Debit   +    "      Available", ExtentColor.BLUE));
			}
			catch(Exception e)
			{
				System.out.println("Debit card option is not available");
				test.log(Status.INFO, MarkupHelper.createLabel("Debit card option is not available", ExtentColor.BLUE));
			}
			Thread.sleep(2000);
			// Credit card option
			try
			{
				String Credit = driver.findElement(checkout.Creditcard).getText();
				System.out.println("Payment method:  "+Credit);
				steps.Verifytext(checkout.Creditcard, driver, Credit, name);
				steps.clickButton(checkout.Creditcard, driver, name);
				System.out.println("Credit card option is available");
				test.log(Status.INFO, MarkupHelper.createLabel("Payment method:  "+Credit+    "Available", ExtentColor.BLUE));
			}
			catch(Exception e)
			{
				System.out.println("Credit card option is not available");
				test.log(Status.INFO, MarkupHelper.createLabel("Credit card option is not available", ExtentColor.BLUE));
			}
			Thread.sleep(2000);
			// Net Banking option
			try
			{
				String NetBanking = driver.findElement(checkout.Netbanking).getText();
				System.out.println("Payment method:  "+NetBanking);
				steps.Verifytext(checkout.Netbanking, driver, NetBanking, name);
				steps.clickButton(checkout.Netbanking, driver, name);
				System.out.println("Net Banking option is available");
				test.log(Status.INFO, MarkupHelper.createLabel("Payment method:  "+NetBanking+    "Available", ExtentColor.BLUE));
			}
			catch(Exception e)
			{
				System.out.println("Net Banking option is not available");
				test.log(Status.INFO, MarkupHelper.createLabel("Net Banking option is not available", ExtentColor.BLUE));
			}
			Thread.sleep(2000);
			// Paytm option
			try
			{
				String Paytm = driver.findElement(checkout.Paytm).getText();
				System.out.println("Payment method:  "+Paytm);
				steps.Verifytext(checkout.Paytm, driver, Paytm, name);
				steps.clickButton(checkout.Paytm, driver, name);
				System.out.println("Paytm option is available");
				test.log(Status.INFO, MarkupHelper.createLabel("Payment method:  "+Paytm+    "Available", ExtentColor.BLUE));
			}
			catch(Exception e)
			{
				System.out.println("Paytm option is not available");
				test.log(Status.INFO, MarkupHelper.createLabel("Paytm option is not available", ExtentColor.BLUE));
			}
			Thread.sleep(2000);
			// MDS Wallet option
			try
			{
				String MDS = driver.findElement(checkout.MDSWallet).getText();
				System.out.println("Payment method:  "+MDS);
				steps.Verifytext(checkout.MDSWallet, driver, MDS, name);
				steps.clickButton(checkout.MDSWallet, driver, name);
				Thread.sleep(2000);
				steps.waitForElement(checkout.MDSWalletPOPup, driver);
				String MDSpopup = driver.findElement(checkout.MDSWalletPOPup).getText();
				System.out.println("Payment method:  "+MDSpopup);
				Thread.sleep(2000);
				steps.clickJSButton(checkout.MDSWalletPOPupOK, driver, name);
				System.out.println("MDS Wallet option is available");
				test.log(Status.INFO, MarkupHelper.createLabel("Payment method:  "+MDS+    "Available", ExtentColor.BLUE));
			}
			catch(Exception e)
			{
				System.out.println("MDS Wallet option is not available");
				test.log(Status.INFO, MarkupHelper.createLabel("MDS Wallet option is not available", ExtentColor.BLUE));
			}
			Thread.sleep(2000);
			// Debit card option
			try
			{
				String Debit = driver.findElement(checkout.Debitcard).getText();
				System.out.println("Payment method:  "+Debit);
				steps.Verifytext(checkout.Debitcard, driver, Debit, name);
				steps.clickButton(checkout.Debitcard, driver, name);
				System.out.println("Debit card option is available");
				test.log(Status.INFO, MarkupHelper.createLabel("Payment method:  "+Debit   +    "      Available", ExtentColor.BLUE));
			}
			catch(Exception e)
			{
				System.out.println("Debit card option is not available");
				test.log(Status.INFO, MarkupHelper.createLabel("Debit card option is not available", ExtentColor.BLUE));
			}
			Thread.sleep(5000);
			String checkout1 = getscreen.capture(driver, "checkout1");
			test.addScreenCaptureFromPath(checkout1);
			steps.MoveElement(checkout.Placeorder, driver, name);
			steps.WaitUntilElementPresent(checkout.Placeorder, driver);
			//		steps.clickButton(checkout.Placeorder, driver, name);
			Thread.sleep(2000);
			steps.clickButton(checkout.Prepaid_Creditcard, driver, name);
			steps.clickButton(checkout.Prepaid_creditcard_Visaradio, driver, name);
			steps.clickButton(checkout.Prepaid_creditcard_Americanradio, driver, name);
			steps.EnterText(checkout.Prepaid_creditcard_CardNumber, driver, strDataFileName, "CardNumber", name);
			steps.EnterText(checkout.Prepaid_creditcard_NameonCard, driver, strDataFileName, "NameonCard", name);
			steps.EnterText(checkout.Prepaid_creditcard_CVV, driver, strDataFileName, "CVV", name);
			steps.selectDropdown(checkout.Prepaid_creditcard_ExpMonth, driver, strDataFileName, "Month", name);
			steps.selectDropdownIndex(checkout.Prepaid_creditcard_ExpYear, driver, 4, name);
			//	steps.selectDropdown(checkout.Prepaid_creditcard_ExpYear, driver, strDataFileName, "Year", name);
			Thread.sleep(1000);
			String Prepaid1 = getscreen.capture(driver, "Prepaid1");
			test.addScreenCaptureFromPath(Prepaid1);
			Thread.sleep(2000);
			steps.clickButton(checkout.Prepaid_Debitcard, driver, name);
			steps.selectDropdown(checkout.Prepaid_Debitcard_SelectCard, driver, strDataFileName, "DebitcardType", name);
			steps.clickButton(checkout.Prepaid_Debitcard_Paynow, driver, name);
			Thread.sleep(1000);
			steps.EnterText(checkout.Prepaid_Debitcard_CardNumber, driver, strDataFileName, "CardNumber", name);
			steps.EnterText(checkout.Prepaid_Debitcard_NameonCard, driver, strDataFileName, "NameonCard", name);
			steps.EnterText(checkout.Prepaid_Debitcard_CVV, driver, strDataFileName, "CVV", name);
			steps.selectDropdown(checkout.Prepaid_Debitcard_ExpMonth, driver, strDataFileName, "Month", name);
			steps.selectDropdownIndex(checkout.Prepaid_Debitcard_ExpYear, driver, 4, name);
			//	steps.selectDropdown(checkout.Prepaid_Debitcard_ExpYear, driver, strDataFileName, "Year", name);
			Thread.sleep(1000);
			String Prepaid2 = getscreen.capture(driver, "Prepaid2");
			test.addScreenCaptureFromPath(Prepaid2);
			Thread.sleep(2000);
			steps.clickButton(checkout.Prepaid_Netbanking, driver, name);
			steps.clickButton(checkout.Prepaid_Netbanking_HDFCradio, driver, name);
			steps.clickButton(checkout.Prepaid_Netbanking_ICICIradio, driver, name);
			steps.selectDropdown(checkout.Prepaid_Netbanking_SelectbankDD, driver, strDataFileName, "Bank", name);
			//		steps.clickButton(checkout.Prepaid_Netbanking_Paynow, driver, name);
			Thread.sleep(1000);
			String Prepaid3 = getscreen.capture(driver, "Prepaid3");
			test.addScreenCaptureFromPath(Prepaid3);
			try
			{
				String Nettext = driver.findElement(checkout.Prepaid_Netbanking_DispayedText).getText();
				System.out.println("Displaying text:    "+Nettext);
			}
			catch(Exception e)
			{
				System.out.println("Netbanking working");
			}
			/*			
			Thread.sleep(2000);
			driver.navigate().back();
			 */
			Thread.sleep(2000);
			steps.clickButton(checkout.Prepaid_Tez, driver, name);
			steps.EnterText(checkout.Prepaid_Tez_UPIfield, driver, strDataFileName, "UPIid", name);
			steps.selectDropdown(checkout.Prepaid_Tez_bank, driver, strDataFileName, "UPIBank", name);
			String Prepaid4 = getscreen.capture(driver, "Prepaid4");
			test.addScreenCaptureFromPath(Prepaid4);
			Thread.sleep(2000);
			steps.clickButton(checkout.Prepaid_UPI, driver, name);
			steps.EnterText(checkout.Prepaid_UPI_UPIfield, driver, strDataFileName, "UPIwithBank", name);
			String Prepaid5 = getscreen.capture(driver, "Prepaid5");
			test.addScreenCaptureFromPath(Prepaid5);
			Thread.sleep(2000);
			steps.clickButton(checkout.Prepaid_lazypaylink, driver, name);
			String Prepaid6 = getscreen.capture(driver, "Prepaid6");
			test.addScreenCaptureFromPath(Prepaid6);
			Thread.sleep(2000);
			steps.clickButton(checkout.Prepaid_visaCheckoutLink, driver, name);
			String Prepaid7 = getscreen.capture(driver, "Prepaid7");
			test.addScreenCaptureFromPath(Prepaid7);
			Thread.sleep(2000);
			steps.clickButton(checkout.Prepaid_EMI, driver, name);
			String Prepaid8 = getscreen.capture(driver, "Prepaid8");
			test.addScreenCaptureFromPath(Prepaid8);
			try
			{
				String EMItext = driver.findElement(checkout.Prepaid_EMI_text).getText();
				System.out.println("Displayed msg in EMI:    "+EMItext);
			}
			catch(Exception e)
			{
				System.out.println("EMI is not available");
			}
			Thread.sleep(2000);
			steps.clickButton(checkout.Prepaid_Wallets, driver, name);
			steps.selectDropdown(checkout.Prepaid_Wallets_SelectwalletDD, driver, strDataFileName, "Wallet", name);
			//		steps.clickButton(checkout.Prepaid_Wallets_Paynow, driver, name);
			String Prepaid9 = getscreen.capture(driver, "Prepaid9");
			test.addScreenCaptureFromPath(Prepaid9);
			try
			{
				String Wallettext = driver.findElement(checkout.Prepaid_Netbanking_DispayedText).getText();
				System.out.println("Displaying text:    "+Wallettext);
			}
			catch(Exception e)
			{
				System.out.println("Wallet working");
			}
			Thread.sleep(2000);
			steps.clickButton(checkout.Prepaid_PayUMoney, driver, name);
			String Prepaid10 = getscreen.capture(driver, "Prepaid10");
			test.addScreenCaptureFromPath(Prepaid10);
			driver.close();
			driver.switchTo().window(Parent);
			LPCLoginUserstatus = passstatus;
			Assert.assertTrue(true);
			System.out.println("Login User Prepaid CheckOut Successful");
			testresultdata.put(Integer.toString(cnt), new Object[] {name, "Login User Prepaid CheckOut ", "Login User Prepaid CheckOut Successful",passstatus});
		}
		catch(Exception e)
		{
			System.out.println("Login User Prepaid CheckOut Unsuccessful");
			testresultdata.put(Integer.toString(cnt), new Object[] {name, "Login User Prepaid CheckOut", "Login User Prepaid CheckOut Unsuccessful",failstatus});
			LPCLoginUserstatus = failstatus;
			LPCLoginUserstatusfail = getscreen.capture(driver, "LPCLoginUserstatusfail");
			test.addScreenCaptureFromPath(LPCLoginUserstatusfail);
			driver.close();
			Assert.assertTrue(false);		
		}
	}
	@Test(priority=1)
	public void LoginCODCheckoutFlow() throws Exception
	{
		String name = new Object(){}.getClass().getEnclosingMethod().getName();
		cnt ++;
		try
		{
			test = extent.createTest("Login User COD CheckOut");
			System.out.println("Login User COD CheckOut Test Case Executing...");
			/*			commonmethods.Login(driver);
			test.log(Status.INFO, MarkupHelper.createLabel("User able to Login", ExtentColor.BLUE));
			 */
			Thread.sleep(1000);
			commonmethods.URLNaviagtion(driver);
			Thread.sleep(1000);
			steps.clickButton(checkout.Cart, driver, name);
			Thread.sleep(2000);
			try
			{
				String ok = driver.findElement(checkout.MDSWalletPOPupOK).getText();
				System.out.println(ok);
				steps.clickButton(checkout.MDSWalletPOPupOK, driver, name);
				steps.clickButton(checkout.Remove, driver, name);
			}
			catch(Exception e)
			{
				System.out.println("Ok button is not displayed");
				steps.clickButton(checkout.Remove, driver, name);
			}
			/*			String Parent = driver.getWindowHandle();
			steps.clickButton(home.Mydreamstore, driver, name);
			Thread.sleep(2000);
			steps.clickButton(home2.Checkout_Image, driver, name);
			steps.ChildWindow(driver);
			driver.manage().window().maximize();
			 */
			LCODcheck1 = getscreen.capture(driver, "LCODcheck1");
			test.addScreenCaptureFromPath(LCODcheck1);
			Thread.sleep(1000);
/*
			try
			{
				steps.clickButton(home.Trend_Image_Readmore, driver, name);
				test.log(Status.INFO, MarkupHelper.createLabel("User clicked on Readmore", ExtentColor.BLUE));
				LCODcheck2 = getscreen.capture(driver, "LCODcheck2");
				test.addScreenCaptureFromPath(LCODcheck2);
				LCODcheck3 = driver.findElement(home.Trend_Image_Description).getText();
				System.out.println("Item Description   "+LCODcheck3);
				test.log(Status.INFO, MarkupHelper.createLabel("Description about Item:  "+LCODcheck3, ExtentColor.BLUE));
				steps.clickButton(home.Trend_Image_Readless, driver, name);
			}
			catch(Exception e)
			{
				steps.clickButton(home.Trend_Image_Readmore, driver, name);
				test.log(Status.INFO, MarkupHelper.createLabel("User clicked on Readmore", ExtentColor.BLUE));
				LCODcheck4 = getscreen.capture(driver, "LCODcheck4");
				test.addScreenCaptureFromPath(LCODcheck4);
				LCODcheck5 = driver.findElement(home.Trend_Image_Description1).getText();
				System.out.println("Item Description   "+LCODcheck5);
				test.log(Status.INFO, MarkupHelper.createLabel("Description about Item:  "+LCODcheck5, ExtentColor.BLUE));
				steps.clickButton(home.Trend_Image_Readless, driver, name);
			}
			Thread.sleep(1000);
			LCODcheck6 = driver.findElement(home.Trend_Image_MRP).getText();
			System.out.println("Item Price   "+LCODcheck6);
			test.log(Status.INFO, MarkupHelper.createLabel("MRP of an Item:  "+LCODcheck6, ExtentColor.ORANGE));
			try
			{
				String col1 = driver.findElement(home.Trend_color1).getCssValue("color");
				System.out.println(col1);
				steps.clickButton(home.Trend_color1, driver, name);
				Thread.sleep(2000);
				LCODcheckColor1 = driver.findElement(home.Trend_color1).getCssValue("background-color");
				LCODcheckhex1 = Color.fromString(LCODcheckColor1).asHex();
				System.out.println("Image color: "+LCODcheckhex1);
				test.log(Status.INFO, MarkupHelper.createLabel("User changed item color to:  "+LCODcheckhex1, ExtentColor.BLUE));
				LCODcheckFirstcolor1 = getscreen.capture(driver, "LCODcheckFirstcolor1");
				test.addScreenCaptureFromPath(LCODcheckFirstcolor1);
			}
			catch(Exception e)
			{
				System.out.println("Color 2 is not exists");
			}
			try
			{
				String col2 = driver.findElement(home.Trend_color5).getCssValue("color");
				System.out.println(col2);
				steps.clickButton(home.Trend_color5, driver, name);
				Thread.sleep(2000);
				LCODcheckColor5 = driver.findElement(home.Trend_color5).getCssValue("background-color");
				LCODcheckhex5 = Color.fromString(LCODcheckColor5).asHex();
				System.out.println("Image color: "+LCODcheckhex5);
				test.log(Status.INFO, MarkupHelper.createLabel("User changed item color to:  "+LCODcheckhex5, ExtentColor.BLUE));
				LCODcheckFifthcolor1 = getscreen.capture(driver, "LCODcheckFifthcolor1");
				test.addScreenCaptureFromPath(LCODcheckFifthcolor1);
			}
			catch(Exception e)
			{
				System.out.println("Color 1 is not exists");
			}
			LCODcheckcqty1 = driver.findElement(home.Qty).getAttribute("value");
			System.out.println("Current Quantity:  "+ LCODcheckcqty1);
			test.log(Status.INFO, MarkupHelper.createLabel("Current Quantity of product:  "+LCODcheckcqty1, ExtentColor.BLUE));
			Thread.sleep(1000);
			steps.clickButton(home.Trend_QtyIncrease, driver, name);
			LCODcheckiqty1 = driver.findElement(home.Qty).getAttribute("value");
			System.out.println("Quantity after increase:  "+ LCODcheckiqty1);
			test.log(Status.INFO, MarkupHelper.createLabel("Current Quantity of product after Increase:  "+LCODcheckiqty1, ExtentColor.BLUE));
			steps.clickButton(home.Trend_QtyDecrease, driver, name);
			LCODcheckdqty1 = driver.findElement(home.Qty).getAttribute("value");
			System.out.println("Quantity after decrease:  "+ LCODcheckdqty1);
			test.log(Status.INFO, MarkupHelper.createLabel("Current Quantity of product after increase:  "+LCODcheckdqty1, ExtentColor.BLUE));
			Thread.sleep(2000);
			steps.clickButton(home.Trend_Addcart, driver, name);
			LCODcheckSizemsg1 = driver.findElement(home.Trend_Sizeerrormsg).getText();
			System.out.println("User clicked on Addcart before Size selection:  "+LCODcheckSizemsg1);
			test.log(Status.INFO, MarkupHelper.createLabel("User clicked on Addcart before Size selection:  "+LCODcheckSizemsg1, ExtentColor.RED));
			steps.clickJSButton(home2.Size_S, driver, name);
			LCODchecksizeS1 = driver.findElement(home2.Size_S).getText();
			System.out.println("Selected Size:  "+LCODchecksizeS1);
			test.log(Status.INFO, MarkupHelper.createLabel("User selected size:  "+LCODchecksizeS1, ExtentColor.BLUE));
			steps.clickButton(home2.Size_M, driver, name);
			LCODchecksizeM1 = driver.findElement(home2.Size_M).getText();
			System.out.println("Selected Size:  "+LCODchecksizeM1);
			test.log(Status.INFO, MarkupHelper.createLabel("User selected size:  "+LCODchecksizeM1, ExtentColor.BLUE));
			steps.clickButton(home2.Size_L, driver, name);
			LCODchecksizeL1 = driver.findElement(home2.Size_L).getText();
			System.out.println("Selected Size:  "+LCODchecksizeL1);
			test.log(Status.INFO, MarkupHelper.createLabel("User selected size:  "+LCODchecksizeL1, ExtentColor.BLUE));
			steps.clickButton(home2.Size_XL, driver, name);
			LCODchecksizeXL1 = driver.findElement(home2.Size_XL).getText();
			System.out.println("Selected Size:  "+LCODchecksizeXL1);
			test.log(Status.INFO, MarkupHelper.createLabel("User selected size:  "+LCODchecksizeXL1, ExtentColor.BLUE));
			steps.clickButton(home2.Size_XXL, driver, name);
			LCODchecksizeXXL1 = driver.findElement(home2.Size_XXL).getText();
			System.out.println("Selected Size:  "+LCODchecksizeXXL1);
			test.log(Status.INFO, MarkupHelper.createLabel("User selected size:  "+LCODchecksizeXXL1, ExtentColor.BLUE));
			steps.clickButton(home2.Size_XXXL, driver, name);
			LCODchecksizeXXXL1 = driver.findElement(home2.Size_XXXL).getText();
			System.out.println("Selected Size:  "+LCODchecksizeXXXL1);
			test.log(Status.INFO, MarkupHelper.createLabel("User selected size:  "+LCODchecksizeXXXL1, ExtentColor.BLUE));
			steps.clickButton(home.Trend_FindUrSize, driver, name);
			test.log(Status.INFO, MarkupHelper.createLabel("User clicked on Find your size", ExtentColor.BLUE));
			Thread.sleep(1000);
			steps.clickButton(home.Trend_FindUrSize_closebutton, driver, name);
			test.log(Status.INFO, MarkupHelper.createLabel("User navigate to Product details page", ExtentColor.BLUE));
			Thread.sleep(2000);
			steps.clickButton(home2.Size_L, driver, name);
			test.log(Status.INFO, MarkupHelper.createLabel("User selected size", ExtentColor.BLUE));
			steps.clickButton(home.Trend_Addcart, driver, name);
			test.log(Status.INFO, MarkupHelper.createLabel("User added product to Cart", ExtentColor.BLUE));
			LCODcheckaddcart1 = getscreen.capture(driver, "LCODcheckaddcart1");
			test.addScreenCaptureFromPath(LCODcheckaddcart1);
			try
			{
				String edit1 = driver.findElement(home.Trend_Addcart_edit).getText();
				System.out.println(edit1);
				steps.clickButton(home.Trend_Addcart_edit, driver, name);
				test.log(Status.INFO, MarkupHelper.createLabel("User clicked on Edit", ExtentColor.BLUE));
				steps.selectDropdown(home.Trend_Addcart_selectsize, driver, strDataFileName, "SelectSize", name);
				steps.selectDropdown(home.Trend_Addcart_selectqty, driver, strDataFileName, "Quantity", name);
				test.log(Status.INFO, MarkupHelper.createLabel("User selected Size and Quantity", ExtentColor.BLUE));
				steps.clickButton(home.Trend_Addcart_cancel, driver, name);
				steps.clickButton(home.Trend_Addcart_edit, driver, name);
				steps.selectDropdown(home.Trend_Addcart_selectsize, driver, strDataFileName, "SelectSize", name);
				steps.selectDropdown(home.Trend_Addcart_selectqty, driver, strDataFileName, "Quantity", name);
				steps.clickButton(home.Trend_Addcart_save, driver, name);
				LCODcheckupdateproduct1 = getscreen.capture(driver, "LCODcheckupdateproduct1");
				test.addScreenCaptureFromPath(LCODcheckupdateproduct1);
				test.log(Status.INFO, MarkupHelper.createLabel("User updated product size and quantity", ExtentColor.BLUE));
			}
			catch(Exception e)
			{
				System.out.println("Edit feature is not available for this product");
			}
			steps.clickButton(home.Trend_Addcart_remove, driver, name);
			LCODcheckremovecart1 = getscreen.capture(driver, "LCODcheckremovecart1");
			test.addScreenCaptureFromPath(LCODcheckremovecart1);
			test.log(Status.INFO, MarkupHelper.createLabel("User removed product from Cart", ExtentColor.BLUE));
*/
			//checkout process
			steps.clickButton(home.Size_L, driver, name);
			test.log(Status.INFO, MarkupHelper.createLabel("User selected size", ExtentColor.BLUE));
			steps.clickButton(checkout.BuyNow, driver, name);
			test.log(Status.INFO, MarkupHelper.createLabel("User clicked on BuyNow, navigated to checkout page", ExtentColor.BLUE));
			LCODbuynow1 = getscreen.capture(driver, "LCODbuynow1");
			test.addScreenCaptureFromPath(LCODbuynow1);
			Thread.sleep(2000);
			// Cash on delivery 
			try
			{
				String cash = driver.findElement(checkout.CashonDeliveryText).getText();
				System.out.println("Payment method:  "+cash);
				test.log(Status.INFO, MarkupHelper.createLabel("Payment method:  "+cash+    "Available", ExtentColor.BLUE));
				steps.Verifytext(checkout.CashonDeliveryText, driver, cash, name);
				steps.clickButton(checkout.CashonDeliveryradio, driver, name);
				Thread.sleep(3000);
				LCODbuynow2 = driver.findElement(checkout.PopUpSaveMsg).getText();
				System.out.println("Displayed message:   "+LCODbuynow2);
				test.log(Status.INFO, MarkupHelper.createLabel("Displayed message:   "+LCODbuynow2, ExtentColor.BLUE));
				LCODbuynow3 = getscreen.capture(driver, "LCODbuynow3");
				test.addScreenCaptureFromPath(LCODbuynow3);
				steps.clickButton(checkout.Cashondelivery, driver, name);
				test.log(Status.INFO, MarkupHelper.createLabel("User clicked on Cash on Delivery link", ExtentColor.BLUE));
			}
			catch(Exception e)
			{
				System.out.println("Cash on Delivery is not available");
				test.log(Status.INFO, MarkupHelper.createLabel("Cash on Delivery is not available", ExtentColor.BLUE));
			}
			Thread.sleep(2000);
			String LCODcheckout1 = getscreen.capture(driver, "LCODcheckout1");
			test.addScreenCaptureFromPath(LCODcheckout1);
			steps.clickButton(checkout.Placeorder, driver, name);
			Thread.sleep(2000);
			String LCODPrepaid10 = getscreen.capture(driver, "LCODPrepaid10");
			test.addScreenCaptureFromPath(LCODPrepaid10);
			steps.clickButton(checkout.Cart, driver, name);
			steps.clickButton(checkout.Remove, driver, name);
			Thread.sleep(1000);
			commonmethods.Logout(driver);
			LCODLoginUserstatus = passstatus;
			Assert.assertTrue(true);
			System.out.println("Login User COD CheckOut Successful");
			testresultdata.put(Integer.toString(cnt), new Object[] {name, "Login User COD CheckOut", "Login User COD CheckOut Successful",passstatus});
		}
		catch(Exception e)
		{
			System.out.println("Login User COD CheckOut Unsuccessful");
			testresultdata.put(Integer.toString(cnt), new Object[] {name, "Login User COD CheckOut", "Login User COD CheckOut Unsuccessful",failstatus});
			LCODLoginUserstatus = failstatus;
			LCODLoginUserstatusfail = getscreen.capture(driver, "LCODLoginUserstatusfail");
			test.addScreenCaptureFromPath(LCODLoginUserstatusfail);
			driver.close();
			Assert.assertTrue(false);		
		}
	}
	@BeforeClass
	public void before()
	{
		workbook = new HSSFWorkbook();
		sheet = workbook.createSheet(strDataFileName);
		testresultdata = new LinkedHashMap<String, Object[]>();
		testresultdata.put("1", new Object[] { "Test Step Name", "Expected Result","Actual Result" ,"Status"});
	}
	@AfterClass
	public void after() throws Exception
	{
		Set<String> keyset = testresultdata.keySet();
		int rownum = 0;
		for (String key : keyset) {
			Row row = sheet.createRow(rownum++);
			Object [] objArr = testresultdata.get(key);
			int cellnum = 0;
			for (Object obj : objArr) {
				Cell cell = row.createCell(cellnum++);
				if(obj instanceof Date) 
					cell.setCellValue((Date)obj);
				else if(obj instanceof Boolean)
					cell.setCellValue((Boolean)obj);
				else if(obj instanceof String)
					cell.setCellValue((String)obj);
				else if(obj instanceof Double)
					cell.setCellValue((Double)obj);
			}
		}
		try {
			FileOutputStream out = new FileOutputStream(new File("TestResults/"+strDataFileName +".xls"));
			workbook.write(out);
			out.close();
			System.out.println("Excel written successfully..");
			TestResult();
			driver.quit();
		}
		catch (FileNotFoundException e) 
		{
			e.printStackTrace();
		} 
		catch (IOException e) 
		{
			e.printStackTrace();
		}
	}
	public void TestResult() throws Exception
	{
		HSSFRow row;
		HSSFCell cell;
		WritableFont cellFont;
		WritableCellFormat cellFormat;
		Border border = null;
		int count;
		FileInputStream inp = new FileInputStream( "TestResults/"+strDataFileName +".xls");
		HSSFWorkbook wb = new HSSFWorkbook(inp);
		HSSFSheet sh = wb.getSheetAt(0);
		count=sh.getLastRowNum();
		for (int i = 1; i <= count; i++) 
		{
			row=sh.getRow(i);
			cell=row.getCell(3);
			String status=row.getCell(3).getStringCellValue();
			if(status.equalsIgnoreCase("Pass"))
			{
				HSSFCellStyle pass= wb.createCellStyle();
				pass.setFillForegroundColor(IndexedColors.GREEN.getIndex());
				pass.setFillPattern(CellStyle.SOLID_FOREGROUND);
				cell.setCellStyle(pass);
			}
			else
			{
				HSSFCellStyle fail= wb.createCellStyle();
				fail.setFillForegroundColor(IndexedColors.RED.getIndex());
				fail.setFillPattern(CellStyle.SOLID_FOREGROUND);
				cell.setCellStyle(fail);
			}
		}
		FileOutputStream out = new FileOutputStream("TestResults/"+strDataFileName+".xls");
		wb.write(out);
		wb.close();
	}
}
