package Com.Sample;
import java.util.HashMap;
import java.util.Map;
import java.util.concurrent.TimeUnit;
import org.openqa.selenium.By;
import org.openqa.selenium.Dimension;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.chrome.ChromeDriver;
import org.openqa.selenium.chrome.ChromeOptions;
import org.openqa.selenium.firefox.FirefoxDriver;
import org.openqa.selenium.firefox.FirefoxProfile;
import org.openqa.selenium.firefox.internal.ProfilesIni;
import org.openqa.selenium.remote.DesiredCapabilities;
import org.testng.annotations.Test;
public class Mobileview 
{
	static WebDriver driver;
	static DesiredCapabilities  capabilities;
	static String deviceName;
	//@Test
	public  void main() {
		//some Sample Devices. Complete list can be found here: https://code.google.com/p/chromium/codesearch#chromium/src/chrome/test/chromedriver/chrome/mobile_device_list.cc
		//pick any of the device
		//  deviceName = "Google Nexus 6";
		//  deviceName = "Samsung Galaxy S4";
		//  deviceName = "Samsung Galaxy Note 3";
		//  deviceName = "Samsung Galaxy Note II";
		//  deviceName = "Apple iPhone 4";
		// deviceName = "Apple iPhone 5";
		//  deviceName = "Apple iPad 3 / 4";
		deviceName = "Nexus 6";
		System.setProperty("webdriver.chrome.driver", "/usr/lib/chromium-browser/chromedriver");
		ChromeOptions options = new ChromeOptions();
		Map<String, String> mobileEmulation = new HashMap<String, String>();
		mobileEmulation.put("deviceName", deviceName);
		Map<String, Object> chromeOptions = new HashMap<String, Object>();
		chromeOptions.put("mobileEmulation", mobileEmulation);
		// options.addArguments("user-data-dir=C:/Users/user_name/AppData/Local/Google/Chrome/User Data");
		options.addArguments("user-data-dir=/home/giri/.config/chromium/Profile 8");
		options.addArguments("--start-maximized");
		capabilities = DesiredCapabilities.chrome();
		capabilities.setCapability(ChromeOptions.CAPABILITY, chromeOptions);
		driver = new ChromeDriver(capabilities);
		driver.manage().window().maximize();
		driver.navigate().to("http://www.google.com");
	}
	@Test
	public void test2()
	{
		System.setProperty("webdriver.chrome.driver", "/usr/lib/chromium-browser/chromedriver");
		Map<String, String> mobileEmulation = new HashMap<>();
		mobileEmulation.put("deviceName", "Nexus 6");
		ChromeOptions chromeOptions = new ChromeOptions();
		chromeOptions.addArguments("user-data-dir=/home/giri/.config/chromium/Profile 8");
		//chromeOptions.addArguments("--start-maximized");
		chromeOptions.setExperimentalOption("mobileEmulation", mobileEmulation);
		driver = new ChromeDriver(chromeOptions);
		driver.navigate().to("https://mydreamstore.in/");
	}
	//@Test
	public void test3()
	{
		//FirefoxProfile ffprofile = new FirefoxProfile();
		ProfilesIni profile = new ProfilesIni();
		FirefoxProfile myprofile = profile.getProfile("SeleniumFF");
		myprofile.setPreference("general.useragent.override", "iPhone"); //this will change the user agent which will open mobile browser
	//	driver = new FirefoxDriver(myprofile);
		driver.manage().timeouts().implicitlyWait(10, TimeUnit.SECONDS);
		driver.manage().window().setSize(new Dimension(800,1280)); //just to change the window size so that it will look like mobile ;)
		//driver.get("https://mydreamstore.in/");
		driver.get("https://www.facebook.com/");
		//driver.findElement(By.xpath("//img")).click();
		driver.findElement(By.name("email")).sendKeys("username");
		driver.findElement(By.name("pass")).sendKeys("************");
		driver.findElement(By.name("login")).click();
	}
	//@Test
	public void test4()
	{
		System.setProperty("webdriver.gecko.driver","/home/giri/Testing/Automation/Mydreamstore/Drivers/geckodriver");
		//driver = new FirefoxDriver(capabilities);
		/*ProfilesIni profile = new ProfilesIni();
		FirefoxProfile myprofile = profile.getProfile("SeleniumFF");
		driver = new FirefoxDriver(myprofile);
		 */
		driver = new FirefoxDriver();
		driver.manage().timeouts().implicitlyWait(10, TimeUnit.SECONDS);
		//driver.manage().window().maximize();
		driver.get("https://www.facebook.com/");
		driver.findElement(By.id("m_login_email")).sendKeys("username");
		driver.findElement(By.name("pass")).sendKeys("************");
	}
}
