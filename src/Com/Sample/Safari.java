package Com.Sample;
import java.io.File;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.IOException;
import java.net.MalformedURLException;
import java.net.URL;
import java.util.Date;
import java.util.HashMap;
import java.util.LinkedHashMap;
import java.util.Map;
import java.util.Set;
import org.apache.poi.hssf.usermodel.HSSFCell;
import org.apache.poi.hssf.usermodel.HSSFCellStyle;
import org.apache.poi.hssf.usermodel.HSSFRow;
import org.apache.poi.hssf.usermodel.HSSFSheet;
import org.apache.poi.hssf.usermodel.HSSFWorkbook;
import org.apache.poi.ss.usermodel.Cell;
import org.apache.poi.ss.usermodel.CellStyle;
import org.apache.poi.ss.usermodel.IndexedColors;
import org.apache.poi.ss.usermodel.Row;
import org.openqa.selenium.By;
import org.openqa.selenium.Keys;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.chrome.ChromeOptions;
import org.openqa.selenium.interactions.Actions;
import org.openqa.selenium.remote.DesiredCapabilities;
import org.openqa.selenium.remote.RemoteWebDriver;
import org.openqa.selenium.safari.SafariDriver;
import org.openqa.selenium.support.Color;
import org.testng.Assert;
import org.testng.annotations.AfterClass;
import org.testng.annotations.BeforeClass;
import org.testng.annotations.BeforeTest;
import org.testng.annotations.Test;
import com.aventstack.extentreports.Status;
import com.aventstack.extentreports.markuputils.ExtentColor;
import com.aventstack.extentreports.markuputils.MarkupHelper;
import Com.ObjectRepository.Checkout_OR;
import Com.ObjectRepository.Homepage2_OR;
import Com.ObjectRepository.Homepage_OR;
import Com.ObjectRepository.Login_OR;
import Com.ObjectRepository.MobileWorkflow_OR;
import Com.Utilities.CommonMethods;
import Com.Utilities.GetScreenShot;
import Com.Utilities.Screenshot;
import Com.Utilities.Steps;
import Com.Utilities.TestBase;
import io.appium.java_client.AppiumDriver;
import io.appium.java_client.android.AndroidDriver;
import jxl.format.Border;
import jxl.write.WritableCellFormat;
import jxl.write.WritableFont;
public class Safari 
{
	WebDriver driver;
	Login_OR login = new Login_OR();
	Steps steps = new Steps();
	GetScreenShot getscreen = new GetScreenShot();
	Screenshot screen = new Screenshot();
	public String strAbsolutepath = new File("").getAbsolutePath();
	HSSFWorkbook workbook;
	HSSFSheet sheet;
	String strDataFileName = this.getClass().getSimpleName();
	Map<String, Object[]> testresultdata; 
	CommonMethods commonmethods = new CommonMethods();
	Homepage_OR home = new Homepage_OR();
	Homepage2_OR home2 = new Homepage2_OR();
	Checkout_OR checkout = new Checkout_OR();
	MobileWorkflow_OR mob = new MobileWorkflow_OR();
	Screenshot screenshot = new Screenshot();
	String passstatus = screenshot.status("PASS");
	String failstatus = screenshot.status("FAIL");
	int cnt = 2;
	public static String LoginUserstatus = "";
	public static final String USERNAME = "raj964";
	public static final String AUTOMATE_KEY = "qesBpyNzdyQGs3wK7ekp";
	public static final String URL = "https://" + USERNAME + ":" + AUTOMATE_KEY + "@hub-cloud.browserstack.com/wd/hub";
	@Test(priority=0)
	public void MobileCheckout() throws Exception
	{
		try
		{
			String name = new Object(){}.getClass().getEnclosingMethod().getName();
			cnt ++;
			//commonmethods.URLNaviagtion(driver);
			DesiredCapabilities caps = new DesiredCapabilities();
			caps.setCapability("os_version", "6.0");
			caps.setCapability("device", "Google Nexus 6");
			caps.setCapability("real_mobile", "true");
			caps.setCapability("browserstack.local", "false");
			caps.setCapability("browserstack.timezone", "IN");
			//  caps.setCapability("autoGrantPermissions", "true");
			caps.setCapability("browserstack.debug", "true");
			AndroidDriver driver = new AndroidDriver(new URL(URL), caps);
			driver.get("http://www.google.com");
			WebElement element = driver.findElement(By.name("q"));
			element.sendKeys("BrowserStack");
			element.submit();
			driver.get("https://mydreamstore.in/");
			Thread.sleep(10000);
			driver.getContext();
			driver.context("NATIVE_APP");
			driver.findElement(By.xpath("//*[@resource-id='android:id/button1']")).click();
			driver.context("CHROMIUM");
			driver.findElement(By.xpath("//img[contains(@src,'https://mydreamstore.in/static/images/logo_mds.png')]")).click();
			Thread.sleep(5000);
			driver.getContext();
			driver.findElement(mob.Mob_Image1).click();
			steps.ChildWindow(driver);
			Thread.sleep(5000);
			driver.getContext();
			try
			{
				String Filterpage1 = driver.findElement(home.Filterpagetext).getText();
				System.out.println("User navigated to :   "+Filterpage1);
			}
			catch(Exception e)
			{
				driver.findElement(mob.Mob_Image2).click();
				System.out.println("User navigated to different landing page");
				driver.close();
				steps.ChildWindow(driver);
				Thread.sleep(1000);
			}
			steps.clickButton(mob.Mob_ImageSlide, driver, name);
			steps.ScrollDown(driver);
			Thread.sleep(2000);
			try
			{
				steps.clickButton(mob.Mob_Readmore, driver, name);
				String check3 = driver.findElement(mob.Mob_Description).getText();
				System.out.println("Item Description   "+check3);
				steps.ScrollDown(driver);
				Thread.sleep(2000);
				steps.clickButton(mob.Mob_Readless, driver, name);
			}
			catch(Exception e)
			{
				steps.clickButton(mob.Mob_Readmore, driver, name);
				String check5 = driver.findElement(mob.Mob_Description1).getText();
				System.out.println("Item Description   "+check5);
				Thread.sleep(2000);
				steps.clickButton(mob.Mob_Readless, driver, name);
			}
			Thread.sleep(1000);
			String check6 = driver.findElement(mob.Mob_MRP).getText();
			System.out.println("Item Price   "+check6);
			try
			{
				String col1 = driver.findElement(mob.Mob_color1).getCssValue("color");
				System.out.println(col1);
				steps.clickButton(mob.Mob_color1, driver, name);
				Thread.sleep(2000);
				String checkColor1 = driver.findElement(mob.Mob_color1).getCssValue("background-color");
				String checkhex1 = Color.fromString(checkColor1).asHex();
				System.out.println("Image color: "+checkhex1);
			}
			catch(Exception e)
			{
				System.out.println("Color 2 is not exists");
			}
			try
			{
				String col2 = driver.findElement(mob.Mob_color5).getCssValue("color");
				System.out.println(col2);
				steps.clickButton(mob.Mob_color5, driver, name);
				Thread.sleep(2000);
				String checkcolor5 = driver.findElement(mob.Mob_color5).getCssValue("background-color");
				String checkhex5 = Color.fromString(checkcolor5).asHex();
				System.out.println("Image color: "+checkhex5);
			}
			catch(Exception e)
			{
				System.out.println("Color 1 is not exists");
			}
			Thread.sleep(1000);
			steps.clickButton(mob.Mob_Findyoursize, driver, name);
			Thread.sleep(1000);
			steps.clickButton(mob.Mob_Findyoursizeclose, driver, name);
			Thread.sleep(1000);
			steps.clickButton(mob.Mob_Addtocart, driver, name);
			Thread.sleep(1000);
			steps.ScrollDown(driver);
			steps.clickButton(mob.Mob_SizeS, driver, name);
			String checksizeS1 = driver.findElement(mob.Mob_SizeS).getText();
			System.out.println("Selected Size:  "+checksizeS1);
			steps.clickButton(mob.Mob_SizeM, driver, name);
			String 	checksizeM1 = driver.findElement(mob.Mob_SizeM).getText();
			System.out.println("Selected Size:  "+checksizeM1);
			steps.clickButton(mob.Mob_SizeL, driver, name);
			String checksizeL1 = driver.findElement(mob.Mob_SizeL).getText();
			System.out.println("Selected Size:  "+checksizeL1);
			steps.clickButton(mob.Mob_SizeXL, driver, name);
			String checksizeXL1 = driver.findElement(mob.Mob_SizeXL).getText();
			System.out.println("Selected Size:  "+checksizeXL1);
			steps.clickButton(mob.Mob_SizeXXL, driver, name);
			String checksizeXXL1 = driver.findElement(mob.Mob_SizeXXL).getText();
			System.out.println("Selected Size:  "+checksizeXXL1);
			steps.clickButton(mob.Mob_SizeXXXL, driver, name);
			String checksizeXXXL1 = driver.findElement(mob.Mob_SizeXXXL).getText();
			System.out.println("Selected Size:  "+checksizeXXXL1);
			Thread.sleep(1000);
			steps.clickButton(mob.Mob_Addtocartfinal, driver, name);
			Thread.sleep(2000);
			try
			{
				String edit1 = driver.findElement(mob.Mob_CartEdit).getText();
				System.out.println(edit1);
				steps.clickButton(mob.Mob_CartEdit, driver, name);
				Thread.sleep(1000);
				steps.selectDropdown(mob.Mob_CartEditSize, driver, strDataFileName, "SelectSize", name);
				steps.ScrollUp(driver);
				Thread.sleep(1000);
				steps.selectDropdown(mob.Mob_CartEditQty, driver, strDataFileName, "Quantity", name);
				steps.ScrollUp(driver);
				Thread.sleep(1000);
				steps.clickButton(mob.Mob_CartEditCancel, driver, name);
				steps.clickButton(mob.Mob_CartEdit, driver, name);
				steps.selectDropdown(mob.Mob_CartEditSize, driver, strDataFileName, "SelectSize", name);
				steps.ScrollUp(driver);
				Thread.sleep(1000);
				steps.selectDropdown(mob.Mob_CartEditQty, driver, strDataFileName, "Quantity", name);
				steps.ScrollUp(driver);
				Thread.sleep(1000);
				steps.clickButton(mob.Mob_CartEditSave, driver, name);
			}
			catch(Exception e)
			{
				System.out.println("Edit feature is not available for this product");
			}
			Thread.sleep(1000);
			steps.clickButton(mob.Mob_CartRemove, driver, name);
			//checkout process
			steps.ScrollDown(driver);
			steps.clickButton(mob.Mob_SizeM, driver, name);
			String checksizeM2 = driver.findElement(mob.Mob_SizeM).getText();
			System.out.println("Selected Size:  "+checksizeM2);
			steps.clickButton(mob.Mob_BuyNow, driver, name);
			Thread.sleep(2000);
			// Debit card option
			try
			{
				//steps.Scroll(driver, checkout.Debitcard);
				steps.ScrollDown(driver);
				String Debit = driver.findElement(checkout.Debitcard).getText();
				System.out.println("Payment method:  "+Debit);
				steps.Verifytext(checkout.Debitcard, driver, Debit, name);
				steps.ScrollUp(driver);
				steps.clickButton(checkout.Debitcard, driver, name);
				System.out.println("Debit card option is available");
			}
			catch(Exception e)
			{
				System.out.println("Debit card option is not available");
			}
			Thread.sleep(2000);
			// Credit card option
			try
			{
				steps.ScrollDown(driver);
				steps.MoveElement(mob.Mob_Creditcard, driver, name);
				String Credit = driver.findElement(mob.Mob_Creditcard).getText();
				System.out.println("Payment method:  "+Credit);
				steps.Verifytext(mob.Mob_Creditcard, driver, Credit, name);
				steps.ScrollDown(driver);
				steps.clickButton(mob.Mob_Creditcard, driver, name);
				System.out.println("Credit card option is available");
			}
			catch(Exception e)
			{
				System.out.println("Credit card option is not available");
			}
			Thread.sleep(2000);
			// Net Banking option
			try
			{
				String NetBanking = driver.findElement(mob.Mob_Netbanking).getText();
				System.out.println("Payment method:  "+NetBanking);
				steps.Verifytext(mob.Mob_Netbanking, driver, NetBanking, name);
				steps.clickButton(mob.Mob_Netbanking, driver, name);
				System.out.println("Net Banking option is available");
			}
			catch(Exception e)
			{
				System.out.println("Net Banking option is not available");
			}
			Thread.sleep(2000);
			// Paytm option
			try
			{
				String Paytm = driver.findElement(mob.Mob_Paytm).getText();
				System.out.println("Payment method:  "+Paytm);
				steps.Verifytext(mob.Mob_Paytm, driver, Paytm, name);
				steps.clickButton(mob.Mob_Paytm, driver, name);
				System.out.println("Paytm option is available");
			}
			catch(Exception e)
			{
				System.out.println("Paytm option is not available");
			}
			Thread.sleep(2000);
			// MDS Wallet option
			try
			{
				String MDS = driver.findElement(mob.Mob_MDSWallet).getText();
				System.out.println("Payment method:  "+MDS);
				steps.Verifytext(mob.Mob_MDSWallet, driver, MDS, name);
				steps.clickButton(mob.Mob_MDSWallet, driver, name);
				Thread.sleep(3000);
				String MDSpopup = driver.findElement(mob.Mob_MDSWalletPOPup).getText();
				System.out.println("Payment method:  "+MDSpopup);
				Thread.sleep(2000);
				steps.clickJSButton(mob.Mob_MDSWalletPOPupOK, driver, name);
				System.out.println("MDS Wallet option is available");
			}
			catch(Exception e)
			{
				System.out.println("MDS Wallet option is not available");
			}
			Thread.sleep(2000);
			// Cash on delivery 
			try
			{
				steps.ScrollDown(driver);
				steps.MoveElement(mob.Mob_CashonDeliveryradio, driver, name);
				String cash = driver.findElement(mob.Mob_CashonDeliveryText).getText();
				System.out.println("Payment method:  "+cash);
				steps.Verifytext(mob.Mob_CashonDeliveryText, driver, cash, name);
				steps.MoveElement(mob.Mob_CashonDeliveryradio, driver, name);
				Thread.sleep(5000);
				steps.ScrollUp(driver);
				steps.clickJSButton(mob.Mob_CashonDeliveryradio, driver, name);
				String buynow2 = driver.findElement(mob.Mob_PopUpSaveMsg).getText();
				System.out.println("Displayed message:   "+buynow2);
				Thread.sleep(1000);
				steps.clickButton(mob.Mob_Cashondelivery, driver, name);
			}
			catch(Exception e)
			{
				System.out.println("Cash on Delivery is not available");
			}
			Thread.sleep(2000);
			steps.clickButton(mob.Mob_Placeorder, driver, name);
			Thread.sleep(2000);
			steps.clickButton(mob.Mob_CartIcon, driver, name);
			steps.clickButton(mob.Mob_CartRemove, driver, name);
			Thread.sleep(1000);
			commonmethods.MobileLogout(driver);
			Thread.sleep(1000);
			LoginUserstatus = passstatus;
			Assert.assertTrue(true);
			System.out.println("Mobile Login User CheckOut Successful");
			Thread.sleep(10000);
			System.out.println("Done");
		}
		catch(Exception e)
		{
			System.out.println("Mobile Login User CheckOut Unsuccessful");
			LoginUserstatus = failstatus;
			Assert.assertTrue(false);
		}
	}
	//@BeforeClass
	public void before()
	{
		workbook = new HSSFWorkbook();
		sheet = workbook.createSheet(strDataFileName);
		testresultdata = new LinkedHashMap<String, Object[]>();
		testresultdata.put("1", new Object[] { "Test Step Name", "Expected Result","Actual Result" ,"Status"});
	}
	//@AfterClass
	public void after() throws Exception
	{
		Set<String> keyset = testresultdata.keySet();
		int rownum = 0;
		for (String key : keyset) {
			Row row = sheet.createRow(rownum++);
			Object [] objArr = testresultdata.get(key);
			int cellnum = 0;
			for (Object obj : objArr) {
				Cell cell = row.createCell(cellnum++);
				if(obj instanceof Date) 
					cell.setCellValue((Date)obj);
				else if(obj instanceof Boolean)
					cell.setCellValue((Boolean)obj);
				else if(obj instanceof String)
					cell.setCellValue((String)obj);
				else if(obj instanceof Double)
					cell.setCellValue((Double)obj);
			}
		}
		try {
			FileOutputStream out = new FileOutputStream(new File("TestResults/"+strDataFileName +".xls"));
			workbook.write(out);
			out.close();
			System.out.println("Excel written successfully..");
			TestResult();
			driver.quit();
		}
		catch (FileNotFoundException e) 
		{
			e.printStackTrace();
		} 
		catch (IOException e) 
		{
			e.printStackTrace();
		}
	}
	public void TestResult() throws Exception
	{
		HSSFRow row;
		HSSFCell cell;
		WritableFont cellFont;
		WritableCellFormat cellFormat;
		Border border = null;
		int count;
		FileInputStream inp = new FileInputStream( "TestResults/"+strDataFileName +".xls");
		HSSFWorkbook wb = new HSSFWorkbook(inp);
		HSSFSheet sh = wb.getSheetAt(0);
		count=sh.getLastRowNum();
		for (int i = 1; i <= count; i++) 
		{
			row=sh.getRow(i);
			cell=row.getCell(3);
			String status=row.getCell(3).getStringCellValue();
			if(status.equalsIgnoreCase("Pass"))
			{
				HSSFCellStyle pass= wb.createCellStyle();
				pass.setFillForegroundColor(IndexedColors.GREEN.getIndex());
				pass.setFillPattern(CellStyle.SOLID_FOREGROUND);
				cell.setCellStyle(pass);
			}
			else
			{
				HSSFCellStyle fail= wb.createCellStyle();
				fail.setFillForegroundColor(IndexedColors.RED.getIndex());
				fail.setFillPattern(CellStyle.SOLID_FOREGROUND);
				cell.setCellStyle(fail);
			}
		}
		FileOutputStream out = new FileOutputStream("TestResults/"+strDataFileName+".xls");
		wb.write(out);
		wb.close();
	}
}