package Com.Sample;
import org.openqa.selenium.By;
import org.openqa.selenium.remote.DesiredCapabilities;
import org.testng.annotations.Test;
import io.appium.java_client.android.AndroidDriver;
import java.net.URL;
public class BrowserCircle 
{
	public static final String USERNAME = "giriprasad2";
	public static final String AUTOMATE_KEY = "uspysghfBZTycvoQHY2i";
	public static final String URL = "https://" + USERNAME + ":" + AUTOMATE_KEY + "@hub-cloud.browserstack.com/wd/hub";
	@Test
	public  void Browser() throws Exception
	{
	DesiredCapabilities caps = new DesiredCapabilities();
	caps.setCapability("os_version", "8.0");
	caps.setCapability("device", "Google Pixel 2");
	caps.setCapability("real_mobile", "true");
	caps.setCapability("browserstack.debug", "true");
	//caps.setCapability("deviceOrientation", "landscape");
	//WebDriver driver = new RemoteWebDriver(new URL(URL), caps);
	AndroidDriver driver = new AndroidDriver(new URL(URL), caps);
	driver.get("https://mydreamstore.in/");
	System.out.println(driver.getTitle());
    Thread.sleep(5000);
    driver.getContext();
    driver.context("NATIVE_APP");
    driver.findElement(By.xpath("//*[@resource-id='android:id/button1']")).click();
    driver.context("CHROMIUM");
	driver.findElement(By.xpath("//img[contains(@src,'https://mydreamstore.in/static/images/logo_mds.png')]")).click();
    Thread.sleep(5000);
    driver.getContext();
    System.out.println("successful");
    driver.quit();
	}
}
