package Com.Sample;
import java.util.HashMap;
import java.util.Map;

import org.openqa.selenium.WebDriver;
import org.openqa.selenium.chrome.ChromeDriver;
import org.openqa.selenium.chrome.ChromeOptions;
import org.openqa.selenium.firefox.FirefoxDriver;
import org.openqa.selenium.ie.InternetExplorerDriver;
import org.testng.annotations.Parameters;
import org.testng.annotations.Test;

public class BrowserClass {

@Test

// Here this parameters we will take from testng.xml
@Parameters("Browser")
public  void test1(String browser) {

if(browser.equalsIgnoreCase("chrome")){

	System.setProperty("webdriver.chrome.driver","/usr/lib/chromium-browser/chromedriver");
	Map<String, Object> prefs = new HashMap<String, Object>();
	prefs.put("profile.default_content_setting_values.notifications", 2);
	ChromeOptions options = new ChromeOptions();
	options.setExperimentalOption("prefs", prefs);
	WebDriver driver = new ChromeDriver(options);
}
else if(browser.equalsIgnoreCase("IE")){

System.setProperty("webdriver.ie.driver", "./server/IEDriverServer.exe");

WebDriver driver=new InternetExplorerDriver();

driver.manage().window().maximize();

driver.get("http://www.facebook.com");

driver.quit();
}
}

}