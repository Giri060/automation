package Com.Sample;
import java.io.File;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Calendar;
import java.util.List;
import java.util.Properties;
import javax.activation.DataHandler;
import javax.activation.DataSource;
import javax.activation.FileDataSource;
import javax.mail.Authenticator;
import javax.mail.BodyPart;
import javax.mail.Message;
import javax.mail.Multipart;
import javax.mail.PasswordAuthentication;
import javax.mail.Session;
import javax.mail.Transport;
import javax.mail.internet.InternetAddress;
import javax.mail.internet.MimeBodyPart;
import javax.mail.internet.MimeMessage;
import javax.mail.internet.MimeMultipart;
import org.openqa.selenium.WebDriver;
import org.testng.annotations.Test;
import Com.Data.ReadDataFile;
import Com.Homepage.Login;
import Com.Sample.SendMail.SMTPAuthenticator;
public class SendMail {
	WebDriver driver;
	private String from;
	// private String to;
	// String []to ;
	private String subject;
	private String messageBody;
	private String fileName;
	private String host;
	private String port;
	ReadDataFile readData = new ReadDataFile();
	String strDataFileName = this.getClass().getSimpleName();
	public String strAbsolutepath = new File("").getAbsolutePath();
	private String strDataPath = strAbsolutepath + "/data/";
	private String strFilePath = strAbsolutepath + "/test-output/";
	int rownumber= 1;
	List<String> to= new ArrayList();
	List<String> cc= new ArrayList();
	//  List<String> array= new ArrayList();
	private Properties properties;
	private MimeMessage message;
	private BodyPart messageBodyPart;
	private Multipart multipart;
	private Authenticator authenticator;
	 public SendMail ()
	    {
	    	SendMailMethod();
	    }
	public void SendMailMethod () {
		String name = new Object(){}.getClass().getEnclosingMethod().getName();
		//HISAutomationReport HISAutomationReport = new HISAutomationReport();
		from = "giri@mydreamstore.in";
		//to = "mayank.mishra@renaissance-it.com,vineeth.v@renaissance-it.com";
		// List<String> to= new ArrayList();
		// WebDriver driver = null ; 
		to = FetchEmails(strDataFileName, "EMailForTo",  name);
		cc = FetchEmails(strDataFileName, "EMailForCC", name);
		/*  to.add("mayank.mishra@renaissance-it.com");
      to.add("vineeth.v@renaissance-it.com");
      cc.add("vineeth.muskula.esics@relindia.in");*/
		// to = {"mayank.mishra@renaissance-it.com","vineeth.v@renaissance-it.com"};
		String dateTime = new SimpleDateFormat("dd-MM-yyyy hh:mm a").format(Calendar.getInstance().getTime()) ;
		//String newfinalstatusText = statusfinal;
		subject = "HIS Automation Regression Report - " + dateTime + "  STATUS : " ;
		subject = "MyDreamStore Automation Report - " + dateTime;
		//String newfinalstatusText = HISDailyRegressionFlow.statusfinal;
		/*messageBody = "<html> <head> </head> <body> <table border=\"1\" cellpadding=\"1\" cellspacing=\"1\"> <tr style=\"background-color: #a9c2e8;\">";
        messageBody +=  "<td>Test Step Name</td><td>Expected Result</td><td>Actual Result</td><td>Status</td></tr><tr>";
  	//      messageBody +=  "<td>Test Step Name</td><td>Expected Result</td><td>Actual Result</td><td>" + openURLStatus +" </td></tr><tr>";
		 */
		// 	messageBody = "<html><body><h1> STATUS : " + newfinalstatusText +" </br></br></br>  </h1></body></html>";
		messageBody = "<html><body><h4> Dear Team, &nbsp; </h4></body></html>";
		messageBody += "<html><body><h4>MyDreamStore Application Status Through Automation : </br></br></br></h4></body></html>";
		//messageBody = "<html><body><h2>Note: This is an auto-generated mail. Please do not reply back to this mail.</h2></body></html>";
		messageBody += "<html> <head> </head> <body> <table border=\"1\" cellpadding=\"1\" cellspacing=\"1\"> <tr style=\"background-color: #a9c2e8;\">";
		//messageBody +=  "<td><strong>Test Step Name &nbsp;&nbsp;</strong></td><td><strong>Expected Result</strong></td><td><strong>Status &nbsp;&nbsp;&nbsp; </strong></td><td><strong>Execution Time &nbsp;&nbsp;&nbsp; </strong></td></tr><tr>";
		messageBody +=  "<td><strong>Test Step Name &nbsp;&nbsp;</strong></td><td><strong>Expected Result</strong></td><td><strong>Status &nbsp;&nbsp;&nbsp; </strong></td></tr><tr>";
		/*Login*/
	    messageBody +=  "<td>URL Navigation &nbsp;</td><td>User Should able to Navigate MyDreamStore Application &nbsp;</td><td>" + Login.URLNavigationstatus  +"&nbsp; </td></tr><tr>";
	    messageBody +=  "<td>User Login &nbsp;</td><td>User Should able to Login Mydreamstore &nbsp;</td><td>" + Login.Loginstatus +"&nbsp; </td></tr><tr></body></html>";
		/*        Registration 
	    messageBody +=  "<td>URL Navigation &nbsp;</td><td>User Should able to Navigate HIS Application &nbsp;</td><td>" + Registration.OpenURLStatus +"&nbsp; </td><td>" + Registration.OpenURLExecutionTime +"&nbsp; </td></tr><tr>";
	    messageBody +=  "<td>UHID Registration &nbsp;</td><td>User Should able to Create UHID &nbsp;</td><td>" + Registration.UHIDRegistrationStatus +"&nbsp; </td><td>" + Registration.UHIDExecutionTime +"&nbsp; </td></tr><tr>";
	    messageBody +=  "<td>Patient CheckIN &nbsp;</td><td>User Should able to do Patient CheckIn &nbsp;</td><td>" + Registration.PatientCheckInStatus +"&nbsp; </td><td>" + Registration.PCExecutionTime +"&nbsp; </td></tr><tr>";
	    messageBody +=  "<td>Patient Admission &nbsp;</td><td>User Should able to do Patient Admission &nbsp;</td><td>" + Registration.PatientAdmissionStatus +"&nbsp; </td><td>" + Registration.PAExecutionTime +"&nbsp; </td></tr><tr>";
	    messageBody +=  "<td>Cancel CheckIN &nbsp;</td><td>User Should able to do Cancel CheckIN &nbsp;</td><td>" + Registration.CancelCheckInStatus +"&nbsp; </td><td>" + Registration.CCExecutionTime +"&nbsp; </td></tr><tr>";
	     Clinical Records 
	    //messageBody +=  "<td>URL Navigation &nbsp;</td><td>User Should able to Navigate HIS Application &nbsp;</td><td>" + CLM.OpenURLStatus +"&nbsp; </td><td>" + CLM.OpenURLExecutionTime +"&nbsp; </td></tr><tr>";
	    //messageBody +=  "<td>Patient CheckIN Registration &nbsp;</td><td>User Should able to do Patient CheckIn &nbsp;</td><td>" + CLM.PatientCheckInStatus +"&nbsp; </td><td>" + CLM.PCExecutionTime +"&nbsp; </td></tr><tr>";
	    messageBody +=  "<td>Doctor Worklist &nbsp;</td><td>User Should able access Doctor Worklist &nbsp;</td><td>" + CLM.DoctorWorklistStatus +"&nbsp; </td><td>" + CLM.DWExecutionTime +"&nbsp; </td></tr><tr>";
	    messageBody +=  "<td>Case Sheet &nbsp;</td><td>User Should able do all transactions in Casesheet &nbsp;</td><td>" + CLM.CaseSheetStatus +"&nbsp; </td><td>" + CLM.CSExecutionTime +"&nbsp; </td></tr><tr>";
	     Stores  
	    messageBody +=  "<td>Adjustments &nbsp;</td><td>User Should able do Adjustments &nbsp;</td><td>" + Stores.AdjustmentsStatus +"&nbsp; </td><td>" + Stores.AdjExecutionTime +"&nbsp; </td></tr><tr>";
	    messageBody +=  "<td>Physical Stock Entry &nbsp;</td><td>User Should able do Physical Stock Entry &nbsp;</td><td>" + Stores.PhysicalStockEntryStatus +"&nbsp; </td><td>" + Stores.PSEExecutionTime +"&nbsp; </td></tr><tr>";
	    messageBody +=  "<td>Pharmacy Issues &nbsp;</td><td>User Should able do Pharmacy Issues &nbsp;</td><td>" + Stores.PharmacyIssuesStatus +"&nbsp; </td><td>" + Stores.PIExecutionTime +"&nbsp; </td></tr><tr>";
	     Operation Theater 
	    messageBody +=  "<td>Viw OT List &nbsp;</td><td>User Should able access View OT list &nbsp;</td><td>" + OperationTheater.ViewOTlistStatus +"&nbsp; </td><td>" + OperationTheater.ViewOTExecutionTime +"&nbsp; </td></tr><tr>";
	    messageBody +=  "<td>PAC &nbsp;</td><td>User Should able do PAC &nbsp;</td><td>" + OperationTheater.PACStatus +"&nbsp; </td><td>" + OperationTheater.PACExecutionTime +"&nbsp; </td></tr><tr>";
	    messageBody +=  "<td>Surgery Record &nbsp;</td><td>User Should able do Surgery Record &nbsp;</td><td>" + OperationTheater.SurgeryRecordStatus +"&nbsp; </td><td>" + OperationTheater.SurgeryExecutionTime +"&nbsp; </td></tr><tr>";
	     Laboratory 
	    messageBody +=  "<td>Lab Worklist &nbsp;</td><td>User Should able access Lab Worklist &nbsp;</td><td>" + Laboratory.LabWorklistStatus +"&nbsp; </td><td>" + Laboratory.WorklistExecutionTime +"&nbsp; </td></tr><tr>";
	    messageBody +=  "<td>Sample Acknowledge &nbsp;</td><td>User Should able do Sample Acknowledge &nbsp;</td><td>" + Laboratory.SampleAcknowledgeStatus +"&nbsp; </td><td>" + Laboratory.SAExecutionTime +"&nbsp; </td></tr><tr>";
	    messageBody +=  "<td>Bulk Result Entry &nbsp;</td><td>User Should able do Bulk Result Entry &nbsp;</td><td>" + Laboratory.BulkResultEntryStatus +"&nbsp; </td><td>" + Laboratory.REExecutionTime +"&nbsp; </td></tr><tr>";
	    messageBody +=  "<td>Bulk Result Verification &nbsp;</td><td>User Should able do Bulk Result Verification &nbsp;</td><td>" + Laboratory.BulkResultVerificationStatus +"&nbsp; </td><td>" + Laboratory.RVExecutionTime +"&nbsp; </td></tr><tr></body></html>";
		 */    
		/* messageBody +=  "<td>Patient Check-IN &nbsp;</td><td>User Should able to Do Patient Check-IN &nbsp;&nbsp;</td><td>" + HISAutomationReport.CheckINStatus +" </td></tr><tr>";
	    messageBody +=  "<td>Doctor Worklist</td><td>User Should able to View Doctor Worklist &nbsp;&nbsp;</td><td>" + HISAutomationReport.DoctorWorklistStatus +" </td></tr><tr>";
	    messageBody +=  "<td>Patient CaseSheet &nbsp;&nbsp;</td><td>User Should able to Open Patient Casesheet &nbsp;&nbsp;</td><td>" + HISAutomationReport.CasesheetStatus +" </td></tr><tr></body></html>";
		 */
		/* if(newfinalstatusText.equalsIgnoreCase("Pass"))
		{
			messageBody += "<html><body><h1><u> STATUS :&nbsp;&nbsp; " +
					" <font color=\"Green\">" + newfinalstatusText +"</u> <h1> </body></html>";
		}
		else
		{	messageBody += "<html><body><h1><u> STATUS : &nbsp;&nbsp;" +
				" <font color=\"Red\">" + newfinalstatusText +" </u><h1> </body></html>";
		}*/
		//messageBody += "<html><body><h1> Total Execution Time : &nbsp;" + ExecutionTime +" <h1> </body></html>";
		messageBody += "<html><body><h4><font color=\"blue\">Note: This is an auto-generated mail. Please do not reply to this mail. </br></br></br></font> </h4></body></html>";
		messageBody += "<html><body><h4><font color=\"green\">Note: Please download the attached document to view Test Results. </br></br></br></font> </h4></body></html>";
		messageBody += "<html><body><h3><font color=\"DarkRed\"> Thanks & Regards,</br></br></br></font> </h3></body></html>";	
		messageBody += "<html><body><h2><font color=\"DarkRed\"> QA Team </br></br></br></font> </h2></body></html>";
		// fileName = "quiz.txt";
		//fileName = ("/home/giri/Testing/automation/Mydreamstore/test-output/TestReport.html"); // Read the excel
		  fileName = (strFilePath + "TestReport.html"); // Read the excel
		host = "smtp.gmail.com";
        port = "465";
		/*host = "mail.renaissance-it.com";
		port = "587";*/
		authenticator = new SMTPAuthenticator ();
		properties = System.getProperties ();
		properties.put ( "mail.smtp.host", host );
		properties.put("mail.smtp.socketFactory.class","javax.net.ssl.SSLSocketFactory");
		properties.put ( "mail.smtp.starttls.enable", "false" );
		properties.put ( "mail.smtp.port",port );
		properties.put ( "mail.smtp.auth", "true" );
	}
	public List<String> FetchEmails(String strDataFileName, String strElement,String Methodname) 
	{
		List<String> array= new ArrayList();
		try {
			int columnCount = 0;
			String[] Emails = new String[0];
			int rowNumber = 1;
			String strEmails = readData.readDataFile(strDataFileName, rowNumber, strElement , Methodname);
			Emails = strEmails.split(";");
			List<String> emailIds= Arrays.asList(Emails);
			columnCount = emailIds.size();
			for (int cnt = 0; cnt < columnCount; cnt++) 
			{
				String excelEmailValues = emailIds.get(cnt);
				array.add(excelEmailValues);
			}
		} catch (Exception e) {
			System.out.println(e.getMessage());
		}
		return array;
	}
	private void SendMailFinal ( String from, List<String> to2,List<String> cc2,
			String subject, String messageBody, String fileName ) {
		try {
			Session session = Session.getDefaultInstance ( properties, authenticator ); //new javax.mail.Authenticator()
			message = new MimeMessage ( session );
			message.setFrom ( new InternetAddress ( from ) );
			for (String string : to2) 
			{
				message.addRecipient ( Message.RecipientType.TO,
						new InternetAddress ( string ) );            }
			for (String string : cc2) 
			{
				message.addRecipient ( Message.RecipientType.CC,
						new InternetAddress ( string ) );            }
			message.setSubject ( subject );
			multipart = new MimeMultipart ();
			messageBodyPart = new MimeBodyPart ();
			messageBodyPart.setContent ( messageBody, "text/html" );
			multipart.addBodyPart ( messageBodyPart );
			messageBodyPart = new MimeBodyPart ();
			DataSource source = new FileDataSource ( fileName );
			messageBodyPart.setDataHandler ( new DataHandler ( source ) );
			messageBodyPart.setFileName ( fileName );
			multipart.addBodyPart ( messageBodyPart );
			message.setContent ( multipart );
			Transport.send ( message );
			System.out.println ( "Message send successfully...." );
		} catch ( Exception me ) {
			me.printStackTrace ();
		}
	} 
	private void performTask () {
		SendMailFinal ( from, to,cc, subject, messageBody, fileName );
	}
	@Test(priority = 0)
	public void SendMailFinal() throws Exception
	{
		Thread.sleep(10000);
		new SendMail().performTask ();
	}
	class SMTPAuthenticator extends Authenticator {
		private static final String SMTP_AUTH_USER = "giri@mydreamstore.in";
		private static final String SMTP_AUTH_PASSWORD = "Giri@2207";
		public PasswordAuthentication getPasswordAuthentication () {
			String username = SMTP_AUTH_USER;
			String password = SMTP_AUTH_PASSWORD;
			return new PasswordAuthentication( username,  password );
		}
	}
}
