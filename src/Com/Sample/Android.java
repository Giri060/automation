package Com.Sample;

import org.openqa.selenium.By;
import org.openqa.selenium.remote.DesiredCapabilities;
import io.appium.java_client.android.AndroidDriver;

import java.net.URL;

public class Android {	
	
	public static final String USERNAME = "raj964";
	public static final String AUTOMATE_KEY = "qesBpyNzdyQGs3wK7ekp";
	public static final String URL = "https://" + USERNAME + ":" + AUTOMATE_KEY + "@hub-cloud.browserstack.com/wd/hub";

	public static void main(String[] args) throws Exception {

	DesiredCapabilities caps = new DesiredCapabilities();
	caps.setCapability("os_version", "6.0");
	caps.setCapability("device", "Google Nexus 6");
	caps.setCapability("realMobile", "true");
	caps.setCapability("browserstack.debug", "true");
	caps.setCapability("browserstack.local", "true");   //its used for CirclCI
	//caps.setCapability("deviceOrientation", "landscape");

	//WebDriver driver = new RemoteWebDriver(new URL(URL), caps);
	AndroidDriver driver = new AndroidDriver(new URL(URL), caps);
	driver.get("https://mydreamstore.in/");
    Thread.sleep(5000);
    driver.getContext();

    driver.context("NATIVE_APP");
    driver.findElement(By.xpath("//*[@resource-id='android:id/button1']")).click();
    driver.context("CHROMIUM");

	driver.findElement(By.xpath("//img[contains(@src,'https://mydreamstore.in/static/images/logo_mds.png')]")).click();
    Thread.sleep(5000);
    driver.getContext();
    
    

    driver.quit();

	}
	}