package Com.Sample;
import java.io.File;
import java.util.Map;
import org.apache.poi.hssf.usermodel.HSSFSheet;
import org.apache.poi.hssf.usermodel.HSSFWorkbook;
import org.openqa.selenium.WebDriver;
import org.testng.Assert;
import org.testng.annotations.Test;
import com.aventstack.extentreports.Status;
import com.aventstack.extentreports.markuputils.ExtentColor;
import com.aventstack.extentreports.markuputils.MarkupHelper;
import Com.ObjectRepository.Checkout_OR;
import Com.ObjectRepository.Homepage2_OR;
import Com.ObjectRepository.Homepage_OR;
import Com.ObjectRepository.Login_OR;
import Com.Utilities.Browsers;
import Com.Utilities.CommonMethods;
import Com.Utilities.GetScreenShot;
import Com.Utilities.Screenshot;
import Com.Utilities.Steps;
import Com.Utilities.TestBase;
public class ConnectDB extends TestBase
{
	WebDriver driver;
	Login_OR login = new Login_OR();
	Steps steps = new Steps();
	GetScreenShot getscreen = new GetScreenShot();
	Screenshot screen = new Screenshot();
	public String strAbsolutepath = new File("").getAbsolutePath();
	HSSFWorkbook workbook;
	HSSFSheet sheet;
	String strDataFileName = this.getClass().getSimpleName();
	Map<String, Object[]> testresultdata; 
	CommonMethods commonmethods = new CommonMethods();
	Homepage_OR home = new Homepage_OR();
	Homepage2_OR home2 = new Homepage2_OR();
	Checkout_OR checkout = new Checkout_OR();
	Login_OR log = new Login_OR();
	Screenshot screenshot = new Screenshot();
	String passstatus = screenshot.status("PASS");
	String failstatus = screenshot.status("FAIL");
	int cnt = 2;
	public static String ConnectDBstatus = "";
	public static String ConnectDBstatusfail = "";
	
	public ConnectDB()
	{
		Browsers b = new Browsers();
		this.driver = b.getBrowsers("chrome1");		 
	}
	@Test(priority=0)
	public void SignUpFlow() throws Exception
	{
		String name = new Object(){}.getClass().getEnclosingMethod().getName();
		cnt ++;
		try
		{
			test = extent.createTest("SignUpFlow");
			System.out.println("SignUp Flow Test Case Executing...");
			commonmethods.URLNaviagtion(driver);
			steps.clickButton(log.log, driver, name);
			System.out.println("User clicked on Login/MyAccount");
			try
			{
				String logtext = driver.findElement(log.Logintext).getText();
				System.out.println("User navigated to   "+logtext+"    page");
				steps.EnterText(log.Login_Email, driver, strDataFileName, "InvalidUsername", name);
				System.out.println("User entered invalid username");
				steps.clickButton(log.Login_Continue, driver, name);
				Thread.sleep(500);
				String text1 = driver.findElement(login.Email_error).getText();
				System.out.println("Displayed error message:  "+text1);
				steps.Clear(log.Login_Email, driver, name);
				steps.EnterText(log.Login_Email, driver, strDataFileName, "Username", name);
				steps.clickButton(log.Login_Continue, driver, name);
				System.out.println("User entered valid username and clicked on continue ");
				Thread.sleep(2000);
				try
				{
					String mobiletext = driver.findElement(log.Mobilecode).getText();
					System.out.println(mobiletext);
					steps.EnterText(log.Mobile, driver, strDataFileName, "Inavlidmobile", name);
					Thread.sleep(5000);
					String mobileerror = driver.findElement(log.MobileErrormsg).getText();
					System.out.println("User entered invalid mobile number    "+mobileerror);
					steps.Clear(log.Mobile, driver, name);
					steps.EnterText(log.Mobile, driver, strDataFileName, "mobileno", name);
					Thread.sleep(2000);
				}
				catch(Exception e)
				{
					System.out.println("Mobile field is not there");
				}
				steps.EnterText(log.Login_password, driver, strDataFileName, "InvalidPassword", name);
				steps.clickJSButton(login.Login_loginbutton, driver, name);
				Thread.sleep(4000);
				try
				{
					String PasswordErrr = driver.findElement(login.Passworderror).getText();
					System.out.println(PasswordErrr);
				}
				catch(Exception e)
				{
					String mobileerror1 = driver.findElement(log.MobileErrormsg1).getText();
					System.out.println(mobileerror1);
				}
			}
			catch(Exception e)
			{
				
			}
			//	driver.close();
			ConnectDBstatus = passstatus;
			Assert.assertTrue(true);
			System.out.println("Login User Prepaid CheckOut Successful");
			testresultdata.put(Integer.toString(cnt), new Object[] {name, "Login User Prepaid CheckOut ", "Login User Prepaid CheckOut Successful",passstatus});
		}
		catch(Exception e)
		{
			System.out.println("Login User Prepaid CheckOut Unsuccessful");
			testresultdata.put(Integer.toString(cnt), new Object[] {name, "Login User Prepaid CheckOut", "Login User Prepaid CheckOut Unsuccessful",failstatus});
			ConnectDBstatus = failstatus;
			ConnectDBstatusfail = getscreen.capture(driver, "ConnectDBstatusfail");
			test.addScreenCaptureFromPath(ConnectDBstatusfail);
			driver.close();
			Assert.assertTrue(false);		
		}
	}
}
