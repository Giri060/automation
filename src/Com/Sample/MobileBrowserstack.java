package Com.Sample;
import java.io.File;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.IOException;
import java.net.MalformedURLException;
import java.net.URL;
import java.util.Date;
import java.util.HashMap;
import java.util.LinkedHashMap;
import java.util.Map;
import java.util.Set;
import org.apache.poi.hssf.usermodel.HSSFCell;
import org.apache.poi.hssf.usermodel.HSSFCellStyle;
import org.apache.poi.hssf.usermodel.HSSFRow;
import org.apache.poi.hssf.usermodel.HSSFSheet;
import org.apache.poi.hssf.usermodel.HSSFWorkbook;
import org.apache.poi.ss.usermodel.Cell;
import org.apache.poi.ss.usermodel.CellStyle;
import org.apache.poi.ss.usermodel.IndexedColors;
import org.apache.poi.ss.usermodel.Row;
import org.openqa.selenium.Keys;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.chrome.ChromeOptions;
import org.openqa.selenium.interactions.Actions;
import org.openqa.selenium.remote.DesiredCapabilities;
import org.openqa.selenium.remote.RemoteWebDriver;
import org.openqa.selenium.support.Color;
import org.testng.Assert;
import org.testng.annotations.AfterClass;
import org.testng.annotations.BeforeClass;
import org.testng.annotations.BeforeTest;
import org.testng.annotations.Test;
import com.aventstack.extentreports.Status;
import com.aventstack.extentreports.markuputils.ExtentColor;
import com.aventstack.extentreports.markuputils.MarkupHelper;
import Com.ObjectRepository.Checkout_OR;
import Com.ObjectRepository.Homepage2_OR;
import Com.ObjectRepository.Homepage_OR;
import Com.ObjectRepository.Login_OR;
import Com.ObjectRepository.MobileWorkflow_OR;
import Com.Utilities.Browsers;
import Com.Utilities.CommonMethods;
import Com.Utilities.GetScreenShot;
import Com.Utilities.Screenshot;
import Com.Utilities.Steps;
import Com.Utilities.TestBase;
import jxl.format.Border;
import jxl.write.WritableCellFormat;
import jxl.write.WritableFont;
public class MobileBrowserstack extends TestBase
{
	WebDriver driver;
	Login_OR login = new Login_OR();
	Steps steps = new Steps();
	GetScreenShot getscreen = new GetScreenShot();
	Screenshot screen = new Screenshot();
	public String strAbsolutepath = new File("").getAbsolutePath();
	HSSFWorkbook workbook;
	HSSFSheet sheet;
	String strDataFileName = this.getClass().getSimpleName();
	Map<String, Object[]> testresultdata; 
	CommonMethods commonmethods = new CommonMethods();
	Homepage_OR home = new Homepage_OR();
	Homepage2_OR home2 = new Homepage2_OR();
	Checkout_OR checkout = new Checkout_OR();
	MobileWorkflow_OR mob = new MobileWorkflow_OR();
	Screenshot screenshot = new Screenshot();
	String passstatus = screenshot.status("PASS");
	String failstatus = screenshot.status("FAIL");
	int cnt = 2;
	public static String LoginUserstatusfail = "";
	public static String LoginUserstatus = "";
	public static String check1 = "";
	public static String check2 = "";
	public static String check3 = "";
	public static String check4 = "";
	public static String check5 = "";
	public static String check6 = "";
	public static String checkColor1 = "";
	public static String checkhex1 = "";
	public static String checkFirstcolor1 = "";
	public static String checkFifthcolor1 = "";
	public static String checkcolor5 = "";
	public static String checkhex5 = "";
	public static String checkcqty1 = "";
	public static String checkiqty1 = "";
	public static String checkdqty1 = "";
	public static String checkSizemsg1 = "";
	public static String checksizeS1 = "";
	public static String checksizeM1 = "";
	public static String checksizeXL1 = "";
	public static String checksizeL1 = "";
	public static String checksizeXXL1 = "";
	public static String checksizeXXXL1 = "";
	public static String checkbelow5101 = "";
	public static String checkabove5101 = "";
	public static String checktightfit1 = "";
	public static String checkcomfit1 = "";
	public static String checkfindursize1 = "";
	public static String checkaddcart1 = "";
	public static String checkupdateproduct1 = "";
	public static String checkremovecart1 = "";
	public static String buynow1 = "";
	public static String buynow2 = "";
	public static String buynow3 = "";
	public static String check7 = "";
	public static String check8 = "";
	public static String check9 = "";
	public static String check10 = "";
	public static String check11 = "";
	public static String check12 = "";
	public static String check13 = "";
	public static String check14 = "";
	public static String check15 = "";
	public static String check16 = "";
	public static String check17 = "";
	public static String check18 = "";
	public static String check19 = "";
	public static String check20 = "";
	public static String check21 = "";
	public static String check22 = "";
	public static String check23 = "";
	public static String check24 = "";
	public static String checkgColor1 = "";
	public static String checkghex1 = "";
	public static String checkgFirstcolor1 = "";
	public static String checkgcolor5 = "";
	public static String checkghex5 = "";
	public static String checkgFifthcolor1 = "";
	public static String checkgiqty1 = "";
	public static String checkgSizemsg1 = "";
	public static String checkgdqty1 = "";
	public static String checkgsizeS1 = "";
	public static String checkgsizeM1 = "";
	public static String checkgsizeL1 = "";
	public static String checkgsizeXL1 = "";
	public static String checkgsizeXXL1 = "";
	public static String checkgsizeXXXL1 = "";
	public static String checkgbelow5101 = "";
	public static String checkgabove5101 = "";
	public static String checkgtightfit1 = "";
	public static String checkgcomfit1 = "";
	public static String checkgfindursize1 = "";
	public static String checkgaddcart1 = "";
	public static String checkgremovecart1 = "";
	public static String checkgupdateproduct1 = "";
	public static String buygnow1 = "";
	public static String Debitg = "";
	public static String Creditg = "";
	public static String NetBankingg = "";
	public static String Paytmg = "";
	public static String cashg = "";
	public static String MDSg = "";
	public static String cartg = "";
	public static String GuestUserstatusfail = "";
	public static String GuestUserstatus = "";
	public static String qty = "";
	public static String checksizeM2 = "";
	public static final String USERNAME = "raj964";
	  public static final String AUTOMATE_KEY = "qesBpyNzdyQGs3wK7ekp";
	  public static final String URL = "https://" + USERNAME + ":" + AUTOMATE_KEY + "@hub-cloud.browserstack.com/wd/hub";
/*
	public MobileBrowserstack()
	{
		Browsers b = new Browsers();
		this.driver = b.getBrowsers("MobileProfile");		 
	}
*/
	//@Test(priority=0)
	public void MobileLoginUser() throws Exception
	{
		String name = new Object(){}.getClass().getEnclosingMethod().getName();
		cnt ++;
		try
		{
			test = extent.createTest("Mobile Login User CheckOut");
			System.out.println("Mobile Login User CheckOut Test Case Executing...");
			commonmethods.MobileLogin(driver);
			test.log(Status.INFO, MarkupHelper.createLabel("User able to Login", ExtentColor.BLUE));
			Thread.sleep(1000);
			commonmethods.URLNaviagtion(driver);
			Thread.sleep(1000);
			String Parent = driver.getWindowHandle();
			steps.clickButton(mob.Mob_Mydreamstore, driver, name);
			Thread.sleep(2000);
			steps.clickButton(mob.Mob_Image1, driver, name);
			steps.ChildWindow(driver);
			//driver.manage().window().maximize();
			check1 = getscreen.capture(driver, "check1");
			test.addScreenCaptureFromPath(check1);
			Thread.sleep(1000);
			try
			{
				String Filterpage1 = driver.findElement(home.Filterpagetext).getText();
				System.out.println("User navigated to :   "+Filterpage1);
			}
			catch(Exception e)
			{
				steps.clickButton(mob.Mob_Image2, driver, name);
				System.out.println("User navigated to different landing page");
				driver.close();
				steps.ChildWindow(driver);
				Thread.sleep(1000);
			}
			steps.clickButton(mob.Mob_ImageSlide, driver, name);
			check22 = getscreen.capture(driver, "check22");
			test.addScreenCaptureFromPath(check22);
			steps.ScrollDown(driver);
			Thread.sleep(2000);
			try
			{
				steps.clickButton(mob.Mob_Readmore, driver, name);
				test.log(Status.INFO, MarkupHelper.createLabel("User clicked on Readmore", ExtentColor.BLUE));
				check2 = getscreen.capture(driver, "check2");
				test.addScreenCaptureFromPath(check2);
				check3 = driver.findElement(mob.Mob_Description).getText();
				System.out.println("Item Description   "+check3);
				test.log(Status.INFO, MarkupHelper.createLabel("Description about Item:  "+check3, ExtentColor.BLUE));
				steps.clickButton(mob.Mob_Readless, driver, name);
			}
			catch(Exception e)
			{
				steps.clickButton(mob.Mob_Readmore, driver, name);
				test.log(Status.INFO, MarkupHelper.createLabel("User clicked on Readmore", ExtentColor.BLUE));
				check4 = getscreen.capture(driver, "check4");
				test.addScreenCaptureFromPath(check4);
				check5 = driver.findElement(mob.Mob_Description1).getText();
				System.out.println("Item Description   "+check5);
				test.log(Status.INFO, MarkupHelper.createLabel("Description about Item:  "+check5, ExtentColor.BLUE));
				steps.clickButton(mob.Mob_Readless, driver, name);
			}
			Thread.sleep(1000);
			check6 = driver.findElement(mob.Mob_MRP).getText();
			System.out.println("Item Price   "+check6);
			test.log(Status.INFO, MarkupHelper.createLabel("MRP of an Item:  "+check6, ExtentColor.ORANGE));
			try
			{
				String col1 = driver.findElement(mob.Mob_color1).getCssValue("color");
				System.out.println(col1);
				steps.clickButton(mob.Mob_color1, driver, name);
				Thread.sleep(2000);
				checkColor1 = driver.findElement(mob.Mob_color1).getCssValue("background-color");
				checkhex1 = Color.fromString(checkColor1).asHex();
				System.out.println("Image color: "+checkhex1);
				test.log(Status.INFO, MarkupHelper.createLabel("User changed item color to:  "+checkhex1, ExtentColor.BLUE));
				checkFirstcolor1 = getscreen.capture(driver, "checkFirstcolor1");
				test.addScreenCaptureFromPath(checkFirstcolor1);
			}
			catch(Exception e)
			{
				System.out.println("Color 2 is not exists");
			}
			try
			{
				String col2 = driver.findElement(mob.Mob_color5).getCssValue("color");
				System.out.println(col2);
				steps.clickButton(mob.Mob_color5, driver, name);
				Thread.sleep(2000);
				checkcolor5 = driver.findElement(mob.Mob_color5).getCssValue("background-color");
				checkhex5 = Color.fromString(checkcolor5).asHex();
				System.out.println("Image color: "+checkhex5);
				test.log(Status.INFO, MarkupHelper.createLabel("User changed item color to:  "+checkhex5, ExtentColor.BLUE));
				checkFifthcolor1 = getscreen.capture(driver, "checkFifthcolor1");
				test.addScreenCaptureFromPath(checkFifthcolor1);
			}
			catch(Exception e)
			{
				System.out.println("Color 1 is not exists");
			}
			Thread.sleep(1000);
			steps.clickButton(mob.Mob_Findyoursize, driver, name);
			test.log(Status.INFO, MarkupHelper.createLabel("User clicked on Find your size", ExtentColor.BLUE));
			check24 = getscreen.capture(driver, "check24");
			test.addScreenCaptureFromPath(check24);
			steps.clickButton(mob.Mob_Findyoursizeclose, driver, name);
			Thread.sleep(1000);
			steps.clickButton(mob.Mob_Addtocart, driver, name);
			check23 = getscreen.capture(driver, "check23");
			test.addScreenCaptureFromPath(check23);
			Thread.sleep(1000);
			steps.ScrollDown(driver);
			steps.clickButton(mob.Mob_SizeS, driver, name);
			checksizeS1 = driver.findElement(mob.Mob_SizeS).getText();
			System.out.println("Selected Size:  "+checksizeS1);
			test.log(Status.INFO, MarkupHelper.createLabel("User selected size:  "+checksizeS1, ExtentColor.BLUE));
			steps.clickButton(mob.Mob_SizeM, driver, name);
			checksizeM1 = driver.findElement(mob.Mob_SizeM).getText();
			System.out.println("Selected Size:  "+checksizeM1);
			test.log(Status.INFO, MarkupHelper.createLabel("User selected size:  "+checksizeM1, ExtentColor.BLUE));
			steps.clickButton(mob.Mob_SizeL, driver, name);
			checksizeL1 = driver.findElement(mob.Mob_SizeL).getText();
			System.out.println("Selected Size:  "+checksizeL1);
			test.log(Status.INFO, MarkupHelper.createLabel("User selected size:  "+checksizeL1, ExtentColor.BLUE));
			steps.clickButton(mob.Mob_SizeXL, driver, name);
			checksizeXL1 = driver.findElement(mob.Mob_SizeXL).getText();
			System.out.println("Selected Size:  "+checksizeXL1);
			test.log(Status.INFO, MarkupHelper.createLabel("User selected size:  "+checksizeXL1, ExtentColor.BLUE));
			steps.clickButton(mob.Mob_SizeXXL, driver, name);
			checksizeXXL1 = driver.findElement(mob.Mob_SizeXXL).getText();
			System.out.println("Selected Size:  "+checksizeXXL1);
			test.log(Status.INFO, MarkupHelper.createLabel("User selected size:  "+checksizeXXL1, ExtentColor.BLUE));
			steps.clickButton(mob.Mob_SizeXXXL, driver, name);
			checksizeXXXL1 = driver.findElement(mob.Mob_SizeXXXL).getText();
			System.out.println("Selected Size:  "+checksizeXXXL1);
			test.log(Status.INFO, MarkupHelper.createLabel("User selected size:  "+checksizeXXXL1, ExtentColor.BLUE));
			Thread.sleep(1000);
			steps.clickButton(mob.Mob_Addtocartfinal, driver, name);
			test.log(Status.INFO, MarkupHelper.createLabel("User added product to Cart", ExtentColor.BLUE));
			checkaddcart1 = getscreen.capture(driver, "checkaddcart1");
			test.addScreenCaptureFromPath(checkaddcart1);
			Thread.sleep(2000);
			try
			{
				String edit1 = driver.findElement(mob.Mob_CartEdit).getText();
				System.out.println(edit1);
				steps.clickButton(mob.Mob_CartEdit, driver, name);
				test.log(Status.INFO, MarkupHelper.createLabel("User clicked on Edit", ExtentColor.BLUE));
				steps.selectDropdown(mob.Mob_CartEditSize, driver, strDataFileName, "SelectSize", name);
				steps.ScrollUp(driver);
				Thread.sleep(1000);
				steps.selectDropdown(mob.Mob_CartEditQty, driver, strDataFileName, "Quantity", name);
				steps.ScrollUp(driver);
				test.log(Status.INFO, MarkupHelper.createLabel("User selected Size and Quantity", ExtentColor.BLUE));
				Thread.sleep(1000);
				steps.clickButton(mob.Mob_CartEditCancel, driver, name);
				steps.clickButton(mob.Mob_CartEdit, driver, name);
				steps.selectDropdown(mob.Mob_CartEditSize, driver, strDataFileName, "SelectSize", name);
				steps.ScrollUp(driver);
				Thread.sleep(1000);
				steps.selectDropdown(mob.Mob_CartEditQty, driver, strDataFileName, "Quantity", name);
				steps.ScrollUp(driver);
				Thread.sleep(1000);
				steps.clickButton(mob.Mob_CartEditSave, driver, name);
				checkupdateproduct1 = getscreen.capture(driver, "checkupdateproduct1");
				test.addScreenCaptureFromPath(checkupdateproduct1);
				test.log(Status.INFO, MarkupHelper.createLabel("User updated product size and quantity", ExtentColor.BLUE));
			}
			catch(Exception e)
			{
				System.out.println("Edit feature is not available for this product");
			}
			Thread.sleep(1000);
			steps.clickButton(mob.Mob_CartRemove, driver, name);
			checkremovecart1 = getscreen.capture(driver, "checkremovecart1");
			test.addScreenCaptureFromPath(checkremovecart1);
			test.log(Status.INFO, MarkupHelper.createLabel("User removed product from Cart", ExtentColor.BLUE));
			//checkout process
			steps.ScrollDown(driver);
			steps.clickButton(mob.Mob_SizeM, driver, name);
			checksizeM2 = driver.findElement(mob.Mob_SizeM).getText();
			System.out.println("Selected Size:  "+checksizeM2);
			test.log(Status.INFO, MarkupHelper.createLabel("User selected size:  "+checksizeM2, ExtentColor.BLUE));
			steps.clickButton(mob.Mob_BuyNow, driver, name);
			test.log(Status.INFO, MarkupHelper.createLabel("User clicked on BuyNow, navigated to checkout page", ExtentColor.BLUE));
			buynow1 = getscreen.capture(driver, "buynow1");
			test.addScreenCaptureFromPath(buynow1);
			Thread.sleep(2000);
			// Debit card option
			try
			{
				//steps.Scroll(driver, checkout.Debitcard);
				steps.ScrollDown(driver);
				String Debit = driver.findElement(checkout.Debitcard).getText();
				System.out.println("Payment method:  "+Debit);
				steps.Verifytext(checkout.Debitcard, driver, Debit, name);
				steps.ScrollDown(driver);
				steps.clickButton(checkout.Debitcard, driver, name);
				System.out.println("Debit card option is available");
				test.log(Status.INFO, MarkupHelper.createLabel("Payment method:  "+Debit   +    "      Available", ExtentColor.BLUE));
			}
			catch(Exception e)
			{
				System.out.println("Debit card option is not available");
				test.log(Status.INFO, MarkupHelper.createLabel("Debit card option is not available", ExtentColor.BLUE));
			}
			Thread.sleep(2000);
			// Credit card option
			try
			{
				steps.ScrollDown(driver);
				steps.MoveElement(mob.Mob_Creditcard, driver, name);
				String Credit = driver.findElement(mob.Mob_Creditcard).getText();
				System.out.println("Payment method:  "+Credit);
				steps.Verifytext(mob.Mob_Creditcard, driver, Credit, name);
				steps.ScrollDown(driver);
				steps.clickButton(mob.Mob_Creditcard, driver, name);
				System.out.println("Credit card option is available");
				test.log(Status.INFO, MarkupHelper.createLabel("Payment method:  "+Credit+    "Available", ExtentColor.BLUE));
			}
			catch(Exception e)
			{
				System.out.println("Credit card option is not available");
				test.log(Status.INFO, MarkupHelper.createLabel("Credit card option is not available", ExtentColor.BLUE));
			}
			Thread.sleep(2000);
			// Net Banking option
			try
			{
				String NetBanking = driver.findElement(mob.Mob_Netbanking).getText();
				System.out.println("Payment method:  "+NetBanking);
				steps.Verifytext(mob.Mob_Netbanking, driver, NetBanking, name);
				steps.clickButton(mob.Mob_Netbanking, driver, name);
				System.out.println("Net Banking option is available");
				test.log(Status.INFO, MarkupHelper.createLabel("Payment method:  "+NetBanking+    "Available", ExtentColor.BLUE));
			}
			catch(Exception e)
			{
				System.out.println("Net Banking option is not available");
				test.log(Status.INFO, MarkupHelper.createLabel("Net Banking option is not available", ExtentColor.BLUE));
			}
			Thread.sleep(2000);
			// Paytm option
			try
			{
				String Paytm = driver.findElement(mob.Mob_Paytm).getText();
				System.out.println("Payment method:  "+Paytm);
				steps.Verifytext(mob.Mob_Paytm, driver, Paytm, name);
				steps.clickButton(mob.Mob_Paytm, driver, name);
				System.out.println("Paytm option is available");
				test.log(Status.INFO, MarkupHelper.createLabel("Payment method:  "+Paytm+    "Available", ExtentColor.BLUE));
			}
			catch(Exception e)
			{
				System.out.println("Paytm option is not available");
				test.log(Status.INFO, MarkupHelper.createLabel("Paytm option is not available", ExtentColor.BLUE));
			}
			Thread.sleep(2000);
			// MDS Wallet option
			try
			{
				String MDS = driver.findElement(mob.Mob_MDSWallet).getText();
				System.out.println("Payment method:  "+MDS);
				steps.Verifytext(mob.Mob_MDSWallet, driver, MDS, name);
				steps.clickButton(mob.Mob_MDSWallet, driver, name);
				Thread.sleep(3000);
				String MDSpopup = driver.findElement(mob.Mob_MDSWalletPOPup).getText();
				System.out.println("Payment method:  "+MDSpopup);
				Thread.sleep(2000);
				steps.clickJSButton(mob.Mob_MDSWalletPOPupOK, driver, name);
				System.out.println("MDS Wallet option is available");
				test.log(Status.INFO, MarkupHelper.createLabel("Payment method:  "+MDS+    "Available", ExtentColor.BLUE));
			}
			catch(Exception e)
			{
				System.out.println("MDS Wallet option is not available");
				test.log(Status.INFO, MarkupHelper.createLabel("MDS Wallet option is not available", ExtentColor.BLUE));
			}
			Thread.sleep(2000);
			// Cash on delivery 
			try
			{
				steps.ScrollDown(driver);
				steps.MoveElement(mob.Mob_CashonDeliveryradio, driver, name);
				String cash = driver.findElement(mob.Mob_CashonDeliveryText).getText();
				System.out.println("Payment method:  "+cash);
				test.log(Status.INFO, MarkupHelper.createLabel("Payment method:  "+cash+    "Available", ExtentColor.BLUE));
				steps.Verifytext(mob.Mob_CashonDeliveryText, driver, cash, name);
				steps.MoveElement(mob.Mob_CashonDeliveryradio, driver, name);
				Thread.sleep(5000);
				steps.ScrollUp(driver);
				steps.clickJSButton(mob.Mob_CashonDeliveryradio, driver, name);
				buynow2 = driver.findElement(mob.Mob_PopUpSaveMsg).getText();
				System.out.println("Displayed message:   "+buynow2);
				test.log(Status.INFO, MarkupHelper.createLabel("Displayed message:   "+buynow2, ExtentColor.BLUE));
				Thread.sleep(1000);
				buynow3 = getscreen.capture(driver, "buynow3");
				test.addScreenCaptureFromPath(buynow3);
				steps.clickButton(mob.Mob_Cashondelivery, driver, name);
				test.log(Status.INFO, MarkupHelper.createLabel("User clicked on Cash on Delivery link", ExtentColor.BLUE));
			}
			catch(Exception e)
			{
				System.out.println("Cash on Delivery is not available");
				test.log(Status.INFO, MarkupHelper.createLabel("Cash on Delivery is not available", ExtentColor.BLUE));
			}
			Thread.sleep(2000);
			String checkout1 = getscreen.capture(driver, "checkout1");
			test.addScreenCaptureFromPath(checkout1);
			steps.clickButton(mob.Mob_Placeorder, driver, name);
			Thread.sleep(2000);
			String Placeorder1 = getscreen.capture(driver, "Placeorder1");
			test.addScreenCaptureFromPath(Placeorder1);
			steps.clickButton(mob.Mob_CartIcon, driver, name);
			steps.clickButton(mob.Mob_CartRemove, driver, name);
			Thread.sleep(1000);
			commonmethods.MobileLogout(driver);
			Thread.sleep(1000);
			LoginUserstatus = passstatus;
			Assert.assertTrue(true);
			System.out.println("Mobile Login User CheckOut Successful");
			testresultdata.put(Integer.toString(cnt), new Object[] {name, "Mobile Login User CheckOut", "Mobile Login User CheckOut Successful",passstatus});
		}
		catch(Exception e)
		{
			System.out.println("Mobile Login User CheckOut Unsuccessful");
			testresultdata.put(Integer.toString(cnt), new Object[] {name, "Mobile Login User CheckOut", "Mobile Login User CheckOut Unsuccessful",failstatus});
			LoginUserstatus = failstatus;
			LoginUserstatusfail = getscreen.capture(driver, "LoginUserstatusfail");
			test.addScreenCaptureFromPath(LoginUserstatusfail);
			driver.close();
			Assert.assertTrue(false);		
		}
	}
	//@Test(priority=1)
	public void MobileGuestUser() throws Exception
	{
		String name = new Object(){}.getClass().getEnclosingMethod().getName();
		cnt ++;
		try
		{
			test = extent.createTest("Mobile Guest User CheckOut");
			System.out.println("Mobile Guest User CheckOut Test Case Executing...");
			/*	commonmethods.Login(driver);
			test.log(Status.INFO, MarkupHelper.createLabel("User able to Login", ExtentColor.BLUE));
			Thread.sleep(1000);*/
			Thread.sleep(2000);
			commonmethods.URLNaviagtion(driver);
			//steps.OpenUrl(driver, strDataFileName, "URL", name);
			Thread.sleep(5000);
			String Parent = driver.getWindowHandle();
			steps.clickJSButton(mob.Mob_Mydreamstore1, driver, name);
			System.out.println(driver.findElement(mob.Mob_Mydreamstore1).getText());
			Thread.sleep(2000);
			steps.clickButton(mob.Mob_Image1, driver, name);
			steps.ChildWindow(driver);
			//	driver.manage().window().maximize();
			check7 = getscreen.capture(driver, "check7");
			test.addScreenCaptureFromPath(check7);
			Thread.sleep(1000);
			try
			{
				String Filterpage2 = driver.findElement(home.Filterpagetext).getText();
				System.out.println("User navigated to :   "+Filterpage2);
			}
			catch(Exception e)
			{
				steps.clickButton(mob.Mob_Image2, driver, name);
				System.out.println("User navigated to different landing page");
				driver.close();
				steps.ChildWindow(driver);
				Thread.sleep(1000);
			}
			steps.clickButton(mob.Mob_ImageSlide, driver, name);
			String check25 = getscreen.capture(driver, "check25");
			test.addScreenCaptureFromPath(check25);
			steps.ScrollDown(driver);
			Thread.sleep(2000);
			try
			{
				steps.clickButton(mob.Mob_Readmore, driver, name);
				test.log(Status.INFO, MarkupHelper.createLabel("User clicked on Readmore", ExtentColor.BLUE));
				check8 = getscreen.capture(driver, "check8");
				test.addScreenCaptureFromPath(check8);
				check12 = driver.findElement(home.Trend_Image_Description).getText();
				System.out.println("Item Description   "+check12);
				test.log(Status.INFO, MarkupHelper.createLabel("Description about Item:  "+check12, ExtentColor.BLUE));
				steps.clickButton(mob.Mob_Readless, driver, name);
			}
			catch(Exception e)
			{
				steps.clickButton(mob.Mob_Readmore, driver, name);
				test.log(Status.INFO, MarkupHelper.createLabel("User clicked on Readmore", ExtentColor.BLUE));
				check9 = getscreen.capture(driver, "check9");
				test.addScreenCaptureFromPath(check9);
				check10 = driver.findElement(home.Trend_Image_Description1).getText();
				System.out.println("Item Description   "+check10);
				test.log(Status.INFO, MarkupHelper.createLabel("Description about Item:  "+check10, ExtentColor.BLUE));
				steps.clickButton(mob.Mob_Readless, driver, name);
			}
			Thread.sleep(1000);
			check11 = driver.findElement(mob.Mob_MRP).getText();
			System.out.println("Item Price   "+check11);
			test.log(Status.INFO, MarkupHelper.createLabel("MRP of an Item:  "+check11, ExtentColor.ORANGE));
			try
			{
				String gcol1 = driver.findElement(mob.Mob_color1).getCssValue("color");
				System.out.println(gcol1);
				steps.clickButton(mob.Mob_color1, driver, name);
				Thread.sleep(2000);
				String gcheckgColor1 = driver.findElement(mob.Mob_color1).getCssValue("background-color");
				String gcheckghex1 = Color.fromString(gcheckgColor1).asHex();
				System.out.println("Image color: "+gcheckghex1);
				test.log(Status.INFO, MarkupHelper.createLabel("User changed item color to:  "+gcheckghex1, ExtentColor.BLUE));
				String gcheckgFirstcolor1 = getscreen.capture(driver, "gcheckgFirstcolor1");
				test.addScreenCaptureFromPath(gcheckgFirstcolor1);
			}
			catch(Exception e)
			{
				System.out.println("Color 2 is not exists");
			}
			try
			{
				String gcol2 = driver.findElement(mob.Mob_color5).getCssValue("color");
				System.out.println(gcol2);
				steps.clickButton(mob.Mob_color5, driver, name);
				Thread.sleep(2000);
				String gcheckgcolor5 = driver.findElement(mob.Mob_color5).getCssValue("background-color");
				String gcheckghex5 = Color.fromString(gcheckgcolor5).asHex();
				System.out.println("Image color: "+gcheckghex5);
				test.log(Status.INFO, MarkupHelper.createLabel("User changed item color to:  "+checkghex5, ExtentColor.BLUE));
				checkgFifthcolor1 = getscreen.capture(driver, "checkgFifthcolor1");
				test.addScreenCaptureFromPath(checkgFifthcolor1);
			}
			catch(Exception e)
			{
				System.out.println("Color 1 is not exists");
			}
			Thread.sleep(1000);
			steps.clickButton(mob.Mob_Findyoursize, driver, name);
			test.log(Status.INFO, MarkupHelper.createLabel("User clicked on Find your size", ExtentColor.BLUE));
			check24 = getscreen.capture(driver, "check24");
			test.addScreenCaptureFromPath(check24);
			steps.clickButton(mob.Mob_Findyoursizeclose, driver, name);
			Thread.sleep(2000);
			steps.clickButton(mob.Mob_Addtocart, driver, name);
			String check26 = getscreen.capture(driver, "check26");
			test.addScreenCaptureFromPath(check26);
			Thread.sleep(1000);
			steps.ScrollDown(driver);
			steps.clickJSButton(mob.Mob_SizeS, driver, name);
			String gcheckgsizeS1 = driver.findElement(mob.Mob_SizeS).getText();
			System.out.println("Selected Size:  "+gcheckgsizeS1);
			test.log(Status.INFO, MarkupHelper.createLabel("User selected size:  "+gcheckgsizeS1, ExtentColor.BLUE));
			steps.clickButton(mob.Mob_SizeM, driver, name);
			String gcheckgsizeM1 = driver.findElement(mob.Mob_SizeM).getText();
			System.out.println("Selected Size:  "+gcheckgsizeM1);
			test.log(Status.INFO, MarkupHelper.createLabel("User selected size:  "+gcheckgsizeM1, ExtentColor.BLUE));
			steps.clickButton(mob.Mob_SizeL, driver, name);
			String gcheckgsizeL1 = driver.findElement(mob.Mob_SizeL).getText();
			System.out.println("Selected Size:  "+gcheckgsizeL1);
			test.log(Status.INFO, MarkupHelper.createLabel("User selected size:  "+gcheckgsizeL1, ExtentColor.BLUE));
			steps.clickButton(mob.Mob_SizeXL, driver, name);
			String gcheckgsizeXL1 = driver.findElement(mob.Mob_SizeXL).getText();
			System.out.println("Selected Size:  "+gcheckgsizeXL1);
			test.log(Status.INFO, MarkupHelper.createLabel("User selected size:  "+gcheckgsizeXL1, ExtentColor.BLUE));
			steps.clickButton(mob.Mob_SizeXXL, driver, name);
			String gcheckgsizeXXL1 = driver.findElement(mob.Mob_SizeXXL).getText();
			System.out.println("Selected Size:  "+gcheckgsizeXXL1);
			test.log(Status.INFO, MarkupHelper.createLabel("User selected size:  "+gcheckgsizeXXL1, ExtentColor.BLUE));
			steps.clickButton(mob.Mob_SizeXXXL, driver, name);
			String gcheckgsizeXXXL1 = driver.findElement(mob.Mob_SizeXXXL).getText();
			System.out.println("Selected Size:  "+gcheckgsizeXXXL1);
			test.log(Status.INFO, MarkupHelper.createLabel("User selected size:  "+gcheckgsizeXXXL1, ExtentColor.BLUE));
			Thread.sleep(1000);
			steps.clickButton(mob.Mob_Addtocartfinal, driver, name);
			test.log(Status.INFO, MarkupHelper.createLabel("User added product to Cart", ExtentColor.BLUE));
			String gcheckaddcart1 = getscreen.capture(driver, "gcheckaddcart1");
			test.addScreenCaptureFromPath(gcheckaddcart1);
			Thread.sleep(2000);
			try
			{
				String edit1 = driver.findElement(mob.Mob_CartEdit).getText();
				System.out.println(edit1);
				steps.clickButton(mob.Mob_CartEdit, driver, name);
				test.log(Status.INFO, MarkupHelper.createLabel("User clicked on Edit", ExtentColor.BLUE));
				steps.selectDropdown(mob.Mob_CartEditSize, driver, strDataFileName, "SelectSize", name);
				steps.ScrollUp(driver);
				Thread.sleep(1000);
				steps.selectDropdown(mob.Mob_CartEditQty, driver, strDataFileName, "Quantity", name);
				steps.ScrollUp(driver);
				test.log(Status.INFO, MarkupHelper.createLabel("User selected Size and Quantity", ExtentColor.BLUE));
				Thread.sleep(1000);
				steps.clickButton(mob.Mob_CartEditCancel, driver, name);
				steps.clickButton(mob.Mob_CartEdit, driver, name);
				steps.selectDropdown(mob.Mob_CartEditSize, driver, strDataFileName, "SelectSize", name);
				steps.ScrollUp(driver);
				Thread.sleep(1000);
				steps.selectDropdown(mob.Mob_CartEditQty, driver, strDataFileName, "Quantity", name);
				steps.ScrollUp(driver);
				Thread.sleep(1000);
				steps.clickButton(mob.Mob_CartEditSave, driver, name);
				String gcheckgupdateproduct1 = getscreen.capture(driver, "gcheckgupdateproduct1");
				test.addScreenCaptureFromPath(gcheckgupdateproduct1);
				test.log(Status.INFO, MarkupHelper.createLabel("User updated product size and quantity", ExtentColor.BLUE));
			}
			catch(Exception e)
			{
				System.out.println("Edit feature is not available for this product");
			}
			Thread.sleep(1000);
			steps.clickButton(mob.Mob_CartRemove, driver, name);
			String gcheckgremovecart1 = getscreen.capture(driver, "gcheckgremovecart1");
			test.addScreenCaptureFromPath(gcheckgremovecart1);
			test.log(Status.INFO, MarkupHelper.createLabel("User removed product from Cart", ExtentColor.BLUE));
			//checkout process
			steps.ScrollDown(driver);
			steps.clickButton(mob.Mob_SizeM, driver, name);
			String gchecksizeM2 = driver.findElement(mob.Mob_SizeM).getText();
			System.out.println("Selected Size:  "+gchecksizeM2);
			test.log(Status.INFO, MarkupHelper.createLabel("User selected size:  "+gchecksizeM2, ExtentColor.BLUE));
			steps.clickButton(mob.Mob_BuyNow, driver, name);
			test.log(Status.INFO, MarkupHelper.createLabel("User clicked on BuyNow, navigated to checkout page", ExtentColor.BLUE));
			String gbuynow1 = getscreen.capture(driver, "gbuynow1");
			test.addScreenCaptureFromPath(gbuynow1);
			Thread.sleep(2000);
			// Debit card option
			try
			{
				steps.ScrollDown(driver);
				String Debit = driver.findElement(checkout.Debitcard).getText();
				System.out.println("Payment method:  "+Debit);
				steps.Verifytext(mob.Mob_Debitcard, driver, Debit, name);
				steps.ScrollDown(driver);
				steps.MoveElement(mob.Mob_Debitcard, driver, name);
				Thread.sleep(5000);
				steps.ScrollUp(driver);
				steps.clickButton(mob.Mob_Debitcard, driver, name);
				System.out.println("Debit card option is available");
				test.log(Status.INFO, MarkupHelper.createLabel("Payment method:  "+Debit   +    "      Available", ExtentColor.BLUE));
				Debitg = getscreen.capture(driver, "Debitg");
				test.addScreenCaptureFromPath(Debitg);
			}
			catch(Exception e)
			{
				System.out.println("Debit card option is not available");
				test.log(Status.INFO, MarkupHelper.createLabel("Debit card option is not available", ExtentColor.BLUE));
			}
			Thread.sleep(2000);
			// Credit card option
			try
			{
				steps.ScrollDown(driver);
				steps.MoveElement(mob.Mob_Creditcard, driver, name);
				String Credit = driver.findElement(mob.Mob_Creditcard).getText();
				System.out.println("Payment method:  "+Credit);
				steps.Verifytext(mob.Mob_Creditcard, driver, Credit, name);
				steps.ScrollDown(driver);
				steps.MoveElement(mob.Mob_Creditcard, driver, name);
				Thread.sleep(5000);
				steps.ScrollUp(driver);
				steps.clickJSButton(mob.Mob_Creditcard, driver, name);
				System.out.println("Credit card option is available");
				test.log(Status.INFO, MarkupHelper.createLabel("Payment method:  "+Credit+    "Available", ExtentColor.BLUE));
				Creditg = getscreen.capture(driver, "Creditg");
				test.addScreenCaptureFromPath(Creditg);
			}
			catch(Exception e)
			{
				System.out.println("Credit card option is not available");
				test.log(Status.INFO, MarkupHelper.createLabel("Credit card option is not available", ExtentColor.BLUE));
			}
			Thread.sleep(2000);
			// Net Banking option
			try
			{
				String NetBanking = driver.findElement(mob.Mob_Netbanking).getText();
				System.out.println("Payment method:  "+NetBanking);
				steps.Verifytext(mob.Mob_Netbanking, driver, NetBanking, name);
				steps.MoveElement(mob.Mob_Netbanking, driver, name);
				Thread.sleep(5000);
				steps.ScrollUp(driver);
				steps.clickJSButton(mob.Mob_Netbanking, driver, name);
				System.out.println("Net Banking option is available");
				test.log(Status.INFO, MarkupHelper.createLabel("Payment method:  "+NetBanking+    "Available", ExtentColor.BLUE));
				NetBankingg = getscreen.capture(driver, "NetBankingg");
				test.addScreenCaptureFromPath(NetBankingg);
			}
			catch(Exception e)
			{
				System.out.println("Net Banking option is not available");
				test.log(Status.INFO, MarkupHelper.createLabel("Net Banking option is not available", ExtentColor.BLUE));
			}
			Thread.sleep(2000);
			// Paytm option
			try
			{
				String Paytm = driver.findElement(mob.Mob_Paytm).getText();
				System.out.println("Payment method:  "+Paytm);
				steps.Verifytext(mob.Mob_Paytm, driver, Paytm, name);
				steps.MoveElement(mob.Mob_Paytm, driver, name);
				Thread.sleep(5000);
				steps.ScrollUp(driver);
				steps.clickJSButton(mob.Mob_Paytm, driver, name);
				System.out.println("Paytm option is available");
				test.log(Status.INFO, MarkupHelper.createLabel("Payment method:  "+Paytm+    "Available", ExtentColor.BLUE));
				Paytmg = getscreen.capture(driver, "Paytmg");
				test.addScreenCaptureFromPath(Paytmg);
			}
			catch(Exception e)
			{
				System.out.println("Paytm option is not available");
				test.log(Status.INFO, MarkupHelper.createLabel("Paytm option is not available", ExtentColor.BLUE));
			}
			Thread.sleep(2000);
			// Cash on delivery 
			try
			{
				steps.ScrollDown(driver);
				steps.MoveElement(mob.Mob_CashonDeliveryradio, driver, name);
				String gcash = driver.findElement(mob.Mob_CashonDeliveryText).getText();
				System.out.println("Payment method:  "+gcash);
				test.log(Status.INFO, MarkupHelper.createLabel("Payment method:  "+gcash+    "Available", ExtentColor.BLUE));
				steps.Verifytext(mob.Mob_CashonDeliveryText, driver, gcash, name);
				steps.MoveElement(mob.Mob_CashonDeliveryradio, driver, name);
				Thread.sleep(5000);
				steps.ScrollUp(driver);
				steps.clickJSButton(mob.Mob_CashonDeliveryradio, driver, name);
				String gbuynow2 = driver.findElement(mob.Mob_PopUpSaveMsg).getText();
				System.out.println("Displayed message:   "+gbuynow2);
				test.log(Status.INFO, MarkupHelper.createLabel("Displayed message:   "+gbuynow2, ExtentColor.BLUE));
				Thread.sleep(1000);
				String gbuynow3 = getscreen.capture(driver, "gbuynow3");
				test.addScreenCaptureFromPath(gbuynow3);
				steps.clickButton(mob.Mob_Cashondelivery, driver, name);
				test.log(Status.INFO, MarkupHelper.createLabel("User clicked on Cash on Delivery link", ExtentColor.BLUE));
			}
			catch(Exception e)
			{
				System.out.println("Cash on Delivery is not available");
				test.log(Status.INFO, MarkupHelper.createLabel("Cash on Delivery is not available", ExtentColor.BLUE));
			}
			Thread.sleep(2000);
			String gcheckout1 = getscreen.capture(driver, "gcheckout1");
			test.addScreenCaptureFromPath(gcheckout1);
			steps.clickButton(mob.Mob_Placeorder, driver, name);
			Thread.sleep(2000);
			try
			{
				check14 = driver.findElement(mob.Mob_FirstNameerror).getText();
				System.out.println(check14);
				test.log(Status.INFO, MarkupHelper.createLabel("Error message:   "+check14, ExtentColor.RED));
				steps.EnterText(mob.Mob_FirstName, driver, strDataFileName, "Firstname", name);
				steps.clickButton(mob.Mob_Placeorder, driver, name);
			}
			catch(Exception e )
			{
				System.out.println("Firstname error message is not displayed");
			}
			Thread.sleep(1000);
			try
			{
				check16 = driver.findElement(mob.Mob_EmailError).getText();
				System.out.println(check16);
				test.log(Status.INFO, MarkupHelper.createLabel("Error message:   "+check16, ExtentColor.RED));
				steps.EnterText(mob.Mob_Email, driver, strDataFileName, "Email", name);
				steps.clickButton(mob.Mob_Placeorder, driver, name);
			}
			catch(Exception e )
			{
				System.out.println("Email error message is not displayed");
			}
			Thread.sleep(1000);
			try
			{
				check15 = driver.findElement(mob.Mob_MobileNumberError1).getText();
				System.out.println(check15);
				test.log(Status.INFO, MarkupHelper.createLabel("Error message:   "+check15, ExtentColor.RED));
				steps.EnterText(mob.Mob_MobileNumber, driver, strDataFileName, "Mobile", name);
				steps.clickButton(mob.Mob_Placeorder, driver, name);
			}
			catch(Exception e )
			{
				System.out.println("Mobile Number error message is not displayed");
			}
			Thread.sleep(1000);
			try
			{
				check17 = driver.findElement(mob.Mob_ZipcodeError1).getText();
				System.out.println(check17);
				test.log(Status.INFO, MarkupHelper.createLabel("Error message:   "+check17, ExtentColor.RED));
				steps.EnterText(mob.Mob_Zipcode, driver, strDataFileName, "Zipcode", name);
				steps.clickButton(mob.Mob_Placeorder, driver, name);
			}
			catch(Exception e )
			{
				System.out.println("Zipcode error message is not displayed");
			}
			Thread.sleep(1000);
			try
			{
				check18 = driver.findElement(mob.Mob_AddressError1).getText();
				System.out.println(check18);
				test.log(Status.INFO, MarkupHelper.createLabel("Error message:   "+check18, ExtentColor.RED));
				steps.EnterText(mob.Mob_Address, driver, strDataFileName, "Address", name);
				steps.clickButton(mob.Mob_Placeorder, driver, name);
			}
			catch(Exception e )
			{
				System.out.println("Address 1  error message is not displayed");
			}
			Thread.sleep(1000);
			try
			{
				check19 = driver.findElement(mob.Mob_Address2Error1).getText();
				System.out.println(check19);
				test.log(Status.INFO, MarkupHelper.createLabel("Error message:   "+check19, ExtentColor.RED));
				steps.EnterText(mob.Mob_Address2, driver, strDataFileName, "Address2", name);
				steps.clickButton(mob.Mob_Placeorder, driver, name);
			}
			catch(Exception e )
			{
				System.out.println("Address 2  error message is not displayed");
			}
/*			try
			{
				check20 = driver.findElement(mob.Mob_CityError).getText();
				System.out.println(check20);
				test.log(Status.INFO, MarkupHelper.createLabel("Error message:   "+check20, ExtentColor.RED));
				steps.clickButton(mob.Mob_Placeorder, driver, name);
			}
			catch(Exception e )
			{
				System.out.println("City  error message is not displayed");
			}
			try
			{
				check21 = driver.findElement(mob.Mob_StateError).getText();
				System.out.println(check21);
				test.log(Status.INFO, MarkupHelper.createLabel("Error message:   "+check21, ExtentColor.RED));
				steps.clickButton(mob.Mob_Placeorder, driver, name);
			}
			catch(Exception e )
			{
				System.out.println("State  error message is not displayed");
			}
*/
			Thread.sleep(1000);
			String checkout2 = getscreen.capture(driver, "checkout2");
			test.addScreenCaptureFromPath(checkout2);
			steps.clickButton(mob.Mob_CartIcon, driver, name);
			steps.clickButton(home.Trend_Addcart_remove, driver, name);
			cartg = getscreen.capture(driver, "cartg");
			test.addScreenCaptureFromPath(cartg);
			Thread.sleep(1000);
			GuestUserstatus = passstatus;
			Assert.assertTrue(true);
			System.out.println("Guest User CheckOut Successful");
			testresultdata.put(Integer.toString(cnt), new Object[] {name, "Guest User CheckOut", "Guest User CheckOut Successful",passstatus});
		}
		catch(Exception e)
		{
			System.out.println("Guest User CheckOut Unsuccessful");
			testresultdata.put(Integer.toString(cnt), new Object[] {name, "Guest User CheckOut", "Guest User CheckOut Unsuccessful",failstatus});
			GuestUserstatus = failstatus;
			GuestUserstatusfail = getscreen.capture(driver, "GuestUserstatusfail");
			test.addScreenCaptureFromPath(GuestUserstatusfail);
			driver.close();
			Assert.assertTrue(false);		
		}
	}
	@Test(priority=2)
	public void simpletest() throws Exception
	{
		commonmethods.URLNaviagtion(driver);
		Thread.sleep(2000);
		driver.findElement(mob.Mob_Mydreamstore1).click();
		System.out.println("clicked on "+driver.findElement(mob.Mob_Mydreamstore1).getText());
		driver.findElement(mob.Mob_Image1).click();
	}
	@BeforeTest
	public void bef() throws MalformedURLException
	{
		ChromeOptions options = new ChromeOptions();
	    Map<String, Object> prefs = new HashMap<String, Object>();
	    Map<String, Object> profile = new HashMap<String, Object>();
	    Map<String, Object> contentSettings = new HashMap<String, Object>();
	    contentSettings.put("notifications", 2);
	    profile.put("managed_default_content_settings", contentSettings);
	    prefs.put("profile", profile);
	    options.setExperimentalOption("prefs", prefs);
	    options.addArguments("--disable-plugins");
	    options.addArguments("--start-maximized");
	    DesiredCapabilities caps = DesiredCapabilities.chrome();
	    caps.setCapability("browserName", "android");
	    caps.setCapability("device", "Samsung Galaxy S8");
	    caps.setCapability("realMobile", "true");
	    caps.setCapability("os_version", "7.0");
	    caps.setCapability("browserstack.debug", "true");
	    caps.setCapability(ChromeOptions.CAPABILITY, options);
	    driver = new RemoteWebDriver(new URL(URL), caps);
	}
	@BeforeClass
	public void before()
	{
		workbook = new HSSFWorkbook();
		sheet = workbook.createSheet(strDataFileName);
		testresultdata = new LinkedHashMap<String, Object[]>();
		testresultdata.put("1", new Object[] { "Test Step Name", "Expected Result","Actual Result" ,"Status"});
	}
	@AfterClass
	public void after() throws Exception
	{
		Set<String> keyset = testresultdata.keySet();
		int rownum = 0;
		for (String key : keyset) {
			Row row = sheet.createRow(rownum++);
			Object [] objArr = testresultdata.get(key);
			int cellnum = 0;
			for (Object obj : objArr) {
				Cell cell = row.createCell(cellnum++);
				if(obj instanceof Date) 
					cell.setCellValue((Date)obj);
				else if(obj instanceof Boolean)
					cell.setCellValue((Boolean)obj);
				else if(obj instanceof String)
					cell.setCellValue((String)obj);
				else if(obj instanceof Double)
					cell.setCellValue((Double)obj);
			}
		}
		try {
			FileOutputStream out = new FileOutputStream(new File("TestResults/"+strDataFileName +".xls"));
			workbook.write(out);
			out.close();
			System.out.println("Excel written successfully..");
			TestResult();
			driver.quit();
		}
		catch (FileNotFoundException e) 
		{
			e.printStackTrace();
		} 
		catch (IOException e) 
		{
			e.printStackTrace();
		}
	}
	public void TestResult() throws Exception
	{
		HSSFRow row;
		HSSFCell cell;
		WritableFont cellFont;
		WritableCellFormat cellFormat;
		Border border = null;
		int count;
		FileInputStream inp = new FileInputStream( "TestResults/"+strDataFileName +".xls");
		HSSFWorkbook wb = new HSSFWorkbook(inp);
		HSSFSheet sh = wb.getSheetAt(0);
		count=sh.getLastRowNum();
		for (int i = 1; i <= count; i++) 
		{
			row=sh.getRow(i);
			cell=row.getCell(3);
			String status=row.getCell(3).getStringCellValue();
			if(status.equalsIgnoreCase("Pass"))
			{
				HSSFCellStyle pass= wb.createCellStyle();
				pass.setFillForegroundColor(IndexedColors.GREEN.getIndex());
				pass.setFillPattern(CellStyle.SOLID_FOREGROUND);
				cell.setCellStyle(pass);
			}
			else
			{
				HSSFCellStyle fail= wb.createCellStyle();
				fail.setFillForegroundColor(IndexedColors.RED.getIndex());
				fail.setFillPattern(CellStyle.SOLID_FOREGROUND);
				cell.setCellStyle(fail);
			}
		}
		FileOutputStream out = new FileOutputStream("TestResults/"+strDataFileName+".xls");
		wb.write(out);
		wb.close();
	}
}
