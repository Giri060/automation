package Com.Mobile;
import java.net.URL;
import org.openqa.selenium.By;
import org.openqa.selenium.remote.DesiredCapabilities;
import org.testng.annotations.Test;
import io.appium.java_client.android.AndroidDriver;
public class AndroidMobile {
	public static final String USERNAME = "raj964";
	public static final String AUTOMATE_KEY = "qesBpyNzdyQGs3wK7ekp";
	public static final String URL = "https://" + USERNAME + ":" + AUTOMATE_KEY + "@hub-cloud.browserstack.com/wd/hub";
	@Test
	public  void Browser() throws Exception
	{
	DesiredCapabilities caps = new DesiredCapabilities();
	caps.setCapability("os_version", "6.0");
	caps.setCapability("device", "Google Nexus 6");
	caps.setCapability("realMobile", "true");
	caps.setCapability("browserstack.debug", "true");
	//caps.setCapability("deviceOrientation", "landscape");
	//WebDriver driver = new RemoteWebDriver(new URL(URL), caps);
	AndroidDriver driver = new AndroidDriver(new URL(URL), caps);
	driver.get("https://mydreamstore.in/");
	System.out.println(driver.getTitle());
    Thread.sleep(5000);
    driver.getContext();
    driver.context("NATIVE_APP");
    driver.findElement(By.xpath("//*[@resource-id='android:id/button1']")).click();
    driver.context("CHROMIUM");
	driver.findElement(By.xpath("//img[contains(@src,'https://mydreamstore.in/static/images/logo_mds.png')]")).click();
    Thread.sleep(5000);
    driver.getContext();
    System.out.println("successful");
    driver.quit();
	}
}
