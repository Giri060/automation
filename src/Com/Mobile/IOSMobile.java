package Com.Mobile;
import java.net.URL;
import java.util.Set;
import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.remote.DesiredCapabilities;
import org.testng.annotations.Test;
import io.appium.java_client.android.AndroidDriver;
import io.appium.java_client.ios.IOSDriver;
import io.appium.java_client.ios.IOSElement;
public class IOSMobile {
	WebDriver driver;
	public static final String USERNAME = "raj964";
	public static final String AUTOMATE_KEY = "qesBpyNzdyQGs3wK7ekp";
	public static final String URL = "https://" + USERNAME + ":" + AUTOMATE_KEY + "@hub-cloud.browserstack.com/wd/hub";
	@Test
	public  void Browser() throws Exception
	{
		DesiredCapabilities caps = new DesiredCapabilities();
		caps.setCapability("os_version", "11.2");
		caps.setCapability("device", "iPhone SE");
		caps.setCapability("real_mobile", "true");
		caps.setCapability("browserstack.local", "false");
		//caps.setCapability("deviceOrientation", "landscape");
		//WebDriver driver = new RemoteWebDriver(new URL(URL), caps);
		//AndroidDriver driver = new AndroidDriver(new URL(URL), caps);
		driver.get("https://mydreamstore.in/");
	IOSDriver<IOSElement> driver = new IOSDriver<IOSElement>(new URL(URL), caps);
		System.out.println(driver.getTitle());
	    Thread.sleep(5000);
	    driver.getContext();
	        Set<String> handles=driver.getContextHandles();
	        for(String handle: handles) {
	        	if(handle.contains("Block")) {	
	        		driver.context(handle);
	        	}
	        }
	       System.out.println(driver.getPageSource());  // in order to verify the element is present
	        Thread.sleep(5000);
	        driver.getContext(); //to verify if the context switch has occurred.
	    driver.context("NATIVE_APP");
	    driver.findElement(By.xpath("//*[@resource-id='android:id/button1']")).click();
	    driver.context("CHROMIUM");
		driver.findElement(By.xpath("//img[contains(@src,'https://mydreamstore.in/static/images/logo_mds.png')]")).click();
	    Thread.sleep(5000);
	    driver.getContext();
	    System.out.println("successful");
	    driver.quit();
		}
}
