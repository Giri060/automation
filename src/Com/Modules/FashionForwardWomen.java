package Com.Modules;
import java.io.File;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.IOException;
import java.util.Date;
import java.util.LinkedHashMap;
import java.util.Map;
import java.util.Set;
import org.apache.poi.hssf.usermodel.HSSFCell;
import org.apache.poi.hssf.usermodel.HSSFCellStyle;
import org.apache.poi.hssf.usermodel.HSSFRow;
import org.apache.poi.hssf.usermodel.HSSFSheet;
import org.apache.poi.hssf.usermodel.HSSFWorkbook;
import org.apache.poi.ss.usermodel.Cell;
import org.apache.poi.ss.usermodel.CellStyle;
import org.apache.poi.ss.usermodel.IndexedColors;
import org.apache.poi.ss.usermodel.Row;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.support.Color;
import org.testng.Assert;
import org.testng.annotations.AfterClass;
import org.testng.annotations.BeforeClass;
import org.testng.annotations.Test;
import com.aventstack.extentreports.Status;
import com.aventstack.extentreports.markuputils.ExtentColor;
import com.aventstack.extentreports.markuputils.MarkupHelper;
import Com.ObjectRepository.Checkout_OR;
import Com.ObjectRepository.FashionForwardMan_OR;
import Com.ObjectRepository.FashionForwardWomen_OR;
import Com.ObjectRepository.Homepage2_OR;
import Com.ObjectRepository.Homepage_OR;
import Com.ObjectRepository.Login_OR;
import Com.ObjectRepository.Tshirts_OR;
import Com.Utilities.Browsers;
import Com.Utilities.CommonMethods;
import Com.Utilities.GetScreenShot;
import Com.Utilities.Screenshot;
import Com.Utilities.Steps;
import Com.Utilities.TestBase;
import jxl.format.Border;
import jxl.write.WritableCellFormat;
import jxl.write.WritableFont;
public class FashionForwardWomen extends TestBase
{
	WebDriver driver;
	Login_OR login = new Login_OR();
	Steps steps = new Steps();
	GetScreenShot getscreen = new GetScreenShot();
	Screenshot screen = new Screenshot();
	public String strAbsolutepath = new File("").getAbsolutePath();
	HSSFWorkbook workbook;
	HSSFSheet sheet;
	String strDataFileName = this.getClass().getSimpleName();
	Map<String, Object[]> testresultdata; 
	CommonMethods commonmethods = new CommonMethods();
	Homepage_OR home = new Homepage_OR();
	Homepage2_OR home2 = new Homepage2_OR();
	FashionForwardWomen_OR FFW = new FashionForwardWomen_OR();
	Checkout_OR checkout = new Checkout_OR();
	Screenshot screenshot = new Screenshot();
	String passstatus = screenshot.status("PASS");
	String failstatus = screenshot.status("FAIL");
	int cnt = 2;
	public static String FFWstatus = "";
	public static String FFWstatusfail = "";
	public static String FFWDescription1 = "";
	public static String FFWDescription2 = "";
	public static String FFWDescription3 = "";
	public static String FFWDescription4 = "";
	public static String FFWMRP = "";
	public static String FFWColor12 = "";
	public static String FFWhex12 = "";
	public static String FFWFirstcolor2 = "";
	public static String FFWColor22 = "";
	public static String FFWhex22 = "";
	public static String FFWSecondcolor2 = "";
	public static String FFWColor32 = "";
	public static String FFWhex32 = "";
	public static String FFWThirdcolor2 = "";
	public static String FFWColor42 = "";
	public static String FFWhex42 = "";
	public static String FFWFourthcolor2 = "";
	public static String FFWColor13 = "";
	public static String FFWhex13 = "";
	public static String FFWFirstcolor3 = "";
	public static String FFWColor23 = "";
	public static String FFWhex23 = "";
	public static String FFWSecondcolor3 = "";
	public static String FFWColor33 = "";
	public static String FFWhex33 = "";
	public static String FFWThirdcolor3 = "";
	public static String FFWColor43 = "";
	public static String FFWhex43 = "";
	public static String FFWFourthcolor3 = "";
	public static String FFWColor11 = "";
	public static String FFWhex11 = "";
	public static String FFWFirstcolor1 = "";
	public static String FFWColor21 = "";
	public static String FFWhex21 = "";
	public static String FFWSecondcolor1 = "";
	public static String FFWColor31 = "";
	public static String FFWhex31 = "";
	public static String FFWThirdcolor1 = "";
	public static String FFWColor41 = "";
	public static String FFWhex41 = "";
	public static String FFWFourthcolor1 = "";
	public static String FFWcqty = "";
	public static String FFWiqty = "";
	public static String FFWdqty = "";
	public static String FFWSizemsg = "";
	public static String FFWsizeS = "";
	public static String FFWsizeM = "";
	public static String FFWsizeL = "";
	public static String FFWsizeXL = "";
	public static String FFWsizeXXL = "";
	public static String FFWsizeXXXL = "";
	public static String FFWbelow510 = "";
	public static String FFWabove510 = "";
	public static String FFWtightfit = "";
	public static String FFWcomfit = "";
	public static String FFWfindursize = "";
	public static String FFWaddcart = "";
	public static String FFWupdateproduct1 = "";
	public static String FFWremovecart = "";
	public FashionForwardWomen()
	{
		Browsers b = new Browsers();
		this.driver = b.getBrowsers("chrome1");		 
	}
	@Test
	public void FashionForwardWomenCheckout() throws Exception
	{
		String name = new Object(){}.getClass().getEnclosingMethod().getName();
		cnt ++;
		try 
		{
			test = extent.createTest("FashionForwardWomen Module");
			System.out.println("FashionForwardWomen Module Test Case Executing...");
			commonmethods.Login(driver);
			test.log(Status.INFO, MarkupHelper.createLabel("User able to Login", ExtentColor.BLUE));
			Thread.sleep(3000);
			steps.clickJSButton(home.Mydreamstore, driver, name);
			String homepage = getscreen.capture(driver, name);
			test.addScreenCaptureFromPath(homepage);
			String Parent = driver.getWindowHandle();
			steps.MoveElement(FFW.FFW, driver, name);
			String FFW1 = driver.findElement(FFW.FFW).getText();
			System.out.println(FFW1);
			steps.clickJSButton(FFW.FFW, driver, name);
			test.log(Status.INFO, MarkupHelper.createLabel("User clicked on FFW link", ExtentColor.BLUE));
			Thread.sleep(2000);
			try
			{
				steps.waitForElement(home.Filterpagetext, driver);
				String Filterpage1 = driver.findElement(home.Filterpagetext).getText();
				System.out.println("User navigated to :   "+Filterpage1);
				//steps.WaitUntilElementPresent(FFW.FFW_Image1, driver);
				steps.WaitElementPresent(FFW.FFW_Image, driver);
				steps.clickJSButton(FFW.FFW_Image, driver, name);
				test.log(Status.INFO, MarkupHelper.createLabel("User clicked on First Image in T Shirts", ExtentColor.BLUE));
			}
			catch(Exception e )
			{
				System.out.println("User navigated to different landing page");
				steps.ChildWindow(driver);
				driver.manage().window().maximize();
				Thread.sleep(1000);
				steps.clickJSButton(FFW.FFW_Image1, driver, name);
				test.log(Status.INFO, MarkupHelper.createLabel("User clicked on First Image in T Shirts", ExtentColor.BLUE));
				driver.close();
				steps.ChildWindow(driver);
				driver.manage().window().maximize();
				Thread.sleep(1000);
			}
			Thread.sleep(1000);
			try
			{
				steps.clickJSButton(FFW.Readmore, driver, name);
				FFWDescription1 = driver.findElement(FFW.FFW_Description).getText();
				System.out.println("Product Description:   "+FFWDescription1);
				steps.clickJSButton(FFW.Readless, driver, name);
				test.log(Status.INFO, MarkupHelper.createLabel("Product Description:  "+ FFWDescription1, ExtentColor.BLUE));
			}
			catch(Exception e)
			{
				steps.clickJSButton(FFW.Readmore, driver, name);
				FFWDescription2 = driver.findElement(FFW.FFW_Description1).getText();
				System.out.println("Product Description:   "+FFWDescription2);
				steps.clickJSButton(FFW.Readless, driver, name);
				test.log(Status.INFO, MarkupHelper.createLabel("Product Description:  "+FFWDescription2, ExtentColor.BLUE));
			}
			Thread.sleep(1000);
			FFWMRP = driver.findElement(FFW.FFW_MRP).getText();
			test.log(Status.INFO, MarkupHelper.createLabel("Product Price:  "+FFWMRP, ExtentColor.ORANGE));
			Thread.sleep(2000);
			try
			{
				String prod2 = driver.findElement(FFW.FFW_Product2).getCssValue("color");
				System.out.println("Product:  "+prod2);
				steps.clickJSButton(FFW.FFW_Product2, driver, name);
				try
				{
					String col1 = driver.findElement(home2.color1).getCssValue("color");
					System.out.println(col1);
					steps.clickJSButton(home2.color1, driver, name);
					Thread.sleep(2000);
					FFWColor12 = driver.findElement(home2.color1).getCssValue("background-color");
					FFWhex12 = Color.fromString(FFWColor12).asHex();
					System.out.println("Image color: "+FFWhex12);
					test.log(Status.INFO, MarkupHelper.createLabel("User changed item color to:  "+FFWhex12, ExtentColor.BLUE));
					FFWFirstcolor2 = getscreen.capture(driver, "FFWFirstcolor2");
					test.addScreenCaptureFromPath(FFWFirstcolor2);
				}
				catch(Exception e)
				{
					System.out.println("Color 1 is not exists");
				}
				try
				{
					String col2 = driver.findElement(home2.color2).getCssValue("color");
					System.out.println(col2);
					steps.clickJSButton(home2.color2, driver, name);
					Thread.sleep(1000);
					FFWColor22 = driver.findElement(home2.color2).getCssValue("background-color");
					FFWhex22 = Color.fromString(FFWColor22).asHex();
					System.out.println("Image color: "+FFWhex22);
					test.log(Status.INFO, MarkupHelper.createLabel("User changed item color to:  "+FFWhex22, ExtentColor.BLUE));
					FFWSecondcolor2 = getscreen.capture(driver, "FFWSecondcolor2");
					test.addScreenCaptureFromPath(FFWSecondcolor2);
				}
				catch(Exception e)
				{
					System.out.println("Color 2 is not exists");
				}
				try
				{
					String col3 = driver.findElement(home2.color3).getCssValue("color");
					System.out.println(col3);
					steps.clickJSButton(home2.color3, driver, name);
					Thread.sleep(1000);
					FFWColor32 = driver.findElement(home2.color3).getCssValue("background-color");
					FFWhex32 = Color.fromString(FFWColor32).asHex();
					System.out.println("Image color: "+FFWhex32);
					test.log(Status.INFO, MarkupHelper.createLabel("User changed item color to:  "+FFWhex32, ExtentColor.BLUE));
					FFWThirdcolor2 = getscreen.capture(driver, "FFWThirdcolor2");
					test.addScreenCaptureFromPath(FFWThirdcolor2);
				}
				catch(Exception e)
				{
					System.out.println("Color 3 is not exists");
				}
				try 
				{
					String col4 = driver.findElement(home2.color4).getCssValue("color");
					System.out.println(col4);
					steps.clickJSButton(home2.color4, driver, name);
					Thread.sleep(1000);
					FFWColor42 = driver.findElement(home2.color4).getCssValue("background-color");
					FFWhex42 = Color.fromString(FFWColor42).asHex();
					System.out.println("Image color: "+FFWhex42);
					test.log(Status.INFO, MarkupHelper.createLabel("User changed item color to:  "+FFWhex42, ExtentColor.BLUE));
					FFWFourthcolor2 = getscreen.capture(driver, "FFWFourthcolor2");
					test.addScreenCaptureFromPath(FFWFourthcolor2);
				}
				catch(Exception e)
				{
					System.out.println("Color 4 is not exists");
				}
			}
			catch(Exception e)
			{
				System.out.println("Product 2 doesn't exists");
			}
			Thread.sleep(2000);
			try
			{
				String prod3 = driver.findElement(FFW.FFW_Product3).getCssValue("color");
				System.out.println("Product:  "+prod3);
				steps.clickJSButton(FFW.FFW_Product3, driver, name);
				try
				{
					String col1 = driver.findElement(home2.color1).getCssValue("color");
					System.out.println(col1);
					steps.clickJSButton(home2.color1, driver, name);
					Thread.sleep(2000);
					FFWColor13 = driver.findElement(home2.color1).getCssValue("background-color");
					FFWhex13 = Color.fromString(FFWColor13).asHex();
					System.out.println("Image color: "+FFWhex13);
					test.log(Status.INFO, MarkupHelper.createLabel("User changed item color to:  "+FFWhex13, ExtentColor.BLUE));
					FFWFirstcolor3 = getscreen.capture(driver, "FFWFirstcolor3");
					test.addScreenCaptureFromPath(FFWFirstcolor3);
				}
				catch(Exception e)
				{
					System.out.println("Color 1 is not exists");
				}
				try
				{
					String col2 = driver.findElement(home2.color2).getCssValue("color");
					System.out.println(col2);
					steps.clickJSButton(home2.color2, driver, name);
					Thread.sleep(1000);
					FFWColor23 = driver.findElement(home2.color2).getCssValue("background-color");
					FFWhex23 = Color.fromString(FFWColor23).asHex();
					System.out.println("Image color: "+FFWhex23);
					test.log(Status.INFO, MarkupHelper.createLabel("User changed item color to:  "+FFWhex23, ExtentColor.BLUE));
					FFWSecondcolor3 = getscreen.capture(driver, "FFWSecondcolor3");
					test.addScreenCaptureFromPath(FFWSecondcolor3);
				}
				catch(Exception e)
				{
					System.out.println("Color 2 is not exists");
				}
				try
				{
					String col3 = driver.findElement(home2.color3).getCssValue("color");
					System.out.println(col3);
					steps.clickJSButton(home2.color3, driver, name);
					Thread.sleep(1000);
					FFWColor33 = driver.findElement(home2.color3).getCssValue("background-color");
					FFWhex33 = Color.fromString(FFWColor33).asHex();
					System.out.println("Image color: "+FFWhex33);
					test.log(Status.INFO, MarkupHelper.createLabel("User changed item color to:  "+FFWhex33, ExtentColor.BLUE));
					FFWThirdcolor3 = getscreen.capture(driver, "FFWThirdcolor3");
					test.addScreenCaptureFromPath(FFWThirdcolor3);
				}
				catch(Exception e)
				{
					System.out.println("Color 3 is not exists");
				}
				try 
				{
					String col4 = driver.findElement(home2.color4).getCssValue("color");
					System.out.println(col4);
					steps.clickJSButton(home2.color4, driver, name);
					Thread.sleep(1000);
					FFWColor43 = driver.findElement(home2.color4).getCssValue("background-color");
					FFWhex43 = Color.fromString(FFWColor43).asHex();
					System.out.println("Image color: "+FFWhex43);
					test.log(Status.INFO, MarkupHelper.createLabel("User changed item color to:  "+FFWhex43, ExtentColor.BLUE));
					FFWFourthcolor3 = getscreen.capture(driver, "FFWFourthcolor3");
					test.addScreenCaptureFromPath(FFWFourthcolor3);
				}
				catch(Exception e)
				{
					System.out.println("Color 4 is not exists");
				}
			}
			catch(Exception e)
			{
				System.out.println("Product 3 doesn't exists");
			}
			Thread.sleep(2000);
			try
			{
				String prod1 = driver.findElement(FFW.FFW_Product1).getCssValue("color");
				System.out.println("Product:  "+prod1);
				steps.clickJSButton(FFW.FFW_Product1, driver, name);
				try
				{
					String col1 = driver.findElement(home2.color1).getCssValue("color");
					System.out.println(col1);
					steps.clickJSButton(home2.color1, driver, name);
					Thread.sleep(2000);
					FFWColor11 = driver.findElement(home2.color1).getCssValue("background-color");
					FFWhex11 = Color.fromString(FFWColor11).asHex();
					System.out.println("Image color: "+FFWhex11);
					test.log(Status.INFO, MarkupHelper.createLabel("User changed item color to:  "+FFWhex11, ExtentColor.BLUE));
					FFWFirstcolor1 = getscreen.capture(driver, "FFWFirstcolor1");
					test.addScreenCaptureFromPath(FFWFirstcolor1);
				}
				catch(Exception e)
				{
					System.out.println("Color 1 is not exists");
				}
				try
				{
					String col2 = driver.findElement(home2.color2).getCssValue("color");
					System.out.println(col2);
					steps.clickJSButton(home2.color2, driver, name);
					Thread.sleep(1000);
					FFWColor21 = driver.findElement(home2.color2).getCssValue("background-color");
					FFWhex21 = Color.fromString(FFWColor21).asHex();
					System.out.println("Image color: "+FFWhex21);
					test.log(Status.INFO, MarkupHelper.createLabel("User changed item color to:  "+FFWhex21, ExtentColor.BLUE));
					FFWSecondcolor1 = getscreen.capture(driver, "FFWSecondcolor1");
					test.addScreenCaptureFromPath(FFWSecondcolor1);
				}
				catch(Exception e)
				{
					System.out.println("Color 2 is not exists");
				}
				try
				{
					String col3 = driver.findElement(home2.color3).getCssValue("color");
					System.out.println(col3);
					steps.clickJSButton(home2.color3, driver, name);
					Thread.sleep(1000);
					FFWColor31 = driver.findElement(home2.color3).getCssValue("background-color");
					FFWhex31 = Color.fromString(FFWColor31).asHex();
					System.out.println("Image color: "+FFWhex31);
					test.log(Status.INFO, MarkupHelper.createLabel("User changed item color to:  "+FFWhex31, ExtentColor.BLUE));
					FFWThirdcolor1 = getscreen.capture(driver, "FFWThirdcolor1");
					test.addScreenCaptureFromPath(FFWThirdcolor1);
				}
				catch(Exception e)
				{
					System.out.println("Color 3 is not exists");
				}
				try 
				{
					String col4 = driver.findElement(home2.color4).getCssValue("color");
					System.out.println(col4);
					steps.clickJSButton(home2.color4, driver, name);
					Thread.sleep(1000);
					FFWColor41 = driver.findElement(home2.color4).getCssValue("background-color");
					FFWhex41 = Color.fromString(FFWColor41).asHex();
					System.out.println("Image color: "+FFWhex41);
					test.log(Status.INFO, MarkupHelper.createLabel("User changed item color to:  "+FFWhex41, ExtentColor.BLUE));
					FFWFourthcolor1 = getscreen.capture(driver, "FFWFourthcolor1");
					test.addScreenCaptureFromPath(FFWFourthcolor1);
				}
				catch(Exception e)
				{
					System.out.println("Color 4 is not exists");
				}
			}
			catch(Exception e)
			{
				System.out.println("Product 1 doesn't exists");
			}
			FFWcqty = driver.findElement(home.Qty).getAttribute("value");
			System.out.println("Current Quantity:  "+ FFWcqty);
			test.log(Status.INFO, MarkupHelper.createLabel("Current Quantity of product:  "+FFWcqty, ExtentColor.BLUE));
			Thread.sleep(1000);
			steps.clickJSButton(home.Trend_QtyIncrease, driver, name);
			FFWiqty = driver.findElement(home.Qty).getAttribute("value");
			System.out.println("Quantity after increase:  "+ FFWiqty);
			test.log(Status.INFO, MarkupHelper.createLabel("Current Quantity of product after Increase:  "+FFWiqty, ExtentColor.BLUE));
			steps.clickJSButton(home.Trend_QtyDecrease, driver, name);
			FFWdqty = driver.findElement(home.Qty).getAttribute("value");
			System.out.println("Quantity after decrease:  "+ FFWdqty);
			test.log(Status.INFO, MarkupHelper.createLabel("Current Quantity of product after increase:  "+FFWdqty, ExtentColor.BLUE));
			steps.clickJSButton(home.Trend_Addcart, driver, name);
			FFWSizemsg = driver.findElement(home.Trend_Sizeerrormsg).getText();
			System.out.println("User clicked on Addcart before Size selection:  "+FFWSizemsg);
			test.log(Status.INFO, MarkupHelper.createLabel("User clicked on Addcart before Size selection:  "+FFWSizemsg, ExtentColor.RED));
			//steps.clickJSButton(home2.FFW_sizeS, driver, name);
			try
			{
				steps.clickJSButton(home2.Size_S, driver, name);
				FFWsizeS = driver.findElement(home2.Size_S).getText();
				System.out.println("Selected Size:  "+FFWsizeS);
				test.log(Status.INFO, MarkupHelper.createLabel("User selected size:  "+FFWsizeS, ExtentColor.BLUE));
			}
			catch(Exception e)
			{
				System.out.println("Size S is not available");
				test.log(Status.INFO, MarkupHelper.createLabel("Size S is not available", ExtentColor.RED));
			}
			try
			{
				steps.clickJSButton(home2.Size_M, driver, name);
				FFWsizeM = driver.findElement(home2.Size_M).getText();
				System.out.println("Selected Size:  "+FFWsizeM);
				test.log(Status.INFO, MarkupHelper.createLabel("User selected size:  "+FFWsizeM, ExtentColor.BLUE));
			}
			catch(Exception e)
			{
				System.out.println("Size M is not available");
				test.log(Status.INFO, MarkupHelper.createLabel("Size M is not available", ExtentColor.RED));
			}
			try
			{
				steps.clickJSButton(home2.Size_L, driver, name);
				FFWsizeL = driver.findElement(home2.Size_L).getText();
				System.out.println("Selected Size:  "+FFWsizeL);
				test.log(Status.INFO, MarkupHelper.createLabel("User selected size:  "+FFWsizeL, ExtentColor.BLUE));
			}
			catch(Exception e)
			{
				System.out.println("Size L is not available");
				test.log(Status.INFO, MarkupHelper.createLabel("Size L is not available", ExtentColor.RED));
			}
			try
			{
				steps.clickJSButton(home2.Size_XL, driver, name);
				FFWsizeXL = driver.findElement(home2.Size_XL).getText();
				System.out.println("Selected Size:  "+FFWsizeXL);
				test.log(Status.INFO, MarkupHelper.createLabel("User selected size:  "+FFWsizeXL, ExtentColor.BLUE));
			}
			catch(Exception e)
			{
				System.out.println("Size XL is not available");
				test.log(Status.INFO, MarkupHelper.createLabel("Size XL is not available", ExtentColor.RED));
			}
			try
			{
				steps.clickJSButton(home2.Size_XXL, driver, name);
				FFWsizeXXL = driver.findElement(home2.Size_XXL).getText();
				System.out.println("Selected Size:  "+FFWsizeXXL);
				test.log(Status.INFO, MarkupHelper.createLabel("User selected size:  "+FFWsizeXXL, ExtentColor.BLUE));
			}
			catch(Exception e)
			{
				System.out.println("Size XXL is not available");
				test.log(Status.INFO, MarkupHelper.createLabel("Size XXL is not available", ExtentColor.RED));
			}
			try
			{
				steps.clickJSButton(home2.Size_XXXL, driver, name);
				FFWsizeXXXL = driver.findElement(home2.Size_XXXL).getText();
				System.out.println("Selected Size:  "+FFWsizeXXXL);
				test.log(Status.INFO, MarkupHelper.createLabel("User selected size:  "+FFWsizeXXXL, ExtentColor.BLUE));
			}
			catch(Exception e)
			{
				System.out.println("Size XXXL is not available");
				test.log(Status.INFO, MarkupHelper.createLabel("Size XXXL is not available", ExtentColor.RED));
			}
			Thread.sleep(2000);
			steps.clickJSButton(home.Trend_FindUrSize, driver, name);
			test.log(Status.INFO, MarkupHelper.createLabel("User clicked on Find your size", ExtentColor.BLUE));
			Thread.sleep(1000);
			try
			{
				String text1 =  driver.findElement(home.Trend_FindUrSize_510radio).getText();
				System.out.println(text1);
				steps.clickJSButton(home.Trend_FindUrSize_510radio, driver, name);
				FFWbelow510 = driver.findElement(home.Trend_FindUrSize_510radio).getText();
				System.out.println("User Selected:  "+FFWbelow510);
				test.log(Status.INFO, MarkupHelper.createLabel("User selected Height:  "+FFWbelow510, ExtentColor.BLUE));
				steps.clickJSButton(home.Trend_FindUrSize_above510radio, driver, name);
				FFWabove510 = driver.findElement(home.Trend_FindUrSize_above510radio).getText();
				System.out.println("User Selected:  "+FFWabove510);
				test.log(Status.INFO, MarkupHelper.createLabel("User selected Height:  "+FFWabove510, ExtentColor.BLUE));
				steps.clickJSButton(home.Trend_FindUrSize_Tightradio, driver, name);
				FFWtightfit = driver.findElement(home.Trend_FindUrSize_Tightradio).getText();
				System.out.println("User Selected:  "+FFWtightfit);
				test.log(Status.INFO, MarkupHelper.createLabel("User selected Fit:  "+FFWtightfit, ExtentColor.BLUE));
				steps.clickJSButton(home.Trend_FindUrSize_Comfortradio, driver, name);
				FFWcomfit = driver.findElement(home.Trend_FindUrSize_Comfortradio).getText();
				System.out.println("User Selected:  "+FFWcomfit);
				test.log(Status.INFO, MarkupHelper.createLabel("User selected Fit:  "+FFWcomfit, ExtentColor.BLUE));
				Thread.sleep(1000);
				steps.selectDropdown(home.Trend_FindUrSize_sizeDD, driver, strDataFileName, "size", name);
				test.log(Status.INFO, MarkupHelper.createLabel("User selected shirt size", ExtentColor.BLUE));
				steps.clickJSButton(home.Trend_FindUrSize_submit, driver, name);
				test.log(Status.INFO, MarkupHelper.createLabel("User clicked on Submit", ExtentColor.BLUE));
				FFWfindursize = getscreen.capture(driver, "FFWfindursize");
				test.addScreenCaptureFromPath(FFWfindursize);
				Thread.sleep(1000);
				steps.clickJSButton(home.Trend_FindUrSize_Okbutton, driver, name);
				test.log(Status.INFO, MarkupHelper.createLabel("system displayed size for given metrics", ExtentColor.BLUE));
				steps.clickJSButton(home.Trend_FindUrSize_closebutton, driver, name);
				test.log(Status.INFO, MarkupHelper.createLabel("User navigate to Product details page", ExtentColor.BLUE));
			}
			catch(Exception e)
			{
				steps.clickJSButton(home.Trend_FindUrSize_closebutton, driver, name);
				test.log(Status.INFO, MarkupHelper.createLabel("User navigate to Product details page", ExtentColor.BLUE));
			}
			Thread.sleep(1000);
			steps.clickJSButton(home2.Size_L, driver, name);
			test.log(Status.INFO, MarkupHelper.createLabel("User selected size", ExtentColor.BLUE));
			steps.clickJSButton(home.Trend_Addcart, driver, name);
			Thread.sleep(1000);
			test.log(Status.INFO, MarkupHelper.createLabel("User added product to Cart", ExtentColor.BLUE));
			FFWaddcart = getscreen.capture(driver, "FFWaddcart");
			test.addScreenCaptureFromPath(FFWaddcart);
			Thread.sleep(1000);
			try
			{
				String edit1 = driver.findElement(home2.Addcart_edit).getText();
				System.out.println(edit1);
				steps.clickJSButton(home2.Addcart_edit, driver, name);
				test.log(Status.INFO, MarkupHelper.createLabel("User clicked on Edit", ExtentColor.BLUE));
				steps.selectDropdown(home2.Addcart_selectsize, driver, strDataFileName, "SelectSize", name);
				steps.selectDropdown(home2.Addcart_selectqty, driver, strDataFileName, "Quantity", name);
				test.log(Status.INFO, MarkupHelper.createLabel("User selected Size and Quantity", ExtentColor.BLUE));
				steps.clickJSButton(home2.Addcart_cancel, driver, name);
				steps.clickJSButton(home2.Addcart_edit, driver, name);
				steps.selectDropdown(home2.Addcart_selectsize, driver, strDataFileName, "SelectSize", name);
				steps.selectDropdown(home2.Addcart_selectqty, driver, strDataFileName, "Quantity", name);
				steps.clickJSButton(home2.Addcart_save, driver, name);
				FFWupdateproduct1 = getscreen.capture(driver, "FFWupdateproduct1");
				test.addScreenCaptureFromPath(FFWupdateproduct1);
				test.log(Status.INFO, MarkupHelper.createLabel("User updated product size and quantity", ExtentColor.BLUE));
			}
			catch(Exception e)
			{
				System.out.println("Edit feature is not available for this product");
			}
			Thread.sleep(2000);
			steps.clickJSButton(home2.Biker_remove, driver, name);
			FFWremovecart = getscreen.capture(driver, "FFWremovecart");
			test.addScreenCaptureFromPath(FFWremovecart);
			test.log(Status.INFO, MarkupHelper.createLabel("User removed product from Cart", ExtentColor.BLUE));
			//checkout process
			steps.clickJSButton(home.Size_L, driver, name);
			test.log(Status.INFO, MarkupHelper.createLabel("User selected size", ExtentColor.BLUE));
			steps.clickJSButton(checkout.BuyNow, driver, name);
			test.log(Status.INFO, MarkupHelper.createLabel("User clicked on BuyNow, navigated to checkout page", ExtentColor.BLUE));
			String buynow1 = getscreen.capture(driver, "buynow1");
			test.addScreenCaptureFromPath(buynow1);
			Thread.sleep(2000);
			// Debit card option
			try
			{
				String Debit = driver.findElement(checkout.Debitcard).getText();
				System.out.println("Payment method:  "+Debit);
				steps.Verifytext(checkout.Debitcard, driver, Debit, name);
				steps.clickJSButton(checkout.Debitcard, driver, name);
				System.out.println("Debit card option is available");
				test.log(Status.INFO, MarkupHelper.createLabel("Payment method:  "+Debit   +    "      Available", ExtentColor.BLUE));
			}
			catch(Exception e)
			{
				System.out.println("Debit card option is not available");
				test.log(Status.INFO, MarkupHelper.createLabel("Debit card option is not available", ExtentColor.BLUE));
			}
			Thread.sleep(2000);
			// Credit card option
			try
			{
				String Credit = driver.findElement(checkout.Creditcard).getText();
				System.out.println("Payment method:  "+Credit);
				steps.Verifytext(checkout.Creditcard, driver, Credit, name);
				steps.clickJSButton(checkout.Creditcard, driver, name);
				System.out.println("Credit card option is available");
				test.log(Status.INFO, MarkupHelper.createLabel("Payment method:  "+Credit+    "      Available", ExtentColor.BLUE));
			}
			catch(Exception e)
			{
				System.out.println("Credit card option is not available");
				test.log(Status.INFO, MarkupHelper.createLabel("Credit card option is not available", ExtentColor.BLUE));
			}
			Thread.sleep(2000);
			// Net Banking option
			try
			{
				String NetBanking = driver.findElement(checkout.Netbanking).getText();
				System.out.println("Payment method:  "+NetBanking);
				steps.Verifytext(checkout.Netbanking, driver, NetBanking, name);
				steps.clickJSButton(checkout.Netbanking, driver, name);
				System.out.println("Net Banking option is available");
				test.log(Status.INFO, MarkupHelper.createLabel("Payment method:  "+NetBanking+    "      Available", ExtentColor.BLUE));
			}
			catch(Exception e)
			{
				System.out.println("Net Banking option is not available");
				test.log(Status.INFO, MarkupHelper.createLabel("Net Banking option is not available", ExtentColor.BLUE));
			}
			Thread.sleep(2000);
			// Paytm option
			try
			{
				String Paytm = driver.findElement(checkout.Paytm).getText();
				System.out.println("Payment method:  "+Paytm);
				steps.Verifytext(checkout.Paytm, driver, Paytm, name);
				steps.clickJSButton(checkout.Paytm, driver, name);
				System.out.println("Paytm option is available");
				test.log(Status.INFO, MarkupHelper.createLabel("Payment method:  "+Paytm+    "      Available", ExtentColor.BLUE));
			}
			catch(Exception e)
			{
				System.out.println("Paytm option is not available");
				test.log(Status.INFO, MarkupHelper.createLabel("Paytm option is not available", ExtentColor.BLUE));
			}
			Thread.sleep(2000);
			// MDS Wallet option
			try
			{
				String MDS = driver.findElement(checkout.MDSWallet).getText();
				System.out.println("Payment method:  "+MDS);
				steps.Verifytext(checkout.MDSWallet, driver, MDS, name);
				steps.clickJSButton(checkout.MDSWallet, driver, name);
				Thread.sleep(2000);
				String MDSpopup = driver.findElement(checkout.MDSWalletPOPup).getText();
				System.out.println("Payment method:  "+MDSpopup);
				Thread.sleep(2000);
				steps.clickJSButton(checkout.MDSWalletPOPupOK, driver, name);
				System.out.println("MDS Wallet option is available");
				test.log(Status.INFO, MarkupHelper.createLabel("Payment method:  "+MDS+    "       Available", ExtentColor.BLUE));
			}
			catch(Exception e)
			{
				System.out.println("MDS Wallet option is not available");
				test.log(Status.INFO, MarkupHelper.createLabel("MDS Wallet option is not available", ExtentColor.BLUE));
			}
			Thread.sleep(2000);
			// Cash on delivery 
			try
			{
				String cash = driver.findElement(checkout.CashonDeliveryText).getText();
				System.out.println("Payment method:  "+cash);
				test.log(Status.INFO, MarkupHelper.createLabel("Payment method:  "+cash+    "        Available", ExtentColor.BLUE));
				steps.Verifytext(checkout.CashonDeliveryText, driver, cash, name);
				steps.clickJSButton(checkout.CashonDeliveryradio, driver, name);
				Thread.sleep(2000);
				String buynow2 = driver.findElement(checkout.PopUpSaveMsg).getText();
				System.out.println("Displayed message:   "+buynow2);
				test.log(Status.INFO, MarkupHelper.createLabel("Displayed message:   "+buynow2, ExtentColor.BLUE));
				Thread.sleep(1000);
				String buynow3 = getscreen.capture(driver, "buynow3");
				test.addScreenCaptureFromPath(buynow3);
				steps.clickJSButton(checkout.Cashondelivery, driver, name);
				test.log(Status.INFO, MarkupHelper.createLabel("User clicked on Cash on Delivery link", ExtentColor.BLUE));
			}
			catch(Exception e)
			{
				System.out.println("Cash on Delivery is not available");
				test.log(Status.INFO, MarkupHelper.createLabel("Cash on Delivery is not available", ExtentColor.BLUE));
			}
			Thread.sleep(2000);
			String checkout1 = getscreen.capture(driver, "checkout1");
			test.addScreenCaptureFromPath(checkout1);
			steps.clickJSButton(checkout.CartIcon, driver, name);
			steps.clickJSButton(home.Trend_Addcart_remove, driver, name);
			Thread.sleep(1000);
			test.log(Status.INFO, MarkupHelper.createLabel("User Completed transactions of 1st Image in FashionForwardMan", ExtentColor.PURPLE));
			FFWstatus = passstatus;
			Assert.assertTrue(true);
			System.out.println("FashionForwardWomen Successful");
			testresultdata.put(Integer.toString(cnt), new Object[] {name, "FashionForwardWomen CheckOut", "FashionForwardWomen CheckOut Successful",passstatus});	
		}
		catch(Exception e)
		{
			System.out.println("FashionForwardWomen Unsuccessful");
			testresultdata.put(Integer.toString(cnt), new Object[] {name, "FashionForwardWomen CheckOut", "FashionForwardWomen CheckOut Unsuccessful",failstatus});
			FFWstatus = failstatus;
			FFWstatusfail = getscreen.capture(driver, "FFWstatusfail");
			test.addScreenCaptureFromPath(FFWstatusfail);
			Assert.assertTrue(false);
		}
	}
	@BeforeClass
	public void before()
	{
		workbook = new HSSFWorkbook();
		sheet = workbook.createSheet(strDataFileName);
		testresultdata = new LinkedHashMap<String, Object[]>();
		testresultdata.put("1", new Object[] { "Test Step Name", "Expected Result","Actual Result" ,"Status"});
	}
	@AfterClass
	public void after() throws Exception
	{
		Set<String> keyset = testresultdata.keySet();
		int rownum = 0;
		for (String key : keyset) {
			Row row = sheet.createRow(rownum++);
			Object [] objArr = testresultdata.get(key);
			int cellnum = 0;
			for (Object obj : objArr) {
				Cell cell = row.createCell(cellnum++);
				if(obj instanceof Date) 
					cell.setCellValue((Date)obj);
				else if(obj instanceof Boolean)
					cell.setCellValue((Boolean)obj);
				else if(obj instanceof String)
					cell.setCellValue((String)obj);
				else if(obj instanceof Double)
					cell.setCellValue((Double)obj);
			}
		}
		try {
			FileOutputStream out = new FileOutputStream(new File("TestResults/"+strDataFileName +".xls"));
			workbook.write(out);
			out.close();
			System.out.println("Excel written successfully..");
			TestResult();
			driver.quit();
		}
		catch (FileNotFoundException e) 
		{
			e.printStackTrace();
		} 
		catch (IOException e) 
		{
			e.printStackTrace();
		}
	}
	public void TestResult() throws Exception
	{
		HSSFRow row;
		HSSFCell cell;
		WritableFont cellFont;
		WritableCellFormat cellFormat;
		Border border = null;
		int count;
		FileInputStream inp = new FileInputStream( "TestResults/"+strDataFileName +".xls");
		HSSFWorkbook wb = new HSSFWorkbook(inp);
		HSSFSheet sh = wb.getSheetAt(0);
		count=sh.getLastRowNum();
		for (int i = 1; i <= count; i++) 
		{
			row=sh.getRow(i);
			cell=row.getCell(3);
			String status=row.getCell(3).getStringCellValue();
			if(status.equalsIgnoreCase("Pass"))
			{
				HSSFCellStyle pass= wb.createCellStyle();
				pass.setFillForegroundColor(IndexedColors.GREEN.getIndex());
				pass.setFillPattern(CellStyle.SOLID_FOREGROUND);
				cell.setCellStyle(pass);
			}
			else
			{
				HSSFCellStyle fail= wb.createCellStyle();
				fail.setFillForegroundColor(IndexedColors.RED.getIndex());
				fail.setFillPattern(CellStyle.SOLID_FOREGROUND);
				cell.setCellStyle(fail);
			}
		}
		FileOutputStream out = new FileOutputStream("TestResults/"+strDataFileName+".xls");
		wb.write(out);
		wb.close();
	}
}
