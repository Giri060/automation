package Com.Modules;
import java.io.File;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.IOException;
import java.util.Date;
import java.util.LinkedHashMap;
import java.util.Map;
import java.util.Set;
import org.apache.poi.hssf.usermodel.HSSFCell;
import org.apache.poi.hssf.usermodel.HSSFCellStyle;
import org.apache.poi.hssf.usermodel.HSSFRow;
import org.apache.poi.hssf.usermodel.HSSFSheet;
import org.apache.poi.hssf.usermodel.HSSFWorkbook;
import org.apache.poi.ss.usermodel.Cell;
import org.apache.poi.ss.usermodel.CellStyle;
import org.apache.poi.ss.usermodel.IndexedColors;
import org.apache.poi.ss.usermodel.Row;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.support.Color;
import org.testng.Assert;
import org.testng.annotations.AfterClass;
import org.testng.annotations.BeforeClass;
import org.testng.annotations.Test;
import com.aventstack.extentreports.Status;
import com.aventstack.extentreports.markuputils.ExtentColor;
import com.aventstack.extentreports.markuputils.MarkupHelper;
import Com.ObjectRepository.Checkout_OR;
import Com.ObjectRepository.FashionForwardMan_OR;
import Com.ObjectRepository.Homepage2_OR;
import Com.ObjectRepository.Homepage_OR;
import Com.ObjectRepository.Login_OR;
import Com.ObjectRepository.Tshirts_OR;
import Com.Utilities.Browsers;
import Com.Utilities.CommonMethods;
import Com.Utilities.GetScreenShot;
import Com.Utilities.Screenshot;
import Com.Utilities.Steps;
import Com.Utilities.TestBase;
import jxl.format.Border;
import jxl.write.WritableCellFormat;
import jxl.write.WritableFont;
public class FashionForwardMan extends TestBase
{
	WebDriver driver;
	Login_OR login = new Login_OR();
	Steps steps = new Steps();
	GetScreenShot getscreen = new GetScreenShot();
	Screenshot screen = new Screenshot();
	public String strAbsolutepath = new File("").getAbsolutePath();
	HSSFWorkbook workbook;
	HSSFSheet sheet;
	String strDataFileName = this.getClass().getSimpleName();
	Map<String, Object[]> testresultdata; 
	CommonMethods commonmethods = new CommonMethods();
	Homepage_OR home = new Homepage_OR();
	Homepage2_OR home2 = new Homepage2_OR();
	FashionForwardMan_OR FFM = new FashionForwardMan_OR();
	Checkout_OR checkout = new Checkout_OR();
	Screenshot screenshot = new Screenshot();
	String passstatus = screenshot.status("PASS");
	String failstatus = screenshot.status("FAIL");
	int cnt = 2;
	public static String FFMstatus = "";
	public static String FFMstatusfail = "";
	public static String FFMDescription1 = "";
	public static String FFMDescription2 = "";
	public static String FFMDescription3 = "";
	public static String FFMDescription4 = "";
	public static String FFMMRP = "";
	public static String FFMColor12 = "";
	public static String FFMhex12 = "";
	public static String FFMFirstcolor2 = "";
	public static String FFMColor22 = "";
	public static String FFMhex22 = "";
	public static String FFMSecondcolor2 = "";
	public static String FFMColor32 = "";
	public static String FFMhex32 = "";
	public static String FFMThirdcolor2 = "";
	public static String FFMColor42 = "";
	public static String FFMhex42 = "";
	public static String FFMFourthcolor2 = "";
	public static String FFMColor13 = "";
	public static String FFMhex13 = "";
	public static String FFMFirstcolor3 = "";
	public static String FFMColor23 = "";
	public static String FFMhex23 = "";
	public static String FFMSecondcolor3 = "";
	public static String FFMColor33 = "";
	public static String FFMhex33 = "";
	public static String FFMThirdcolor3 = "";
	public static String FFMColor43 = "";
	public static String FFMhex43 = "";
	public static String FFMFourthcolor3 = "";
	public static String FFMColor11 = "";
	public static String FFMhex11 = "";
	public static String FFMFirstcolor1 = "";
	public static String FFMColor21 = "";
	public static String FFMhex21 = "";
	public static String FFMSecondcolor1 = "";
	public static String FFMColor31 = "";
	public static String FFMhex31 = "";
	public static String FFMThirdcolor1 = "";
	public static String FFMColor41 = "";
	public static String FFMhex41 = "";
	public static String FFMFourthcolor1 = "";
	public static String FFMcqty = "";
	public static String FFMiqty = "";
	public static String FFMdqty = "";
	public static String FFMSizemsg = "";
	public static String FFMsizeS = "";
	public static String FFMsizeM = "";
	public static String FFMsizeL = "";
	public static String FFMsizeXL = "";
	public static String FFMsizeXXL = "";
	public static String FFMsizeXXXL = "";
	public static String FFMbelow510 = "";
	public static String FFMabove510 = "";
	public static String FFMtightfit = "";
	public static String FFMcomfit = "";
	public static String FFMfindursize = "";
	public static String FFMaddcart = "";
	public static String FFMupdateproduct1 = "";
	public static String FFMremovecart = "";
	public FashionForwardMan()
	{
		Browsers b = new Browsers();
		this.driver = b.getBrowsers("chrome1");		 
	}
	@Test
	public void FashionForwardManCheckout() throws Exception
	{
		String name = new Object(){}.getClass().getEnclosingMethod().getName();
		cnt ++;
		try 
		{
			test = extent.createTest("FashionForwardMan Module");
			System.out.println("FashionForwardMan Module Test Case Executing...");
			commonmethods.Login(driver);
			test.log(Status.INFO, MarkupHelper.createLabel("User able to Login", ExtentColor.BLUE));
			Thread.sleep(3000);
			steps.clickJSButton(home.Mydreamstore, driver, name);
			String homepage = getscreen.capture(driver, name);
			test.addScreenCaptureFromPath(homepage);
			String Parent = driver.getWindowHandle();
			steps.MoveElement(FFM.FFM, driver, name);
			String FFM1 = driver.findElement(FFM.FFM).getText();
			System.out.println(FFM1);
			steps.clickJSButton(FFM.FFM, driver, name);
			test.log(Status.INFO, MarkupHelper.createLabel("User clicked on FFM link", ExtentColor.BLUE));
			Thread.sleep(2000);
			try
			{
				steps.waitForElement(home.Filterpagetext, driver);
				String Filterpage1 = driver.findElement(home.Filterpagetext).getText();
				System.out.println("User navigated to :   "+Filterpage1);
				//steps.WaitUntilElementPresent(FFM.FFM_Image1, driver);
				steps.WaitElementPresent(FFM.FFM_Image1, driver);
				steps.clickJSButton(FFM.FFM_Image1, driver, name);
				test.log(Status.INFO, MarkupHelper.createLabel("User clicked on First Image in T Shirts", ExtentColor.BLUE));
			}
			catch(Exception e )
			{
				System.out.println("User navigated to different landing page");
				steps.ChildWindow(driver);
				driver.manage().window().maximize();
				Thread.sleep(1000);
				steps.clickJSButton(FFM.FFM_Image1, driver, name);
				test.log(Status.INFO, MarkupHelper.createLabel("User clicked on First Image in T Shirts", ExtentColor.BLUE));
				driver.close();
				steps.ChildWindow(driver);
				driver.manage().window().maximize();
				Thread.sleep(1000);
			}
			Thread.sleep(1000);
			try
			{
				steps.clickJSButton(FFM.Readmore, driver, name);
				FFMDescription1 = driver.findElement(FFM.FFM_Description).getText();
				System.out.println("Product Description:   "+FFMDescription1);
				steps.clickJSButton(FFM.Readless, driver, name);
				test.log(Status.INFO, MarkupHelper.createLabel("Product Description:  "+ FFMDescription1, ExtentColor.BLUE));
			}
			catch(Exception e)
			{
				steps.clickJSButton(FFM.Readmore, driver, name);
				FFMDescription2 = driver.findElement(FFM.FFM_Description1).getText();
				System.out.println("Product Description:   "+FFMDescription2);
				steps.clickJSButton(FFM.Readless, driver, name);
				test.log(Status.INFO, MarkupHelper.createLabel("Product Description:  "+FFMDescription2, ExtentColor.BLUE));
			}
			Thread.sleep(1000);
			FFMMRP = driver.findElement(FFM.FFM_MRP).getText();
			test.log(Status.INFO, MarkupHelper.createLabel("Product Price:  "+FFMMRP, ExtentColor.ORANGE));
			Thread.sleep(2000);
			try
			{
				String prod2 = driver.findElement(FFM.FFM_Product2).getCssValue("color");
				System.out.println("Product:  "+prod2);
				steps.clickJSButton(FFM.FFM_Product2, driver, name);
				try
				{
					String col1 = driver.findElement(home2.color1).getCssValue("color");
					System.out.println(col1);
					steps.clickJSButton(home2.color1, driver, name);
					Thread.sleep(2000);
					FFMColor12 = driver.findElement(home2.color1).getCssValue("background-color");
					FFMhex12 = Color.fromString(FFMColor12).asHex();
					System.out.println("Image color: "+FFMhex12);
					test.log(Status.INFO, MarkupHelper.createLabel("User changed item color to:  "+FFMhex12, ExtentColor.BLUE));
					FFMFirstcolor2 = getscreen.capture(driver, "FFMFirstcolor2");
					test.addScreenCaptureFromPath(FFMFirstcolor2);
				}
				catch(Exception e)
				{
					System.out.println("Color 1 is not exists");
				}
				try
				{
					String col2 = driver.findElement(home2.color2).getCssValue("color");
					System.out.println(col2);
					steps.clickJSButton(home2.color2, driver, name);
					Thread.sleep(1000);
					FFMColor22 = driver.findElement(home2.color2).getCssValue("background-color");
					FFMhex22 = Color.fromString(FFMColor22).asHex();
					System.out.println("Image color: "+FFMhex22);
					test.log(Status.INFO, MarkupHelper.createLabel("User changed item color to:  "+FFMhex22, ExtentColor.BLUE));
					FFMSecondcolor2 = getscreen.capture(driver, "FFMSecondcolor2");
					test.addScreenCaptureFromPath(FFMSecondcolor2);
				}
				catch(Exception e)
				{
					System.out.println("Color 2 is not exists");
				}
				try
				{
					String col3 = driver.findElement(home2.color3).getCssValue("color");
					System.out.println(col3);
					steps.clickJSButton(home2.color3, driver, name);
					Thread.sleep(1000);
					FFMColor32 = driver.findElement(home2.color3).getCssValue("background-color");
					FFMhex32 = Color.fromString(FFMColor32).asHex();
					System.out.println("Image color: "+FFMhex32);
					test.log(Status.INFO, MarkupHelper.createLabel("User changed item color to:  "+FFMhex32, ExtentColor.BLUE));
					FFMThirdcolor2 = getscreen.capture(driver, "FFMThirdcolor2");
					test.addScreenCaptureFromPath(FFMThirdcolor2);
				}
				catch(Exception e)
				{
					System.out.println("Color 3 is not exists");
				}
				try 
				{
					String col4 = driver.findElement(home2.color4).getCssValue("color");
					System.out.println(col4);
					steps.clickJSButton(home2.color4, driver, name);
					Thread.sleep(1000);
					FFMColor42 = driver.findElement(home2.color4).getCssValue("background-color");
					FFMhex42 = Color.fromString(FFMColor42).asHex();
					System.out.println("Image color: "+FFMhex42);
					test.log(Status.INFO, MarkupHelper.createLabel("User changed item color to:  "+FFMhex42, ExtentColor.BLUE));
					FFMFourthcolor2 = getscreen.capture(driver, "FFMFourthcolor2");
					test.addScreenCaptureFromPath(FFMFourthcolor2);
				}
				catch(Exception e)
				{
					System.out.println("Color 4 is not exists");
				}
			}
			catch(Exception e)
			{
				System.out.println("Product 2 doesn't exists");
			}
			Thread.sleep(2000);
			try
			{
				String prod3 = driver.findElement(FFM.FFM_Product3).getCssValue("color");
				System.out.println("Product:  "+prod3);
				steps.clickJSButton(FFM.FFM_Product3, driver, name);
				try
				{
					String col1 = driver.findElement(home2.color1).getCssValue("color");
					System.out.println(col1);
					steps.clickJSButton(home2.color1, driver, name);
					Thread.sleep(2000);
					FFMColor13 = driver.findElement(home2.color1).getCssValue("background-color");
					FFMhex13 = Color.fromString(FFMColor13).asHex();
					System.out.println("Image color: "+FFMhex13);
					test.log(Status.INFO, MarkupHelper.createLabel("User changed item color to:  "+FFMhex13, ExtentColor.BLUE));
					FFMFirstcolor3 = getscreen.capture(driver, "FFMFirstcolor3");
					test.addScreenCaptureFromPath(FFMFirstcolor3);
				}
				catch(Exception e)
				{
					System.out.println("Color 1 is not exists");
				}
				try
				{
					String col2 = driver.findElement(home2.color2).getCssValue("color");
					System.out.println(col2);
					steps.clickJSButton(home2.color2, driver, name);
					Thread.sleep(1000);
					FFMColor23 = driver.findElement(home2.color2).getCssValue("background-color");
					FFMhex23 = Color.fromString(FFMColor23).asHex();
					System.out.println("Image color: "+FFMhex23);
					test.log(Status.INFO, MarkupHelper.createLabel("User changed item color to:  "+FFMhex23, ExtentColor.BLUE));
					FFMSecondcolor3 = getscreen.capture(driver, "FFMSecondcolor3");
					test.addScreenCaptureFromPath(FFMSecondcolor3);
				}
				catch(Exception e)
				{
					System.out.println("Color 2 is not exists");
				}
				try
				{
					String col3 = driver.findElement(home2.color3).getCssValue("color");
					System.out.println(col3);
					steps.clickJSButton(home2.color3, driver, name);
					Thread.sleep(1000);
					FFMColor33 = driver.findElement(home2.color3).getCssValue("background-color");
					FFMhex33 = Color.fromString(FFMColor33).asHex();
					System.out.println("Image color: "+FFMhex33);
					test.log(Status.INFO, MarkupHelper.createLabel("User changed item color to:  "+FFMhex33, ExtentColor.BLUE));
					FFMThirdcolor3 = getscreen.capture(driver, "FFMThirdcolor3");
					test.addScreenCaptureFromPath(FFMThirdcolor3);
				}
				catch(Exception e)
				{
					System.out.println("Color 3 is not exists");
				}
				try 
				{
					String col4 = driver.findElement(home2.color4).getCssValue("color");
					System.out.println(col4);
					steps.clickJSButton(home2.color4, driver, name);
					Thread.sleep(1000);
					FFMColor43 = driver.findElement(home2.color4).getCssValue("background-color");
					FFMhex43 = Color.fromString(FFMColor43).asHex();
					System.out.println("Image color: "+FFMhex43);
					test.log(Status.INFO, MarkupHelper.createLabel("User changed item color to:  "+FFMhex43, ExtentColor.BLUE));
					FFMFourthcolor3 = getscreen.capture(driver, "FFMFourthcolor3");
					test.addScreenCaptureFromPath(FFMFourthcolor3);
				}
				catch(Exception e)
				{
					System.out.println("Color 4 is not exists");
				}
			}
			catch(Exception e)
			{
				System.out.println("Product 3 doesn't exists");
			}
			Thread.sleep(2000);
			try
			{
				String prod1 = driver.findElement(FFM.FFM_Product1).getCssValue("color");
				System.out.println("Product:  "+prod1);
				steps.clickJSButton(FFM.FFM_Product1, driver, name);
				try
				{
					String col1 = driver.findElement(home2.color1).getCssValue("color");
					System.out.println(col1);
					steps.clickJSButton(home2.color1, driver, name);
					Thread.sleep(2000);
					FFMColor11 = driver.findElement(home2.color1).getCssValue("background-color");
					FFMhex11 = Color.fromString(FFMColor11).asHex();
					System.out.println("Image color: "+FFMhex11);
					test.log(Status.INFO, MarkupHelper.createLabel("User changed item color to:  "+FFMhex11, ExtentColor.BLUE));
					FFMFirstcolor1 = getscreen.capture(driver, "FFMFirstcolor1");
					test.addScreenCaptureFromPath(FFMFirstcolor1);
				}
				catch(Exception e)
				{
					System.out.println("Color 1 is not exists");
				}
				try
				{
					String col2 = driver.findElement(home2.color2).getCssValue("color");
					System.out.println(col2);
					steps.clickJSButton(home2.color2, driver, name);
					Thread.sleep(1000);
					FFMColor21 = driver.findElement(home2.color2).getCssValue("background-color");
					FFMhex21 = Color.fromString(FFMColor21).asHex();
					System.out.println("Image color: "+FFMhex21);
					test.log(Status.INFO, MarkupHelper.createLabel("User changed item color to:  "+FFMhex21, ExtentColor.BLUE));
					FFMSecondcolor1 = getscreen.capture(driver, "FFMSecondcolor1");
					test.addScreenCaptureFromPath(FFMSecondcolor1);
				}
				catch(Exception e)
				{
					System.out.println("Color 2 is not exists");
				}
				try
				{
					String col3 = driver.findElement(home2.color3).getCssValue("color");
					System.out.println(col3);
					steps.clickJSButton(home2.color3, driver, name);
					Thread.sleep(1000);
					FFMColor31 = driver.findElement(home2.color3).getCssValue("background-color");
					FFMhex31 = Color.fromString(FFMColor31).asHex();
					System.out.println("Image color: "+FFMhex31);
					test.log(Status.INFO, MarkupHelper.createLabel("User changed item color to:  "+FFMhex31, ExtentColor.BLUE));
					FFMThirdcolor1 = getscreen.capture(driver, "FFMThirdcolor1");
					test.addScreenCaptureFromPath(FFMThirdcolor1);
				}
				catch(Exception e)
				{
					System.out.println("Color 3 is not exists");
				}
				try 
				{
					String col4 = driver.findElement(home2.color4).getCssValue("color");
					System.out.println(col4);
					steps.clickJSButton(home2.color4, driver, name);
					Thread.sleep(1000);
					FFMColor41 = driver.findElement(home2.color4).getCssValue("background-color");
					FFMhex41 = Color.fromString(FFMColor41).asHex();
					System.out.println("Image color: "+FFMhex41);
					test.log(Status.INFO, MarkupHelper.createLabel("User changed item color to:  "+FFMhex41, ExtentColor.BLUE));
					FFMFourthcolor1 = getscreen.capture(driver, "FFMFourthcolor1");
					test.addScreenCaptureFromPath(FFMFourthcolor1);
				}
				catch(Exception e)
				{
					System.out.println("Color 4 is not exists");
				}
			}
			catch(Exception e)
			{
				System.out.println("Product 1 doesn't exists");
			}
			FFMcqty = driver.findElement(home.Qty).getAttribute("value");
			System.out.println("Current Quantity:  "+ FFMcqty);
			test.log(Status.INFO, MarkupHelper.createLabel("Current Quantity of product:  "+FFMcqty, ExtentColor.BLUE));
			Thread.sleep(1000);
			steps.clickJSButton(home.Trend_QtyIncrease, driver, name);
			FFMiqty = driver.findElement(home.Qty).getAttribute("value");
			System.out.println("Quantity after increase:  "+ FFMiqty);
			test.log(Status.INFO, MarkupHelper.createLabel("Current Quantity of product after Increase:  "+FFMiqty, ExtentColor.BLUE));
			steps.clickJSButton(home.Trend_QtyDecrease, driver, name);
			FFMdqty = driver.findElement(home.Qty).getAttribute("value");
			System.out.println("Quantity after decrease:  "+ FFMdqty);
			test.log(Status.INFO, MarkupHelper.createLabel("Current Quantity of product after increase:  "+FFMdqty, ExtentColor.BLUE));
			steps.clickJSButton(home.Trend_Addcart, driver, name);
			FFMSizemsg = driver.findElement(home.Trend_Sizeerrormsg).getText();
			System.out.println("User clicked on Addcart before Size selection:  "+FFMSizemsg);
			test.log(Status.INFO, MarkupHelper.createLabel("User clicked on Addcart before Size selection:  "+FFMSizemsg, ExtentColor.RED));
			//steps.clickJSButton(home2.FFM_sizeS, driver, name);
			steps.clickJSButton(home2.Size_S, driver, name);
			FFMsizeS = driver.findElement(home2.Size_S).getText();
			System.out.println("Selected Size:  "+FFMsizeS);
			test.log(Status.INFO, MarkupHelper.createLabel("User selected size:  "+FFMsizeS, ExtentColor.BLUE));
			steps.clickJSButton(home2.Size_M, driver, name);
			FFMsizeM = driver.findElement(home2.Size_M).getText();
			System.out.println("Selected Size:  "+FFMsizeM);
			test.log(Status.INFO, MarkupHelper.createLabel("User selected size:  "+FFMsizeM, ExtentColor.BLUE));
			steps.clickJSButton(home2.Size_L, driver, name);
			FFMsizeL = driver.findElement(home2.Size_L).getText();
			System.out.println("Selected Size:  "+FFMsizeL);
			test.log(Status.INFO, MarkupHelper.createLabel("User selected size:  "+FFMsizeL, ExtentColor.BLUE));
			steps.clickJSButton(home2.Size_XL, driver, name);
			FFMsizeXL = driver.findElement(home2.Size_XL).getText();
			System.out.println("Selected Size:  "+FFMsizeXL);
			test.log(Status.INFO, MarkupHelper.createLabel("User selected size:  "+FFMsizeXL, ExtentColor.BLUE));
			steps.clickJSButton(home2.Size_XXL, driver, name);
			FFMsizeXXL = driver.findElement(home2.Size_XXL).getText();
			System.out.println("Selected Size:  "+FFMsizeXXL);
			test.log(Status.INFO, MarkupHelper.createLabel("User selected size:  "+FFMsizeXXL, ExtentColor.BLUE));
			steps.clickJSButton(home2.Size_XXXL, driver, name);
			FFMsizeXXXL = driver.findElement(home2.Size_XXXL).getText();
			System.out.println("Selected Size:  "+FFMsizeXXXL);
			test.log(Status.INFO, MarkupHelper.createLabel("User selected size:  "+FFMsizeXXXL, ExtentColor.BLUE));
			steps.clickJSButton(home.Trend_FindUrSize, driver, name);
			test.log(Status.INFO, MarkupHelper.createLabel("User clicked on Find your size", ExtentColor.BLUE));
			Thread.sleep(1000);
			try
			{
				String text1 =  driver.findElement(home.Trend_FindUrSize_510radio).getText();
				System.out.println(text1);
				steps.clickJSButton(home.Trend_FindUrSize_510radio, driver, name);
				FFMbelow510 = driver.findElement(home.Trend_FindUrSize_510radio).getText();
				System.out.println("User Selected:  "+FFMbelow510);
				test.log(Status.INFO, MarkupHelper.createLabel("User selected Height:  "+FFMbelow510, ExtentColor.BLUE));
				steps.clickJSButton(home.Trend_FindUrSize_above510radio, driver, name);
				FFMabove510 = driver.findElement(home.Trend_FindUrSize_above510radio).getText();
				System.out.println("User Selected:  "+FFMabove510);
				test.log(Status.INFO, MarkupHelper.createLabel("User selected Height:  "+FFMabove510, ExtentColor.BLUE));
				steps.clickJSButton(home.Trend_FindUrSize_Tightradio, driver, name);
				FFMtightfit = driver.findElement(home.Trend_FindUrSize_Tightradio).getText();
				System.out.println("User Selected:  "+FFMtightfit);
				test.log(Status.INFO, MarkupHelper.createLabel("User selected Fit:  "+FFMtightfit, ExtentColor.BLUE));
				steps.clickJSButton(home.Trend_FindUrSize_Comfortradio, driver, name);
				FFMcomfit = driver.findElement(home.Trend_FindUrSize_Comfortradio).getText();
				System.out.println("User Selected:  "+FFMcomfit);
				test.log(Status.INFO, MarkupHelper.createLabel("User selected Fit:  "+FFMcomfit, ExtentColor.BLUE));
				Thread.sleep(1000);
				steps.selectDropdown(home.Trend_FindUrSize_sizeDD, driver, strDataFileName, "size", name);
				test.log(Status.INFO, MarkupHelper.createLabel("User selected shirt size", ExtentColor.BLUE));
				steps.clickJSButton(home.Trend_FindUrSize_submit, driver, name);
				test.log(Status.INFO, MarkupHelper.createLabel("User clicked on Submit", ExtentColor.BLUE));
				FFMfindursize = getscreen.capture(driver, "FFMfindursize");
				test.addScreenCaptureFromPath(FFMfindursize);
				Thread.sleep(1000);
				steps.clickJSButton(home.Trend_FindUrSize_Okbutton, driver, name);
				test.log(Status.INFO, MarkupHelper.createLabel("system displayed size for given metrics", ExtentColor.BLUE));
				steps.clickJSButton(home.Trend_FindUrSize_closebutton, driver, name);
				test.log(Status.INFO, MarkupHelper.createLabel("User navigate to Product details page", ExtentColor.BLUE));
			}
			catch(Exception e)
			{
				steps.clickJSButton(home.Trend_FindUrSize_closebutton, driver, name);
				test.log(Status.INFO, MarkupHelper.createLabel("User navigate to Product details page", ExtentColor.BLUE));
			}
			Thread.sleep(1000);
			steps.clickJSButton(home2.Size_L, driver, name);
			test.log(Status.INFO, MarkupHelper.createLabel("User selected size", ExtentColor.BLUE));
			steps.clickJSButton(home.Trend_Addcart, driver, name);
			Thread.sleep(1000);
			test.log(Status.INFO, MarkupHelper.createLabel("User added product to Cart", ExtentColor.BLUE));
			FFMaddcart = getscreen.capture(driver, "FFMaddcart");
			test.addScreenCaptureFromPath(FFMaddcart);
			Thread.sleep(1000);
			try
			{
				String edit1 = driver.findElement(home2.Addcart_edit).getText();
				System.out.println(edit1);
				steps.clickJSButton(home2.Addcart_edit, driver, name);
				test.log(Status.INFO, MarkupHelper.createLabel("User clicked on Edit", ExtentColor.BLUE));
				steps.selectDropdown(home2.Addcart_selectsize, driver, strDataFileName, "SelectSize", name);
				steps.selectDropdown(home2.Addcart_selectqty, driver, strDataFileName, "Quantity", name);
				test.log(Status.INFO, MarkupHelper.createLabel("User selected Size and Quantity", ExtentColor.BLUE));
				steps.clickJSButton(home2.Addcart_cancel, driver, name);
				steps.clickJSButton(home2.Addcart_edit, driver, name);
				steps.selectDropdown(home2.Addcart_selectsize, driver, strDataFileName, "SelectSize", name);
				steps.selectDropdown(home2.Addcart_selectqty, driver, strDataFileName, "Quantity", name);
				steps.clickJSButton(home2.Addcart_save, driver, name);
				FFMupdateproduct1 = getscreen.capture(driver, "FFMupdateproduct1");
				test.addScreenCaptureFromPath(FFMupdateproduct1);
				test.log(Status.INFO, MarkupHelper.createLabel("User updated product size and quantity", ExtentColor.BLUE));
			}
			catch(Exception e)
			{
				System.out.println("Edit feature is not available for this product");
			}
			Thread.sleep(2000);
			steps.clickJSButton(home2.Biker_remove, driver, name);
			FFMremovecart = getscreen.capture(driver, "FFMremovecart");
			test.addScreenCaptureFromPath(FFMremovecart);
			test.log(Status.INFO, MarkupHelper.createLabel("User removed product from Cart", ExtentColor.BLUE));
			//checkout process
			steps.clickJSButton(home.Size_L, driver, name);
			test.log(Status.INFO, MarkupHelper.createLabel("User selected size", ExtentColor.BLUE));
			steps.clickJSButton(checkout.BuyNow, driver, name);
			test.log(Status.INFO, MarkupHelper.createLabel("User clicked on BuyNow, navigated to checkout page", ExtentColor.BLUE));
			String buynow1 = getscreen.capture(driver, "buynow1");
			test.addScreenCaptureFromPath(buynow1);
			Thread.sleep(2000);
			// Debit card option
			try
			{
				String Debit = driver.findElement(checkout.Debitcard).getText();
				System.out.println("Payment method:  "+Debit);
				steps.Verifytext(checkout.Debitcard, driver, Debit, name);
				steps.clickJSButton(checkout.Debitcard, driver, name);
				System.out.println("Debit card option is available");
				test.log(Status.INFO, MarkupHelper.createLabel("Payment method:  "+Debit   +    "      Available", ExtentColor.BLUE));
			}
			catch(Exception e)
			{
				System.out.println("Debit card option is not available");
				test.log(Status.INFO, MarkupHelper.createLabel("Debit card option is not available", ExtentColor.BLUE));
			}
			Thread.sleep(2000);
			// Credit card option
			try
			{
				String Credit = driver.findElement(checkout.Creditcard).getText();
				System.out.println("Payment method:  "+Credit);
				steps.Verifytext(checkout.Creditcard, driver, Credit, name);
				steps.clickJSButton(checkout.Creditcard, driver, name);
				System.out.println("Credit card option is available");
				test.log(Status.INFO, MarkupHelper.createLabel("Payment method:  "+Credit+    "      Available", ExtentColor.BLUE));
			}
			catch(Exception e)
			{
				System.out.println("Credit card option is not available");
				test.log(Status.INFO, MarkupHelper.createLabel("Credit card option is not available", ExtentColor.BLUE));
			}
			Thread.sleep(2000);
			// Net Banking option
			try
			{
				String NetBanking = driver.findElement(checkout.Netbanking).getText();
				System.out.println("Payment method:  "+NetBanking);
				steps.Verifytext(checkout.Netbanking, driver, NetBanking, name);
				steps.clickJSButton(checkout.Netbanking, driver, name);
				System.out.println("Net Banking option is available");
				test.log(Status.INFO, MarkupHelper.createLabel("Payment method:  "+NetBanking+    "      Available", ExtentColor.BLUE));
			}
			catch(Exception e)
			{
				System.out.println("Net Banking option is not available");
				test.log(Status.INFO, MarkupHelper.createLabel("Net Banking option is not available", ExtentColor.BLUE));
			}
			Thread.sleep(2000);
			// Paytm option
			try
			{
				String Paytm = driver.findElement(checkout.Paytm).getText();
				System.out.println("Payment method:  "+Paytm);
				steps.Verifytext(checkout.Paytm, driver, Paytm, name);
				steps.clickJSButton(checkout.Paytm, driver, name);
				System.out.println("Paytm option is available");
				test.log(Status.INFO, MarkupHelper.createLabel("Payment method:  "+Paytm+    "      Available", ExtentColor.BLUE));
			}
			catch(Exception e)
			{
				System.out.println("Paytm option is not available");
				test.log(Status.INFO, MarkupHelper.createLabel("Paytm option is not available", ExtentColor.BLUE));
			}
			Thread.sleep(2000);
			// MDS Wallet option
			try
			{
				String MDS = driver.findElement(checkout.MDSWallet).getText();
				System.out.println("Payment method:  "+MDS);
				steps.Verifytext(checkout.MDSWallet, driver, MDS, name);
				steps.clickJSButton(checkout.MDSWallet, driver, name);
				Thread.sleep(2000);
				String MDSpopup = driver.findElement(checkout.MDSWalletPOPup).getText();
				System.out.println("Payment method:  "+MDSpopup);
				Thread.sleep(2000);
				steps.clickJSButton(checkout.MDSWalletPOPupOK, driver, name);
				System.out.println("MDS Wallet option is available");
				test.log(Status.INFO, MarkupHelper.createLabel("Payment method:  "+MDS+    "       Available", ExtentColor.BLUE));
			}
			catch(Exception e)
			{
				System.out.println("MDS Wallet option is not available");
				test.log(Status.INFO, MarkupHelper.createLabel("MDS Wallet option is not available", ExtentColor.BLUE));
			}
			Thread.sleep(2000);
			// Cash on delivery 
			try
			{
				String cash = driver.findElement(checkout.CashonDeliveryText).getText();
				System.out.println("Payment method:  "+cash);
				test.log(Status.INFO, MarkupHelper.createLabel("Payment method:  "+cash+    "        Available", ExtentColor.BLUE));
				steps.Verifytext(checkout.CashonDeliveryText, driver, cash, name);
				steps.clickJSButton(checkout.CashonDeliveryradio, driver, name);
				Thread.sleep(2000);
				String buynow2 = driver.findElement(checkout.PopUpSaveMsg).getText();
				System.out.println("Displayed message:   "+buynow2);
				test.log(Status.INFO, MarkupHelper.createLabel("Displayed message:   "+buynow2, ExtentColor.BLUE));
				Thread.sleep(1000);
				String buynow3 = getscreen.capture(driver, "buynow3");
				test.addScreenCaptureFromPath(buynow3);
				steps.clickJSButton(checkout.Cashondelivery, driver, name);
				test.log(Status.INFO, MarkupHelper.createLabel("User clicked on Cash on Delivery link", ExtentColor.BLUE));
			}
			catch(Exception e)
			{
				System.out.println("Cash on Delivery is not available");
				test.log(Status.INFO, MarkupHelper.createLabel("Cash on Delivery is not available", ExtentColor.BLUE));
			}
			Thread.sleep(2000);
			String checkout1 = getscreen.capture(driver, "checkout1");
			test.addScreenCaptureFromPath(checkout1);
			steps.clickJSButton(checkout.CartIcon, driver, name);
			steps.clickJSButton(home.Trend_Addcart_remove, driver, name);
			Thread.sleep(1000);
			test.log(Status.INFO, MarkupHelper.createLabel("User Completed transactions of 1st Image in FashionForwardMan", ExtentColor.PURPLE));
			FFMstatus = passstatus;
			Assert.assertTrue(true);
			System.out.println("FashionForwardMan Successful");
			testresultdata.put(Integer.toString(cnt), new Object[] {name, "FashionForwardMan CheckOut", "FashionForwardMan CheckOut Successful",passstatus});	
		}
		catch(Exception e)
		{
			System.out.println("FashionForwardMan Unsuccessful");
			testresultdata.put(Integer.toString(cnt), new Object[] {name, "FashionForwardMan CheckOut", "FashionForwardMan CheckOut Unsuccessful",failstatus});
			FFMstatus = failstatus;
			FFMstatusfail = getscreen.capture(driver, "FFMstatusfail");
			test.addScreenCaptureFromPath(FFMstatusfail);
			Assert.assertTrue(false);
		}
	}
	@BeforeClass
	public void before()
	{
		workbook = new HSSFWorkbook();
		sheet = workbook.createSheet(strDataFileName);
		testresultdata = new LinkedHashMap<String, Object[]>();
		testresultdata.put("1", new Object[] { "Test Step Name", "Expected Result","Actual Result" ,"Status"});
	}
	@AfterClass
	public void after() throws Exception
	{
		Set<String> keyset = testresultdata.keySet();
		int rownum = 0;
		for (String key : keyset) {
			Row row = sheet.createRow(rownum++);
			Object [] objArr = testresultdata.get(key);
			int cellnum = 0;
			for (Object obj : objArr) {
				Cell cell = row.createCell(cellnum++);
				if(obj instanceof Date) 
					cell.setCellValue((Date)obj);
				else if(obj instanceof Boolean)
					cell.setCellValue((Boolean)obj);
				else if(obj instanceof String)
					cell.setCellValue((String)obj);
				else if(obj instanceof Double)
					cell.setCellValue((Double)obj);
			}
		}
		try {
			FileOutputStream out = new FileOutputStream(new File("TestResults/"+strDataFileName +".xls"));
			workbook.write(out);
			out.close();
			System.out.println("Excel written successfully..");
			TestResult();
			driver.quit();
		}
		catch (FileNotFoundException e) 
		{
			e.printStackTrace();
		} 
		catch (IOException e) 
		{
			e.printStackTrace();
		}
	}
	public void TestResult() throws Exception
	{
		HSSFRow row;
		HSSFCell cell;
		WritableFont cellFont;
		WritableCellFormat cellFormat;
		Border border = null;
		int count;
		FileInputStream inp = new FileInputStream( "TestResults/"+strDataFileName +".xls");
		HSSFWorkbook wb = new HSSFWorkbook(inp);
		HSSFSheet sh = wb.getSheetAt(0);
		count=sh.getLastRowNum();
		for (int i = 1; i <= count; i++) 
		{
			row=sh.getRow(i);
			cell=row.getCell(3);
			String status=row.getCell(3).getStringCellValue();
			if(status.equalsIgnoreCase("Pass"))
			{
				HSSFCellStyle pass= wb.createCellStyle();
				pass.setFillForegroundColor(IndexedColors.GREEN.getIndex());
				pass.setFillPattern(CellStyle.SOLID_FOREGROUND);
				cell.setCellStyle(pass);
			}
			else
			{
				HSSFCellStyle fail= wb.createCellStyle();
				fail.setFillForegroundColor(IndexedColors.RED.getIndex());
				fail.setFillPattern(CellStyle.SOLID_FOREGROUND);
				cell.setCellStyle(fail);
			}
		}
		FileOutputStream out = new FileOutputStream("TestResults/"+strDataFileName+".xls");
		wb.write(out);
		wb.close();
	}
}
