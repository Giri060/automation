package Com.Modules;
import java.io.File;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.IOException;
import java.util.Date;
import java.util.LinkedHashMap;
import java.util.Map;
import java.util.Set;
import org.apache.poi.hssf.usermodel.HSSFCell;
import org.apache.poi.hssf.usermodel.HSSFCellStyle;
import org.apache.poi.hssf.usermodel.HSSFRow;
import org.apache.poi.hssf.usermodel.HSSFSheet;
import org.apache.poi.hssf.usermodel.HSSFWorkbook;
import org.apache.poi.ss.usermodel.Cell;
import org.apache.poi.ss.usermodel.CellStyle;
import org.apache.poi.ss.usermodel.IndexedColors;
import org.apache.poi.ss.usermodel.Row;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.support.Color;
import org.testng.Assert;
import org.testng.annotations.AfterClass;
import org.testng.annotations.BeforeClass;
import org.testng.annotations.Test;
import com.aventstack.extentreports.Status;
import com.aventstack.extentreports.markuputils.ExtentColor;
import com.aventstack.extentreports.markuputils.MarkupHelper;
import Com.ObjectRepository.Checkout_OR;
import Com.ObjectRepository.Fullsleeve_OR;
import Com.ObjectRepository.Homepage2_OR;
import Com.ObjectRepository.Homepage_OR;
import Com.ObjectRepository.Login_OR;
import Com.ObjectRepository.Tshirts_OR;
import Com.Utilities.Browsers;
import Com.Utilities.CommonMethods;
import Com.Utilities.GetScreenShot;
import Com.Utilities.Screenshot;
import Com.Utilities.Steps;
import Com.Utilities.TestBase;
import jxl.format.Border;
import jxl.write.WritableCellFormat;
import jxl.write.WritableFont;
public class FullSleeve extends TestBase
{
	WebDriver driver;
	Login_OR login = new Login_OR();
	Steps steps = new Steps();
	GetScreenShot getscreen = new GetScreenShot();
	Screenshot screen = new Screenshot();
	public String strAbsolutepath = new File("").getAbsolutePath();
	HSSFWorkbook workbook;
	HSSFSheet sheet;
	String strDataFileName = this.getClass().getSimpleName();
	Map<String, Object[]> testresultdata; 
	CommonMethods commonmethods = new CommonMethods();
	Homepage_OR home = new Homepage_OR();
	Homepage2_OR home2 = new Homepage2_OR();
	Fullsleeve_OR Fullsleeve = new Fullsleeve_OR();
	Checkout_OR checkout = new Checkout_OR();
	Screenshot screenshot = new Screenshot();
	String passstatus = screenshot.status("PASS");
	String failstatus = screenshot.status("FAIL");
	int cnt = 2;
	public static String Fullsleevestatus = "";
	public static String Fullsleevestatusfail = "";
	public static String FullsleeveDescription1 = "";
	public static String FullsleeveDescription2 = "";
	public static String FullsleeveDescription3 = "";
	public static String FullsleeveDescription4 = "";
	public static String FullsleeveMRP = "";
	public static String FullsleeveColor12 = "";
	public static String Fullsleevehex12 = "";
	public static String FullsleeveFirstcolor2 = "";
	public static String FullsleeveColor22 = "";
	public static String Fullsleevehex22 = "";
	public static String FullsleeveSecondcolor2 = "";
	public static String FullsleeveColor32 = "";
	public static String Fullsleevehex32 = "";
	public static String FullsleeveThirdcolor2 = "";
	public static String FullsleeveColor42 = "";
	public static String Fullsleevehex42 = "";
	public static String FullsleeveFourthcolor2 = "";
	public static String FullsleeveColor13 = "";
	public static String Fullsleevehex13 = "";
	public static String FullsleeveFirstcolor3 = "";
	public static String FullsleeveColor23 = "";
	public static String Fullsleevehex23 = "";
	public static String FullsleeveSecondcolor3 = "";
	public static String FullsleeveColor33 = "";
	public static String Fullsleevehex33 = "";
	public static String FullsleeveThirdcolor3 = "";
	public static String FullsleeveColor43 = "";
	public static String Fullsleevehex43 = "";
	public static String FullsleeveFourthcolor3 = "";
	public static String FullsleeveColor11 = "";
	public static String Fullsleevehex11 = "";
	public static String FullsleeveFirstcolor1 = "";
	public static String FullsleeveColor21 = "";
	public static String Fullsleevehex21 = "";
	public static String FullsleeveSecondcolor1 = "";
	public static String FullsleeveColor31 = "";
	public static String Fullsleevehex31 = "";
	public static String FullsleeveThirdcolor1 = "";
	public static String FullsleeveColor41 = "";
	public static String Fullsleevehex41 = "";
	public static String FullsleeveFourthcolor1 = "";
	public static String Fullsleevecqty = "";
	public static String Fullsleeveiqty = "";
	public static String Fullsleevedqty = "";
	public static String FullsleeveSizemsg = "";
	public static String FullsleevesizeS = "";
	public static String FullsleevesizeM = "";
	public static String FullsleevesizeL = "";
	public static String FullsleevesizeXL = "";
	public static String FullsleevesizeXXL = "";
	public static String FullsleevesizeXXXL = "";
	public static String Fullsleevebelow510 = "";
	public static String Fullsleeveabove510 = "";
	public static String Fullsleevetightfit = "";
	public static String Fullsleevecomfit = "";
	public static String Fullsleevefindursize = "";
	public static String Fullsleeveaddcart = "";
	public static String Fullsleeveupdateproduct1 = "";
	public static String Fullsleeveremovecart = "";
	public FullSleeve()
	{
		Browsers b = new Browsers();
		this.driver = b.getBrowsers("chrome1");		 
	}
	@Test
	public void FullsleeveCheckout() throws Exception
	{
		String name = new Object(){}.getClass().getEnclosingMethod().getName();
		cnt ++;
		try 
		{
			test = extent.createTest("Fullsleeve Module");
			System.out.println("Fullsleeve Module Test Case Executing...");
			commonmethods.Login(driver);
			test.log(Status.INFO, MarkupHelper.createLabel("User able to Login", ExtentColor.BLUE));
			Thread.sleep(3000);
			steps.clickJSButton(home.Mydreamstore, driver, name);
			String homepage = getscreen.capture(driver, name);
			test.addScreenCaptureFromPath(homepage);
			String Parent = driver.getWindowHandle();
			steps.MoveElement(Fullsleeve.Fullsleeve, driver, name);
			String Fullsleeve1 = driver.findElement(Fullsleeve.Fullsleeve).getText();
			System.out.println(Fullsleeve1);
			steps.clickJSButton(Fullsleeve.Fullsleeve, driver, name);
			test.log(Status.INFO, MarkupHelper.createLabel("User clicked on Fullsleeve link", ExtentColor.BLUE));
			Thread.sleep(2000);
			try
			{
				steps.waitForElement(home.Filterpagetext, driver);
				String Filterpage1 = driver.findElement(home.Filterpagetext).getText();
				System.out.println("User navigated to :   "+Filterpage1);
				//steps.WaitUntilElementPresent(Fullsleeve.Fullsleeve_Image1, driver);
				steps.WaitElementPresent(Fullsleeve.Fullsleeve_Image1, driver);
				steps.clickJSButton(Fullsleeve.Fullsleeve_Image1, driver, name);
				test.log(Status.INFO, MarkupHelper.createLabel("User clicked on First Image in T Shirts", ExtentColor.BLUE));
			}
			catch(Exception e )
			{
				System.out.println("User navigated to different landing page");
			}
			Thread.sleep(1000);
			try
			{
				steps.clickJSButton(Fullsleeve.Readmore, driver, name);
				FullsleeveDescription1 = driver.findElement(Fullsleeve.Fullsleeve_Description).getText();
				System.out.println("Product Description:   "+FullsleeveDescription1);
				steps.clickJSButton(Fullsleeve.Readless, driver, name);
				test.log(Status.INFO, MarkupHelper.createLabel("Product Description:  "+ FullsleeveDescription1, ExtentColor.BLUE));
			}
			catch(Exception e)
			{
				steps.clickJSButton(Fullsleeve.Readmore, driver, name);
				FullsleeveDescription2 = driver.findElement(Fullsleeve.Fullsleeve_Description1).getText();
				System.out.println("Product Description:   "+FullsleeveDescription2);
				steps.clickJSButton(Fullsleeve.Readless, driver, name);
				test.log(Status.INFO, MarkupHelper.createLabel("Product Description:  "+FullsleeveDescription2, ExtentColor.BLUE));
			}
			Thread.sleep(1000);
			FullsleeveMRP = driver.findElement(Fullsleeve.Fullsleeve_MRP).getText();
			test.log(Status.INFO, MarkupHelper.createLabel("Product Price:  "+FullsleeveMRP, ExtentColor.ORANGE));
			Thread.sleep(2000);
			try
			{
				String prod2 = driver.findElement(Fullsleeve.Fullsleeve_Product2).getCssValue("color");
				System.out.println("Product:  "+prod2);
				steps.clickJSButton(Fullsleeve.Fullsleeve_Product2, driver, name);
				try
				{
					String col1 = driver.findElement(home2.color1).getCssValue("color");
					System.out.println(col1);
					steps.clickJSButton(home2.color1, driver, name);
					Thread.sleep(2000);
					FullsleeveColor12 = driver.findElement(home2.color1).getCssValue("background-color");
					Fullsleevehex12 = Color.fromString(FullsleeveColor12).asHex();
					System.out.println("Image color: "+Fullsleevehex12);
					test.log(Status.INFO, MarkupHelper.createLabel("User changed item color to:  "+Fullsleevehex12, ExtentColor.BLUE));
					FullsleeveFirstcolor2 = getscreen.capture(driver, "FullsleeveFirstcolor2");
					test.addScreenCaptureFromPath(FullsleeveFirstcolor2);
				}
				catch(Exception e)
				{
					System.out.println("Color 1 is not exists");
				}
				try
				{
					String col2 = driver.findElement(home2.color2).getCssValue("color");
					System.out.println(col2);
					steps.clickJSButton(home2.color2, driver, name);
					Thread.sleep(1000);
					FullsleeveColor22 = driver.findElement(home2.color2).getCssValue("background-color");
					Fullsleevehex22 = Color.fromString(FullsleeveColor22).asHex();
					System.out.println("Image color: "+Fullsleevehex22);
					test.log(Status.INFO, MarkupHelper.createLabel("User changed item color to:  "+Fullsleevehex22, ExtentColor.BLUE));
					FullsleeveSecondcolor2 = getscreen.capture(driver, "FullsleeveSecondcolor2");
					test.addScreenCaptureFromPath(FullsleeveSecondcolor2);
				}
				catch(Exception e)
				{
					System.out.println("Color 2 is not exists");
				}
				try
				{
					String col3 = driver.findElement(home2.color3).getCssValue("color");
					System.out.println(col3);
					steps.clickJSButton(home2.color3, driver, name);
					Thread.sleep(1000);
					FullsleeveColor32 = driver.findElement(home2.color3).getCssValue("background-color");
					Fullsleevehex32 = Color.fromString(FullsleeveColor32).asHex();
					System.out.println("Image color: "+Fullsleevehex32);
					test.log(Status.INFO, MarkupHelper.createLabel("User changed item color to:  "+Fullsleevehex32, ExtentColor.BLUE));
					FullsleeveThirdcolor2 = getscreen.capture(driver, "FullsleeveThirdcolor2");
					test.addScreenCaptureFromPath(FullsleeveThirdcolor2);
				}
				catch(Exception e)
				{
					System.out.println("Color 3 is not exists");
				}
				try 
				{
					String col4 = driver.findElement(home2.color4).getCssValue("color");
					System.out.println(col4);
					steps.clickJSButton(home2.color4, driver, name);
					Thread.sleep(1000);
					FullsleeveColor42 = driver.findElement(home2.color4).getCssValue("background-color");
					Fullsleevehex42 = Color.fromString(FullsleeveColor42).asHex();
					System.out.println("Image color: "+Fullsleevehex42);
					test.log(Status.INFO, MarkupHelper.createLabel("User changed item color to:  "+Fullsleevehex42, ExtentColor.BLUE));
					FullsleeveFourthcolor2 = getscreen.capture(driver, "FullsleeveFourthcolor2");
					test.addScreenCaptureFromPath(FullsleeveFourthcolor2);
				}
				catch(Exception e)
				{
					System.out.println("Color 4 is not exists");
				}
			}
			catch(Exception e)
			{
				System.out.println("Product 2 doesn't exists");
			}
			Thread.sleep(2000);
			try
			{
				String prod3 = driver.findElement(Fullsleeve.Fullsleeve_Product3).getCssValue("color");
				System.out.println("Product:  "+prod3);
				steps.clickJSButton(Fullsleeve.Fullsleeve_Product3, driver, name);
				try
				{
					String col1 = driver.findElement(home2.color1).getCssValue("color");
					System.out.println(col1);
					steps.clickJSButton(home2.color1, driver, name);
					Thread.sleep(2000);
					FullsleeveColor13 = driver.findElement(home2.color1).getCssValue("background-color");
					Fullsleevehex13 = Color.fromString(FullsleeveColor13).asHex();
					System.out.println("Image color: "+Fullsleevehex13);
					test.log(Status.INFO, MarkupHelper.createLabel("User changed item color to:  "+Fullsleevehex13, ExtentColor.BLUE));
					FullsleeveFirstcolor3 = getscreen.capture(driver, "FullsleeveFirstcolor3");
					test.addScreenCaptureFromPath(FullsleeveFirstcolor3);
				}
				catch(Exception e)
				{
					System.out.println("Color 1 is not exists");
				}
				try
				{
					String col2 = driver.findElement(home2.color2).getCssValue("color");
					System.out.println(col2);
					steps.clickJSButton(home2.color2, driver, name);
					Thread.sleep(1000);
					FullsleeveColor23 = driver.findElement(home2.color2).getCssValue("background-color");
					Fullsleevehex23 = Color.fromString(FullsleeveColor23).asHex();
					System.out.println("Image color: "+Fullsleevehex23);
					test.log(Status.INFO, MarkupHelper.createLabel("User changed item color to:  "+Fullsleevehex23, ExtentColor.BLUE));
					FullsleeveSecondcolor3 = getscreen.capture(driver, "FullsleeveSecondcolor3");
					test.addScreenCaptureFromPath(FullsleeveSecondcolor3);
				}
				catch(Exception e)
				{
					System.out.println("Color 2 is not exists");
				}
				try
				{
					String col3 = driver.findElement(home2.color3).getCssValue("color");
					System.out.println(col3);
					steps.clickJSButton(home2.color3, driver, name);
					Thread.sleep(1000);
					FullsleeveColor33 = driver.findElement(home2.color3).getCssValue("background-color");
					Fullsleevehex33 = Color.fromString(FullsleeveColor33).asHex();
					System.out.println("Image color: "+Fullsleevehex33);
					test.log(Status.INFO, MarkupHelper.createLabel("User changed item color to:  "+Fullsleevehex33, ExtentColor.BLUE));
					FullsleeveThirdcolor3 = getscreen.capture(driver, "FullsleeveThirdcolor3");
					test.addScreenCaptureFromPath(FullsleeveThirdcolor3);
				}
				catch(Exception e)
				{
					System.out.println("Color 3 is not exists");
				}
				try 
				{
					String col4 = driver.findElement(home2.color4).getCssValue("color");
					System.out.println(col4);
					steps.clickJSButton(home2.color4, driver, name);
					Thread.sleep(1000);
					FullsleeveColor43 = driver.findElement(home2.color4).getCssValue("background-color");
					Fullsleevehex43 = Color.fromString(FullsleeveColor43).asHex();
					System.out.println("Image color: "+Fullsleevehex43);
					test.log(Status.INFO, MarkupHelper.createLabel("User changed item color to:  "+Fullsleevehex43, ExtentColor.BLUE));
					FullsleeveFourthcolor3 = getscreen.capture(driver, "FullsleeveFourthcolor3");
					test.addScreenCaptureFromPath(FullsleeveFourthcolor3);
				}
				catch(Exception e)
				{
					System.out.println("Color 4 is not exists");
				}
			}
			catch(Exception e)
			{
				System.out.println("Product 3 doesn't exists");
			}
			try
			{
				String prod1 = driver.findElement(Fullsleeve.Fullsleeve_Product1).getCssValue("color");
				System.out.println("Product:  "+prod1);
				steps.clickJSButton(Fullsleeve.Fullsleeve_Product1, driver, name);
				try
				{
					String col1 = driver.findElement(home2.color1).getCssValue("color");
					System.out.println(col1);
					steps.clickJSButton(home2.color1, driver, name);
					Thread.sleep(2000);
					FullsleeveColor11 = driver.findElement(home2.color1).getCssValue("background-color");
					Fullsleevehex11 = Color.fromString(FullsleeveColor11).asHex();
					System.out.println("Image color: "+Fullsleevehex11);
					test.log(Status.INFO, MarkupHelper.createLabel("User changed item color to:  "+Fullsleevehex11, ExtentColor.BLUE));
					FullsleeveFirstcolor1 = getscreen.capture(driver, "FullsleeveFirstcolor1");
					test.addScreenCaptureFromPath(FullsleeveFirstcolor1);
				}
				catch(Exception e)
				{
					System.out.println("Color 1 is not exists");
				}
				try
				{
					String col2 = driver.findElement(home2.color2).getCssValue("color");
					System.out.println(col2);
					steps.clickJSButton(home2.color2, driver, name);
					Thread.sleep(1000);
					FullsleeveColor21 = driver.findElement(home2.color2).getCssValue("background-color");
					Fullsleevehex21 = Color.fromString(FullsleeveColor21).asHex();
					System.out.println("Image color: "+Fullsleevehex21);
					test.log(Status.INFO, MarkupHelper.createLabel("User changed item color to:  "+Fullsleevehex21, ExtentColor.BLUE));
					FullsleeveSecondcolor1 = getscreen.capture(driver, "FullsleeveSecondcolor1");
					test.addScreenCaptureFromPath(FullsleeveSecondcolor1);
				}
				catch(Exception e)
				{
					System.out.println("Color 2 is not exists");
				}
				try
				{
					String col3 = driver.findElement(home2.color3).getCssValue("color");
					System.out.println(col3);
					steps.clickJSButton(home2.color3, driver, name);
					Thread.sleep(1000);
					FullsleeveColor31 = driver.findElement(home2.color3).getCssValue("background-color");
					Fullsleevehex31 = Color.fromString(FullsleeveColor31).asHex();
					System.out.println("Image color: "+Fullsleevehex31);
					test.log(Status.INFO, MarkupHelper.createLabel("User changed item color to:  "+Fullsleevehex31, ExtentColor.BLUE));
					FullsleeveThirdcolor1 = getscreen.capture(driver, "FullsleeveThirdcolor1");
					test.addScreenCaptureFromPath(FullsleeveThirdcolor1);
				}
				catch(Exception e)
				{
					System.out.println("Color 3 is not exists");
				}
				try 
				{
					String col4 = driver.findElement(home2.color4).getCssValue("color");
					System.out.println(col4);
					steps.clickJSButton(home2.color4, driver, name);
					Thread.sleep(1000);
					FullsleeveColor41 = driver.findElement(home2.color4).getCssValue("background-color");
					Fullsleevehex41 = Color.fromString(FullsleeveColor41).asHex();
					System.out.println("Image color: "+Fullsleevehex41);
					test.log(Status.INFO, MarkupHelper.createLabel("User changed item color to:  "+Fullsleevehex41, ExtentColor.BLUE));
					FullsleeveFourthcolor1 = getscreen.capture(driver, "FullsleeveFourthcolor1");
					test.addScreenCaptureFromPath(FullsleeveFourthcolor1);
				}
				catch(Exception e)
				{
					System.out.println("Color 4 is not exists");
				}
			}
			catch(Exception e)
			{
				System.out.println("Product 1 doesn't exists");
			}
			Fullsleevecqty = driver.findElement(home.Qty).getAttribute("value");
			System.out.println("Current Quantity:  "+ Fullsleevecqty);
			test.log(Status.INFO, MarkupHelper.createLabel("Current Quantity of product:  "+Fullsleevecqty, ExtentColor.BLUE));
			Thread.sleep(1000);
			steps.clickJSButton(home.Trend_QtyIncrease, driver, name);
			Fullsleeveiqty = driver.findElement(home.Qty).getAttribute("value");
			System.out.println("Quantity after increase:  "+ Fullsleeveiqty);
			test.log(Status.INFO, MarkupHelper.createLabel("Current Quantity of product after Increase:  "+Fullsleeveiqty, ExtentColor.BLUE));
			steps.clickJSButton(home.Trend_QtyDecrease, driver, name);
			Fullsleevedqty = driver.findElement(home.Qty).getAttribute("value");
			System.out.println("Quantity after decrease:  "+ Fullsleevedqty);
			test.log(Status.INFO, MarkupHelper.createLabel("Current Quantity of product after increase:  "+Fullsleevedqty, ExtentColor.BLUE));
			steps.clickJSButton(home.Trend_Addcart, driver, name);
			FullsleeveSizemsg = driver.findElement(home.Trend_Sizeerrormsg).getText();
			System.out.println("User clicked on Addcart before Size selection:  "+FullsleeveSizemsg);
			test.log(Status.INFO, MarkupHelper.createLabel("User clicked on Addcart before Size selection:  "+FullsleeveSizemsg, ExtentColor.RED));
			//steps.clickJSButton(home2.Fullsleeve_sizeS, driver, name);
			steps.clickJSButton(home2.Size_S, driver, name);
			FullsleevesizeS = driver.findElement(home2.Size_S).getText();
			System.out.println("Selected Size:  "+FullsleevesizeS);
			test.log(Status.INFO, MarkupHelper.createLabel("User selected size:  "+FullsleevesizeS, ExtentColor.BLUE));
			steps.clickJSButton(home2.Size_M, driver, name);
			FullsleevesizeM = driver.findElement(home2.Size_M).getText();
			System.out.println("Selected Size:  "+FullsleevesizeM);
			test.log(Status.INFO, MarkupHelper.createLabel("User selected size:  "+FullsleevesizeM, ExtentColor.BLUE));
			steps.clickJSButton(home2.Size_L, driver, name);
			FullsleevesizeL = driver.findElement(home2.Size_L).getText();
			System.out.println("Selected Size:  "+FullsleevesizeL);
			test.log(Status.INFO, MarkupHelper.createLabel("User selected size:  "+FullsleevesizeL, ExtentColor.BLUE));
			steps.clickJSButton(home2.Size_XL, driver, name);
			FullsleevesizeXL = driver.findElement(home2.Size_XL).getText();
			System.out.println("Selected Size:  "+FullsleevesizeXL);
			test.log(Status.INFO, MarkupHelper.createLabel("User selected size:  "+FullsleevesizeXL, ExtentColor.BLUE));
			steps.clickJSButton(home2.Size_XXL, driver, name);
			FullsleevesizeXXL = driver.findElement(home2.Size_XXL).getText();
			System.out.println("Selected Size:  "+FullsleevesizeXXL);
			test.log(Status.INFO, MarkupHelper.createLabel("User selected size:  "+FullsleevesizeXXL, ExtentColor.BLUE));
			steps.clickJSButton(home2.Size_XXXL, driver, name);
			FullsleevesizeXXXL = driver.findElement(home2.Size_XXXL).getText();
			System.out.println("Selected Size:  "+FullsleevesizeXXXL);
			test.log(Status.INFO, MarkupHelper.createLabel("User selected size:  "+FullsleevesizeXXXL, ExtentColor.BLUE));
			steps.clickJSButton(home.Trend_FindUrSize, driver, name);
			test.log(Status.INFO, MarkupHelper.createLabel("User clicked on Find your size", ExtentColor.BLUE));
			Thread.sleep(1000);
			try
			{
				String text1 =  driver.findElement(home.Trend_FindUrSize_510radio).getText();
				System.out.println(text1);
				steps.clickJSButton(home.Trend_FindUrSize_510radio, driver, name);
				Fullsleevebelow510 = driver.findElement(home.Trend_FindUrSize_510radio).getText();
				System.out.println("User Selected:  "+Fullsleevebelow510);
				test.log(Status.INFO, MarkupHelper.createLabel("User selected Height:  "+Fullsleevebelow510, ExtentColor.BLUE));
				steps.clickJSButton(home.Trend_FindUrSize_above510radio, driver, name);
				Fullsleeveabove510 = driver.findElement(home.Trend_FindUrSize_above510radio).getText();
				System.out.println("User Selected:  "+Fullsleeveabove510);
				test.log(Status.INFO, MarkupHelper.createLabel("User selected Height:  "+Fullsleeveabove510, ExtentColor.BLUE));
				steps.clickJSButton(home.Trend_FindUrSize_Tightradio, driver, name);
				Fullsleevetightfit = driver.findElement(home.Trend_FindUrSize_Tightradio).getText();
				System.out.println("User Selected:  "+Fullsleevetightfit);
				test.log(Status.INFO, MarkupHelper.createLabel("User selected Fit:  "+Fullsleevetightfit, ExtentColor.BLUE));
				steps.clickJSButton(home.Trend_FindUrSize_Comfortradio, driver, name);
				Fullsleevecomfit = driver.findElement(home.Trend_FindUrSize_Comfortradio).getText();
				System.out.println("User Selected:  "+Fullsleevecomfit);
				test.log(Status.INFO, MarkupHelper.createLabel("User selected Fit:  "+Fullsleevecomfit, ExtentColor.BLUE));
				Thread.sleep(1000);
				steps.selectDropdown(home.Trend_FindUrSize_sizeDD, driver, strDataFileName, "size", name);
				test.log(Status.INFO, MarkupHelper.createLabel("User selected shirt size", ExtentColor.BLUE));
				steps.clickJSButton(home.Trend_FindUrSize_submit, driver, name);
				test.log(Status.INFO, MarkupHelper.createLabel("User clicked on Submit", ExtentColor.BLUE));
				Fullsleevefindursize = getscreen.capture(driver, "Fullsleevefindursize");
				test.addScreenCaptureFromPath(Fullsleevefindursize);
				Thread.sleep(1000);
				steps.clickJSButton(home.Trend_FindUrSize_Okbutton, driver, name);
				test.log(Status.INFO, MarkupHelper.createLabel("system displayed size for given metrics", ExtentColor.BLUE));
				steps.clickJSButton(home.Trend_FindUrSize_closebutton, driver, name);
				test.log(Status.INFO, MarkupHelper.createLabel("User navigate to Product details page", ExtentColor.BLUE));
			}
			catch(Exception e)
			{
				steps.clickJSButton(home.Trend_FindUrSize_closebutton, driver, name);
				test.log(Status.INFO, MarkupHelper.createLabel("User navigate to Product details page", ExtentColor.BLUE));
			}
			Thread.sleep(1000);
			steps.clickJSButton(home2.Size_L, driver, name);
			test.log(Status.INFO, MarkupHelper.createLabel("User selected size", ExtentColor.BLUE));
			steps.clickJSButton(home.Trend_Addcart, driver, name);
			Thread.sleep(1000);
			test.log(Status.INFO, MarkupHelper.createLabel("User added product to Cart", ExtentColor.BLUE));
			Fullsleeveaddcart = getscreen.capture(driver, "Fullsleeveaddcart");
			test.addScreenCaptureFromPath(Fullsleeveaddcart);
			Thread.sleep(1000);
			try
			{
				String edit1 = driver.findElement(home2.Addcart_edit).getText();
				System.out.println(edit1);
				steps.clickJSButton(home2.Addcart_edit, driver, name);
				test.log(Status.INFO, MarkupHelper.createLabel("User clicked on Edit", ExtentColor.BLUE));
				steps.selectDropdown(home2.Addcart_selectsize, driver, strDataFileName, "SelectSize", name);
				steps.selectDropdown(home2.Addcart_selectqty, driver, strDataFileName, "Quantity", name);
				test.log(Status.INFO, MarkupHelper.createLabel("User selected Size and Quantity", ExtentColor.BLUE));
				steps.clickJSButton(home2.Addcart_cancel, driver, name);
				steps.clickJSButton(home2.Addcart_edit, driver, name);
				steps.selectDropdown(home2.Addcart_selectsize, driver, strDataFileName, "SelectSize", name);
				steps.selectDropdown(home2.Addcart_selectqty, driver, strDataFileName, "Quantity", name);
				steps.clickJSButton(home2.Addcart_save, driver, name);
				Fullsleeveupdateproduct1 = getscreen.capture(driver, "Fullsleeveupdateproduct1");
				test.addScreenCaptureFromPath(Fullsleeveupdateproduct1);
				test.log(Status.INFO, MarkupHelper.createLabel("User updated product size and quantity", ExtentColor.BLUE));
			}
			catch(Exception e)
			{
				System.out.println("Edit feature is not available for this product");
			}
			Thread.sleep(2000);
			steps.clickJSButton(home2.Biker_remove, driver, name);
			Fullsleeveremovecart = getscreen.capture(driver, "Fullsleeveremovecart");
			test.addScreenCaptureFromPath(Fullsleeveremovecart);
			test.log(Status.INFO, MarkupHelper.createLabel("User removed product from Cart", ExtentColor.BLUE));
			//checkout process
			steps.clickJSButton(home.Size_L, driver, name);
			test.log(Status.INFO, MarkupHelper.createLabel("User selected size", ExtentColor.BLUE));
			steps.clickJSButton(checkout.BuyNow, driver, name);
			test.log(Status.INFO, MarkupHelper.createLabel("User clicked on BuyNow, navigated to checkout page", ExtentColor.BLUE));
			String buynow1 = getscreen.capture(driver, "buynow1");
			test.addScreenCaptureFromPath(buynow1);
			Thread.sleep(2000);
			// Debit card option
			try
			{
				String Debit = driver.findElement(checkout.Debitcard).getText();
				System.out.println("Payment method:  "+Debit);
				steps.Verifytext(checkout.Debitcard, driver, Debit, name);
				steps.clickJSButton(checkout.Debitcard, driver, name);
				System.out.println("Debit card option is available");
				test.log(Status.INFO, MarkupHelper.createLabel("Payment method:  "+Debit   +    "      Available", ExtentColor.BLUE));
			}
			catch(Exception e)
			{
				System.out.println("Debit card option is not available");
				test.log(Status.INFO, MarkupHelper.createLabel("Debit card option is not available", ExtentColor.BLUE));
			}
			Thread.sleep(2000);
			// Credit card option
			try
			{
				String Credit = driver.findElement(checkout.Creditcard).getText();
				System.out.println("Payment method:  "+Credit);
				steps.Verifytext(checkout.Creditcard, driver, Credit, name);
				steps.clickJSButton(checkout.Creditcard, driver, name);
				System.out.println("Credit card option is available");
				test.log(Status.INFO, MarkupHelper.createLabel("Payment method:  "+Credit+    "      Available", ExtentColor.BLUE));
			}
			catch(Exception e)
			{
				System.out.println("Credit card option is not available");
				test.log(Status.INFO, MarkupHelper.createLabel("Credit card option is not available", ExtentColor.BLUE));
			}
			Thread.sleep(2000);
			// Net Banking option
			try
			{
				String NetBanking = driver.findElement(checkout.Netbanking).getText();
				System.out.println("Payment method:  "+NetBanking);
				steps.Verifytext(checkout.Netbanking, driver, NetBanking, name);
				steps.clickJSButton(checkout.Netbanking, driver, name);
				System.out.println("Net Banking option is available");
				test.log(Status.INFO, MarkupHelper.createLabel("Payment method:  "+NetBanking+    "      Available", ExtentColor.BLUE));
			}
			catch(Exception e)
			{
				System.out.println("Net Banking option is not available");
				test.log(Status.INFO, MarkupHelper.createLabel("Net Banking option is not available", ExtentColor.BLUE));
			}
			Thread.sleep(2000);
			// Paytm option
			try
			{
				String Paytm = driver.findElement(checkout.Paytm).getText();
				System.out.println("Payment method:  "+Paytm);
				steps.Verifytext(checkout.Paytm, driver, Paytm, name);
				steps.clickJSButton(checkout.Paytm, driver, name);
				System.out.println("Paytm option is available");
				test.log(Status.INFO, MarkupHelper.createLabel("Payment method:  "+Paytm+    "      Available", ExtentColor.BLUE));
			}
			catch(Exception e)
			{
				System.out.println("Paytm option is not available");
				test.log(Status.INFO, MarkupHelper.createLabel("Paytm option is not available", ExtentColor.BLUE));
			}
			Thread.sleep(2000);
			// MDS Wallet option
			try
			{
				String MDS = driver.findElement(checkout.MDSWallet).getText();
				System.out.println("Payment method:  "+MDS);
				steps.Verifytext(checkout.MDSWallet, driver, MDS, name);
				steps.clickJSButton(checkout.MDSWallet, driver, name);
				Thread.sleep(2000);
				String MDSpopup = driver.findElement(checkout.MDSWalletPOPup).getText();
				System.out.println("Payment method:  "+MDSpopup);
				Thread.sleep(2000);
				steps.clickJSButton(checkout.MDSWalletPOPupOK, driver, name);
				System.out.println("MDS Wallet option is available");
				test.log(Status.INFO, MarkupHelper.createLabel("Payment method:  "+MDS+    "       Available", ExtentColor.BLUE));
			}
			catch(Exception e)
			{
				System.out.println("MDS Wallet option is not available");
				test.log(Status.INFO, MarkupHelper.createLabel("MDS Wallet option is not available", ExtentColor.BLUE));
			}
			Thread.sleep(2000);
			// Cash on delivery 
			try
			{
				String cash = driver.findElement(checkout.CashonDeliveryText).getText();
				System.out.println("Payment method:  "+cash);
				test.log(Status.INFO, MarkupHelper.createLabel("Payment method:  "+cash+    "        Available", ExtentColor.BLUE));
				steps.Verifytext(checkout.CashonDeliveryText, driver, cash, name);
				steps.clickJSButton(checkout.CashonDeliveryradio, driver, name);
				Thread.sleep(2000);
				String buynow2 = driver.findElement(checkout.PopUpSaveMsg).getText();
				System.out.println("Displayed message:   "+buynow2);
				test.log(Status.INFO, MarkupHelper.createLabel("Displayed message:   "+buynow2, ExtentColor.BLUE));
				Thread.sleep(1000);
				String buynow3 = getscreen.capture(driver, "buynow3");
				test.addScreenCaptureFromPath(buynow3);
				steps.clickJSButton(checkout.Cashondelivery, driver, name);
				test.log(Status.INFO, MarkupHelper.createLabel("User clicked on Cash on Delivery link", ExtentColor.BLUE));
			}
			catch(Exception e)
			{
				System.out.println("Cash on Delivery is not available");
				test.log(Status.INFO, MarkupHelper.createLabel("Cash on Delivery is not available", ExtentColor.BLUE));
			}
			Thread.sleep(2000);
			String checkout1 = getscreen.capture(driver, "checkout1");
			test.addScreenCaptureFromPath(checkout1);
			steps.clickJSButton(checkout.CartIcon, driver, name);
			steps.clickJSButton(home.Trend_Addcart_remove, driver, name);
			Thread.sleep(1000);
			test.log(Status.INFO, MarkupHelper.createLabel("User Completed transactions of 1st Image in Fullsleeve", ExtentColor.PURPLE));
			Fullsleevestatus = passstatus;
			Assert.assertTrue(true);
			System.out.println("Fullsleeve Successful");
			testresultdata.put(Integer.toString(cnt), new Object[] {name, "Fullsleeve CheckOut", "Fullsleeve CheckOut Successful",passstatus});	
		}
		catch(Exception e)
		{
			System.out.println("Fullsleeve Unsuccessful");
			testresultdata.put(Integer.toString(cnt), new Object[] {name, "Fullsleeve CheckOut", "Fullsleeve CheckOut Unsuccessful",failstatus});
			Fullsleevestatus = failstatus;
			Fullsleevestatusfail = getscreen.capture(driver, "Fullsleevestatusfail");
			test.addScreenCaptureFromPath(Fullsleevestatusfail);
			Assert.assertTrue(false);
		}
	}
	@BeforeClass
	public void before()
	{
		workbook = new HSSFWorkbook();
		sheet = workbook.createSheet(strDataFileName);
		testresultdata = new LinkedHashMap<String, Object[]>();
		testresultdata.put("1", new Object[] { "Test Step Name", "Expected Result","Actual Result" ,"Status"});
	}
	@AfterClass
	public void after() throws Exception
	{
		Set<String> keyset = testresultdata.keySet();
		int rownum = 0;
		for (String key : keyset) {
			Row row = sheet.createRow(rownum++);
			Object [] objArr = testresultdata.get(key);
			int cellnum = 0;
			for (Object obj : objArr) {
				Cell cell = row.createCell(cellnum++);
				if(obj instanceof Date) 
					cell.setCellValue((Date)obj);
				else if(obj instanceof Boolean)
					cell.setCellValue((Boolean)obj);
				else if(obj instanceof String)
					cell.setCellValue((String)obj);
				else if(obj instanceof Double)
					cell.setCellValue((Double)obj);
			}
		}
		try {
			FileOutputStream out = new FileOutputStream(new File("TestResults/"+strDataFileName +".xls"));
			workbook.write(out);
			out.close();
			System.out.println("Excel written successfully..");
			TestResult();
			driver.quit();
		}
		catch (FileNotFoundException e) 
		{
			e.printStackTrace();
		} 
		catch (IOException e) 
		{
			e.printStackTrace();
		}
	}
	public void TestResult() throws Exception
	{
		HSSFRow row;
		HSSFCell cell;
		WritableFont cellFont;
		WritableCellFormat cellFormat;
		Border border = null;
		int count;
		FileInputStream inp = new FileInputStream( "TestResults/"+strDataFileName +".xls");
		HSSFWorkbook wb = new HSSFWorkbook(inp);
		HSSFSheet sh = wb.getSheetAt(0);
		count=sh.getLastRowNum();
		for (int i = 1; i <= count; i++) 
		{
			row=sh.getRow(i);
			cell=row.getCell(3);
			String status=row.getCell(3).getStringCellValue();
			if(status.equalsIgnoreCase("Pass"))
			{
				HSSFCellStyle pass= wb.createCellStyle();
				pass.setFillForegroundColor(IndexedColors.GREEN.getIndex());
				pass.setFillPattern(CellStyle.SOLID_FOREGROUND);
				cell.setCellStyle(pass);
			}
			else
			{
				HSSFCellStyle fail= wb.createCellStyle();
				fail.setFillForegroundColor(IndexedColors.RED.getIndex());
				fail.setFillPattern(CellStyle.SOLID_FOREGROUND);
				cell.setCellStyle(fail);
			}
		}
		FileOutputStream out = new FileOutputStream("TestResults/"+strDataFileName+".xls");
		wb.write(out);
		wb.close();
	}
}
