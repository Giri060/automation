package Com.Modules;
import java.io.File;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.IOException;
import java.util.LinkedHashMap;
import java.util.List;
import java.util.Map;
import java.util.Set;
import java.util.Date;
import java.util.concurrent.TimeUnit;
import org.apache.poi.hssf.usermodel.HSSFCell;
import org.apache.poi.hssf.usermodel.HSSFCellStyle;
import org.apache.poi.hssf.usermodel.HSSFRow;
import org.apache.poi.hssf.usermodel.HSSFSheet;
import org.apache.poi.hssf.usermodel.HSSFWorkbook;
import org.apache.poi.ss.usermodel.Row;
import org.apache.poi.ss.usermodel.Cell;
import org.apache.poi.ss.usermodel.CellStyle;
import org.apache.poi.ss.usermodel.IndexedColors;
import org.openqa.selenium.By;
import org.openqa.selenium.Keys;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.interactions.Actions;
import org.openqa.selenium.support.Color;
import org.openqa.selenium.support.ui.ExpectedConditions;
import org.openqa.selenium.support.ui.WebDriverWait;
import org.testng.Assert;
import org.testng.annotations.AfterClass;
import org.testng.annotations.BeforeClass;
import org.testng.annotations.Test;
import com.aventstack.extentreports.Status;
import com.aventstack.extentreports.markuputils.ExtentColor;
import com.aventstack.extentreports.markuputils.MarkupHelper;
import Com.ObjectRepository.Homepage2_OR;
import Com.ObjectRepository.Homepage_OR;
import Com.ObjectRepository.Login_OR;
import Com.ObjectRepository.MobileCases_OR;
import Com.ObjectRepository.Tshirts_OR;
import Com.Utilities.Browsers;
import Com.Utilities.CommonMethods;
import Com.Utilities.GetScreenShot;
import Com.Utilities.Screenshot;
import Com.Utilities.Steps;
import Com.Utilities.TestBase;
import jxl.format.Border;
import jxl.format.UnderlineStyle;
import jxl.write.Colour;
import jxl.write.WritableCellFormat;
import jxl.write.WritableFont;
import jxl.write.WriteException;
public class MobileCases extends TestBase
{
	WebDriver driver;
	Login_OR login = new Login_OR();
	Steps steps = new Steps();
	GetScreenShot getscreen = new GetScreenShot();
	Screenshot screen = new Screenshot();
	public String strAbsolutepath = new File("").getAbsolutePath();
	HSSFWorkbook workbook;
	HSSFSheet sheet;
	String strDataFileName = this.getClass().getSimpleName();
	Map<String, Object[]> testresultdata; 
	CommonMethods commonmethods = new CommonMethods();
	Homepage_OR home = new Homepage_OR();
	Homepage2_OR home2 = new Homepage2_OR();
	//Mobilecases_OR Mobilecases = new Mobilecases_OR();
	MobileCases_OR Mobilecases = new MobileCases_OR();
	Screenshot screenshot = new Screenshot();
	String passstatus = screenshot.status("PASS");
	String failstatus = screenshot.status("FAIL");
	int cnt = 2;
	public static String Mobilecasesstatus = "";
	public static String Mobilecasesstatusfail = "";
	public static String MobilecasesDescription1 = "";
	public static String MobilecasesDescription2 = "";
	public static String MobilecasesDescription3 = "";
	public static String MobilecasesDescription4 = "";
	public static String MobilecasesDescription5 = "";
	public static String MobilecasesMRP = "";
	public static String MobilecasesProduct1 = "";
	public static String MobilecasesProduct2 = "";
	public static String MobilecasesProduct3 = "";
	public static String MobilecasesProduct4 = "";
	public static String MobilecasesProduct5 = "";
	public static String MobilecasesProduct6 = "";
	public static String MobilecasesProduct7 = "";
	public static String MobilecasesProduct8 = "";
	public static String Mobilecasescqty = "";
	public static String Mobilecasesiqty = "";
	public static String Mobilecasesdqty = "";
	public static String Mobilecasesaddcart = "";
	public static String Mobilecasesupdateproduct1 = "";
	public static String Mobilecasesremovecart = "";
	public static String currentproduct1 = "";
	public MobileCases()
	{
		Browsers b = new Browsers();
		this.driver = b.getBrowsers("chrome1");		 
	}
	@Test
	public void MobilecasesCheckout() throws Exception
	{
		String name = new Object(){}.getClass().getEnclosingMethod().getName();
		cnt ++;
		try 
		{
			test = extent.createTest("Mobile cases");
			commonmethods.Login(driver);
			test.log(Status.INFO, MarkupHelper.createLabel("User able to Login", ExtentColor.BLUE));
			Thread.sleep(3000);
			steps.clickJSButton(home.Mydreamstore, driver, name);
			String homepage = getscreen.capture(driver, name);
			test.addScreenCaptureFromPath(homepage);
			String Parent = driver.getWindowHandle();
			steps.MoveElement(Mobilecases.Mobilecaseslink, driver, name);
			steps.clickJSButton(Mobilecases.Mobilecaseslink, driver, name);
			test.log(Status.INFO, MarkupHelper.createLabel("User clicked on Mobilecases link", ExtentColor.BLUE));
			Thread.sleep(2000);
			steps.WaitUntilElementPresent(Mobilecases.Mobilecases_Image1, driver);
			/*steps.WaitElementPresent(Mobilecases.Mobilecases_Image1, driver);
			steps.clickJSButton(Mobilecases.Mobilecases_Image1, driver, name);*/
			test.log(Status.INFO, MarkupHelper.createLabel("User clicked on First Image in Mobilecases", ExtentColor.BLUE));
			try
			{
				steps.clickJSButton(Mobilecases.Readmore, driver, name);
				MobilecasesDescription1 = driver.findElement(Mobilecases.Mobilecases_Description1).getText();
				MobilecasesDescription2 = driver.findElement(Mobilecases.Mobilecases_Description2).getText();
				MobilecasesDescription3 = driver.findElement(Mobilecases.Mobilecases_Description3).getText();
				steps.clickJSButton(Mobilecases.Readless, driver, name);
				test.log(Status.INFO, MarkupHelper.createLabel("Product Description:  "+ MobilecasesDescription1+MobilecasesDescription2+MobilecasesDescription3, ExtentColor.BLUE));
			}
			catch(Exception e)
			{
				try
				{
					MobilecasesDescription4 = driver.findElement(Mobilecases.Mobilecases_Description4).getText();
					test.log(Status.INFO, MarkupHelper.createLabel("Product Description:  "+ MobilecasesDescription4, ExtentColor.BLUE));
				}
				catch(Exception e1)
				{
					MobilecasesDescription5 = driver.findElement(Mobilecases.Mobilecases_Description5).getText();
					test.log(Status.INFO, MarkupHelper.createLabel("Product Description:  "+ MobilecasesDescription5, ExtentColor.BLUE));
				}
			}
			MobilecasesMRP = driver.findElement(Mobilecases.Mobilecases_MRP).getText();
			test.log(Status.INFO, MarkupHelper.createLabel("Product Price:  "+MobilecasesMRP, ExtentColor.BLUE));
			Thread.sleep(2000);
			currentproduct1 = driver.findElement(Mobilecases.Mobilecases_ProductDD).getText();
			System.out.println("Product:  "+currentproduct1);
			Thread.sleep(1000);
			steps.selectDropdownIndex(Mobilecases.Mobilecases_ProductDD, driver, 3, name);
			Thread.sleep(2000);
			MobilecasesProduct2 = getscreen.capture(driver, "MobilecasesProduct2");
			test.addScreenCaptureFromPath(MobilecasesProduct2);
			Thread.sleep(1000);
			steps.selectDropdownIndex(Mobilecases.Mobilecases_ProductDD, driver, 7, name);
			Thread.sleep(2000);
			MobilecasesProduct3 = getscreen.capture(driver, "MobilecasesProduct3");
			test.addScreenCaptureFromPath(MobilecasesProduct3);
			Thread.sleep(1000);
			steps.selectDropdownIndex(Mobilecases.Mobilecases_ProductDD, driver, 11, name);
			Thread.sleep(2000);
			MobilecasesProduct4 = getscreen.capture(driver, "MobilecasesProduct4");
			test.addScreenCaptureFromPath(MobilecasesProduct4);
			Thread.sleep(1000);
			steps.selectDropdownIndex(Mobilecases.Mobilecases_ProductDD, driver, 12, name);
			Thread.sleep(2000);
			MobilecasesProduct5 = getscreen.capture(driver, "MobilecasesProduct5");
			test.addScreenCaptureFromPath(MobilecasesProduct5);
			Thread.sleep(1000);
			steps.selectDropdownIndex(Mobilecases.Mobilecases_ProductDD, driver, 17, name);
			Thread.sleep(2000);
			MobilecasesProduct6 = getscreen.capture(driver, "MobilecasesProduct6");
			test.addScreenCaptureFromPath(MobilecasesProduct6);
			Thread.sleep(1000);
			steps.selectDropdownIndex(Mobilecases.Mobilecases_ProductDD, driver, 21, name);
			Thread.sleep(2000);
			MobilecasesProduct6 = getscreen.capture(driver, "MobilecasesProduct6");
			test.addScreenCaptureFromPath(MobilecasesProduct6);
			Thread.sleep(1000);
			steps.selectDropdownIndex(Mobilecases.Mobilecases_ProductDD, driver, 27, name);
			Thread.sleep(2000);
			MobilecasesProduct7 = getscreen.capture(driver, "MobilecasesProduct7");
			test.addScreenCaptureFromPath(MobilecasesProduct7);
			Thread.sleep(1000);
			steps.selectDropdownIndex(Mobilecases.Mobilecases_ProductDD, driver, 0, name);
			Thread.sleep(2000);
			MobilecasesProduct8 = getscreen.capture(driver, "MobilecasesProduct8");
			test.addScreenCaptureFromPath(MobilecasesProduct8);
			Mobilecasescqty = driver.findElement(home.Qty).getAttribute("value");
			System.out.println("Current Quantity:  "+ Mobilecasescqty);
			test.log(Status.INFO, MarkupHelper.createLabel("Current Quantity of product:  "+Mobilecasescqty, ExtentColor.BLUE));
			Thread.sleep(1000);
			steps.clickJSButton(home.Trend_QtyIncrease, driver, name);
			Mobilecasesiqty = driver.findElement(home.Qty).getAttribute("value");
			System.out.println("Quantity after increase:  "+ Mobilecasesiqty);
			test.log(Status.INFO, MarkupHelper.createLabel("Current Quantity of product after Increase:  "+Mobilecasesiqty, ExtentColor.BLUE));
			steps.clickJSButton(home.Trend_QtyDecrease, driver, name);
			Mobilecasesdqty = driver.findElement(home.Qty).getAttribute("value");
			System.out.println("Quantity after decrease:  "+ Mobilecasesdqty);
			test.log(Status.INFO, MarkupHelper.createLabel("Current Quantity of product after increase:  "+Mobilecasesdqty, ExtentColor.BLUE));
			steps.clickJSButton(home.Trend_Addcart, driver, name);
			Thread.sleep(1000);
			test.log(Status.INFO, MarkupHelper.createLabel("User added product to Cart", ExtentColor.BLUE));
			Mobilecasesaddcart = getscreen.capture(driver, "Mobilecasesaddcart");
			test.addScreenCaptureFromPath(Mobilecasesaddcart);
			Thread.sleep(1000);
			steps.clickJSButton(home2.Addcart_edit, driver, name);
			test.log(Status.INFO, MarkupHelper.createLabel("User clicked on Edit", ExtentColor.BLUE));
			//steps.selectDropdown(home2.Addcart_selectsize, driver, strDataFileName, "SelectSize", name);
			steps.selectDropdown(home2.Addcart_selectqty, driver, strDataFileName, "Quantity", name);
			test.log(Status.INFO, MarkupHelper.createLabel("User selected Size and Quantity", ExtentColor.BLUE));
			steps.clickJSButton(home2.Addcart_cancel, driver, name);
			steps.clickJSButton(home2.Addcart_edit, driver, name);
			//steps.selectDropdown(home2.Addcart_selectsize, driver, strDataFileName, "SelectSize", name);
			steps.selectDropdown(home2.Addcart_selectqty, driver, strDataFileName, "Quantity", name);
			steps.clickJSButton(home2.Addcart_save, driver, name);
			Mobilecasesupdateproduct1 = getscreen.capture(driver, "Mobilecasesupdateproduct1");
			test.addScreenCaptureFromPath(Mobilecasesupdateproduct1);
			test.log(Status.INFO, MarkupHelper.createLabel("User updated product size and quantity", ExtentColor.BLUE));
			Thread.sleep(2000);
			steps.clickJSButton(home2.Biker_remove, driver, name);
			Mobilecasesremovecart = getscreen.capture(driver, "Mobilecasesremovecart");
			test.addScreenCaptureFromPath(Mobilecasesremovecart);
			test.log(Status.INFO, MarkupHelper.createLabel("User removed product from Cart", ExtentColor.BLUE));
			test.log(Status.INFO, MarkupHelper.createLabel("User Completed transactions of 1st Image in Mobilecases", ExtentColor.PURPLE));
			Mobilecasesstatus = passstatus;
			Assert.assertTrue(true);
			System.out.println("Mobilecases Successful");
			testresultdata.put(Integer.toString(cnt), new Object[] {name, "Mobilecases Checkout", "Mobilecases Checkout Successful",passstatus});	
		}
		catch(Exception e)
		{
			System.out.println("Mobilecases Checkout Unsuccessful");
			testresultdata.put(Integer.toString(cnt), new Object[] {name, "Mobilecases Checkout", "Mobilecases Checkout Unsuccessful",failstatus});
			Mobilecasesstatus = failstatus;
			Mobilecasesstatusfail = getscreen.capture(driver, "Mobilecasesstatusfail");
			test.addScreenCaptureFromPath(Mobilecasesstatusfail);
			Assert.assertTrue(false);
		}
	}
	@BeforeClass
	public void before()
	{
		workbook = new HSSFWorkbook();
		sheet = workbook.createSheet(strDataFileName);
		testresultdata = new LinkedHashMap<String, Object[]>();
		testresultdata.put("1", new Object[] { "Test Step Name", "Expected Result","Actual Result" ,"Status"});
	}
	@AfterClass
	public void after() throws Exception
	{
		Set<String> keyset = testresultdata.keySet();
		int rownum = 0;
		for (String key : keyset) {
			Row row = sheet.createRow(rownum++);
			Object [] objArr = testresultdata.get(key);
			int cellnum = 0;
			for (Object obj : objArr) {
				Cell cell = row.createCell(cellnum++);
				if(obj instanceof Date) 
					cell.setCellValue((Date)obj);
				else if(obj instanceof Boolean)
					cell.setCellValue((Boolean)obj);
				else if(obj instanceof String)
					cell.setCellValue((String)obj);
				else if(obj instanceof Double)
					cell.setCellValue((Double)obj);
			}
		}
		try {
			FileOutputStream out = new FileOutputStream(new File("TestResults/"+strDataFileName +".xls"));
			workbook.write(out);
			out.close();
			System.out.println("Excel written successfully..");
			TestResult();
			driver.quit();
		}
		catch (FileNotFoundException e) 
		{
			e.printStackTrace();
		} 
		catch (IOException e) 
		{
			e.printStackTrace();
		}
	}
	public void TestResult() throws Exception
	{
		HSSFRow row;
		HSSFCell cell;
		WritableFont cellFont;
		WritableCellFormat cellFormat;
		Border border = null;
		int count;
		FileInputStream inp = new FileInputStream( "TestResults/"+strDataFileName +".xls");
		HSSFWorkbook wb = new HSSFWorkbook(inp);
		HSSFSheet sh = wb.getSheetAt(0);
		count=sh.getLastRowNum();
		for (int i = 1; i <= count; i++) 
		{
			row=sh.getRow(i);
			cell=row.getCell(3);
			String status=row.getCell(3).getStringCellValue();
			if(status.equalsIgnoreCase("Pass"))
			{
				HSSFCellStyle pass= wb.createCellStyle();
				pass.setFillForegroundColor(IndexedColors.GREEN.getIndex());
				pass.setFillPattern(CellStyle.SOLID_FOREGROUND);
				cell.setCellStyle(pass);
			}
			else
			{
				HSSFCellStyle fail= wb.createCellStyle();
				fail.setFillForegroundColor(IndexedColors.RED.getIndex());
				fail.setFillPattern(CellStyle.SOLID_FOREGROUND);
				cell.setCellStyle(fail);
			}
		}
		FileOutputStream out = new FileOutputStream("TestResults/"+strDataFileName+".xls");
		wb.write(out);
		wb.close();
	}
}
