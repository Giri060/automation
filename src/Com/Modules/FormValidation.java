package Com.Modules;
import java.io.File;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.IOException;
import java.util.Date;
import java.util.LinkedHashMap;
import java.util.Map;
import java.util.Set;
import org.apache.poi.hssf.usermodel.HSSFCell;
import org.apache.poi.hssf.usermodel.HSSFCellStyle;
import org.apache.poi.hssf.usermodel.HSSFRow;
import org.apache.poi.hssf.usermodel.HSSFSheet;
import org.apache.poi.hssf.usermodel.HSSFWorkbook;
import org.apache.poi.ss.usermodel.Cell;
import org.apache.poi.ss.usermodel.CellStyle;
import org.apache.poi.ss.usermodel.IndexedColors;
import org.apache.poi.ss.usermodel.Row;
import org.openqa.selenium.WebDriver;
import org.testng.Assert;
import org.testng.annotations.AfterClass;
import org.testng.annotations.BeforeClass;
import org.testng.annotations.Test;
import com.aventstack.extentreports.Status;
import com.aventstack.extentreports.markuputils.ExtentColor;
import com.aventstack.extentreports.markuputils.MarkupHelper;
import Com.ObjectRepository.Checkout_OR;
import Com.ObjectRepository.Homepage2_OR;
import Com.ObjectRepository.Homepage_OR;
import Com.ObjectRepository.Login_OR;
import Com.Utilities.Browsers;
import Com.Utilities.CommonMethods;
import Com.Utilities.GetScreenShot;
import Com.Utilities.Screenshot;
import Com.Utilities.Steps;
import Com.Utilities.TestBase;
import jxl.format.Border;
import jxl.write.WritableCellFormat;
import jxl.write.WritableFont;
public class FormValidation extends TestBase
{
	WebDriver driver;
	Login_OR login = new Login_OR();
	Steps steps = new Steps();
	GetScreenShot getscreen = new GetScreenShot();
	Screenshot screen = new Screenshot();
	public String strAbsolutepath = new File("").getAbsolutePath();
	HSSFWorkbook workbook;
	HSSFSheet sheet;
	String strDataFileName = this.getClass().getSimpleName();
	Map<String, Object[]> testresultdata; 
	CommonMethods commonmethods = new CommonMethods();
	Homepage_OR home = new Homepage_OR();
	Homepage2_OR home2 = new Homepage2_OR();
	Checkout_OR checkout = new Checkout_OR();
	Screenshot screenshot = new Screenshot();
	String passstatus = screenshot.status("PASS");
	String failstatus = screenshot.status("FAIL");
	int cnt = 2;
	public static String Formvalidstatus = "";
	public static String Formvalid1 = "";
	public static String Formvalid2 = "";
	public static String Formvalid3 = "";
	public static String Formvalid4 = "";
	public static String Formvalid5 = "";
	public static String Formvalid6 = "";
	public static String Formvalid7 = "";
	public static String Formvalid8 = "";
	public static String Formvalid9 = "";
	public static String Formvalid10 = "";
	public static String Formvalid11 = "";
	public static String Formvalid12 = "";
	public static String Formvalidstatusfail = "";
	public static String Formvalid13 = "";
	public static String Formvalid14 = "";
	public static String Formvalid15 = "";
	public static String Formvalid16 = "";
	public static String Formvalid17 = "";
	public static String Formvalid18 = "";
	public static String Formvalid19 = "";
	public static String Formvalid20 = "";
	public static String Formvalid21 = "";
	public static String Formvalid22 = "";
	public static String Formvalid23 = "";
	public static String Formvalid24 = "";
	public static String Formvalid25 = "";
	public static String Formvalid26 = "";
	public static String Formvalid27 = "";
	public static String Formvalid28 = "";
	public static String Formvalid29 = "";
	public static String Formvalid30 = "";
	public static String Formvalid31 = "";
	public static String Formvalid32 = "";
	public FormValidation()
	{
		Browsers b = new Browsers();
		this.driver = b.getBrowsers("chrome1");		 
	}
	@Test
	public void FormFillValidation() throws IOException, InterruptedException
	{
		String name = new Object(){}.getClass().getEnclosingMethod().getName();
		cnt ++;
		try
		{
			test = extent.createTest("Form Validation");
			System.out.println("Form Validation Test Case Executing...");
			/*	commonmethods.Login(driver);
			test.log(Status.INFO, MarkupHelper.createLabel("User able to Login", ExtentColor.BLUE));
			Thread.sleep(1000);*/
			Thread.sleep(2000);
			commonmethods.URLNaviagtion(driver);
			Thread.sleep(2000);
			String Parent = driver.getWindowHandle();
			steps.clickButton(home.Mydreamstore, driver, name);
			Thread.sleep(2000);
			steps.clickButton(home2.Checkout_Image, driver, name);
			steps.ChildWindow(driver);
			driver.manage().window().maximize();
			Formvalid1 = getscreen.capture(driver, "Formvalid1");
			test.addScreenCaptureFromPath(Formvalid1);
			Thread.sleep(1000);
			steps.clickButton(home.Size_L, driver, name);
			test.log(Status.INFO, MarkupHelper.createLabel("User selected size", ExtentColor.BLUE));
			steps.clickButton(checkout.BuyNow, driver, name);
			test.log(Status.INFO, MarkupHelper.createLabel("User clicked on BuyNow, navigated to checkout page", ExtentColor.BLUE));
			Formvalid2 = getscreen.capture(driver, "Formvalid2");
			test.addScreenCaptureFromPath(Formvalid2);
			Thread.sleep(2000);
			steps.clickButton(checkout.Placeorder, driver, name);
			Formvalid3 = getscreen.capture(driver, "Formvalid3");
			test.addScreenCaptureFromPath(Formvalid3);
			try
			{
				Formvalid4 = driver.findElement(checkout.FirstNameerror).getText();
				System.out.println(Formvalid4);
				test.log(Status.INFO, MarkupHelper.createLabel("Error message:   "+Formvalid4, ExtentColor.RED));
				steps.EnterText(checkout.FirstName, driver, strDataFileName, "FirstnameNum", name);
				Formvalid13 = driver.findElement(checkout.FirstName).getAttribute("value");
				System.out.println(Formvalid13);
				test.log(Status.INFO, MarkupHelper.createLabel("Entered Data:   "+Formvalid13, ExtentColor.ORANGE));
				steps.clickButton(checkout.Placeorder, driver, name);
				steps.Clear(checkout.FirstName, driver, name);
				steps.EnterText(checkout.FirstName, driver, strDataFileName, "FirstnameChar", name);
				Formvalid14 = driver.findElement(checkout.FirstName).getAttribute("value");
				System.out.println(Formvalid14);
				test.log(Status.INFO, MarkupHelper.createLabel("Entered Data:   "+Formvalid14, ExtentColor.ORANGE));
				steps.clickButton(checkout.Placeorder, driver, name);
				steps.Clear(checkout.FirstName, driver, name);
				steps.EnterText(checkout.FirstName, driver, strDataFileName, "Firstname", name);
				Formvalid15 = driver.findElement(checkout.FirstName).getAttribute("value");
				System.out.println(Formvalid15);
				test.log(Status.INFO, MarkupHelper.createLabel("Entered Data:   "+Formvalid15, ExtentColor.ORANGE));
				steps.clickButton(checkout.Placeorder, driver, name);
			}
			catch(Exception e )
			{
				System.out.println("Firstname error message is not displayed");
			}
			try
			{
				Formvalid5 = driver.findElement(checkout.MobileNumberError).getText();
				System.out.println(Formvalid5);
				test.log(Status.INFO, MarkupHelper.createLabel("Error message:   "+Formvalid5, ExtentColor.RED));
				steps.EnterText(checkout.MobileNumber, driver, strDataFileName, "MobileAlphabets", name);
				steps.clickButton(checkout.Placeorder, driver, name);
				Formvalid16 = driver.findElement(checkout.MobileNumberError).getText();
				Formvalid27 = driver.findElement(checkout.MobileNumber).getAttribute("value");
				System.out.println(Formvalid16+Formvalid27);
				test.log(Status.INFO, MarkupHelper.createLabel("Entered Data:   "+Formvalid16  +"   "+ "Error Message:   "+Formvalid27, ExtentColor.ORANGE));
				steps.Clear(checkout.MobileNumber, driver, name);
				steps.EnterText(checkout.MobileNumber, driver, strDataFileName, "MobileChar", name);
				steps.clickButton(checkout.Placeorder, driver, name);
				Formvalid17 = driver.findElement(checkout.MobileNumber).getAttribute("value");
				Formvalid28 = driver.findElement(checkout.MobileNumberError).getText();
				System.out.println(Formvalid17);
				test.log(Status.INFO, MarkupHelper.createLabel("Entered Data:   "+Formvalid17 +"   "+ "Error Message:   "+Formvalid28, ExtentColor.ORANGE));
				steps.Clear(checkout.MobileNumber, driver, name);
				steps.EnterText(checkout.MobileNumber, driver, strDataFileName, "Mobile", name);
				steps.clickButton(checkout.Placeorder, driver, name);
				Formvalid18 = driver.findElement(checkout.MobileNumber).getAttribute("value");
				System.out.println(Formvalid18);
				test.log(Status.INFO, MarkupHelper.createLabel("Entered Data:   "+Formvalid18, ExtentColor.ORANGE));
			}
			catch(Exception e )
			{
				System.out.println("Mobile Number error message is not displayed");
			}
			try
			{
				Formvalid6 = driver.findElement(checkout.EmailError).getText();
				System.out.println(Formvalid6);
				test.log(Status.INFO, MarkupHelper.createLabel("Error message:   "+Formvalid6, ExtentColor.RED));
				steps.EnterText(checkout.Email, driver, strDataFileName, "EmailNum", name);
				steps.clickButton(checkout.Placeorder, driver, name);
				Formvalid19 = driver.findElement(checkout.Email).getAttribute("value");
				Formvalid29 = driver.findElement(checkout.EmailError).getText();
				System.out.println(Formvalid19);
				test.log(Status.INFO, MarkupHelper.createLabel("Entered Data:   "+Formvalid19  +"   "+ "Error Message:   "+Formvalid29, ExtentColor.ORANGE));
				steps.Clear(checkout.Email, driver, name);
				steps.EnterText(checkout.Email, driver, strDataFileName, "EmailChar", name);
				steps.clickButton(checkout.Placeorder, driver, name);
				Formvalid20 = driver.findElement(checkout.Email).getAttribute("value");
				Formvalid30 = driver.findElement(checkout.EmailError).getText();
				System.out.println(Formvalid20);
				test.log(Status.INFO, MarkupHelper.createLabel("Entered Data:   "+Formvalid20  +"   "+ "Error Message:   "+Formvalid30, ExtentColor.ORANGE));
				steps.Clear(checkout.Email, driver, name);
				steps.EnterText(checkout.Email, driver, strDataFileName, "Email", name);
				steps.clickButton(checkout.Placeorder, driver, name);
				Formvalid21 = driver.findElement(checkout.Email).getAttribute("value");
				System.out.println(Formvalid21);
				test.log(Status.INFO, MarkupHelper.createLabel("Entered Data:   "+Formvalid21, ExtentColor.ORANGE));
			}
			catch(Exception e )
			{
				System.out.println("Email error message is not displayed");
			}
			try
			{
				Formvalid7 = driver.findElement(checkout.ZipcodeError).getText();
				System.out.println(Formvalid7);
				test.log(Status.INFO, MarkupHelper.createLabel("Error message:   "+Formvalid7, ExtentColor.RED));
				steps.EnterText(checkout.Zipcode, driver, strDataFileName, "ZipcodeAlpha", name);
				steps.clickButton(checkout.Placeorder, driver, name);
				Formvalid23 = driver.findElement(checkout.Zipcode).getAttribute("value");
				Formvalid31 = driver.findElement(checkout.ZipcodeError).getText();
				System.out.println(Formvalid23);
				test.log(Status.INFO, MarkupHelper.createLabel("Entered Data:   "+Formvalid23 +"   "+ "Error message:  "+Formvalid31 , ExtentColor.ORANGE));
				steps.Clear(checkout.Zipcode, driver, name);
				steps.EnterText(checkout.Zipcode, driver, strDataFileName, "ZipcodeChar", name);
				steps.clickButton(checkout.Placeorder, driver, name);
				Formvalid22 = driver.findElement(checkout.ZipcodeError).getText();
				Formvalid32 = driver.findElement(checkout.Zipcode).getAttribute("value");
				System.out.println(Formvalid22);
				test.log(Status.INFO, MarkupHelper.createLabel("Entered Data:   "+Formvalid32  +"   "+ "Error message:  "+Formvalid22, ExtentColor.ORANGE));
				steps.Clear(checkout.Zipcode, driver, name);
				steps.EnterText(checkout.Zipcode, driver, strDataFileName, "Zipcode", name);
				steps.clickButton(checkout.Placeorder, driver, name);
				Formvalid24 = driver.findElement(checkout.Zipcode).getAttribute("value");
				System.out.println(Formvalid24);
				test.log(Status.INFO, MarkupHelper.createLabel("Entered Data:   "+Formvalid24, ExtentColor.ORANGE));
			}
			catch(Exception e )
			{
				System.out.println("Zipcode error message is not displayed");
			}
			try
			{
				Formvalid8 = driver.findElement(checkout.AddressError).getText();
				System.out.println(Formvalid8);
				test.log(Status.INFO, MarkupHelper.createLabel("Error message:   "+Formvalid8, ExtentColor.RED));
				steps.EnterText(checkout.Address, driver, strDataFileName, "AddressChar", name);
				steps.clickButton(checkout.Placeorder, driver, name);
				Formvalid25 = driver.findElement(checkout.Address).getAttribute("value");
				System.out.println(Formvalid25);
				test.log(Status.INFO, MarkupHelper.createLabel("Entered Data:   "+Formvalid25, ExtentColor.ORANGE));
				steps.Clear(checkout.Address, driver, name);
				steps.EnterText(checkout.Address, driver, strDataFileName, "Address", name);
				steps.clickButton(checkout.Placeorder, driver, name);
				Formvalid26 = driver.findElement(checkout.Address).getAttribute("value");
				System.out.println(Formvalid26);
				test.log(Status.INFO, MarkupHelper.createLabel("Entered Data:   "+Formvalid26, ExtentColor.ORANGE));
			}
			catch(Exception e )
			{
				System.out.println("Address 1  error message is not displayed");
			}
			try
			{
				Formvalid9 = driver.findElement(checkout.Address2Error).getText();
				System.out.println(Formvalid9);
				test.log(Status.INFO, MarkupHelper.createLabel("Error message:   "+Formvalid9, ExtentColor.RED));
				steps.EnterText(checkout.Address2, driver, strDataFileName, "Address2Char", name);
				steps.clickButton(checkout.Placeorder, driver, name);
				Formvalid26 = driver.findElement(checkout.Address2).getAttribute("value");
				System.out.println(Formvalid26);
				test.log(Status.INFO, MarkupHelper.createLabel("Entered Data:   "+Formvalid26, ExtentColor.ORANGE));
				steps.Clear(checkout.Address2, driver, name);
				steps.EnterText(checkout.Address2, driver, strDataFileName, "Address2", name);
				steps.clickButton(checkout.Placeorder, driver, name);
				Formvalid26 = driver.findElement(checkout.Address2).getAttribute("value");
				System.out.println(Formvalid26);
				test.log(Status.INFO, MarkupHelper.createLabel("Entered Data:   "+Formvalid26, ExtentColor.ORANGE));
			}
			catch(Exception e )
			{
				System.out.println("Address 2  error message is not displayed");
			}
/*			try
			{
				Formvalid10 = driver.findElement(checkout.CityError).getText();
				System.out.println(Formvalid10);
				test.log(Status.INFO, MarkupHelper.createLabel("Error message:   "+Formvalid10, ExtentColor.RED));
				steps.clickButton(checkout.Placeorder, driver, name);
			}
			catch(Exception e )
			{
				System.out.println("City  error message is not displayed");
			}
			try
			{
				Formvalid11 = driver.findElement(checkout.StateError).getText();
				System.out.println(Formvalid11);
				test.log(Status.INFO, MarkupHelper.createLabel("Error message:   "+Formvalid11, ExtentColor.RED));
				steps.clickButton(checkout.Placeorder, driver, name);
			}
			catch(Exception e )
			{
				System.out.println("State  error message is not displayed");
			}
*/
			Thread.sleep(1000);
			Formvalid12 = getscreen.capture(driver, "Formvalid12");
			test.addScreenCaptureFromPath(Formvalid12);
		//	steps.clickButton(checkout.CartIcon, driver, name);
		//	steps.clickButton(home.Trend_Addcart_remove, driver, name);
			Thread.sleep(1000);
			Formvalidstatus = passstatus;
			Assert.assertTrue(true);
			System.out.println("Form Validation Successful");
			testresultdata.put(Integer.toString(cnt), new Object[] {name, "Form Validation", "Form Validation Successful",passstatus});
		}
		catch(Exception e)
		{
			System.out.println("Form Validation Unsuccessful");
			testresultdata.put(Integer.toString(cnt), new Object[] {name, "Form Validation", "Form Validation Unsuccessful",failstatus});
			Formvalidstatus = failstatus;
			Formvalidstatusfail = getscreen.capture(driver, "Formvalidstatusfail");
			test.addScreenCaptureFromPath(Formvalidstatusfail);
			driver.close();
			Assert.assertTrue(false);		
		}
	}
	@BeforeClass
	public void before()
	{
		workbook = new HSSFWorkbook();
		sheet = workbook.createSheet(strDataFileName);
		testresultdata = new LinkedHashMap<String, Object[]>();
		testresultdata.put("1", new Object[] { "Test Step Name", "Expected Result","Actual Result" ,"Status"});
	}
	@AfterClass
	public void after() throws Exception
	{
		Set<String> keyset = testresultdata.keySet();
		int rownum = 0;
		for (String key : keyset) {
			Row row = sheet.createRow(rownum++);
			Object [] objArr = testresultdata.get(key);
			int cellnum = 0;
			for (Object obj : objArr) {
				Cell cell = row.createCell(cellnum++);
				if(obj instanceof Date) 
					cell.setCellValue((Date)obj);
				else if(obj instanceof Boolean)
					cell.setCellValue((Boolean)obj);
				else if(obj instanceof String)
					cell.setCellValue((String)obj);
				else if(obj instanceof Double)
					cell.setCellValue((Double)obj);
			}
		}
		try 
		{
			FileOutputStream out = new FileOutputStream(new File("TestResults/"+strDataFileName +".xls"));
			workbook.write(out);
			out.close();
			System.out.println("Excel written successfully..");
			TestResult();
			driver.quit();
		}
		catch (FileNotFoundException e) 
		{
			e.printStackTrace();
		} 
		catch (IOException e) 
		{
			e.printStackTrace();
		}
	}
	public void TestResult() throws Exception
	{
		HSSFRow row;
		HSSFCell cell;
		WritableFont cellFont;
		WritableCellFormat cellFormat;
		Border border = null;
		int count;
		FileInputStream inp = new FileInputStream( "TestResults/"+strDataFileName +".xls");
		HSSFWorkbook wb = new HSSFWorkbook(inp);
		HSSFSheet sh = wb.getSheetAt(0);
		count=sh.getLastRowNum();
		for (int i = 1; i <= count; i++) 
		{
			row=sh.getRow(i);
			cell=row.getCell(3);
			String status=row.getCell(3).getStringCellValue();
			if(status.equalsIgnoreCase("Pass"))
			{
				HSSFCellStyle pass= wb.createCellStyle();
				pass.setFillForegroundColor(IndexedColors.GREEN.getIndex());
				pass.setFillPattern(CellStyle.SOLID_FOREGROUND);
				cell.setCellStyle(pass);
			}
			else
			{
				HSSFCellStyle fail= wb.createCellStyle();
				fail.setFillForegroundColor(IndexedColors.RED.getIndex());
				fail.setFillPattern(CellStyle.SOLID_FOREGROUND);
				cell.setCellStyle(fail);
			}
		}
		FileOutputStream out = new FileOutputStream("TestResults/"+strDataFileName+".xls");
		wb.write(out);
		wb.close();
	}
}
