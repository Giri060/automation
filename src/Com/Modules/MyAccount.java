package Com.Modules;
import java.awt.datatransfer.StringSelection;
import java.io.File;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.IOException;
import java.util.Date;
import java.util.LinkedHashMap;
import java.util.Map;
import java.util.Set;
import org.apache.poi.hssf.usermodel.HSSFCell;
import org.apache.poi.hssf.usermodel.HSSFCellStyle;
import org.apache.poi.hssf.usermodel.HSSFRow;
import org.apache.poi.hssf.usermodel.HSSFSheet;
import org.apache.poi.hssf.usermodel.HSSFWorkbook;
import org.apache.poi.ss.usermodel.Cell;
import org.apache.poi.ss.usermodel.CellStyle;
import org.apache.poi.ss.usermodel.IndexedColors;
import org.apache.poi.ss.usermodel.Row;
import org.openqa.selenium.By;
import org.openqa.selenium.Keys;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.testng.Assert;
import org.testng.annotations.AfterClass;
import org.testng.annotations.BeforeClass;
import org.testng.annotations.Test;
import com.aventstack.extentreports.Status;
import com.aventstack.extentreports.markuputils.ExtentColor;
import com.aventstack.extentreports.markuputils.MarkupHelper;
import Com.ObjectRepository.Homepage2_OR;
import Com.ObjectRepository.Homepage_OR;
import Com.ObjectRepository.Login_OR;
import Com.ObjectRepository.MyAccount_OR;
import Com.ObjectRepository.Polos_OR;
import Com.ObjectRepository.Tshirts_OR;
import Com.Utilities.Browsers;
import Com.Utilities.CommonMethods;
import Com.Utilities.GetScreenShot;
import Com.Utilities.Screenshot;
import Com.Utilities.Steps;
import Com.Utilities.TestBase;
import jxl.format.Border;
import jxl.write.WritableCellFormat;
import jxl.write.WritableFont;
import java.awt.event.KeyEvent;
import java.awt.*;
import org.openqa.selenium.By;
public class MyAccount extends TestBase
{
	WebDriver driver;
	Login_OR login = new Login_OR();
	Steps steps = new Steps();
	GetScreenShot getscreen = new GetScreenShot();
	Screenshot screen = new Screenshot();
	public String strAbsolutepath = new File("").getAbsolutePath();
	HSSFWorkbook workbook;
	HSSFSheet sheet;
	String strDataFileName = this.getClass().getSimpleName();
	Map<String, Object[]> testresultdata; 
	CommonMethods commonmethods = new CommonMethods();
	Homepage_OR home = new Homepage_OR();
	MyAccount_OR Account = new MyAccount_OR();
	Screenshot screenshot = new Screenshot();
	String passstatus = screenshot.status("PASS");
	String failstatus = screenshot.status("FAIL");
	int cnt = 2;
	public static String Dashboard  = "";
	public static String CreateSell  = "";
	public static String Product1  = "";
	public static String Product2  = "";
	public static String Product3  = "";
	public static String Msg1  = "";
	public static String Msg2  = "";
	public static String CreateSellstatus  = "";
	public static String CreateSellstatusfail  = "";
	public static String Upload2  = "";
	public static String Upload1  = "";
	public static String ImageFilter1  = "";
	public static String ImageFilterMsg1  = "";
	public static String ImageFilter2  = "";
	public static String ImageFilterMsg2  = "";
	public static String ImageFilter3  = "";
	public static String ImageFilterMsg3  = "";
	public static String ImageFilter4  = "";
	public static String ImageFilterMsg4  = "";
	public static String ImageFilter5  = "";
	public static String ImageFilterMsg5  = "";
	public static String ImageFilter6  = "";
	public static String ImageFilterMsg6  = "";
	public static String MirrorEffect1  = "";
	public static String MirrorEffect2  = "";
	public static String MirrorEffect3  = "";
	public static String MirrorEffect4  = "";
	public static String AddClipart1  = "";
	public static String AddClipart2  = "";
	public static String Copy1  = "";
	public static String Delete1  = "";
	public static String Clear1  = "";
	public static String Flip1  = "";
	public static String Flip2  = "";
	public static String Color1  = "";
	public static String Color2  = "";
	public static String Color3  = "";
	public static String Color4  = "";
	public static String Color5  = "";
	public static String Color6  = "";
	public static String Color7  = "";
	public static String Prod1  = "";
	public static String Prod2  = "";
	public static String Prod3  = "";
	public static String Prod4  = "";
	public static String Prod5  = "";
	public static String Prod6  = "";
	public static String Prod7  = "";
	public static String Prod8  = "";
	public static String Prod9  = "";
	public static String Prod10  = "";
	public static String Prod11  = "";
	public static String Prod12  = "";
	public static String Prod13  = "";
	public static String Prod14  = "";
	public static String Prod15  = "";
	public static String Prod16  = "";
	public static String Prod17  = "";
	public static String Prod18  = "";
	public static String CampaignDashboard1  = "";
	public static String CampaignCreateSell  = "";
	public static String CampaignProduct1  = "";
	public static String CampaignMRP1  = "";
	public static String MarkupPrice1  = "";
	public static String MarkupPrice2  = "";
	public static String SellingPrice1  = "";
	public static String SellingPrice2  = "";
	public static String CampaignColor  = "";
	public static String CampaignColor1  = "";
	public static String CampaignAdd  = "";
	public static String CampaignTitle1  = "";
	public static String Example1  = "";
	public static String Example2  = "";
	public static String Example3  = "";
	public static String CampaignTitle2  = "";
	public static String DesExample1  = "";
	public static String DesExample2  = "";
	public static String DesExample3  = "";
	public static String CampaignTitle3  = "";
	public static String CampaignTitle4  = "";
	public static String CongratsMSG  = "";
	public static String CampaignTitle5  = "";
	public static String CampaignTitle6  = "";
	public static String CampaignTitle7  = "";
	public static String CampaignTitle8  = "";
	public static String CampaignTitle9  = "";
	public static String CampaignTitle10  = "";
	public static String CampaignTitle11  = "";
	public static String CampaignTitle12  = "";
	public static String CampaignTitle13  = "";
	public static String Text1  = "";
	public static String Text2  = "";
	public static String Text3  = "";
	public static String Text4  = "";
	public static String Text5  = "";
	public static String Text6  = "";
	public static String Text7  = "";
	public static String Text8  = "";
	public static String StartCampaigntatus  = "";
	public static String StartCampaigntatusfail  = "";
	public static String ProfileSettings1  = "";
	public static String ProfileSettingstatus  = "";
	public static String ProfileSettingstatusfail  = "";
	public MyAccount()
	{
		Browsers b = new Browsers();
		this.driver = b.getBrowsers("chrome1");		 
	}
	//@Test(priority = 0)
	public void CreateSell() throws Exception
	{
		String name = new Object(){}.getClass().getEnclosingMethod().getName();
		cnt ++;
		try 
		{
			test = extent.createTest("Create & Sell");
			System.out.println("Create & Sell Test Case Executing...");
			commonmethods.Login(driver);
			test.log(Status.INFO, MarkupHelper.createLabel("User able to Login", ExtentColor.BLUE));
			Thread.sleep(3000);
			steps.clickButton(login.MyAccount, driver, name);
			steps.clickButton(Account.Dashboard, driver, name);
			Dashboard = getscreen.capture(driver, "Dashboard");
			test.addScreenCaptureFromPath(Dashboard);
			test.log(Status.INFO, MarkupHelper.createLabel("User navigated to Dashboard page", ExtentColor.BLUE));
			steps.clickButton(Account.CreateSell, driver, name);
			CreateSell = getscreen.capture(driver, "CreateSell");
			test.addScreenCaptureFromPath(CreateSell);
			test.log(Status.INFO, MarkupHelper.createLabel("User navigated to Create & Sell page", ExtentColor.BLUE));
			steps.clickButton(Account.Create_EnterText, driver, name);
			steps.EnterText(Account.Create_EnterText, driver, strDataFileName, "EnterText", name);
			Thread.sleep(2000);
			steps.clickButton(Account.Create_ChooseFont, driver, name);
			Thread.sleep(2000);
			steps.clickButton(Account.Create_selectFont, driver, name);
			steps.selectDropdownIndex(Account.Create_ChooseFont, driver, 0, name);
			steps.selectDropdown(Account.Create_FontSize, driver, strDataFileName, "FontSize", name);
			test.log(Status.INFO, MarkupHelper.createLabel("User selected Font and Font Size", ExtentColor.BLUE));
			Product1 = getscreen.capture(driver, "Product1");
			test.addScreenCaptureFromPath(Product1);
			steps.clickButton(Account.Create_FontColor, driver, name);
			steps.clickButton(Account.Create_FontColorBlue, driver, name);
			test.log(Status.INFO, MarkupHelper.createLabel("User changed Font Color", ExtentColor.BLUE));
			Product2 = getscreen.capture(driver, "Product2");
			test.addScreenCaptureFromPath(Product2);
			steps.clickButton(Account.Create_Bold, driver, name);
			steps.clickButton(Account.Create_Italic, driver, name);
			test.log(Status.INFO, MarkupHelper.createLabel("User changed Font to Bold and Italic", ExtentColor.BLUE));
			steps.clickButton(Account.Create_AlignLeft, driver, name);
			steps.clickButton(Account.Create_AlignCenter, driver, name);
			steps.clickButton(Account.Create_AlignRight, driver, name);
			Product3 = getscreen.capture(driver, "Product3");
			test.addScreenCaptureFromPath(Product3);
			test.log(Status.INFO, MarkupHelper.createLabel("User changed Text Alignment to Left, Center and Right", ExtentColor.BLUE));
			testresultdata.put(Integer.toString(cnt), new Object[] {name, "Add Text feature in Create & Sell", "Add Text feature in Create & Sell Successful",passstatus});
			cnt++;
			steps.ScrollUp(driver);
			Thread.sleep(2000);
			steps.clickButton(Account.Create_UploadImage, driver, name);
			Thread.sleep(2000);
			steps.clickButton(Account.Create_Uploadfile, driver, name);
			Thread.sleep(2000);
			StringSelection sel = new StringSelection("/home/giri/Testing/automation/Images/3.jpg");
			Toolkit.getDefaultToolkit().getSystemClipboard().setContents(sel,null);
			System.out.println("selection" +sel);
			commonmethods.Uplodfile(driver);
			Thread.sleep(2000);
			Msg1 = driver.findElement(Account.Create_Uploadfilemsg1).getText();
			System.out.println("Message:   "+Msg1);
			test.log(Status.INFO, MarkupHelper.createLabel("User uploaded wrong Image:   "+Msg1, ExtentColor.RED));
			Upload1 = getscreen.capture(driver, "Upload1");
			test.addScreenCaptureFromPath(Upload1);
			steps.clickButton(Account.Create_UploadfileOKbtn, driver, name);
			Thread.sleep(2000);
			steps.clickButton(Account.Create_Uploadfile, driver, name);
			Thread.sleep(2000);
			StringSelection Imge2 = new StringSelection("/home/giri/Testing/automation/Images/1.jpg");
			Toolkit.getDefaultToolkit().getSystemClipboard().setContents(Imge2,null);
			System.out.println("selection" +Imge2);
			commonmethods.Uplodfile(driver);
			Thread.sleep(2000);
			Msg2 = driver.findElement(Account.Create_Uploadfilemsg1).getText();
			System.out.println("Message:   "+Msg2);
			test.log(Status.INFO, MarkupHelper.createLabel("User uploaded right Image:   "+Msg2, ExtentColor.BLUE));
			Upload2 = getscreen.capture(driver, "Upload2");
			test.addScreenCaptureFromPath(Upload2);
			steps.clickButton(Account.Create_UploadfileOKbtn, driver, name);
			Thread.sleep(2000);
			steps.clickButton(Account.Create_Imgecenter, driver, name);
			test.log(Status.INFO, MarkupHelper.createLabel("User selected Snap Image to Center radio box", ExtentColor.BLUE));
			testresultdata.put(Integer.toString(cnt), new Object[] {name, "Upload Image feature in Create & Sell", "Upload Image feature in Create & Sell Successful",passstatus});
			cnt++;
			steps.clickButton(Account.Create_ImageFilter, driver, name);
			ImageFilter1 = getscreen.capture(driver, "ImageFilter1");
			test.addScreenCaptureFromPath(ImageFilter1);
			test.log(Status.INFO, MarkupHelper.createLabel("User selected Image Filter", ExtentColor.BLUE));
			steps.clickButton(Account.Create_ImageFilter_ConverttoBlack, driver, name);
			Thread.sleep(2000);
			steps.WaitElementPresent(Account.Create_Uploadfilemsg1, driver);
			ImageFilterMsg1 = driver.findElement(Account.Create_Uploadfilemsg1).getText();
			System.out.println("Displayed Message:   "+ImageFilterMsg1);
			test.log(Status.INFO, MarkupHelper.createLabel("Displayed Message:   "+ImageFilterMsg1, ExtentColor.BLUE));
			steps.clickButton(Account.Create_UploadfileOKbtn, driver, name);
			ImageFilter2 = getscreen.capture(driver, "ImageFilter2");
			test.addScreenCaptureFromPath(ImageFilter2);
			steps.clickButton(Account.Create_ImageFilter_ReduceColor, driver, name);
			Thread.sleep(2000);
			steps.WaitElementPresent(Account.Create_Uploadfilemsg1, driver);
			ImageFilterMsg2 = driver.findElement(Account.Create_Uploadfilemsg1).getText();
			System.out.println("Displayed Message:   "+ImageFilterMsg2);
			test.log(Status.INFO, MarkupHelper.createLabel("Displayed Message:   "+ImageFilterMsg2, ExtentColor.BLUE));
			steps.clickButton(Account.Create_UploadfileOKbtn, driver, name);
			ImageFilter3 = getscreen.capture(driver, "ImageFilter3");
			test.addScreenCaptureFromPath(ImageFilter3);
			steps.clickButton(Account.Create_ImageFilter_OriginalImage, driver, name);
			Thread.sleep(2000);
			steps.WaitElementPresent(Account.Create_Uploadfilemsg1, driver);
			ImageFilterMsg3 = driver.findElement(Account.Create_Uploadfilemsg1).getText();
			System.out.println("Displayed Message:   "+ImageFilterMsg3);
			test.log(Status.INFO, MarkupHelper.createLabel("Displayed Message:   "+ImageFilterMsg3, ExtentColor.BLUE));
			steps.clickButton(Account.Create_UploadfileOKbtn, driver, name);
			ImageFilter4 = getscreen.capture(driver, "ImageFilter4");
			test.addScreenCaptureFromPath(ImageFilter4);
			steps.clickButton(Account.Create_ImageFilter_ImageColor, driver, name);
			Thread.sleep(2000);
			steps.clickButton(Account.Create_ImageFilter_ImageColorChange, driver, name);
			steps.clickButton(Account.Create_ImageFilter_Apply, driver, name);
			ImageFilter5 = getscreen.capture(driver, "ImageFilter5");
			test.addScreenCaptureFromPath(ImageFilter5);
			steps.clickButton(Account.Create_ImageFilter_OriginalImage, driver, name);
			Thread.sleep(2000);
			steps.WaitElementPresent(Account.Create_Uploadfilemsg1, driver);
			ImageFilterMsg4 = driver.findElement(Account.Create_Uploadfilemsg1).getText();
			System.out.println("Displayed Message:   "+ImageFilterMsg4);
			test.log(Status.INFO, MarkupHelper.createLabel("Displayed Message:   "+ImageFilterMsg4, ExtentColor.BLUE));
			steps.clickButton(Account.Create_UploadfileOKbtn, driver, name);
			ImageFilter6 = getscreen.capture(driver, "ImageFilter6");
			test.addScreenCaptureFromPath(ImageFilter6);
			testresultdata.put(Integer.toString(cnt), new Object[] {name, "Image Filter feature in Create & Sell", "Image Filter feature in Create & Sell Successful",passstatus});
			cnt++;
			steps.clickButton(Account.Create_MirrorEffect, driver, name);
			steps.clickButton(Account.Create_Mirrorr_Horizontal, driver, name);
			MirrorEffect1 = getscreen.capture(driver, "MirrorEffect1");
			test.addScreenCaptureFromPath(MirrorEffect1);
			steps.clickButton(Account.Create_Mirrorr_Horizontal, driver, name);
			MirrorEffect2 = getscreen.capture(driver, "MirrorEffect2");
			test.addScreenCaptureFromPath(MirrorEffect2);
			test.log(Status.INFO, MarkupHelper.createLabel("User used Mirror effect in Horizontal  ", ExtentColor.BLUE));
			steps.clickButton(Account.Create_Mirrorr_Vertical, driver, name);
			MirrorEffect3 = getscreen.capture(driver, "MirrorEffect3");
			test.addScreenCaptureFromPath(MirrorEffect3);
			steps.clickButton(Account.Create_Mirrorr_Vertical, driver, name);
			MirrorEffect4 = getscreen.capture(driver, "MirrorEffect4");
			test.addScreenCaptureFromPath(MirrorEffect4);
			test.log(Status.INFO, MarkupHelper.createLabel("User used Mirror effect in Vertical  ", ExtentColor.BLUE));
			testresultdata.put(Integer.toString(cnt), new Object[] {name, "Mirror Effect feature in Create & Sell", "Mirror Effect feature in Create & Sell Successful",passstatus});
			cnt++;
			steps.clickButton(Account.Create_AddClipart, driver, name);
			steps.selectDropdown(Account.Create_Clipart_CategoryDD, driver, strDataFileName, "Category", name);
			steps.clickButton(Account.Create_Clipart_Image1, driver, name);
			AddClipart1 = getscreen.capture(driver, "AddClipart1");
			test.addScreenCaptureFromPath(AddClipart1);
			test.log(Status.INFO, MarkupHelper.createLabel("User added Clipart Image  ", ExtentColor.BLUE));
			steps.clickButton(Account.Create_Clipart_Color, driver, name);
			Thread.sleep(1000);
			steps.clickButton(Account.Create_Clipart_ColorChange, driver, name);
			Thread.sleep(1000);
			steps.clickButton(Account.Create_Clipart_Apply, driver, name);
			AddClipart2 = getscreen.capture(driver, "AddClipart2");
			test.addScreenCaptureFromPath(AddClipart2);
			testresultdata.put(Integer.toString(cnt), new Object[] {name, "Add Clipart feature in Create & Sell", "Add Clipart feature in Create & Sell Successful",passstatus});
			cnt++;
			steps.clickButton(Account.Create_ColorWhite, driver, name);
			Color1= getscreen.capture(driver, "Color1");
			test.addScreenCaptureFromPath(Color1);
			test.log(Status.INFO, MarkupHelper.createLabel("User changed color to White   ", ExtentColor.BLUE));
			steps.clickButton(Account.Create_ColorGrey, driver, name);
			Color2= getscreen.capture(driver, "Color2");
			test.addScreenCaptureFromPath(Color2);
			test.log(Status.INFO, MarkupHelper.createLabel("User changed color to Grey   ", ExtentColor.BLUE));
			steps.clickButton(Account.Create_ColorYellow, driver, name);
			Color3 = getscreen.capture(driver, "Color3");
			test.addScreenCaptureFromPath(Color3);
			test.log(Status.INFO, MarkupHelper.createLabel("User changed color to Yellow   ", ExtentColor.BLUE));
			steps.clickButton(Account.Create_ColorRoyalBlue, driver, name);
			Color4 = getscreen.capture(driver, "Color4");
			test.addScreenCaptureFromPath(Color4);
			test.log(Status.INFO, MarkupHelper.createLabel("User changed color to Royal Blue   ", ExtentColor.BLUE));
			steps.clickButton(Account.Create_ColorNavyBlue, driver, name);
			Color5 = getscreen.capture(driver, "Color5");
			test.addScreenCaptureFromPath(Color5);
			test.log(Status.INFO, MarkupHelper.createLabel("User changed color to Navy Blue   ", ExtentColor.BLUE));
			steps.clickButton(Account.Create_ColorRed, driver, name);
			Color6 = getscreen.capture(driver, "Color6");
			test.addScreenCaptureFromPath(Color6);
			test.log(Status.INFO, MarkupHelper.createLabel("User changed color to Red   ", ExtentColor.BLUE));
			steps.clickButton(Account.Create_ColorBlack, driver, name);
			Color7 = getscreen.capture(driver, "Color7");
			test.addScreenCaptureFromPath(Color7);
			test.log(Status.INFO, MarkupHelper.createLabel("User changed color to Black   ", ExtentColor.BLUE));
			steps.selectDropdown(Account.Create_ProductDD, driver, strDataFileName, "Product1", name);
			Thread.sleep(3000);
			try
			{
				steps.clickJSButton(Account.Create_Color2, driver, name);
				steps.clickJSButton(Account.Create_Color3, driver, name);
				steps.clickJSButton(Account.Create_Color4, driver, name);
				steps.clickJSButton(Account.Create_Color5, driver, name);
				steps.clickJSButton(Account.Create_Color1, driver, name);
			}
			catch(Exception e)
			{
				System.out.println("Color doesn't exists");
			}
			Prod1 = getscreen.capture(driver, "Prod1");
			test.addScreenCaptureFromPath(Prod1);
			test.log(Status.INFO, MarkupHelper.createLabel("User selected Womn's Tee Product   ", ExtentColor.BLUE));
			Thread.sleep(1000);
			steps.selectDropdown(Account.Create_ProductDD, driver, strDataFileName, "Product2", name);
			Thread.sleep(3000);
			try
			{
				steps.clickJSButton(Account.Create_Color2, driver, name);
				steps.clickJSButton(Account.Create_Color3, driver, name);
				steps.clickJSButton(Account.Create_Color4, driver, name);
				steps.clickJSButton(Account.Create_Color1, driver, name);
			}
			catch(Exception e)
			{
				System.out.println("Color doesn't exists");
			}
			Prod2 = getscreen.capture(driver, "Prod2");
			test.addScreenCaptureFromPath(Prod2);
			test.log(Status.INFO, MarkupHelper.createLabel("User selected Men's Vest Product   ", ExtentColor.BLUE));
			Thread.sleep(1000);
			steps.selectDropdown(Account.Create_ProductDD, driver, strDataFileName, "Product3", name);
			Thread.sleep(3000);
			try
			{
				steps.clickJSButton(Account.Create_Color2, driver, name);
				steps.clickJSButton(Account.Create_Color3, driver, name);
				steps.clickJSButton(Account.Create_Color4, driver, name);
				steps.clickJSButton(Account.Create_Color1, driver, name);
			}
			catch(Exception e)
			{
				System.out.println("Color doesn't exists");
			}
			Prod3  = getscreen.capture(driver, "Prod3");
			test.addScreenCaptureFromPath(Prod3);
			test.log(Status.INFO, MarkupHelper.createLabel("User selected Women's Tank Top Product   ", ExtentColor.BLUE));
			testresultdata.put(Integer.toString(cnt), new Object[] {name, "Round Necks Product in Create & Sell", "Round Necks Product in Create & Sell Successful",passstatus});
			cnt++;
			Thread.sleep(1000);
			steps.selectDropdown(Account.Create_ProductDD, driver, strDataFileName, "Product4", name);
			Thread.sleep(3000);
			try
			{
				steps.clickJSButton(Account.Create_Color2, driver, name);
				steps.clickJSButton(Account.Create_Color3, driver, name);
				steps.clickJSButton(Account.Create_Color4, driver, name);
				steps.clickJSButton(Account.Create_Color5, driver, name);
				steps.clickJSButton(Account.Create_Color6, driver, name);
				steps.clickJSButton(Account.Create_Color7, driver, name);
				steps.clickJSButton(Account.Create_Color1, driver, name);
			}
			catch(Exception e)
			{
				System.out.println("Color doesn't exists");
			}
			Prod4  = getscreen.capture(driver, "Prod4");
			test.addScreenCaptureFromPath(Prod4);
			test.log(Status.INFO, MarkupHelper.createLabel("User selected Unisex Hoodie Product   ", ExtentColor.BLUE));
			Thread.sleep(1000);
			steps.selectDropdown(Account.Create_ProductDD, driver, strDataFileName, "Product5", name);
			Thread.sleep(3000);
			try
			{
				steps.clickJSButton(Account.Create_Color2, driver, name);
				steps.clickJSButton(Account.Create_Color3, driver, name);
				steps.clickJSButton(Account.Create_Color1, driver, name);
			}
			catch(Exception e)
			{
				System.out.println("Color doesn't exists");
			}
			Prod5  = getscreen.capture(driver, "Prod5");
			test.addScreenCaptureFromPath(Prod5);
			test.log(Status.INFO, MarkupHelper.createLabel("User selected Unisex Full Sleeve Tees Product   ", ExtentColor.BLUE));
			Thread.sleep(1000);
			steps.selectDropdown(Account.Create_ProductDD, driver, strDataFileName, "ZipperProduct1", name);
			Thread.sleep(3000);
			try
			{
				steps.clickJSButton(Account.Create_Color2, driver, name);
				steps.clickJSButton(Account.Create_Color3, driver, name);
				steps.clickJSButton(Account.Create_Color4, driver, name);
				steps.clickJSButton(Account.Create_Color1, driver, name);
			}
			catch(Exception e)
			{
				System.out.println("Color doesn't exists");
			}
			Prod6  = getscreen.capture(driver, "Prod6");
			test.addScreenCaptureFromPath(Prod6);
			test.log(Status.INFO, MarkupHelper.createLabel("User selected Men's FS Zipper Hoodie Product   ", ExtentColor.BLUE));
			Thread.sleep(1000);
			steps.selectDropdown(Account.Create_ProductDD, driver, strDataFileName, "ZipperProduct2", name);
			Thread.sleep(3000);
			try
			{
				steps.clickJSButton(Account.Create_Color2, driver, name);
				steps.clickJSButton(Account.Create_Color1, driver, name);
			}
			catch(Exception e)
			{
				System.out.println("Color doesn't exists");
			}
			Prod7  = getscreen.capture(driver, "Prod7");
			test.addScreenCaptureFromPath(Prod7);
			test.log(Status.INFO, MarkupHelper.createLabel("User selected Women's Zipper Hoodie Product   ", ExtentColor.BLUE));
			testresultdata.put(Integer.toString(cnt), new Object[] {name, "Zipper Hoodie Product in Create & Sell", "Zipper Hoodie Product in Create & Sell Successful",passstatus});
			cnt++;
			Thread.sleep(1000);
			steps.selectDropdown(Account.Create_ProductDD, driver, strDataFileName, "PoloProduct1", name);
			Thread.sleep(3000);
			try
			{
				steps.clickJSButton(Account.Create_Color2, driver, name);
				steps.clickJSButton(Account.Create_Color3, driver, name);
				steps.clickJSButton(Account.Create_Color4, driver, name);
				steps.clickJSButton(Account.Create_Color1, driver, name);
			}
			catch(Exception e)
			{
				System.out.println("Color doesn't exists");
			}
			Prod8  = getscreen.capture(driver, "Prod8");
			test.addScreenCaptureFromPath(Prod8);
			test.log(Status.INFO, MarkupHelper.createLabel("User selected Polo T-Shirt Product   ", ExtentColor.BLUE));
			testresultdata.put(Integer.toString(cnt), new Object[] {name, "Polo T-Shirt Product in Create & Sell", "Polo T-Shirt Product in Create & Sell Successful",passstatus});
			cnt++;
			Thread.sleep(1000);
			steps.selectDropdown(Account.Create_ProductDD, driver, strDataFileName, "CasesProduct1", name);
			Thread.sleep(3000);
			try
			{
				steps.clickJSButton(Account.Create_Product_Color, driver, name);
				steps.clickJSButton(Account.Create_Product_ColorChange, driver, name);
				steps.clickJSButton(Account.Create_Product_Color, driver, name);
				steps.clickJSButton(Account.Create_Product_ColorChange1, driver, name);
			}
			catch(Exception e)
			{
				System.out.println("Color doesn't exists");
			}
			Prod9  = getscreen.capture(driver, "Prod9");
			test.addScreenCaptureFromPath(Prod9);
			test.log(Status.INFO, MarkupHelper.createLabel("User selected iphone 7plus Product   ", ExtentColor.BLUE));
			Thread.sleep(1000);
			steps.selectDropdown(Account.Create_ProductDD, driver, strDataFileName, "CasesProduct2", name);
			Thread.sleep(3000);
			try
			{
				steps.clickJSButton(Account.Create_Product_Color, driver, name);
				steps.clickJSButton(Account.Create_Product_ColorChange, driver, name);
			}
			catch(Exception e)
			{
				System.out.println("Color doesn't exists");
			}
			Prod10  = getscreen.capture(driver, "Prod10");
			test.addScreenCaptureFromPath(Prod10);
			test.log(Status.INFO, MarkupHelper.createLabel("User selected iPhone 6/6S Product   ", ExtentColor.BLUE));
			Thread.sleep(1000);
			steps.selectDropdown(Account.Create_ProductDD, driver, strDataFileName, "CasesProduct3", name);
			Thread.sleep(3000);
			try
			{
				steps.clickJSButton(Account.Create_Product_Color, driver, name);
				steps.clickJSButton(Account.Create_Product_ColorChange, driver, name);
			}
			catch(Exception e)
			{
				System.out.println("Color doesn't exists");
			}
			Prod11  = getscreen.capture(driver, "Prod11");
			test.addScreenCaptureFromPath(Prod11);
			test.log(Status.INFO, MarkupHelper.createLabel("User selected Moto G3 Product   ", ExtentColor.BLUE));
			Thread.sleep(1000);
			steps.selectDropdown(Account.Create_ProductDD, driver, strDataFileName, "CasesProduct4", name);
			Thread.sleep(3000);
			try
			{
				steps.clickJSButton(Account.Create_Product_Color, driver, name);
				steps.clickJSButton(Account.Create_Product_ColorChange, driver, name);
			}
			catch(Exception e)
			{
				System.out.println("Color doesn't exists");
			}
			Prod12  = getscreen.capture(driver, "Prod12");
			test.addScreenCaptureFromPath(Prod12);
			test.log(Status.INFO, MarkupHelper.createLabel("User selected Redmi Note 3 Product   ", ExtentColor.BLUE));
			Thread.sleep(1000);
			steps.selectDropdown(Account.Create_ProductDD, driver, strDataFileName, "CasesProduct5", name);
			Thread.sleep(3000);
			try
			{
				steps.clickJSButton(Account.Create_Product_Color, driver, name);
				steps.clickJSButton(Account.Create_Product_ColorChange, driver, name);
			}
			catch(Exception e)
			{
				System.out.println("Color doesn't exists");
			}
			Prod13  = getscreen.capture(driver, "Prod13");
			test.addScreenCaptureFromPath(Prod13);
			test.log(Status.INFO, MarkupHelper.createLabel("User selected One Plus 3 Product   ", ExtentColor.BLUE));
			testresultdata.put(Integer.toString(cnt), new Object[] {name, "Mobile Cases Product in Create & Sell", "Mobile Cases Product in Create & Sell Successful",passstatus});
			cnt++;
			Thread.sleep(1000);
			steps.selectDropdown(Account.Create_ProductDD, driver, strDataFileName, "PosterProduct1", name);
			Thread.sleep(3000);
			try
			{
				steps.clickJSButton(Account.Create_Product_Color, driver, name);
				steps.clickJSButton(Account.Create_Product_ColorChange, driver, name);
			}
			catch(Exception e)
			{
				System.out.println("Color doesn't exists");
			}
			Prod14  = getscreen.capture(driver, "Prod14");
			test.addScreenCaptureFromPath(Prod14);
			test.log(Status.INFO, MarkupHelper.createLabel("User selected Regular poster : 12'x18' Product   ", ExtentColor.BLUE));
			Thread.sleep(1000);
			steps.selectDropdown(Account.Create_ProductDD, driver, strDataFileName, "PosterProduct2", name);
			Thread.sleep(3000);
			try
			{
				steps.clickJSButton(Account.Create_Product_Color, driver, name);
				steps.clickJSButton(Account.Create_Product_ColorChange, driver, name);
			}
			catch(Exception e)
			{
				System.out.println("Color doesn't exists");
			}
			Prod15  = getscreen.capture(driver, "Prod15");
			test.addScreenCaptureFromPath(Prod15);
			test.log(Status.INFO, MarkupHelper.createLabel("User selected Photo frame : 12'x8' Product   ", ExtentColor.BLUE));
			testresultdata.put(Integer.toString(cnt), new Object[] {name, "Posters Product in Create & Sell", "Posters Product in Create & Sell Successful",passstatus});
			cnt++;
			Thread.sleep(1000);
			steps.selectDropdown(Account.Create_ProductDD, driver, strDataFileName, "LaptopProduct1", name);
			Thread.sleep(3000);
			try
			{
				steps.clickJSButton(Account.Create_Product_Color, driver, name);
				steps.clickJSButton(Account.Create_Product_ColorChange, driver, name);
			}
			catch(Exception e)
			{
				System.out.println("Color doesn't exists");
			}
			Prod16  = getscreen.capture(driver, "Prod16");
			test.addScreenCaptureFromPath(Prod16);
			test.log(Status.INFO, MarkupHelper.createLabel("User selected Laptop Skin-15.6 inches Product   ", ExtentColor.BLUE));
			Thread.sleep(1000);
			steps.selectDropdown(Account.Create_ProductDD, driver, strDataFileName, "LaptopProduct2", name);
			Thread.sleep(3000);
			try
			{
				steps.clickJSButton(Account.Create_Product_Color, driver, name);
				steps.clickJSButton(Account.Create_Product_ColorChange, driver, name);
			}
			catch(Exception e)
			{
				System.out.println("Color doesn't exists");
			}
			Prod17  = getscreen.capture(driver, "Prod17");
			test.addScreenCaptureFromPath(Prod17);
			test.log(Status.INFO, MarkupHelper.createLabel("User selected Laptop Skin-14 inches Product   ", ExtentColor.BLUE));
			testresultdata.put(Integer.toString(cnt), new Object[] {name, "Laptop Skin Product in Create & Sell", "Laptop Skin Product in Create & Sell Successful",passstatus});
			cnt++;
			Thread.sleep(1000);
			steps.selectDropdown(Account.Create_ProductDD, driver, strDataFileName, "CoffeeProduct1", name);
			Thread.sleep(3000);
			try
			{
				steps.clickJSButton(Account.Create_Product_Color, driver, name);
				steps.clickJSButton(Account.Create_Product_ColorChange, driver, name);
				steps.clickJSButton(Account.Create_Product_ColorChange1, driver, name);
			}
			catch(Exception e)
			{
				System.out.println("Color doesn't exists");
			}
			Prod18  = getscreen.capture(driver, "Prod18");
			test.addScreenCaptureFromPath(Prod18);
			test.log(Status.INFO, MarkupHelper.createLabel("User selected Coffee Mug Product   ", ExtentColor.BLUE));
			testresultdata.put(Integer.toString(cnt), new Object[] {name, "Coffee Mug Product in Create & Sell", "Coffee Mug Product in Create & Sell Successful",passstatus});
			cnt++;
			steps.selectDropdown(Account.Create_ProductDD, driver, strDataFileName, "Product1", name);
			steps.ScrollUp(driver);
			steps.clickButton(Account.Create_Copy, driver, name);
			steps.clickButton(Account.Create_Paste, driver, name);
			Copy1= getscreen.capture(driver, "Copy1");
			test.addScreenCaptureFromPath(Copy1);
			test.log(Status.INFO, MarkupHelper.createLabel("User used Copy and Paste icons ", ExtentColor.BLUE));
			steps.clickButton(Account.Create_Delete, driver, name);
			Delete1= getscreen.capture(driver, "Delete1");
			test.addScreenCaptureFromPath(Delete1);
			test.log(Status.INFO, MarkupHelper.createLabel("User used Delete icon  ", ExtentColor.BLUE));
			steps.clickButton(Account.Create_Clear, driver, name);
			Clear1= getscreen.capture(driver, "Clear1");
			test.addScreenCaptureFromPath(Clear1);
			test.log(Status.INFO, MarkupHelper.createLabel("User used Clear icon  ", ExtentColor.BLUE));
			steps.clickButton(Account.Create_Flip, driver, name);
			Flip1= getscreen.capture(driver, "Flip1");
			test.addScreenCaptureFromPath(Flip1);
			steps.clickButton(Account.Create_Flip, driver, name);
			Flip2= getscreen.capture(driver, "Flip2");
			test.addScreenCaptureFromPath(Flip2);
			test.log(Status.INFO, MarkupHelper.createLabel("User Flipped product   ", ExtentColor.BLUE));
			Thread.sleep(1000);
			CreateSellstatus = passstatus;
			Assert.assertTrue(true);
			System.out.println("Create & Sell Successful");
			testresultdata.put(Integer.toString(cnt), new Object[] {name, "Create & Sell", "Create & Sell Successful",passstatus});	
		}
		catch(Exception e)
		{
			System.out.println("Create & Sell Unsuccessful");
			testresultdata.put(Integer.toString(cnt), new Object[] {name, "Create & Sell", "Create & Sell Unsuccessful",failstatus});
			CreateSellstatus = failstatus;
			CreateSellstatusfail = getscreen.capture(driver, "CreateSellstatusfail");
			test.addScreenCaptureFromPath(CreateSellstatusfail);
			Assert.assertTrue(false);
		}
	}
	//@Test(priority = 1)
	public void StartCampaign() throws Exception
	{
		String name = new Object(){}.getClass().getEnclosingMethod().getName();
		cnt ++;
		try
		{
			test = extent.createTest("Start Campaign");
			System.out.println("Start Campaign Test Case Executing...");
			commonmethods.Login(driver);
			test.log(Status.INFO, MarkupHelper.createLabel("User able to Login", ExtentColor.BLUE));
			Thread.sleep(3000);
			steps.clickButton(login.MyAccount, driver, name);
			steps.clickButton(Account.Dashboard, driver, name);
			String Parent = driver.getWindowHandle();
			CampaignDashboard1 = getscreen.capture(driver, "CampaignDashboard1");
			test.addScreenCaptureFromPath(CampaignDashboard1);
			test.log(Status.INFO, MarkupHelper.createLabel("User navigated to Dashboard page", ExtentColor.BLUE));
			steps.clickButton(Account.CreateSell, driver, name);
			CampaignCreateSell = getscreen.capture(driver, "CampaignCreateSell");
			test.addScreenCaptureFromPath(CampaignCreateSell);
			test.log(Status.INFO, MarkupHelper.createLabel("User navigated to Create & Sell page", ExtentColor.BLUE));
			steps.clickButton(Account.Create_EnterText, driver, name);
			steps.EnterText(Account.Create_EnterText, driver, strDataFileName, "EnterText", name);
			Thread.sleep(1000);
			CampaignMRP1 = driver.findElement(Account.Create_Cost).getText();
			System.out.println("MRP of Product:   "+CampaignMRP1);
			test.log(Status.INFO, MarkupHelper.createLabel("MRP of Product:   "+CampaignMRP1, ExtentColor.BLUE));
			Thread.sleep(2000);
			steps.clickButton(Account.Create_Submit, driver, name);
			Thread.sleep(5000);
			steps.waitForElement(Account.Create_Campaign_MarkupPriceField, driver);
			MarkupPrice2 = driver.findElement(Account.Create_Campaign_MarkupPriceField).getText();
			System.out.println(MarkupPrice2);
			Thread.sleep(1000);
			MarkupPrice1 = driver.findElement(Account.Create_Campaign_MarkupPrice).getAttribute("value");
			System.out.println(MarkupPrice1);
			System.out.println(MarkupPrice1 +"    " + MarkupPrice2 );
			test.log(Status.INFO, MarkupHelper.createLabel(MarkupPrice1 +"    " + MarkupPrice2 , ExtentColor.BLUE));
			SellingPrice1 = driver.findElement(Account.Create_Campaign_Sellingprice).getText();
			SellingPrice2 = driver.findElement(Account.Create_Campaign_SellingpriceField).getText();
			System.out.println(SellingPrice1+"    "   + SellingPrice2 );
			test.log(Status.INFO, MarkupHelper.createLabel(SellingPrice1+"    "  + SellingPrice2 , ExtentColor.BLUE));
			steps.clickButton(Account.Create_Campaign_AddColors, driver, name);
			Thread.sleep(1000);
			steps.clickButton(Account.Create_Campaign_Color1, driver, name);
			steps.clickButton(Account.Create_Campaign_Done, driver, name);
			test.log(Status.INFO, MarkupHelper.createLabel("User added more colors to the Product" , ExtentColor.BLUE));
			CampaignColor = getscreen.capture(driver, "CampaignColor");
			test.addScreenCaptureFromPath(CampaignColor);
			steps.clickButton(Account.Create_Campaign_ColorRemove, driver, name);
			test.log(Status.INFO, MarkupHelper.createLabel("User removed color" , ExtentColor.BLUE));
			CampaignColor1 = getscreen.capture(driver, "CampaignColor1");
			test.addScreenCaptureFromPath(CampaignColor1);
			steps.clickButton(Account.Create_Campaign_AddProdcuts, driver, name);
			steps.selectDropdown(Account.Create_Campaign_ProductDD, driver, strDataFileName, "Product", name);
			steps.clickButton(Account.Create_Campaign_Add, driver, name);
			CampaignAdd = getscreen.capture(driver, "CampaignAdd");
			test.addScreenCaptureFromPath(CampaignAdd);
			test.log(Status.INFO, MarkupHelper.createLabel("User Added more Products" , ExtentColor.BLUE));
			steps.clickButton(Account.Create_Campaign_ProductDelete, driver, name);
			test.log(Status.INFO, MarkupHelper.createLabel("User Deleted Product" , ExtentColor.BLUE));
			steps.clickButton(Account.Create_Campaign_Next, driver, name);
			test.log(Status.INFO, MarkupHelper.createLabel("User Navigated to Campaign Title and Description page" , ExtentColor.BLUE));
			CampaignTitle1 = getscreen.capture(driver, "CampaignTitle1");
			test.addScreenCaptureFromPath(CampaignTitle1);
			steps.clickButton(Account.Create_Campaign_TitleExamples, driver, name);
			steps.clickButton(Account.Create_Campaign_TitleExample1, driver, name);
			Example1 = driver.findElement(Account.Create_Campaign_TitleExample1Text).getText();
			System.out.println("Title Example 1:  "+Example1);
			test.log(Status.INFO, MarkupHelper.createLabel("Title Example 1:  "+Example1 , ExtentColor.BLUE));
			steps.clickButton(Account.Create_Campaign_TitleExample2, driver, name);
			Thread.sleep(2000);
			Example2 = driver.findElement(Account.Create_Campaign_TitleExample2Text).getText();
			System.out.println("Title Example 2:  "+Example2);
			test.log(Status.INFO, MarkupHelper.createLabel("Title Example 2:  "+Example2 , ExtentColor.BLUE));
			steps.clickButton(Account.Create_Campaign_TitleExample3, driver, name);
			Thread.sleep(2000);
			Example3 = driver.findElement(Account.Create_Campaign_TitleExample3Text).getText();
			System.out.println("Title Example 3:  "+Example3);
			test.log(Status.INFO, MarkupHelper.createLabel("Title Example 3:  "+Example3 , ExtentColor.BLUE));
			CampaignTitle2 = getscreen.capture(driver, "CampaignTitle2");
			test.addScreenCaptureFromPath(CampaignTitle2);
			steps.clickButton(Account.Create_Campaign_TitleExampleClose, driver, name);
			steps.EnterText(Account.Create_Campaign_Title, driver, strDataFileName, "Title", name);
			test.log(Status.INFO, MarkupHelper.createLabel("User Entered Title" , ExtentColor.BLUE));
			Thread.sleep(1000);
			steps.clickButton(Account.Create_Campaign_DescriptionExamples, driver, name);
			steps.clickButton(Account.Create_Campaign_DescriptionExample1, driver, name);
			Thread.sleep(2000);
			DesExample1 = driver.findElement(Account.Create_Campaign_DescriptionExample1Text).getText();
			System.out.println("Description Example 1:  "+DesExample1);
			test.log(Status.INFO, MarkupHelper.createLabel("Description Example 1:  "+DesExample1 , ExtentColor.BLUE));
			steps.clickButton(Account.Create_Campaign_DescriptionExample2, driver, name);
			Thread.sleep(2000);
			DesExample2 = driver.findElement(Account.Create_Campaign_DescriptionExample2Text).getText();
			System.out.println("Description Example 2:  "+DesExample2);
			test.log(Status.INFO, MarkupHelper.createLabel("Description Example 2:  "+DesExample2 , ExtentColor.BLUE));
			steps.clickButton(Account.Create_Campaign_DescriptionExample3, driver, name);
			Thread.sleep(2000);
			DesExample3 = driver.findElement(Account.Create_Campaign_DescriptionExample3Text).getText();
			System.out.println("Description Example 3:  "+DesExample3);
			test.log(Status.INFO, MarkupHelper.createLabel("Description Example 3:  "+DesExample3 , ExtentColor.BLUE));
			CampaignTitle3 = getscreen.capture(driver, "CampaignTitle3");
			test.addScreenCaptureFromPath(CampaignTitle3);
			steps.clickButton(Account.Create_Campaign_DescriptionExampleClose, driver, name);
			steps.EnterText(Account.Create_Campaign_Description, driver, strDataFileName, "Description", name);
			test.log(Status.INFO, MarkupHelper.createLabel("User Entered Description" , ExtentColor.BLUE));
			Thread.sleep(2000);
			//steps.clickButton(Account.Create_Campaign_CampaignLengthDD, driver, name);
			steps.selectDropdownIndex(Account.Create_Campaign_CampaignLengthDD, driver, 0, name);
			//steps.selectDropdown(Account.Create_Campaign_CampaignLengthDD, driver, strDataFileName, "CampaignLength", name);
			test.log(Status.INFO, MarkupHelper.createLabel("User Selected Campaign Lenth" , ExtentColor.BLUE));
			Thread.sleep(2000);
			steps.clickButton(Account.Create_Campaign_SelectCategoryDD, driver, name);
			driver.findElement(Account.Create_Campaign_SelectCategoryDD).sendKeys(Keys.ENTER);
			/*steps.clickButton(Account.Create_Campaign_SelectCategoryMovies, driver, name);
			steps.selectDropdownIndex(Account.Create_Campaign_SelectCategoryDD, driver, 2, name);
			steps.selectDropdown(Account.Create_Campaign_SelectCategoryDD, driver, strDataFileName, "SelectCategory", name);
			 */			
			test.log(Status.INFO, MarkupHelper.createLabel("User Selected Category" , ExtentColor.BLUE));
			steps.clickButton(Account.Create_Campaign_TermsRadio, driver, name);
			steps.clickButton(Account.Create_Campaign_Finish, driver, name);
			CampaignTitle4 = getscreen.capture(driver, "CampaignTitle4");
			test.addScreenCaptureFromPath(CampaignTitle4);
			test.log(Status.INFO, MarkupHelper.createLabel("User clicked on Finish" , ExtentColor.BLUE));
			Thread.sleep(3000);
			steps.waitForElement(Account.Create_Campaign_CongratsMsg, driver);
			CongratsMSG = driver.findElement(Account.Create_Campaign_CongratsMsg).getText();
			System.out.println("Message displayed as:  "+CongratsMSG);
			test.log(Status.INFO, MarkupHelper.createLabel("Message displayed as:  "+CongratsMSG , ExtentColor.BLUE));
			steps.clickJSButton(Account.Create_Campaign_FinishClickHere, driver, name);
			steps.ChildWindow(driver);
			driver.manage().window().maximize();
			Thread.sleep(3000);
			CampaignTitle5 = getscreen.capture(driver, "CampaignTitle5");
			test.addScreenCaptureFromPath(CampaignTitle5);
			System.out.println("User Navigated to Campaign page and verified Placed Campagin");
			test.log(Status.INFO, MarkupHelper.createLabel("User Navigated to Campaign page and verified Placed Campagin " , ExtentColor.BLUE));
			driver.close();
			driver.switchTo().window(Parent);
			Thread.sleep(2000);
			try
			{
				steps.clickButton(Account.DiscountDailogclose, driver, name);
			}
			catch(Exception e)
			{
				System.out.println("Discount popup not displayed");
			}
			Thread.sleep(2000);
			steps.clickJSButton(Account.Create_Campaign_Howitworks, driver, name);
			steps.ChildWindow(driver);
			driver.manage().window().maximize();
			Thread.sleep(2000);
			/*			Text1 = driver.findElement(Account.Create_Campaign_HowitworksText).getText();
			System.out.println("Displayed Heading of Page is:       "+ Text1);
			test.log(Status.INFO, MarkupHelper.createLabel("Displayed Heading of Page is:       "+ Text1 , ExtentColor.BLUE));
			 */
			CampaignTitle6 = getscreen.capture(driver, "CampaignTitle6");
			test.addScreenCaptureFromPath(CampaignTitle6);
			System.out.println("User Navigated to How it Works page ");
			test.log(Status.INFO, MarkupHelper.createLabel("User Navigated to How it Works page " , ExtentColor.BLUE));
			driver.close();
			driver.switchTo().window(Parent);
			Thread.sleep(2000);
			steps.clickJSButton(Account.Create_Campaign_IntroAds, driver, name);
			steps.ChildWindow(driver);
			driver.manage().window().maximize();
			Thread.sleep(2000);
			/*			Text2 = driver.findElement(Account.Create_Campaign_HowitworksText).getText();
			System.out.println("Displayed Heading of Page is:       "+ Text2);
			test.log(Status.INFO, MarkupHelper.createLabel("Displayed Heading of Page is:       "+ Text2 , ExtentColor.BLUE));
			 */
			CampaignTitle7 = getscreen.capture(driver, "CampaignTitle7");
			test.addScreenCaptureFromPath(CampaignTitle7);
			System.out.println("User Navigated to Introduction to Facebook Ads page ");
			test.log(Status.INFO, MarkupHelper.createLabel("User Navigated to Introduction to Facebook Ads page " , ExtentColor.BLUE));
			driver.close();
			driver.switchTo().window(Parent);
			Thread.sleep(2000);
			steps.clickJSButton(Account.Create_Campaign_CreateFB, driver, name);
			steps.ChildWindow(driver);
			driver.manage().window().maximize();
			Thread.sleep(2000);
			/*			Text3 = driver.findElement(Account.Create_Campaign_HowitworksText).getText();
			System.out.println("Displayed Heading of Page is:       "+ Text3);
			test.log(Status.INFO, MarkupHelper.createLabel("Displayed Heading of Page is:       "+ Text3 , ExtentColor.BLUE));
			 */
			CampaignTitle8 = getscreen.capture(driver, "CampaignTitle8");
			test.addScreenCaptureFromPath(CampaignTitle8);
			System.out.println("User Navigated to Create a Facebook page ");
			test.log(Status.INFO, MarkupHelper.createLabel("User Navigated to Create a Facebook page " , ExtentColor.BLUE));
			driver.close();
			driver.switchTo().window(Parent);
			Thread.sleep(2000);
			steps.clickJSButton(Account.Create_Campaign_TypesFB, driver, name);
			steps.ChildWindow(driver);
			driver.manage().window().maximize();
			Thread.sleep(2000);
			/*			Text4 = driver.findElement(Account.Create_Campaign_HowitworksText).getText();
			System.out.println("Displayed Heading of Page is:       "+ Text4);
			test.log(Status.INFO, MarkupHelper.createLabel("Displayed Heading of Page is:       "+ Text4 , ExtentColor.BLUE));
			 */
			CampaignTitle9 = getscreen.capture(driver, "CampaignTitle9");
			test.addScreenCaptureFromPath(CampaignTitle9);
			System.out.println("User Navigated to Types of Facebook Ads page ");
			test.log(Status.INFO, MarkupHelper.createLabel("User Navigated to Types of Facebook Ads page " , ExtentColor.BLUE));
			driver.close();
			driver.switchTo().window(Parent);
			Thread.sleep(2000);
			steps.clickJSButton(Account.Create_Campaign_LaunchFBAd, driver, name);
			steps.ChildWindow(driver);
			driver.manage().window().maximize();
			Thread.sleep(2000);
			/*			Text5 = driver.findElement(Account.Create_Campaign_HowitworksText).getText();
			System.out.println("Displayed Heading of Page is:       "+ Text5);
			test.log(Status.INFO, MarkupHelper.createLabel("Displayed Heading of Page is:       "+ Text5 , ExtentColor.BLUE));
			 */
			CampaignTitle10 = getscreen.capture(driver, "CampaignTitle10");
			test.addScreenCaptureFromPath(CampaignTitle10);
			System.out.println("User Navigated to Launch your first Facebook Ad page ");
			test.log(Status.INFO, MarkupHelper.createLabel("User Navigated to Launch your first Facebook Ad page " , ExtentColor.BLUE));
			driver.close();
			driver.switchTo().window(Parent);
			Thread.sleep(2000);
			steps.clickJSButton(Account.Create_Campaign_InstallFBPixel, driver, name);
			steps.ChildWindow(driver);
			driver.manage().window().maximize();
			Thread.sleep(2000);
			/*			Text6 = driver.findElement(Account.Create_Campaign_HowitworksText).getText();
			System.out.println("Displayed Heading of Page is:       "+ Text6);
			test.log(Status.INFO, MarkupHelper.createLabel("Displayed Heading of Page is:       "+ Text6 , ExtentColor.BLUE));
			 */
			CampaignTitle13 = getscreen.capture(driver, "CampaignTitle13");
			test.addScreenCaptureFromPath(CampaignTitle13);
			System.out.println("User Navigated to Install Facebook Pixel page");
			test.log(Status.INFO, MarkupHelper.createLabel("User Navigated to Install Facebook Pixel page " , ExtentColor.BLUE));
			driver.close();
			driver.switchTo().window(Parent);
			Thread.sleep(2000);
			steps.clickJSButton(Account.Create_Campaign_MailSupport, driver, name);
			steps.ChildWindow(driver);
			driver.manage().window().maximize();
			Thread.sleep(2000);
			/*			Text7 = driver.findElement(Account.Create_Campaign_MailSupportText).getText();
			System.out.println("Displayed Heading of Page is:       "+ Text7);
			test.log(Status.INFO, MarkupHelper.createLabel("Displayed Heading of Page is:       "+ Text7 , ExtentColor.BLUE));
			 */
			CampaignTitle11 = getscreen.capture(driver, "CampaignTitle11");
			test.addScreenCaptureFromPath(CampaignTitle11);
			System.out.println("User clicked on Mail and Navigated to Mail page");
			test.log(Status.INFO, MarkupHelper.createLabel("User clicked on Mail and Navigated to Mail page " , ExtentColor.BLUE));
			driver.close();
			driver.switchTo().window(Parent);
			Thread.sleep(2000);
			steps.clickJSButton(Account.Create_Campaign_JoinFBGroup, driver, name);
			steps.ChildWindow(driver);
			driver.manage().window().maximize();
			Thread.sleep(2000);
			/*			Text8 = driver.findElement(Account.Create_Campaign_JoinFBGroupText).getText();
			System.out.println("Displayed Heading of Page is:       "+ Text8);
			test.log(Status.INFO, MarkupHelper.createLabel("Displayed Heading of Page is:       "+ Text8 , ExtentColor.BLUE));
			 */
			CampaignTitle12 = getscreen.capture(driver, "CampaignTitle12");
			test.addScreenCaptureFromPath(CampaignTitle12);
			System.out.println("User clicked on Join FB Group and Navigated to Facebook login page ");
			test.log(Status.INFO, MarkupHelper.createLabel("User clicked on Join FB Group and Navigated to Facebook login page " , ExtentColor.BLUE));
			driver.close();
			driver.switchTo().window(Parent);
			Thread.sleep(1000);
			StartCampaigntatus = passstatus;
			Assert.assertTrue(true);
			System.out.println("Start Campaign Successful");
			testresultdata.put(Integer.toString(cnt), new Object[] {name, "User able to Start Campaign", "Start Campaign Successful",passstatus});	
		}
		catch(Exception e)
		{
			System.out.println("Start Campaign Unsuccessful");
			testresultdata.put(Integer.toString(cnt), new Object[] {name, "User able to Start Campaign", "Start Campaign Unsuccessful",failstatus});
			StartCampaigntatus = failstatus;
			StartCampaigntatusfail = getscreen.capture(driver, "StartCampaigntatusfail");
			test.addScreenCaptureFromPath(StartCampaigntatusfail);
			Assert.assertTrue(false);
		}
	}
	//@Test(priority = 2)
	public void ProfileSettings() throws Exception
	{
		String name = new Object(){}.getClass().getEnclosingMethod().getName();
		cnt ++;
		try
		{
			test = extent.createTest("Profile Settings");
			System.out.println("Profile Settings Test Case Executing...");
			/*			commonmethods.Login(driver);
			test.log(Status.INFO, MarkupHelper.createLabel("User able to Login", ExtentColor.BLUE));
			 */
			Thread.sleep(3000);
			steps.clickButton(login.MyAccount, driver, name);
			steps.clickButton(Account.Account_ProfileSettings, driver, name);
			String Parent = driver.getWindowHandle();
			ProfileSettings1 = getscreen.capture(driver, "ProfileSettings1");
			test.addScreenCaptureFromPath(ProfileSettings1);
			test.log(Status.INFO, MarkupHelper.createLabel("User navigated to Profile Settings page", ExtentColor.BLUE));
			Thread.sleep(1000);
			String str = driver.findElement(Account.Account_ProfileSettings_PrivateRadio).getAttribute("checked");
			if (str.equalsIgnoreCase("true"))
			{
				System.out.println("Checkbox already selected");
			}
			else 
			{
				steps.clickButton(Account.Account_ProfileSettings_PrivateRadio, driver, name);
				System.out.println("Private Radio button checked now");
			}
			Thread.sleep(3000);
			/*String str1 = driver.findElement(Account.Account_ProfileSettings_AffiliateRadio).getAttribute("checked");
			if (str1.equalsIgnoreCase("true"))
			{
				System.out.println("Checkbox already selected");
				Thread.sleep(1000);
				steps.clickButton(Account.Account_ProfileSettings_AffiliateRadio, driver, name);
				System.out.println("Affiliate Radio button Unchecked now");
			}
			else 
			{
				steps.clickButton(Account.Account_ProfileSettings_AffiliateRadio, driver, name);
				System.out.println("Affiliate Radio button checked now");
steps.clickButton(Account.Account_ProfileSettings_AffiliateRadio, driver, name);
				System.out.println("Affiliate Radio button Unchecked now");
			}
			 */
			/*		steps.clickButton(Account.Account_ProfileSettings_AffiliateRadio, driver, name);
			Thread.sleep(1000);
			steps.clickButton(Account.Account_ProfileSettings_ChangesOK, driver, name);
			Thread.sleep(1000);
			steps.clickButton(Account.Account_ProfileSettings_AffiliateRadio, driver, name);
			 */
			steps.Clear(Account.Account_ProfileSettings_GoogleAnalytics, driver, name);
			steps.EnterText(Account.Account_ProfileSettings_GoogleAnalytics, driver, "GoogleAnalytics", name);
			steps.clickButton(Account.Account_ProfileSettings_GoogleAnalyticsSave, driver, name);
			Thread.sleep(1000);
			steps.clickButton(Account.Account_ProfileSettings_ChangesOK, driver, name);
			Thread.sleep(1000);
			steps.Clear(Account.Account_ProfileSettings_FBPixel, driver, name);
			steps.EnterText(Account.Account_ProfileSettings_FBPixel, driver, "FBPixel", name);
			steps.clickButton(Account.Account_ProfileSettings_FBPixelSave, driver, name);
			Thread.sleep(1000);
			steps.clickButton(Account.Account_ProfileSettings_ChangesOK, driver, name);
			Thread.sleep(1000);
			steps.Clear(Account.Account_ProfileSettings_GSTNumber, driver, name);
			steps.EnterText(Account.Account_ProfileSettings_GSTNumber, driver, strDataFileName, "GST", name);
			steps.clickButton(Account.Account_ProfileSettings_GSTNumberSave, driver, name);
			Thread.sleep(1000);
			steps.clickButton(Account.Account_ProfileSettings_ChangesOK, driver, name);
			Thread.sleep(1000);
			steps.clickButton(Account.Account_ProfileSettings_PayoutSettings, driver, name);
			Thread.sleep(1000);
			steps.Clear(Account.Account_ProfileSettings_PayoutSettingsAccNo, driver, name);
			steps.EnterText(Account.Account_ProfileSettings_PayoutSettingsAccNo, driver, strDataFileName, "AccNo", name);
			steps.Clear(Account.Account_ProfileSettings_PayoutSettingsAccName, driver, name);
			steps.EnterText(Account.Account_ProfileSettings_PayoutSettingsAccName, driver, strDataFileName, "AccName", name);
			steps.Clear(Account.Account_ProfileSettings_PayoutSettingsBankName, driver, name);
			steps.EnterText(Account.Account_ProfileSettings_PayoutSettingsBankName, driver, strDataFileName, "BankName", name);
			steps.Clear(Account.Account_ProfileSettings_PayoutSettingsBranchName, driver, name);
			steps.EnterText(Account.Account_ProfileSettings_PayoutSettingsBranchName, driver, strDataFileName, "BranchName", name);
			steps.Clear(Account.Account_ProfileSettings_PayoutSettingsIFSC, driver, name);
			steps.EnterText(Account.Account_ProfileSettings_PayoutSettingsIFSC, driver, strDataFileName, "IFSC", name);
			try
			{
				steps.Clear(Account.Account_ProfileSettings_PayoutSettingsPAN, driver, name);
				steps.EnterText(Account.Account_ProfileSettings_PayoutSettingsPAN, driver, strDataFileName, "PAN", name);
			}
			catch(Exception e)
			{
				System.out.println("PAN Number already saved");
			}
			steps.clickButton(Account.Account_ProfileSettings_PayoutSettingsSave, driver, name);
			Thread.sleep(1000);
			steps.clickButton(Account.Account_ProfileSettings_ChangesOK, driver, name);
			Thread.sleep(1000);
			steps.clickButton(Account.Account_ProfileSettings_PayoutSettingsDashboard, driver, name);
			Thread.sleep(1000);
			ProfileSettingstatus = passstatus;
			Assert.assertTrue(true);
			System.out.println("Profile Settings Successful");
			testresultdata.put(Integer.toString(cnt), new Object[] {name, "Profile Settings", "Profile Settings Successful",passstatus});	
		}
		catch(Exception e)
		{
			System.out.println("Profile Settings Unsuccessful");
			testresultdata.put(Integer.toString(cnt), new Object[] {name, "Profile Settings", "Profile Settings Unsuccessful",failstatus});
			ProfileSettingstatus = failstatus;
			ProfileSettingstatusfail = getscreen.capture(driver, "ProfileSettingstatusfail");
			test.addScreenCaptureFromPath(ProfileSettingstatusfail);
			Assert.assertTrue(false);
		}
	}
	@Test
	public void Storefronts()
	{
		String name = new Object(){}.getClass().getEnclosingMethod().getName();
		cnt ++;
		try 
		{
			test = extent.createTest("Storefronts");
			System.out.println("Storefronts Test Case Executing...");
			commonmethods.Login(driver);
			steps.clickButton(login.MyAccount, driver, name);
			steps.clickButton(Account.Account_Storefronts, driver, name);
			steps.clickButton(Account.Account_Storefront_Createstorefront, driver, name);
			steps.EnterText(Account.Account_Storefront_name, driver, strDataFileName, "Name", name);
			steps.EnterText(Account.Account_Storefront_description, driver, strDataFileName, "Description", name);
			steps.EnterText(Account.Account_Storefront_url, driver, strDataFileName, "URL", name);
			steps.clickButton(Account.Account_Storefront_create, driver, name);
			Thread.sleep(2000);
			String text1 = driver.findElement(Account.Account_Storefront_textdisplyed).getText();
			System.out.println("Displayed text:   "+text1);
			steps.clickButton(Account.Account_Storefront_ok, driver, name);
			Thread.sleep(2000);
			steps.clickJSButton(Account.Account_Storefront_campaign1, driver, name);
			steps.clickJSButton(Account.Account_Storefront_campaign2, driver, name);
		}
		catch(Exception e)
		{
		}
	}
	@BeforeClass
	public void before()
	{
		workbook = new HSSFWorkbook();
		sheet = workbook.createSheet(strDataFileName);
		testresultdata = new LinkedHashMap<String, Object[]>();
		testresultdata.put("1", new Object[] { "Test Step Name", "Expected Result","Actual Result" ,"Status"});
	}
	@AfterClass
	public void after() throws Exception
	{
		Set<String> keyset = testresultdata.keySet();
		int rownum = 0;
		for (String key : keyset) {
			Row row = sheet.createRow(rownum++);
			Object [] objArr = testresultdata.get(key);
			int cellnum = 0;
			for (Object obj : objArr) {
				Cell cell = row.createCell(cellnum++);
				if(obj instanceof Date) 
					cell.setCellValue((Date)obj);
				else if(obj instanceof Boolean)
					cell.setCellValue((Boolean)obj);
				else if(obj instanceof String)
					cell.setCellValue((String)obj);
				else if(obj instanceof Double)
					cell.setCellValue((Double)obj);
			}
		}
		try {
			FileOutputStream out = new FileOutputStream(new File("TestResults/"+strDataFileName +".xls"));
			workbook.write(out);
			out.close();
			System.out.println("Excel written successfully..");
			TestResult();
			//	driver.quit();
		}
		catch (FileNotFoundException e) 
		{
			e.printStackTrace();
		} 
		catch (IOException e) 
		{
			e.printStackTrace();
		}
	}
	public void TestResult() throws Exception
	{
		HSSFRow row;
		HSSFCell cell;
		WritableFont cellFont;
		WritableCellFormat cellFormat;
		Border border = null;
		int count;
		FileInputStream inp = new FileInputStream( "TestResults/"+strDataFileName +".xls");
		HSSFWorkbook wb = new HSSFWorkbook(inp);
		HSSFSheet sh = wb.getSheetAt(0);
		count=sh.getLastRowNum();
		for (int i = 1; i <= count; i++) 
		{
			row=sh.getRow(i);
			cell=row.getCell(3);
			String status=row.getCell(3).getStringCellValue();
			if(status.equalsIgnoreCase("Pass"))
			{
				HSSFCellStyle pass= wb.createCellStyle();
				pass.setFillForegroundColor(IndexedColors.GREEN.getIndex());
				pass.setFillPattern(CellStyle.SOLID_FOREGROUND);
				cell.setCellStyle(pass);
			}
			else
			{
				HSSFCellStyle fail= wb.createCellStyle();
				fail.setFillForegroundColor(IndexedColors.RED.getIndex());
				fail.setFillPattern(CellStyle.SOLID_FOREGROUND);
				cell.setCellStyle(fail);
			}
		}
		FileOutputStream out = new FileOutputStream("TestResults/"+strDataFileName+".xls");
		wb.write(out);
		wb.close();
	}
}
