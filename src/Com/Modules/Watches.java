package Com.Modules;
import java.io.File;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.IOException;
import java.util.Date;
import java.util.LinkedHashMap;
import java.util.Map;
import java.util.Set;
import org.apache.poi.hssf.usermodel.HSSFCell;
import org.apache.poi.hssf.usermodel.HSSFCellStyle;
import org.apache.poi.hssf.usermodel.HSSFRow;
import org.apache.poi.hssf.usermodel.HSSFSheet;
import org.apache.poi.hssf.usermodel.HSSFWorkbook;
import org.apache.poi.ss.usermodel.Cell;
import org.apache.poi.ss.usermodel.CellStyle;
import org.apache.poi.ss.usermodel.IndexedColors;
import org.apache.poi.ss.usermodel.Row;
import org.openqa.selenium.WebDriver;
import org.testng.Assert;
import org.testng.annotations.AfterClass;
import org.testng.annotations.BeforeClass;
import org.testng.annotations.Test;
import com.aventstack.extentreports.Status;
import com.aventstack.extentreports.markuputils.ExtentColor;
import com.aventstack.extentreports.markuputils.MarkupHelper;
import Com.ObjectRepository.Homepage2_OR;
import Com.ObjectRepository.Homepage_OR;
import Com.ObjectRepository.Login_OR;
import Com.ObjectRepository.Watches_OR;
import Com.Utilities.Browsers;
import Com.Utilities.CommonMethods;
import Com.Utilities.GetScreenShot;
import Com.Utilities.Screenshot;
import Com.Utilities.Steps;
import Com.Utilities.TestBase;
import jxl.format.Border;
import jxl.write.WritableCellFormat;
import jxl.write.WritableFont;
public class Watches extends TestBase
{
	WebDriver driver;
	Login_OR login = new Login_OR();
	Steps steps = new Steps();
	GetScreenShot getscreen = new GetScreenShot();
	Screenshot screen = new Screenshot();
	public String strAbsolutepath = new File("").getAbsolutePath();
	HSSFWorkbook workbook;
	HSSFSheet sheet;
	String strDataFileName = this.getClass().getSimpleName();
	Map<String, Object[]> testresultdata; 
	CommonMethods commonmethods = new CommonMethods();
	Homepage_OR home = new Homepage_OR();
	Homepage2_OR home2 = new Homepage2_OR();
	Watches_OR watches = new Watches_OR();
	Screenshot screenshot = new Screenshot();
	String passstatus = screenshot.status("PASS");
	String failstatus = screenshot.status("FAIL");
	int cnt = 2;
	public static String watchesDescription = "";
	public static String watchesMRP = "";
	public static String watchescqty = "";
	public static String watchesiqty = "";
	public static String watchesdqty = "";
	public static String watchesaddcart = "";
	public static String watchesupdateproduct1 = "";
	public static String watchesremovecart = "";
	public static String watchesstatus = "";
	public static String watchesstatusfail = "";
	public static String watcheshomepage = "";
	public static String watcheshomepage1 = "";
	public Watches()
	{
		Browsers b = new Browsers();
		this.driver = b.getBrowsers("FF");		 
	}
	@Test
	public void WatchesCheckout() throws Exception
	{
		String name = new Object(){}.getClass().getEnclosingMethod().getName();
		cnt ++;
		try 
		{
			test = extent.createTest("Watches Checkout");
			commonmethods.Login(driver);
			test.log(Status.INFO, MarkupHelper.createLabel("User able to Login", ExtentColor.BLUE));
			Thread.sleep(3000);
			steps.clickButton(home.Mydreamstore, driver, name);
			String homepage = getscreen.capture(driver, name);
			test.addScreenCaptureFromPath(homepage);
			String Parent = driver.getWindowHandle();
			steps.MoveElement(watches.Watcheslink, driver, name);
			steps.clickJSButton(watches.Watcheslink, driver, name);
			watcheshomepage = getscreen.capture(driver, "watcheshomepage");
			test.addScreenCaptureFromPath(watcheshomepage);
			test.log(Status.INFO, MarkupHelper.createLabel("User clicked on Watches link", ExtentColor.BLUE));
			Thread.sleep(2000);
			//steps.WaitUntilElementPresent(Polos.Polos_Image1, driver);
			steps.WaitElementPresent(watches.Watches_Image1, driver);
			steps.clickJSButton(watches.Watches_Image1, driver, name);
			watcheshomepage1 = getscreen.capture(driver, "watcheshomepage1");
			test.addScreenCaptureFromPath(watcheshomepage1);
			test.log(Status.INFO, MarkupHelper.createLabel("User clicked on First Image in watches", ExtentColor.BLUE));
			try
			{
				steps.clickButton(watches.Readmore, driver, name);
				watchesDescription = driver.findElement(watches.Watches_Description).getText();
				steps.clickButton(watches.Readless, driver, name);
				test.log(Status.INFO, MarkupHelper.createLabel("Product Description:  "+ watchesDescription, ExtentColor.BLUE));
			}
			catch(Exception e)
			{
				System.out.println("Description hasn't fetched");
			}
			watchesMRP = driver.findElement(watches.Watches_MRP).getText();
			test.log(Status.INFO, MarkupHelper.createLabel("Product Price:  "+watchesMRP, ExtentColor.BLUE));
			Thread.sleep(2000);
			watchescqty = driver.findElement(home.Qty).getAttribute("value");
			System.out.println("Current Quantity:  "+ watchescqty);
			test.log(Status.INFO, MarkupHelper.createLabel("Current Quantity of product:  "+watchescqty, ExtentColor.BLUE));
			Thread.sleep(1000);
			steps.clickButton(home.Trend_QtyIncrease, driver, name);
			watchesiqty = driver.findElement(home.Qty).getAttribute("value");
			System.out.println("Quantity after increase:  "+ watchesiqty);
			test.log(Status.INFO, MarkupHelper.createLabel("Current Quantity of product after Increase:  "+watchesiqty, ExtentColor.BLUE));
			steps.clickButton(home.Trend_QtyDecrease, driver, name);
			watchesdqty = driver.findElement(home.Qty).getAttribute("value");
			System.out.println("Quantity after decrease:  "+ watchesdqty);
			test.log(Status.INFO, MarkupHelper.createLabel("Current Quantity of product after increase:  "+watchesdqty, ExtentColor.BLUE));
			steps.clickButton(home.Trend_Addcart, driver, name);
			Thread.sleep(1000);
			test.log(Status.INFO, MarkupHelper.createLabel("User added product to Cart", ExtentColor.BLUE));
			watchesaddcart = getscreen.capture(driver, "watchesaddcart");
			test.addScreenCaptureFromPath(watchesaddcart);
			Thread.sleep(1000);
			steps.clickButton(home2.Addcart_edit, driver, name);
			test.log(Status.INFO, MarkupHelper.createLabel("User clicked on Edit", ExtentColor.BLUE));
			//steps.selectDropdown(home2.Addcart_selectsize, driver, strDataFileName, "SelectSize", name);
			steps.selectDropdown(home2.Addcart_selectqty, driver, strDataFileName, "Quantity", name);
			test.log(Status.INFO, MarkupHelper.createLabel("User selected Size and Quantity", ExtentColor.BLUE));
			steps.clickButton(home2.Addcart_cancel, driver, name);
			steps.clickButton(home2.Addcart_edit, driver, name);
			//steps.selectDropdown(home2.Addcart_selectsize, driver, strDataFileName, "SelectSize", name);
			steps.selectDropdown(home2.Addcart_selectqty, driver, strDataFileName, "Quantity", name);
			steps.clickButton(home2.Addcart_save, driver, name);
			watchesupdateproduct1 = getscreen.capture(driver, "watchesupdateproduct1");
			test.addScreenCaptureFromPath(watchesupdateproduct1);
			test.log(Status.INFO, MarkupHelper.createLabel("User updated product size and quantity", ExtentColor.BLUE));
			Thread.sleep(2000);
			steps.clickButton(home2.Biker_remove, driver, name);
			watchesremovecart = getscreen.capture(driver, "watchesremovecart");
			test.addScreenCaptureFromPath(watchesremovecart);
			test.log(Status.INFO, MarkupHelper.createLabel("User removed product from Cart", ExtentColor.BLUE));
			test.log(Status.INFO, MarkupHelper.createLabel("User Completed transactions of 1st Image in watches", ExtentColor.PURPLE));
			watchesstatus = passstatus;
			Assert.assertTrue(true);
			System.out.println("watches Successful");
			testresultdata.put(Integer.toString(cnt), new Object[] {name, "watches Checkout", "watches Checkout Successful",passstatus});	
		}
		catch(Exception e)
		{
			System.out.println("watches Checkout Unsuccessful");
			testresultdata.put(Integer.toString(cnt), new Object[] {name, "watches Checkout", "watches Checkout Unsuccessful",failstatus});
			watchesstatus = failstatus;
			watchesstatusfail = getscreen.capture(driver, "watchesstatusfail");
			test.addScreenCaptureFromPath(watchesstatusfail);
			Assert.assertTrue(false);
		}
	}
	@BeforeClass
	public void before()
	{
		workbook = new HSSFWorkbook();
		sheet = workbook.createSheet(strDataFileName);
		testresultdata = new LinkedHashMap<String, Object[]>();
		testresultdata.put("1", new Object[] { "Test Step Name", "Expected Result","Actual Result" ,"Status"});
	}
	@AfterClass
	public void after() throws Exception
	{
		Set<String> keyset = testresultdata.keySet();
		int rownum = 0;
		for (String key : keyset) {
			Row row = sheet.createRow(rownum++);
			Object [] objArr = testresultdata.get(key);
			int cellnum = 0;
			for (Object obj : objArr) {
				Cell cell = row.createCell(cellnum++);
				if(obj instanceof Date) 
					cell.setCellValue((Date)obj);
				else if(obj instanceof Boolean)
					cell.setCellValue((Boolean)obj);
				else if(obj instanceof String)
					cell.setCellValue((String)obj);
				else if(obj instanceof Double)
					cell.setCellValue((Double)obj);
			}
		}
		try {
			FileOutputStream out = new FileOutputStream(new File("TestResults/"+strDataFileName +".xls"));
			workbook.write(out);
			out.close();
			System.out.println("Excel written successfully..");
			TestResult();
			driver.quit();
		}
		catch (FileNotFoundException e) 
		{
			e.printStackTrace();
		} 
		catch (IOException e) 
		{
			e.printStackTrace();
		}
	}
	public void TestResult() throws Exception
	{
		HSSFRow row;
		HSSFCell cell;
		WritableFont cellFont;
		WritableCellFormat cellFormat;
		Border border = null;
		int count;
		FileInputStream inp = new FileInputStream( "TestResults/"+strDataFileName +".xls");
		HSSFWorkbook wb = new HSSFWorkbook(inp);
		HSSFSheet sh = wb.getSheetAt(0);
		count=sh.getLastRowNum();
		for (int i = 1; i <= count; i++) 
		{
			row=sh.getRow(i);
			cell=row.getCell(3);
			String status=row.getCell(3).getStringCellValue();
			if(status.equalsIgnoreCase("Pass"))
			{
				HSSFCellStyle pass= wb.createCellStyle();
				pass.setFillForegroundColor(IndexedColors.GREEN.getIndex());
				pass.setFillPattern(CellStyle.SOLID_FOREGROUND);
				cell.setCellStyle(pass);
			}
			else
			{
				HSSFCellStyle fail= wb.createCellStyle();
				fail.setFillForegroundColor(IndexedColors.RED.getIndex());
				fail.setFillPattern(CellStyle.SOLID_FOREGROUND);
				cell.setCellStyle(fail);
			}
		}
		FileOutputStream out = new FileOutputStream("TestResults/"+strDataFileName+".xls");
		wb.write(out);
		wb.close();
	}
}
