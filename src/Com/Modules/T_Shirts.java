package Com.Modules;
import java.io.File;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.IOException;
import java.util.LinkedHashMap;
import java.util.List;
import java.util.Map;
import java.util.Set;
import java.util.Date;
import java.util.concurrent.TimeUnit;
import org.apache.poi.hssf.usermodel.HSSFCell;
import org.apache.poi.hssf.usermodel.HSSFCellStyle;
import org.apache.poi.hssf.usermodel.HSSFRow;
import org.apache.poi.hssf.usermodel.HSSFSheet;
import org.apache.poi.hssf.usermodel.HSSFWorkbook;
import org.apache.poi.ss.usermodel.Row;
import org.apache.poi.ss.usermodel.Cell;
import org.apache.poi.ss.usermodel.CellStyle;
import org.apache.poi.ss.usermodel.IndexedColors;
import org.openqa.selenium.By;
import org.openqa.selenium.Keys;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.interactions.Actions;
import org.openqa.selenium.support.Color;
import org.openqa.selenium.support.ui.ExpectedConditions;
import org.openqa.selenium.support.ui.WebDriverWait;
import org.testng.Assert;
import org.testng.annotations.AfterClass;
import org.testng.annotations.BeforeClass;
import org.testng.annotations.Test;
import com.aventstack.extentreports.Status;
import com.aventstack.extentreports.markuputils.ExtentColor;
import com.aventstack.extentreports.markuputils.MarkupHelper;
import Com.ObjectRepository.Checkout_OR;
import Com.ObjectRepository.Homepage2_OR;
import Com.ObjectRepository.Homepage_OR;
import Com.ObjectRepository.Login_OR;
import Com.ObjectRepository.Tshirts_OR;
import Com.Utilities.Browsers;
import Com.Utilities.CommonMethods;
import Com.Utilities.GetScreenShot;
import Com.Utilities.Screenshot;
import Com.Utilities.Steps;
import Com.Utilities.TestBase;
import jxl.format.Border;
import jxl.format.UnderlineStyle;
import jxl.write.Colour;
import jxl.write.WritableCellFormat;
import jxl.write.WritableFont;
import jxl.write.WriteException;
public class T_Shirts extends TestBase
{
	WebDriver driver;
	Login_OR login = new Login_OR();
	Steps steps = new Steps();
	GetScreenShot getscreen = new GetScreenShot();
	Screenshot screen = new Screenshot();
	public String strAbsolutepath = new File("").getAbsolutePath();
	HSSFWorkbook workbook;
	HSSFSheet sheet;
	String strDataFileName = this.getClass().getSimpleName();
	Map<String, Object[]> testresultdata; 
	CommonMethods commonmethods = new CommonMethods();
	Homepage_OR home = new Homepage_OR();
	Homepage2_OR home2 = new Homepage2_OR();
	Tshirts_OR Tshirts = new Tshirts_OR();
	Checkout_OR checkout = new Checkout_OR();
	Screenshot screenshot = new Screenshot();
	String passstatus = screenshot.status("PASS");
	String failstatus = screenshot.status("FAIL");
	int cnt = 2;
	public static String Tshirtsstatus = "";
	public static String Tshirtsstatusfail = "";
	public static String TshirtsDescription1 = "";
	public static String TshirtsDescription2 = "";
	public static String TshirtsDescription3 = "";
	public static String TshirtsDescription4 = "";
	public static String TshirtsMRP = "";
	public static String TshirtsColor12 = "";
	public static String Tshirtshex12 = "";
	public static String TshirtsFirstcolor2 = "";
	public static String TshirtsColor22 = "";
	public static String Tshirtshex22 = "";
	public static String TshirtsSecondcolor2 = "";
	public static String TshirtsColor32 = "";
	public static String Tshirtshex32 = "";
	public static String TshirtsThirdcolor2 = "";
	public static String TshirtsColor42 = "";
	public static String Tshirtshex42 = "";
	public static String TshirtsFourthcolor2 = "";
	public static String TshirtsColor13 = "";
	public static String Tshirtshex13 = "";
	public static String TshirtsFirstcolor3 = "";
	public static String TshirtsColor23 = "";
	public static String Tshirtshex23 = "";
	public static String TshirtsSecondcolor3 = "";
	public static String TshirtsColor33 = "";
	public static String Tshirtshex33 = "";
	public static String TshirtsThirdcolor3 = "";
	public static String TshirtsColor43 = "";
	public static String Tshirtshex43 = "";
	public static String TshirtsFourthcolor3 = "";
	public static String TshirtsColor11 = "";
	public static String Tshirtshex11 = "";
	public static String TshirtsFirstcolor1 = "";
	public static String TshirtsColor21 = "";
	public static String Tshirtshex21 = "";
	public static String TshirtsSecondcolor1 = "";
	public static String TshirtsColor31 = "";
	public static String Tshirtshex31 = "";
	public static String TshirtsThirdcolor1 = "";
	public static String TshirtsColor41 = "";
	public static String Tshirtshex41 = "";
	public static String TshirtsFourthcolor1 = "";
	public static String Tshirtscqty = "";
	public static String Tshirtsiqty = "";
	public static String Tshirtsdqty = "";
	public static String TshirtsSizemsg = "";
	public static String TshirtssizeS = "";
	public static String TshirtssizeM = "";
	public static String TshirtssizeL = "";
	public static String TshirtssizeXL = "";
	public static String TshirtssizeXXL = "";
	public static String TshirtssizeXXXL = "";
	public static String Tshirtsbelow510 = "";
	public static String Tshirtsabove510 = "";
	public static String Tshirtstightfit = "";
	public static String Tshirtscomfit = "";
	public static String Tshirtsfindursize = "";
	public static String Tshirtsaddcart = "";
	public static String Tshirtsupdateproduct1 = "";
	public static String Tshirtsremovecart = "";
	public T_Shirts()
	{
		Browsers b = new Browsers();
		this.driver = b.getBrowsers("chrome1");		 
	}
	@Test
	public void TShirtsCheckout() throws Exception
	{
		String name = new Object(){}.getClass().getEnclosingMethod().getName();
		cnt ++;
		try 
		{
			test = extent.createTest("T Shirts Module");
			System.out.println("T Shirts Module Test Case Executing...");
			commonmethods.Login(driver);
			test.log(Status.INFO, MarkupHelper.createLabel("User able to Login", ExtentColor.BLUE));
			Thread.sleep(3000);
			steps.clickJSButton(home.Mydreamstore, driver, name);
			String homepage = getscreen.capture(driver, name);
			test.addScreenCaptureFromPath(homepage);
			String Parent = driver.getWindowHandle();
			steps.MoveElement(Tshirts.Tshirts, driver, name);
			String Tshirts1 = driver.findElement(Tshirts.Tshirts).getText();
			System.out.println(Tshirts1);
			steps.clickJSButton(Tshirts.Tshirts, driver, name);
			test.log(Status.INFO, MarkupHelper.createLabel("User clicked on T Shirts link", ExtentColor.BLUE));
			Thread.sleep(2000);
			try
			{
				steps.waitForElement(home.Filterpagetext, driver);
				String Filterpage1 = driver.findElement(home.Filterpagetext).getText();
				System.out.println("User navigated to :   "+Filterpage1);
				//steps.WaitUntilElementPresent(Tshirts.Tshirts_Image1, driver);
				steps.WaitElementPresent(Tshirts.Tshirts_Image1, driver);
				steps.clickJSButton(Tshirts.Tshirts_Image1, driver, name);
				test.log(Status.INFO, MarkupHelper.createLabel("User clicked on First Image in T Shirts", ExtentColor.BLUE));
			}
			catch(Exception e )
			{
				System.out.println("User navigated to different landing page");
			}
			Thread.sleep(1000);
			try
			{
				steps.clickJSButton(Tshirts.Readmore, driver, name);
				TshirtsDescription1 = driver.findElement(Tshirts.Tshirts_Description).getText();
				System.out.println("Product Description:   "+TshirtsDescription1);
				steps.clickJSButton(Tshirts.Readless, driver, name);
				test.log(Status.INFO, MarkupHelper.createLabel("Product Description:  "+ TshirtsDescription1, ExtentColor.BLUE));
			}
			catch(Exception e)
			{
				steps.clickJSButton(Tshirts.Readmore, driver, name);
				TshirtsDescription2 = driver.findElement(Tshirts.Tshirts_Description1).getText();
				System.out.println("Product Description:   "+TshirtsDescription2);
				steps.clickJSButton(Tshirts.Readless, driver, name);
				test.log(Status.INFO, MarkupHelper.createLabel("Product Description:  "+TshirtsDescription2, ExtentColor.BLUE));
			}
			Thread.sleep(1000);
			TshirtsMRP = driver.findElement(Tshirts.Tshirts_MRP).getText();
			test.log(Status.INFO, MarkupHelper.createLabel("Product Price:  "+TshirtsMRP, ExtentColor.ORANGE));
			Thread.sleep(2000);
			try
			{
				String prod2 = driver.findElement(Tshirts.Tshirts_Product2).getCssValue("color");
				System.out.println(prod2);
				steps.clickJSButton(Tshirts.Tshirts_Product2, driver, name);
				try
				{
					String col1 = driver.findElement(home2.color1).getCssValue("color");
					System.out.println(col1);
					steps.clickJSButton(home2.color1, driver, name);
					Thread.sleep(2000);
					TshirtsColor12 = driver.findElement(home2.color1).getCssValue("background-color");
					Tshirtshex12 = Color.fromString(TshirtsColor12).asHex();
					System.out.println("Image color: "+Tshirtshex12);
					test.log(Status.INFO, MarkupHelper.createLabel("User changed item color to:  "+Tshirtshex12, ExtentColor.BLUE));
					TshirtsFirstcolor2 = getscreen.capture(driver, "TshirtsFirstcolor2");
					test.addScreenCaptureFromPath(TshirtsFirstcolor2);
				}
				catch(Exception e)
				{
					System.out.println("Color 1 is not exists");
				}
				try
				{
					String col2 = driver.findElement(home2.color2).getCssValue("color");
					System.out.println(col2);
					steps.clickJSButton(home2.color2, driver, name);
					Thread.sleep(1000);
					TshirtsColor22 = driver.findElement(home2.color2).getCssValue("background-color");
					Tshirtshex22 = Color.fromString(TshirtsColor22).asHex();
					System.out.println("Image color: "+Tshirtshex22);
					test.log(Status.INFO, MarkupHelper.createLabel("User changed item color to:  "+Tshirtshex22, ExtentColor.BLUE));
					TshirtsSecondcolor2 = getscreen.capture(driver, "TshirtsSecondcolor2");
					test.addScreenCaptureFromPath(TshirtsSecondcolor2);
				}
				catch(Exception e)
				{
					System.out.println("Color 2 is not exists");
				}
				try
				{
					String col3 = driver.findElement(home2.color3).getCssValue("color");
					System.out.println(col3);
					steps.clickJSButton(home2.color3, driver, name);
					Thread.sleep(1000);
					TshirtsColor32 = driver.findElement(home2.color3).getCssValue("background-color");
					Tshirtshex32 = Color.fromString(TshirtsColor32).asHex();
					System.out.println("Image color: "+Tshirtshex32);
					test.log(Status.INFO, MarkupHelper.createLabel("User changed item color to:  "+Tshirtshex32, ExtentColor.BLUE));
					TshirtsThirdcolor2 = getscreen.capture(driver, "TshirtsThirdcolor2");
					test.addScreenCaptureFromPath(TshirtsThirdcolor2);
				}
				catch(Exception e)
				{
					System.out.println("Color 3 is not exists");
				}
				try 
				{
					String col4 = driver.findElement(home2.color4).getCssValue("color");
					System.out.println(col4);
					steps.clickJSButton(home2.color4, driver, name);
					Thread.sleep(1000);
					TshirtsColor42 = driver.findElement(home2.color4).getCssValue("background-color");
					Tshirtshex42 = Color.fromString(TshirtsColor42).asHex();
					System.out.println("Image color: "+Tshirtshex42);
					test.log(Status.INFO, MarkupHelper.createLabel("User changed item color to:  "+Tshirtshex42, ExtentColor.BLUE));
					TshirtsFourthcolor2 = getscreen.capture(driver, "TshirtsFourthcolor2");
					test.addScreenCaptureFromPath(TshirtsFourthcolor2);
				}
				catch(Exception e)
				{
					System.out.println("Color 4 is not exists");
				}
			}
			catch(Exception e)
			{
				System.out.println("Product 2 doesn't exists");
			}
			Thread.sleep(2000);
			try
			{
				String prod3 = driver.findElement(Tshirts.Tshirts_Product3).getCssValue("color");
				System.out.println(prod3);
				steps.clickJSButton(Tshirts.Tshirts_Product3, driver, name);
				try
				{
					String col1 = driver.findElement(home2.color1).getCssValue("color");
					System.out.println(col1);
					steps.clickJSButton(home2.color1, driver, name);
					Thread.sleep(2000);
					TshirtsColor13 = driver.findElement(home2.color1).getCssValue("background-color");
					Tshirtshex13 = Color.fromString(TshirtsColor13).asHex();
					System.out.println("Image color: "+Tshirtshex13);
					test.log(Status.INFO, MarkupHelper.createLabel("User changed item color to:  "+Tshirtshex13, ExtentColor.BLUE));
					TshirtsFirstcolor3 = getscreen.capture(driver, "TshirtsFirstcolor3");
					test.addScreenCaptureFromPath(TshirtsFirstcolor3);
				}
				catch(Exception e)
				{
					System.out.println("Color 1 is not exists");
				}
				try
				{
					String col2 = driver.findElement(home2.color2).getCssValue("color");
					System.out.println(col2);
					steps.clickJSButton(home2.color2, driver, name);
					Thread.sleep(1000);
					TshirtsColor23 = driver.findElement(home2.color2).getCssValue("background-color");
					Tshirtshex23 = Color.fromString(TshirtsColor23).asHex();
					System.out.println("Image color: "+Tshirtshex23);
					test.log(Status.INFO, MarkupHelper.createLabel("User changed item color to:  "+Tshirtshex23, ExtentColor.BLUE));
					TshirtsSecondcolor3 = getscreen.capture(driver, "TshirtsSecondcolor3");
					test.addScreenCaptureFromPath(TshirtsSecondcolor3);
				}
				catch(Exception e)
				{
					System.out.println("Color 2 is not exists");
				}
				try
				{
					String col3 = driver.findElement(home2.color3).getCssValue("color");
					System.out.println(col3);
					steps.clickJSButton(home2.color3, driver, name);
					Thread.sleep(1000);
					TshirtsColor33 = driver.findElement(home2.color3).getCssValue("background-color");
					Tshirtshex33 = Color.fromString(TshirtsColor33).asHex();
					System.out.println("Image color: "+Tshirtshex33);
					test.log(Status.INFO, MarkupHelper.createLabel("User changed item color to:  "+Tshirtshex33, ExtentColor.BLUE));
					TshirtsThirdcolor3 = getscreen.capture(driver, "TshirtsThirdcolor3");
					test.addScreenCaptureFromPath(TshirtsThirdcolor3);
				}
				catch(Exception e)
				{
					System.out.println("Color 3 is not exists");
				}
				try 
				{
					String col4 = driver.findElement(home2.color4).getCssValue("color");
					System.out.println(col4);
					steps.clickJSButton(home2.color4, driver, name);
					Thread.sleep(1000);
					TshirtsColor43 = driver.findElement(home2.color4).getCssValue("background-color");
					Tshirtshex43 = Color.fromString(TshirtsColor43).asHex();
					System.out.println("Image color: "+Tshirtshex43);
					test.log(Status.INFO, MarkupHelper.createLabel("User changed item color to:  "+Tshirtshex43, ExtentColor.BLUE));
					TshirtsFourthcolor3 = getscreen.capture(driver, "TshirtsFourthcolor3");
					test.addScreenCaptureFromPath(TshirtsFourthcolor3);
				}
				catch(Exception e)
				{
					System.out.println("Color 4 is not exists");
				}
			}
			catch(Exception e)
			{
				System.out.println("Product 3 doesn't exists");
			}
			try
			{
				String prod1 = driver.findElement(Tshirts.Tshirts_Product1).getCssValue("color");
				System.out.println(prod1);
				steps.clickJSButton(Tshirts.Tshirts_Product1, driver, name);
				try
				{
					String col1 = driver.findElement(home2.color1).getCssValue("color");
					System.out.println(col1);
					steps.clickJSButton(home2.color1, driver, name);
					Thread.sleep(2000);
					TshirtsColor11 = driver.findElement(home2.color1).getCssValue("background-color");
					Tshirtshex11 = Color.fromString(TshirtsColor11).asHex();
					System.out.println("Image color: "+Tshirtshex11);
					test.log(Status.INFO, MarkupHelper.createLabel("User changed item color to:  "+Tshirtshex11, ExtentColor.BLUE));
					TshirtsFirstcolor1 = getscreen.capture(driver, "TshirtsFirstcolor1");
					test.addScreenCaptureFromPath(TshirtsFirstcolor1);
				}
				catch(Exception e)
				{
					System.out.println("Color 1 is not exists");
				}
				try
				{
					String col2 = driver.findElement(home2.color2).getCssValue("color");
					System.out.println(col2);
					steps.clickJSButton(home2.color2, driver, name);
					Thread.sleep(1000);
					TshirtsColor21 = driver.findElement(home2.color2).getCssValue("background-color");
					Tshirtshex21 = Color.fromString(TshirtsColor21).asHex();
					System.out.println("Image color: "+Tshirtshex21);
					test.log(Status.INFO, MarkupHelper.createLabel("User changed item color to:  "+Tshirtshex21, ExtentColor.BLUE));
					TshirtsSecondcolor1 = getscreen.capture(driver, "TshirtsSecondcolor1");
					test.addScreenCaptureFromPath(TshirtsSecondcolor1);
				}
				catch(Exception e)
				{
					System.out.println("Color 2 is not exists");
				}
				try
				{
					String col3 = driver.findElement(home2.color3).getCssValue("color");
					System.out.println(col3);
					steps.clickJSButton(home2.color3, driver, name);
					Thread.sleep(1000);
					TshirtsColor31 = driver.findElement(home2.color3).getCssValue("background-color");
					Tshirtshex31 = Color.fromString(TshirtsColor31).asHex();
					System.out.println("Image color: "+Tshirtshex31);
					test.log(Status.INFO, MarkupHelper.createLabel("User changed item color to:  "+Tshirtshex31, ExtentColor.BLUE));
					TshirtsThirdcolor1 = getscreen.capture(driver, "TshirtsThirdcolor1");
					test.addScreenCaptureFromPath(TshirtsThirdcolor1);
				}
				catch(Exception e)
				{
					System.out.println("Color 3 is not exists");
				}
				try 
				{
					String col4 = driver.findElement(home2.color4).getCssValue("color");
					System.out.println(col4);
					steps.clickJSButton(home2.color4, driver, name);
					Thread.sleep(1000);
					TshirtsColor41 = driver.findElement(home2.color4).getCssValue("background-color");
					Tshirtshex41 = Color.fromString(TshirtsColor41).asHex();
					System.out.println("Image color: "+Tshirtshex41);
					test.log(Status.INFO, MarkupHelper.createLabel("User changed item color to:  "+Tshirtshex41, ExtentColor.BLUE));
					TshirtsFourthcolor1 = getscreen.capture(driver, "TshirtsFourthcolor1");
					test.addScreenCaptureFromPath(TshirtsFourthcolor1);
				}
				catch(Exception e)
				{
					System.out.println("Color 4 is not exists");
				}
			}
			catch(Exception e)
			{
				System.out.println("Product 1 doesn't exists");
			}
			Tshirtscqty = driver.findElement(home.Qty).getAttribute("value");
			System.out.println("Current Quantity:  "+ Tshirtscqty);
			test.log(Status.INFO, MarkupHelper.createLabel("Current Quantity of product:  "+Tshirtscqty, ExtentColor.BLUE));
			Thread.sleep(1000);
			steps.clickJSButton(home.Trend_QtyIncrease, driver, name);
			Tshirtsiqty = driver.findElement(home.Qty).getAttribute("value");
			System.out.println("Quantity after increase:  "+ Tshirtsiqty);
			test.log(Status.INFO, MarkupHelper.createLabel("Current Quantity of product after Increase:  "+Tshirtsiqty, ExtentColor.BLUE));
			steps.clickJSButton(home.Trend_QtyDecrease, driver, name);
			Tshirtsdqty = driver.findElement(home.Qty).getAttribute("value");
			System.out.println("Quantity after decrease:  "+ Tshirtsdqty);
			test.log(Status.INFO, MarkupHelper.createLabel("Current Quantity of product after increase:  "+Tshirtsdqty, ExtentColor.BLUE));
			steps.clickJSButton(home.Trend_Addcart, driver, name);
			TshirtsSizemsg = driver.findElement(home.Trend_Sizeerrormsg).getText();
			System.out.println("User clicked on Addcart before Size selection:  "+TshirtsSizemsg);
			test.log(Status.INFO, MarkupHelper.createLabel("User clicked on Addcart before Size selection:  "+TshirtsSizemsg, ExtentColor.RED));
			//steps.clickJSButton(home2.Tshirts_sizeS, driver, name);
			steps.clickJSButton(home2.Size_S, driver, name);
			TshirtssizeS = driver.findElement(home2.Size_S).getText();
			System.out.println("Selected Size:  "+TshirtssizeS);
			test.log(Status.INFO, MarkupHelper.createLabel("User selected size:  "+TshirtssizeS, ExtentColor.BLUE));
			steps.clickJSButton(home2.Size_M, driver, name);
			TshirtssizeM = driver.findElement(home2.Size_M).getText();
			System.out.println("Selected Size:  "+TshirtssizeM);
			test.log(Status.INFO, MarkupHelper.createLabel("User selected size:  "+TshirtssizeM, ExtentColor.BLUE));
			steps.clickJSButton(home2.Size_L, driver, name);
			TshirtssizeL = driver.findElement(home2.Size_L).getText();
			System.out.println("Selected Size:  "+TshirtssizeL);
			test.log(Status.INFO, MarkupHelper.createLabel("User selected size:  "+TshirtssizeL, ExtentColor.BLUE));
			steps.clickJSButton(home2.Size_XL, driver, name);
			TshirtssizeXL = driver.findElement(home2.Size_XL).getText();
			System.out.println("Selected Size:  "+TshirtssizeXL);
			test.log(Status.INFO, MarkupHelper.createLabel("User selected size:  "+TshirtssizeXL, ExtentColor.BLUE));
			steps.clickJSButton(home2.Size_XXL, driver, name);
			TshirtssizeXXL = driver.findElement(home2.Size_XXL).getText();
			System.out.println("Selected Size:  "+TshirtssizeXXL);
			test.log(Status.INFO, MarkupHelper.createLabel("User selected size:  "+TshirtssizeXXL, ExtentColor.BLUE));
			Thread.sleep(1000);
			try
			{
				steps.clickJSButton(home2.Size_XXXL, driver, name);
				TshirtssizeXXXL = driver.findElement(home2.Size_XXXL).getText();
				System.out.println("Selected Size:  "+TshirtssizeXXXL);
				test.log(Status.INFO, MarkupHelper.createLabel("User selected size:  "+TshirtssizeXXXL, ExtentColor.BLUE));
			}
			catch(Exception e)
			{
				System.out.println("XXXL size is not available");
			}
			
			steps.clickJSButton(home.Trend_FindUrSize, driver, name);
			test.log(Status.INFO, MarkupHelper.createLabel("User clicked on Find your size", ExtentColor.BLUE));
			Thread.sleep(1000);
			try
			{
				String text1 =  driver.findElement(home.Trend_FindUrSize_510radio).getText();
				System.out.println(text1);
				steps.clickJSButton(home.Trend_FindUrSize_510radio, driver, name);
				Tshirtsbelow510 = driver.findElement(home.Trend_FindUrSize_510radio).getText();
				System.out.println("User Selected:  "+Tshirtsbelow510);
				test.log(Status.INFO, MarkupHelper.createLabel("User selected Height:  "+Tshirtsbelow510, ExtentColor.BLUE));
				steps.clickJSButton(home.Trend_FindUrSize_above510radio, driver, name);
				Tshirtsabove510 = driver.findElement(home.Trend_FindUrSize_above510radio).getText();
				System.out.println("User Selected:  "+Tshirtsabove510);
				test.log(Status.INFO, MarkupHelper.createLabel("User selected Height:  "+Tshirtsabove510, ExtentColor.BLUE));
				steps.clickJSButton(home.Trend_FindUrSize_Tightradio, driver, name);
				Tshirtstightfit = driver.findElement(home.Trend_FindUrSize_Tightradio).getText();
				System.out.println("User Selected:  "+Tshirtstightfit);
				test.log(Status.INFO, MarkupHelper.createLabel("User selected Fit:  "+Tshirtstightfit, ExtentColor.BLUE));
				steps.clickJSButton(home.Trend_FindUrSize_Comfortradio, driver, name);
				Tshirtscomfit = driver.findElement(home.Trend_FindUrSize_Comfortradio).getText();
				System.out.println("User Selected:  "+Tshirtscomfit);
				test.log(Status.INFO, MarkupHelper.createLabel("User selected Fit:  "+Tshirtscomfit, ExtentColor.BLUE));
				Thread.sleep(1000);
				steps.selectDropdown(home.Trend_FindUrSize_sizeDD, driver, strDataFileName, "size", name);
				test.log(Status.INFO, MarkupHelper.createLabel("User selected shirt size", ExtentColor.BLUE));
				steps.clickJSButton(home.Trend_FindUrSize_submit, driver, name);
				test.log(Status.INFO, MarkupHelper.createLabel("User clicked on Submit", ExtentColor.BLUE));
				Tshirtsfindursize = getscreen.capture(driver, "Tshirtsfindursize");
				test.addScreenCaptureFromPath(Tshirtsfindursize);
				Thread.sleep(1000);
				steps.clickJSButton(home.Trend_FindUrSize_Okbutton, driver, name);
				test.log(Status.INFO, MarkupHelper.createLabel("system displayed size for given metrics", ExtentColor.BLUE));
				steps.clickJSButton(home.Trend_FindUrSize_closebutton, driver, name);
				test.log(Status.INFO, MarkupHelper.createLabel("User navigate to Product details page", ExtentColor.BLUE));
			}
			catch(Exception e)
			{
				steps.clickJSButton(home.Trend_FindUrSize_closebutton, driver, name);
				test.log(Status.INFO, MarkupHelper.createLabel("User navigate to Product details page", ExtentColor.BLUE));
			}
			steps.clickJSButton(home2.Size_L, driver, name);
			test.log(Status.INFO, MarkupHelper.createLabel("User selected size", ExtentColor.BLUE));
			steps.clickJSButton(home.Trend_Addcart, driver, name);
			Thread.sleep(1000);
			test.log(Status.INFO, MarkupHelper.createLabel("User added product to Cart", ExtentColor.BLUE));
			Tshirtsaddcart = getscreen.capture(driver, "Tshirtsaddcart");
			test.addScreenCaptureFromPath(Tshirtsaddcart);
			Thread.sleep(1000);
			try
			{
				String edit1 = driver.findElement(home2.Addcart_edit).getText();
				System.out.println(edit1);
				steps.clickJSButton(home2.Addcart_edit, driver, name);
				test.log(Status.INFO, MarkupHelper.createLabel("User clicked on Edit", ExtentColor.BLUE));
				steps.selectDropdown(home2.Addcart_selectsize, driver, strDataFileName, "SelectSize", name);
				steps.selectDropdown(home2.Addcart_selectqty, driver, strDataFileName, "Quantity", name);
				test.log(Status.INFO, MarkupHelper.createLabel("User selected Size and Quantity", ExtentColor.BLUE));
				steps.clickJSButton(home2.Addcart_cancel, driver, name);
				steps.clickJSButton(home2.Addcart_edit, driver, name);
				steps.selectDropdown(home2.Addcart_selectsize, driver, strDataFileName, "SelectSize", name);
				steps.selectDropdown(home2.Addcart_selectqty, driver, strDataFileName, "Quantity", name);
				steps.clickJSButton(home2.Addcart_save, driver, name);
				Tshirtsupdateproduct1 = getscreen.capture(driver, "Tshirtsupdateproduct1");
				test.addScreenCaptureFromPath(Tshirtsupdateproduct1);
				test.log(Status.INFO, MarkupHelper.createLabel("User updated product size and quantity", ExtentColor.BLUE));
			}
			catch(Exception e)
			{
				System.out.println("Edit feature is not available for this product");
			}
			Thread.sleep(2000);
			steps.clickJSButton(home2.Biker_remove, driver, name);
			Tshirtsremovecart = getscreen.capture(driver, "Tshirtsremovecart");
			test.addScreenCaptureFromPath(Tshirtsremovecart);
			test.log(Status.INFO, MarkupHelper.createLabel("User removed product from Cart", ExtentColor.BLUE));
			//checkout process
			steps.clickJSButton(home.Size_L, driver, name);
			test.log(Status.INFO, MarkupHelper.createLabel("User selected size", ExtentColor.BLUE));
			steps.clickJSButton(checkout.BuyNow, driver, name);
			test.log(Status.INFO, MarkupHelper.createLabel("User clicked on BuyNow, navigated to checkout page", ExtentColor.BLUE));
			String buynow1 = getscreen.capture(driver, "buynow1");
			test.addScreenCaptureFromPath(buynow1);
			Thread.sleep(2000);
			// Debit card option
			try
			{
				String Debit = driver.findElement(checkout.Debitcard).getText();
				System.out.println("Payment method:  "+Debit);
				steps.Verifytext(checkout.Debitcard, driver, Debit, name);
				steps.clickJSButton(checkout.Debitcard, driver, name);
				System.out.println("Debit card option is available");
				test.log(Status.INFO, MarkupHelper.createLabel("Payment method:  "+Debit   +    "      Available", ExtentColor.BLUE));
			}
			catch(Exception e)
			{
				System.out.println("Debit card option is not available");
				test.log(Status.INFO, MarkupHelper.createLabel("Debit card option is not available", ExtentColor.BLUE));
			}
			Thread.sleep(2000);
			// Credit card option
			try
			{
				String Credit = driver.findElement(checkout.Creditcard).getText();
				System.out.println("Payment method:  "+Credit);
				steps.Verifytext(checkout.Creditcard, driver, Credit, name);
				steps.clickJSButton(checkout.Creditcard, driver, name);
				System.out.println("Credit card option is available");
				test.log(Status.INFO, MarkupHelper.createLabel("Payment method:  "+Credit+    "      Available", ExtentColor.BLUE));
			}
			catch(Exception e)
			{
				System.out.println("Credit card option is not available");
				test.log(Status.INFO, MarkupHelper.createLabel("Credit card option is not available", ExtentColor.BLUE));
			}
			Thread.sleep(2000);
			// Net Banking option
			try
			{
				String NetBanking = driver.findElement(checkout.Netbanking).getText();
				System.out.println("Payment method:  "+NetBanking);
				steps.Verifytext(checkout.Netbanking, driver, NetBanking, name);
				steps.clickJSButton(checkout.Netbanking, driver, name);
				System.out.println("Net Banking option is available");
				test.log(Status.INFO, MarkupHelper.createLabel("Payment method:  "+NetBanking+    "      Available", ExtentColor.BLUE));
			}
			catch(Exception e)
			{
				System.out.println("Net Banking option is not available");
				test.log(Status.INFO, MarkupHelper.createLabel("Net Banking option is not available", ExtentColor.BLUE));
			}
			Thread.sleep(2000);
			// Paytm option
			try
			{
				String Paytm = driver.findElement(checkout.Paytm).getText();
				System.out.println("Payment method:  "+Paytm);
				steps.Verifytext(checkout.Paytm, driver, Paytm, name);
				steps.clickJSButton(checkout.Paytm, driver, name);
				System.out.println("Paytm option is available");
				test.log(Status.INFO, MarkupHelper.createLabel("Payment method:  "+Paytm+    "      Available", ExtentColor.BLUE));
			}
			catch(Exception e)
			{
				System.out.println("Paytm option is not available");
				test.log(Status.INFO, MarkupHelper.createLabel("Paytm option is not available", ExtentColor.BLUE));
			}
			Thread.sleep(2000);
			// MDS Wallet option
			try
			{
				String MDS = driver.findElement(checkout.MDSWallet).getText();
				System.out.println("Payment method:  "+MDS);
				steps.Verifytext(checkout.MDSWallet, driver, MDS, name);
				steps.clickJSButton(checkout.MDSWallet, driver, name);
				Thread.sleep(2000);
				String MDSpopup = driver.findElement(checkout.MDSWalletPOPup).getText();
				System.out.println("Payment method:  "+MDSpopup);
				Thread.sleep(2000);
				steps.clickJSButton(checkout.MDSWalletPOPupOK, driver, name);
				System.out.println("MDS Wallet option is available");
				test.log(Status.INFO, MarkupHelper.createLabel("Payment method:  "+MDS+    "       Available", ExtentColor.BLUE));
			}
			catch(Exception e)
			{
				System.out.println("MDS Wallet option is not available");
				test.log(Status.INFO, MarkupHelper.createLabel("MDS Wallet option is not available", ExtentColor.BLUE));
			}
			Thread.sleep(2000);
			// Cash on delivery 
			try
			{
				String cash = driver.findElement(checkout.CashonDeliveryText).getText();
				System.out.println("Payment method:  "+cash);
				test.log(Status.INFO, MarkupHelper.createLabel("Payment method:  "+cash+    "        Available", ExtentColor.BLUE));
				steps.Verifytext(checkout.CashonDeliveryText, driver, cash, name);
				steps.clickJSButton(checkout.CashonDeliveryradio, driver, name);
				Thread.sleep(2000);
				String buynow2 = driver.findElement(checkout.PopUpSaveMsg).getText();
				System.out.println("Displayed message:   "+buynow2);
				test.log(Status.INFO, MarkupHelper.createLabel("Displayed message:   "+buynow2, ExtentColor.BLUE));
				Thread.sleep(1000);
				String buynow3 = getscreen.capture(driver, "buynow3");
				test.addScreenCaptureFromPath(buynow3);
				steps.clickJSButton(checkout.Cashondelivery, driver, name);
				test.log(Status.INFO, MarkupHelper.createLabel("User clicked on Cash on Delivery link", ExtentColor.BLUE));
			}
			catch(Exception e)
			{
				System.out.println("Cash on Delivery is not available");
				test.log(Status.INFO, MarkupHelper.createLabel("Cash on Delivery is not available", ExtentColor.BLUE));
			}
			Thread.sleep(2000);
			String checkout1 = getscreen.capture(driver, "checkout1");
			test.addScreenCaptureFromPath(checkout1);
			steps.clickJSButton(checkout.CartIcon, driver, name);
			steps.clickJSButton(home.Trend_Addcart_remove, driver, name);
			Thread.sleep(1000);
			test.log(Status.INFO, MarkupHelper.createLabel("User Completed transactions of 1st Image in Tshirts", ExtentColor.PURPLE));
			Tshirtsstatus = passstatus;
			Assert.assertTrue(true);
			System.out.println("T Shirts Successful");
			testresultdata.put(Integer.toString(cnt), new Object[] {name, "T Shirts CheckOut", "T Shirts CheckOut Successful",passstatus});	
		}
		catch(Exception e)
		{
			System.out.println("T Shirts Unsuccessful");
			testresultdata.put(Integer.toString(cnt), new Object[] {name, "T Shirts CheckOut", "T Shirts CheckOut Unsuccessful",failstatus});
			Tshirtsstatus = failstatus;
			Tshirtsstatusfail = getscreen.capture(driver, "Tshirtsstatusfail");
			test.addScreenCaptureFromPath(Tshirtsstatusfail);
			Assert.assertTrue(false);
		}
	}
	@BeforeClass
	public void before()
	{
		workbook = new HSSFWorkbook();
		sheet = workbook.createSheet(strDataFileName);
		testresultdata = new LinkedHashMap<String, Object[]>();
		testresultdata.put("1", new Object[] { "Test Step Name", "Expected Result","Actual Result" ,"Status"});
	}
	@AfterClass
	public void after() throws Exception
	{
		Set<String> keyset = testresultdata.keySet();
		int rownum = 0;
		for (String key : keyset) {
			Row row = sheet.createRow(rownum++);
			Object [] objArr = testresultdata.get(key);
			int cellnum = 0;
			for (Object obj : objArr) {
				Cell cell = row.createCell(cellnum++);
				if(obj instanceof Date) 
					cell.setCellValue((Date)obj);
				else if(obj instanceof Boolean)
					cell.setCellValue((Boolean)obj);
				else if(obj instanceof String)
					cell.setCellValue((String)obj);
				else if(obj instanceof Double)
					cell.setCellValue((Double)obj);
			}
		}
		try {
			FileOutputStream out = new FileOutputStream(new File("TestResults/"+strDataFileName +".xls"));
			workbook.write(out);
			out.close();
			System.out.println("Excel written successfully..");
			TestResult();
			driver.quit();
		}
		catch (FileNotFoundException e) 
		{
			e.printStackTrace();
		} 
		catch (IOException e) 
		{
			e.printStackTrace();
		}
	}
	public void TestResult() throws Exception
	{
		HSSFRow row;
		HSSFCell cell;
		WritableFont cellFont;
		WritableCellFormat cellFormat;
		Border border = null;
		int count;
		FileInputStream inp = new FileInputStream( "TestResults/"+strDataFileName +".xls");
		HSSFWorkbook wb = new HSSFWorkbook(inp);
		HSSFSheet sh = wb.getSheetAt(0);
		count=sh.getLastRowNum();
		for (int i = 1; i <= count; i++) 
		{
			row=sh.getRow(i);
			cell=row.getCell(3);
			String status=row.getCell(3).getStringCellValue();
			if(status.equalsIgnoreCase("Pass"))
			{
				HSSFCellStyle pass= wb.createCellStyle();
				pass.setFillForegroundColor(IndexedColors.GREEN.getIndex());
				pass.setFillPattern(CellStyle.SOLID_FOREGROUND);
				cell.setCellStyle(pass);
			}
			else
			{
				HSSFCellStyle fail= wb.createCellStyle();
				fail.setFillForegroundColor(IndexedColors.RED.getIndex());
				fail.setFillPattern(CellStyle.SOLID_FOREGROUND);
				cell.setCellStyle(fail);
			}
		}
		FileOutputStream out = new FileOutputStream("TestResults/"+strDataFileName+".xls");
		wb.write(out);
		wb.close();
	}
}
