package Com.Modules;
import java.io.File;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.IOException;
import java.util.Date;
import java.util.LinkedHashMap;
import java.util.Map;
import java.util.Set;
import org.apache.poi.hssf.usermodel.HSSFCell;
import org.apache.poi.hssf.usermodel.HSSFCellStyle;
import org.apache.poi.hssf.usermodel.HSSFRow;
import org.apache.poi.hssf.usermodel.HSSFSheet;
import org.apache.poi.hssf.usermodel.HSSFWorkbook;
import org.apache.poi.ss.usermodel.Cell;
import org.apache.poi.ss.usermodel.CellStyle;
import org.apache.poi.ss.usermodel.IndexedColors;
import org.apache.poi.ss.usermodel.Row;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.support.Color;
import org.testng.Assert;
import org.testng.annotations.AfterClass;
import org.testng.annotations.BeforeClass;
import org.testng.annotations.Test;
import com.aventstack.extentreports.Status;
import com.aventstack.extentreports.markuputils.ExtentColor;
import com.aventstack.extentreports.markuputils.MarkupHelper;
import Com.ObjectRepository.Checkout_OR;
import Com.ObjectRepository.Hoodie_OR;
import Com.ObjectRepository.Homepage2_OR;
import Com.ObjectRepository.Homepage_OR;
import Com.ObjectRepository.Login_OR;
import Com.ObjectRepository.Tshirts_OR;
import Com.ObjectRepository.Hoodie_OR;
import Com.Utilities.Browsers;
import Com.Utilities.CommonMethods;
import Com.Utilities.GetScreenShot;
import Com.Utilities.Screenshot;
import Com.Utilities.Steps;
import Com.Utilities.TestBase;
import jxl.format.Border;
import jxl.write.WritableCellFormat;
import jxl.write.WritableFont;
public class Hoodie extends TestBase
{
	WebDriver driver;
	Login_OR login = new Login_OR();
	Steps steps = new Steps();
	GetScreenShot getscreen = new GetScreenShot();
	Screenshot screen = new Screenshot();
	public String strAbsolutepath = new File("").getAbsolutePath();
	HSSFWorkbook workbook;
	HSSFSheet sheet;
	String strDataFileName = this.getClass().getSimpleName();
	Map<String, Object[]> testresultdata; 
	CommonMethods commonmethods = new CommonMethods();
	Homepage_OR home = new Homepage_OR();
	Homepage2_OR home2 = new Homepage2_OR();
	Hoodie_OR Hoodie = new Hoodie_OR();
	Checkout_OR checkout = new Checkout_OR();
	Screenshot screenshot = new Screenshot();
	String passstatus = screenshot.status("PASS");
	String failstatus = screenshot.status("FAIL");
	int cnt = 2;
	public static String Hoodiestatus = "";
	public static String Hoodiestatusfail = "";
	public static String HoodieDescription1 = "";
	public static String HoodieDescription2 = "";
	public static String HoodieDescription3 = "";
	public static String HoodieDescription4 = "";
	public static String HoodieMRP = "";
	public static String HoodieColor12 = "";
	public static String Hoodiehex12 = "";
	public static String HoodieFirstcolor2 = "";
	public static String HoodieColor22 = "";
	public static String Hoodiehex22 = "";
	public static String HoodieSecondcolor2 = "";
	public static String HoodieColor32 = "";
	public static String Hoodiehex32 = "";
	public static String HoodieThirdcolor2 = "";
	public static String HoodieColor42 = "";
	public static String Hoodiehex42 = "";
	public static String HoodieFourthcolor2 = "";
	public static String HoodieColor13 = "";
	public static String Hoodiehex13 = "";
	public static String HoodieFirstcolor3 = "";
	public static String HoodieColor23 = "";
	public static String Hoodiehex23 = "";
	public static String HoodieSecondcolor3 = "";
	public static String HoodieColor33 = "";
	public static String Hoodiehex33 = "";
	public static String HoodieThirdcolor3 = "";
	public static String HoodieColor43 = "";
	public static String Hoodiehex43 = "";
	public static String HoodieFourthcolor3 = "";
	public static String HoodieColor11 = "";
	public static String Hoodiehex11 = "";
	public static String HoodieFirstcolor1 = "";
	public static String HoodieColor21 = "";
	public static String Hoodiehex21 = "";
	public static String HoodieSecondcolor1 = "";
	public static String HoodieColor31 = "";
	public static String Hoodiehex31 = "";
	public static String HoodieThirdcolor1 = "";
	public static String HoodieColor41 = "";
	public static String Hoodiehex41 = "";
	public static String HoodieFourthcolor1 = "";
	public static String Hoodiecqty = "";
	public static String Hoodieiqty = "";
	public static String Hoodiedqty = "";
	public static String HoodieSizemsg = "";
	public static String HoodiesizeS = "";
	public static String HoodiesizeM = "";
	public static String HoodiesizeL = "";
	public static String HoodiesizeXL = "";
	public static String HoodiesizeXXL = "";
	public static String HoodiesizeXXXL = "";
	public static String Hoodiebelow510 = "";
	public static String Hoodieabove510 = "";
	public static String Hoodietightfit = "";
	public static String Hoodiecomfit = "";
	public static String Hoodiefindursize = "";
	public static String Hoodieaddcart = "";
	public static String Hoodieupdateproduct1 = "";
	public static String Hoodieremovecart = "";
	public Hoodie()
	{
		Browsers b = new Browsers();
		this.driver = b.getBrowsers("chrome1");		 
	}
	@Test
	public void HoodieCheckout() throws Exception
	{
		String name = new Object(){}.getClass().getEnclosingMethod().getName();
		cnt ++;
		try 
		{
			test = extent.createTest("Hoodie Module");
			System.out.println("Hoodie Module Test Case Executing...");
			commonmethods.Login(driver);
			test.log(Status.INFO, MarkupHelper.createLabel("User able to Login", ExtentColor.BLUE));
			Thread.sleep(3000);
			steps.clickJSButton(home.Mydreamstore, driver, name);
			String homepage = getscreen.capture(driver, name);
			test.addScreenCaptureFromPath(homepage);
			String Parent = driver.getWindowHandle();
			steps.MoveElement(Hoodie.Hoodie, driver, name);
			String Hoodie1 = driver.findElement(Hoodie.Hoodie).getText();
			System.out.println(Hoodie1);
			steps.clickJSButton(Hoodie.Hoodie, driver, name);
			test.log(Status.INFO, MarkupHelper.createLabel("User clicked on Hoodie link", ExtentColor.BLUE));
			Thread.sleep(2000);
			try
			{
				steps.waitForElement(home.Filterpagetext, driver);
				String Filterpage1 = driver.findElement(home.Filterpagetext).getText();
				System.out.println("User navigated to :   "+Filterpage1);
				//steps.WaitUntilElementPresent(Hoodie.Hoodie_Image1, driver);
				steps.WaitElementPresent(Hoodie.Hoodie_Image1, driver);
				steps.clickJSButton(Hoodie.Hoodie_Image1, driver, name);
				test.log(Status.INFO, MarkupHelper.createLabel("User clicked on First Image in T Shirts", ExtentColor.BLUE));
			}
			catch(Exception e )
			{
				System.out.println("User navigated to different landing page");
			}
			Thread.sleep(1000);
			try
			{
				steps.clickJSButton(Hoodie.Readmore, driver, name);
				HoodieDescription1 = driver.findElement(Hoodie.Hoodie_Description).getText();
				System.out.println("Product Description:   "+HoodieDescription1);
				steps.clickJSButton(Hoodie.Readless, driver, name);
				test.log(Status.INFO, MarkupHelper.createLabel("Product Description:  "+ HoodieDescription1, ExtentColor.BLUE));
			}
			catch(Exception e)
			{
				steps.clickJSButton(Hoodie.Readmore, driver, name);
				HoodieDescription2 = driver.findElement(Hoodie.Hoodie_Description1).getText();
				System.out.println("Product Description:   "+HoodieDescription2);
				steps.clickJSButton(Hoodie.Readless, driver, name);
				test.log(Status.INFO, MarkupHelper.createLabel("Product Description:  "+HoodieDescription2, ExtentColor.BLUE));
			}
			Thread.sleep(1000);
			HoodieMRP = driver.findElement(Hoodie.Hoodie_MRP).getText();
			test.log(Status.INFO, MarkupHelper.createLabel("Product Price:  "+HoodieMRP, ExtentColor.ORANGE));
			Thread.sleep(2000);
			try
			{
				String prod2 = driver.findElement(Hoodie.Hoodie_Product2).getCssValue("color");
				System.out.println("Product:  "+prod2);
				steps.clickJSButton(Hoodie.Hoodie_Product2, driver, name);
				try
				{
					String col1 = driver.findElement(home2.color1).getCssValue("color");
					System.out.println(col1);
					steps.clickJSButton(home2.color1, driver, name);
					Thread.sleep(2000);
					HoodieColor12 = driver.findElement(home2.color1).getCssValue("background-color");
					Hoodiehex12 = Color.fromString(HoodieColor12).asHex();
					System.out.println("Image color: "+Hoodiehex12);
					test.log(Status.INFO, MarkupHelper.createLabel("User changed item color to:  "+Hoodiehex12, ExtentColor.BLUE));
					HoodieFirstcolor2 = getscreen.capture(driver, "HoodieFirstcolor2");
					test.addScreenCaptureFromPath(HoodieFirstcolor2);
				}
				catch(Exception e)
				{
					System.out.println("Color 1 is not exists");
				}
				try
				{
					String col2 = driver.findElement(home2.color2).getCssValue("color");
					System.out.println(col2);
					steps.clickJSButton(home2.color2, driver, name);
					Thread.sleep(1000);
					HoodieColor22 = driver.findElement(home2.color2).getCssValue("background-color");
					Hoodiehex22 = Color.fromString(HoodieColor22).asHex();
					System.out.println("Image color: "+Hoodiehex22);
					test.log(Status.INFO, MarkupHelper.createLabel("User changed item color to:  "+Hoodiehex22, ExtentColor.BLUE));
					HoodieSecondcolor2 = getscreen.capture(driver, "HoodieSecondcolor2");
					test.addScreenCaptureFromPath(HoodieSecondcolor2);
				}
				catch(Exception e)
				{
					System.out.println("Color 2 is not exists");
				}
				try
				{
					String col3 = driver.findElement(home2.color3).getCssValue("color");
					System.out.println(col3);
					steps.clickJSButton(home2.color3, driver, name);
					Thread.sleep(1000);
					HoodieColor32 = driver.findElement(home2.color3).getCssValue("background-color");
					Hoodiehex32 = Color.fromString(HoodieColor32).asHex();
					System.out.println("Image color: "+Hoodiehex32);
					test.log(Status.INFO, MarkupHelper.createLabel("User changed item color to:  "+Hoodiehex32, ExtentColor.BLUE));
					HoodieThirdcolor2 = getscreen.capture(driver, "HoodieThirdcolor2");
					test.addScreenCaptureFromPath(HoodieThirdcolor2);
				}
				catch(Exception e)
				{
					System.out.println("Color 3 is not exists");
				}
				try 
				{
					String col4 = driver.findElement(home2.color4).getCssValue("color");
					System.out.println(col4);
					steps.clickJSButton(home2.color4, driver, name);
					Thread.sleep(1000);
					HoodieColor42 = driver.findElement(home2.color4).getCssValue("background-color");
					Hoodiehex42 = Color.fromString(HoodieColor42).asHex();
					System.out.println("Image color: "+Hoodiehex42);
					test.log(Status.INFO, MarkupHelper.createLabel("User changed item color to:  "+Hoodiehex42, ExtentColor.BLUE));
					HoodieFourthcolor2 = getscreen.capture(driver, "HoodieFourthcolor2");
					test.addScreenCaptureFromPath(HoodieFourthcolor2);
				}
				catch(Exception e)
				{
					System.out.println("Color 4 is not exists");
				}
			}
			catch(Exception e)
			{
				System.out.println("Product 2 doesn't exists");
			}
			Thread.sleep(2000);
			try
			{
				String prod3 = driver.findElement(Hoodie.Hoodie_Product3).getCssValue("color");
				System.out.println("Product:  "+prod3);
				steps.clickJSButton(Hoodie.Hoodie_Product3, driver, name);
				try
				{
					String col1 = driver.findElement(home2.color1).getCssValue("color");
					System.out.println(col1);
					steps.clickJSButton(home2.color1, driver, name);
					Thread.sleep(2000);
					HoodieColor13 = driver.findElement(home2.color1).getCssValue("background-color");
					Hoodiehex13 = Color.fromString(HoodieColor13).asHex();
					System.out.println("Image color: "+Hoodiehex13);
					test.log(Status.INFO, MarkupHelper.createLabel("User changed item color to:  "+Hoodiehex13, ExtentColor.BLUE));
					HoodieFirstcolor3 = getscreen.capture(driver, "HoodieFirstcolor3");
					test.addScreenCaptureFromPath(HoodieFirstcolor3);
				}
				catch(Exception e)
				{
					System.out.println("Color 1 is not exists");
				}
				try
				{
					String col2 = driver.findElement(home2.color2).getCssValue("color");
					System.out.println(col2);
					steps.clickJSButton(home2.color2, driver, name);
					Thread.sleep(1000);
					HoodieColor23 = driver.findElement(home2.color2).getCssValue("background-color");
					Hoodiehex23 = Color.fromString(HoodieColor23).asHex();
					System.out.println("Image color: "+Hoodiehex23);
					test.log(Status.INFO, MarkupHelper.createLabel("User changed item color to:  "+Hoodiehex23, ExtentColor.BLUE));
					HoodieSecondcolor3 = getscreen.capture(driver, "HoodieSecondcolor3");
					test.addScreenCaptureFromPath(HoodieSecondcolor3);
				}
				catch(Exception e)
				{
					System.out.println("Color 2 is not exists");
				}
				try
				{
					String col3 = driver.findElement(home2.color3).getCssValue("color");
					System.out.println(col3);
					steps.clickJSButton(home2.color3, driver, name);
					Thread.sleep(1000);
					HoodieColor33 = driver.findElement(home2.color3).getCssValue("background-color");
					Hoodiehex33 = Color.fromString(HoodieColor33).asHex();
					System.out.println("Image color: "+Hoodiehex33);
					test.log(Status.INFO, MarkupHelper.createLabel("User changed item color to:  "+Hoodiehex33, ExtentColor.BLUE));
					HoodieThirdcolor3 = getscreen.capture(driver, "HoodieThirdcolor3");
					test.addScreenCaptureFromPath(HoodieThirdcolor3);
				}
				catch(Exception e)
				{
					System.out.println("Color 3 is not exists");
				}
				try 
				{
					String col4 = driver.findElement(home2.color4).getCssValue("color");
					System.out.println(col4);
					steps.clickJSButton(home2.color4, driver, name);
					Thread.sleep(1000);
					HoodieColor43 = driver.findElement(home2.color4).getCssValue("background-color");
					Hoodiehex43 = Color.fromString(HoodieColor43).asHex();
					System.out.println("Image color: "+Hoodiehex43);
					test.log(Status.INFO, MarkupHelper.createLabel("User changed item color to:  "+Hoodiehex43, ExtentColor.BLUE));
					HoodieFourthcolor3 = getscreen.capture(driver, "HoodieFourthcolor3");
					test.addScreenCaptureFromPath(HoodieFourthcolor3);
				}
				catch(Exception e)
				{
					System.out.println("Color 4 is not exists");
				}
			}
			catch(Exception e)
			{
				System.out.println("Product 3 doesn't exists");
			}
			Thread.sleep(2000);
			try
			{
				String prod1 = driver.findElement(Hoodie.Hoodie_Product1).getCssValue("color");
				System.out.println("Product:  "+prod1);
				steps.clickJSButton(Hoodie.Hoodie_Product1, driver, name);
				try
				{
					String col1 = driver.findElement(home2.color1).getCssValue("color");
					System.out.println(col1);
					steps.clickJSButton(home2.color1, driver, name);
					Thread.sleep(2000);
					HoodieColor11 = driver.findElement(home2.color1).getCssValue("background-color");
					Hoodiehex11 = Color.fromString(HoodieColor11).asHex();
					System.out.println("Image color: "+Hoodiehex11);
					test.log(Status.INFO, MarkupHelper.createLabel("User changed item color to:  "+Hoodiehex11, ExtentColor.BLUE));
					HoodieFirstcolor1 = getscreen.capture(driver, "HoodieFirstcolor1");
					test.addScreenCaptureFromPath(HoodieFirstcolor1);
				}
				catch(Exception e)
				{
					System.out.println("Color 1 is not exists");
				}
				try
				{
					String col2 = driver.findElement(home2.color2).getCssValue("color");
					System.out.println(col2);
					steps.clickJSButton(home2.color2, driver, name);
					Thread.sleep(1000);
					HoodieColor21 = driver.findElement(home2.color2).getCssValue("background-color");
					Hoodiehex21 = Color.fromString(HoodieColor21).asHex();
					System.out.println("Image color: "+Hoodiehex21);
					test.log(Status.INFO, MarkupHelper.createLabel("User changed item color to:  "+Hoodiehex21, ExtentColor.BLUE));
					HoodieSecondcolor1 = getscreen.capture(driver, "HoodieSecondcolor1");
					test.addScreenCaptureFromPath(HoodieSecondcolor1);
				}
				catch(Exception e)
				{
					System.out.println("Color 2 is not exists");
				}
				try
				{
					String col3 = driver.findElement(home2.color3).getCssValue("color");
					System.out.println(col3);
					steps.clickJSButton(home2.color3, driver, name);
					Thread.sleep(1000);
					HoodieColor31 = driver.findElement(home2.color3).getCssValue("background-color");
					Hoodiehex31 = Color.fromString(HoodieColor31).asHex();
					System.out.println("Image color: "+Hoodiehex31);
					test.log(Status.INFO, MarkupHelper.createLabel("User changed item color to:  "+Hoodiehex31, ExtentColor.BLUE));
					HoodieThirdcolor1 = getscreen.capture(driver, "HoodieThirdcolor1");
					test.addScreenCaptureFromPath(HoodieThirdcolor1);
				}
				catch(Exception e)
				{
					System.out.println("Color 3 is not exists");
				}
				try 
				{
					String col4 = driver.findElement(home2.color4).getCssValue("color");
					System.out.println(col4);
					steps.clickJSButton(home2.color4, driver, name);
					Thread.sleep(1000);
					HoodieColor41 = driver.findElement(home2.color4).getCssValue("background-color");
					Hoodiehex41 = Color.fromString(HoodieColor41).asHex();
					System.out.println("Image color: "+Hoodiehex41);
					test.log(Status.INFO, MarkupHelper.createLabel("User changed item color to:  "+Hoodiehex41, ExtentColor.BLUE));
					HoodieFourthcolor1 = getscreen.capture(driver, "HoodieFourthcolor1");
					test.addScreenCaptureFromPath(HoodieFourthcolor1);
				}
				catch(Exception e)
				{
					System.out.println("Color 4 is not exists");
				}
			}
			catch(Exception e)
			{
				System.out.println("Product 1 doesn't exists");
			}
			Hoodiecqty = driver.findElement(home.Qty).getAttribute("value");
			System.out.println("Current Quantity:  "+ Hoodiecqty);
			test.log(Status.INFO, MarkupHelper.createLabel("Current Quantity of product:  "+Hoodiecqty, ExtentColor.BLUE));
			Thread.sleep(1000);
			steps.clickJSButton(home.Trend_QtyIncrease, driver, name);
			Hoodieiqty = driver.findElement(home.Qty).getAttribute("value");
			System.out.println("Quantity after increase:  "+ Hoodieiqty);
			test.log(Status.INFO, MarkupHelper.createLabel("Current Quantity of product after Increase:  "+Hoodieiqty, ExtentColor.BLUE));
			steps.clickJSButton(home.Trend_QtyDecrease, driver, name);
			Hoodiedqty = driver.findElement(home.Qty).getAttribute("value");
			System.out.println("Quantity after decrease:  "+ Hoodiedqty);
			test.log(Status.INFO, MarkupHelper.createLabel("Current Quantity of product after increase:  "+Hoodiedqty, ExtentColor.BLUE));
			steps.clickJSButton(home.Trend_Addcart, driver, name);
			HoodieSizemsg = driver.findElement(home.Trend_Sizeerrormsg).getText();
			System.out.println("User clicked on Addcart before Size selection:  "+HoodieSizemsg);
			test.log(Status.INFO, MarkupHelper.createLabel("User clicked on Addcart before Size selection:  "+HoodieSizemsg, ExtentColor.RED));
			//steps.clickJSButton(home2.Hoodie_sizeS, driver, name);
			steps.clickJSButton(home2.Size_S, driver, name);
			HoodiesizeS = driver.findElement(home2.Size_S).getText();
			System.out.println("Selected Size:  "+HoodiesizeS);
			test.log(Status.INFO, MarkupHelper.createLabel("User selected size:  "+HoodiesizeS, ExtentColor.BLUE));
			steps.clickJSButton(home2.Size_M, driver, name);
			HoodiesizeM = driver.findElement(home2.Size_M).getText();
			System.out.println("Selected Size:  "+HoodiesizeM);
			test.log(Status.INFO, MarkupHelper.createLabel("User selected size:  "+HoodiesizeM, ExtentColor.BLUE));
			steps.clickJSButton(home2.Size_L, driver, name);
			HoodiesizeL = driver.findElement(home2.Size_L).getText();
			System.out.println("Selected Size:  "+HoodiesizeL);
			test.log(Status.INFO, MarkupHelper.createLabel("User selected size:  "+HoodiesizeL, ExtentColor.BLUE));
			steps.clickJSButton(home2.Size_XL, driver, name);
			HoodiesizeXL = driver.findElement(home2.Size_XL).getText();
			System.out.println("Selected Size:  "+HoodiesizeXL);
			test.log(Status.INFO, MarkupHelper.createLabel("User selected size:  "+HoodiesizeXL, ExtentColor.BLUE));
			steps.clickJSButton(home2.Size_XXL, driver, name);
			HoodiesizeXXL = driver.findElement(home2.Size_XXL).getText();
			System.out.println("Selected Size:  "+HoodiesizeXXL);
			test.log(Status.INFO, MarkupHelper.createLabel("User selected size:  "+HoodiesizeXXL, ExtentColor.BLUE));
			steps.clickJSButton(home2.Size_XXXL, driver, name);
			HoodiesizeXXXL = driver.findElement(home2.Size_XXXL).getText();
			System.out.println("Selected Size:  "+HoodiesizeXXXL);
			test.log(Status.INFO, MarkupHelper.createLabel("User selected size:  "+HoodiesizeXXXL, ExtentColor.BLUE));
			steps.clickJSButton(home.Trend_FindUrSize, driver, name);
			test.log(Status.INFO, MarkupHelper.createLabel("User clicked on Find your size", ExtentColor.BLUE));
			Thread.sleep(1000);
			try
			{
				String text1 =  driver.findElement(home.Trend_FindUrSize_510radio).getText();
				System.out.println(text1);
				steps.clickJSButton(home.Trend_FindUrSize_510radio, driver, name);
				Hoodiebelow510 = driver.findElement(home.Trend_FindUrSize_510radio).getText();
				System.out.println("User Selected:  "+Hoodiebelow510);
				test.log(Status.INFO, MarkupHelper.createLabel("User selected Height:  "+Hoodiebelow510, ExtentColor.BLUE));
				steps.clickJSButton(home.Trend_FindUrSize_above510radio, driver, name);
				Hoodieabove510 = driver.findElement(home.Trend_FindUrSize_above510radio).getText();
				System.out.println("User Selected:  "+Hoodieabove510);
				test.log(Status.INFO, MarkupHelper.createLabel("User selected Height:  "+Hoodieabove510, ExtentColor.BLUE));
				steps.clickJSButton(home.Trend_FindUrSize_Tightradio, driver, name);
				Hoodietightfit = driver.findElement(home.Trend_FindUrSize_Tightradio).getText();
				System.out.println("User Selected:  "+Hoodietightfit);
				test.log(Status.INFO, MarkupHelper.createLabel("User selected Fit:  "+Hoodietightfit, ExtentColor.BLUE));
				steps.clickJSButton(home.Trend_FindUrSize_Comfortradio, driver, name);
				Hoodiecomfit = driver.findElement(home.Trend_FindUrSize_Comfortradio).getText();
				System.out.println("User Selected:  "+Hoodiecomfit);
				test.log(Status.INFO, MarkupHelper.createLabel("User selected Fit:  "+Hoodiecomfit, ExtentColor.BLUE));
				Thread.sleep(1000);
				steps.selectDropdown(home.Trend_FindUrSize_sizeDD, driver, strDataFileName, "size", name);
				test.log(Status.INFO, MarkupHelper.createLabel("User selected shirt size", ExtentColor.BLUE));
				steps.clickJSButton(home.Trend_FindUrSize_submit, driver, name);
				test.log(Status.INFO, MarkupHelper.createLabel("User clicked on Submit", ExtentColor.BLUE));
				Hoodiefindursize = getscreen.capture(driver, "Hoodiefindursize");
				test.addScreenCaptureFromPath(Hoodiefindursize);
				Thread.sleep(1000);
				steps.clickJSButton(home.Trend_FindUrSize_Okbutton, driver, name);
				test.log(Status.INFO, MarkupHelper.createLabel("system displayed size for given metrics", ExtentColor.BLUE));
				steps.clickJSButton(home.Trend_FindUrSize_closebutton, driver, name);
				test.log(Status.INFO, MarkupHelper.createLabel("User navigate to Product details page", ExtentColor.BLUE));
			}
			catch(Exception e)
			{
				steps.clickJSButton(home.Trend_FindUrSize_closebutton, driver, name);
				test.log(Status.INFO, MarkupHelper.createLabel("User navigate to Product details page", ExtentColor.BLUE));
			}
			Thread.sleep(1000);
			steps.clickJSButton(home2.Size_L, driver, name);
			test.log(Status.INFO, MarkupHelper.createLabel("User selected size", ExtentColor.BLUE));
			steps.clickJSButton(home.Trend_Addcart, driver, name);
			Thread.sleep(1000);
			test.log(Status.INFO, MarkupHelper.createLabel("User added product to Cart", ExtentColor.BLUE));
			Hoodieaddcart = getscreen.capture(driver, "Hoodieaddcart");
			test.addScreenCaptureFromPath(Hoodieaddcart);
			Thread.sleep(1000);
			try
			{
				String edit1 = driver.findElement(home2.Addcart_edit).getText();
				System.out.println(edit1);
				steps.clickJSButton(home2.Addcart_edit, driver, name);
				test.log(Status.INFO, MarkupHelper.createLabel("User clicked on Edit", ExtentColor.BLUE));
				steps.selectDropdown(home2.Addcart_selectsize, driver, strDataFileName, "SelectSize", name);
				steps.selectDropdown(home2.Addcart_selectqty, driver, strDataFileName, "Quantity", name);
				test.log(Status.INFO, MarkupHelper.createLabel("User selected Size and Quantity", ExtentColor.BLUE));
				steps.clickJSButton(home2.Addcart_cancel, driver, name);
				steps.clickJSButton(home2.Addcart_edit, driver, name);
				steps.selectDropdown(home2.Addcart_selectsize, driver, strDataFileName, "SelectSize", name);
				steps.selectDropdown(home2.Addcart_selectqty, driver, strDataFileName, "Quantity", name);
				steps.clickJSButton(home2.Addcart_save, driver, name);
				Hoodieupdateproduct1 = getscreen.capture(driver, "Hoodieupdateproduct1");
				test.addScreenCaptureFromPath(Hoodieupdateproduct1);
				test.log(Status.INFO, MarkupHelper.createLabel("User updated product size and quantity", ExtentColor.BLUE));
			}
			catch(Exception e)
			{
				System.out.println("Edit feature is not available for this product");
			}
			Thread.sleep(2000);
			steps.clickJSButton(home2.Biker_remove, driver, name);
			Hoodieremovecart = getscreen.capture(driver, "Hoodieremovecart");
			test.addScreenCaptureFromPath(Hoodieremovecart);
			test.log(Status.INFO, MarkupHelper.createLabel("User removed product from Cart", ExtentColor.BLUE));
			//checkout process
			steps.clickJSButton(home.Size_L, driver, name);
			test.log(Status.INFO, MarkupHelper.createLabel("User selected size", ExtentColor.BLUE));
			steps.clickJSButton(checkout.BuyNow, driver, name);
			test.log(Status.INFO, MarkupHelper.createLabel("User clicked on BuyNow, navigated to checkout page", ExtentColor.BLUE));
			String buynow1 = getscreen.capture(driver, "buynow1");
			test.addScreenCaptureFromPath(buynow1);
			Thread.sleep(2000);
			// Debit card option
			try
			{
				String Debit = driver.findElement(checkout.Debitcard).getText();
				System.out.println("Payment method:  "+Debit);
				steps.Verifytext(checkout.Debitcard, driver, Debit, name);
				steps.clickJSButton(checkout.Debitcard, driver, name);
				System.out.println("Debit card option is available");
				test.log(Status.INFO, MarkupHelper.createLabel("Payment method:  "+Debit   +    "      Available", ExtentColor.BLUE));
			}
			catch(Exception e)
			{
				System.out.println("Debit card option is not available");
				test.log(Status.INFO, MarkupHelper.createLabel("Debit card option is not available", ExtentColor.BLUE));
			}
			Thread.sleep(2000);
			// Credit card option
			try
			{
				String Credit = driver.findElement(checkout.Creditcard).getText();
				System.out.println("Payment method:  "+Credit);
				steps.Verifytext(checkout.Creditcard, driver, Credit, name);
				steps.clickJSButton(checkout.Creditcard, driver, name);
				System.out.println("Credit card option is available");
				test.log(Status.INFO, MarkupHelper.createLabel("Payment method:  "+Credit+    "      Available", ExtentColor.BLUE));
			}
			catch(Exception e)
			{
				System.out.println("Credit card option is not available");
				test.log(Status.INFO, MarkupHelper.createLabel("Credit card option is not available", ExtentColor.BLUE));
			}
			Thread.sleep(2000);
			// Net Banking option
			try
			{
				String NetBanking = driver.findElement(checkout.Netbanking).getText();
				System.out.println("Payment method:  "+NetBanking);
				steps.Verifytext(checkout.Netbanking, driver, NetBanking, name);
				steps.clickJSButton(checkout.Netbanking, driver, name);
				System.out.println("Net Banking option is available");
				test.log(Status.INFO, MarkupHelper.createLabel("Payment method:  "+NetBanking+    "      Available", ExtentColor.BLUE));
			}
			catch(Exception e)
			{
				System.out.println("Net Banking option is not available");
				test.log(Status.INFO, MarkupHelper.createLabel("Net Banking option is not available", ExtentColor.BLUE));
			}
			Thread.sleep(2000);
			// Paytm option
			try
			{
				String Paytm = driver.findElement(checkout.Paytm).getText();
				System.out.println("Payment method:  "+Paytm);
				steps.Verifytext(checkout.Paytm, driver, Paytm, name);
				steps.clickJSButton(checkout.Paytm, driver, name);
				System.out.println("Paytm option is available");
				test.log(Status.INFO, MarkupHelper.createLabel("Payment method:  "+Paytm+    "      Available", ExtentColor.BLUE));
			}
			catch(Exception e)
			{
				System.out.println("Paytm option is not available");
				test.log(Status.INFO, MarkupHelper.createLabel("Paytm option is not available", ExtentColor.BLUE));
			}
			Thread.sleep(2000);
			// MDS Wallet option
			try
			{
				String MDS = driver.findElement(checkout.MDSWallet).getText();
				System.out.println("Payment method:  "+MDS);
				steps.Verifytext(checkout.MDSWallet, driver, MDS, name);
				steps.clickJSButton(checkout.MDSWallet, driver, name);
				Thread.sleep(2000);
				String MDSpopup = driver.findElement(checkout.MDSWalletPOPup).getText();
				System.out.println("Payment method:  "+MDSpopup);
				Thread.sleep(2000);
				steps.clickJSButton(checkout.MDSWalletPOPupOK, driver, name);
				System.out.println("MDS Wallet option is available");
				test.log(Status.INFO, MarkupHelper.createLabel("Payment method:  "+MDS+    "       Available", ExtentColor.BLUE));
			}
			catch(Exception e)
			{
				System.out.println("MDS Wallet option is not available");
				test.log(Status.INFO, MarkupHelper.createLabel("MDS Wallet option is not available", ExtentColor.BLUE));
			}
			Thread.sleep(2000);
			// Cash on delivery 
			try
			{
				String cash = driver.findElement(checkout.CashonDeliveryText).getText();
				System.out.println("Payment method:  "+cash);
				test.log(Status.INFO, MarkupHelper.createLabel("Payment method:  "+cash+    "        Available", ExtentColor.BLUE));
				steps.Verifytext(checkout.CashonDeliveryText, driver, cash, name);
				steps.clickJSButton(checkout.CashonDeliveryradio, driver, name);
				Thread.sleep(2000);
				String buynow2 = driver.findElement(checkout.PopUpSaveMsg).getText();
				System.out.println("Displayed message:   "+buynow2);
				test.log(Status.INFO, MarkupHelper.createLabel("Displayed message:   "+buynow2, ExtentColor.BLUE));
				Thread.sleep(1000);
				String buynow3 = getscreen.capture(driver, "buynow3");
				test.addScreenCaptureFromPath(buynow3);
				steps.clickJSButton(checkout.Cashondelivery, driver, name);
				test.log(Status.INFO, MarkupHelper.createLabel("User clicked on Cash on Delivery link", ExtentColor.BLUE));
			}
			catch(Exception e)
			{
				System.out.println("Cash on Delivery is not available");
				test.log(Status.INFO, MarkupHelper.createLabel("Cash on Delivery is not available", ExtentColor.BLUE));
			}
			Thread.sleep(2000);
			String checkout1 = getscreen.capture(driver, "checkout1");
			test.addScreenCaptureFromPath(checkout1);
			steps.clickJSButton(checkout.CartIcon, driver, name);
			steps.clickJSButton(home.Trend_Addcart_remove, driver, name);
			Thread.sleep(1000);
			test.log(Status.INFO, MarkupHelper.createLabel("User Completed transactions of 1st Image in Hoodie", ExtentColor.PURPLE));
			Hoodiestatus = passstatus;
			Assert.assertTrue(true);
			System.out.println("Hoodie Successful");
			testresultdata.put(Integer.toString(cnt), new Object[] {name, "Hoodie CheckOut", "Hoodie CheckOut Successful",passstatus});	
		}
		catch(Exception e)
		{
			System.out.println("Hoodie Unsuccessful");
			testresultdata.put(Integer.toString(cnt), new Object[] {name, "Hoodie CheckOut", "Hoodie CheckOut Unsuccessful",failstatus});
			Hoodiestatus = failstatus;
			Hoodiestatusfail = getscreen.capture(driver, "Hoodiestatusfail");
			test.addScreenCaptureFromPath(Hoodiestatusfail);
			Assert.assertTrue(false);
		}
	}
	@BeforeClass
	public void before()
	{
		workbook = new HSSFWorkbook();
		sheet = workbook.createSheet(strDataFileName);
		testresultdata = new LinkedHashMap<String, Object[]>();
		testresultdata.put("1", new Object[] { "Test Step Name", "Expected Result","Actual Result" ,"Status"});
	}
	@AfterClass
	public void after() throws Exception
	{
		Set<String> keyset = testresultdata.keySet();
		int rownum = 0;
		for (String key : keyset) {
			Row row = sheet.createRow(rownum++);
			Object [] objArr = testresultdata.get(key);
			int cellnum = 0;
			for (Object obj : objArr) {
				Cell cell = row.createCell(cellnum++);
				if(obj instanceof Date) 
					cell.setCellValue((Date)obj);
				else if(obj instanceof Boolean)
					cell.setCellValue((Boolean)obj);
				else if(obj instanceof String)
					cell.setCellValue((String)obj);
				else if(obj instanceof Double)
					cell.setCellValue((Double)obj);
			}
		}
		try {
			FileOutputStream out = new FileOutputStream(new File("TestResults/"+strDataFileName +".xls"));
			workbook.write(out);
			out.close();
			System.out.println("Excel written successfully..");
			TestResult();
			driver.quit();
		}
		catch (FileNotFoundException e) 
		{
			e.printStackTrace();
		} 
		catch (IOException e) 
		{
			e.printStackTrace();
		}
	}
	public void TestResult() throws Exception
	{
		HSSFRow row;
		HSSFCell cell;
		WritableFont cellFont;
		WritableCellFormat cellFormat;
		Border border = null;
		int count;
		FileInputStream inp = new FileInputStream( "TestResults/"+strDataFileName +".xls");
		HSSFWorkbook wb = new HSSFWorkbook(inp);
		HSSFSheet sh = wb.getSheetAt(0);
		count=sh.getLastRowNum();
		for (int i = 1; i <= count; i++) 
		{
			row=sh.getRow(i);
			cell=row.getCell(3);
			String status=row.getCell(3).getStringCellValue();
			if(status.equalsIgnoreCase("Pass"))
			{
				HSSFCellStyle pass= wb.createCellStyle();
				pass.setFillForegroundColor(IndexedColors.GREEN.getIndex());
				pass.setFillPattern(CellStyle.SOLID_FOREGROUND);
				cell.setCellStyle(pass);
			}
			else
			{
				HSSFCellStyle fail= wb.createCellStyle();
				fail.setFillForegroundColor(IndexedColors.RED.getIndex());
				fail.setFillPattern(CellStyle.SOLID_FOREGROUND);
				cell.setCellStyle(fail);
			}
		}
		FileOutputStream out = new FileOutputStream("TestResults/"+strDataFileName+".xls");
		wb.write(out);
		wb.close();
	}
}
