package Com.Modules;
import java.io.File;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.IOException;
import java.util.Date;
import java.util.LinkedHashMap;
import java.util.Map;
import java.util.Set;
import org.apache.poi.hssf.usermodel.HSSFCell;
import org.apache.poi.hssf.usermodel.HSSFCellStyle;
import org.apache.poi.hssf.usermodel.HSSFRow;
import org.apache.poi.hssf.usermodel.HSSFSheet;
import org.apache.poi.hssf.usermodel.HSSFWorkbook;
import org.apache.poi.ss.usermodel.Cell;
import org.apache.poi.ss.usermodel.CellStyle;
import org.apache.poi.ss.usermodel.IndexedColors;
import org.apache.poi.ss.usermodel.Row;
import org.openqa.selenium.WebDriver;
import org.testng.Assert;
import org.testng.annotations.AfterClass;
import org.testng.annotations.BeforeClass;
import org.testng.annotations.Test;
import com.aventstack.extentreports.Status;
import com.aventstack.extentreports.markuputils.ExtentColor;
import com.aventstack.extentreports.markuputils.MarkupHelper;
import Com.ObjectRepository.Mugs_OR;
import Com.ObjectRepository.Homepage2_OR;
import Com.ObjectRepository.Homepage_OR;
import Com.ObjectRepository.Login_OR;
import Com.Utilities.Browsers;
import Com.Utilities.CommonMethods;
import Com.Utilities.GetScreenShot;
import Com.Utilities.Screenshot;
import Com.Utilities.Steps;
import Com.Utilities.TestBase;
import jxl.format.Border;
import jxl.write.WritableCellFormat;
import jxl.write.WritableFont;
public class Mugs extends TestBase
{
	WebDriver driver;
	Login_OR login = new Login_OR();
	Steps steps = new Steps();
	GetScreenShot getscreen = new GetScreenShot();
	Screenshot screen = new Screenshot();
	public String strAbsolutepath = new File("").getAbsolutePath();
	HSSFWorkbook workbook;
	HSSFSheet sheet;
	String strDataFileName = this.getClass().getSimpleName();
	Map<String, Object[]> testresultdata; 
	CommonMethods commonmethods = new CommonMethods();
	Homepage_OR home = new Homepage_OR();
	Homepage2_OR home2 = new Homepage2_OR();
	Mugs_OR Mugs = new Mugs_OR();
	Screenshot screenshot = new Screenshot();
	String passstatus = screenshot.status("PASS");
	String failstatus = screenshot.status("FAIL");
	int cnt = 2;
	public static String MugsDescription = "";
	public static String MugsDescription1 = "";
	public static String MugsDescription2 = "";
	public static String MugsDescription3 = "";
	public static String MugsMRP = "";
	public static String Mugscqty = "";
	public static String Mugsiqty = "";
	public static String Mugsdqty = "";
	public static String Mugsaddcart = "";
	public static String Mugsupdateproduct1 = "";
	public static String Mugsremovecart = "";
	public static String Mugsstatus = "";
	public static String Mugsstatusfail = "";
	public static String Mugshomepage = "";
	public static String Mugshomepage1 = "";
	public static String Mugsproduct2 = "";
	public static String Mugsproduct1 = "";
	public Mugs()
	{
		Browsers b = new Browsers();
		this.driver = b.getBrowsers("FF");		 
	}
	@Test
	public void MugsCheckout() throws Exception
	{
		String name = new Object(){}.getClass().getEnclosingMethod().getName();
		cnt ++;
		try 
		{
			test = extent.createTest("Mugs Checkout");
			commonmethods.Login(driver);
			test.log(Status.INFO, MarkupHelper.createLabel("User able to Login", ExtentColor.BLUE));
			Thread.sleep(3000);
			steps.clickButton(home.Mydreamstore, driver, name);
			String homepage = getscreen.capture(driver, name);
			test.addScreenCaptureFromPath(homepage);
			String Parent = driver.getWindowHandle();
			steps.MoveElement(Mugs.Mugslink, driver, name);
			steps.clickJSButton(Mugs.Mugslink, driver, name);
			Mugshomepage = getscreen.capture(driver, "Mugshomepage");
			test.addScreenCaptureFromPath(Mugshomepage);
			test.log(Status.INFO, MarkupHelper.createLabel("User clicked on Mugs link", ExtentColor.BLUE));
			Thread.sleep(2000);
			//steps.WaitUntilElementPresent(Polos.Polos_Image1, driver);
			steps.WaitElementPresent(Mugs.Mugs_Image1, driver);
			steps.clickJSButton(Mugs.Mugs_Image1, driver, name);
			Mugshomepage1 = getscreen.capture(driver, "Mugshomepage1");
			test.addScreenCaptureFromPath(Mugshomepage1);
			test.log(Status.INFO, MarkupHelper.createLabel("User clicked on First Image in Mugs", ExtentColor.BLUE));
			Thread.sleep(1000);
			try
			{
				steps.clickButton(Mugs.Readmore, driver, name);
				MugsDescription1 = driver.findElement(Mugs.Mugs_Description1).getText();
				MugsDescription2 = driver.findElement(Mugs.Mugs_Description2).getText();
				steps.clickButton(Mugs.Readless, driver, name);
				test.log(Status.INFO, MarkupHelper.createLabel("Product Description:  "+ MugsDescription1+MugsDescription2, ExtentColor.BLUE));
			}
			catch(Exception e)
			{
				steps.clickButton(Mugs.Readmore, driver, name);
				MugsDescription3 = driver.findElement(Mugs.Mugs_Description3).getText();
				steps.clickButton(Mugs.Readless, driver, name);
				test.log(Status.INFO, MarkupHelper.createLabel("Product Description:  "+ MugsDescription3, ExtentColor.BLUE));
			}
			MugsMRP = driver.findElement(Mugs.Mugs_MRP).getText();
			test.log(Status.INFO, MarkupHelper.createLabel("Product Price:  "+MugsMRP, ExtentColor.BLUE));
			Thread.sleep(2000);
			Mugscqty = driver.findElement(home.Qty).getAttribute("value");
			System.out.println("Current Quantity:  "+ Mugscqty);
			test.log(Status.INFO, MarkupHelper.createLabel("Current Quantity of product:  "+Mugscqty, ExtentColor.BLUE));
			Thread.sleep(1000);
			steps.clickButton(home.Trend_QtyIncrease, driver, name);
			Mugsiqty = driver.findElement(home.Qty).getAttribute("value");
			System.out.println("Quantity after increase:  "+ Mugsiqty);
			test.log(Status.INFO, MarkupHelper.createLabel("Current Quantity of product after Increase:  "+Mugsiqty, ExtentColor.BLUE));
			steps.clickButton(home.Trend_QtyDecrease, driver, name);
			Mugsdqty = driver.findElement(home.Qty).getAttribute("value");
			System.out.println("Quantity after decrease:  "+ Mugsdqty);
			test.log(Status.INFO, MarkupHelper.createLabel("Current Quantity of product after increase:  "+Mugsdqty, ExtentColor.BLUE));
			steps.clickButton(home.Trend_Addcart, driver, name);
			Thread.sleep(1000);
			test.log(Status.INFO, MarkupHelper.createLabel("User added product to Cart", ExtentColor.BLUE));
			Mugsaddcart = getscreen.capture(driver, "Mugsaddcart");
			test.addScreenCaptureFromPath(Mugsaddcart);
			Thread.sleep(1000);
			steps.clickButton(home2.Addcart_edit, driver, name);
			test.log(Status.INFO, MarkupHelper.createLabel("User clicked on Edit", ExtentColor.BLUE));
			//steps.selectDropdown(home2.Addcart_selectsize, driver, strDataFileName, "SelectSize", name);
			steps.selectDropdown(home2.Addcart_selectqty, driver, strDataFileName, "Quantity", name);
			test.log(Status.INFO, MarkupHelper.createLabel("User selected Size and Quantity", ExtentColor.BLUE));
			steps.clickButton(home2.Addcart_cancel, driver, name);
			steps.clickButton(home2.Addcart_edit, driver, name);
			//steps.selectDropdown(home2.Addcart_selectsize, driver, strDataFileName, "SelectSize", name);
			steps.selectDropdown(home2.Addcart_selectqty, driver, strDataFileName, "Quantity", name);
			steps.clickButton(home2.Addcart_save, driver, name);
			Mugsupdateproduct1 = getscreen.capture(driver, "Mugsupdateproduct1");
			test.addScreenCaptureFromPath(Mugsupdateproduct1);
			test.log(Status.INFO, MarkupHelper.createLabel("User updated product size and quantity", ExtentColor.BLUE));
			Thread.sleep(2000);
			steps.clickButton(home2.Biker_remove, driver, name);
			Mugsremovecart = getscreen.capture(driver, "Mugsremovecart");
			test.addScreenCaptureFromPath(Mugsremovecart);
			test.log(Status.INFO, MarkupHelper.createLabel("User removed product from Cart", ExtentColor.BLUE));
			test.log(Status.INFO, MarkupHelper.createLabel("User Completed transactions of 1st Image in Mugs", ExtentColor.PURPLE));
			Mugsstatus = passstatus;
			Assert.assertTrue(true);
			System.out.println("Mugs Successful");
			testresultdata.put(Integer.toString(cnt), new Object[] {name, "Mugs Checkout", "Mugs Checkout Successful",passstatus});	
		}
		catch(Exception e)
		{
			System.out.println("Mugs Checkout Unsuccessful");
			testresultdata.put(Integer.toString(cnt), new Object[] {name, "Mugs Checkout", "Mugs Checkout Unsuccessful",failstatus});
			Mugsstatus = failstatus;
			Mugsstatusfail = getscreen.capture(driver, "Mugsstatusfail");
			test.addScreenCaptureFromPath(Mugsstatusfail);
			Assert.assertTrue(false);
		}
	}
	@BeforeClass
	public void before()
	{
		workbook = new HSSFWorkbook();
		sheet = workbook.createSheet(strDataFileName);
		testresultdata = new LinkedHashMap<String, Object[]>();
		testresultdata.put("1", new Object[] { "Test Step Name", "Expected Result","Actual Result" ,"Status"});
	}
	@AfterClass
	public void after() throws Exception
	{
		Set<String> keyset = testresultdata.keySet();
		int rownum = 0;
		for (String key : keyset) {
			Row row = sheet.createRow(rownum++);
			Object [] objArr = testresultdata.get(key);
			int cellnum = 0;
			for (Object obj : objArr) {
				Cell cell = row.createCell(cellnum++);
				if(obj instanceof Date) 
					cell.setCellValue((Date)obj);
				else if(obj instanceof Boolean)
					cell.setCellValue((Boolean)obj);
				else if(obj instanceof String)
					cell.setCellValue((String)obj);
				else if(obj instanceof Double)
					cell.setCellValue((Double)obj);
			}
		}
		try {
			FileOutputStream out = new FileOutputStream(new File("TestResults/"+strDataFileName +".xls"));
			workbook.write(out);
			out.close();
			System.out.println("Excel written successfully..");
			TestResult();
			driver.quit();
		}
		catch (FileNotFoundException e) 
		{
			e.printStackTrace();
		} 
		catch (IOException e) 
		{
			e.printStackTrace();
		}
	}
	public void TestResult() throws Exception
	{
		HSSFRow row;
		HSSFCell cell;
		WritableFont cellFont;
		WritableCellFormat cellFormat;
		Border border = null;
		int count;
		FileInputStream inp = new FileInputStream( "TestResults/"+strDataFileName +".xls");
		HSSFWorkbook wb = new HSSFWorkbook(inp);
		HSSFSheet sh = wb.getSheetAt(0);
		count=sh.getLastRowNum();
		for (int i = 1; i <= count; i++) 
		{
			row=sh.getRow(i);
			cell=row.getCell(3);
			String status=row.getCell(3).getStringCellValue();
			if(status.equalsIgnoreCase("Pass"))
			{
				HSSFCellStyle pass= wb.createCellStyle();
				pass.setFillForegroundColor(IndexedColors.GREEN.getIndex());
				pass.setFillPattern(CellStyle.SOLID_FOREGROUND);
				cell.setCellStyle(pass);
			}
			else
			{
				HSSFCellStyle fail= wb.createCellStyle();
				fail.setFillForegroundColor(IndexedColors.RED.getIndex());
				fail.setFillPattern(CellStyle.SOLID_FOREGROUND);
				cell.setCellStyle(fail);
			}
		}
		FileOutputStream out = new FileOutputStream("TestResults/"+strDataFileName+".xls");
		wb.write(out);
		wb.close();
	}
}
