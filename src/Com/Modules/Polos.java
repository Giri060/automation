package Com.Modules;
import java.io.File;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.IOException;
import java.util.LinkedHashMap;
import java.util.List;
import java.util.Map;
import java.util.Set;
import java.util.Date;
import java.util.concurrent.TimeUnit;
import org.apache.poi.hssf.usermodel.HSSFCell;
import org.apache.poi.hssf.usermodel.HSSFCellStyle;
import org.apache.poi.hssf.usermodel.HSSFRow;
import org.apache.poi.hssf.usermodel.HSSFSheet;
import org.apache.poi.hssf.usermodel.HSSFWorkbook;
import org.apache.poi.ss.usermodel.Row;
import org.apache.poi.ss.usermodel.Cell;
import org.apache.poi.ss.usermodel.CellStyle;
import org.apache.poi.ss.usermodel.IndexedColors;
import org.openqa.selenium.By;
import org.openqa.selenium.Keys;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.interactions.Actions;
import org.openqa.selenium.support.Color;
import org.openqa.selenium.support.ui.ExpectedConditions;
import org.openqa.selenium.support.ui.WebDriverWait;
import org.testng.Assert;
import org.testng.annotations.AfterClass;
import org.testng.annotations.BeforeClass;
import org.testng.annotations.Test;
import com.aventstack.extentreports.Status;
import com.aventstack.extentreports.markuputils.ExtentColor;
import com.aventstack.extentreports.markuputils.MarkupHelper;
import Com.ObjectRepository.Checkout_OR;
import Com.ObjectRepository.Homepage2_OR;
import Com.ObjectRepository.Homepage_OR;
import Com.ObjectRepository.Login_OR;
import Com.ObjectRepository.Polos_OR;
import Com.ObjectRepository.Tshirts_OR;
import Com.Utilities.Browsers;
import Com.Utilities.CommonMethods;
import Com.Utilities.GetScreenShot;
import Com.Utilities.Screenshot;
import Com.Utilities.Steps;
import Com.Utilities.TestBase;
import jxl.format.Border;
import jxl.format.UnderlineStyle;
import jxl.write.Colour;
import jxl.write.WritableCellFormat;
import jxl.write.WritableFont;
import jxl.write.WriteException;
public class Polos extends TestBase
{
	WebDriver driver;
	Login_OR login = new Login_OR();
	Steps steps = new Steps();
	GetScreenShot getscreen = new GetScreenShot();
	Screenshot screen = new Screenshot();
	public String strAbsolutepath = new File("").getAbsolutePath();
	HSSFWorkbook workbook;
	HSSFSheet sheet;
	String strDataFileName = this.getClass().getSimpleName();
	Map<String, Object[]> testresultdata; 
	CommonMethods commonmethods = new CommonMethods();
	Homepage_OR home = new Homepage_OR();
	Homepage2_OR home2 = new Homepage2_OR();
	Polos_OR polos = new Polos_OR();
	Tshirts_OR Tshirts = new Tshirts_OR();
	Screenshot screenshot = new Screenshot();
	Checkout_OR checkout = new Checkout_OR();
	String passstatus = screenshot.status("PASS");
	String failstatus = screenshot.status("FAIL");
	int cnt = 2;
	public static String Polosstatus = "";
	public static String Polosstatusfail = "";
	public static String PolosDescription1 = "";
	public static String PolosDescription2 = "";
	public static String PolosDescription3 = "";
	public static String PolosDescription4 = "";
	public static String PolosMRP = "";
	public static String PolosColor1 = "";
	public static String Poloshex1 = "";
	public static String PolosFirstcolor1 = "";
	public static String PolosColor2 = "";
	public static String Poloshex2 = "";
	public static String PolosSecondcolor1 = "";
	public static String PolosColor3 = "";
	public static String Poloshex3 = "";
	public static String PolosThirdcolor1 = "";
	public static String PolosColor4 = "";
	public static String Poloshex4 = "";
	public static String PolosFourthcolor1 = "";
	public static String Poloscqty = "";
	public static String Polosiqty = "";
	public static String Polosdqty = "";
	public static String PolosSizemsg = "";
	public static String PolossizeS = "";
	public static String PolossizeM = "";
	public static String PolossizeL = "";
	public static String PolossizeXL = "";
	public static String PolossizeXXL = "";
	public static String PolossizeXXXL = "";
	public static String Polosbelow510 = "";
	public static String Polosabove510 = "";
	public static String Polostightfit = "";
	public static String Poloscomfit = "";
	public static String Polosfindursize = "";
	public static String Polosaddcart = "";
	public static String Polosupdateproduct1 = "";
	public static String Polosremovecart = "";
	public Polos()
	{
		Browsers b = new Browsers();
		this.driver = b.getBrowsers("chrome1");		 
	}
	@Test
	public void PolosCheckout() throws Exception
	{
		String name = new Object(){}.getClass().getEnclosingMethod().getName();
		cnt ++;
		try 
		{
			test = extent.createTest("Polos");
			commonmethods.Login(driver);
			test.log(Status.INFO, MarkupHelper.createLabel("User able to Login", ExtentColor.BLUE));
			Thread.sleep(3000);
			steps.clickJSButton(home.Mydreamstore, driver, name);
			String homepage = getscreen.capture(driver, name);
			test.addScreenCaptureFromPath(homepage);
			String Parent = driver.getWindowHandle();
			steps.MoveElement(polos.Poloslink, driver, name);
			String Polo = driver.findElement(polos.Poloslink).getText();
			System.out.println(Polo);
			steps.clickJSButton(polos.Poloslink, driver, name);
			test.log(Status.INFO, MarkupHelper.createLabel("User clicked on Polos link", ExtentColor.BLUE));
			Thread.sleep(2000);
			//steps.WaitUntilElementPresent(Polos.Polos_Image1, driver);
			steps.WaitElementPresent(polos.Polos_Image1, driver);
			steps.clickJSButton(polos.Polos_Image1, driver, name);
			test.log(Status.INFO, MarkupHelper.createLabel("User clicked on First Image in Polos", ExtentColor.BLUE));
			try
			{
				steps.clickJSButton(Tshirts.Readmore, driver, name);
				PolosDescription1 = driver.findElement(polos.Polos_Description).getText();
				System.out.println(PolosDescription1);
				steps.clickJSButton(Tshirts.Readless, driver, name);
				test.log(Status.INFO, MarkupHelper.createLabel("Product Description:  "+ PolosDescription1, ExtentColor.BLUE));
			}
			catch(Exception e)
			{
				System.out.println("Description hasn't fetched");
			}
			Thread.sleep(1000);
			PolosMRP = driver.findElement(Tshirts.Tshirts_MRP).getText();
			test.log(Status.INFO, MarkupHelper.createLabel("Product Price:  "+PolosMRP, ExtentColor.BLUE));
			Thread.sleep(2000);
			try
			{
				String col1 = driver.findElement(home2.color1).getCssValue("color");
				System.out.println(col1);
				steps.clickJSButton(home2.color1, driver, name);
				Thread.sleep(2000);
				PolosColor1 = driver.findElement(home2.color1).getCssValue("background-color");
				Poloshex1 = Color.fromString(PolosColor1).asHex();
				System.out.println("Image color: "+Poloshex1);
				test.log(Status.INFO, MarkupHelper.createLabel("User changed item color to:  "+Poloshex1, ExtentColor.BLUE));
				PolosFirstcolor1 = getscreen.capture(driver, "PolosFirstcolor1");
				test.addScreenCaptureFromPath(PolosFirstcolor1);
			}
			catch(Exception e)
			{
				System.out.println("Color 1 is not exists");
			}
			try
			{
				String col2 = driver.findElement(home2.color2).getCssValue("color");
				System.out.println(col2);
				steps.clickJSButton(home2.color2, driver, name);
				Thread.sleep(1000);
				PolosColor2 = driver.findElement(home2.color2).getCssValue("background-color");
				Poloshex2 = Color.fromString(PolosColor2).asHex();
				System.out.println("Image color: "+Poloshex2);
				test.log(Status.INFO, MarkupHelper.createLabel("User changed item color to:  "+Poloshex2, ExtentColor.BLUE));
				PolosSecondcolor1 = getscreen.capture(driver, "PolosSecondcolor1");
				test.addScreenCaptureFromPath(PolosSecondcolor1);
			}
			catch(Exception e)
			{
				System.out.println("Color 2 is not exists");
			}
			try
			{
				String col3 = driver.findElement(home2.color3).getCssValue("color");
				System.out.println(col3);
				steps.clickJSButton(home2.color3, driver, name);
				Thread.sleep(1000);
				PolosColor3 = driver.findElement(home2.color3).getCssValue("background-color");
				Poloshex3 = Color.fromString(PolosColor3).asHex();
				System.out.println("Image color: "+Poloshex3);
				test.log(Status.INFO, MarkupHelper.createLabel("User changed item color to:  "+Poloshex3, ExtentColor.BLUE));
				PolosThirdcolor1 = getscreen.capture(driver, "PolosThirdcolor1");
				test.addScreenCaptureFromPath(PolosThirdcolor1);
			}
			catch(Exception e)
			{
				System.out.println("Color 3 is not exists");
			}
			try 
			{
				String col4 = driver.findElement(home2.color4).getCssValue("color");
				System.out.println(col4);
				steps.clickJSButton(home2.color4, driver, name);
				Thread.sleep(1000);
				PolosColor4 = driver.findElement(home2.color4).getCssValue("background-color");
				Poloshex4 = Color.fromString(PolosColor4).asHex();
				System.out.println("Image color: "+Poloshex4);
				test.log(Status.INFO, MarkupHelper.createLabel("User changed item color to:  "+Poloshex4, ExtentColor.BLUE));
				PolosFourthcolor1 = getscreen.capture(driver, "PolosFourthcolor1");
				test.addScreenCaptureFromPath(PolosFourthcolor1);
			}
			catch(Exception e)
			{
				System.out.println("Color 4 is not exists");
			}
			Thread.sleep(1000);
			Poloscqty = driver.findElement(home.Qty).getAttribute("value");
			System.out.println("Current Quantity:  "+ Poloscqty);
			test.log(Status.INFO, MarkupHelper.createLabel("Current Quantity of product:  "+Poloscqty, ExtentColor.BLUE));
			Thread.sleep(1000);
			steps.clickJSButton(home.Trend_QtyIncrease, driver, name);
			Polosiqty = driver.findElement(home.Qty).getAttribute("value");
			System.out.println("Quantity after increase:  "+ Polosiqty);
			test.log(Status.INFO, MarkupHelper.createLabel("Current Quantity of product after Increase:  "+Polosiqty, ExtentColor.BLUE));
			steps.clickJSButton(home.Trend_QtyDecrease, driver, name);
			Polosdqty = driver.findElement(home.Qty).getAttribute("value");
			System.out.println("Quantity after decrease:  "+ Polosdqty);
			test.log(Status.INFO, MarkupHelper.createLabel("Current Quantity of product after increase:  "+Polosdqty, ExtentColor.BLUE));
			steps.clickJSButton(home.Trend_Addcart, driver, name);
			PolosSizemsg = driver.findElement(home.Trend_Sizeerrormsg).getText();
			System.out.println("User clicked on Addcart before Size selection:  "+PolosSizemsg);
			test.log(Status.INFO, MarkupHelper.createLabel("User clicked on Addcart before Size selection:  "+PolosSizemsg, ExtentColor.RED));
			//steps.clickJSButton(home2.Polos_sizeS, driver, name);
			steps.clickJSButton(home2.Size_S, driver, name);
			PolossizeS = driver.findElement(home2.Size_S).getText();
			System.out.println("Selected Size:  "+PolossizeS);
			test.log(Status.INFO, MarkupHelper.createLabel("User selected size:  "+PolossizeS, ExtentColor.BLUE));
			steps.clickJSButton(home2.Size_M, driver, name);
			PolossizeM = driver.findElement(home2.Size_M).getText();
			System.out.println("Selected Size:  "+PolossizeM);
			test.log(Status.INFO, MarkupHelper.createLabel("User selected size:  "+PolossizeM, ExtentColor.BLUE));
			steps.clickJSButton(home2.Size_L, driver, name);
			PolossizeL = driver.findElement(home2.Size_L).getText();
			System.out.println("Selected Size:  "+PolossizeL);
			test.log(Status.INFO, MarkupHelper.createLabel("User selected size:  "+PolossizeL, ExtentColor.BLUE));
			steps.clickJSButton(home2.Size_XL, driver, name);
			PolossizeXL = driver.findElement(home2.Size_XL).getText();
			System.out.println("Selected Size:  "+PolossizeXL);
			test.log(Status.INFO, MarkupHelper.createLabel("User selected size:  "+PolossizeXL, ExtentColor.BLUE));
			steps.clickJSButton(home2.Size_XXL, driver, name);
			PolossizeXXL = driver.findElement(home2.Size_XXL).getText();
			System.out.println("Selected Size:  "+PolossizeXXL);
			test.log(Status.INFO, MarkupHelper.createLabel("User selected size:  "+PolossizeXXL, ExtentColor.BLUE));
			steps.clickJSButton(home2.Size_XXXL, driver, name);
			PolossizeXXXL = driver.findElement(home2.Size_XXXL).getText();
			System.out.println("Selected Size:  "+PolossizeXXXL);
			test.log(Status.INFO, MarkupHelper.createLabel("User selected size:  "+PolossizeXXXL, ExtentColor.BLUE));
			steps.clickJSButton(home.Trend_FindUrSize, driver, name);
			test.log(Status.INFO, MarkupHelper.createLabel("User clicked on Find your size", ExtentColor.BLUE));
			Thread.sleep(1000);
			try
			{
				Polosbelow510 = driver.findElement(home.Trend_FindUrSize_510radio).getText();
				System.out.println("User Selected:  "+Polosbelow510);
				steps.clickJSButton(home.Trend_FindUrSize_510radio, driver, name);
				Polosbelow510 = driver.findElement(home.Trend_FindUrSize_510radio).getText();
				System.out.println("User Selected:  "+Polosbelow510);
				test.log(Status.INFO, MarkupHelper.createLabel("User selected Height:  "+Polosbelow510, ExtentColor.BLUE));
				steps.clickJSButton(home.Trend_FindUrSize_above510radio, driver, name);
				Polosabove510 = driver.findElement(home.Trend_FindUrSize_above510radio).getText();
				System.out.println("User Selected:  "+Polosabove510);
				test.log(Status.INFO, MarkupHelper.createLabel("User selected Height:  "+Polosabove510, ExtentColor.BLUE));
				steps.clickJSButton(home.Trend_FindUrSize_Tightradio, driver, name);
				Polostightfit = driver.findElement(home.Trend_FindUrSize_Tightradio).getText();
				System.out.println("User Selected:  "+Polostightfit);
				test.log(Status.INFO, MarkupHelper.createLabel("User selected Fit:  "+Polostightfit, ExtentColor.BLUE));
				steps.clickJSButton(home.Trend_FindUrSize_Comfortradio, driver, name);
				Poloscomfit = driver.findElement(home.Trend_FindUrSize_Comfortradio).getText();
				System.out.println("User Selected:  "+Poloscomfit);
				test.log(Status.INFO, MarkupHelper.createLabel("User selected Fit:  "+Poloscomfit, ExtentColor.BLUE));
				Thread.sleep(1000);
				steps.selectDropdown(home.Trend_FindUrSize_sizeDD, driver, strDataFileName, "size", name);
				test.log(Status.INFO, MarkupHelper.createLabel("User selected shirt size", ExtentColor.BLUE));
				steps.clickJSButton(home.Trend_FindUrSize_submit, driver, name);
				test.log(Status.INFO, MarkupHelper.createLabel("User clicked on Submit", ExtentColor.BLUE));
				Polosfindursize = getscreen.capture(driver, "Polosfindursize");
				test.addScreenCaptureFromPath(Polosfindursize);
				Thread.sleep(1000);
				steps.clickJSButton(home.Trend_FindUrSize_Okbutton, driver, name);
				test.log(Status.INFO, MarkupHelper.createLabel("system displayed size for given metrics", ExtentColor.BLUE));
				steps.clickJSButton(home.Trend_FindUrSize_closebutton, driver, name);
				test.log(Status.INFO, MarkupHelper.createLabel("User navigate to Product details page", ExtentColor.BLUE));
			}
			catch(Exception e)
			{
				steps.clickJSButton(home.Trend_FindUrSize_closebutton, driver, name);
				test.log(Status.INFO, MarkupHelper.createLabel("User navigate to Product details page", ExtentColor.BLUE));
			}
			Thread.sleep(1000);		
			steps.clickJSButton(home2.Size_L, driver, name);
			test.log(Status.INFO, MarkupHelper.createLabel("User selected size", ExtentColor.BLUE));
			steps.clickJSButton(home.Trend_Addcart, driver, name);
			Thread.sleep(1000);
			test.log(Status.INFO, MarkupHelper.createLabel("User added product to Cart", ExtentColor.BLUE));
			Polosaddcart = getscreen.capture(driver, "Polosaddcart");
			test.addScreenCaptureFromPath(Polosaddcart);
			Thread.sleep(1000);
			try
			{
				String Edit = driver.findElement(home2.Addcart_edit).getText();
				System.out.println(Edit);
				steps.clickJSButton(home2.Addcart_edit, driver, name);
				test.log(Status.INFO, MarkupHelper.createLabel("User clicked on Edit", ExtentColor.BLUE));
				steps.selectDropdown(home2.Addcart_selectsize, driver, strDataFileName, "SelectSize", name);
				steps.selectDropdown(home2.Addcart_selectqty, driver, strDataFileName, "Quantity", name);
				test.log(Status.INFO, MarkupHelper.createLabel("User selected Size and Quantity", ExtentColor.BLUE));
				steps.clickJSButton(home2.Addcart_cancel, driver, name);
				steps.clickJSButton(home2.Addcart_edit, driver, name);
				steps.selectDropdown(home2.Addcart_selectsize, driver, strDataFileName, "SelectSize", name);
				steps.selectDropdown(home2.Addcart_selectqty, driver, strDataFileName, "Quantity", name);
				steps.clickJSButton(home2.Addcart_save, driver, name);
				Polosupdateproduct1 = getscreen.capture(driver, "Polosupdateproduct1");
				test.addScreenCaptureFromPath(Polosupdateproduct1);
				test.log(Status.INFO, MarkupHelper.createLabel("User updated product size and quantity", ExtentColor.BLUE));
			}
			catch(Exception e)
			{
				System.out.println("Edit feature not available");
			}
			Thread.sleep(1000);
			steps.clickJSButton(home2.Biker_remove, driver, name);
			Polosremovecart = getscreen.capture(driver, "Polosremovecart");
			test.addScreenCaptureFromPath(Polosremovecart);
			test.log(Status.INFO, MarkupHelper.createLabel("User removed product from Cart", ExtentColor.BLUE));
			Thread.sleep(2000);
			//checkout process
			steps.clickJSButton(home.Size_L, driver, name);
			test.log(Status.INFO, MarkupHelper.createLabel("User selected size", ExtentColor.BLUE));
			steps.clickJSButton(checkout.BuyNow, driver, name);
			test.log(Status.INFO, MarkupHelper.createLabel("User clicked on BuyNow, navigated to checkout page", ExtentColor.BLUE));
			String buynow1 = getscreen.capture(driver, "buynow1");
			test.addScreenCaptureFromPath(buynow1);
			Thread.sleep(2000);
			// Debit card option
			try
			{
				String Debit = driver.findElement(checkout.Debitcard).getText();
				System.out.println("Payment method:  "+Debit);
				steps.Verifytext(checkout.Debitcard, driver, Debit, name);
				steps.clickJSButton(checkout.Debitcard, driver, name);
				System.out.println("Debit card option is available");
				test.log(Status.INFO, MarkupHelper.createLabel("Payment method:  "+Debit   +    "      Available", ExtentColor.BLUE));
			}
			catch(Exception e)
			{
				System.out.println("Debit card option is not available");
				test.log(Status.INFO, MarkupHelper.createLabel("Debit card option is not available", ExtentColor.BLUE));
			}
			Thread.sleep(2000);
			// Credit card option
			try
			{
				String Credit = driver.findElement(checkout.Creditcard).getText();
				System.out.println("Payment method:  "+Credit);
				steps.Verifytext(checkout.Creditcard, driver, Credit, name);
				steps.clickJSButton(checkout.Creditcard, driver, name);
				System.out.println("Credit card option is available");
				test.log(Status.INFO, MarkupHelper.createLabel("Payment method:  "+Credit+    "      Available", ExtentColor.BLUE));
			}
			catch(Exception e)
			{
				System.out.println("Credit card option is not available");
				test.log(Status.INFO, MarkupHelper.createLabel("Credit card option is not available", ExtentColor.BLUE));
			}
			Thread.sleep(2000);
			// Net Banking option
			try
			{
				String NetBanking = driver.findElement(checkout.Netbanking).getText();
				System.out.println("Payment method:  "+NetBanking);
				steps.Verifytext(checkout.Netbanking, driver, NetBanking, name);
				steps.clickJSButton(checkout.Netbanking, driver, name);
				System.out.println("Net Banking option is available");
				test.log(Status.INFO, MarkupHelper.createLabel("Payment method:  "+NetBanking+    "      Available", ExtentColor.BLUE));
			}
			catch(Exception e)
			{
				System.out.println("Net Banking option is not available");
				test.log(Status.INFO, MarkupHelper.createLabel("Net Banking option is not available", ExtentColor.BLUE));
			}
			Thread.sleep(2000);
			// Paytm option
			try
			{
				String Paytm = driver.findElement(checkout.Paytm).getText();
				System.out.println("Payment method:  "+Paytm);
				steps.Verifytext(checkout.Paytm, driver, Paytm, name);
				steps.clickJSButton(checkout.Paytm, driver, name);
				System.out.println("Paytm option is available");
				test.log(Status.INFO, MarkupHelper.createLabel("Payment method:  "+Paytm+    "      Available", ExtentColor.BLUE));
			}
			catch(Exception e)
			{
				System.out.println("Paytm option is not available");
				test.log(Status.INFO, MarkupHelper.createLabel("Paytm option is not available", ExtentColor.BLUE));
			}
			Thread.sleep(2000);
			// MDS Wallet option
			try
			{
				String MDS = driver.findElement(checkout.MDSWallet).getText();
				System.out.println("Payment method:  "+MDS);
				steps.Verifytext(checkout.MDSWallet, driver, MDS, name);
				steps.clickJSButton(checkout.MDSWallet, driver, name);
				Thread.sleep(2000);
				String MDSpopup = driver.findElement(checkout.MDSWalletPOPup).getText();
				System.out.println("Payment method:  "+MDSpopup);
				Thread.sleep(2000);
				steps.clickJSButton(checkout.MDSWalletPOPupOK, driver, name);
				System.out.println("MDS Wallet option is available");
				test.log(Status.INFO, MarkupHelper.createLabel("Payment method:  "+MDS+    "       Available", ExtentColor.BLUE));
			}
			catch(Exception e)
			{
				System.out.println("MDS Wallet option is not available");
				test.log(Status.INFO, MarkupHelper.createLabel("MDS Wallet option is not available", ExtentColor.BLUE));
			}
			Thread.sleep(2000);
			// Cash on delivery 
			try
			{
				String cash = driver.findElement(checkout.CashonDeliveryText).getText();
				System.out.println("Payment method:  "+cash);
				test.log(Status.INFO, MarkupHelper.createLabel("Payment method:  "+cash+    "        Available", ExtentColor.BLUE));
				steps.Verifytext(checkout.CashonDeliveryText, driver, cash, name);
				steps.clickJSButton(checkout.CashonDeliveryradio, driver, name);
				Thread.sleep(2000);
				String buynow2 = driver.findElement(checkout.PopUpSaveMsg).getText();
				System.out.println("Displayed message:   "+buynow2);
				test.log(Status.INFO, MarkupHelper.createLabel("Displayed message:   "+buynow2, ExtentColor.BLUE));
				Thread.sleep(1000);
				String buynow3 = getscreen.capture(driver, "buynow3");
				test.addScreenCaptureFromPath(buynow3);
				steps.clickJSButton(checkout.Cashondelivery, driver, name);
				test.log(Status.INFO, MarkupHelper.createLabel("User clicked on Cash on Delivery link", ExtentColor.BLUE));
			}
			catch(Exception e)
			{
				System.out.println("Cash on Delivery is not available");
				test.log(Status.INFO, MarkupHelper.createLabel("Cash on Delivery is not available", ExtentColor.BLUE));
			}
			Thread.sleep(2000);
			String checkout1 = getscreen.capture(driver, "checkout1");
			test.addScreenCaptureFromPath(checkout1);
			steps.clickJSButton(checkout.CartIcon, driver, name);
			Thread.sleep(1000);
			steps.clickJSButton(home.Trend_Addcart_remove, driver, name);
			test.log(Status.INFO, MarkupHelper.createLabel("User Completed transactions of 1st Image in Polos", ExtentColor.PURPLE));
			Polosstatus = passstatus;
			Assert.assertTrue(true);
			System.out.println("Polos Successful");
			testresultdata.put(Integer.toString(cnt), new Object[] {name, "Polos Checkout", "Polos Checkout Successful",passstatus});	
		}
		catch(Exception e)
		{
			System.out.println("Polos Checkout Unsuccessful");
			testresultdata.put(Integer.toString(cnt), new Object[] {name, "Polos Checkout", "Polos Checkout Unsuccessful",failstatus});
			Polosstatus = failstatus;
			Polosstatusfail = getscreen.capture(driver, "Polosstatusfail");
			test.addScreenCaptureFromPath(Polosstatusfail);
			Assert.assertTrue(false);
		}
	}
	@BeforeClass
	public void before()
	{
		workbook = new HSSFWorkbook();
		sheet = workbook.createSheet(strDataFileName);
		testresultdata = new LinkedHashMap<String, Object[]>();
		testresultdata.put("1", new Object[] { "Test Step Name", "Expected Result","Actual Result" ,"Status"});
	}
	@AfterClass
	public void after() throws Exception
	{
		Set<String> keyset = testresultdata.keySet();
		int rownum = 0;
		for (String key : keyset) {
			Row row = sheet.createRow(rownum++);
			Object [] objArr = testresultdata.get(key);
			int cellnum = 0;
			for (Object obj : objArr) {
				Cell cell = row.createCell(cellnum++);
				if(obj instanceof Date) 
					cell.setCellValue((Date)obj);
				else if(obj instanceof Boolean)
					cell.setCellValue((Boolean)obj);
				else if(obj instanceof String)
					cell.setCellValue((String)obj);
				else if(obj instanceof Double)
					cell.setCellValue((Double)obj);
			}
		}
		try {
			FileOutputStream out = new FileOutputStream(new File("TestResults/"+strDataFileName +".xls"));
			workbook.write(out);
			out.close();
			System.out.println("Excel written successfully..");
			TestResult();
			driver.quit();
		}
		catch (FileNotFoundException e) 
		{
			e.printStackTrace();
		} 
		catch (IOException e) 
		{
			e.printStackTrace();
		}
	}
	public void TestResult() throws Exception
	{
		HSSFRow row;
		HSSFCell cell;
		WritableFont cellFont;
		WritableCellFormat cellFormat;
		Border border = null;
		int count;
		FileInputStream inp = new FileInputStream( "TestResults/"+strDataFileName +".xls");
		HSSFWorkbook wb = new HSSFWorkbook(inp);
		HSSFSheet sh = wb.getSheetAt(0);
		count=sh.getLastRowNum();
		for (int i = 1; i <= count; i++) 
		{
			row=sh.getRow(i);
			cell=row.getCell(3);
			String status=row.getCell(3).getStringCellValue();
			if(status.equalsIgnoreCase("Pass"))
			{
				HSSFCellStyle pass= wb.createCellStyle();
				pass.setFillForegroundColor(IndexedColors.GREEN.getIndex());
				pass.setFillPattern(CellStyle.SOLID_FOREGROUND);
				cell.setCellStyle(pass);
			}
			else
			{
				HSSFCellStyle fail= wb.createCellStyle();
				fail.setFillForegroundColor(IndexedColors.RED.getIndex());
				fail.setFillPattern(CellStyle.SOLID_FOREGROUND);
				cell.setCellStyle(fail);
			}
		}
		FileOutputStream out = new FileOutputStream("TestResults/"+strDataFileName+".xls");
		wb.write(out);
		wb.close();
	}
}
