package Com.Modules;
import java.io.File;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.IOException;
import java.util.Date;
import java.util.LinkedHashMap;
import java.util.Map;
import java.util.Set;
import org.apache.poi.hssf.usermodel.HSSFCell;
import org.apache.poi.hssf.usermodel.HSSFCellStyle;
import org.apache.poi.hssf.usermodel.HSSFRow;
import org.apache.poi.hssf.usermodel.HSSFSheet;
import org.apache.poi.hssf.usermodel.HSSFWorkbook;
import org.apache.poi.ss.usermodel.Cell;
import org.apache.poi.ss.usermodel.CellStyle;
import org.apache.poi.ss.usermodel.IndexedColors;
import org.apache.poi.ss.usermodel.Row;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.support.Color;
import org.testng.Assert;
import org.testng.annotations.AfterClass;
import org.testng.annotations.BeforeClass;
import org.testng.annotations.Test;
import com.aventstack.extentreports.Status;
import com.aventstack.extentreports.markuputils.ExtentColor;
import com.aventstack.extentreports.markuputils.MarkupHelper;
import Com.ObjectRepository.Checkout_OR;
import Com.ObjectRepository.WomanTee_OR;
import Com.ObjectRepository.Homepage2_OR;
import Com.ObjectRepository.Homepage_OR;
import Com.ObjectRepository.Login_OR;
import Com.ObjectRepository.Tshirts_OR;
import Com.ObjectRepository.WomanTee_OR;
import Com.Utilities.Browsers;
import Com.Utilities.CommonMethods;
import Com.Utilities.GetScreenShot;
import Com.Utilities.Screenshot;
import Com.Utilities.Steps;
import Com.Utilities.TestBase;
import jxl.format.Border;
import jxl.write.WritableCellFormat;
import jxl.write.WritableFont;
public class WomanTee extends TestBase
{
	WebDriver driver;
	Login_OR login = new Login_OR();
	Steps steps = new Steps();
	GetScreenShot getscreen = new GetScreenShot();
	Screenshot screen = new Screenshot();
	public String strAbsolutepath = new File("").getAbsolutePath();
	HSSFWorkbook workbook;
	HSSFSheet sheet;
	String strDataFileName = this.getClass().getSimpleName();
	Map<String, Object[]> testresultdata; 
	CommonMethods commonmethods = new CommonMethods();
	Homepage_OR home = new Homepage_OR();
	Homepage2_OR home2 = new Homepage2_OR();
	WomanTee_OR WomanTee = new WomanTee_OR();
	Checkout_OR checkout = new Checkout_OR();
	Screenshot screenshot = new Screenshot();
	String passstatus = screenshot.status("PASS");
	String failstatus = screenshot.status("FAIL");
	int cnt = 2;
	public static String WomanTeestatus = "";
	public static String WomanTeestatusfail = "";
	public static String WomanTeeDescription1 = "";
	public static String WomanTeeDescription2 = "";
	public static String WomanTeeDescription3 = "";
	public static String WomanTeeDescription4 = "";
	public static String WomanTeeMRP = "";
	public static String WomanTeeColor12 = "";
	public static String WomanTeehex12 = "";
	public static String WomanTeeFirstcolor2 = "";
	public static String WomanTeeColor22 = "";
	public static String WomanTeehex22 = "";
	public static String WomanTeeSecondcolor2 = "";
	public static String WomanTeeColor32 = "";
	public static String WomanTeehex32 = "";
	public static String WomanTeeThirdcolor2 = "";
	public static String WomanTeeColor42 = "";
	public static String WomanTeehex42 = "";
	public static String WomanTeeFourthcolor2 = "";
	public static String WomanTeeColor13 = "";
	public static String WomanTeehex13 = "";
	public static String WomanTeeFirstcolor3 = "";
	public static String WomanTeeColor23 = "";
	public static String WomanTeehex23 = "";
	public static String WomanTeeSecondcolor3 = "";
	public static String WomanTeeColor33 = "";
	public static String WomanTeehex33 = "";
	public static String WomanTeeThirdcolor3 = "";
	public static String WomanTeeColor43 = "";
	public static String WomanTeehex43 = "";
	public static String WomanTeeFourthcolor3 = "";
	public static String WomanTeeColor11 = "";
	public static String WomanTeehex11 = "";
	public static String WomanTeeFirstcolor1 = "";
	public static String WomanTeeColor21 = "";
	public static String WomanTeehex21 = "";
	public static String WomanTeeSecondcolor1 = "";
	public static String WomanTeeColor31 = "";
	public static String WomanTeehex31 = "";
	public static String WomanTeeThirdcolor1 = "";
	public static String WomanTeeColor41 = "";
	public static String WomanTeehex41 = "";
	public static String WomanTeeFourthcolor1 = "";
	public static String WomanTeecqty = "";
	public static String WomanTeeiqty = "";
	public static String WomanTeedqty = "";
	public static String WomanTeeSizemsg = "";
	public static String WomanTeesizeS = "";
	public static String WomanTeesizeM = "";
	public static String WomanTeesizeL = "";
	public static String WomanTeesizeXL = "";
	public static String WomanTeesizeXXL = "";
	public static String WomanTeesizeXXXL = "";
	public static String WomanTeebelow510 = "";
	public static String WomanTeeabove510 = "";
	public static String WomanTeetightfit = "";
	public static String WomanTeecomfit = "";
	public static String WomanTeefindursize = "";
	public static String WomanTeeaddcart = "";
	public static String WomanTeeupdateproduct1 = "";
	public static String WomanTeeremovecart = "";
	public WomanTee()
	{
		Browsers b = new Browsers();
		this.driver = b.getBrowsers("chrome1");		 
	}
	@Test
	public void WomanTeeCheckout() throws Exception
	{
		String name = new Object(){}.getClass().getEnclosingMethod().getName();
		cnt ++;
		try 
		{
			test = extent.createTest("WomanTee Module");
			System.out.println("WomanTee Module Test Case Executing...");
			commonmethods.Login(driver);
			test.log(Status.INFO, MarkupHelper.createLabel("User able to Login", ExtentColor.BLUE));
			Thread.sleep(3000);
			steps.clickJSButton(home.Mydreamstore, driver, name);
			String homepage = getscreen.capture(driver, name);
			test.addScreenCaptureFromPath(homepage);
			String Parent = driver.getWindowHandle();
			steps.MoveElement(WomanTee.WomanTee, driver, name);
			String WomanTee1 = driver.findElement(WomanTee.WomanTee).getText();
			System.out.println(WomanTee1);
			steps.clickJSButton(WomanTee.WomanTee, driver, name);
			test.log(Status.INFO, MarkupHelper.createLabel("User clicked on WomanTee link", ExtentColor.BLUE));
			Thread.sleep(2000);
			try
			{
				steps.waitForElement(home.Filterpagetext, driver);
				String Filterpage1 = driver.findElement(home.Filterpagetext).getText();
				System.out.println("User navigated to :   "+Filterpage1);
				//steps.WaitUntilElementPresent(WomanTee.WomanTee_Image1, driver);
				steps.WaitElementPresent(WomanTee.WomanTee_Image1, driver);
				steps.clickJSButton(WomanTee.WomanTee_Image1, driver, name);
				test.log(Status.INFO, MarkupHelper.createLabel("User clicked on First Image in T Shirts", ExtentColor.BLUE));
			}
			catch(Exception e )
			{
				System.out.println("User navigated to different landing page");
			}
			Thread.sleep(1000);
			try
			{
				steps.clickJSButton(WomanTee.Readmore, driver, name);
				WomanTeeDescription1 = driver.findElement(WomanTee.WomanTee_Description).getText();
				System.out.println("Product Description:   "+WomanTeeDescription1);
				steps.clickJSButton(WomanTee.Readless, driver, name);
				test.log(Status.INFO, MarkupHelper.createLabel("Product Description:  "+ WomanTeeDescription1, ExtentColor.BLUE));
			}
			catch(Exception e)
			{
				steps.clickJSButton(WomanTee.Readmore, driver, name);
				WomanTeeDescription2 = driver.findElement(WomanTee.WomanTee_Description1).getText();
				System.out.println("Product Description:   "+WomanTeeDescription2);
				steps.clickJSButton(WomanTee.Readless, driver, name);
				test.log(Status.INFO, MarkupHelper.createLabel("Product Description:  "+WomanTeeDescription2, ExtentColor.BLUE));
			}
			Thread.sleep(1000);
			WomanTeeMRP = driver.findElement(WomanTee.WomanTee_MRP).getText();
			test.log(Status.INFO, MarkupHelper.createLabel("Product Price:  "+WomanTeeMRP, ExtentColor.ORANGE));
			Thread.sleep(2000);
			try
			{
				String prod2 = driver.findElement(WomanTee.WomanTee_Product2).getCssValue("color");
				System.out.println("Product:  "+prod2);
				steps.clickJSButton(WomanTee.WomanTee_Product2, driver, name);
				try
				{
					String col1 = driver.findElement(home2.color1).getCssValue("color");
					System.out.println(col1);
					steps.clickJSButton(home2.color1, driver, name);
					Thread.sleep(2000);
					WomanTeeColor12 = driver.findElement(home2.color1).getCssValue("background-color");
					WomanTeehex12 = Color.fromString(WomanTeeColor12).asHex();
					System.out.println("Image color: "+WomanTeehex12);
					test.log(Status.INFO, MarkupHelper.createLabel("User changed item color to:  "+WomanTeehex12, ExtentColor.BLUE));
					WomanTeeFirstcolor2 = getscreen.capture(driver, "WomanTeeFirstcolor2");
					test.addScreenCaptureFromPath(WomanTeeFirstcolor2);
				}
				catch(Exception e)
				{
					System.out.println("Color 1 is not exists");
				}
				try
				{
					String col2 = driver.findElement(home2.color2).getCssValue("color");
					System.out.println(col2);
					steps.clickJSButton(home2.color2, driver, name);
					Thread.sleep(1000);
					WomanTeeColor22 = driver.findElement(home2.color2).getCssValue("background-color");
					WomanTeehex22 = Color.fromString(WomanTeeColor22).asHex();
					System.out.println("Image color: "+WomanTeehex22);
					test.log(Status.INFO, MarkupHelper.createLabel("User changed item color to:  "+WomanTeehex22, ExtentColor.BLUE));
					WomanTeeSecondcolor2 = getscreen.capture(driver, "WomanTeeSecondcolor2");
					test.addScreenCaptureFromPath(WomanTeeSecondcolor2);
				}
				catch(Exception e)
				{
					System.out.println("Color 2 is not exists");
				}
				try
				{
					String col3 = driver.findElement(home2.color3).getCssValue("color");
					System.out.println(col3);
					steps.clickJSButton(home2.color3, driver, name);
					Thread.sleep(1000);
					WomanTeeColor32 = driver.findElement(home2.color3).getCssValue("background-color");
					WomanTeehex32 = Color.fromString(WomanTeeColor32).asHex();
					System.out.println("Image color: "+WomanTeehex32);
					test.log(Status.INFO, MarkupHelper.createLabel("User changed item color to:  "+WomanTeehex32, ExtentColor.BLUE));
					WomanTeeThirdcolor2 = getscreen.capture(driver, "WomanTeeThirdcolor2");
					test.addScreenCaptureFromPath(WomanTeeThirdcolor2);
				}
				catch(Exception e)
				{
					System.out.println("Color 3 is not exists");
				}
				try 
				{
					String col4 = driver.findElement(home2.color4).getCssValue("color");
					System.out.println(col4);
					steps.clickJSButton(home2.color4, driver, name);
					Thread.sleep(1000);
					WomanTeeColor42 = driver.findElement(home2.color4).getCssValue("background-color");
					WomanTeehex42 = Color.fromString(WomanTeeColor42).asHex();
					System.out.println("Image color: "+WomanTeehex42);
					test.log(Status.INFO, MarkupHelper.createLabel("User changed item color to:  "+WomanTeehex42, ExtentColor.BLUE));
					WomanTeeFourthcolor2 = getscreen.capture(driver, "WomanTeeFourthcolor2");
					test.addScreenCaptureFromPath(WomanTeeFourthcolor2);
				}
				catch(Exception e)
				{
					System.out.println("Color 4 is not exists");
				}
			}
			catch(Exception e)
			{
				System.out.println("Product 2 doesn't exists");
			}
			Thread.sleep(2000);
			try
			{
				String prod1 = driver.findElement(WomanTee.WomanTee_Product1).getCssValue("color");
				System.out.println("Product:  "+prod1);
				steps.clickJSButton(WomanTee.WomanTee_Product1, driver, name);
				try
				{
					String col1 = driver.findElement(home2.color1).getCssValue("color");
					System.out.println(col1);
					steps.clickJSButton(home2.color1, driver, name);
					Thread.sleep(2000);
					WomanTeeColor11 = driver.findElement(home2.color1).getCssValue("background-color");
					WomanTeehex11 = Color.fromString(WomanTeeColor11).asHex();
					System.out.println("Image color: "+WomanTeehex11);
					test.log(Status.INFO, MarkupHelper.createLabel("User changed item color to:  "+WomanTeehex11, ExtentColor.BLUE));
					WomanTeeFirstcolor1 = getscreen.capture(driver, "WomanTeeFirstcolor1");
					test.addScreenCaptureFromPath(WomanTeeFirstcolor1);
				}
				catch(Exception e)
				{
					System.out.println("Color 1 is not exists");
				}
				try
				{
					String col2 = driver.findElement(home2.color2).getCssValue("color");
					System.out.println(col2);
					steps.clickJSButton(home2.color2, driver, name);
					Thread.sleep(1000);
					WomanTeeColor21 = driver.findElement(home2.color2).getCssValue("background-color");
					WomanTeehex21 = Color.fromString(WomanTeeColor21).asHex();
					System.out.println("Image color: "+WomanTeehex21);
					test.log(Status.INFO, MarkupHelper.createLabel("User changed item color to:  "+WomanTeehex21, ExtentColor.BLUE));
					WomanTeeSecondcolor1 = getscreen.capture(driver, "WomanTeeSecondcolor1");
					test.addScreenCaptureFromPath(WomanTeeSecondcolor1);
				}
				catch(Exception e)
				{
					System.out.println("Color 2 is not exists");
				}
				try
				{
					String col3 = driver.findElement(home2.color3).getCssValue("color");
					System.out.println(col3);
					steps.clickJSButton(home2.color3, driver, name);
					Thread.sleep(1000);
					WomanTeeColor31 = driver.findElement(home2.color3).getCssValue("background-color");
					WomanTeehex31 = Color.fromString(WomanTeeColor31).asHex();
					System.out.println("Image color: "+WomanTeehex31);
					test.log(Status.INFO, MarkupHelper.createLabel("User changed item color to:  "+WomanTeehex31, ExtentColor.BLUE));
					WomanTeeThirdcolor1 = getscreen.capture(driver, "WomanTeeThirdcolor1");
					test.addScreenCaptureFromPath(WomanTeeThirdcolor1);
				}
				catch(Exception e)
				{
					System.out.println("Color 3 is not exists");
				}
				try 
				{
					String col4 = driver.findElement(home2.color4).getCssValue("color");
					System.out.println(col4);
					steps.clickJSButton(home2.color4, driver, name);
					Thread.sleep(1000);
					WomanTeeColor41 = driver.findElement(home2.color4).getCssValue("background-color");
					WomanTeehex41 = Color.fromString(WomanTeeColor41).asHex();
					System.out.println("Image color: "+WomanTeehex41);
					test.log(Status.INFO, MarkupHelper.createLabel("User changed item color to:  "+WomanTeehex41, ExtentColor.BLUE));
					WomanTeeFourthcolor1 = getscreen.capture(driver, "WomanTeeFourthcolor1");
					test.addScreenCaptureFromPath(WomanTeeFourthcolor1);
				}
				catch(Exception e)
				{
					System.out.println("Color 4 is not exists");
				}
			}
			catch(Exception e)
			{
				System.out.println("Product 1 doesn't exists");
			}
			WomanTeecqty = driver.findElement(home.Qty).getAttribute("value");
			System.out.println("Current Quantity:  "+ WomanTeecqty);
			test.log(Status.INFO, MarkupHelper.createLabel("Current Quantity of product:  "+WomanTeecqty, ExtentColor.BLUE));
			Thread.sleep(1000);
			steps.clickJSButton(home.Trend_QtyIncrease, driver, name);
			WomanTeeiqty = driver.findElement(home.Qty).getAttribute("value");
			System.out.println("Quantity after increase:  "+ WomanTeeiqty);
			test.log(Status.INFO, MarkupHelper.createLabel("Current Quantity of product after Increase:  "+WomanTeeiqty, ExtentColor.BLUE));
			steps.clickJSButton(home.Trend_QtyDecrease, driver, name);
			WomanTeedqty = driver.findElement(home.Qty).getAttribute("value");
			System.out.println("Quantity after decrease:  "+ WomanTeedqty);
			test.log(Status.INFO, MarkupHelper.createLabel("Current Quantity of product after increase:  "+WomanTeedqty, ExtentColor.BLUE));
			steps.clickJSButton(home.Trend_Addcart, driver, name);
			WomanTeeSizemsg = driver.findElement(home.Trend_Sizeerrormsg).getText();
			System.out.println("User clicked on Addcart before Size selection:  "+WomanTeeSizemsg);
			test.log(Status.INFO, MarkupHelper.createLabel("User clicked on Addcart before Size selection:  "+WomanTeeSizemsg, ExtentColor.RED));
			//steps.clickJSButton(home2.WomanTee_sizeS, driver, name);
			steps.clickJSButton(home2.Size_S, driver, name);
			WomanTeesizeS = driver.findElement(home2.Size_S).getText();
			System.out.println("Selected Size:  "+WomanTeesizeS);
			test.log(Status.INFO, MarkupHelper.createLabel("User selected size:  "+WomanTeesizeS, ExtentColor.BLUE));
			steps.clickJSButton(home2.Size_M, driver, name);
			WomanTeesizeM = driver.findElement(home2.Size_M).getText();
			System.out.println("Selected Size:  "+WomanTeesizeM);
			test.log(Status.INFO, MarkupHelper.createLabel("User selected size:  "+WomanTeesizeM, ExtentColor.BLUE));
			steps.clickJSButton(home2.Size_L, driver, name);
			WomanTeesizeL = driver.findElement(home2.Size_L).getText();
			System.out.println("Selected Size:  "+WomanTeesizeL);
			test.log(Status.INFO, MarkupHelper.createLabel("User selected size:  "+WomanTeesizeL, ExtentColor.BLUE));
			steps.clickJSButton(home2.Size_XL, driver, name);
			WomanTeesizeXL = driver.findElement(home2.Size_XL).getText();
			System.out.println("Selected Size:  "+WomanTeesizeXL);
			test.log(Status.INFO, MarkupHelper.createLabel("User selected size:  "+WomanTeesizeXL, ExtentColor.BLUE));
			steps.clickJSButton(home2.Size_XXL, driver, name);
			WomanTeesizeXXL = driver.findElement(home2.Size_XXL).getText();
			System.out.println("Selected Size:  "+WomanTeesizeXXL);
			test.log(Status.INFO, MarkupHelper.createLabel("User selected size:  "+WomanTeesizeXXL, ExtentColor.BLUE));
			Thread.sleep(1000);
			try
			{
				steps.clickJSButton(home2.Size_XXXL, driver, name);
				WomanTeesizeXXXL = driver.findElement(home2.Size_XXXL).getText();
				System.out.println("Selected Size:  "+WomanTeesizeXXXL);
				test.log(Status.INFO, MarkupHelper.createLabel("User selected size:  "+WomanTeesizeXXXL, ExtentColor.BLUE));
			}
			catch(Exception e)
			{
				System.out.println("XXXL size is not available");
			}
			steps.clickJSButton(home.Trend_FindUrSize, driver, name);
			test.log(Status.INFO, MarkupHelper.createLabel("User clicked on Find your size", ExtentColor.BLUE));
			Thread.sleep(1000);
			try
			{
				String text1 =  driver.findElement(home.Trend_FindUrSize_510radio).getText();
				System.out.println(text1);
				steps.clickJSButton(home.Trend_FindUrSize_510radio, driver, name);
				WomanTeebelow510 = driver.findElement(home.Trend_FindUrSize_510radio).getText();
				System.out.println("User Selected:  "+WomanTeebelow510);
				test.log(Status.INFO, MarkupHelper.createLabel("User selected Height:  "+WomanTeebelow510, ExtentColor.BLUE));
				steps.clickJSButton(home.Trend_FindUrSize_above510radio, driver, name);
				WomanTeeabove510 = driver.findElement(home.Trend_FindUrSize_above510radio).getText();
				System.out.println("User Selected:  "+WomanTeeabove510);
				test.log(Status.INFO, MarkupHelper.createLabel("User selected Height:  "+WomanTeeabove510, ExtentColor.BLUE));
				steps.clickJSButton(home.Trend_FindUrSize_Tightradio, driver, name);
				WomanTeetightfit = driver.findElement(home.Trend_FindUrSize_Tightradio).getText();
				System.out.println("User Selected:  "+WomanTeetightfit);
				test.log(Status.INFO, MarkupHelper.createLabel("User selected Fit:  "+WomanTeetightfit, ExtentColor.BLUE));
				steps.clickJSButton(home.Trend_FindUrSize_Comfortradio, driver, name);
				WomanTeecomfit = driver.findElement(home.Trend_FindUrSize_Comfortradio).getText();
				System.out.println("User Selected:  "+WomanTeecomfit);
				test.log(Status.INFO, MarkupHelper.createLabel("User selected Fit:  "+WomanTeecomfit, ExtentColor.BLUE));
				Thread.sleep(1000);
				steps.selectDropdown(home.Trend_FindUrSize_sizeDD, driver, strDataFileName, "size", name);
				test.log(Status.INFO, MarkupHelper.createLabel("User selected shirt size", ExtentColor.BLUE));
				steps.clickJSButton(home.Trend_FindUrSize_submit, driver, name);
				test.log(Status.INFO, MarkupHelper.createLabel("User clicked on Submit", ExtentColor.BLUE));
				WomanTeefindursize = getscreen.capture(driver, "WomanTeefindursize");
				test.addScreenCaptureFromPath(WomanTeefindursize);
				Thread.sleep(1000);
				steps.clickJSButton(home.Trend_FindUrSize_Okbutton, driver, name);
				test.log(Status.INFO, MarkupHelper.createLabel("system displayed size for given metrics", ExtentColor.BLUE));
				steps.clickJSButton(home.Trend_FindUrSize_closebutton, driver, name);
				test.log(Status.INFO, MarkupHelper.createLabel("User navigate to Product details page", ExtentColor.BLUE));
			}
			catch(Exception e)
			{
				steps.clickJSButton(home.Trend_FindUrSize_closebutton, driver, name);
				test.log(Status.INFO, MarkupHelper.createLabel("User navigate to Product details page", ExtentColor.BLUE));
			}
			Thread.sleep(1000);
			steps.clickJSButton(home2.Size_L, driver, name);
			test.log(Status.INFO, MarkupHelper.createLabel("User selected size", ExtentColor.BLUE));
			steps.clickJSButton(home.Trend_Addcart, driver, name);
			Thread.sleep(1000);
			test.log(Status.INFO, MarkupHelper.createLabel("User added product to Cart", ExtentColor.BLUE));
			WomanTeeaddcart = getscreen.capture(driver, "WomanTeeaddcart");
			test.addScreenCaptureFromPath(WomanTeeaddcart);
			Thread.sleep(1000);
			try
			{
				String edit1 = driver.findElement(home2.Addcart_edit).getText();
				System.out.println(edit1);
				steps.clickJSButton(home2.Addcart_edit, driver, name);
				test.log(Status.INFO, MarkupHelper.createLabel("User clicked on Edit", ExtentColor.BLUE));
				steps.selectDropdown(home2.Addcart_selectsize, driver, strDataFileName, "SelectSize", name);
				steps.selectDropdown(home2.Addcart_selectqty, driver, strDataFileName, "Quantity", name);
				test.log(Status.INFO, MarkupHelper.createLabel("User selected Size and Quantity", ExtentColor.BLUE));
				steps.clickJSButton(home2.Addcart_cancel, driver, name);
				steps.clickJSButton(home2.Addcart_edit, driver, name);
				steps.selectDropdown(home2.Addcart_selectsize, driver, strDataFileName, "SelectSize", name);
				steps.selectDropdown(home2.Addcart_selectqty, driver, strDataFileName, "Quantity", name);
				steps.clickJSButton(home2.Addcart_save, driver, name);
				WomanTeeupdateproduct1 = getscreen.capture(driver, "WomanTeeupdateproduct1");
				test.addScreenCaptureFromPath(WomanTeeupdateproduct1);
				test.log(Status.INFO, MarkupHelper.createLabel("User updated product size and quantity", ExtentColor.BLUE));
			}
			catch(Exception e)
			{
				System.out.println("Edit feature is not available for this product");
			}
			Thread.sleep(2000);
			steps.clickJSButton(home2.Biker_remove, driver, name);
			WomanTeeremovecart = getscreen.capture(driver, "WomanTeeremovecart");
			test.addScreenCaptureFromPath(WomanTeeremovecart);
			test.log(Status.INFO, MarkupHelper.createLabel("User removed product from Cart", ExtentColor.BLUE));
			//checkout process
			steps.clickJSButton(home.Size_L, driver, name);
			test.log(Status.INFO, MarkupHelper.createLabel("User selected size", ExtentColor.BLUE));
			steps.clickJSButton(checkout.BuyNow, driver, name);
			test.log(Status.INFO, MarkupHelper.createLabel("User clicked on BuyNow, navigated to checkout page", ExtentColor.BLUE));
			String buynow1 = getscreen.capture(driver, "buynow1");
			test.addScreenCaptureFromPath(buynow1);
			Thread.sleep(2000);
			// Debit card option
			try
			{
				String Debit = driver.findElement(checkout.Debitcard).getText();
				System.out.println("Payment method:  "+Debit);
				steps.Verifytext(checkout.Debitcard, driver, Debit, name);
				steps.clickJSButton(checkout.Debitcard, driver, name);
				System.out.println("Debit card option is available");
				test.log(Status.INFO, MarkupHelper.createLabel("Payment method:  "+Debit   +    "      Available", ExtentColor.BLUE));
			}
			catch(Exception e)
			{
				System.out.println("Debit card option is not available");
				test.log(Status.INFO, MarkupHelper.createLabel("Debit card option is not available", ExtentColor.BLUE));
			}
			Thread.sleep(2000);
			// Credit card option
			try
			{
				String Credit = driver.findElement(checkout.Creditcard).getText();
				System.out.println("Payment method:  "+Credit);
				steps.Verifytext(checkout.Creditcard, driver, Credit, name);
				steps.clickJSButton(checkout.Creditcard, driver, name);
				System.out.println("Credit card option is available");
				test.log(Status.INFO, MarkupHelper.createLabel("Payment method:  "+Credit+    "      Available", ExtentColor.BLUE));
			}
			catch(Exception e)
			{
				System.out.println("Credit card option is not available");
				test.log(Status.INFO, MarkupHelper.createLabel("Credit card option is not available", ExtentColor.BLUE));
			}
			Thread.sleep(2000);
			// Net Banking option
			try
			{
				String NetBanking = driver.findElement(checkout.Netbanking).getText();
				System.out.println("Payment method:  "+NetBanking);
				steps.Verifytext(checkout.Netbanking, driver, NetBanking, name);
				steps.clickJSButton(checkout.Netbanking, driver, name);
				System.out.println("Net Banking option is available");
				test.log(Status.INFO, MarkupHelper.createLabel("Payment method:  "+NetBanking+    "      Available", ExtentColor.BLUE));
			}
			catch(Exception e)
			{
				System.out.println("Net Banking option is not available");
				test.log(Status.INFO, MarkupHelper.createLabel("Net Banking option is not available", ExtentColor.BLUE));
			}
			Thread.sleep(2000);
			// Paytm option
			try
			{
				String Paytm = driver.findElement(checkout.Paytm).getText();
				System.out.println("Payment method:  "+Paytm);
				steps.Verifytext(checkout.Paytm, driver, Paytm, name);
				steps.clickJSButton(checkout.Paytm, driver, name);
				System.out.println("Paytm option is available");
				test.log(Status.INFO, MarkupHelper.createLabel("Payment method:  "+Paytm+    "      Available", ExtentColor.BLUE));
			}
			catch(Exception e)
			{
				System.out.println("Paytm option is not available");
				test.log(Status.INFO, MarkupHelper.createLabel("Paytm option is not available", ExtentColor.BLUE));
			}
			Thread.sleep(2000);
			// MDS Wallet option
			try
			{
				String MDS = driver.findElement(checkout.MDSWallet).getText();
				System.out.println("Payment method:  "+MDS);
				steps.Verifytext(checkout.MDSWallet, driver, MDS, name);
				steps.clickJSButton(checkout.MDSWallet, driver, name);
				Thread.sleep(2000);
				String MDSpopup = driver.findElement(checkout.MDSWalletPOPup).getText();
				System.out.println("Payment method:  "+MDSpopup);
				Thread.sleep(2000);
				steps.clickJSButton(checkout.MDSWalletPOPupOK, driver, name);
				System.out.println("MDS Wallet option is available");
				test.log(Status.INFO, MarkupHelper.createLabel("Payment method:  "+MDS+    "       Available", ExtentColor.BLUE));
			}
			catch(Exception e)
			{
				System.out.println("MDS Wallet option is not available");
				test.log(Status.INFO, MarkupHelper.createLabel("MDS Wallet option is not available", ExtentColor.BLUE));
			}
			Thread.sleep(2000);
			// Cash on delivery 
			try
			{
				String cash = driver.findElement(checkout.CashonDeliveryText).getText();
				System.out.println("Payment method:  "+cash);
				test.log(Status.INFO, MarkupHelper.createLabel("Payment method:  "+cash+    "        Available", ExtentColor.BLUE));
				steps.Verifytext(checkout.CashonDeliveryText, driver, cash, name);
				steps.clickJSButton(checkout.CashonDeliveryradio, driver, name);
				Thread.sleep(2000);
				String buynow2 = driver.findElement(checkout.PopUpSaveMsg).getText();
				System.out.println("Displayed message:   "+buynow2);
				test.log(Status.INFO, MarkupHelper.createLabel("Displayed message:   "+buynow2, ExtentColor.BLUE));
				Thread.sleep(1000);
				String buynow3 = getscreen.capture(driver, "buynow3");
				test.addScreenCaptureFromPath(buynow3);
				steps.clickJSButton(checkout.Cashondelivery, driver, name);
				test.log(Status.INFO, MarkupHelper.createLabel("User clicked on Cash on Delivery link", ExtentColor.BLUE));
			}
			catch(Exception e)
			{
				System.out.println("Cash on Delivery is not available");
				test.log(Status.INFO, MarkupHelper.createLabel("Cash on Delivery is not available", ExtentColor.BLUE));
			}
			Thread.sleep(2000);
			String checkout1 = getscreen.capture(driver, "checkout1");
			test.addScreenCaptureFromPath(checkout1);
			steps.clickJSButton(checkout.CartIcon, driver, name);
			steps.clickJSButton(home.Trend_Addcart_remove, driver, name);
			Thread.sleep(1000);
			test.log(Status.INFO, MarkupHelper.createLabel("User Completed transactions of 1st Image in WomanTee", ExtentColor.PURPLE));
			WomanTeestatus = passstatus;
			Assert.assertTrue(true);
			System.out.println("WomanTee Successful");
			testresultdata.put(Integer.toString(cnt), new Object[] {name, "WomanTee CheckOut", "WomanTee CheckOut Successful",passstatus});	
		}
		catch(Exception e)
		{
			System.out.println("WomanTee Unsuccessful");
			testresultdata.put(Integer.toString(cnt), new Object[] {name, "WomanTee CheckOut", "WomanTee CheckOut Unsuccessful",failstatus});
			WomanTeestatus = failstatus;
			WomanTeestatusfail = getscreen.capture(driver, "WomanTeestatusfail");
			test.addScreenCaptureFromPath(WomanTeestatusfail);
			Assert.assertTrue(false);
		}
	}
	@BeforeClass
	public void before()
	{
		workbook = new HSSFWorkbook();
		sheet = workbook.createSheet(strDataFileName);
		testresultdata = new LinkedHashMap<String, Object[]>();
		testresultdata.put("1", new Object[] { "Test Step Name", "Expected Result","Actual Result" ,"Status"});
	}
	@AfterClass
	public void after() throws Exception
	{
		Set<String> keyset = testresultdata.keySet();
		int rownum = 0;
		for (String key : keyset) {
			Row row = sheet.createRow(rownum++);
			Object [] objArr = testresultdata.get(key);
			int cellnum = 0;
			for (Object obj : objArr) {
				Cell cell = row.createCell(cellnum++);
				if(obj instanceof Date) 
					cell.setCellValue((Date)obj);
				else if(obj instanceof Boolean)
					cell.setCellValue((Boolean)obj);
				else if(obj instanceof String)
					cell.setCellValue((String)obj);
				else if(obj instanceof Double)
					cell.setCellValue((Double)obj);
			}
		}
		try {
			FileOutputStream out = new FileOutputStream(new File("TestResults/"+strDataFileName +".xls"));
			workbook.write(out);
			out.close();
			System.out.println("Excel written successfully..");
			TestResult();
			driver.quit();
		}
		catch (FileNotFoundException e) 
		{
			e.printStackTrace();
		} 
		catch (IOException e) 
		{
			e.printStackTrace();
		}
	}
	public void TestResult() throws Exception
	{
		HSSFRow row;
		HSSFCell cell;
		WritableFont cellFont;
		WritableCellFormat cellFormat;
		Border border = null;
		int count;
		FileInputStream inp = new FileInputStream( "TestResults/"+strDataFileName +".xls");
		HSSFWorkbook wb = new HSSFWorkbook(inp);
		HSSFSheet sh = wb.getSheetAt(0);
		count=sh.getLastRowNum();
		for (int i = 1; i <= count; i++) 
		{
			row=sh.getRow(i);
			cell=row.getCell(3);
			String status=row.getCell(3).getStringCellValue();
			if(status.equalsIgnoreCase("Pass"))
			{
				HSSFCellStyle pass= wb.createCellStyle();
				pass.setFillForegroundColor(IndexedColors.GREEN.getIndex());
				pass.setFillPattern(CellStyle.SOLID_FOREGROUND);
				cell.setCellStyle(pass);
			}
			else
			{
				HSSFCellStyle fail= wb.createCellStyle();
				fail.setFillForegroundColor(IndexedColors.RED.getIndex());
				fail.setFillPattern(CellStyle.SOLID_FOREGROUND);
				cell.setCellStyle(fail);
			}
		}
		FileOutputStream out = new FileOutputStream("TestResults/"+strDataFileName+".xls");
		wb.write(out);
		wb.close();
	}
}
