package Com.Modules;
import java.io.File;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.IOException;
import java.util.LinkedHashMap;
import java.util.List;
import java.util.Map;
import java.util.Set;
import java.util.Date;
import java.util.concurrent.TimeUnit;
import org.apache.poi.hssf.usermodel.HSSFCell;
import org.apache.poi.hssf.usermodel.HSSFCellStyle;
import org.apache.poi.hssf.usermodel.HSSFRow;
import org.apache.poi.hssf.usermodel.HSSFSheet;
import org.apache.poi.hssf.usermodel.HSSFWorkbook;
import org.apache.poi.ss.usermodel.Row;
import org.apache.poi.ss.usermodel.Cell;
import org.apache.poi.ss.usermodel.CellStyle;
import org.apache.poi.ss.usermodel.IndexedColors;
import org.openqa.selenium.By;
import org.openqa.selenium.Keys;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.interactions.Actions;
import org.openqa.selenium.support.Color;
import org.openqa.selenium.support.ui.ExpectedConditions;
import org.openqa.selenium.support.ui.WebDriverWait;
import org.testng.Assert;
import org.testng.annotations.AfterClass;
import org.testng.annotations.BeforeClass;
import org.testng.annotations.Test;
import com.aventstack.extentreports.Status;
import com.aventstack.extentreports.markuputils.ExtentColor;
import com.aventstack.extentreports.markuputils.MarkupHelper;
import Com.ObjectRepository.Homepage2_OR;
import Com.ObjectRepository.Homepage_OR;
import Com.ObjectRepository.Login_OR;
import Com.ObjectRepository.Posters_OR;
import Com.ObjectRepository.Posters_OR;
import Com.ObjectRepository.Tshirts_OR;
import Com.Utilities.Browsers;
import Com.Utilities.CommonMethods;
import Com.Utilities.GetScreenShot;
import Com.Utilities.Screenshot;
import Com.Utilities.Steps;
import Com.Utilities.TestBase;
import jxl.format.Border;
import jxl.format.UnderlineStyle;
import jxl.write.Colour;
import jxl.write.WritableCellFormat;
import jxl.write.WritableFont;
import jxl.write.WriteException;
public class Posters extends TestBase
{
	WebDriver driver;
	Login_OR login = new Login_OR();
	Steps steps = new Steps();
	GetScreenShot getscreen = new GetScreenShot();
	Screenshot screen = new Screenshot();
	public String strAbsolutepath = new File("").getAbsolutePath();
	HSSFWorkbook workbook;
	HSSFSheet sheet;
	String strDataFileName = this.getClass().getSimpleName();
	Map<String, Object[]> testresultdata; 
	CommonMethods commonmethods = new CommonMethods();
	Homepage_OR home = new Homepage_OR();
	Homepage2_OR home2 = new Homepage2_OR();
	//Posters_OR Posters = new Posters_OR();
	Posters_OR posters = new Posters_OR();
	Screenshot screenshot = new Screenshot();
	String passstatus = screenshot.status("PASS");
	String failstatus = screenshot.status("FAIL");
	int cnt = 2;
	public static String Postersstatus = "";
	public static String Postersstatusfail = "";
	public static String PostersDescription1 = "";
	public static String PostersMRP = "";
	public static String PosterProduct1 = "";
	public static String PosterProduct2 = "";
	public static String PosterProduct3 = "";
	public static String Posterscqty = "";
	public static String Postersiqty = "";
	public static String Postersdqty = "";
	public static String Postersaddcart = "";
	public static String Postersupdateproduct1 = "";
	public static String Postersremovecart = "";
	public static String currentproduct1 = "";
	public Posters()
	{
		Browsers b = new Browsers();
		this.driver = b.getBrowsers("FF");		 
	}
	@Test
	public void PostersCheckout() throws Exception
	{
		String name = new Object(){}.getClass().getEnclosingMethod().getName();
		cnt ++;
		try 
		{
			test = extent.createTest("Posters");
			commonmethods.Login(driver);
			test.log(Status.INFO, MarkupHelper.createLabel("User able to Login", ExtentColor.BLUE));
			Thread.sleep(3000);
			steps.clickButton(home.Mydreamstore, driver, name);
			String homepage = getscreen.capture(driver, name);
			test.addScreenCaptureFromPath(homepage);
			String Parent = driver.getWindowHandle();
			steps.MoveElement(posters.PostersLink, driver, name);
			steps.clickJSButton(posters.PostersLink, driver, name);
			test.log(Status.INFO, MarkupHelper.createLabel("User clicked on Posters link", ExtentColor.BLUE));
			Thread.sleep(2000);
			//steps.WaitUntilElementPresent(Posters.Posters_Image1, driver);
			steps.WaitElementPresent(posters.Posters_Image1, driver);
			steps.clickJSButton(posters.Posters_Image1, driver, name);
			test.log(Status.INFO, MarkupHelper.createLabel("User clicked on First Image in Posters", ExtentColor.BLUE));
			try
			{
				steps.clickButton(posters.Readmore, driver, name);
				PostersDescription1 = driver.findElement(posters.Posters_Description).getText();
				steps.clickButton(posters.Readless, driver, name);
				test.log(Status.INFO, MarkupHelper.createLabel("Product Description:  "+ PostersDescription1, ExtentColor.BLUE));
			}
			catch(Exception e)
			{
				System.out.println("Description hasn't fetched");
			}
			PostersMRP = driver.findElement(posters.Posters_MRP).getText();
			test.log(Status.INFO, MarkupHelper.createLabel("Product Price:  "+PostersMRP, ExtentColor.BLUE));
			Thread.sleep(2000);
			currentproduct1 = driver.findElement(posters.Posters_ProductDD).getText();
			System.out.println("Product:  "+currentproduct1);
			steps.selectDropdown(posters.Posters_ProductDD, driver, strDataFileName, "Product2", name);
			Thread.sleep(1000);
			PosterProduct2 = getscreen.capture(driver, "PosterProduct2");
			test.addScreenCaptureFromPath(PosterProduct2);
			test.log(Status.INFO, MarkupHelper.createLabel("User Selected 2nd Product", ExtentColor.BLUE));
			Thread.sleep(2000);
			steps.selectDropdown(posters.Posters_ProductDD, driver, strDataFileName, "Product3", name);
			Thread.sleep(1000);
			PosterProduct3 = getscreen.capture(driver, "PosterProduct3");
			test.addScreenCaptureFromPath(PosterProduct3);
			test.log(Status.INFO, MarkupHelper.createLabel("User Selected 3rd Product", ExtentColor.BLUE));
			Thread.sleep(2000);
			steps.selectDropdown(posters.Posters_ProductDD, driver, strDataFileName, "Product1", name);
			Thread.sleep(1000);
			PosterProduct1 = getscreen.capture(driver, "PosterProduct1");
			test.addScreenCaptureFromPath(PosterProduct1);
			test.log(Status.INFO, MarkupHelper.createLabel("User Selected 1st Product", ExtentColor.BLUE));
			Posterscqty = driver.findElement(home.Qty).getAttribute("value");
			System.out.println("Current Quantity:  "+ Posterscqty);
			test.log(Status.INFO, MarkupHelper.createLabel("Current Quantity of product:  "+Posterscqty, ExtentColor.BLUE));
			Thread.sleep(1000);
			steps.clickButton(home.Trend_QtyIncrease, driver, name);
			Postersiqty = driver.findElement(home.Qty).getAttribute("value");
			System.out.println("Quantity after increase:  "+ Postersiqty);
			test.log(Status.INFO, MarkupHelper.createLabel("Current Quantity of product after Increase:  "+Postersiqty, ExtentColor.BLUE));
			steps.clickButton(home.Trend_QtyDecrease, driver, name);
			Postersdqty = driver.findElement(home.Qty).getAttribute("value");
			System.out.println("Quantity after decrease:  "+ Postersdqty);
			test.log(Status.INFO, MarkupHelper.createLabel("Current Quantity of product after increase:  "+Postersdqty, ExtentColor.BLUE));
			steps.clickButton(home.Trend_Addcart, driver, name);
			Thread.sleep(1000);
			test.log(Status.INFO, MarkupHelper.createLabel("User added product to Cart", ExtentColor.BLUE));
			Postersaddcart = getscreen.capture(driver, "Postersaddcart");
			test.addScreenCaptureFromPath(Postersaddcart);
			Thread.sleep(1000);
			steps.clickButton(home2.Addcart_edit, driver, name);
			test.log(Status.INFO, MarkupHelper.createLabel("User clicked on Edit", ExtentColor.BLUE));
			//steps.selectDropdown(home2.Addcart_selectsize, driver, strDataFileName, "SelectSize", name);
			steps.selectDropdown(home2.Addcart_selectqty, driver, strDataFileName, "Quantity", name);
			test.log(Status.INFO, MarkupHelper.createLabel("User selected Size and Quantity", ExtentColor.BLUE));
			steps.clickButton(home2.Addcart_cancel, driver, name);
			steps.clickButton(home2.Addcart_edit, driver, name);
			//steps.selectDropdown(home2.Addcart_selectsize, driver, strDataFileName, "SelectSize", name);
			steps.selectDropdown(home2.Addcart_selectqty, driver, strDataFileName, "Quantity", name);
			steps.clickButton(home2.Addcart_save, driver, name);
			Postersupdateproduct1 = getscreen.capture(driver, "Postersupdateproduct1");
			test.addScreenCaptureFromPath(Postersupdateproduct1);
			test.log(Status.INFO, MarkupHelper.createLabel("User updated product size and quantity", ExtentColor.BLUE));
			Thread.sleep(2000);
			steps.clickButton(home2.Biker_remove, driver, name);
			Postersremovecart = getscreen.capture(driver, "Postersremovecart");
			test.addScreenCaptureFromPath(Postersremovecart);
			test.log(Status.INFO, MarkupHelper.createLabel("User removed product from Cart", ExtentColor.BLUE));
			test.log(Status.INFO, MarkupHelper.createLabel("User Completed transactions of 1st Image in Posters", ExtentColor.PURPLE));
			Postersstatus = passstatus;
			Assert.assertTrue(true);
			System.out.println("Posters Successful");
			testresultdata.put(Integer.toString(cnt), new Object[] {name, "Posters Checkout", "Posters Checkout Successful",passstatus});	
		}
		catch(Exception e)
		{
			System.out.println("Posters Checkout Unsuccessful");
			testresultdata.put(Integer.toString(cnt), new Object[] {name, "Posters Checkout", "Posters Checkout Unsuccessful",failstatus});
			Postersstatus = failstatus;
			Postersstatusfail = getscreen.capture(driver, "Postersstatusfail");
			test.addScreenCaptureFromPath(Postersstatusfail);
			Assert.assertTrue(false);
		}
	}
	@BeforeClass
	public void before()
	{
		workbook = new HSSFWorkbook();
		sheet = workbook.createSheet(strDataFileName);
		testresultdata = new LinkedHashMap<String, Object[]>();
		testresultdata.put("1", new Object[] { "Test Step Name", "Expected Result","Actual Result" ,"Status"});
	}
	@AfterClass
	public void after() throws Exception
	{
		Set<String> keyset = testresultdata.keySet();
		int rownum = 0;
		for (String key : keyset) {
			Row row = sheet.createRow(rownum++);
			Object [] objArr = testresultdata.get(key);
			int cellnum = 0;
			for (Object obj : objArr) {
				Cell cell = row.createCell(cellnum++);
				if(obj instanceof Date) 
					cell.setCellValue((Date)obj);
				else if(obj instanceof Boolean)
					cell.setCellValue((Boolean)obj);
				else if(obj instanceof String)
					cell.setCellValue((String)obj);
				else if(obj instanceof Double)
					cell.setCellValue((Double)obj);
			}
		}
		try {
			FileOutputStream out = new FileOutputStream(new File("TestResults/"+strDataFileName +".xls"));
			workbook.write(out);
			out.close();
			System.out.println("Excel written successfully..");
			TestResult();
			//driver.quit();
		}
		catch (FileNotFoundException e) 
		{
			e.printStackTrace();
		} 
		catch (IOException e) 
		{
			e.printStackTrace();
		}
	}
	public void TestResult() throws Exception
	{
		HSSFRow row;
		HSSFCell cell;
		WritableFont cellFont;
		WritableCellFormat cellFormat;
		Border border = null;
		int count;
		FileInputStream inp = new FileInputStream( "TestResults/"+strDataFileName +".xls");
		HSSFWorkbook wb = new HSSFWorkbook(inp);
		HSSFSheet sh = wb.getSheetAt(0);
		count=sh.getLastRowNum();
		for (int i = 1; i <= count; i++) 
		{
			row=sh.getRow(i);
			cell=row.getCell(3);
			String status=row.getCell(3).getStringCellValue();
			if(status.equalsIgnoreCase("Pass"))
			{
				HSSFCellStyle pass= wb.createCellStyle();
				pass.setFillForegroundColor(IndexedColors.GREEN.getIndex());
				pass.setFillPattern(CellStyle.SOLID_FOREGROUND);
				cell.setCellStyle(pass);
			}
			else
			{
				HSSFCellStyle fail= wb.createCellStyle();
				fail.setFillForegroundColor(IndexedColors.RED.getIndex());
				fail.setFillPattern(CellStyle.SOLID_FOREGROUND);
				cell.setCellStyle(fail);
			}
		}
		FileOutputStream out = new FileOutputStream("TestResults/"+strDataFileName+".xls");
		wb.write(out);
		wb.close();
	}
}
